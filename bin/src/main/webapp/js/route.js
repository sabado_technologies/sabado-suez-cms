var myApp = angular.module('myApp', [ 'ui.router']);

myApp.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/login');

	$stateProvider

	// HOME STATES AND NESTED VIEWS ========================================
	.state('login', {
		url : '/login',
		templateUrl : 'html/login.html',
		controller : 'LoginController',
		data : {
			displayName : 'Login',
		}
	}).state('warehouse', {
		url : '/warehouse',
		templateUrl : 'dashboards/warehousemanager.html',
		controller : ''
		
	})
	// HOME STATES AND NESTED VIEWS ========================================
	
});

