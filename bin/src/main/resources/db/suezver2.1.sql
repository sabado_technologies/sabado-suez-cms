-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: suez_version2
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `batch`
--

DROP TABLE IF EXISTS `batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch` (
  `batch_id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_number` int(25) DEFAULT NULL,
  `batch_date` timestamp NULL DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`batch_id`),
  KEY `orderid_idx` (`order_id`),
  KEY `batchshiftid_idx` (`shift_id`),
  CONSTRAINT `batchshiftid` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`shift_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `orderid` FOREIGN KEY (`order_id`) REFERENCES `process_order` (`process_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch`
--

LOCK TABLES `batch` WRITE;
/*!40000 ALTER TABLE `batch` DISABLE KEYS */;
INSERT INTO `batch` VALUES (1,2671,'2017-11-01 18:29:59',1,1),(2,2672,'2017-11-01 18:29:59',2,2);
/*!40000 ALTER TABLE `batch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(45) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`branch_id`),
  KEY `branch_locationid_idx` (`location_id`),
  CONSTRAINT `branch_locationid` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` VALUES (1,'bangalore',5),(2,'Mumbai',10);
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `container`
--

DROP TABLE IF EXISTS `container`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `container` (
  `container_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_name` varchar(45) DEFAULT NULL,
  `container_type` varchar(45) DEFAULT NULL,
  `capacity` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`container_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `container`
--

LOCK TABLES `container` WRITE;
/*!40000 ALTER TABLE `container` DISABLE KEYS */;
INSERT INTO `container` VALUES (1,'pail','small',25),(2,'pellete','large',1000),(3,'tots','medium',500);
/*!40000 ALTER TABLE `container` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designation`
--

DROP TABLE IF EXISTS `designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designation` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `designation_name` varchar(45) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designation`
--

LOCK TABLES `designation` WRITE;
/*!40000 ALTER TABLE `designation` DISABLE KEYS */;
INSERT INTO `designation` VALUES (1,'Chemical Engineer','2017-11-01 18:29:59',NULL),(2,'HR Management','2017-11-01 18:29:59',NULL),(3,'Working Supervisor','2017-11-01 18:29:59',NULL);
/*!40000 ALTER TABLE `designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(45) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `level`
--

LOCK TABLES `level` WRITE;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` VALUES (1,'world',1),(2,'country',1),(3,'zone',1),(4,'state',1),(5,'city',1),(6,'area',1);
/*!40000 ALTER TABLE `level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(45) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `locationlevelid_idx` (`level_id`),
  KEY `locationparentid_idx` (`parent_id`),
  KEY `locationcreated_by_idx` (`created_by`),
  KEY `locationmodified_by_idx` (`modified_by`),
  CONSTRAINT `locationcreated_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`ssoid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `locationlevelid` FOREIGN KEY (`level_id`) REFERENCES `level` (`level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `locationmodified_by` FOREIGN KEY (`modified_by`) REFERENCES `user` (`ssoid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `locationparentid` FOREIGN KEY (`parent_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'continent',1,NULL,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(2,'India',2,1,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(3,'Southzone',3,2,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(4,'Karnataka',4,3,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(5,'Bangalore',5,4,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(6,'Kadugodi',6,5,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(7,'Northzone',3,2,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(8,'Eastzone',3,2,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(9,'Maharastra',4,3,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(10,'Mumbai',5,4,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(11,'anderi',6,5,NULL,'2017-11-01 18:29:59',NULL,NULL,1),(12,'bhannergatta',6,5,NULL,'2017-11-01 18:29:59',NULL,NULL,1);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packaging`
--

DROP TABLE IF EXISTS `packaging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packaging` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_barcode` varchar(45) DEFAULT NULL,
  `process_order_id` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `packed_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`package_id`),
  KEY `batchid_idx` (`batch_id`),
  KEY `containerid_idx` (`container_id`),
  KEY `packageprocessorderid_idx` (`process_order_id`),
  CONSTRAINT `packageprocessorderid` FOREIGN KEY (`process_order_id`) REFERENCES `process_order` (`process_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `packingbatchid` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`batch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `packingcontainerid` FOREIGN KEY (`container_id`) REFERENCES `container` (`container_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packaging`
--

LOCK TABLES `packaging` WRITE;
/*!40000 ALTER TABLE `packaging` DISABLE KEYS */;
INSERT INTO `packaging` VALUES (1,NULL,1,1,1,250,NULL),(2,NULL,1,2,1,1000,NULL),(3,NULL,2,1,2,500,NULL);
/*!40000 ALTER TABLE `packaging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plant`
--

DROP TABLE IF EXISTS `plant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plant` (
  `plant_id` int(11) NOT NULL AUTO_INCREMENT,
  `Plant_number` int(11) DEFAULT NULL,
  `plant_name` varchar(45) DEFAULT NULL,
  `plant_email` varchar(45) DEFAULT NULL,
  `plant_phone_number` varchar(45) DEFAULT NULL,
  `plant_location_id` int(11) DEFAULT NULL,
  `plant_address` varchar(45) DEFAULT NULL,
  `plant_pincode` int(11) DEFAULT NULL,
  PRIMARY KEY (`plant_id`),
  KEY `plantlocation_idx` (`plant_location_id`),
  CONSTRAINT `plantlocation` FOREIGN KEY (`plant_location_id`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plant`
--

LOCK TABLES `plant` WRITE;
/*!40000 ALTER TABLE `plant` DISABLE KEYS */;
INSERT INTO `plant` VALUES (1,5583,'Hoskote','abc@suez.com','98898989',6,'xyz',5600321),(2,5584,'Bhannerghatta','xyz@suez.com','214421',12,'vgvvhgv',560036),(3,6844,'anderi','aaa@suez.com','123123',11,'saddasd',782002);
/*!40000 ALTER TABLE `plant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process`
--

DROP TABLE IF EXISTS `process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process` (
  `process_id` int(11) NOT NULL AUTO_INCREMENT,
  `process_name` varchar(45) DEFAULT NULL,
  `process_description` varchar(45) DEFAULT NULL,
  `child_of` int(11) DEFAULT NULL,
  PRIMARY KEY (`process_id`),
  KEY `childof_idx` (`child_of`),
  CONSTRAINT `childof` FOREIGN KEY (`child_of`) REFERENCES `process` (`process_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process`
--

LOCK TABLES `process` WRITE;
/*!40000 ALTER TABLE `process` DISABLE KEYS */;
INSERT INTO `process` VALUES (1,'Reading Planer',NULL,NULL),(2,'release the Rm',NULL,1),(3,'Charging RM',NULL,NULL),(4,'scan Barcode of RM',NULL,3),(5,'Attach label for leftover',NULL,3),(6,'Packaging',NULL,NULL),(7,'place packed material in WH',NULL,6);
/*!40000 ALTER TABLE `process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process_order`
--

DROP TABLE IF EXISTS `process_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process_order` (
  `process_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `volume_in_ltrs` decimal(10,0) DEFAULT NULL,
  `process_order_barcode` varchar(45) DEFAULT NULL,
  `process_order_type_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `reservation` varchar(45) DEFAULT NULL,
  `storage_code` int(11) DEFAULT NULL,
  `ssoid` varchar(45) DEFAULT NULL,
  `process_tracker_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`process_order_id`),
  KEY `productid_idx` (`product_id`),
  KEY `processordertypeid_idx` (`process_order_type_id`),
  KEY `processstatusid_idx` (`status_id`),
  KEY `processordertrackerid_idx` (`process_tracker_id`),
  KEY `processusersoid_idx` (`ssoid`),
  CONSTRAINT `processordertrackerid` FOREIGN KEY (`process_tracker_id`) REFERENCES `process_tracker` (`process_tracker_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processordertypeid` FOREIGN KEY (`process_order_type_id`) REFERENCES `process_order_type` (`process_order_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processstatusid` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processusersoid` FOREIGN KEY (`ssoid`) REFERENCES `user` (`ssoid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `productid` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_order`
--

LOCK TABLES `process_order` WRITE;
/*!40000 ALTER TABLE `process_order` DISABLE KEYS */;
INSERT INTO `process_order` VALUES (1,1,1600,1413,NULL,1,NULL,NULL,2,'126',NULL),(2,1,1600,1231,NULL,2,NULL,NULL,2,'127',NULL);
/*!40000 ALTER TABLE `process_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process_order_type`
--

DROP TABLE IF EXISTS `process_order_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process_order_type` (
  `process_order_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `process_order_type_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`process_order_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_order_type`
--

LOCK TABLES `process_order_type` WRITE;
/*!40000 ALTER TABLE `process_order_type` DISABLE KEYS */;
INSERT INTO `process_order_type` VALUES (1,'PI01'),(2,'ZPKG');
/*!40000 ALTER TABLE `process_order_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process_sequence`
--

DROP TABLE IF EXISTS `process_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process_sequence` (
  `process_Sequence_id` int(11) NOT NULL AUTO_INCREMENT,
  `process_order_id` int(11) DEFAULT NULL,
  `rawmanterial_id` int(11) DEFAULT NULL,
  `rawmaterial_quantity` varchar(45) DEFAULT NULL,
  `process_sequence_number` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `phase` varchar(45) DEFAULT NULL,
  `staus_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`process_Sequence_id`),
  KEY `processstatus_idx` (`staus_id`),
  KEY `rawmaterialid_idx` (`rawmanterial_id`),
  KEY `batchid_idx` (`batch_id`),
  KEY `processseqorderid_idx` (`process_order_id`),
  CONSTRAINT `batchid` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`batch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processseqorderid` FOREIGN KEY (`process_order_id`) REFERENCES `process_order` (`process_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `processstatus` FOREIGN KEY (`staus_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rawmaterialid` FOREIGN KEY (`rawmanterial_id`) REFERENCES `rawmaterial` (`rawmaterial_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_sequence`
--

LOCK TABLES `process_sequence` WRITE;
/*!40000 ALTER TABLE `process_sequence` DISABLE KEYS */;
INSERT INTO `process_sequence` VALUES (1,1,1,'300',1,1,'1100',NULL),(2,1,2,'100',2,1,'1120',NULL),(3,1,3,'50',3,1,'1130',NULL);
/*!40000 ALTER TABLE `process_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process_tracker`
--

DROP TABLE IF EXISTS `process_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process_tracker` (
  `process_tracker_id` int(11) NOT NULL AUTO_INCREMENT,
  `process_order_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `process_sequence_id` int(11) DEFAULT NULL,
  `sensor_id` int(11) DEFAULT NULL,
  `weight_from_sensor` decimal(10,0) DEFAULT NULL,
  `temparature_from_reactor` decimal(10,0) DEFAULT NULL,
  `process_start_time` timestamp NULL DEFAULT NULL,
  `process_end_time` timestamp NULL DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`process_tracker_id`),
  KEY `trackerprocessid_idx` (`process_id`),
  KEY `trackersequenceid_idx` (`process_sequence_id`),
  KEY `statusid_idx` (`status_id`),
  KEY `sensorid_idx` (`sensor_id`),
  KEY `trackerprocessorderid_idx` (`process_order_id`),
  CONSTRAINT `sensorid` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`sensor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `statusid` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trackerprocessid` FOREIGN KEY (`process_id`) REFERENCES `process` (`process_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trackerprocessorderid` FOREIGN KEY (`process_order_id`) REFERENCES `process_order` (`process_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trackersequenceid` FOREIGN KEY (`process_sequence_id`) REFERENCES `process_sequence` (`process_Sequence_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_tracker`
--

LOCK TABLES `process_tracker` WRITE;
/*!40000 ALTER TABLE `process_tracker` DISABLE KEYS */;
INSERT INTO `process_tracker` VALUES (1,1,1,1,1,300,45,NULL,NULL,NULL),(2,1,3,2,3,50,35,NULL,NULL,NULL),(3,1,6,3,1,200,30,NULL,NULL,NULL);
/*!40000 ALTER TABLE `process_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `process_product_number` bigint(20) DEFAULT NULL,
  `product_name` varchar(45) DEFAULT NULL,
  `product_description` varchar(45) DEFAULT NULL,
  `product_barcode` varchar(45) DEFAULT NULL,
  `product_group_id` int(11) DEFAULT NULL,
  `packaging_product_number` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `productgrupid_idx` (`product_group_id`),
  CONSTRAINT `productgrupid` FOREIGN KEY (`product_group_id`) REFERENCES `product_group` (`product_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,5023286,'hyperspere MSI140',NULL,NULL,1,6101254),(2,5023288,'hypomerseer psi',NULL,NULL,1,6202124);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_group`
--

DROP TABLE IF EXISTS `product_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_group` (
  `product_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_group_name` varchar(45) DEFAULT NULL,
  `product_group_number` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`product_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_group`
--

LOCK TABLES `product_group` WRITE;
/*!40000 ALTER TABLE `product_group` DISABLE KEYS */;
INSERT INTO `product_group` VALUES (1,'generic',23646),(2,'generic-container',23916),(3,'corrosiuos',23913);
/*!40000 ALTER TABLE `product_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rawmaterial`
--

DROP TABLE IF EXISTS `rawmaterial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rawmaterial` (
  `rawmaterial_id` int(11) NOT NULL AUTO_INCREMENT,
  `rawmaterial_number` int(11) DEFAULT NULL,
  `rawmaterial_description` varchar(255) DEFAULT NULL,
  `rawmaterial_barcode` varchar(255) DEFAULT NULL,
  `rawmaterial_typeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`rawmaterial_id`),
  KEY `rawmaterialtypeid_idx` (`rawmaterial_typeid`),
  CONSTRAINT `rawmaterialtypeid` FOREIGN KEY (`rawmaterial_typeid`) REFERENCES `rawmaterial_type` (`rawmaterial_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rawmaterial`
--

LOCK TABLES `rawmaterial` WRITE;
/*!40000 ALTER TABLE `rawmaterial` DISABLE KEYS */;
INSERT INTO `rawmaterial` VALUES (1,4105,'COGULANT',NULL,1),(2,9298,'DEIONIZED WATER',NULL,1),(3,2502,'sodium hydrox',NULL,1);
/*!40000 ALTER TABLE `rawmaterial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rawmaterial_type`
--

DROP TABLE IF EXISTS `rawmaterial_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rawmaterial_type` (
  `rawmaterial_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `rawmaterial_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`rawmaterial_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rawmaterial_type`
--

LOCK TABLES `rawmaterial_type` WRITE;
/*!40000 ALTER TABLE `rawmaterial_type` DISABLE KEYS */;
INSERT INTO `rawmaterial_type` VALUES (1,'liquid'),(2,'liquid'),(3,'powder'),(4,'crystals');
/*!40000 ALTER TABLE `rawmaterial_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin','2017-11-01 18:29:59',NULL),(2,'warehouse_manager','2017-11-01 18:29:59',NULL),(3,'Production_operator','2017-11-01 18:29:59',NULL),(4,'supervisor','2017-11-01 18:29:59',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors` (
  `sensor_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_type` varchar(45) DEFAULT NULL,
  `associated_reactor` varchar(45) DEFAULT NULL,
  `machine_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensors`
--

LOCK TABLES `sensors` WRITE;
/*!40000 ALTER TABLE `sensors` DISABLE KEYS */;
INSERT INTO `sensors` VALUES (1,'Temparature Sensor','1MT','njnads'),(2,'Temparature Sensor','5MT','nkdansk'),(3,'Weight Sensor','1MT','jadsnkj'),(4,'Weight Sensor','5MT','bsadn');
/*!40000 ALTER TABLE `sensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shift`
--

DROP TABLE IF EXISTS `shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shift` (
  `shift_id` int(11) NOT NULL AUTO_INCREMENT,
  `shift_number` int(11) DEFAULT NULL,
  `shift_start_time` timestamp NULL DEFAULT NULL,
  `shift_end_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`shift_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shift`
--

LOCK TABLES `shift` WRITE;
/*!40000 ALTER TABLE `shift` DISABLE KEYS */;
INSERT INTO `shift` VALUES (1,1111,NULL,NULL),(2,2222,NULL,NULL);
/*!40000 ALTER TABLE `shift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'started'),(2,'completed'),(3,'error'),(4,'failure');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) DEFAULT NULL,
  `ssoid` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `plant_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `pincode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ssoid_UNIQUE` (`ssoid`),
  KEY `userroleid_idx` (`role_id`),
  KEY `userplantid_idx` (`plant_id`),
  KEY `userdeignation_idx` (`designation_id`),
  CONSTRAINT `userdeignation` FOREIGN KEY (`designation_id`) REFERENCES `designation` (`designation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userplantid` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`plant_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userroleid` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'kiran','123','abc',1,1,1,NULL,NULL,NULL,NULL),(2,'shashank','124','lmn',1,2,2,NULL,NULL,NULL,NULL),(3,'neeraj','125','opq',1,3,3,NULL,NULL,NULL,NULL),(4,'kapil','126','rst',1,4,3,NULL,NULL,NULL,NULL),(5,'sachin','127','xyz',1,4,3,NULL,NULL,NULL,NULL),(6,'virat','128','opr',2,1,1,NULL,NULL,NULL,NULL),(7,'rohith','129','blr',2,2,2,NULL,NULL,NULL,NULL),(8,'sushanth','122','manglr',2,3,3,NULL,NULL,NULL,NULL),(9,'suresh','130','jzz',2,4,3,NULL,NULL,NULL,NULL),(10,'mahesh','131','ccc',2,4,3,NULL,NULL,NULL,NULL),(11,'girish','132','ddd',2,4,3,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_log`
--

DROP TABLE IF EXISTS `user_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_log` (
  `user_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `ssoid` varchar(45) DEFAULT NULL,
  `login_time` timestamp NULL DEFAULT NULL,
  `logout_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_log_id`),
  KEY `userlogssoid_idx` (`ssoid`),
  CONSTRAINT `userlogssoid` FOREIGN KEY (`ssoid`) REFERENCES `user` (`ssoid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_log`
--

LOCK TABLES `user_log` WRITE;
/*!40000 ALTER TABLE `user_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-11 17:17:25
