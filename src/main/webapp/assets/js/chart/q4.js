
// Create the chart
Highcharts.chart('q4', {
     credits: {
    enabled: false
  },
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Production report of Sept 02  to Dec 30'
    },
    subtitle: {
        text: 'Click the slices to view versions'
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.1f}Tons'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },
    series: [{
        name: 'Production',
        colorByPoint: true,
        data: [{
            name: 'Planned Production Qty. ',
            y: 50,
           
        },
         {
            name: 'Actual Production Qty.',
            y: 40,
            drilldown: 'ap'
        },
        
         
        


         {
            name: 'Outstanding Production Qty.',
            y: 10,
            drilldown: null
        }]
    }],
    drilldown: {
        series: [{
            name: 'production report',
            id: 'po',
            data: [
                ['v11.0', 24.13],
                ['v8.0', 17.2],
                ['v9.0', 8.11],
                ['v10.0', 5.33],
                ['v6.0', 1.06],
                ['v7.0', 0.5]
            ]
        }, {
            name: 'Actual Production Qty.',
            id: 'ap',
            data: [
                  ['FLOGARD MS6209', 5],
                ['KLEEN MCT511 ', 4.32],
                ['BETZDEARBORN AP1110', 3.68],
                ['DEPOSITROL BL5400', 2.96],
                ['PROCHEM 4H5', 2.96],
                ['BETZDEARBORN AE1125', 2.96]
            ]
        }  ]
    }
}); 
