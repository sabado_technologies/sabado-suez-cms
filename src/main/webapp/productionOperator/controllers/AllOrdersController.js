App.controller("AllOrdersController", function($scope, $state, Flash,
		AllOrdersService, $location) {
	
	$scope.showTodays = false;
	$scope.showAll = true;
	$scope.showImg=false;
	
	AllOrdersService.getAllOrders().then(function(response) {
		(response);
		if (response.data !== 'undefined') {
			$scope.allOrders = response.data.data;
			if($scope.allOrders.length=="0"){
				  $scope.showImg=true;
					
				}else if($scope.allOrders.length !="0"){
					$scope.showImg=false;
					
				}
		}

	});
	
//////////////today's orders  8,15,18,14 ///////////////////////
	AllOrdersService.getTodaysOrders().then(function(response) {
		(response);
		if (response.data !== 'undefined') {
			$scope.todaysOrders = response.data.data;
		}

	});
////////////past   8,15,18,14 batch ///// 
	AllOrdersService.getPastOrders().then(function(response) {
		(response);
		if (response.data !== 'undefined') {
			$scope.pastOrders = response.data.data;
		}

	});
	
////////////future   8,15,18,14 batch ///// 
	AllOrdersService.getFutureOrders().then(function(response) {
		(response);
		if (response.data !== 'undefined') {
			$scope.futureOrders = response.data.data;
		}

	});
	
	AllOrdersService.getTodaysCompletedOrders().then(function(response) {
		(response);
		if (response.data !== 'undefined') {
			$scope.completedOrders = response.data.data;
		}

	});
	
	$scope.all = function(){
		
		$scope.showTodays = false;
		$scope.showAll = true;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = false;
		
		if($scope.allOrders.length=="0"){
			  $scope.showImg=true;
				
			}else if($scope.allOrders.length !="0"){
				$scope.showImg=false;
				
			}
		
	}
	
	$scope.todays = function(){
		$scope.showTodays = true;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = false;
		
		if($scope.todaysOrders.length=="0"){
			  $scope.showImg=true;
				
			}else if($scope.todaysOrders.length !="0"){
				$scope.showImg=false;
				
			}
	}
	
	$scope.past = function(){
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = true;
		$scope.showFuture = false;
		$scope.showCompleted = false;
		
		if($scope.pastOrders.length=="0"){
			  $scope.showImg=true;
				
			}else if($scope.pastOrders.length !="0"){
				$scope.showImg=false;
				
			}
		
	}
	
	$scope.future = function(){
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showCompleted = false;
		$scope.showFuture = true;
		
		if($scope.futureOrders.length=="0"){
		  $scope.showImg=true;
			
		}else if($scope.futureOrders.length !="0"){
			$scope.showImg=false;
			
		}
		
	}
	
	$scope.finished = function(){
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = true;
		
		if($scope.completedOrders.length=="0"){
		  $scope.showImg=true;
			
		}else if($scope.completedOrders.length !="0"){
			$scope.showImg=false;
			
		}
		
	}
	
	$scope.allStatus = function(processorderProdNumber,status){
		$scope.orderNumber =processorderProdNumber;
		$scope.currentStatus=status;
		
		AllOrdersService.getStatus(processorderProdNumber).then(function(response) {
			(response);
			if (response.data !== 'undefined') {
				$scope.status = response.data.data;
				($scope.status);
			}

		});
		AllOrdersService.getChargedStatus(processorderProdNumber).then(function(response) {
			(response);
			if (response.data !== 'undefined') {
				$scope.chargedStatus = response.data.data;
				($scope.chargedStatus);
			}

		});
		AllOrdersService.getPackedStatus(processorderProdNumber).then(function(response) {
			(response);
			if (response.data !== 'undefined') {
				$scope.packedStatus = response.data.data;
				($scope.packedStatus);
			}

		});
		AllOrdersService.getQualityCheckStatus(processorderProdNumber).then(function(response) {
			(response);
			if (response.data !== 'undefined') {
				$scope.qualityCheckStatus = response.data.data;
				($scope.qualityCheckStatus);
			}

		});
		AllOrdersService.getQualityInspectionStatus(processorderProdNumber).then(function(response) {
			(response);
			if (response.data !== 'undefined') {
				$scope.qualityInspectionStatus = response.data.data;
				($scope.qualityInspectionStatus);
			}

		});
		
		AllOrdersService.getInventoryStatus(processorderProdNumber).then(function(response) {
			(response);
			if (response.data !== 'undefined') {
				$scope.inventoryStatus = response.data.data;
				($scope.inventoryStatus);
			}

		});
		
		AllOrdersService.getRmInventoryStatus(processorderProdNumber).then(function(response) {
			(response);
			if (response.data !== 'undefined') {
				$scope.rmInventoryStatus = response.data.data;
				($scope.rmInventoryStatus);
			}

		});
	}
	
	
});