App.controller("RepackingInventoryCheckDetailsController", function($state, $scope,
		ProductOperatorService, $localStorage,$window) {

	

	ProductOperatorService.RepackingOrderRecipe($localStorage.RepackingOrderForInventory)
	.then(function(response) {
		
		if (response.data !== 'undefined') {
			$scope.processOrderRecipeList = response.data;
			//console.log($scope.processOrderRecipeList);

	
		}
	});
	
$scope.getVesselHrs =  function(processOrder){
		
		$scope.vesselHrsProcess = processOrder;
		
		ProductOperatorService.getRepackPackinVesselHrs(processOrder)
		.then(function(response) {
			
			if (response.data !== 'undefined') {
				$scope.packingVesselHrs = response.data.data;
				//console.log($scope.packingVesselHrs);

		
			}
		});
	}
		
	$scope.InventoryDone = function() {
		
		
		
		//////////////////alert/////////////
		var Alert = {
	
				         processOrderNumber:"",
						 user:{},
						 status : {
							},
				

				};
				
		        Alert.processOrderNumber = $localStorage.RepackingOrderForInventory;
		        Alert.user.ssoid = $window.sessionStorage.ssoId;
		        Alert.status.statusId = 16;
				ProductOperatorService.AlertInsert(
						Alert).then(function(response) {

					if (response != undefined) {
						$scope.AlertStatus = response.data;
					}

				}

				);
		
		
		/////////////alert close////////////

				///////////////////process tracker////////////////////
		var processTrackerTable={ 
				
				  /* "rawmaterialNumber" :"",*/
					
					"user":{
						"ssoid" : ""
					},
					
					"processorderNumber" : "",
				
						"status":{
						"statusId":""
					}
					
			};
		processTrackerTable.processorderNumber=	$localStorage.RepackingOrderForInventory ;
		processTrackerTable.user.ssoid=$window.sessionStorage.ssoId;  
		processTrackerTable.status.statusId =16;
		//console.log("Calling Tracker Update for releasing Rawmaterials ");
		
		ProductOperatorService.setTrackerStatusForRm(processTrackerTable).then(
				function(response) {
					
					if (response != undefined) {
						$scope.ProcessTrackerId=response.data;
						count++;
						//console.log($scope.ProcessTrackerId);
						if($scope.processOrderRecipeList.length == count){
							//$scope.showRelease = false;
							$scope.ProceedButton = true;
						}
						}
					
					}
				
				);
		
		
		
        ///////////////////process tracker////////////////////
		
		$scope.statusID=16;
		ProductOperatorService.StatusChangeForRepacking($localStorage.RepackingOrderForInventory,$scope.statusID,$window.sessionStorage.ssoId).then(
				function(response) {
					
					if (response != undefined) {
						$scope.packdone=response.data;
						
						}
					
					}
				
				);
		
		$state.go('RepackingInventoryCheck');
		
	};

	

	


});