App.controller("QualityController", function($state, $scope,$localStorage,
		ProductOperatorService,Flash,$window) {
	
	$scope.qulaityCheckProcessNo=$localStorage.processOrderr;
	$scope.qulaityCheckBatchNumber = $localStorage.batchNo;
	$scope.qulaityCheckBatchDate = $localStorage.batchDate;
	
	//$localStorage.upload=" ";
	//$scope.uploadSuccess="200";
	//$scope.uploadSuccess = $localStorage.upload;
	
	
	$scope.QualityRemark =  "";
	$scope.checkboxValue= "";
	ProductOperatorService.productDescription($localStorage.processOrderr).then(
			function(response) {

				if (response.data !== 'undefined') {
					$scope.processOrderDescription = response.data;
					
					

				}
			});

    

	
	
	$scope.showPackingInformation = function(){
		
		
		
		
		
		if($scope.Quality == 'notOk' || $scope.Quality == undefined )
		{
			
			////console.log("hiiiiiii");
			message = 'Quality check should pass';
    		Flash.clear(); 
		    Flash.create('warning', message);
    	   	return;
		}
		else{
	    	
		

			
			
			
			//////////////////alert/////////////
			var Alert = {
		
					         processOrderNumber:"",
							 user:{},
							 status : {
								},
					

					};
					
			        Alert.processOrderNumber = $localStorage.processOrderr;
			        Alert.user.ssoid = $window.sessionStorage.ssoId;
			        Alert.status.statusId = 18;
					ProductOperatorService.AlertInsert(
							Alert).then(function(response) {

						if (response != undefined) {
							$scope.AlertStatus = response.data;
						}

					}

					);
			
			
			/////////////alert close////////////
			
			///////////////////process tracker////////////////////
			var processTrackerTable={ 
					
					  /* "rawmaterialNumber" :"",*/
						
						"user":{
							"ssoid" : ""
						},
						
						"processorderNumber" : "",
					
							"status":{
							"statusId":""
						}
						
				};
			//processTrackerTable.rawmaterialNumber = processOrderRecipe.rawmaterialTable.rawmaterialNumber;
			processTrackerTable.processorderNumber=	$localStorage.processOrderr ;
			processTrackerTable.user.ssoid=$window.sessionStorage.ssoId;  
			processTrackerTable.reactor =$localStorage.reactor;
			processTrackerTable.status.statusId =18;
			//console.log("Calling Tracker Update for releasing Rawmaterials ");
			
			ProductOperatorService.setTrackerStatusForRm(processTrackerTable).then(
					function(response) {
						
						if (response != undefined) {
							$scope.ProcessTrackerId=response.data;
						
							}
						
						}
					
					);
 			
			//////////////////////pro////
	
			  var qualityCheckResult ={

				  		"processOrder":"",
				  		"productNumber":"",
				  		"materialMemo":"",
				  		"batchNo":"",
				  		"sso":"",
				  		"remark":"",


				  }
				//var r =$localStorage.processOrderr;
				qualityCheckResult.processOrder = $localStorage.processOrderr;
				qualityCheckResult.productNumber = ($scope.processOrderDescription[0].productTable.productNumber).toString();
				qualityCheckResult.materialMemo = $scope.processOrderDescription[0].productTable.productDescription;
				qualityCheckResult.batchNo = $scope.processOrderDescription[0].batchNumber;
				qualityCheckResult.sso  = $window.sessionStorage.ssoId;
				
				//alert($scope.checkboxValue);
				if($scope.checkboxValue == "")
				{qualityCheckResult.remark =  "Quality ok";}
				else
				{qualityCheckResult.remark =  $scope.QualityRemark;}
			
		//alert(qualityCheckResult);
		////console.log(qualityCheckResult);
		//return;
			ProductOperatorService.insertIntoQualityCheckPage(qualityCheckResult).then(
					function(response) {
						////console.log(response);
						if (response != undefined) {
					
								$scope.remarkeAdded = response.data;
							}
						}
					
					);
						
			
             //////////////////////pro////
			
			
			
            ///////////////////process tracker////////////////////
		    
			$scope.statusID=18;
			ProductOperatorService.StatusChange($localStorage.processOrderr,$scope.statusID,$window.sessionStorage.ssoId).then(
					function(response) {
						
						if (response != undefined) {
							$scope.packdone=response.data;
							
							}
						
						}
					
					);
			
           //////////////////////////////////process tracker end time///////////////////////////////
			$scope.stat=15;
			ProductOperatorService.updateEndTime($localStorage.processOrderr,$scope.stat).then(
					function(response) {
						
						if (response != undefined) {
							$scope.Updated=response.data;
							
							}
						
						}
					
					);
			///////////////////////////////  process tracker end time///////////////////////////////
			$state.go('packaging');
	}
	
	
	}
	
	$scope.inspectionLotUpload = function() {
		var file = $scope.myFile;
		var uploadUrl = "/suez/inspectionLot/qualityCheckFileUpload";
		if(file == undefined){
			//alert('Please select file')
			var message = 'Please select file to upload';
			Flash.create('danger', message);
			return;
		}
		
		ProductOperatorService.uploadInspectionLot(file, uploadUrl);
		
		
	};


});

