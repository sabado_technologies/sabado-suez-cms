App
		.controller(
				"PackingOrderDetailsController",
				function($state, $scope, ProductOperatorService, $stateParams,
						$localStorage, $window, $interval, $rootScope) {
					$rootScope.divData = 0;
				//	$rootScope.weightGlobal = 0;
					$rootScope.palletChangeArr = [];
					$scope.nitish = $localStorage.reactor;
					ProductOperatorService
							.ReactorInfo()
							.then(
									function(response) {

										if (response != undefined) {
											$scope.Reactor = response.data;
											if ($localStorage.reactor == "5 MT") {

												$scope.ThresholdTemp = $scope.Reactor[0].maxThreashold;
											} else {

												$scope.ThresholdTemp = $scope.Reactor[1].maxThreashold;
											}
										}

									}

							);

					var auto = $interval(
							function() {
								if ($scope.Reactor != undefined) {

									if ($localStorage.reactor == "5 MT") {
										$scope.TempDataValPara = 1;
										ProductOperatorService
												.TempData(
														$scope.TempDataValPara)
												.then(
														function(response) {

															if (response != undefined) {

																$scope.result = response.data;
																$scope.TempDataVal = $scope.result.tsdata;
																$scope.pack1 = "Packing";
																if ($scope.TempDataVal > $scope.Reactor[0].maxThreashold) {
																	ProductOperatorService
																			.TempAlert(
																					$localStorage.reactor,
																					$scope.TempDataVal,
																					$scope.pack1,
																					$localStorage.processOrderr,
																					$window.sessionStorage.ssoId,
																					$localStorage.Username)
																			.then(
																					function(
																							response) {

																						if (response != undefined) {
																							$scope.email = response.data;
																						}

																					}

																			);

																}
															}
														}

												);
									} else {
										$scope.result = response.data;
										$scope.TempDataValPara = $scope.result.tsdata;
										ProductOperatorService
												.TempData(
														$scope.TempDataValPara)
												.then(
														function(response) {

															if (response != undefined) {

																$scope.TempDataVal = response.data;
																$scope.pack1 = "Packing";
																if ($scope.TempDataVal > 60) {
																	ProductOperatorService
																			.TempAlert(
																					$localStorage.reactor,
																					$scope.TempDataVal,
																					$scope.pack1,
																					$localStorage.processOrderr,
																					$window.sessionStorage.ssoId,
																					$localStorage.Username)
																			.then(
																					function(
																							response) {

																						if (response != undefined) {
																							$scope.email = response.data;
																						}

																					}

																			);

																}

															}
														}

												);

									}
								}
								/*
								 * $scope.displayMsg = "This is auto Refreshed " +
								 * count + " counter time."; count++;
								 */
							}, 1000);

					$scope.WeightDataVal = '0';

					$scope.WeightSenserOne = function() {

						$localStorage.WeightSenserNo = 1;

					}

					$scope.WeightSenserTwo = function() {

						$localStorage.WeightSenserNo = 2;

					}

					$scope.WeightSenserThree = function() {

						$localStorage.WeightSenserNo = 3;

					}
				//	$scope.WeightDataVal = 0;
					var auto2 = $interval(
							function() {
								/*
								 * ProductOperatorService.WeightData($localStorage.WeightSenserNo).then(
								 * function(response) {
								 * 
								 * if (response != undefined) {
								 * 
								 * $scope.WeightDataVal = response.data; } }
								 *  );
								 */

								// alert("called");
								ProductOperatorService
										.PackingDetailsInfo(
												$localStorage.packingOrderNo)
										.then(
												function(response) {
													$scope.numberArr = [];
													$scope.packingdetails = response.data;
													$scope.weightPerPackege = $scope.packingdetails[0].wghtPckg;

													// $scope.number1=parseInt($scope.packingdetails[0].totalQuantity);

													$scope.number = parseInt($scope.packingdetails[0].totalQuantity);
													/*
													 * ('hiiiii');
													 * //(number1)
													 * ('ends here');
													 */
													var a = ("10")
													for (var i = 0; i < $scope.number; i++) {
														$scope.numberArr
																.push($scope.number);
													}

													//console
															//.log($scope.numberArr);
													//console
															//.log($scope.packingdetails);
												});
								ProductOperatorService
										.WeightData(
												$localStorage.WeightSenserNo)
										.then(
												function(response) {
													if (response != undefined) {
														// $scope.lastDivData =
														// $rootScope.divData;
													//	$scope.lastWeightData = $rootScope.weightGlobal;
														$scope.temp = response.data;
														$scope.WeightDataVal = $scope.temp.Weight;
														// pallet changes at
														// zero

														if (response.data == 0
																&& $rootScope.divData > 0) {
															$scope.WeightDataVal += (($scope.weightPerPackege) * ($rootScope.divData))
																	+ response.data;
														}

														var count = 0;
														$scope.currentDivData = parseInt(parseInt($scope.WeightDataVal)
																/ $scope.weightPerPackege);
														$rootScope.divData = parseInt(parseInt($scope.WeightDataVal)
																/ $scope.weightPerPackege);
													//	$rootScope.weightGlobal = $scope.WeightDataVal;

														var unbindWatcher = $scope
																.$watch(
																		"divData",
																		function(
																				newValue,
																				oldValue) {

																			if (newValue != oldValue) {
																				if ($scope.packingdetails[0].totalQuantity == count) {
																					//console
																							//.log('process ended 000000000000000000000');
																					unbindWatcher();
																				}
																				count++;
																			} else {
																				//console
																						//.log(newValue);
																			}
																		});
														for (var i = 1; i <= $rootScope.divData; i++) {
															$(
																	"#container"
																			+ $rootScope.divData)
																	.removeClass(
																			"text-indigo-150");
															$(
																	"#container"
																			+ $rootScope.divData)
																	.addClass(
																			"text-green-300");
														}

													}
												}

										);

							}, 1000);

					$scope.pack = function(processOrderRecipe) {
						
						//alert($scope.producedQuantity);
						//alert($scope.usedPails);
						//alert($scope.extraInfo);
						if($scope.producedQuantity == undefined || $scope.usedPails == undefined || $scope.extraInfo == undefined){
							//alert('Empty');
							var message = 'Please enter produced quantity , No. of Pails used and Remarks for extra pails';
							alertForError(message);
							return;
						}
						//return;

						var processTrackerTable = {

							"rawmaterialNumber" : "",

							"user" : {
								"ssoid" : ""
							},

							"processorderNumber" : "",

							"status" : {
								"statusId" : ""
							}

						};
						// processTrackerTable.rawmaterialTable.rawmaterialNumber
						// =
						// processOrderRecipe.rawmaterialTable.rawmaterialNumber;
						processTrackerTable.processorderNumber = processOrderRecipe;
						processTrackerTable.user.ssoid = $window.sessionStorage.ssoId;
						processTrackerTable.reactor = $localStorage.reactor;
						processTrackerTable.status.statusId = 14;
						//console
								//.log("Calling Tracker Update for releasing Rawmaterials ");

						ProductOperatorService.setTrackerStatusForRm(
								processTrackerTable).then(function(response) {

							if (response != undefined) {
								$scope.ProcessTrackerId = response.data;
								($scope.ProcessTrackerId);
							}

						}

						);

						$scope.statusID = 14;
						ProductOperatorService.StatusChangeforpacking(
								$localStorage.packingOrderNo, $scope.statusID,$window.sessionStorage.ssoId,$scope.producedQuantity, $scope.usedPails, $scope.extraInfo)
								.then(function(response) {

									if (response != undefined) {
										//alert('Success');
										$scope.packdone = response.data;
										$state.go('packaging');
									}

								}

								);

					}
					
					function alertForError(message){
						new PNotify({
					        text    : message,
					        confirm : {
					            confirm: true,
					            buttons: [
					                {
					                    text    : 'Close',
					                    addClass: 'btn btn-link',
					                    click   : function (notice)
					                    {
					                        notice.remove();
					                    }
					                },
					                null
					            ]
					        },
					        buttons : {
					            closer : false,
					            sticker: false
					        },
					        animate : {
					            animate  : true,
					            in_class : 'slideInDown',
					            out_class: 'slideOutUp'
					        },
					        addclass: 'md multiline'
					    });
						
					}
					// $scope.number=$scope.packingdetails[0].containerTable.containerQuant;
					// $scope.number = 5;
					/*
					 * if($scope.number != "undefined"){ $scope.getNumber =
					 * function(num) { return new Array(num); } }
					 */
					/*
					 * $scope.noOfcontainer=packingdetails[0].containerTable.containerQuant;
					 * 
					 * $scope.divs=new Array(); $scope.create=function(){ var
					 * a=$scope.noOfcontainer; for(var i=0;i<a;i++) {
					 * 
					 * $scope.divs.push(a); alert(a); }
					 *  };
					 */

					/*
					 * $scope.packDone = function() {
					 * 
					 * $scope.statusID=14;
					 * 
					 * ProductOperatorService.StatusChange($localStorage.processOrderr,$scope.statusID).then(
					 * function(resp onse) {
					 * 
					 * if (response != undefined) {
					 * $scope.packdone=response.data;
					 * $state.go('ProductOperator'); }
					 *  }
					 *  );
					 * 
					 * var message = 'process order has been completed
					 * successfully'; Flash.create('success', message);
					 * 
					 * 
					 *  };
					 */

				});
