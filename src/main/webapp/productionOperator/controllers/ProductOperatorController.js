App
		.controller(
				"ProductOperatorControllers",
				function($scope, ProductOperatorService, $state, $localStorage,
						$window,Flash) {
					
					$scope.vessels = [{name:"5 MT"},{name:"1 MT"}];
					$scope.shifts = [ {
						name : "General"
					}, {
						name : "1"
					}, {
						name : "2"
					} ];

					$scope.showProcess =true;
					$scope.showPackage =false;
					
					ProductOperatorService
							.processOrder()
							.then(
									function(response) {
										$scope.isPOAvaiable = true;
										if (response.data !== 'undefined') {
											$scope.processOrderList = response.data;
											$scope.responselength = $scope.processOrderList.length;
											if (response.data.length == 0) {
												$scope.isPOAviable = false;
											} 
										}
								
									});
					
			   $scope.sixMilInfo = function(processOrder){
					ProductOperatorService.sixMilInfoData(processOrder).then(
							function(response) {
								if (response.data !== 'undefined') {
									$scope.sixMillionInfoDatas = response.data.data;
									$scope.sixMillionInfoDatasLength=$scope.sixMillionInfoDatas.length;
									$scope.showProcess =false;
									$scope.showPackage =true;
								}
							});
					
					 }
					
			   $scope.refresh= function(){
				   $state.reload();
				   
				   
			   }
					/////////toggle////////
					$scope.selected=[];
				    $scope.toggleSelection = function(user){
						var idx=$scope.selected.indexOf(user);
						if(idx > -1){
							$scope.selected.splice(idx, 1);
							$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
						}
						else{
							//alert(user);
							////console.log(user);
							$scope.selected.push(user);
							$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
						}
						
						if($scope.selected.length == ''){
							$scope.selectedUsers="";
						}
					}
					
			/////////toggle end////////
					ProductOperatorService.listofuser().then(
							function(response) {

								if (response.data !== 'undefined') {
									$scope.ListOfUsers = response.data;

								}
							});

					$scope.showDetails = function(processOrder) {
						ProductOperatorService
								.processOrderRecipeList(processOrder)
								.then(
										function(response) {

											if (response.data !== undefined) {
												$scope.processOrderListDetails = response.data;
												//console.log($scope.processOrderListDetails);
												$scope.responcelength = processOrderList
														.lenght();
												$scope.processDetails = true;
											}
										});

					};
					var message; 
					
					
/////////////////////  initiate button  /////////////////////////////////
					/*var reac1;
					$('#foo').on("change",function(){
					    reac1 = $("#foo option:selected").attr('data-id');
					    alert(reac1);
					});*/
					
					
					$scope.initiateOrder = function(processOrder) {	
						//var reac1 = $("#foo option:selected").attr('data-id');
						
						//alert($scope.reactorInfo);
						//var reac = $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
						//var reac = document.getElementById("reactorInfo").value;
						//alert(reac1);
						
						//var reac= $("#reactorInfo").val();
						//var reac = $("#foo option:selected").attr('data-id');
						//var shif = $("#shiftt").val();
						//var shif = $("#sii option:selected").attr('data-id');;
						//$scope.reactorr = reac;
						//$scope.shiftt = $("shiftt").val();
						//$scope.shiftt = shif;
						//alert('***');
						//alert(reac);
						//alert(shif);
					    //alert('Reactor =>' +$scope.reactorr);
						//alert('Shift =>' + $scope.shiftt);
						
						$localStorage.reactor =$scope.reactorr;
					    $localStorage.shift = $scope.shiftt;
						$localStorage.processOrderr = processOrder.processorderProdNumber;
						$localStorage.batchNo = processOrder.batchNumber;
						$localStorage.batchDate = processOrder.batchDate;
						$localStorage.material = processOrder.productTable.productNumber;
						
						//console.log("initiate.,.....");
						//console.log(processOrder);
						
				    	/*ProductOperatorService.smsAlert($localStorage.processOrderr,$window.sessionStorage.ssoId)
						.then(function(response) {
							
							$scope.sms = response.data;
						
						
						});*/
				    	if(processOrder.isSelected ==1){
				    		//var user = {ssoid:nnnn};
				    		$localStorage.reactor =processOrder.reactor;
						    $localStorage.shift = processOrder.sift;
				    		$state.go('intiate');
				    		//$scope.reactorr = processOrder.reactor;
				    		//$scope.shiftt = processOrder.sift;
				    	}
						
						 if(processOrder.status.statusId == 18)
						{
							$localStorage.processOrderr = processOrder.processorderProdNumber;	
							$state.go('packaging');	
							
						}
						
						else if(processOrder.status.statusId == 15)
						{
							$localStorage.processOrderr = processOrder.processorderProdNumber;	
							$state.go('QualityCheck');	
							
						}
						 
					  else if(processOrder.isAssociated == '0')
						{
						    message = 'Please associated the process order and then proceed';
						    Flash.create('warning', message);
						
							
						} 
						
					/*	else if($scope.reactorr == 'undefined' && $scope.shiftt == 'undefined' && $scope.selected.length == 0  )
						{    
							 message = 'please select user and reactor';
						    Flash.create('warning', message);
							
						}*/
						 if(processOrder.isSelected !=1){
							 
							 if($scope.reactorArray.length > 1 || $scope.shiftArray.length > 1){
									var message ="Please select only one Reactor and Shift";
									alertForError(message);
								}							 
							 
							  if($scope.reactorr != undefined && $scope.shiftt != undefined && $scope.selected.length != 0  )
								{
									
							    $localStorage.reactor =$scope.reactorr;
							    $localStorage.shift = $scope.shiftt;
								$localStorage.processOrderr = processOrder.processorderProdNumber;
								$localStorage.batchNo = processOrder.batchNumber;
								$localStorage.batchDate = processOrder.batchDate;
								$localStorage.material = processOrder.productTable.productNumber;
								
								var UserRequestArray = [ ];
						    	for (var i = 0; i < $scope.selected.length; i++) {
						    		
						    	
						    		$scope.processorderProd={
						    				processorderProdNumber:"",
						    		 };
						              
						    		$scope.user={
						    				ssoid:"",
						    		 };
						              
						    		
						    		
						    		$scope.UserRequest={
						    				processorderProd:{},
						    				user:{}
						    			
						    				
						      		};
						              

						    		
						    		$scope.UserRequest.user.ssoid =$scope.selected[i].ssoid;
						    		$scope.UserRequest.processorderProd.processorderProdNumber =$localStorage.processOrderr;
						    		
						    		
						    		
						    		
						    		UserRequestArray.push($scope.UserRequest);
						    	}				    	
						    	
						    	ProductOperatorService.insertUserinpoAssosiatedwithporder(UserRequestArray)
								.then(function(response) {
									$scope.assosiatedpowithprocess = response.data;
								});
						    	
						    	
						    	

								var processTrackerTable = {
					
										rawmaterialNumber:"",
										processorderNumber:"",
										 user:{},
										 status : {
											},
								

								};
								processTrackerTable.processorderNumber = processOrder.processorderProdNumber;
								processTrackerTable.user.ssoid = $window.sessionStorage.ssoId;
								processTrackerTable.reactor = $localStorage.reactor;
								processTrackerTable.sift = $localStorage.shift;
								processTrackerTable.status.statusId = 1;
							
								ProductOperatorService.setTrackerStatusForRm(
										processTrackerTable).then(function(response) {

									if (response != undefined) {
										$scope.ProcessTrackerId = response.data;
										//console.log($scope.ProcessTrackerId);
									}

								}

								);

								//////////////////alert/////////////
								var Alert = {
							
										         processOrderNumber:"",
												 user:{},
												 status : {
													},
										

										};
										
								        Alert.processOrderNumber = processOrder.processorderProdNumber;
								        Alert.user.ssoid = $window.sessionStorage.ssoId;
								        Alert.status.statusId = 1;
										ProductOperatorService.AlertInsert(
												Alert).then(function(response) {

											if (response != undefined) {
												$scope.AlertStatus = response.data;
											}

										}

										);
								
								
								/////////////alert close////////////
								
								
								
								
								
							/*	
								ProductOperatorService.setTrackerStatusForRm(
										processTrackerTable).then(function(response) {

									if (response != undefined) {
										$scope.ProcessTrackerId = response.data;
										//console.log($scope.ProcessTrackerId);
									}

								}

								);*/
								
								ProductOperatorService.UpdateinProcessOrderProd($localStorage.processOrderr,$localStorage.reactor,$localStorage.shift).then(
										function(response) {

											if (response.data !== 'undefined') {
												$scope.updatedprocessOrder = response.data;

											}
										});
								
								
								$state.go('intiate');

							}
								 
								else{    
									 message = 'please select user and reactor and shift';
									 alertForError(message);
									 //Flash.clear();  
									 //Flash.create('warning', message);
									 return;
										
									}
							 
							 
						 }
						
						
					
					}

					$scope.showDropDown = function() {

						$scope.drop = true;

					}

					$scope.reactor1 = function() {
                      
						$scope.reactorr = "1 MT";

					}

					$scope.reactor5 = function() {

						$scope.reactorr = "5 MT";
						//alert($scope.reactorr);

					}
				
					
					$scope.shift1 = function() {

						$scope.shiftt = '1';

					}
					
					$scope.shift2 = function() {

						$scope.shiftt = '2';

					}
					
					$scope.general = function() {

						$scope.shiftt = '3';

					}
					
					$scope.reactorArray=[];
				    $scope.toggleReactor = function(reactor){
						var idx=$scope.reactorArray.indexOf(reactor);
						if(idx > -1){
							$scope.reactorArray.splice(idx, 1);
						}
						else{
							$scope.reactorArray.push(reactor);
						}
						if($scope.reactorArray.length > 1){
							var message ="Please select only one Reactor";
							alertForError(message);
						}else{
							$scope.reactorr = $scope.reactorArray[0].name;
						}
						//alert("From Array => " + $scope.reactorArray[0].name);
						//alert("Length => " + $scope.reactorArray.length);
						//alert("Final =>" + $scope.reactorr);
						
					}
				    
				    $scope.shiftArray=[];
				    $scope.toggleShift = function(shift){
						var idx=$scope.shiftArray.indexOf(shift);
						if(idx > -1){
							$scope.shiftArray.splice(idx, 1);
						}
						else{
							$scope.shiftArray.push(shift);
						}
						if($scope.shiftArray.length > 1){
							var message ="Please select only one Shift";
							alertForError(message);
						}else{
							$scope.shiftt = $scope.shiftArray[0].name;
						}
						if($scope.shiftt === 'General'){
							 $scope.shiftt = '3';
						 }
						//alert("From Array => " + $scope.reactorArray[0].name);
						//alert("Length => " + $scope.reactorArray.length);
						//alert("Final =>" + $scope.shiftt);
						
					}
				    
				    function alertForError(message){
				    	new PNotify({
				            text    : message,
				            confirm : {
				                confirm: true,
				                buttons: [
				                    {
				                        text    : 'Close',
				                        addClass: 'btn btn-link',
				                        click   : function (notice)
				                        {
				                            notice.remove();
				                        }
				                    },
				                    null
				                ]
				            },
				            buttons : {
				                closer : false,
				                sticker: false
				            },
				            animate : {
				                animate  : true,
				                in_class : 'slideInDown',
				                out_class: 'slideOutUp'
				            },
				            addclass: 'md multiline'
				        });
				    	
				    }
					
				

				})