App.controller("QualityRepackingController", function($state, $scope,$localStorage,
		ProductOperatorService,Flash,$window) {
	
	$scope.qulaityCheckProcessNo=$localStorage.processOrderr;
	$scope.qulaityCheckBatchNumber = $localStorage.batchNo;
	$scope.qulaityCheckBatchDate = $localStorage.batchDate;
	
	$scope.QualityRemark =  "";
	
	$scope.goBack = function(){
		$state.go('repacking');
	}
	ProductOperatorService.RepackingProductDescription($localStorage.processorderPckgNumberForRePacking).then(
			function(response) {

				if (response.data !== 'undefined') {
					$scope.processOrderDescription = response.data;
					
					//console.log($scope.processOrderDescription );

				}
			});

    

	
	
	$scope.showPackingInformation = function(){
		
		
		
		
		
		if($scope.Quality == 'notOk' || $scope.Quality == undefined )
		{
			
			////console.log("hiiiiiii");
			message = 'Quality check should pass';
    		Flash.clear(); 
		    Flash.create('warning', message);
    	   	return;
		}
		else{
	    	
		

			
			
			
			//////////////////alert/////////////
			var Alert = {
		
					         processOrderNumber:"",
							 user:{},
							 status : {
								},
					

					};
					
			        Alert.processOrderNumber = $localStorage.processorderPckgNumberForRePacking;;
			        Alert.user.ssoid = $window.sessionStorage.ssoId;
			        Alert.status.statusId = 18;
					ProductOperatorService.AlertInsert(
							Alert).then(function(response) {

						if (response != undefined) {
							$scope.AlertStatus = response.data;
						}

					}

					);
			
			
			/////////////alert close////////////
			
	////////////////// process tracker//////////////////
					var processTrackerTable = {
				
				
						processorderNumber : "",
						user : {},
						status : {},
						isRepack:""

					};

					processTrackerTable.processorderNumber = $localStorage.processorderPckgNumberForRePacking;
					processTrackerTable.user.ssoid = $window.sessionStorage.ssoId;
					processTrackerTable.reactor = $localStorage.RepackingReactor;
					processTrackerTable.sift = $localStorage.RepackingShift;
					processTrackerTable.status.statusId =18 ;
					processTrackerTable.isRepack = 1;
					//console
							//.log("Calling Tracker Update for releasing Rawmaterials ");

					ProductOperatorService
							.setTrackerStatusForRm(processTrackerTable)
							.then(
									function(response) {

										if (response != undefined) {
											$scope.ProcessTrackerId = response.data;
											//console
													//.log($scope.ProcessTrackerId);
										}

									}

							);

					
	              ////////////////// process tracker end//////////////////
			  var qualityCheckResult ={

				  		"processOrder":"",
				  		"productNumber":"",
				  		"materialMemo":"",
				  		"batchNo":"",
				  		"sso":"",
				  		"remark":"",


				  }
				//var r =$localStorage.processOrderr;
				qualityCheckResult.processOrder = $localStorage.processorderPckgNumberForRePacking;
				qualityCheckResult.productNumber = ($scope.processOrderDescription[0].productNumber).toString();
				qualityCheckResult.materialMemo = $scope.processOrderDescription[0].materialMemo;
				qualityCheckResult.batchNo = $scope.processOrderDescription[0].batchNumber;
				qualityCheckResult.sso  = $window.sessionStorage.ssoId;
				qualityCheckResult.remark =  $scope.QualityRemark;
			
		
			ProductOperatorService.insertIntoQualityCheckPage(qualityCheckResult).then(
					function(response) {
						//console.log(response);
						if (response != undefined) {
					
								$scope.remarkeAdded = response.data;
							}
						}
					
					);
						
			
    
			
			
			
            ///////////////////process tracker////////////////////
		    
			////////////////// packing order status change//////////////////	
			$scope.statusID=18;
			var packQuantity = '0';
			var pailsUsed = 0;
			var packRemarks = 'Empty';
			ProductOperatorService.StatusChangeforpacking($localStorage.processorderPckgNumberForRePacking,$scope.statusID,$window.sessionStorage.ssoId,packQuantity,pailsUsed,packRemarks).then(
					function(response) {
						
						if (response != undefined) {
							$scope.packdone=response.data;
							
							ProductOperatorService.insertPackingTime($localStorage.processorderPckgNumberForRePacking).then(
									function(response) {
										
										if (response != undefined) {
											//$scope.packdone=response.data;
											//$state.go('RepackingDetails');
											}
										
										}
									
									);
							$state.go('RepackingDetails');
							}
						
						}
					
					);
	    //////////////////packing order status change end//////////////////
			

			
	}
	
	
	}
	
	$scope.inspectionLotUpload = function() {
		var file = $scope.myFile;
		var uploadUrl = "/suez/inspectionLot/qualityCheckFileUpload";
		if(file == undefined){
			//alert('Please select file')
			var message = 'Please select file to upload';
			Flash.create('danger', message);
			return;
		}
		
		ProductOperatorService.uploadInspectionLot(file, uploadUrl);
		
		
	};
	


});

