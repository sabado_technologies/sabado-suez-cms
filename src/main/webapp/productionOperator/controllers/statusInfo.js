App.controller("StatusInfoController", function($scope, $state, Flash,
		ProductOperatorService, $location, $interval) {	
	
	var auto = $interval(function() {
		ProductOperatorService.getAllStatusInfo().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.statusDetails = response.data.data;
				//$state.reload();
			}

		});
      }, 1000);
	
	$scope.statusReport = function () {
		//alert();
		 alasql('SELECT [processorderProdNumber] AS [Process Order],[materialMemo] AS [Material Memo],[batchDate] AS [Batch Date],[uploadedDate] AS [Uploaded Date],[uploadedTime] AS [Uploaded Time],[rmIssuedDate] AS [RM issued date],[rmIssuedTime] AS [RM issued time],[rmIssuedBy] AS [RM issued by],[rmChargedDate] AS [RM charge Date],[rmChargedTime] AS [RM charge time],[rmChargedBy] AS [RM charged by],[qualityCheckDate] AS [Quality check date],[qualityCheckTime] AS [Quality check time],[qualityCheckedBy] AS [Quality checked by],[packedDate] AS [Packed date],[packedTime] AS [Packed time],[packedBy] AS [Packed by],[rmPostDate] AS [RM posted date],[rmPostTime] AS [RM posted time],[rmPostedBy] AS [RM posted by],[qualityInspectionDate] AS [Quality inspection date],[qulityInspectionTime] AS [Quality inspection time],[qualityInspectedBy] AS [Quality inspection by],[inventoryDate] AS [Inventory check date],[inventoryTime] AS [Inventory check time],[inventoryBy] AS [Inventory check by] INTO XLSX("Process Order Status Report.xlsx",{headers:true}) FROM ?',[$scope.statusDetails]);
    };
	

});