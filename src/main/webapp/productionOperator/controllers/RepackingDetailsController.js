App.controller("RepackingDetailsController", function($state, $scope,
		ProductOperatorService, $stateParams, $localStorage,$window,$interval,$rootScope) {

	ProductOperatorService.PackingDetailsInfo($localStorage.processorderPckgNumberForRePacking)
	.then(function(response) {
		
		$scope.packingdetails = response.data;
	    //console.log($scope.packingdetails);
		
	
	});

	

	

	
	$scope.Repack = function() {
		
		//alert($scope.producedQuantity);
		//alert($scope.usedPails);
		//alert($scope.extraInfo);
		if($scope.producedQuantity == undefined || $scope.usedPails == undefined || $scope.extraInfo == undefined){
			//alert('Empty');
			var message = 'Please enter produced quantity , No. of Pails used and Remarks for extra pails';
			alertForError(message);
			return;
		}

		////////////////// process tracker//////////////////
		var processTrackerTable = {
	
	
			processorderNumber : "",
			user : {},
			status : {},
			isRepack:""

		};

		processTrackerTable.processorderNumber = $localStorage.processorderPckgNumberForRePacking;
		processTrackerTable.user.ssoid = $window.sessionStorage.ssoId;
		processTrackerTable.reactor = $localStorage.RepackingReactor;
		processTrackerTable.sift = $localStorage.RepackingShift;
		processTrackerTable.status.statusId =14;
		processTrackerTable.isRepack = 1;
		//console
				//.log("Calling Tracker Update for releasing Rawmaterials ");

		ProductOperatorService
				.setTrackerStatusForRm(processTrackerTable)
				.then(
						function(response) {

							if (response != undefined) {
								$scope.ProcessTrackerId = response.data;
								//console
										//.log($scope.ProcessTrackerId);
							}

						}

				);

		
      ////////////////// process tracker end//////////////////
	
		
	////////////////// packing order status change//////////////////	
		$scope.statusID=14;
		ProductOperatorService.StatusChangeforpacking($localStorage.processorderPckgNumberForRePacking,$scope.statusID,$window.sessionStorage.ssoId,$scope.producedQuantity, $scope.usedPails, $scope.extraInfo).then(
				function(response) {
					
					if (response != undefined) {
						$scope.packdone=response.data;
						$state.go('repacking');
						}
					
					}
				
				);
    //////////////////packing order status change end//////////////////
		
	
		

	}
	
	function alertForError(message){
		new PNotify({
	        text    : message,
	        confirm : {
	            confirm: true,
	            buttons: [
	                {
	                    text    : 'Close',
	                    addClass: 'btn btn-link',
	                    click   : function (notice)
	                    {
	                        notice.remove();
	                    }
	                },
	                null
	            ]
	        },
	        buttons : {
	            closer : false,
	            sticker: false
	        },
	        animate : {
	            animate  : true,
	            in_class : 'slideInDown',
	            out_class: 'slideOutUp'
	        },
	        addclass: 'md multiline'
	    });
		
	}

	

});

