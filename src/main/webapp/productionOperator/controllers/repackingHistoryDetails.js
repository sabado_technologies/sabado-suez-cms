App.controller("RepackingHistoryDetailsController", function($scope, $state,
		$localStorage, $window, ProductOperatorService) {
	
	$scope.processorderPckgNumber = $window.sessionStorage.processorderRepackHistoryPckgNumber;
	
	ProductOperatorService.getRepackProcessOrderForHistory($scope.processorderPckgNumber).then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data;
					
				}

			})
			
$scope.getVesselHrs =  function(){
		
		$scope.vesselHrsProcess = $scope.processorderPckgNumber;
		
		ProductOperatorService.getRepackPackinVesselHrs($scope.processorderPckgNumber)
		.then(function(response) {
			
			if (response.data !== 'undefined') {
				$scope.packingVesselHrs = response.data.data;
				//console.log($scope.packingVesselHrs);

		
			}
		});
	}
	
			
});