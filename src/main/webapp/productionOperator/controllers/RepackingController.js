App
		.controller(
				"repackingController",
				function($state, $scope, $localStorage, ProductOperatorService,Flash,$window) {
					
					$scope.vessels = [{name:"5 MT"},{name:"1 MT"}];
					$scope.shifts = [ {
						name : "General"
					}, {
						name : "1"
					}, {
						name : "2"
					} ];

					ProductOperatorService.RepackingData().then(
							function(response) {
								//console.log(response);
								if (response != undefined) {
									//console.log(response.data);
									$scope.processOrderList = response.data;
									$scope.responselength = $scope.processOrderList.length;
								}
							}

					);
					
					
					
					ProductOperatorService.listofuser().then(
							function(response) {

								if (response.data !== 'undefined') {
									$scope.ListOfUsers = response.data;

								}
							});

					
					
					
			/////////toggle////////
					
					$scope.selected=[];
					
					
				    $scope.toggleSelection = function(user){
						
						var idx=$scope.selected.indexOf(user);
						if(idx > -1){
							$scope.selected.splice(idx, 1);
							$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
							
						}
						else{
							$scope.selected.push(user);
							$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
						}
						
						if($scope.selected.length == ''){
							$scope.selectedUsers="";
						}
					}
					
			/////////toggle end////////
				    
				    
				    
					$scope.initiateOrder = function(processOrder) {
						//$scope.Repackreactor = $("#reactor").val();
						//$scope.Repackshift = $("#shiftt").val();
						//alert('Reactor =>' +$scope.Repackreactor);
						//alert('Shift =>' + $scope.Repackshift);
						//alert(processOrder.status.statusId);
						//var reac= $("#reactorInfo").val();
						//var shif = $("#shiftt").val();
						//alert(reac);
						//alert(shif);
						//$scope.Repackreactor = reac;
						//$scope.shiftt = $("shiftt").val();
						//$scope.Repackshift = shif;
						//alert('Reactor =>' +$scope.Repackreactor);
						//alert('Shift =>' + $scope.Repackshift);
						$localStorage.RepackingReactor =processOrder.reactor;
					    $localStorage.RepackingShift = processOrder.shift;
						
						$localStorage.processOrderr = processOrder.processorderPckgNumber;
						$localStorage.batchNo = processOrder.batchNumber;
						$localStorage.batchDate = processOrder.batchDate;
						$localStorage.materialForRePacking = processOrder.productNumber;
						$localStorage.processorderPckgNumberForRePacking = processOrder.processorderPckgNumber;
						//$localStorage.material = processOrder.productTable.productNumber;
						
						ProductOperatorService.smsAlert($localStorage.processOrderr,$window.sessionStorage.ssoId)
						.then(function(response) {
							
							$scope.assosiatedpowithprocess = response.data;
						
						
						});
						
						 if(processOrder.status.statusId == 18)
							{
							// alert();
								$localStorage.processOrderr = processOrder.processorderPckgNumber;
								$localStorage.processorderPckgNumberForRePacking = processOrder.processorderPckgNumber;
								$localStorage.RepackingReactor =processOrder.reactor;
							    $localStorage.RepackingShift = processOrder.shift;
								$state.go('RepackingDetails');	
								
							}
						 else if(processOrder.status.statusId == 8 && processOrder.isSelected ==1){
							    $localStorage.processOrderr = processOrder.processorderPckgNumber;
								$localStorage.processorderPckgNumberForRePacking = processOrder.processorderPckgNumber;
								$localStorage.RepackingReactor =processOrder.reactor;
							    $localStorage.RepackingShift = processOrder.shift;
								$state.go('RepackingQualityCheck');
						 }
						 
						 else{
							 
							 if($scope.reactorArray.length > 1 || $scope.shiftArray.length > 1){
									var message ="Please select only one Reactor and Shift";
									alertForError(message);
								}
							 
							 if ($scope.Repackreactor != undefined
										&& $scope.Repackshift != undefined
										&& $scope.selected.length != 0) {

									$localStorage.RepackingReactor = $scope.Repackreactor;
									$localStorage.RepackingShift = $scope.Repackshift;
									$localStorage.processorderPckgNumberForRePacking = processOrder.processorderPckgNumber;
									$localStorage.materialForRePacking = processOrder.productNumber;

									var UserRequestArray = [];
									// var len = oFullResponse.results.length;
									for (var i = 0; i < $scope.selected.length; i++) {

										$scope.processorderPckg = {
												processorderPckgNumber : "",
										};

										$scope.user = {
											ssoid : "",
										};

										$scope.UserRequest = {
											processorderProd : {},
											user : {}

										};

										$scope.UserRequest.user.ssoid = $scope.selected[i].ssoid;
										$scope.UserRequest.processorderProd.processorderProdNumber = $localStorage.processorderPckgNumberForRePacking;

										UserRequestArray.push($scope.UserRequest);
									}

									
									
									
									
									ProductOperatorService
											.SaveprocessAssociatedwithpoForRepack(
													UserRequestArray)
											.then(
													function(response) {
														$scope.assosiatedpowithprocess = response.data;
											});

									
									////////////////// process tracker//////////////////
									var processTrackerTable = {
								
								
										processorderNumber : "",
										user : {},
										status : {},
										isRepack:""

									};
			
									processTrackerTable.processorderNumber = processOrder.processorderPckgNumber;
									processTrackerTable.user.ssoid = $window.sessionStorage.ssoId;
									processTrackerTable.reactor = $localStorage.RepackingReactor;
									processTrackerTable.sift = $localStorage.RepackingShift;
									processTrackerTable.status.statusId =1 ;
									processTrackerTable.isRepack = 1;
									//console
											//.log("Calling Tracker Update for releasing Rawmaterials ");

									ProductOperatorService
											.setTrackerStatusForRm(processTrackerTable)
											.then(
													function(response) {

														if (response != undefined) {
															$scope.ProcessTrackerId = response.data;
															//console
																	//.log($scope.ProcessTrackerId);
														}

													}

											);
									$state.go('RepackingQualityCheck');
								}

								else {
									message = 'please select user and reactor and shift';
									Flash.clear();
									Flash.create('warning', message);

								}
							 
						 }

						
				    					    	
				    	
						
						 

					}
					
					
					
					
					
					$scope.reactor1 = function() {
	                      
						//$localStorage.reactor = "1 MT";
						$scope.Repackreactor = "1 MT";

					}

					$scope.reactor5 = function() {

						$scope.Repackreactor = "5 MT";

					}
					
					$scope.pack = function() {

						$scope.Repackreactor = "Pack";

					}
				
					
					$scope.shift1 = function() {

						$scope.Repackshift = '1';

					}
					
					$scope.shift2 = function() {

						$scope.Repackshift = '2';

					}
					
					$scope.general = function() {

						$scope.Repackshift = 'General';

					}
					
					$scope.reactorArray=[];
				    $scope.toggleReactor = function(reactor){
						var idx=$scope.reactorArray.indexOf(reactor);
						if(idx > -1){
							$scope.reactorArray.splice(idx, 1);
						}
						else{
							$scope.reactorArray.push(reactor);
						}
						if($scope.reactorArray.length > 1){
							var message ="Please select only one Reactor";
							alertForError(message);
						}else{
							$scope.Repackreactor = $scope.reactorArray[0].name;
						}
						//alert("From Array => " + $scope.reactorArray[0].name);
						//alert("Length => " + $scope.reactorArray.length);
						//alert("Final =>" + $scope.reactorr);
						
					}
				    
				    $scope.shiftArray=[];
				    $scope.toggleShift = function(shift){
						var idx=$scope.shiftArray.indexOf(shift);
						if(idx > -1){
							$scope.shiftArray.splice(idx, 1);
						}
						else{
							$scope.shiftArray.push(shift);
						}
						if($scope.shiftArray.length > 1){
							var message ="Please select only one Shift";
							alertForError(message);
						}else{
							$scope.Repackshift = $scope.shiftArray[0].name;
						}
						if($scope.Repackshift === 'General'){
							 $scope.Repackshift = '3';
						 }
						//alert("From Array => " + $scope.reactorArray[0].name);
						//alert("Length => " + $scope.reactorArray.length);
						//alert("Final =>" + $scope.shiftt);
						
					}
				    
				    function alertForError(message){
				    	new PNotify({
				            text    : message,
				            confirm : {
				                confirm: true,
				                buttons: [
				                    {
				                        text    : 'Close',
				                        addClass: 'btn btn-link',
				                        click   : function (notice)
				                        {
				                            notice.remove();
				                        }
				                    },
				                    null
				                ]
				            },
				            buttons : {
				                closer : false,
				                sticker: false
				            },
				            animate : {
				                animate  : true,
				                in_class : 'slideInDown',
				                out_class: 'slideOutUp'
				            },
				            addclass: 'md multiline'
				        });
				    	
				    }
					

				});
