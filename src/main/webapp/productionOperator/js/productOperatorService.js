App.factory('ProductOperatorService', function($http,Flash,$q) {

	return {
		processOrder : function() {
			return $http.get('/suez/po/process', {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		
		getPackinVesselHrs : function(process) {
			return $http.get('/suez/Management/cycleTimeForModal/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		
		getRepackPackinVesselHrs : function(process) {
			return $http.get('/suez/pending/repackingHours/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},


		processOrderRecipe : function(p) {
			return $http.post('/suez/po/processDetails/' + p, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},

		associatedPackingInfo : function(p) {
			return $http.post('/suez/po/associatedPackingInfo/' + p, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},

		TempData : function(TempSenserNo) {
			return $http.get('/suez/sensor/tempdata/'+ TempSenserNo, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		
		WeightData : function(WeightSenserNo) {
			return $http.get('/suez/sensor/weight/'+ WeightSenserNo, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		
		WeightDataTest : function(WeightSenserNo,old) {
			return $http.get('/suez/sensor/weight/'+ WeightSenserNo+'/'+old, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},


		statusUpdate : function(processOrder) {
			return $http.put('/suez/po/updateStatus/' + processOrder, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},

		listofuser : function() {
			return $http.get('/suez/po/listofpo', {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		StatusChange : function(processOrderId,status,sso) {
			return $http.put('/suez/po/updateStatus/'+processOrderId+'/'+status +'/'+sso, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});

		},
		
		
		
		
		
		
		setTrackerStatusForRm : function(processTrackerTable) {
			return $http.post(
					'/suez/po/updateProcessStatus',
					JSON.stringify(processTrackerTable),
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},
		
		PackingDetailsInfo : function(packingOrder) {
			return $http.post('/suez/po/PackingOrderInfo/' + packingOrder, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		getAllDailyProcessOrderDetails: function(){
			return $http.get('/suez/po/dailyProcessOrders',{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				return $q.reject(response);
			});
		},
		getAllWeeklyOrderDetails: function(){
				return $http.get('/suez/po/weeklyProcessOrders',{
					'Content-Type':'application/json'
				}).then(function(response){
					return response;
				},function(response){
					return $q.reject(response);
				});
						
			},
			
			insertUserinpoAssosiatedwithporder: function(selected){
			return $http.post('/suez/po/SavePoProcessOrderDetails',
					JSON.stringify(selected), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
			},
			StatusChangeforpacking : function(packingOrderId,status,sso,quantity,pails,remarks) {
				//alert('Service called');
				return $http.put('/suez/po/updateStatusForPacking/'+packingOrderId+'/'+status+'/'+sso+'/'+quantity+'/'+pails+'/'+remarks, {
					'Content-Type' : 'application/json'
				}).then(function(response) {
					return response;
				}, function(response) {
					//alert('FAiled');
					return $q.reject(response);
				});

			},

			StatusChangeForRm: function(packingOrderId,rmNumber,status) {
				return $http.put('/suez/po/updateStatusForRM/'+packingOrderId+'/'+rmNumber+'/'+status, {
					'Content-Type' : 'application/json'
				}).then(function(response) {
					return response;
				}, function(response) {
					return $q.reject(response);
				});

			},
			QualityData : function(QualityData) {
				return $http.post('/suez/po/getQualityMaster/' + QualityData, {
					'Content-Type' : 'application/json'
				}).then(function(response) {
					return response;
				}, function(response) {
					return $q.reject(response);
				});
			}, 
			InsertQualityData : function(QualityDataList){
				return $http.post('/suez/po/SaveQualityCheckData',
						JSON.stringify(QualityDataList), {
							'Content-Type' : 'application/json'
						}).then(function(response) {
					return response;

				}, function(response) {
					// something went wrong
					return $q.reject(response);
				});
				},
				UpdateinProcessOrderProd: function(packingOrderId,reactor,shift) {
					return $http.put('/suez/po/updateProcessOrderProd/'+packingOrderId+'/'+reactor+'/'+shift, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});

				},
				processOrderforInventory : function() {
					return $http.get('/suez/po/processOrderForInventory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				dailyProcessOrder : function() {
					return $http.get('/suez/po/dailyProcessOrder', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				AlertInsert :  function(Alert) {
					return $http.post(
							'/suez/po/SaveAlert',
							JSON.stringify(Alert),
							{
								'Content-Type' : 'application/json'
							}).then(function(response) {
						return response;

					}, function(response) {
						// something went wrong
						return response;
					});
				},
				
				TempAlert :  function(reactor,tempdata,from,porder,sso,username) {
					return $http.post(
							'/suez/user/tempDeviation/'+reactor+'/'+tempdata+'/'+from+'/'+porder+'/'+sso+'/'+username,
							JSON.stringify(),
							{
								'Content-Type' : 'application/json'
							}).then(function(response) {
						return response;

					}, function(response) {
						return response;
					});
				},
				
				ReactorInfo :  function() {
					return $http.get('/suez/po/ReactorInfo', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				addRemark :function(remark) {
					return $http.post(
							'/suez/po/insertRemark',
							JSON.stringify(remark),
							{
								'Content-Type' : 'application/json'
							}).then(function(response) {
						return response;

					}, function(response) {
						return response;
					});
				}, 
			
				
				updateEndTime : function(processOrder,status) {
					return $http.put('/suez/po/updateEndTime/' + processOrder+'/'+status, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				RepackingData : function() {
					return $http.get('/suez/po/reapckingOrder', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				SaveprocessAssociatedwithpoForRepack : function(selected){
					return $http.post('/suez/po/SaveprocessAssociatedwithpoForRepack',
							JSON.stringify(selected), {
								'Content-Type' : 'application/json'
							}).then(function(response) {
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
					},
			
					productDescription: function(p) {
						return $http.post('/suez/po/processOrderDetailsForQuality/' + p, {
							'Content-Type' : 'application/json'
						}).then(function(response) {
							return response;
						}, function(response) {
							return $q.reject(response);
						});
					},
					
					insertIntoQualityCheckPage : function(selected){
						return $http.post('/suez/po/insertQualityResult',
								JSON.stringify(selected), {
									'Content-Type' : 'application/json'
								}).then(function(response) {
							return response;

						}, function(response) {
							// something went wrong
							return $q.reject(response);
						});
						},
				ReapckingOrderforInventory	: function() {
					return $http.get('/suez/po/RepackingprocessOrderForInventory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				RepackingOrderRecipe : function(p) {
					return $http.post('/suez/po/RepackingprocessDetails/' + p, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				StatusChangeForRepacking : function(processOrderId,status,sso) {
					return $http.put('/suez/po/RepackingOrderStatusChange/'+processOrderId+'/'+status+'/'+sso, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});

				},
				
				sixMilInfoData  : function(p) {
					return $http.get('/suez/packingInfo/getAllPackingInfo/' + p, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				RepackingProductDescription : function(p) {
					return $http.post('/suez/po/RepackingQualityProcessDetails/' + p, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				uploadInspectionLot : function(file, uploadUrl) {
					var fd = new FormData();
					fd.append('file', file);
					
					$http.post(uploadUrl, fd, {
						transformRequest : angular.identity,
						headers : {
							'Content-Type' : undefined
						}
					}).then(function(success) {
						//console.log(success);
						var status = success.data.code;
						if (status == "200") {
							var message = 'Inspection Lot Re-uploaded for batch number '+'<strong>' +success.data.data+' </strong>';
							Flash.create('success', message);
							
						} else if(status == "400"){
							var message = 'Please try again !';
							Flash.create('danger', message);
						}
					}).then(function(error) {
					});
				},
				
				
				smsAlert : function (processorderProdNumber ,ssoId){
					return $http.get(
							'/suez/RoleBasedAlerts/smsAlertForCharging/'+processorderProdNumber+"/ "+ssoId,
							 {
								'Content-Type' : 'application/json'
							}).then(function(response) {
								return response;
							}, function(response) {
								//alert('error');
							});
				},/*,
				
				oneMTtempData : function() {
					return $http.jsonp('http://10.185.45.29:8080/tsdata.py', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						//alert('data');
						//alert(response);
						return response;
					}, function(response) {						
						//alert('failed');
						//alert(response);
						return $q.reject(response);
					});
				},*/
				/*oneMTtempData : function() {
					return $http.jsonp('http://125.99.157.180/suez/dispweigh.php', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						//alert('data');
						//alert(response);
						return response;
					}, function(response) {						
						alert('failed');
						alert(response);
						return $q.reject(response);
					});
				},*/
				oneMTtempData : function() {
					return $http.jsonp('http://125.99.157.180/suez/dispweigh.php', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						//alert('data');
						//alert(response);
						return response;
					}, function(response) {						
						//alert('failed');
						//alert(response);
						return $q.reject(response);
					});
				},
				getAllStatusInfo : function() {
					return $http.get('/suez/pending/getAllStatusInfo').then(
							function(response) {

								return response;

							}, function(response) {
								// something went wrong
								return $q.reject(response);
							});
				},
				
				insertPackingTime : function(p) {
					return $http.post('/suez/po/insertPackingStartTime/' + p, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						return $q.reject(response);
					});
				},
				
				getAllRepackingStatusInfo : function() {
					return $http.get('/suez/pending/getAllRepackingStatusInfo').then(
							function(response) {

								return response;

							}, function(response) {
								// something went wrong
								return $q.reject(response);
							});
				},
				getProcessHistory : function() {
					return $http.get(
							'/suez/pending/getProcessOrderHistory', {
								'Content-Type' : 'application/json'
							}).then(function(response) {
								return response;
							}, function(response) {
							});
				},		
				getProcessOrderForHistory : function(processorderProdNumber) {
					return $http.get(
							'/suez/WareHouseController/getProcessOrdersForPostingCheck/'+processorderProdNumber, {
								'Content-Type' : 'application/json'
							}).then(function(response) {
								return response;
							}, function(response) {
								//alert('error');
							});
				} ,
				associatedPackingHistory : function(processorderProdNumber) {
					return $http.get(
							'/suez/WareHouseController/associatedPackingInfoForPosting/'+processorderProdNumber, {
								'Content-Type' : 'application/json'
							}).then(function(response) {
								return response;
							}, function(response) {
								//alert('error');
							});
				},
				getRepackProcessHistory : function() {
					return $http.get(
							'/suez/pending/getRepackingProcessOrderHistory', {
								'Content-Type' : 'application/json'
							}).then(function(response) {
								return response;
							}, function(response) {
							});
				},
				getRepackProcessOrderForHistory : function(processorderProdNumber) {
					return $http.get(
							'/suez/WareHouseController/getPackingOrderForInventoryPosting/'+processorderProdNumber, {
								'Content-Type' : 'application/json'
							}).then(function(response) {
								return response;
							}, function(response) {
								//alert('error');
							});
				},
				updateExtraRm : function(process,rmInfo) {
					return $http.post('/suez/pending/addExtraRawMaterial/'+ process+'/'+rmInfo, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//console.log('error')
						//alert('error');
					});
				}
	}

});
