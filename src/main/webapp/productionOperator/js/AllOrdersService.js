App.factory('AllOrdersService', function($http, $q) {

	return {

		getAllOrders : function() {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get('/suez/poHome/getAllOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getReactors : function() {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get('/suez/pending/getReactors').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysOrders : function() {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get('/suez/poHome/getTodaysOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getPastOrders : function() {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get('/suez/poHome/getPreviousOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getFutureOrders : function() {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get('/suez/poHome/getFutureOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysCompletedOrders : function() {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get('/suez/poHome/getTodaysCompletedOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getStatus : function(processorderProdNumber) {
			//alert('service');
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get(
					'/suez/pending/getStatus/' + processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getChargedStatus : function(processorderProdNumber) {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get(
					'/suez/pending/getChargedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getPackedStatus : function(processorderProdNumber) {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get(
					'/suez/pending/getPackedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityCheckStatus : function(processorderProdNumber) {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get(
					'/suez/pending/getQualityCheckStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityInspectionStatus : function(processorderProdNumber) {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get(
					'/suez/pending/getQualityInspectionStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getInventoryStatus : function(processorderProdNumber) {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get(
					'/suez/pending/getInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
		
		getRmInventoryStatus : function(processorderProdNumber) {
			// the $http API is based on the deferred/promise APIs exposed by
			// the $q service
			// so it returns a promise for us by default
			return $http.get(
					'/suez/pending/getRmInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		}

	}

});