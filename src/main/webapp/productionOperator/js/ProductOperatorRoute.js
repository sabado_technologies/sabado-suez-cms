var App = angular.module('App', [ 'ui.router', 'ngStorage', 'ngFlash','angularUtils.directives.dirPagination' ]);

App.directive('fileModel', [ '$parse', function($parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;
			element.bind('change', function() {
				scope.$apply(function() {
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
} ]);
App.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/home');

	$stateProvider

	// HOME STATES AND NESTED VIEWS ========================================
	.state('ProductOperator', {
		url : '/ProductOperator',
		templateUrl : 'view/processOrder.html',
		controller : 'ProductOperatorControllers'
	})

	.state('login', {
		url : '/login',
		templateUrl : '../login/login.html',
		controller : 'LoginController',
		data : {
			displayName : 'Login',
		}
	})

	.state('intiate', {
		url : '/intiate',
		templateUrl : 'view/processList.html',
		controller : 'ProcessOrderRecipeControllers',
		params : {
			input : null
		}
	})

	.state('reactor', {
		url : '/reactor',
		templateUrl : 'view/reactor.html',
		controller : 'dailyProcessOrderReports',

	})

	.state('packaging', {
		url : '/packaging',
		templateUrl : 'view/processPacking.html',
		controller : 'poPackingController',

	})

	.state('home', {
		url : '/home',
		templateUrl : 'view/allProcessOrdersTiles.html',
		controller : 'AllOrdersController',
		data : {
			displayName : 'AllProcessOrdersList',
		}
	})

	.state('tempCanvas', {
		url : '/tempCanvas',
		templateUrl : 'view/TemparatureCanvas.html',
		controller : 'ChartController',

	})

	.state('PackingDetails', {
		url : '/PackingDetails',
		templateUrl : 'view/PackingDetails.html',
		controller : 'PackingOrderDetailsController',

	})
	
	.state('processOrders', {
		url : '/processOrders',
		templateUrl : '../productionOperator/view/po-home.html',
		controller : 'poHomeController'
	})
	
	.state('QualityCheck', {
		url : '/QualityCheck',
		templateUrl : '../productionOperator/view/Quality.html',
		controller : 'QualityController'
	})
	
	.state('InventoryCheck', {
		url : '/InventoryCheck',
		templateUrl : '../productionOperator/view/orders.html',
		controller : 'InventoryCheckController'
	})
	
	.state('InventoryCheckDetails', {
		url : '/InventoryCheckDone',
		templateUrl : '../productionOperator/view/poinventory.html',
		controller : 'InventoryCheckDetailsController'
	})
	
	.state('repacking', {
		url : '/repacking',
		templateUrl : '../productionOperator/view/Repacking.html',
		controller : 'repackingController'
	})
	
	.state('RepackingDetails', {
		url : '/RepackingDetails',
		templateUrl : '../productionOperator/view/RepackingDeatails.html',
		controller : 'RepackingDetailsController'
	})

	.state('RepackingInventoryCheck', {
		url : '/RepackingInventoryCheck',
		templateUrl : '../productionOperator/view/RepackingOrders.html',
		controller : 'RepackingInventoryCheckController'
	})
	
	.state('RepackingInventoryCheckDetails', {
		url : '/RepackingInventoryCheckDetails',
		templateUrl : '../productionOperator/view/RepackingRMInventory.html',
		controller : 'RepackingInventoryCheckDetailsController'
	})
	
	.state('RepackingQualityCheck', {
		url : '/RepackingQualityCheck',
		templateUrl : '../productionOperator/view/QualityRepacking.html',
		controller : 'QualityRepackingController'
	})
	.state('processOrder-status', {
		url : '/processOrder-status',
		templateUrl : 'view/live.html',
		controller : 'StatusInfoController'
	}).state('repacking-status-info', {
		url : '/repacking-status-info',
		templateUrl : 'view/repackingLive.html',
		controller : 'RepackingStatusInfoController'
	})
	.state('repackingHistory', {
		url : '/repackingHistory',
		templateUrl : 'view/repackingHistory.html',
		controller : 'RepackingHistoryController'
	})
	.state('repackingHistoryDetails', {
		url : '/repackingHistoryDetails',
		templateUrl : 'view/repackingHistoryDetails.html',
		controller : 'RepackingHistoryDetailsController'
	})
	.state('history', {
		url : '/history',
		templateUrl : 'view/history.html',
		controller : 'ProcessHistoryController'
	})
	
	.state('historyfirst', {
		url : '/history',
		templateUrl : 'view/historyfirst.html',
		controller : 'ProcessHistoryDetailsController'
	})
});

App.run([ '$state', '$rootScope', '$transitions',
		function($state, $rootScope, $transitions) {
			$transitions.onExit({}, function(trans, $state) {
				//console.log(trans._targetState._identifier);

				if ($state.name == "intiate") {

					if (trans._targetState._identifier == 'QualityCheck') {

					} else {
						var answer = confirm("Are you really want to leave, Charging is in process?")
						if (!answer) {
							return false;
						}
					}

				}
			});
		} ]);

App.controller('logOutController', [
		'$scope',
		'$window',
		'$http',
		'$filter',
		'$localStorage',
		'$rootScope',
		'Flash','$localStorage',
		function($scope, $window, $http, $filter, $localStorage, $rootScope,
				Flash,$localStorage) {

			$scope.showModal = true;
			$scope.showButtons = true;
			$scope.showClose = false;
			$scope.showFailed = false;

			$scope.getAlertsForProductionOperator = function() {
				$http({
					method : "GET",
					url : '/suez/RoleBasedAlerts/alertsForAdmin ',
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function mySucces(response) {
					//console.log(response);
					$scope.alertInformation = response.data;
					//console.log($scope.alertInformation);

				}, function myError(error) {
					//console.log(error);
				});

			};

			var diffRoles = [];
			var sso = $window.sessionStorage.ssoId;
			if(sso == undefined){
				//alert('Empty');
				window.location.href = "/suez";
			}
			$http({
				method : "GET",
				url : '/suez/po/RoleList/' + sso,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function mySucces(response) {
				$scope.roles = response.data;
				diffRoles = $scope.roles;
				if (diffRoles.length > 1) {
					$rootScope.showLoginAs = true;
				}

				for (var i = 0; i < diffRoles.length; i++) {
					if (diffRoles[i].role == "1") {
						$rootScope.isAdmin = true;
					} else if (diffRoles[i].role == "2") {
						$rootScope.isWarehouse = true;
					} else if (diffRoles[i].role == "3") {
						$rootScope.isManagement = true;
					} else if (diffRoles[i].role == "5") {
						$rootScope.isQuality = true;
					}
				}

			}, function myError(error) {
				//console.log(error);
			});
			$rootScope.isAdmin = false;
			$rootScope.isWarehouse = false;
			$rootScope.isOperator = false;
			$rootScope.isQuality = false;
			$rootScope.isManagement = false;
			$rootScope.showLoginAs = false;
			$scope.warehouseLogin = function() {
				window.location.href = "../warehouse/warehouse.html";
			}

			$scope.adminLogin = function() {
				window.location.href = "../admin/admin.html";
			}

			$scope.managementLogin = function() {
				window.location.href = "../management/management.html";
			}

			$scope.qualityLogin = function() {
				window.location.href = "../quality/quality.html";
			}

			$scope.count = 0;

			$scope.CurrentDate = new Date();
			$scope.getAlertsForProductionOperator();
			$scope.ddMMyyyy = $filter('date')(new Date(), 'dd/MM/yyyy');
			$scope.ddMMMMyyyy = $filter('date')(new Date(), 'dd, MMMM yyyy');
			$scope.HHmmss = $filter('date')(new Date(), 'HH:mm:ss');
			$scope.hhmmsstt = $filter('date')(new Date(), 'hh:mm:ss a');

			$scope.logOut = function() {
				$window.sessionStorage.ssoId = '';
				$window.sessionStorage.role = '';
				$localStorage.userSso = " ";	
				$localStorage.userData =" ";
				localStorage.setItem('loginUser', '');
				window.location.href = "/suez";
			};

			$scope.getUserInformation = function(ssoId) {
				var ssoId = $localStorage.userSso;
				$http({
					method : "GET",
					url : '/suez/user/getUserBySSOId/' + ssoId,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function mySucces(response) {
					//console.log(response);
					$scope.userInformation = response.data;
					$localStorage.Username = $scope.userInformation.userName;
				}, function myError(error) {
					//console.log(error);
				});
			};

			$scope.update = function(newPassword, checkPassword) {
				//alert();
				if (newPassword == undefined || checkPassword == undefined) {
					$scope.fillAll = true;
					return;

				}
				if(newPassword !=checkPassword){
					return;
				}
				var user = {};
				user.ssoid = $window.sessionStorage.ssoId;
				user.password = newPassword;

				$http.post('/suez/user/changePassword', JSON.stringify(user), {
					'Content-Type' : 'application/json'
				}).then(function(response) {
					$scope.showModal = false;
					$scope.showButtons = false;
					$scope.showClose = true;
					$scope.showFailed = false;
				}, function(response) {
					var message = "";
					message = 'Please try again';
					Flash.create('warning', message);
				});
			}
		} ]);
