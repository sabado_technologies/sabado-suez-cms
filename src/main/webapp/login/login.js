myApp.controller("LoginController", function($scope, $state, $localStorage,	LoginService,$window) {
	
	var init = function () {
		var ssoId = $window.sessionStorage.ssoId;
		var roleId = $window.sessionStorage.role;
		
		
		//alert("ssoIdd==>"+ssoId+"&roleId=>"+roleId);
		if (roleId ==  1) {
			//$state.go('admin');
			window.location.href = "admin/admin.html";
		}
		////Warehouse///////
		else if (roleId == 2) {
			//$state.go('warehouse');
			window.location.href = "warehouse/warehouse.html";
		}
		/////MANAGER/////
		else if (roleId == 3) {
			//$state.go('Manager');
			window.location.href = "management/management.html";
		}
		//PO
		else if (roleId == 4){
			//$state.go('login');
			window.location.href ="productionOperator/poWelcome.html";
		}
		
		else if (roleId == 5){
			//$state.go('login');
			window.location.href ="quality/quality.html";
		}
	};
	
	init();
	
		
		
	
	$scope.message=""
		
	$scope.checkLogin = function(u) {
		LoginService.login(u).then(function(response) {
			console.log(response);
			if (response.data.code === '200') {
				$scope.userlist = response.data.data;
				console.log("*****************");
				console.log($scope.userlist.length);
				console.log("*****************");
				if($scope.userlist.length != 0){
					$scope.isLabelVisible = false;
					$localStorage.userData = $scope.userlist[0];
					$window.sessionStorage.ssoId = $scope.userlist[0].ssoid;
					$localStorage.userSso=$scope.userlist[0].ssoid;
					var loginUser = $scope.userlist[0].userName;
					localStorage.setItem('loginUser', loginUser);
					//$localStorage.userName=$scope.userlist[0].userName;
					//sessionStorage.setItem("loggedUsername", loginUser);
					$window.sessionStorage.role = $scope.userlist[0].role.roleId;
					//console.log($localStorage.userData);
					//alert($localStorage.userData);
				/////Admin Page/////
					if ($window.sessionStorage.role ==  1) {
						//$state.go('admin');
						window.location.href = "admin/admin.html";
					}
					////Warehouse///////
					else if ($window.sessionStorage.role == 2) {
						//$state.go('warehouse');
						window.location.href = "warehouse/warehouse.html";
					}
					/////MANAGER/////
					else if ($window.sessionStorage.role == 3) {
						//$state.go('warehouse');
						window.location.href = "management/management.html";
					}
					//PO
					else if ($window.sessionStorage.role == 4){
						//$state.go('login');
						window.location.href ="productionOperator/poWelcome.html";
					}
					else if ($window.sessionStorage.role == 5){
						//$state.go('login');
						window.location.href ="quality/quality.html";
					}
					
					else if (response.data.code == ''){
						$scope.message=("SSO and Password does not matched");
					}
				}
				else if($scope.userlist.length == 0){
					//alert("No data found");
					$scope.isLabelVisible = true;
				}
				
			}
			
			if(response.data.code === '300')
			{
				
				//$scope.message=("SSO and Password does not matched");
				$scope.isLabelVisible1 = true;
			}
			
			 $scope.clearMessage =function(){
				   $scope.message="";
			   }
		});
	}

});
