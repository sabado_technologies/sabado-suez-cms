var myApp = angular.module('myApp', [ 'ui.router','ngStorage','ui.bootstrap','angularUtils.directives.dirPagination','multipleDatePicker']);

myApp.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/login');

	$stateProvider

	// HOME STATES AND NESTED VIEWS ========================================
	.state('login', {
		url : '/login',
		templateUrl : 'login/login.html',
		controller : 'LoginController',
		data : {
			displayName : 'Login',
		}
	}).state('warehouse', {
		url : '/warehouse',
		templateUrl : 'html/warehouse.html',
		controller : 'WarehouseController'
		
	}).state('admin', {
		url : '/admin',
		templateUrl : 'dashboards/admin.html',
		controller : ''
			
	}).state('warehouselists',{
		url : '/warehouselists',
		templateUrl : 'html/warehouselists.html',
		controller : 'WarehouseController1'
			
	})/*.state('adduser',{
		url : '/adduser',
		templateUrl : 'html/admin/adduser.html',
		controller : ''
	})*/
	// HOME STATES AND NESTED VIEWS ========================================
	
});

