myApp.factory("LoginService", function($http, $q) {
	// F var context = ${rootxon}
	return {

		login : function(user) {

			return $http.post('/suez/loginController/login',
					JSON.stringify(user), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(error) {
				return {
					error : error.data.message
				};
			});
		},
		
		getUserRoles : function(sso) {
			return $http.get(
					'/suez/po/RoleList/'
							+ sso, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;
			}, function(response) {
				alert('error');
			});
		}

	}

});

myApp.factory('WarehouseService', function($http) {
	return {
		getAllProcessOrders : function() {
			return $http.get('/suez/WareHouseController/getAllProcessOrders', {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				alert('error');
			});
		}

	}
});

myApp.factory('WarehouseService1', function($http) {
	return {
		getProcessOrderDetails : function(processOrderId) {
			return $http.get(
					'/suez/WareHouseController/getProcessOrderDetails/'
							+ processOrderId, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;
			}, function(response) {
				alert('error');
			});
		}

	}
});
