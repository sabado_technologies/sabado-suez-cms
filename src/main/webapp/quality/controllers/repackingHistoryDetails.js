myQuality.controller("RepackingHistoryDetailsController", function($scope, $state,
		$localStorage, $window, QualityService) {
	
	$scope.processorderPckgNumber = $window.sessionStorage.processorderRepackHistoryPckgNumber;
	
	QualityService.getRepackProcessOrderForHistory($scope.processorderPckgNumber).then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data;
					
				}

			})
			
$scope.getVesselHrs =  function(){
		
		$scope.vesselHrsProcess = $scope.processorderPckgNumber;
		
		QualityService.getRepackPackinVesselHrs($scope.processorderPckgNumber)
		.then(function(response) {
			
			if (response.data !== 'undefined') {
				$scope.packingVesselHrs = response.data.data;
				//console.log($scope.packingVesselHrs);

		
			}
		});
	}
	
			
});