myQuality.controller("QualityResultController", function($scope, $state,$window,Flash, QualityResultService) {
		
	function alertForError(message){
		new PNotify({
	        text    : message,
	        confirm : {
	            confirm: true,
	            buttons: [
	                {
	                    text    : 'Close',
	                    addClass: 'btn btn-link',
	                    click   : function (notice)
	                    {
	                        notice.remove();
	                    }
	                },
	                null
	            ]
	        },
	        buttons : {
	            closer : false,
	            sticker: false
	        },
	        animate : {
	            animate  : true,
	            in_class : 'slideInDown',
	            out_class: 'slideOutUp'
	        },
	        addclass: 'md multiline'
	    });
		
	}
	
	$scope.showRework = true;
	$scope.showScrap = false;
	
	QualityResultService.getReworkDetails().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.reworkOrders =[];
			$scope.reworkOrders = response.data.data;	
		}
	});
	
	QualityResultService.getScrapDetails().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.scrapDetails = response.data.data;
		}
	});
	
	$scope.packingDetails = function(processOrder){
		QualityResultService.getReworkPackingDetails(processOrder).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.reworkPackDetails = response.data.data;
			}
		});
	}
	
	$scope.getRework = function(){
		$scope.showRework = true;
		$scope.showScrap = false;
	}
	$scope.getScrap = function(){
		$scope.showRework = false;
		$scope.showScrap = true;
	}
	
	$scope.giveComments = function(poNumber){
		$scope.orderNumber = poNumber;
		//alert($scope.orderNumber);
	}
	
	$scope.approve = function(){
		if($scope.comments == undefined){
			var message = 'Please give Remarks';
			alertForError(message);
			return;
		}
		//alert('Order =>' +$scope.orderNumber);
		//alert('Comments =>' +$scope.comments);
		
			QualityResultService.approveRework($scope.orderNumber,$scope.comments,$window.sessionStorage.ssoId).then(function(response) {
				if (response.data !== 'undefined') {
					if(response.data.code === '200'){
						//alert(response.data.data +' Approved');
						var message = 'Quality Inspection Completed for Process Order '+'<strong>'+response.data.data+'</strong>';
						alertForError(message);
						$state.reload();
					}else{
						var message = 'Please try again';
						alertForError(message);
						$state.reload();
						//alert(response.data.data +' Failed');
					}
					//$scope.reworkPackDetails = response.data.data;
				}
			});
		
	}
	
	
});