myQuality.controller("RepackController", function($scope, $state,$window,Flash, RepackService,QualityService) {
		
	function alertForError(message){
		new PNotify({
	        text    : message,
	        confirm : {
	            confirm: true,
	            buttons: [
	                {
	                    text    : 'Close',
	                    addClass: 'btn btn-link',
	                    click   : function (notice)
	                    {
	                        notice.remove();
	                    }
	                },
	                null
	            ]
	        },
	        buttons : {
	            closer : false,
	            sticker: false
	        },
	        animate : {
	            animate  : true,
	            in_class : 'slideInDown',
	            out_class: 'slideOutUp'
	        },
	        addclass: 'md multiline'
	    });
		
	}
	
	$scope.showRepackDetails = true;
	$scope.showQualityCheckDetails = false;
	$scope.repackOrders = [];
	RepackService.getRepackProcessOrders().then(function(response) {
		if (response.data !== 'undefined') {			
			$scope.repackOrders = response.data.data;
			//$scope.count = $scope.productOrders.length;
			
		}
	});
	
	/*QualityService.getQuality().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.qualities = response.data.data;
			//count = $scope.qualities.length;
		}
	});*/
	
	var processOrder = {};
	
	$scope.showQuality = function(packingOrder){
		
		processOrder.processorderPckgNumber = packingOrder;
		
		$scope.packList = false;
		$scope.qualityList = true;
		//alert(processOrder);
	}
	var product={};
	$scope.showPackedOrders = function(processorderProdNumber){
		product.processorderProdNumber =processorderProdNumber;
		
		QualityService.getPackingOrders(processorderProdNumber).then(function(response){
			if(response.data != undefined){
				if(response.data.code === '200'){
					$scope.packingOrders = response.data.data;
					
					$scope.packList = true;
					$scope.qualityList = false;
					$scope.productList = false;
					$scope.showProceed = false;
					//console.log($scope.packingOrders);
					$state.reload();
				
			}
			else {
				//alert('Not done')
				$state.reload();
		       }
			}
		})
		
	}
	
    $scope.selected=[];
	
	
      
    
    
    var processorderProdNumber;
   // var processorderPckgNumber;
 $scope.showAllQualities = function(processOrderNumber){
	 processorderProdNumber=processOrderNumber;
	 $scope.acceptOrder = processOrderNumber;
	 
	 $scope.showRepackDetails = false;
	 $scope.showQualityCheckDetails = true;
	 
	 QualityService.getAllQualities(processOrderNumber).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.allQualities = response.data.data;
				
			}
		});
    }
 var ssoid;
 
 $scope.refresh = function(){
	 $state.reload();
 }
 
 $scope.acceptQuality = function(){
	 ssoid= $window.sessionStorage.ssoId;
	 //alert('SSO =>'+ssoid);
	 //alert('Process order =>'+processorderProdNumber);
	 
	 RepackService.acceptQuality(processorderProdNumber,ssoid).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					
					//alert(response.data.data);
					//Flash.clear();
					//var message = "";
					var message = 'Quality Inspection Is Sucessfully Completed for ' +'<strong>'+ response.data.data+'</strong>';
					//Flash.create('success', message);
					alertForError(message);
					
					//$scope.updateSuccess =("Quality Inspection Is Sucessfully Completed for " + response.data.data );
					$state.reload();
					
				}else{
					//Flash.clear();
					//var message = "";
					var message = 'Please try again';
					alertForError(message);
					//Flash.create('danger', message);
					
					//$scope.updateSuccess =("Quality Inspection Is Sucessfully Completed for " + response.data.data );
					$state.reload();
					//alert('Failed');
					//$state.reload();
				}
				//$scope.allQualities = response.data.data;
				
			}
		});
	 $scope.packList = false;
	 $scope.qualityList = false;
	 $scope.productList = true;
	 /*if($scope.packingOrders.length==''){
		 alert('No Packing Orders')
		 
	 }*/
	 
 }
 
 $scope.rejectQuality = function(){
		// var processorderProdNumber=orderNumber;
		 //alert(processorderProdNumber);
		// alert($scope.selection);
	 if($scope.comments == undefined){
		 //alert('Please give remarks');
		 var message = 'Please give remarks';
		 alertForError(message);
		 return;
	 } else if($scope.selection == undefined){
		 //alert('Please select option');
		 var message = 'Please select option';
		 alertForError(message);
		 return;
	 }
	 ssoid= $window.sessionStorage.ssoId;
	 //alert(ssoid);
	// alert($scope.selection);
 	if($scope.selection == "Rework" ){
 		//alert('Rework');
 		RepackService.declineQuality(processorderProdNumber,ssoid,$scope.comments).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					
					//alert(response.data.data);
					//Flash.clear();
					//alert('Rework initiated');
					//var message = "";
					var message = 'Process Order ' +'<strong>'+ response.data.data+'</strong>' +' is initiated to Re-work';
					//Flash.create('success', message);
					alertForError(message);
					//$scope.updateSuccess =("Quality Inspection Declined For " + response.data.data );
					$state.reload();
					
				}else{
					//Flash.clear();
					//var message = "";
					//alert('Failed');
					var message = 'Please try again ';
					//Flash.create('danger', message);
					alertForError(message);
					$state.reload();
				}
				//$scope.allQualities = response.data.data;
				
			}
		});
 	} else if($scope.selection == "Scrap"){
 		//alert('Scrap');
 		RepackService.scrapQuality(processorderProdNumber,ssoid,$scope.comments).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					
					//alert(response.data.data);
					//Flash.clear();
					//var message = "";
					//alert('Scrap initiated');
					var message = 'Process Order'+'<strong>'+ response.data.data+'</strong>' +' is Scraped';
					//Flash.create('success', message);
					alertForError(message);
					//$scope.updateSuccess =("Quality Inspection Declined For " + response.data.data );
					$state.reload();
					
				}else{
					//Flash.clear();
					//var message = "";
					//alert('Failed');
					var message = 'Please try again ';
					alertForError(message);
					//Flash.create('danger', message);
					$state.reload();
				}
				//$scope.allQualities = response.data.data;
				
			}
		});
 	}
		 
		 $scope.packList = false;
		 $scope.qualityList = false;
		 $scope.productList = true;
		 
	 }
	
});