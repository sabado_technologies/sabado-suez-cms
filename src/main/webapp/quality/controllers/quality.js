myQuality.controller("QualityController", function($scope, $state,$window,Flash,$localStorage, QualityService) {
	
	$scope.packList = false;
	$scope.qualityList = false;
	$scope.productList = true;
	$scope.showProceed = false;
	//$scope.showPackages = false;
	//$scope.showProducts = true;
	//var count="0";
	
	/*QualityService.getPackingOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.packingOrders = response.data.data;
			
		}
	});*/
	
	function alertForError(message){
		new PNotify({
	        text    : message,
	        confirm : {
	            confirm: true,
	            buttons: [
	                {
	                    text    : 'Close',
	                    addClass: 'btn btn-link',
	                    click   : function (notice)
	                    {
	                        notice.remove();
	                    }
	                },
	                null
	            ]
	        },
	        buttons : {
	            closer : false,
	            sticker: false
	        },
	        animate : {
	            animate  : true,
	            in_class : 'slideInDown',
	            out_class: 'slideOutUp'
	        },
	        addclass: 'md multiline'
	    });
		
	}
	
	QualityService.getProductOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.productOrders = response.data.data;
			$scope.count = $scope.productOrders.length;
			
		}
	});
	//var count
	QualityService.getQuality().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.qualities = response.data.data;
			//count = $scope.qualities.length;
		}
	});
	
	var processOrder = {};
	
	$scope.showQuality = function(packingOrder){
		
		processOrder.processorderPckgNumber = packingOrder;
		
		$scope.packList = false;
		$scope.qualityList = true;
		//alert(processOrder);
	}
	var product={};
	$scope.showPackedOrders = function(processorderProdNumber){
		product.processorderProdNumber =processorderProdNumber;
		
		QualityService.getPackingOrders(processorderProdNumber).then(function(response){
			if(response.data != undefined){
				if(response.data.code === '200'){
					$scope.packingOrders = response.data.data;
					
					$scope.packList = true;
					$scope.qualityList = false;
					$scope.productList = false;
					$scope.showProceed = false;
					//console.log($scope.packingOrders);
					$state.reload();
				
			}
			else {
				alert('Not done')
				$state.reload();
		       }
			}
		})
		
	}
	
    $scope.selected=[];
	
	
    /*$scope.toggleSelection = function(qualitie){
		
		var idx=$scope.selected.indexOf(qualitie);
		if(idx > -1){
			$scope.selected.splice(idx, 1);			
		}
		else{
			$scope.selected.push(qualitie);
		}
		
		if($scope.selected.length == count){
			$scope.showProceed = true;
		}else{
			$scope.showProceed = false;
		}
	}*/
    
    /*$scope.qualityOption = function(){
    	alert($scope.selection);
    	if($scope.selection == "Rework" ){
    		alert('Rework');
    	} else if($scope.selection == "Scrap"){
    		alert('Scrap');
    	}
    }*/
    
    $scope.checkQuality = function(){
    	
    	QualityService.updateQuality(processOrder).then(function(response){
			if(response.data != undefined){
				if(response.data.code === '200'){
					////console.log(processOrder);
			    	//alert(processOrder.processorderPckgNumber);
			    	// processOrder = null;
			    	//alert(processOrder.processorderPckgNumber);
			    	////console.log(processOrder);
			    	//alert(product.processorderProdNumber);
					//$scope.showModal = false;
					//var message = response.data.data  + " " + 'updated successfully';
					//var message = 'User is updated successfully';
				   // Flash.create('success', message);  //Display message if Updating User Success
					//alert('done')
					$state.reload();
					//$scope.updation = ('User is updated successfully');
				   // $state.reload();
				
			}
			else {
				alert('Not done')
				//var message = 'Updating' +  response.data.data + 'is failed';
				//var message = 'Updating user is failed';
			   // Flash.create('danger', message);  //Display message if Updating User fails
				$state.reload();
			     // $scope.updation = ('Updating user is failed');
			     // $state.reload();
		       }
			}
		})
		
		/*if ($scope.packingOrders.length ==='0'){
			
		}*/
    }
    
    var processorderProdNumber;
   // var processorderPckgNumber;
 $scope.showAllQualities = function(processOrderNumber){
	 processorderProdNumber=processOrderNumber;
	 $scope.acceptOrder = processOrderNumber;
	// processorderPckgNumber=pckNumber;
	// alert(processorderProdNumber);
	 //alert(processorderPckgNumber);
	 
	 $scope.packList = false;
	 $scope.qualityList = true;
	 $scope.productList = false;
	 
	 /*QualityService.getAllQualities(processOrderNumber).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.allQualities = response.data.data;
				
			}
		});*/
	 
	 QualityService.getAllQualities(processOrderNumber).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.allQualities = response.data.data;
				
			}
		});
    }
 var ssoid;
 
 $scope.refresh = function(){
	 $state.reload();
 }
 
 $scope.acceptQuality = function(){
	 ssoid= $localStorage.userSso;
	 //alert($localStorage.userSso);
	 //console.log($localStorage.userSso);
	 //return;
	 QualityService.acceptQuality(processorderProdNumber,ssoid).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					
					//alert(response.data.data);
					//Flash.clear();
					//var message = "";
					var message = 'Quality Inspection Is Sucessfully Completed for ' +'<strong>'+ response.data.data+'</strong>';
					//Flash.create('success', message);
					alertForError(message);
					
					//$scope.updateSuccess =("Quality Inspection Is Sucessfully Completed for " + response.data.data );
					$state.reload();
					
				}else{
					//Flash.clear();
					//var message = "";
					var message = 'Please try again';
					alertForError(message);
					//Flash.create('danger', message);
					
					//$scope.updateSuccess =("Quality Inspection Is Sucessfully Completed for " + response.data.data );
					$state.reload();
					//alert('Failed');
					//$state.reload();
				}
				//$scope.allQualities = response.data.data;
				
			}
		});
	 $scope.packList = false;
	 $scope.qualityList = false;
	 $scope.productList = true;
	 /*if($scope.packingOrders.length==''){
		 alert('No Packing Orders')
		 
	 }*/
	 
 }
 
 $scope.rejectQuality = function(){
		// var processorderProdNumber=orderNumber;
		 //alert(processorderProdNumber);
		 //alert(processorderPckgNumber);
	 if($scope.comments == undefined){
		 //alert('Please give remarks');
		 var message = 'Please give remarks';
		 alertForError(message);
		 return;
	 } else if($scope.selection == undefined){
		 //alert('Please select option');
		 var message = 'Please select option';
		 alertForError(message);
		 return;
	 }
	 ssoid= $window.sessionStorage.ssoId;
	 //alert(ssoid);
	// alert($scope.selection);
 	if($scope.selection == "Rework" ){
 		var batchNo = $scope.allQualities[0].batchNumber;
 		var prodNo = $scope.allQualities[0].productNumber;
 		var memo = $scope.allQualities[0].materialMemo;
 		//alert('Rework');
 		QualityService.declineQuality(processorderProdNumber,ssoid,$scope.comments,batchNo,prodNo,memo).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					
					//alert(response.data.data);
					//Flash.clear();
					//alert('Rework initiated');
					//var message = "";
					var message = 'Process Order ' +'<strong>'+ response.data.data+'</strong>' +' is initiated to Re-work';
					//Flash.create('success', message);
					alertForError(message);
					//$scope.updateSuccess =("Quality Inspection Declined For " + response.data.data );
					$state.reload();
					
				}else{
					//Flash.clear();
					//var message = "";
					//alert('Failed');
					var message = 'Please try again ';
					//Flash.create('danger', message);
					alertForError(message);
					$state.reload();
				}
				//$scope.allQualities = response.data.data;
				
			}
		});
 	} else if($scope.selection == "Scrap"){
 		//alert('Scrap');
 		QualityService.scrapQuality(processorderProdNumber,ssoid,$scope.comments).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					
					//alert(response.data.data);
					//Flash.clear();
					//var message = "";
					//alert('Scrap initiated');
					var message = 'Process Order'+'<strong>'+ response.data.data+'</strong>' +' is Scraped';
					//Flash.create('success', message);
					alertForError(message);
					//$scope.updateSuccess =("Quality Inspection Declined For " + response.data.data );
					$state.reload();
					
				}else{
					//Flash.clear();
					//var message = "";
					//alert('Failed');
					var message = 'Please try again ';
					alertForError(message);
					//Flash.create('danger', message);
					$state.reload();
				}
				//$scope.allQualities = response.data.data;
				
			}
		});
 	}
		 /*QualityService.declineQuality(processorderProdNumber,ssoid).then(function(response) {
				if (response.data !== 'undefined') {
					if(response.data.code === '200'){
						
						//alert(response.data.data);
						Flash.clear();
						//var message = "";
						var message = 'Quality Inspection Declined For ' +'<strong>'+ response.data.data+'</strong>';
						Flash.create('success', message);
						//$scope.updateSuccess =("Quality Inspection Declined For " + response.data.data );
						$state.reload();
						
					}else{
						Flash.clear();
						//var message = "";
						var message = 'Please try again ';
						Flash.create('danger', message);
						$state.reload();
					}
					//$scope.allQualities = response.data.data;
					
				}
			});*/
		 $scope.packList = false;
		 $scope.qualityList = false;
		 $scope.productList = true;
		 /*if($scope.packingOrders.length==''){
			 alert('No Packing Orders')
			 
		 }*/
		 
	 }
	
	
	
});