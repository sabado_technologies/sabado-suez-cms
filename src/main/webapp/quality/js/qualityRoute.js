
var myQuality = angular.module('myQuality', [ 'ui.router', 'ngFlash', 'ngStorage',
		'ui.bootstrap', 'angularUtils.directives.dirPagination','ngMessages' ]);


myQuality.config(function($stateProvider, $urlRouterProvider) {

	// $urlRouterProvider.otherwise();
	
	$urlRouterProvider.otherwise('packingOrders');

	$stateProvider

	// HOME STATES AND NESTED VIEWS ========================================
	.state('packingOrders', {
		url : '/packingOrders',
		templateUrl : 'view/packingOrders.html',
		controller : 'QualityController',
		data : {
			displayName : 'Login',
		}
	})
	.state('reject', {
		url : '/reject',
		templateUrl : 'view/reject.html',
		controller : 'QualityResultController',
		data : {
			displayName : 'Login',
		}
	}).state('repack', {
		url : '/repack',
		templateUrl : 'view/repack.html',
		controller : 'RepackController',
		data : {
			displayName : 'Login',
		}
	})
	.state('processOrder-status', {
		url : '/processOrder-status',
		templateUrl : 'view/live.html',
		controller : 'StatusInfoController'
	}).state('repacking-status-info', {
		url : '/repacking-status-info',
		templateUrl : 'view/repackingLive.html',
		controller : 'RepackingStatusInfoController'
	})
	.state('repackingHistory', {
		url : '/repackingHistory',
		templateUrl : 'view/repackingHistory.html',
		controller : 'RepackingHistoryController'
	})
	.state('repackingHistoryDetails', {
		url : '/repackingHistoryDetails',
		templateUrl : 'view/repackingHistoryDetails.html',
		controller : 'RepackingHistoryDetailsController'
	})
	.state('history', {
		url : '/history',
		templateUrl : 'view/history.html',
		controller : 'ProcessHistoryController'
	})
	
	.state('historyfirst', {
		url : '/history',
		templateUrl : 'view/historyfirst.html',
		controller : 'ProcessHistoryDetailsController'
	})
});

myQuality.controller('logOutController', [ '$scope', '$window', '$http','$filter','$timeout','Flash','$rootScope','$localStorage',
		function($scope, $window, $http, $filter, $timeout,Flash,$rootScope,$localStorage ) {
	
	/*$scope.showModal = true;
	$scope.showButtons = true;
	$scope.showClose = false;
	$scope.showFailed = false;*/
	var diffRoles =[];
	var sso = $localStorage.userSso;
	if(sso == undefined){
		//alert('Empty');
		window.location.href = "/suez";
	}
	$http({		
		method : "GET",		
		url : '/suez/po/RoleList/'+ sso ,		
	headers : {		
			'Content-Type' : 'application/json'		
		}		
	}).then(function mySucces(response) {		
		//console.log(response);	
		//alert('Got all Rroles');
		$scope.roles = response.data;
		diffRoles = $scope.roles;
		/*alert($scope.roles[0].role);
		alert($scope.roles[1].role);
		alert($scope.roles[2].role);*/
		if(diffRoles.length > 1){
			$rootScope.showLoginAs=true;
		}
		
		for(var i=0; i<diffRoles.length; i++){
			if(diffRoles[i].role=="2"){
				$rootScope.isWarehouse=true;
			} else if (diffRoles[i].role=="3"){
				$rootScope.isManagement=true;
			} else if (diffRoles[i].role=="4"){
				$rootScope.isOperator=true;
			}else if (diffRoles[i].role=="1"){
				$rootScope.isAdmin=true;
			}
		}
		////console.log($scope.alertInformation);		
				
	}, function myError(error) {		
		//console.log(error);		
	});
	
	//var roles=["2","3"];
	$rootScope.isAdmin=false;
	$rootScope.isWarehouse=false;
	$rootScope.isOperator=false;
	$rootScope.isQuality=false;
	$rootScope.isManagement=false;
	$rootScope.showLoginAs=false;
	
		
	$scope.warehouseLogin = function(){
		//alert('Login to warehouse');
		window.location.href = "../warehouse/warehouse.html";
	}
	
	$scope.operatorLogin = function(){
		//alert('Login to Production');
		window.location.href ="../productionOperator/poWelcome.html";
	}
	
	$scope.managementLogin = function(){
		//alert('Login to Production');
		window.location.href ="../management/management.html";
	}
	
	$scope.adminLogin = function(){
		//alert('Login to Production');
		window.location.href ="../admin/admin.html";
	}
	
	$scope.showModal = true;
	$scope.showButtons = true;
	$scope.showClose = false;
	$scope.showFailed = false;
	
	$scope.getAlertsForQualityAssurance =function(){		
		
		$http({		
				method : "GET",		
				url : '/suez/RoleBasedAlerts/alertsForAdmin ' ,		
			headers : {		
					'Content-Type' : 'application/json'		
				}		
			}).then(function mySucces(response) {		
				//console.log(response);		
				$scope.alertInformation = response.data;
				//console.log($scope.alertInformation);		
						
			}, function myError(error) {		
				//console.log(error);		
			});		
					
		};		
	
	$scope.CurrentDate = new Date();
	$scope.getAlertsForQualityAssurance();
	
	$scope.ddMMyyyy = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.ddMMMMyyyy = $filter('date')(new Date(), 'dd, MMMM yyyy');
    $scope.HHmmss = $filter('date')(new Date(), 'HH:mm:ss');
    $scope.hhmmsstt = $filter('date')(new Date(), 'hh:mm:ss a');
    
			$scope.count = 0;
			$scope.logOut = function() {
				$window.sessionStorage.ssoId = '';
				$window.sessionStorage.role = '';
				$localStorage.userSso = " ";	
				$localStorage.userData =" ";
				localStorage.setItem('loginUser', '');
				window.location.href = "/suez";
			};

			$scope.getUserInformation = function(ssoId) {
				var ssoId = $window.sessionStorage.ssoId;
				$http({
					method : "GET",
					url : '/suez/user/getUserBySSOId/' + ssoId,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function mySucces(response) {
					//console.log(response);
					$scope.userInformation = response.data;
					//console.log($scope.userInformation);
				}, function myError(error) {
					//console.log(error);
				});
				// alert(ssoId);
			};
			
			$scope.update = function(newPassword,checkPassword) {
				if(newPassword == undefined || checkPassword == undefined){
					$scope.fillAll = true;
					return;
					
				}
				if(newPassword !=checkPassword){
					return;
				}
				var user={};
				user.ssoid=$window.sessionStorage.ssoId;
				user.password=newPassword;
				
				$http.post('/suez/user/changePassword', JSON.stringify(user), {
					'Content-Type' : 'application/json'
				}).then(function(response) {
					$scope.showModal = false;
					$scope.showButtons = false;
					$scope.showClose = true;
					$scope.showFailed = false;

				}, function(response) {
					// something went wrong
					var message = "";
					message = 'Please try again';
					Flash.create('warning', message);
					//return response;
				});

			}

			

		} ]);
