myQuality.factory('QualityService', function($http, $q ) {
	
	
	return {
        
		getPackingOrders : function(processorderProdNumber) {
			return $http.get('/suez/quality/getPackingOrders/'+ processorderProdNumber,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		getProductOrders : function() {
			return $http.get('/suez/quality/getProductDetails').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		getQuality : function() {
			return $http.get('/suez/quality/getQualities').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		updateQuality : function(processOrder) {
			return $http.post(
					'/suez/quality/updatePackage',
					JSON.stringify(processOrder),
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},
		
		getAllQualities : function(processOrderNumber) {
			return $http.get('/suez/quality/getQualityCheckDetails/'+ processOrderNumber,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		acceptQuality : function(processorderProdNumber,ssoid) {
			return $http.post('/suez/quality/acceptQuality/'
					+ processorderProdNumber +"/"+ ssoid,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		declineQuality : function(processorderProdNumber,ssoid,comment,batchNo,prodNo,memo) {
			return $http.post('/suez/quality/rejectQuality/'
					+ processorderProdNumber +"/"+ ssoid +"/"+ comment +"/"+ batchNo +"/"+ prodNo +"/"+ memo,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		scrapQuality : function(processorderProdNumber,ssoid,comment) {
			return $http.post('/suez/quality/scrapQuality/'
					+ processorderProdNumber +"/"+ ssoid +"/"+ comment,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getAllStatusInfo : function() {
			return $http.get('/suez/pending/getAllStatusInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getAllRepackingStatusInfo : function() {
			return $http.get('/suez/pending/getAllRepackingStatusInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getProcessHistory : function() {
			return $http.get(
					'/suez/pending/getProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},		
		getProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getProcessOrdersForPostingCheck/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		} ,
		associatedPackingHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/associatedPackingInfoForPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		getRepackProcessHistory : function() {
			return $http.get(
					'/suez/pending/getRepackingProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},
		getRepackProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventoryPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		updateExtraRm : function(process,rmInfo) {
			return $http.post('/suez/pending/addExtraRawMaterial/'+ process+'/'+rmInfo, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				//console.log('error')
				//alert('error');
			});
		},
		getPackinVesselHrs : function(process) {
			return $http.get('/suez/Management/cycleTimeForModal/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		getRepackPackinVesselHrs : function(process) {
			return $http.get('/suez/pending/repackingHours/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
	}

});

myQuality.factory('QualityResultService', function($http, $q ) {
	
	
	return {
        			
		getReworkDetails : function() {
			return $http.get('/suez/qualityResults/getReWorkedProcessOrders').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getReworkPackingDetails : function(processOrder) {
			return $http.get('/suez/qualityResults/getReworkPackingDetails/'+ processOrder).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getScrapDetails : function() {
			return $http.get('/suez/qualityResults/getScrapDetails').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		approveRework : function(processOrder,remarks,sso) {
			return $http.post('/suez/qualityResults/approveRework/'+ processOrder +"/"+ remarks +"/"+ sso).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		}
		
	}
});

myQuality.factory('RepackService', function($http, $q ) {
	
	
	return {
        			
		getRepackProcessOrders : function() {
			return $http.get('/suez/repackQuality/getRepackDetails').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		acceptQuality : function(processorderProdNumber,ssoid) {
			return $http.post('/suez/repackQuality/acceptQuality/'
					+ processorderProdNumber +"/"+ ssoid,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		declineQuality : function(processorderProdNumber,ssoid,comment,batchNo,prodNo,memo) {
			return $http.post('/suez/repackQuality/rejectQuality/'
					+ processorderProdNumber +"/"+ ssoid +"/"+ comment +"/"+ batchNo +"/"+ prodNo +"/"+ memo,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		scrapQuality : function(processorderProdNumber,ssoid,comment) {
			return $http.post('/suez/repackQuality/scrapQuality/'
					+ processorderProdNumber +"/"+ ssoid +"/"+ comment,{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		}
		/*getRepackProcessOrders : function() {
			return $http.get('/suez/quality/getRepackDetails/',{
				'Content-Type' : 'application/json'
			}).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		}*//*,
		getReworkPackingDetails : function(processOrder) {
			return $http.get('/suez/qualityResults/getReworkPackingDetails/'+ processOrder).then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		}*/
		
	}

});