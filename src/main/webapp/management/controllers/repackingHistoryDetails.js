ManagementApp.controller("RepackingHistoryDetailsController", function($scope, $state,
		$localStorage, $window, ManagementService1) {
	
	$scope.processorderPckgNumber = $window.sessionStorage.processorderRepackHistoryPckgNumber;
	
	ManagementService1.getRepackProcessOrderForHistory($scope.processorderPckgNumber).then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data;
					
				}

			})
			
$scope.getVesselHrs =  function(){
		
		$scope.vesselHrsProcess = $scope.processorderPckgNumber;
		
		ManagementService1.getRepackPackinVesselHrs($scope.processorderPckgNumber)
		.then(function(response) {
			
			if (response.data !== 'undefined') {
				$scope.packingVesselHrs = response.data.data;
				//console.log($scope.packingVesselHrs);

		
			}
		});
	}
	
			
});