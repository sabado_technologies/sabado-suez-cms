 
ManagementApp.controller("ManagementController1",function ($scope,$http,$state, ManagementService1) {
	
	ManagementService1.getAllDailyProcessOrderDetails().then(function(response) {
		if(response != undefined){
			$scope.DailyOrders = response.data;
		}
			
		})
	});

ManagementApp.controller("ManagementController2",function ($scope,$http,$state, ManagementService2,ManagementService4) {
	ManagementService2.getAllWeeklyOrderDetails().then(function(response) {
		var totalAct=0;
		var i=0;
	//Start date and end date	
		/*var current = new Date();
		var weekstart = current.getDate() - current.getDay() +1;    
		var weekend = weekstart + 6;       // end day is the first day + 6 
		var monday = new Date(current.setDate(weekstart));  
		var sunday = new Date(current.setDate(weekend));
		
		var stDate=((monday.getMonth() + 1) + '/' + monday.getDate() + '/' + monday.getFullYear());
		var enDate=((sunday.getMonth() + 1) + '/' + sunday.getDate() + '/' + sunday.getFullYear());
		$scope.startDate=stDate;
		$scope.endDate=enDate;*/
		//alert(startDate);
		//alert(endDate);
//------------------
		
		if(response != undefined){
			$scope.weeklyorders = response.data;
			for(i=0;i<$scope.weeklyorders.length;i++){
			 totalAct= Number(totalAct)+Number($scope.weeklyorders[i].sum1);
			}
			$scope.totalActual=totalAct.toFixed(2);
			$scope.startDate=$scope.weeklyorders[0].batchDate;
			$scope.endDate=$scope.weeklyorders[6].batchDate;
			//alert($scope.startDate);
			//alert($scope.endDate);
			weeklyQty();
		}
			
		})
		function weeklyQty(){
		var weeklyPlanned=0;
		var i=0;
		ManagementService2.getAllPlannedProduction().then(function(response) {
			if(response != undefined){
				$scope.plannedProduction = response.data;
				/*var weeklyPlanned=Number($scope.plannedProduction[0].weeklySum)* 5;*/
				for(i=0;i<$scope.plannedProduction.length;i++){
					weeklyPlanned= Number(weeklyPlanned)+Number($scope.plannedProduction[i].weeklySum);
				}
				$scope.totalPlanned=weeklyPlanned.toFixed(2);
				var remainingQty=($scope.totalPlanned-$scope.totalActual);
				$scope.remaining=remainingQty.toFixed(2);
				
			}
				
			})
	}
	
	ManagementService4.getPastWorkingDaysCount().then(function(response) {
		if(response != undefined){
			$scope.totalPastDays=response.data;
		}
		askingRateFunction();
			
		})
	function askingRateFunction(){	
	ManagementService4.currentQuaterProductionPlan().then(function(response) {
		if(response != undefined){
			$scope.quarterProductionPlan = response.data;
			
			$scope.actualQty=$scope.quarterProductionPlan.actualQuantity;
			$scope.plannedQty=$scope.quarterProductionPlan.plannedProduction;
			$scope.remainingQty=$scope.plannedQty-$scope.actualQty;
			
			$scope.plannedWD=$scope.quarterProductionPlan.workingDays;
			$scope.actualWD=$scope.quarterProductionPlan.actualWorkingDays;
			$scope.remaingWD=$scope.plannedWD-$scope.actualWD;
			
			$scope.asngRate=$scope.remainingQty  / $scope.remaingWD;
			$scope.askingRate=$scope.asngRate.toFixed(2);
		}
			
		})
	}
	
	
	
	
	
	});

ManagementApp.controller("ManagementController15",function ($scope,$http,$state,$window, ManagementService15) {
	$scope.currentQuarterDays=true;
	$scope.selectQuarterDays=false;
	var today = new Date();
	ManagementService15.currentQuaterDaywiseProductionReport().then(function(response) {
		if(response != undefined){
			$scope.currentQuarterdays = response.data;
			$scope.currentQuarterdaysLength=$scope.currentQuarterdays.length;
			$scope.SelectQuarterCount = false;
			$scope.CurrentQuarterCount = true;
			var quarter = Math.floor((today.getMonth() + 3) / 3);
			$scope.curQuarter=quarter;

		}
		$scope.currentQuarterProductReport = function () {
			//alert();
			 alasql('SELECT [plannedDate] AS [Date ],[sum1] AS [Planned Production],[weeklySum] AS [Actual Production],[dailySum] AS [Deviation %] INTO XLSX("Day Wise Production Report.xlsx",{headers:true}) FROM ?',[$scope.currentQuarterdays]);
	 };
		
	});
	
	
	
	$scope.selectingQuarter=function(quarter){
		$scope.currentQuarterDays=false;
		$scope.selectQuarterDays=true;
		$window.sessionStorage.quarter=quarter;
	ManagementService15.SelectQuaterDaywiseProductionReport($window.sessionStorage.quarter).then(function(response) {
		if(response != undefined){
			$scope.selectQuarterdays = response.data;
			$scope.selectQuarterdaysLength=$scope.selectQuarterdays.length;
			$scope.SelectQuarterCount = true;
			$scope.CurrentQuarterCount = false;
		}
			
		});
	$scope.selectQuarterProductReport = function () {
		//alert();
		 alasql('SELECT [plannedDate] AS [Date ],[sum1] AS [Planned Production],[weeklySum] AS [Actual Production],[dailySum] AS [Deviation %] INTO XLSX("Day Wise Production Report.xlsx",{headers:true}) FROM ?',[$scope.selectQuarterdays]); };
	};
	});



ManagementApp.controller("ManagementController3",function ($scope,$http,$state, ManagementService3) {
ManagementService3.currentQuaterProductionReport().then(function(response) {
	if(response != undefined){
		$scope.quarterReports = response.data;
	}
		
	});


});


ManagementApp.controller("ManagementController4",function ($scope,$http,$state, ManagementService4) {
/*ManagementService4.getAllYearlyProductionReport().then(function(response) {
	if(response != undefined){
		$scope.yearlyReports = response.data;
	}
		
	})*/
	ManagementService4.getPastWorkingDaysCount().then(function(response) {
		if(response != undefined){
			$scope.totalPastDays=response.data;
		}
		askingRateFunction();
			
		})
	function askingRateFunction(){	
	ManagementService4.currentQuaterProductionPlan().then(function(response) {
		if(response != undefined){
			$scope.quarterProductionPlan = response.data;
			
			$scope.actualQty=$scope.quarterProductionPlan.actualQuantity;
			$scope.actualProduced=Number($scope.actualQty).toFixed(2); 
			
			$scope.plannedQty=$scope.quarterProductionPlan.plannedProduction;
			$scope.remainingQty=$scope.plannedQty-$scope.actualQty;
			
			$scope.plannedWD=$scope.quarterProductionPlan.workingDays;
			$scope.actualWD=$scope.quarterProductionPlan.actualWorkingDays;
			$scope.remaingWD=$scope.plannedWD-$scope.actualWD;
			
			$scope.asngRate=$scope.remainingQty  / $scope.remaingWD;
			$scope.askingRate=$scope.asngRate.toFixed(2);
		}
			
		})
	}
});

ManagementApp.controller("ManagementController5",function ($scope,ManagementService5) {
    /*ManagementService5.getCurrentYearMonthWiseProductionReport().then(function(response) {
		if(response != undefined){
		   ymData = response.data;
		}
			
		})
		
		
		
		
		Highcharts.chart('container', {
		    credits:{
		enable:false

		    },




		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: ''
		    },
		    subtitle: {
		      
		    },
		    xAxis: {
		        categories: [
		            'Jan',
		            'Feb',
		            'Mar',
		            'Apr',
		            'May',
		            'Jun',
		            'Jul',
		            'Aug',
		            'Sep',
		            'Oct',
		            'Nov',
		            'Dec'
		        ],
		        crosshair: true
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Production (MT)'
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>    ',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.1,
		            borderWidth: 0
		        }
		    },
		    series: [{
		        name: 'Planned Production',
		        data: [60, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

		    }, 
			ManagementService5.getCurrentYearMonthWiseProductionReport().then(function(response) {
				if(response != undefined){
					$scope.ymData = response.data;
				}
					
				}),
		    {
		        name: 'Actual Production',
		        data: [$scope.ymData[0].jan, $scope.ymData[0].feb, $scope.ymData[0].mar, $scope.ymData[0].apr, $scope.ymData[0].may, $scope.ymData[0].june, $scope.ymData[0].july, $scope.ymData[0].aug, $scope.ymData[0].sep, $scope.ymData[0].oct, $scope.ymData[0].nov, $scope.ymData[0].decm]

		    }]
		});
		
		
		
		*/
		
		
		
	});



ManagementApp.controller("ManagementController6",function ($scope,$http,$state,$window,$rootScope, ManagementService6) {
	
	$scope.CurrentYear=true;
	$scope.SelectYear=false;
	 var currentYear= new Date();
	 
	ManagementService6.getAllProductWiseReports().then(function(response) {
		if(response != undefined){
			$scope.productwisereports = response.data;
			$scope.currentlength=$scope.productwisereports.length;
			var year=currentYear.getFullYear();
			$scope.curYear=year;
			$scope.SelectYearCount = false;
			$scope.CurrentYearCount = true;
			
			
		}
			
		});
	
	ManagementService6.getAllPacketSizeCounts().then(function(response) {
		if(response != undefined){
			$scope.packetCounts = response.data;
			$scope.SelectYearCount = false;
			$scope.CurrentYearCount = true;
			
			
		}
			
		});
	$scope.ProductReport = function () {
		//alert();
		 alasql('SELECT [productnumber] AS [Product Number ],[productDescription] AS [Product Name ],[sum1] AS [Quantity],[pckg1] AS [1400Kg Packets],[pckg2] AS [1300Kg Packets],[pckg3] AS [1200Kg Packets] ,[pckg4] AS [1100Kg Packets],[pckg5] AS [1000Kg packets],[pckg6] AS [870kg Packets],[pckg7] AS [860kg Packets],[pckg8] AS [810kg Packets],[pckg9] AS [800kg Packets],[pckg10] AS [280kg Packets],[pckg11] AS [272kg Packets],[pckg12] AS [250kg Packets],[pckg13] AS [240kg Packets],[pckg14] AS [230kg Packets],[pckg15] AS [227kg Packets],[pckg16] AS [220kg Packets],[pckg17] AS [210kg Packets],[pckg18] AS [206kg Packets],[pckg19] AS [205kg Packets],[pckg20] AS [204kg Packets],[pckg21] AS [200kg Packets],[pckg22] AS [190kg Packets],[pckg23] AS [188kg Packets],[pckg24] AS [185kg Packets], [pckg25] AS [180kg Packets],[pckg26] AS [175kg Packets],[pckg27] AS [170kg Packets],[pckg28] AS [165kg Packets],[pckg29] AS [160kg Packets],[pckg30] AS [150kg Packets],[pckg31] AS [110kg Packets],[pckg32] AS [100kg Packets],[pckg33] AS [80kg Packets],[pckg34] AS [70kg Packets],[pckg35] AS [60kg Packets],[pckg36] AS [50kg Packets],[pckg37] AS [28kg Packets],[pckg38] AS [25kg Packets],[pckg39] AS [20kg Packets],[pckg40] AS [0.4kg Packets],[pckg41] AS [0.2kg Packets]INTO XLSX("Product Wise Report.xlsx",{headers:true}) FROM ?',[$scope.productwisereports]);;
 };
	
	
	$scope.yearwiseReport=function(year){
		$scope.CurrentYear=false;
		$scope.SelectYear=true;
		$window.sessionStorage.year=year;
	ManagementService6.getAllProductReportYearWise($window.sessionStorage.year).then(function(response) {
		if(response != undefined){
			$scope.productwisereportsYearwise = response.data;
			$scope.selectLength=$scope.productwisereportsYearwise.length;
			$scope.SelectYearCount = true;
			$scope.CurrentYearCount = false;
		}
			
		});
	
	
	
	ManagementService6.getAllPacketCountsYearWise($window.sessionStorage.year).then(function(response) {
		if(response != undefined){
			$scope.packetCountsYearwise = response.data;
			//$scope.selectLength=$scope.productwisereportsYearwise.length;
			$scope.SelectYearCount = true;
			$scope.CurrentYearCount = false;
		}
			
		});
	
	$scope.YearlyProductReport = function () {
		//alert();
		alasql('SELECT [productnumber] AS [Product Number ],[productDescription] AS [Product Name ],[sum1] AS [Quantity],[pckg1] AS [1400Kg Packets],[pckg2] AS [1300Kg Packets],[pckg3] AS [1200Kg Packets] ,[pckg4] AS [1100Kg Packets],[pckg5] AS [1000Kg packets],[pckg6] AS [870kg Packets],[pckg7] AS [860kg Packets],[pckg8] AS [810kg Packets],[pckg9] AS [800kg Packets],[pckg10] AS [280kg Packets],[pckg11] AS [272kg Packets],[pckg12] AS [250kg Packets],[pckg13] AS [240kg Packets],[pckg14] AS [230kg Packets],[pckg15] AS [227kg Packets],[pckg16] AS [220kg Packets],[pckg17] AS [210kg Packets],[pckg18] AS [206kg Packets],[pckg19] AS [205kg Packets],[pckg20] AS [204kg Packets],[pckg21] AS [200kg Packets],[pckg22] AS [190kg Packets],[pckg23] AS [188kg Packets],[pckg24] AS [185kg Packets], [pckg25] AS [180kg Packets],[pckg26] AS [175kg Packets],[pckg27] AS [170kg Packets],[pckg28] AS [165kg Packets],[pckg29] AS [160kg Packets],[pckg30] AS [150kg Packets],[pckg31] AS [110kg Packets],[pckg32] AS [100kg Packets],[pckg33] AS [80kg Packets],[pckg34] AS [70kg Packets],[pckg35] AS [60kg Packets],[pckg36] AS [50kg Packets],[pckg37] AS [28kg Packets],[pckg38] AS [25kg Packets],[pckg39] AS [20kg Packets],[pckg40] AS [0.4kg Packets],[pckg41] AS [0.2kg Packets]INTO XLSX("Product Wise Report.xlsx",{headers:true}) FROM ?',[$scope.productwisereports]);;
	 };
	
	
	};
	
	$scope.getDateWiseProductReport=function(productnumber){
		$window.sessionStorage.productnumber=productnumber;
		$state.go('DateWiseProductReport');
	},
	
	$scope.getDateWiseProductReportYearwise=function(productnumber,year){
		$window.sessionStorage.productnumber=productnumber;
		$window.sessionStorage.year=year;
		$state.go('DateWiseProductReports');
	}
		
});

ManagementApp.controller("ManagementController7",function ($rootScope,$window,$scope,$http,$state, ManagementService7) {
	
	$scope.productnumber=$window.sessionStorage.productnumber;
	
	   $scope.Reactor1MT = false;
	   $scope.Reactor5MT = false;
	   $scope.AllReactors = true;
	ManagementService7.getAllDateWiseProductReport($scope.productnumber).then(function(response) {
		if(response != undefined){
			$scope.datewiseproductreports = response.data;
			$scope.allReactorsCount = $scope.datewiseproductreports.length;
			$scope.allReactorsLength = true;
			$scope.all5MTReactorsLength = false;
			$scope.all1MTReactorsLength = false;
		}
		$scope.datewiseProductReport = function () {
			//alert();
			 alasql('SELECT [processOrderProdNumber] AS [Production/Package Order Number],[batchDate] AS [Batch Date ],[productnumber] AS [Product Number],[productDescription] AS [Product name],[sum1] AS [Quantity] ,[completedDate] AS [Completed Date],[reactor] AS [Reactor] ,[leadTime] AS [Lead Time] INTO XLSX("Date Wise Product Report.xlsx",{headers:true}) FROM ?',[$scope.datewiseproductreports]);
	 };
			
		},
		
	$scope.reactor1mt = function(){
		   
		   $scope.Reactor1MT = true;
		   $scope.Reactor5MT = false;
		   $scope.AllReactors = false;
		   ManagementService7.getAll1MTReactors($scope.productnumber).then(function(response) {
				if (response != undefined) {
					$scope.all1MTReactors = response.data;
					$scope.all1MTReactorsCount = $scope.all1MTReactors.length;
					$scope.allReactorsLength = false;
					$scope.all5MTReactorsLength = false;
					$scope.all1MTReactorsLength = true;
				}
				$scope.datewise1MTProductReport = function () {
					//alert();
					 alasql('SELECT [processOrderProdNumber] AS [Production/Package Order Number],[batchDate] AS [Batch Date ],[productnumber] AS [Product Number],[productDescription] AS [Product name],[sum1] AS [Quantity] ,[completedDate] AS [Completed Date],[reactor] AS [Reactor] ,[leadTime] AS [Lead Time] INTO XLSX("Reactor 1MT Products Report.xlsx",{headers:true}) FROM ?',[$scope.all1MTReactors]);
			 };
			})
		   
	   },
	   
	   $scope.reactor5mt = function(){
		   
		   $scope.Reactor1MT = false;
		   $scope.Reactor5MT = true;
		   $scope.AllReactors = false;
		   ManagementService7.getAll5MTReactors($scope.productnumber).then(function(response) {
				if (response != undefined) {
					$scope.all5MTReactors = response.data;
					$scope.all5MTReactorsCount = $scope.all5MTReactors.length;
					$scope.allReactorsLength = false;
					$scope.all5MTReactorsLength = true;
					$scope.all1MTReactorsLength = false;
				}
			})
			$scope.datewise5MTProductReport = function () {
				//alert();
				 alasql('SELECT [processOrderProdNumber] AS [Production/Package Order Number],[batchDate] AS [Batch Date ],[productnumber] AS [Product Number],[productDescription] AS [Product name],[sum1] AS [Quantity] ,[completedDate] AS [Completed Date],[reactor] AS [Reactor] ,[leadTime] AS [Lead Time] INTO XLSX("Reactor 5MT Products Report.xlsx",{headers:true}) FROM ?',[$scope.all5MTReactors]);
		 };
		   
	   })
	
		
});

ManagementApp.controller("ManagementController11",function ($rootScope,$window,$scope,$http,$state, ManagementService11) {
	
	$scope.productnumber=$window.sessionStorage.productnumber;
	$scope.year=$window.sessionStorage.year;
	
	   $scope.Reactor1MTs = false;
	   $scope.Reactor5MTs = false;
	   $scope.AllReactorss = true;
	ManagementService11.getAllDateWiseProductReportYearly($scope.productnumber,$scope.year).then(function(response) {
		if(response != undefined){
			$scope.datewiseproductreportss = response.data;
			$scope.allReactorsCounts = $scope.datewiseproductreportss.length;
			$scope.allReactorsLengths = true;
			$scope.all5MTReactorsLengths = false;
			$scope.all1MTReactorsLengths = false;
		}
		$scope.datewiseProductReports = function () {
			//alert();
			 alasql('SELECT [processOrderProdNumber] AS [Production/Package Order Number],[batchDate] AS [Batch Date ],[productnumber] AS [Product Number],[productDescription] AS [Product name],[sum1] AS [Quantity] ,[completedDate] AS [Completed Date],[reactor] AS [Reactor] ,[completedTime] AS [Lead Time] INTO XLSX("Date Wise Product Reports.xlsx",{headers:true}) FROM ?',[$scope.datewiseproductreportss]);
	 };
			
		},
		
	$scope.reactor1mts = function(){
		   
		   $scope.Reactor1MTs = true;
		   $scope.Reactor5MTs = false;
		   $scope.AllReactorss = false;
		   ManagementService11.getAll1MTReactorsYearly($scope.productnumber,$scope.year).then(function(response) {
				if (response != undefined) {
					$scope.all1MTReactorss = response.data;
					$scope.all1MTReactorsCounts = $scope.all1MTReactorss.length;
					$scope.allReactorsLengths = false;
					$scope.all5MTReactorsLengths = false;
					$scope.all1MTReactorsLengths = true;
				}
			})
			$scope.datewise1MTProductReports = function () {
				//alert();
				 alasql('SELECT [processOrderProdNumber] AS [Production/Package Order Number],[batchDate] AS [Batch Date ],[productnumber] AS [Product Number],[productDescription] AS [Product name],[sum1] AS [Quantity] ,[completedDate] AS [Completed Date],[reactor] AS [Reactor] ,[completedTime] AS [Lead Time] INTO XLSX("Reactor 1MT Products Reports.xlsx",{headers:true}) FROM ?',[$scope.all1MTReactorss]);
		 };
		   
	   },
	   
	   $scope.reactor5mts = function(){
		   
		   $scope.Reactor1MTs = false;
		   $scope.Reactor5MTs = true;
		   $scope.AllReactorss = false;
		   ManagementService11.getAll5MTReactorsYearly($scope.productnumber,$scope.year).then(function(response) {
				if (response != undefined) {
					$scope.all5MTReactorss = response.data;
					$scope.all5MTReactorsCounts = $scope.all5MTReactorss.length;
					$scope.allReactorsLengths = false;
					$scope.all5MTReactorsLengths = true;
					$scope.all1MTReactorsLengths = false;
				}
			})
			$scope.datewise5MTProductReports = function () {
				//alert();
				 alasql('SELECT [processOrderProdNumber] AS [Production/Package Order Number],[batchDate] AS [Batch Date ],[productnumber] AS [Product Number],[productDescription] AS [Product name],[sum1] AS [Quantity] ,[completedDate] AS [Completed Date],[reactor] AS [Reactor] ,[completedTime] AS [Lead Time] INTO XLSX("Reactor 5MT Products Reports.xlsx",{headers:true}) FROM ?',[$scope.all5MTReactorss]);
		 };
	   })
	
		
});




ManagementApp.controller("ManagementController8",function ($scope,$http,$state, ManagementService8) {
	/*ManagementService8.currentQuaterProductionPlan().then(function(response) {
		if(response != undefined){
			$scope.quarterProductionPlan = response.data;
		}
			
		})*/
		ManagementService8.fullQuaterProductionPlan().then(function(response) {
		if(response != undefined){
			$scope.fullquarterProductionPlan = response.data;
			
		}
			
		})
	});

ManagementApp.controller("AllOrdersController", function($scope, $state, Flash,
		managementAllOrdersService, $location) {

	$scope.showTodays = false;
	$scope.showAll = true;
	$scope.showImg = false;
	// $scope.pendingRm = false;

	managementAllOrdersService.getAllOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.allOrders = response.data.data;

			if ($scope.allOrders.length == "0") {
				$scope.showImg = true;

			} else if ($scope.allOrders.length != "0") {
				$scope.showImg = false;

			}
		}

	});

	managementAllOrdersService.getTodaysOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.todaysOrders = response.data.data;
		}

	});

	managementAllOrdersService.getPastOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.pastOrders = response.data.data;
		}

	});
	managementAllOrdersService.getFutureOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.futureOrders = response.data.data;
		}

	});


	managementAllOrdersService.getTodaysCompletedOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.completedOrders = response.data.data;
		}

	});


	$scope.all = function() {

		$scope.showTodays = false;
		$scope.showAll = true;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = false;

		if ($scope.allOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.allOrders.length != "0") {
			$scope.showImg = false;

		}

	}

	$scope.todays = function() {
		$scope.showTodays = true;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = false;

		if ($scope.todaysOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.todaysOrders.length != "0") {
			$scope.showImg = false;

		}
	}
	
	$scope.past = function() {
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = true;
		$scope.showFuture = false;
		$scope.showCompleted = false;

		if ($scope.pastOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.pastOrders.length != "0") {
			$scope.showImg = false;

		}

	}

	$scope.future = function() {
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showCompleted = false;
		$scope.showFuture = true;

		if ($scope.futureOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.futureOrders.length != "0") {
			$scope.showImg = false;

		}

	}
	
	$scope.finished = function() {
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = true;

		if ($scope.completedOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.completedOrders.length != "0") {
			$scope.showImg = false;

		}

	}
	
	$scope.allStatus = function(processorderProdNumber, approved) {
		//alert(approved);
		$scope.orderNumber = processorderProdNumber;
		//$scope.currentStatus = status;
		$scope.reWorked = approved;
	
	/*$scope.allStatus = function(processorderProdNumber) {
		$scope.orderNumber = processorderProdNumber;
		// alert('controller');
*/
		managementAllOrdersService.getStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.status = response.data.data;
					}

				});
		
		managementAllOrdersService.getChargedStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.chargedStatus = response.data.data;
					}

				});
		managementAllOrdersService.getPackedStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.packedStatus = response.data.data;
					}

				});
		managementAllOrdersService.getQualityCheckStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.qualityCheckStatus = response.data.data;
					}

				});
		managementAllOrdersService.getQualityInspectionStatus(processorderProdNumber)
		.then(function(response) {
			if (response.data !== 'undefined') {
				$scope.qualityInspectionStatus = response.data.data;
				// $scope.productLength = $scope.productOrders.length;
			}

		});

		managementAllOrdersService.getQualityInspectionStatus(processorderProdNumber)
		.then(function(response) {
			if (response.data !== 'undefined') {
				$scope.qualityInspectionStatus = response.data.data;
			}

		});
		managementAllOrdersService.getInventoryStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.inventoryStatus = response.data.data;
					}

				});

		managementAllOrdersService.getRmInventoryStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.rmInventoryStatus = response.data.data;
					}

				});

		// alert(number);
	}

});

ManagementApp.controller("CycleTimeReportController", function($scope, $state, 
		managementCycleTimeReportService) {

	$scope.showCurrentWeek = true;
	$scope.showCurrentMonth = false;
	$scope.showCurrentQuarter = false;
	$scope.showCurrentYear = false;
	//$scope.showImg = false;
	var currentYear=new Date();
	var year=currentYear.getFullYear();
	$scope.curYear=year;
	// $scope.pendingRm = false;
	
	managementCycleTimeReportService.getAllWeeklyCTReports().then(function(response) {
		if(response != undefined){
			$scope.weeklyCTReports = response.data;
			$scope.avgCompletedTime = $scope.weeklyCTReports.averageCompletedTime;
			 $scope.avgChargingTime = $scope.weeklyCTReports.averageChargingTime;
			 $scope.avgQualityTime = $scope.weeklyCTReports.averageQualityTime;
			 $scope.avgPackingTime = $scope.weeklyCTReports.averagePackingTime;
			
		}
		$scope.CTDeatilsForPO =function(){
			$state.go('CTRDetails');
		}
			
		});
	
	$scope.week = function() {
		$scope.showCurrentWeek = true;
		$scope.showCurrentMonth = false;
		$scope.showCurrentQuarter = false;
		$scope.showCurrentYear = false;
		managementCycleTimeReportService.getAllWeeklyCTReports().then(function(response) {
			if (response != undefined ) {
				$scope.weeklyCTReports = response.data;
				 $scope.avgCompletedTime = $scope.weeklyCTReports.averageCompletedTime;
				 $scope.avgChargingTime = $scope.weeklyCTReports.averageChargingTime;
				 $scope.avgQualityTime = $scope.weeklyCTReports.averageQualityTime;
				 $scope.avgPackingTime = $scope.weeklyCTReports.averagePackingTime;
			}
		});
		$scope.CTDeatilsForPO =function(){
			$state.go('CTRDetails');
		}
		
		}
	
	$scope.month = function() {
		$scope.showCurrentWeek = false;
		$scope.showCurrentMonth = true;
		$scope.showCurrentQuarter = false;
		$scope.showCurrentYear = false;
		managementCycleTimeReportService.getAllMonthlyCTReports().then(function(response) {
			if (response != undefined ) {
				$scope.weeklyCTReports = response.data;
				 $scope.avgCompletedTime = $scope.weeklyCTReports.averageCompletedTime;
				 $scope.avgChargingTime = $scope.weeklyCTReports.averageChargingTime;
				 $scope.avgQualityTime = $scope.weeklyCTReports.averageQualityTime;
				 $scope.avgPackingTime = $scope.weeklyCTReports.averagePackingTime;
			}
		});
		$scope.CTDeatilsForPOMonth =function(){
			$state.go('CTRDetailsForMonth');
		}
		},
	
	$scope.quarter = function() {
		$scope.showCurrentWeek = false;
		$scope.showCurrentMonth = false;
		$scope.showCurrentQuarter = true;
		$scope.showCurrentYear = false;
		managementCycleTimeReportService.getAllQuarterlyCTReports().then(function(response) {
			if (response != undefined ) {
				$scope.weeklyCTReports = response.data;
				 $scope.avgCompletedTime = $scope.weeklyCTReports.averageCompletedTime;
				 $scope.avgChargingTime = $scope.weeklyCTReports.averageChargingTime;
				 $scope.avgQualityTime = $scope.weeklyCTReports.averageQualityTime;
				 $scope.avgPackingTime = $scope.weeklyCTReports.averagePackingTime;
			}
		 });
		
		$scope.CTDeatilsForPOQuarter =function(){
			$state.go('CTRDetailsForQuarter');
		}
		},
	
	$scope.year = function() {
		$scope.showCurrentWeek = false;
		$scope.showCurrentMonth = false;
		$scope.showCurrentQuarter = false;
		$scope.showCurrentYear = true;
		
		managementCycleTimeReportService.getAllYearlyCTReports().then(function(response) {
			if (response != undefined ) {
				$scope.weeklyCTReports = response.data;
				 $scope.avgCompletedTime = $scope.weeklyCTReports.averageCompletedTime;
				 $scope.avgChargingTime = $scope.weeklyCTReports.averageChargingTime;
				 $scope.avgQualityTime = $scope.weeklyCTReports.averageQualityTime;
				 $scope.avgPackingTime = $scope.weeklyCTReports.averagePackingTime;
			}
		});
		
		$scope.CTDeatilsForPOYear =function(){
			$state.go('CTRDetailsForYear');
		}
		
	}
		
	//add next controllers here
	
});


ManagementApp.controller("CycleTimeReportDetailsController",function ($rootScope,$window,$scope,$http,$state, CycleTimeReportDetailsService) {
	
//Weekly--------------	 
	CycleTimeReportDetailsService.getAllCTDetails().then(function(response) {
		if(response != undefined){
			$scope.CTdetails = response.data;
		}
			
	   });
	   
	   $scope.CTModalDeatilsForPOWeek =function(processOrderNumber){
	   
	   CycleTimeReportDetailsService.getAllCTModalDetails(processOrderNumber).then(function(response) {
			if(response != undefined){
				$scope.CTModaldetails = response.data;
				$scope.processorderno=processOrderNumber;
			}
				
		   });
	   }
	   $scope.WeeklyReportDownload = function () {
			//alert();
			 alasql('SELECT [processOrderNumber] AS [Process Order Number ],[productNumber] AS [Product Number ],[productDescription] AS [Product Name ],[processingDate] AS [Process Order Date],[chargingTime] AS [Charging Time(min)],[qualityTime] AS [Quality Time(min)] ,[packingTime] AS [Packaging Time (min)],[completedTime] AS [Lead Time(min)] INTO XLSX("Weekly Cycle Time Report.xlsx",{headers:true}) FROM ?',[$scope.CTdetails]);
	 };
		
//Monthly-----------------------------------------------	   
	   CycleTimeReportDetailsService.getAllCTDetailsForMonth().then(function(response) {
			if(response != undefined){
				$scope.CTdetailsforMonth = response.data;
			}
				
		   });
	   
	   $scope.CTModalDeatilsForPOMonth =function(processOrderNumber){
	   CycleTimeReportDetailsService.getAllCTModalDetailsForMonth(processOrderNumber).then(function(response) {
			if(response != undefined){
				$scope.CTModaldetailsforMonth = response.data;
				$scope.processorderno=processOrderNumber;
			}
				
		   });
	   }
	   $scope.MonthlyReportDownload = function () {
			//alert();
			 alasql('SELECT [processOrderNumber] AS [Process Order Number ],[productNumber] AS [Product Number ],[productDescription] AS [Product Name ],[processingDate] AS [Process Order Date],[chargingTime] AS [Charging Time(min)],[qualityTime] AS [Quality Time(min)] ,[packingTime] AS [Packaging Time (min)],[completedTime] AS [Lead Time(min)]  INTO XLSX("Monthly Cycle Time Report.xlsx",{headers:true}) FROM ?',[$scope.CTdetailsforMonth]);
	 };

	 
//----------------------------------------------------------------------------------
	   CycleTimeReportDetailsService.getAllCTDetailsForQuarter().then(function(response) {
			if(response != undefined){
				$scope.CTdetailsforQuarter = response.data;
			}
				
		   });
	   $scope.CTModalDeatilsForPOQuarter =function(processOrderNumber){
	   CycleTimeReportDetailsService.getAllCTModalDetailsForQuarter(processOrderNumber).then(function(response) {
			if(response != undefined){
				$scope.CTModaldetailsforQuarter = response.data;
				$scope.processorderno=processOrderNumber;
			}
				
		   });
	   }
	   $scope.QuarterlyReportDownload = function () {
			//alert();
			 alasql('SELECT [processOrderNumber] AS [Process Order Number ],[productNumber] AS [Product Number ],[productDescription] AS [Product Name ],[processingDate] AS [Process Order Date],[chargingTime] AS [Charging Time(min)],[qualityTime] AS [Quality Time(min)] ,[packingTime] AS [Packaging Time (min)],[completedTime] AS [Lead Time(min)]  INTO XLSX("Quarterly Cycle Time Report.xlsx",{headers:true}) FROM ?',[$scope.CTdetailsforQuarter]);
	 };
		

//Quarterly--------------------------------------------------------------------------	 
	 CycleTimeReportDetailsService.getAllCTDetailsForYear().then(function(response) {
			if(response != undefined){
				$scope.CTdetailsforYear = response.data;
			}
				
		   });
	   $scope.CTModalDeatilsForPOYear =function(processOrderNumber){
		  // $window.sessionStorage.processOrderNumber=processOrderNumber;
	   CycleTimeReportDetailsService.getAllCTModalDetailsForYear(processOrderNumber).then(function(response) {
			if(response != undefined){
				$scope.CTModaldetailsforYear = response.data;
				$scope.processorderno=processOrderNumber;
			}
				
		   });
	   }
	   $scope.YearlyReportDownload = function () {
			//alert();
			 alasql('SELECT [processOrderNumber] AS [Process Order Number ],[productNumber] AS [Product Number ],[productDescription] AS [Product Name ],[processingDate] AS [Process Order Date],[chargingTime] AS [Charging Time(min)],[qualityTime] AS [Quality Time(min)] ,[packingTime] AS [Packaging Time (min)],[completedTime] AS [Lead Time(min)]  INTO XLSX("Yearly Cycle Time Report.xlsx",{headers:true}) FROM ?',[$scope.CTdetailsforYear]);
	 };
		
});


ManagementApp.controller("RepackReportController",function ($scope,$http,$state,$window,$rootScope, RepackReportService) {
	
	$scope.CurrentYearRepackReport=true;
	$scope.SelectYearRepackReport=false;
	 var currentYear= new Date();
	 
	 RepackReportService.getAllRepackReports().then(function(response) {
		if(response != undefined){
			$scope.repackreports = response.data;
			$scope.currentlength=$scope.repackreports.length;
			var year=currentYear.getFullYear();
			$scope.curYear=year;
			$scope.SelectYearRepackCount = false;
			$scope.CurrentYearRepackCount = true;
			
			
		}
			
		});
	
	$scope.RepackReport = function () {
		//alert();
		 alasql('SELECT [processOrderPackageNumber] AS [ProcessOrder Package Number],[sixMlNumber] AS [SixMl Number],[productNumber] AS [Product Number],[productName] AS [Product Name] ,[batchDate] AS [Batch Date],[totalQuantity] AS [Quantity Produced] ,[packetSize] AS [Wght_Pckg],[totalPacketCounts] AS [Total Quantity]INTO XLSX("Repack Report.xlsx",{headers:true}) FROM ?',[$scope.repackreports]);
 };
	
	
	$scope.yearwiseRepackReport=function(year){
		$scope.CurrentYearRepackReport=false;
		$scope.SelectYearRepackReport=true;
		$window.sessionStorage.year=year;
		RepackReportService.getAllRepackReportYearWise($window.sessionStorage.year).then(function(response) {
		if(response != undefined){
			$scope.repackReportsYearwise = response.data;
			$scope.selectLength=$scope.repackReportsYearwise.length;
			$scope.SelectYearRepackCount = true;
			$scope.CurrentYearRepackCount = false;
		}
			
		});
	
	$scope.YearlyRepackReport = function () {
		//alert();
		 alasql('SELECT [processOrderPackageNumber] AS [ProcessOrder Package Number],[sixMlNumber] AS [SixMl Number],[productNumber] AS [Product Number],[productName] AS [Product Name] ,[batchDate] AS [Batch Date],[totalQuantity] AS [Quantity Produced] ,[packetSize] AS [Wght_Pckg],[totalPacketCounts] AS [Total Quantity]INTO XLSX("Repack Report.xlsx",{headers:true}) FROM ?',[$scope.repackReportsYearwise]);
	 };
	
	};
	
});


