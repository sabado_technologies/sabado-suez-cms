/*Share Service*/
ManagementApp.factory('shareService', function() {
	var savedData = {};
	function set(data) {
		savedData = data;
	}

	function get() {
		return savedData;
	}

	return {
		set : set,
		get : get
	}
});



ManagementApp.factory('ManagementService1', function($http) {
	return {
		getAllDailyProcessOrderDetails: function(){
			return $http.get('/suez/Management/dailyProcessOrders',{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				////alert('error');
			});
		},
		getAllStatusInfo : function() {
			return $http.get('/suez/pending/getAllStatusInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getProcessHistory : function() {
			return $http.get(
					'/suez/pending/getProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},		
		getProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getProcessOrdersForPostingCheck/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						////alert('error');
					});
		} ,
		associatedPackingHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/associatedPackingInfoForPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						////alert('error');
					});
		},
		getRepackProcessHistory : function() {
			return $http.get(
					'/suez/pending/getRepackingProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},
		getRepackProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventoryPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						////alert('error');
					});
		},
		
		updateExtraRm : function(process,rmInfo) {
			return $http.post('/suez/pending/addExtraRawMaterial/'+ process+'/'+rmInfo, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				//console.log('error')
				////alert('error');
			});
		},
		getPackinVesselHrs : function(process) {
			return $http.get('/suez/Management/cycleTimeForModal/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		getRepackPackinVesselHrs : function(process) {
			return $http.get('/suez/pending/repackingHours/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
	}
});

ManagementApp.factory('ManagementService15', function($http,$rootScope) {
	return {
		currentQuaterDaywiseProductionReport: function(){
			return $http.get('/suez/Management/DayWiseProductionReport',{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				////alert('error');
			});
		},
	
		SelectQuaterDaywiseProductionReport:function(quarter){
		return $http.get('/suez/Management/DailyProductionReport/' +quarter,{
			'Content-Type':'application/json'
		}).then(function(response){
			return response;
		},function(response){
			////alert('error');
		});
	}
	}
});                  


ManagementApp.factory('ManagementService2', function($http) {
	return {
		
		getAllWeeklyOrderDetails: function(){
			return $http.get('/suez/Management/weeklyProcessOrders',{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				////alert('error');
			});
					
		},
	getAllPlannedProduction: function(){
		return $http.get('/suez/Management/PlannedWeeklyProduction',{
			'Content-Type':'application/json'
		}).then(function(response){
			return response;
		},function(response){
			////alert('error');
		});
				
	}
	}
});

ManagementApp.factory('ManagementService3', function($http) {
	return {
		
		currentQuaterProductionReport : function(){
			return $http.get('/suez/Management/currentQuaterProductionReport' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				////alert('error');
			});
		}
		
	}
});

ManagementApp.factory('ManagementService4', function($http) {
	return {
		
		getPastWorkingDaysCount : function(){
			return $http.get('/suez/Management/AskingRate' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				////alert('error');
			});
		},
		
		currentQuaterProductionPlan : function(){
			return $http.get('/suez/Management/currentQuaterProductionPlan' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		}
	}
});

ManagementApp.factory('ManagementService5', function($http) {
	return {
		getCurrentYearMonthWiseProductionReport : function(){
			/*//alert("get current year month wise production");
			return $http.get('/suez/Management/MonthlyProductionProductReports' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});*/
		}
	}
});






ManagementApp.factory('ManagementService6', function($http,$rootScope) {
	return {
		getAllProductWiseReports: function(){
			return $http.get('/suez/Management/productwiseReport',{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
	
	getAllProductReportYearWise:function(year){
		return $http.get('/suez/Management/productwiseReport/' +year,{
			'Content-Type':'application/json'
		}).then(function(response){
			return response;
		},function(response){
			//alert('error');
		});
	},
	
	getAllPacketSizeCounts:function(){
		return $http.get('/suez/Management/totalPacketCounts',{
			'Content-Type':'application/json'
		}).then(function(response){
			return response;
		},function(response){
			//alert('error');
		});
	},
	
	getAllPacketCountsYearWise:function(year){
		return $http.get('/suez/Management/PacketSizeCountsForYear/' +year,{
			'Content-Type':'application/json'
		}).then(function(response){
			return response;
		},function(response){
			//alert('error');
		});
	}
	
	
	}
});                  

ManagementApp.factory('ManagementService7', function($http,$rootScope) {
	return {
		
		getAllDateWiseProductReport: function(productnumber){
			return $http.get('/suez/Management/datewiseProductReport/' + productnumber,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
		getAll1MTReactors : function(productnumber) {
			return $http.get('/suez/Management/getAll1MTReactors/' + productnumber,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
		
		getAll5MTReactors : function(productnumber) {
			return $http.get('/suez/Management/getAll5MTReactors/' + productnumber,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
	}
	}
});


ManagementApp.factory('ManagementService11', function($http,$rootScope) {
	return {
		
		getAllDateWiseProductReportYearly: function(productnumber,year){
			return $http.get('/suez/Management/dateWiseProductReportYearwise/'
					+ productnumber +"/"+ year,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
		getAll1MTReactorsYearly : function(productnumber,year) {
			return $http.get('/suez/Management/getAll1MTReactors/' + productnumber +"/"+ year,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
		
		getAll5MTReactorsYearly : function(productnumber,year) {
			return $http.get('/suez/Management/getAll5MTReactors/' + productnumber +"/"+ year,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
	}
	}
});



ManagementApp.factory('ManagementService8', function($http) {
	return {
		
		/*currentQuaterProductionPlan : function(){
			return $http.get('/suez/Management/currentQuaterProductionPlan' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		}*/
		
		fullQuaterProductionPlan : function(){
			return $http.get('/suez/Management/fullQuaterProductionPlan' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		}
	}
});


ManagementApp.factory('managementAllOrdersService', function($http, $q ) {
	
	
	return {

		getAllOrders : function() {
			return $http.get('/suez/pending/getAllOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getReactors : function() {
			return $http.get('/suez/pending/getReactors').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysOrders : function() {
			return $http.get('/suez/pending/getTodaysOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getPastOrders : function() {
			return $http.get('/suez/pending/getPreviousOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getFutureOrders : function() {
			return $http.get('/suez/pending/getFutureOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysCompletedOrders : function() {
			return $http.get('/suez/pending/getTodaysCompletedOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getStatus/' + processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getChargedStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getChargedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getPackedStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getPackedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityCheckStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getQualityCheckStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityInspectionStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getQualityInspectionStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getInventoryStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
		
		getRmInventoryStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getRmInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
		
	}

});

ManagementApp.factory('managementCycleTimeReportService', function($http) {
	return {
		
		getAllWeeklyCTReports : function(){
			return $http.get('/suez/Management/aveargeCycleTimeForWeek' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
		
		getAllMonthlyCTReports : function(){
			return $http.get('/suez/Management/aveargeCycleTimeForMonth' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
		
		getAllQuarterlyCTReports : function(){
			return $http.get('/suez/Management/aveargeCycleTimeForQuarter' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
		getAllYearlyCTReports : function(){
			return $http.get('/suez/Management/aveargeCycleTimeForYear' ,{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		}
		
	}
});

ManagementApp.factory('CycleTimeReportDetailsService', function($http, $q ) {
	
	
return {
        
		getAllCTDetails : function() {
			return $http.get('/suez/Management/cycleTimeForWeek').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getAllCTModalDetails : function(processOrderNumber) {
			return $http.get('/suez/Management/cycleTimeForModal/' +processOrderNumber,
					{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				return $q.reject(response);
			});
		},
//------------------------------------------------------------------------------		
		getAllCTDetailsForMonth : function() {
			return $http.get('/suez/Management/cycleTimeForMonth').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getAllCTModalDetailsForMonth: function(processOrderNumber) {
			return $http.get('/suez/Management/cycleTimeForModal/' +processOrderNumber,
					{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				return $q.reject(response);
			});
		},
		
//--------------------------------------------------------------------------------------------------
		getAllCTDetailsForQuarter : function() {
			return $http.get('/suez/Management/cycleTimeForQuarter').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getAllCTModalDetailsForQuarter : function(processOrderNumber) {
			return $http.get('/suez/Management/cycleTimeForModal/' +processOrderNumber,
					{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				return $q.reject(response);
			});
		},
		
//---------------------------------------------------------------------------------------
		getAllCTDetailsForYear : function() {
			return $http.get('/suez/Management/cycleTimeForYear').then(
					function(response) {
						
						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getAllCTModalDetailsForYear : function(processOrderNumber) {
			return $http.get('/suez/Management/cycleTimeForModal/' +processOrderNumber,
					{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				return $q.reject(response);
			});
		}
		
	}
});

ManagementApp.factory('RepackReportService', function($http,$rootScope) {
	return {
		getAllRepackReports: function(){
			return $http.get('/suez/Management/RepackingReport',{
				'Content-Type':'application/json'
			}).then(function(response){
				return response;
			},function(response){
				//alert('error');
			});
		},
	
		getAllRepackReportYearWise:function(year){
		return $http.get('/suez/Management/YearwiseRepackingReport/' +year,{
			'Content-Type':'application/json'
		}).then(function(response){
			return response;
		},function(response){
			//alert('error');
		});
	},
	
	getAllRepackingStatusInfo : function() {
		return $http.get('/suez/pending/getAllRepackingStatusInfo').then(
				function(response) {

					return response;

				}, function(response) {
					// something went wrong
					return $q.reject(response);
				});
	}
	
	}
});                  

