$(document).ready(function() {
	//alert("document ready");
 $.ajax({
    url: '/suez/Management/currentQuaterProductionReport',
    type: 'GET',
    async: true,
    dataType: "json",
    success: function (data) {
        visitorData(data);
        var actual = data.actualProductionPer;
        var remaining = "";
    } 
  });
 });
function visitorData(data){
	var productInfo = data.productInfos
	// Create the chart
	Highcharts.chart('current', {
	     credits: {
	    enabled: false
	  },
	    chart: {
	        type: 'pie'
	    },
	    title: {
	        text: 'Production report of current quarter'
	    },
	    subtitle: {
	       
	    },
	    plotOptions: {
	        series: {
	            dataLabels: {
	                enabled: true,
	            }
	        }
	    },
	    tooltip: {
	    	headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            //MT format for the lower drilldowns
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}MT</b> of total<br/>'
	    },
	    series: [{
	    	tooltip: {
	            //Pointformat for the percentage series
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>' 
	        },
	        name: 'Planned Production',
	        colorByPoint: true,
	        data: [{
	            name: 'Remaining Production Qty. ',
	            y: data.remainingPercentage,
	        },
	         {
	            name: 'Actual Production Qty.',
	            y: data.actualPercentage,
	            drilldown: 'ap'
	        }
	        ]
	    }],
	    drilldown: {
	        series: [{
	            name: 'production report',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}MT</b> of total<br/>',
	            id: 'po',
	            data: [
	                
	            ]
	        }, {
	            name: 'Actual Production Qty.',
	            id: 'ap',
	            data: data.productInfos
	            
	        }  ]
	    }
	}); 

}
