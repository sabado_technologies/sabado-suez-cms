$(document).ready(function() {
	//alert("document ready");
 $.ajax({
    url: '/suez/Management/MonthlyProductionProductReports',
    type: 'GET',
    async: true,
    dataType: "json",
    success: function (data) {
    	//alert("visitor data");
        visitorData(data);
        console.log(data);
    }
  });
 });

function visitorData(dataVal){
	//alert("called");
	console.log("*********visitor data**");
	console.log(dataVal);
	console.log(dataVal.planned);
	var planned = dataVal.planned;
	var actuals = dataVal.actual;
	console.log(actuals);
	console.log(planned);
	
	
	console.log([60, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]);
	console.log("*********visitor data**");
	Highcharts.chart('container', {
	    credits:{
	enable:false

	    },




	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    subtitle: {
	      
	    },
	    xAxis: {
	        categories: [
	            'Jan','Feb','Mar','Apr','May','Jun',
	            'Jul','Aug','Sep','Oct','Nov','Dec'
	        ],
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Production (MT)'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>    ',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.1,
	            borderWidth: 0
	        }
	    },
	    series: [{
	        name: 'Planned Production',
	        data: planned

	    },  {
	        name: 'Actual Production',
	        data: actuals

	    }]
	});
}
