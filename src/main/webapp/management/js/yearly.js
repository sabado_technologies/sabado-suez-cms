$(document).ready(function() {
	var currentYear= new Date();
	var currentYear=currentYear.getFullYear();
$.ajax({
		//data:data,
	    url: '/suez/Management/barchartReports/'+currentYear ,    
	    type: 'GET',
	    async: true,
	    dataType: "json",
	    success: function (data) {
	    	//alert("visitor data");
	        visitorData(data);
	    }
	  });
	 
$('#year').change(function(){
	var year=$('#year').val();
  var data='year='+year;
 $.ajax({
	//data:data,
    url: '/suez/Management/barchartReports/'+year ,    
    type: 'GET',
    async: true,
    dataType: "json",
    success: function (data) {
    	//alert("visitor data");
        visitorData(data);
        
    	//alert(year_id);
    }
  });
 });


});
function visitorData(dataVal){
	//alert("called");
	//alert(dataVal.response[0].weekwisedata[12].weeklyPlannedProduction);
	
	var weeklyData = dataVal.weeklyData;
	 var quarterData = dataVal.quarterlyData;
	var  planned = dataVal.plannedData;

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Yearly Production Report'
    },
    credits : {
        enabled: false
    },
    /*subtitle: {
        text: 'Click columns to drill down to single series. Click categories to drill down both.'
    },*/
    xAxis: {
        type: 'category'
    },

    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true
            }
        }
    },

    series: [{
        name: 'Planned Quantity',
        data: [{
            name: '1st Quarter',
            y: parseFloat(dataVal.response[0].plannedQuarterProduction),
            drilldown: 'Quarter1p'
        },
        {
            name: '2nd Quarter',
            y: parseFloat(dataVal.response[1].plannedQuarterProduction),
            drilldown: 'Quarter2p'
        }
        ,{
            name: '3rd Quarter',
            y:parseFloat(dataVal.response[2].plannedQuarterProduction),
            drilldown: 'Quarter3p'
        },
        {
            name: '4th Quarter',
            y: parseFloat(dataVal.response[3].plannedQuarterProduction),
            drilldown: 'Quarter4p'
        }]
    }, {
        name: 'Actual Quantity',
        data: [{
            name: '1st Quarter',
            y:parseFloat(dataVal.response[0].actualQuarterProduction) ,
            drilldown: 'Quarter1a'
        },
        {
            name: '2nd Quarter',
            y: parseFloat(dataVal.response[1].actualQuarterProduction),
            drilldown: 'Quarter2a'
		}, 
		{
			name : '3rd Quarter',
			y : parseFloat(dataVal.response[2].actualQuarterProduction),
			drilldown : 'Quarter3a'
		},
		{
            name: '4th Quarter',
            y: parseFloat(dataVal.response[3].actualQuarterProduction),
            drilldown: 'Quarter4a'
        }
        ]
    }],
    drilldown: {
        series: [{
            id: 'Quarter1a',
                name: 'Actual Quality ',
                data: [{
                    name: 'Week1',
                    y:parseFloat(dataVal.response[0].weekwisedata[0].weeklyActualProduction)
                },
                {
                    name: 'Week2',
                    y:parseFloat(dataVal.response[0].weekwisedata[1].weeklyActualProduction)
                },
                {
                    name: 'Week3',
                    y:parseFloat(dataVal.response[0].weekwisedata[2].weeklyActualProduction)
                },
                {
                    name: 'Week4',
                    y:parseFloat(dataVal.response[0].weekwisedata[3].weeklyActualProduction)
                },
                {
                    name: 'Week5',
                    y:parseFloat(dataVal.response[0].weekwisedata[4].weeklyActualProduction)
                },
                {
                    name: 'Week6',
                    y:parseFloat(dataVal.response[0].weekwisedata[5].weeklyActualProduction)
                },
                {
                    name: 'Week7',
                    y:parseFloat(dataVal.response[0].weekwisedata[6].weeklyActualProduction)
                },
                {
                    name: 'Week8',
                    y:parseFloat(dataVal.response[0].weekwisedata[7].weeklyActualProduction)
                },
                {
                    name: 'Week9',
                    y:parseFloat(dataVal.response[0].weekwisedata[8].weeklyActualProduction)
                },
                {
                    name: 'Week10',
                    y:parseFloat(dataVal.response[0].weekwisedata[9].weeklyActualProduction)
                },
                {
                    name: 'Week11',
                    y:parseFloat(dataVal.response[0].weekwisedata[10].weeklyActualProduction)
                },
                {
                    name: 'Week12',
                    y:parseFloat(dataVal.response[0].weekwisedata[11].weeklyActualProduction)
                },
                {
                    name: 'Week13',
                    y:parseFloat(dataVal.response[0].weekwisedata[12].weeklyActualProduction)
                }
            ]
        }, {
            id: 'Quarter2a',
            name: 'Actual Quality ',
            data: [
                {
                    name: 'Week14',
                    y:parseFloat(dataVal.response[1].weekwisedata[0].weeklyActualProduction)
                },
                {
                    name: 'Week15',
                    y:parseFloat(dataVal.response[1].weekwisedata[1].weeklyActualProduction)
                },
                {
                    name: 'Week16',
                    y:parseFloat(dataVal.response[1].weekwisedata[2].weeklyActualProduction)
                },
                {
                    name: 'Week17',
                    y:parseFloat(dataVal.response[1].weekwisedata[3].weeklyActualProduction)
                },
                {
                    name: 'Week18',
                    y:parseFloat(dataVal.response[1].weekwisedata[4].weeklyActualProduction)
                },
                {
                    name: 'Week19',
                    y:parseFloat(dataVal.response[1].weekwisedata[5].weeklyActualProduction)
                },
                {
                    name: 'Week20',
                    y:parseFloat(dataVal.response[1].weekwisedata[6].weeklyActualProduction)
                },
                {
                    name: 'Week21',
                    y:parseFloat(dataVal.response[1].weekwisedata[7].weeklyActualProduction)
                },
                {
                    name: 'Week22',
                    y:parseFloat(dataVal.response[1].weekwisedata[8].weeklyActualProduction)
                },
                {
                    name: 'Week23',
                    y:parseFloat(dataVal.response[1].weekwisedata[9].weeklyActualProduction)
                },
                {
                    name: 'Week24',
                    y:parseFloat(dataVal.response[1].weekwisedata[10].weeklyActualProduction)
                },
                {
                    name: 'Week25',
                    y:parseFloat(dataVal.response[1].weekwisedata[11].weeklyActualProduction)
                },
                {
                    name: 'Week26',
                    y:parseFloat(dataVal.response[1].weekwisedata[12].weeklyActualProduction)
                }
            ]
        }, {
            id: 'Quarter3a',
            name: 'Actual Quality ',
            data: [
                {
                    name: 'Week27',
                    y:parseFloat(dataVal.response[2].weekwisedata[0].weeklyActualProduction)
                },
                {
                    name: 'Week28',
                    y:parseFloat(dataVal.response[2].weekwisedata[1].weeklyActualProduction)
                },
                {
                    name: 'Week29',
                    y:parseFloat(dataVal.response[2].weekwisedata[2].weeklyActualProduction)
                },
                {
                    name: 'Week30',
                    y:parseFloat(dataVal.response[2].weekwisedata[3].weeklyActualProduction)
                },
                {
                    name: 'Week31',
                    y:parseFloat(dataVal.response[2].weekwisedata[4].weeklyActualProduction)
                },
                {
                    name: 'Week32',
                    y:parseFloat(dataVal.response[2].weekwisedata[5].weeklyActualProduction)
                },
                {
                    name: 'Week33',
                    y:parseFloat(dataVal.response[2].weekwisedata[6].weeklyActualProduction)
                },
                {
                    name: 'Week34',
                    y:parseFloat(dataVal.response[2].weekwisedata[7].weeklyActualProduction)
                },
                {
                    name: 'Week35',
                    y:parseFloat(dataVal.response[2].weekwisedata[8].weeklyActualProduction)
                },
                {
                    name: 'Week36',
                    y:parseFloat(dataVal.response[2].weekwisedata[9].weeklyActualProduction)
                },
                {
                    name: 'Week37',
                    y:parseFloat(dataVal.response[2].weekwisedata[10].weeklyActualProduction)
                },
                {
                    name: 'Week38',
                    y:parseFloat(dataVal.response[2].weekwisedata[11].weeklyActualProduction)
                },
                {
                    name: 'Week39',
                    y:parseFloat(dataVal.response[2].weekwisedata[12].weeklyActualProduction)
                }
            ]
        },{
            id: 'Quarter4a',
            name: 'Actual Quality ',
            data: [
                {
                    name: 'Week40',
                    y:parseFloat(dataVal.response[3].weekwisedata[0].weeklyActualProduction)
                },
                {
                    name: 'Week41',
                    y:parseFloat(dataVal.response[3].weekwisedata[1].weeklyActualProduction)
                },
                {
                    name: 'Week42',
                    y:parseFloat(dataVal.response[3].weekwisedata[2].weeklyActualProduction)
                },
                {
                    name: 'Week43',
                    y:parseFloat(dataVal.response[3].weekwisedata[3].weeklyActualProduction)
                },
                {
                    name: 'Week44',
                    y:parseFloat(dataVal.response[3].weekwisedata[4].weeklyActualProduction)
                },
                {
                    name: 'Week45',
                    y:parseFloat(dataVal.response[3].weekwisedata[5].weeklyActualProduction)
                },
                {
                    name: 'Week46',
                    y:parseFloat(dataVal.response[3].weekwisedata[6].weeklyActualProduction)
                },
                {
                    name: 'Week47',
                    y:parseFloat(dataVal.response[3].weekwisedata[7].weeklyActualProduction)
                },
                {
                    name: 'Week48',
                    y:parseFloat(dataVal.response[3].weekwisedata[8].weeklyActualProduction)
                },
                {
                    name: 'Week49',
                    y:parseFloat(dataVal.response[3].weekwisedata[9].weeklyActualProduction)
                },
                {
                    name: 'Week50',
                    y:parseFloat(dataVal.response[3].weekwisedata[10].weeklyActualProduction)
                },
                {
                    name: 'Week51',
                    y:parseFloat(dataVal.response[3].weekwisedata[11].weeklyActualProduction)
                },
                {
                    name: 'Week52',
                    y:parseFloat(dataVal.response[3].weekwisedata[12].weeklyActualProduction)
                },{
                    name: 'Week53',
                    y:parseFloat(dataVal.response[3].weekwisedata[13].weeklyActualProduction)
                }
            ]
        }, 
        {
            id: 'Quarter1p',
            name: 'Planned Quantity',
            data: [{
                name: 'Week1',
                y:parseFloat(dataVal.response[0].weekwisedata[0].weeklyPlannedProduction)
            },
            {
                name: 'Week2',
                y:parseFloat(dataVal.response[0].weekwisedata[1].weeklyPlannedProduction)
            },
            {
                name: 'Week3',
                y:parseFloat(dataVal.response[0].weekwisedata[2].weeklyPlannedProduction)
            },
            {
                name: 'Week4',
                y:parseFloat(dataVal.response[0].weekwisedata[3].weeklyPlannedProduction)
            },
            {
                name: 'Week5',
                y:parseFloat(dataVal.response[0].weekwisedata[4].weeklyPlannedProduction)
            },
            {
                name: 'Week6',
                y:parseFloat(dataVal.response[0].weekwisedata[5].weeklyPlannedProduction)
            },
            {
                name: 'Week7',
                y:parseFloat(dataVal.response[0].weekwisedata[6].weeklyPlannedProduction)
            },
            {
                name: 'Week8',
                y:parseFloat(dataVal.response[0].weekwisedata[7].weeklyPlannedProduction)
            },
            {
                name: 'Week9',
                y:parseFloat(dataVal.response[0].weekwisedata[8].weeklyPlannedProduction)
            },
            {
                name: 'Week10',
                y:parseFloat(dataVal.response[0].weekwisedata[9].weeklyPlannedProduction)
            },
            {
                name: 'Week11',
                y:parseFloat(dataVal.response[0].weekwisedata[10].weeklyPlannedProduction)
            },
            {
                name: 'Week12',
                y:parseFloat(dataVal.response[0].weekwisedata[11].weeklyPlannedProduction)
            },
            {
                name: 'Week13',
                y:parseFloat(dataVal.response[0].weekwisedata[12].weeklyPlannedProduction)
            }
            ]
        }, {
            id: 'Quarter2p',
            name:'Planned Quantity',
            data: [
            	{
                    name: 'Week14',
                    y:parseFloat(dataVal.response[1].weekwisedata[0].weeklyPlannedProduction)
                },
                {
                    name: 'Week15',
                    y:parseFloat(dataVal.response[1].weekwisedata[1].weeklyPlannedProduction)
                },
                {
                    name: 'Week16',
                    y:parseFloat(dataVal.response[1].weekwisedata[2].weeklyPlannedProduction)
                },
                {
                    name: 'Week17',
                    y:parseFloat(dataVal.response[1].weekwisedata[3].weeklyPlannedProduction)
                },
                {
                    name: 'Week18',
                    y:parseFloat(dataVal.response[1].weekwisedata[4].weeklyPlannedProduction)
                },
                {
                    name: 'Week19',
                    y:parseFloat(dataVal.response[1].weekwisedata[5].weeklyPlannedProduction)
                },
                {
                    name: 'Week20',
                    y:parseFloat(dataVal.response[1].weekwisedata[6].weeklyPlannedProduction)
                },
                {
                    name: 'Week21',
                    y:parseFloat(dataVal.response[1].weekwisedata[7].weeklyPlannedProduction)
                },
                {
                    name: 'Week22',
                    y:parseFloat(dataVal.response[1].weekwisedata[8].weeklyPlannedProduction)
                },
                {
                    name: 'Week23',
                    y:parseFloat(dataVal.response[1].weekwisedata[9].weeklyPlannedProduction)
                },
                {
                    name: 'Week24',
                    y:parseFloat(dataVal.response[1].weekwisedata[10].weeklyPlannedProduction)
                },
                {
                    name: 'Week25',
                    y:parseFloat(dataVal.response[1].weekwisedata[11].weeklyPlannedProduction)
                },
                {
                    name: 'Week26',
                    y:parseFloat(dataVal.response[1].weekwisedata[12].weeklyPlannedProduction)
                }
            ]
        },
         {
            id: 'Quarter3p',
            name:'Planned Quantity',
            data: [
            	{
                    name: 'Week27',
                    y:parseFloat(dataVal.response[2].weekwisedata[0].weeklyPlannedProduction)
                },
                {
                    name: 'Week28',
                    y:parseFloat(dataVal.response[2].weekwisedata[1].weeklyPlannedProduction)
                },
                {
                    name: 'Week29',
                    y:parseFloat(dataVal.response[2].weekwisedata[2].weeklyPlannedProduction)
                },
                {
                    name: 'Week30',
                    y:parseFloat(dataVal.response[2].weekwisedata[3].weeklyPlannedProduction)
                },
                {
                    name: 'Week31',
                    y:parseFloat(dataVal.response[2].weekwisedata[4].weeklyPlannedProduction)
                },
                {
                    name: 'Week32',
                    y:parseFloat(dataVal.response[2].weekwisedata[5].weeklyPlannedProduction)
                },
                {
                    name: 'Week33',
                    y:parseFloat(dataVal.response[2].weekwisedata[6].weeklyPlannedProduction)
                },
                {
                    name: 'Week34',
                    y:parseFloat(dataVal.response[2].weekwisedata[7].weeklyPlannedProduction)
                },
                {
                    name: 'Week35',
                    y:parseFloat(dataVal.response[2].weekwisedata[8].weeklyPlannedProduction)
                },
                {
                    name: 'Week36',
                    y:parseFloat(dataVal.response[2].weekwisedata[9].weeklyPlannedProduction)
                },
                {
                    name: 'Week37',
                    y:parseFloat(dataVal.response[2].weekwisedata[10].weeklyPlannedProduction)
                },
                {
                    name: 'Week38',
                    y:parseFloat(dataVal.response[2].weekwisedata[11].weeklyPlannedProduction)
                },
                {
                    name: 'Week39',
                    y:parseFloat(dataVal.response[2].weekwisedata[12].weeklyPlannedProduction)
                }
            ]
        }, {
            id: 'Quarter4p',
            name:'Planned Quantity',
            data: [
            	 {
                     name: 'Week40',
                     y:parseFloat(dataVal.response[3].weekwisedata[0].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week41',
                     y:parseFloat(dataVal.response[3].weekwisedata[1].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week42',
                     y:parseFloat(dataVal.response[3].weekwisedata[2].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week43',
                     y:parseFloat(dataVal.response[3].weekwisedata[3].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week44',
                     y:parseFloat(dataVal.response[3].weekwisedata[4].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week45',
                     y:parseFloat(dataVal.response[3].weekwisedata[5].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week46',
                     y:parseFloat(dataVal.response[3].weekwisedata[6].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week47',
                     y:parseFloat(dataVal.response[3].weekwisedata[7].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week48',
                     y:parseFloat(dataVal.response[3].weekwisedata[8].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week49',
                     y:parseFloat(dataVal.response[3].weekwisedata[9].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week50',
                     y:parseFloat(dataVal.response[3].weekwisedata[10].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week51',
                     y:parseFloat(dataVal.response[3].weekwisedata[11].weeklyPlannedProduction)
                 },
                 {
                     name: 'Week52',
                     y:parseFloat(dataVal.response[3].weekwisedata[12].weeklyPlannedProduction)
                 },{
                     name: 'Week53',
                     y:parseFloat(dataVal.response[3].weekwisedata[13].weeklyPlannedProduction)
                 }
            ]
        }],

    }


});
}



