var ManagementApp = angular.module('ManagementApp', [ 'ui.router','ngStorage','ngFlash',
                                                      'ui.bootstrap', 'angularUtils.directives.dirPagination','ngMessages']);

ManagementApp.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/WeeklyReport');

	$stateProvider

	/*.state('DailyReport',{
		url: '/DailyReport',
		templateUrl : 'view/managementHome.html',
		controller : 'ManagementController1',
		data : {
			displayName : 'DailyReport',
		}
	})*/
	
	.state('DayWiseReport',{
		url: '/DayWiseReport',
		templateUrl : 'view/dayWiseProductionReport.html',
		controller : 'ManagementController15',
		data : {
			displayName : 'DayWiseReport',
		}
	})
	
	.state('ProcessOrderHistory', {
		url : '/ProcessOrderHistory',
		templateUrl : 'view/allProcessOrdersTiles.html',
		controller : 'AllOrdersController',
		data : {
			displayName : 'ProcessOrderHistory',
		}
	})
	/*.state('DailyReports',{
		url: '/DailyReports',
		templateUrl : '../management/view/managementHome.html',
		controller : 'ManagementController1',
		data : {
			displayName : 'DailyReport',
		}
	})*/
	
	.state('WeeklyReport',{
		url: '/WeeklyReport',
		templateUrl : 'view/managementWeeklyReport.html',
		controller : 'ManagementController2',
		data : {
			displayName : 'WeeklyReport',
		}
	})

	
	.state('QuaterReport',{
		url: '/QuaterReport',
		templateUrl : 'view/managementCurrentQuarterReport.html',
		controller : 'ManagementController4',
		data : {
			displayName : 'QuaterReport',
		}
	})
	
	.state('YearlyReport',{
		url: '/YearlyReport',
		templateUrl : 'view/managementYearlyReport.html',
		controller : 'ManagementController5',
		data : {
			displayName : 'YearlyReport',
		}
	})
	
	
	
	.state('ProductWiseReports',{
		url: '/ProductWiseReports',
		templateUrl : 'view/management-pwr.html',
		controller : 'ManagementController6',
		data : {
			displayName : 'ProductWiseReports',
		}
	})
	
	.state('DateWiseProductReport',{
		url: '/DateWiseProductReport',
		templateUrl : 'view/management-pvr1.html',
		controller : 'ManagementController7',
		data : {
			displayName : 'DateWiseProductReport',
		}
	})
	
	.state('DateWiseProductReports',{
		url: '/DateWiseProductReports',
		templateUrl : 'view/managementPvr2.html',
		controller : 'ManagementController11',
		data : {
			displayName : 'DateWiseProductReports',
		}
	})
	
		
	
	.state('ProductionWiseReport',{
		url: '/ProductionWiseReport',
		templateUrl : 'view/managementQuaterReport.html',
		controller : 'ManagementController8',
		data : {
			displayName : 'ProductionWiseReport',
		}
	})
	
	.state('cycleTimeReport',{
		url: '/cycleTimeReport',
		templateUrl : 'view/ctreport.html',
		controller : 'CycleTimeReportController',
		data : {
			displayName : 'cycleTimeReport',
		}
	})
	
	
	.state('repackReport',{
		url: '/repackReport',
		templateUrl : 'view/repackingReport.html',
		controller : 'RepackReportController',
		data : {
			displayName : 'repackReport',
		}
	})
	
	.state('CTRDetails',{
		url: '/CTRDetails',
		templateUrl : 'view/ctreportdetails.html',
		controller : 'CycleTimeReportDetailsController',
		data : {
			displayName : 'CTRDetails',
		}
	})
	
	.state('CTRDetailsForMonth',{
		url: '/CTRDetailsForMonth',
		templateUrl : 'view/ctReportDetailsForMonth.html',
		controller : 'CycleTimeReportDetailsController',
		data : {
			displayName : 'CTRDetailsForMonth',
		}
	})
	
	.state('CTRDetailsForQuarter',{
		url: '/CTRDetailsForQuarter',
		templateUrl : 'view/ctReportDetailsForQuarter.html',
		controller : 'CycleTimeReportDetailsController',
		data : {
			displayName : 'CTRDetailsForQuarter',
		}
	})
	
	.state('CTRDetailsForYear',{
		url: '/CTRDetailsForYear',
		templateUrl : 'view/ctReportDetailsForYear.html',
		controller : 'CycleTimeReportDetailsController',
		data : {
			displayName : 'CTRDetailsForYear',
		}
	})
	.state('processOrder-status', {
		url : '/processOrder-status',
		templateUrl : 'view/live.html',
		controller : 'StatusInfoController'
	})
	.state('repacking-status-info', {
		url : '/repacking-status-info',
		templateUrl : 'view/repackingLive.html',
		controller : 'RepackingStatusInfoController'
	})
	.state('repackingHistory', {
		url : '/repackingHistory',
		templateUrl : 'view/repackingHistory.html',
		controller : 'RepackingHistoryController'
	})
	.state('repackingHistoryDetails', {
		url : '/repackingHistoryDetails',
		templateUrl : 'view/repackingHistoryDetails.html',
		controller : 'RepackingHistoryDetailsController'
	})
	.state('history', {
		url : '/history',
		templateUrl : 'view/history.html',
		controller : 'ProcessHistoryController'
	})
	
	.state('historyfirst', {
		url : '/history',
		templateUrl : 'view/historyfirst.html',
		controller : 'ProcessHistoryDetailsController'
	})
	
	
	/*.state('QuaterProductionProductReports',{
		url: '/QuaterProductionProductReports',
		templateUrl : 'view/managementQuaterReport.html',
		controller : '',
		data : {
			displayName : 'QuaterProductionProductReports',
		}*/

	
	
	// HOME STATES AND NESTED VIEWS ========================================
	
});



ManagementApp.controller('logOutController', ['$scope','$window','$http','$filter','$timeout','$rootScope','Flash','$localStorage', function($scope,$window,$http,$filter,$timeout,$rootScope,Flash,$localStorage) {
	
	$scope.showModal = true;
	$scope.showButtons = true;
	$scope.showClose = false;
	$scope.showFailed = false;
	
	var diffRoles =[];
	var sso = $localStorage.userSso;
	if(sso == undefined){
		//alert('Empty');
		window.location.href = "/suez";
	}
	$http({		
		method : "GET",		
		url : '/suez/po/RoleList/'+ sso ,		
	headers : {		
			'Content-Type' : 'application/json'		
		}		
	}).then(function mySucces(response) {		
		console.log(response);	
		//alert('Got all Rroles');
		$scope.roles = response.data;
		diffRoles = $scope.roles;
		/*alert($scope.roles[0].role);
		alert($scope.roles[1].role);
		alert($scope.roles[2].role);*/
		if(diffRoles.length > 1){
			$rootScope.showLoginAs=true;
		}
		
		for(var i=0; i<diffRoles.length; i++){
			if(diffRoles[i].role=="1"){
				$rootScope.isAdmin=true;
			} else if (diffRoles[i].role=="2"){
				$rootScope.isWarehouse=true;
			} else if (diffRoles[i].role=="4"){
				$rootScope.isOperator=true;
			}else if (diffRoles[i].role=="5"){
				$rootScope.isQuality=true;
			}
		}
		//console.log($scope.alertInformation);		
				
	}, function myError(error) {		
		console.log(error);		
	});
	
	//var roles=["2","3"];
	$rootScope.isAdmin=false;
	$rootScope.isWarehouse=false;
	$rootScope.isOperator=false;
	$rootScope.isQuality=false;
	$rootScope.isManagement=false;
	$rootScope.showLoginAs=false;
	
		
	$scope.warehouseLogin = function(){
		//alert('Login to warehouse');
		window.location.href = "../warehouse/warehouse.html";
	}
	
	$scope.adminLogin = function(){
		//alert('Login to Admin');
		window.location.href ="../admin/admin.html";
	}
	
	$scope.operatorLogin = function(){
		//alert('Login to Production');
		window.location.href ="../productionOperator/poWelcome.html";
	}
	
	$scope.qualityLogin = function(){
		//alert('Login to Production');
		window.location.href ="../quality/quality.html";
	}
    $scope.count = 0;
    $scope.CurrentDate = new Date();
	//$scope.showDate = true;
	
	$scope.ddMMyyyy = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.ddMMMMyyyy = $filter('date')(new Date(), 'dd, MMMM yyyy');
    $scope.HHmmss = $filter('date')(new Date(), 'HH:mm:ss');
    $scope.hhmmsstt = $filter('date')(new Date(), 'hh:mm:ss a');
    
    $scope.logOut = function() {
    	$window.sessionStorage.ssoId = '';
		$window.sessionStorage.role = '';
		$localStorage.userSso = " ";	
		$localStorage.userData =" ";
		localStorage.setItem('loginUser', '');
		window.location.href = "/suez";
    };
    
    $scope.getUserInformation = function(ssoId){
    	var ssoId = $window.sessionStorage.ssoId;
    	$http({
    		method:"GET",
    		url:'/suez/user/getUserBySSOId/'	+ ssoId,
    		headers: {'Content-Type' : 'application/json'}
    	}).then(function mySucces(response) {
    		console.log(response);
    		$scope.userInformation = response.data;
        }, function myError(error) {
            //console.log(error);
        });
    }
    
    $scope.update = function(newPassword,checkPassword) {
    	//alert();
		if(newPassword == undefined || checkPassword == undefined){
			$scope.fillAll = true;
			return;
			
		}
		if(newPassword !=checkPassword){
			return;
		}
		var user={};
		user.ssoid=$window.sessionStorage.ssoId;
		user.password=newPassword;
		
		$http.post('/suez/user/changePassword', JSON.stringify(user), {
			'Content-Type' : 'application/json'
		}).then(function(response) {
			$scope.showModal = false;
			$scope.showButtons = false;
			$scope.showClose = true;
			$scope.showFailed = false;
		}, function(response) {
			// something went wrong
			var message = "";
			message = 'Please try again';
			Flash.create('warning', message);
		}); 
    }
    
}]);

