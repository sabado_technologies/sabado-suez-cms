$(document).ready(function() {
	alert("document ready");
	
	  $.ajax({ 
		  url: '/suez/Management/fullQuaterProductionReport', 
		  type:'GET',
		  async: true,
		  dataType: "json",
		  success: function (data) {
			  //alert("visitor data"); //visitorData(data);
			  console.log("***vistor*********");
			  console.log(data);
			  for(var i = 0 ; i < data.length;i++){
				  prepareChart(data[i]); 
			  }
			  console.log("************"); 
		} 
	  });
});

function prepareChart(data) {
	var type =data.quarterNumber;
	console.log(data);
	console.log(type);
	var quarterName = "";
	var quarterText = "";
	if(type=="1"){
		quarterName = "q1";
		quarterText = "Production Report from January 01 to April 01 ";
	}else if(type=="2"){
		quarterName = "q2";
		quarterText = "Production Report from April 01 to July 01 ";
	}else if(type=="3"){
		quarterName = "q3";
		quarterText = "Production Report from July 01 to October 01";
	}else if(type=="4"){
		quarterName = "q4";
		quarterText = "Production Report from October 01 to January 01";
	}
	console.log(quarterName);
	
	Highcharts.chart(quarterName,{
						credits : {
							enabled : false
						},
						chart : {
							type : 'pie'
						},
						
						title: {
							  text: 	quarterText					
						},
						
						
						subtitle : {
							text : 'Click the slices to view versions'
						           },
						plotOptions : {
							series : {
								dataLabels : {
									enabled : true,
									format : '{point.name}: {point.y:.1f} MT'
								}
							}
						},

						tooltip : {
							headerFormat : '<span style="font-size:11px">{series.name}</span><br>',
							pointFormat : '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
						},
						series: [{
					        name: 'Planned Production',
					        colorByPoint: true,
					        data: [{
					            name: 'Remaining Production Qty. ',
					            y: data.remainingProductionPer,
					           
					        },
					         {
					            name: 'Actual Production Qty.',
					            y: data.actualProductionPer,
					            drilldown: 'ap'
					        }
					        ]
					    }],
					    drilldown: {
					        series: [{
					            name: 'production report',
					            id: 'po',
					            data: [
					                
					            ]
					        }, {
					            name: 'Actual Production Qty.',
					            id: 'ap',
					            data: data.productInfos
					            
					        }  ]
					    }
					});
}
