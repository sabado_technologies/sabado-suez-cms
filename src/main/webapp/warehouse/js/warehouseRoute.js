var whApp = angular.module('whApp', ['ui.router','ngStorage','ui.bootstrap', 'angularUtils.directives.dirPagination','ngMessages']);


whApp.config(function($stateProvider, $urlRouterProvider) {

$urlRouterProvider.otherwise('/processOrders');

	$stateProvider

	// HOME STATES AND NESTED VIEWS ========================================
	.state('home', {
		url : '/home',
		templateUrl : 'view/allProcessOrdersTiles.html',
		controller : 'AllOrdersController',
		data : {
			displayName : 'AllProcessOrdersList',
		}
	})
	
	.state('processOrders', {
		url : '/processOrders',
		templateUrl : 'view/warehouse-home.html',
		controller : 'wareHouseController',
		data : {
			displayName : 'ProcessOrders',
		}
	})
	.state( 'initiateOrder', {
		url : '/initiateOrder',
		templateUrl : 'view/processOrders.html',
		controller : 'initiateController1',
		
			data : {
			displayName : 'initiateOrder'
			
		}
	})
	
	
	.state( 'PendingOrders', {
		url : '/PendingOrders',
		templateUrl : 'view/PendingOrders.html',
		controller : 'WarehousePendingOrdersController',
		
			data : {
			displayName : 'pendingOrders'
			
		}
	})
	
	.state( 'PendingInitiateOrder', {
		url : '/PendingInitiateOrder',
		templateUrl : 'view/PendingProcessOrders.html',
		controller : 'initiateController2',
		
			data : {
			displayName : 'PendingInitiateOrder'
			
		}
	})
	.state( 'WarehouseInventoryCheck', {
		url : '/WarehouseInventoryCheck',
		templateUrl : 'view/WarehouseInventoryCheck.html',
		controller : 'WarehouseQualityController',
		
			data : {
			displayName : 'WarehouseQualityCheck'
			
		}
	})
	
	.state( 'PackingDetailsInfo', {
		url : '/PackingDetailsInfo',
		templateUrl : 'view/associatedpackagingOrders.html',
		controller : 'WarehouseQualityController2',
		
			data : {
			displayName : 'PackingDetailsInfo'
			
		}
	})
	.state( 'history', {
		url : '/history',
		templateUrl : 'view/history.html',
		controller : 'HistoryOfContainersController',
		
			data : {
			displayName : 'HistoryOfContainers'
			
		}
	})
	
	.state( 'historyofRepack', {
		url : '/historyofRepack',
		templateUrl : 'view/historyOfRepack.html',
		controller : 'HistoryOfContainersController1',
		
			data : {
			displayName : 'HistoryOfRepackContainers'
			
		}
	})
	
	
	
	.state( 'repackInventoryInitiate', {
		url : '/repackInventoryInitiate',
		templateUrl : 'view/initiateRepackingInventory.html',
		controller : 'repackInventoryInitiate',
		
			data : {
			displayName : 'repackInventoryInitiate'
			
		}
	})
	
		.state( 'repack', {
		url : '/repack',
		templateUrl : 'view/repacking.html',
		controller : 'repackInventory',
		
			data : {
			displayName : 'repackInventory'
			
		}
	})
	
	.state( 'repackForRM', {
		url : '/repackForRM',
		templateUrl : 'view/repackingOrders.html',
		controller : 'repackPOForRmRelease',
		
			data : {
			displayName : 'repackForRM'
			
		}
	})
	
	.state( 'initiateRepackOrderForRM', {
		url : '/initiateRepackOrderForRM',
		templateUrl : 'view/repackingOrderDetails.html',
		controller : 'repackPOForRmDetailsRelease',
		
			data : {
			displayName : 'initiateRepackOrderForRM'
			
		}
	})
	.state( 'detailsOfContainers', {
		url : '/detailsOfContainers',
		templateUrl : 'view/DetailsOfContainers.html',
		controller : 'detailsOfContainers',
		
			data : {
			displayName : 'detailsOfContainers'
			
		}
	})
	.state( 'detailsOfContainers1', {
		url : '/detailsOfContainers1',
		templateUrl : 'view/DetailsOfRepackContainers.html',
		controller : 'detailsOfContainers1',
		
			data : {
			displayName : 'detailsOfRepackContainers'
			
		}
	})
	
	.state('WarehouseInventoryPosting' ,{
		url : '/WarehouseInventoryPosting',
		templateUrl : 'view/inventoryPosting.html',
		controller : 'WarehouseProcessOrderInventoryPosting',
		
			data : {
			displayName : 'InventoryPosting'
			
		}
	})
	.state('PackingDetailsInfoForPosting' ,{
		url : '/PackingDetailsInfoForPosting',
		templateUrl : 'view/inventoryPostingDetails.html',
		controller : 'WarehouseProcessOrderInventoryPosting2',
		
			data : {
			displayName : 'InventoryPostingDetails'
			
		}
	})
	.state('RepackInventoryPosting' ,{
		url : '/RepackInventoryPosting',
		templateUrl : 'view/repackingPostingInitiate.html',
		controller : 'repackInventoryPostingInitiate',
		
			data : {
			displayName : 'RepackInventoryPosting'
			
		}
	})
	.state('repackInventoryPostingDetailsList' ,{
		url : '/repackInventoryPostingDetailsList',
		templateUrl : 'view/repackingPostingDetails.html',
		controller : 'repackInventoryPostingDetails',
		
			data : {
			displayName : 'repackInventoryPostingDetails'
			
		}
	})
	.state('processOrder-status', {
		url : '/processOrder-status',
		templateUrl : 'view/live.html',
		controller : 'StatusInfoController'
	})
	.state('repacking-status-info', {
		url : '/repacking-status-info',
		templateUrl : 'view/repackingLive.html',
		controller : 'RepackingStatusInfoController'
	})
	.state('repackingHistory', {
		url : '/repackingHistory',
		templateUrl : 'view/repackingHistory.html',
		controller : 'RepackingHistoryController'
	})
	.state('repackingHistoryDetails', {
		url : '/repackingHistoryDetails',
		templateUrl : 'view/repackingHistoryDetails.html',
		controller : 'RepackingHistoryDetailsController'
	})
	.state('historyDetails', {
		url : '/historyDetails',
		templateUrl : 'view/processHistory.html',
		controller : 'ProcessHistoryController'
	})
	
	.state('historyfirst', {
		url : '/historyDetails',
		templateUrl : 'view/historyfirst.html',
		controller : 'ProcessHistoryDetailsController'
	})
	
});


whApp.run([ '$state', '$rootScope', '$transitions',
	function($state, $rootScope, $transitions) {
		$transitions.onExit({}, function(trans, $state) {
			//console.log(trans._targetState._identifier);

			if ($state.name == "initiateOrder") {

				if (trans._targetState._identifier == 'processOrders') {

				} else {
					var answer = confirm("Want to leave this page?")
					if (!answer) {
						return false;
					}
				}

			}
			
			if ($state.name == "PendingInitiateOrder") {

				if (trans._targetState._identifier == 'PendingOrders') {

				} else {
					var answer = confirm("Want to leave this page?")
					if (!answer) {
						return false;
					}
				}

			}
			
			if ($state.name == "PackingDetailsInfo") {

				if (trans._targetState._identifier == 'WarehouseInventoryCheck') {

				} else {
					var answer = confirm("Want to leave this page?")
					if (!answer) {
						return false;
					}
				}

			}
			
			if ($state.name == "repack") {

				if (trans._targetState._identifier == 'repackInventoryInitiate') {

				} else {
					var answer = confirm("Want to leave this page?")
					if (!answer) {
						return false;
					}
				}

			}
			
			
		});
	} ]);




whApp.controller('logOutController', ['$scope','$window','$http','$filter','$timeout','$rootScope','$localStorage', function($scope,$window,$http,$filter,$timeout,$rootScope,$localStorage) {
	
	$scope.showModal = true;
	$scope.showButtons = true;
	$scope.showClose = false;
	$scope.showFailed = false;
	
	var diffRoles =[];
	var sso = $localStorage.userSso;
	if(sso == undefined){
		//alert('Empty');
		window.location.href = "/suez";
	}
	$http({		
		method : "GET",		
		url : '/suez/po/RoleList/'+ sso ,		
	headers : {		
			'Content-Type' : 'application/json'		
		}		
	}).then(function mySucces(response) {		
		//console.log(response);	
		//alert('Got all Rroles');
		$scope.roles = response.data;
		diffRoles = $scope.roles;
		/*alert($scope.roles[0].role);
		alert($scope.roles[1].role);
		alert($scope.roles[2].role);*/
		if(diffRoles.length > 1){
			$rootScope.showLoginAs=true;
		}
		
		for(var i=0; i<diffRoles.length; i++){
			if(diffRoles[i].role=="1"){
				$rootScope.isAdmin=true;
			} else if (diffRoles[i].role=="3"){
				$rootScope.isManagement=true;
			} else if (diffRoles[i].role=="4"){
				$rootScope.isOperator=true;
			}else if (diffRoles[i].role=="5"){
				$rootScope.isQuality=true;
			}
		}
		//console.log($scope.alertInformation);		
				
	}, function myError(error) {		
		//console.log(error);		
	});
	
	
	//var roles=["2","3"];
	$rootScope.isAdmin=false;
	$rootScope.isWarehouse=false;
	$rootScope.isOperator=false;
	$rootScope.isQuality=false;
	$rootScope.isManagement=false;
	$rootScope.showLoginAs=false;
	
		
	$scope.operatorLogin = function(){
		//alert('Login to warehouse');
		window.location.href = "../productionOperator/poWelcome.html";
	}
	
	$scope.adminLogin = function(){
		//alert('Login to Admin');
		window.location.href ="../admin/admin.html";
	}
	
	$scope.managementLogin = function(){
		//alert('Login to Production');
		window.location.href ="../management/management.html";
	}
	
	$scope.qualityLogin = function(){
		//alert('Login to Production');
		window.location.href ="../quality/quality.html";
	}
	
	$scope.getAlertsForWarehouse =function(){		
			
	$http({		
			method : "GET",		
			url : '/suez/RoleBasedAlerts/alertsForWarehouse ' ,		
		headers : {		
				'Content-Type' : 'application/json'		
			}		
		}).then(function mySucces(response) {		
			//console.log(response);	
			var i=0;
			$scope.alertLength=0;
			$scope.alertInformation = response.data;
			/*while($scope.alertInformation!=0){
				if($scope.alertInformation[i].status.statusId==17 || $scope.alertInformation[i].status.statusId==7){
					$scope.alertLength=$scope.alertLength+1;
					i++;
				}
				
			}
			console.log($scope.alertLength);
			console.log($scope.alertInformation);*/		
					
		}, function myError(error) {		
			//console.log(error);		
		});		
				
	};		

	
    $scope.count = 0;
    
    $scope.CurrentDate = new Date();
    $scope.getAlertsForWarehouse();
	//$scope.showDate = true;
	
	$scope.ddMMyyyy = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.ddMMMMyyyy = $filter('date')(new Date(), 'dd, MMMM yyyy');
    $scope.HHmmss = $filter('date')(new Date(), 'HH:mm:ss');
    $scope.hhmmsstt = $filter('date')(new Date(), 'hh:mm:ss a');
    
    $scope.logOut = function() {
    	$window.sessionStorage.ssoId = '';
		$window.sessionStorage.role = '';
		$localStorage.userSso = " ";	
		$localStorage.userData =" ";
		localStorage.setItem('loginUser', '');
		window.location.href = "/suez";
    };
    
    $scope.getUserInformation = function(ssoId){
    	var ssoId = $window.sessionStorage.ssoId;
    	$http({
    		method:"GET",
    		url:'/suez/user/getUserBySSOId/'	+ ssoId,
    		headers: {'Content-Type' : 'application/json'}
    	}).then(function mySucces(response) {
    		//console.log(response);
    		$scope.userInformation = response.data;
        }, function myError(error) {
            //console.log(error);
        });
    }
    
    $scope.update = function(newPassword,checkPassword) {
		if(newPassword == undefined || checkPassword == undefined){
			$scope.fillAll = true;
			return;
			
		}
		if(newPassword !=checkPassword){
			return;
		}
		var user={};
		user.ssoid=$window.sessionStorage.ssoId;
		user.password=newPassword;
		
		$http.post('/suez/user/changePassword', JSON.stringify(user), {
			'Content-Type' : 'application/json'
		}).then(function(response) {
			$scope.showModal = false;
			$scope.showButtons = false;
			$scope.showClose = true;
			$scope.showFailed = false;
		}, function(response) {
			// something went wrong
			var message = "";
			message = 'Please try again';
			/*Flash.create('warning', message);*/
		});
    }
    
}]);

