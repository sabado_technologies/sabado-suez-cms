/*Share Service*/
whApp.factory('shareService', function() {
	var savedData = {};
	function set(data) {
		savedData = data;
	}

	function get() {
		return savedData;
	}

	return {
		set : set,
		get : get
	}
});

/* Warehouse Service */
whApp.factory('wareHouseService', function($http) {
	return {
		getAllProcessOrders : function() {
			return $http.get('/suez/WareHouseController/getAllProcessOrders', {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				alert('error');
			});
		},

		getProcessOrderCount : function() {
			return $http.get('/suez/WareHouseController/getProcessOrderCount',
					{
				'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},

		getProcessOrderCompletedCount : function() {
			return $http.get(
					'/suez/WareHouseController/getProcessOrderCompletedCount',
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},

		getUserInfo : function(ssoId) {
			return $http.get('/suez/getUserBySSOId/' + ssoId, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				alert('error');
			});
		},
		setProcessOrderTrackerStatus : function(processorderProdNumber, ssoId) {
			return $http.post(
					'/suez/WareHouseController/setProcessOrderTrackerStatusForRejection/'
					+ processorderProdNumber + "/" + ssoId, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
						
					});
		},
		
		setProcessOrderTrackerStatusForRejection : function(processorderProdNumber, ssoId ,submitMessage) {
			return $http.post(
					'/suez/WareHouseController/setProcessOrderTrackerStatusForRejection/'
					+ processorderProdNumber + "/" + ssoId + "/" + submitMessage, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		getAllRepackingStatusInfo : function() {
			return $http.get('/suez/pending/getAllRepackingStatusInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getProcessHistory : function() {
			return $http.get(
					'/suez/pending/getProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},		
		getProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getProcessOrdersForPostingCheck/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		} ,
		associatedPackingHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/associatedPackingInfoForPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		getRepackProcessHistory : function() {
			return $http.get(
					'/suez/pending/getRepackingProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},
		getRepackProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventoryPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		updateExtraRm : function(process,rmInfo) {
			return $http.post('/suez/pending/addExtraRawMaterial/'+ process+'/'+rmInfo, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				//console.log('error')
				//alert('error');
			});
		},
		getPackinVesselHrs : function(process) {
			return $http.get('/suez/Management/cycleTimeForModal/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		getRepackPackinVesselHrs : function(process) {
			return $http.get('/suez/pending/repackingHours/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		
	}

});

/* Warehouse Service 1 */
whApp.factory('wareHouseService1', function($http, $rootScope) {
	return {
		getProcessOrderDetails : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getProcessOrderDetails/'
					+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},

		updateProcessOrderStatus : function(processorderProdNumber,
				ProcessTrackerId,ssoId) {
			return $http.post(
					'/suez/WareHouseController/updateProcessOrderStatus/'
					+ processorderProdNumber + "/" + ProcessTrackerId +"/"+ssoId,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		// ******************************
		setLocationOfRm : function(processTrackerTable) {
			return $http.post('/suez/WareHouseController/setLocationOfRm/',
					JSON.stringify(processTrackerTable), {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				alert('error');
			});
		},
		// ******************************
		setProcessOrderTrackerStatus : function(processorderProdNumber, ssoId) {
			return $http.post(
					'/suez/WareHouseController/setProcessOrderTrackerStatus/'
					+ processorderProdNumber + "/" + ssoId, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},

		setRmLocationDetails : function(processorderProdNumber,rmId,
				locationRawmaterialDetails) {
			return $http.post(
					'/suez/WareHouseController/setRmLocationDetails/'
					+ processorderProdNumber +"/"+ rmId,
					JSON.stringify(locationRawmaterialDetails), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});

		},
		
		setRmLocationDetailsForRepack : function(processorderProdNumber,ssoId,
				locationRawmaterialDetails) {
			return $http.post(
					'/suez/WareHouseController/setRmLocationDetailsForRepack/'
					+ processorderProdNumber +"/"+ ssoId,
					JSON.stringify(locationRawmaterialDetails), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});

		},

		setTrackerStatusForRm : function(processTrackerTable) {
			return $http.post(
					'/suez/WareHouseController/setTrackerStatusForRm',
					JSON.stringify(processTrackerTable), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;

					}, function(response) {
						alert('error');
					});

		}

	}

});

whApp.factory('wareHouseService2', function($http, $rootScope) {

	return {
		getAllTodayProcessOrders : function() {
			return $http.get(
					'/suez/WareHouseController/getAllTodayProcessOrders', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}
	}

});

whApp.factory('wareHouseService3', function($http, $rootScope) {

	return {
		getProcessOrdersForQuality : function() {
			return $http.get(
					'/suez/WareHouseController/getProcessOrdersForQuality', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		getAllStatusInfo : function() {
			return $http.get('/suez/pending/getAllStatusInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		}
	}

});

whApp.factory('wareHouseService4', function($http, $rootScope) {

	return {
		getProcessOrderForQuality : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/processOrderQualityCheck/'
					+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		associatedPackingInfo : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/associatedPackingInfo/'
					+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},

		setTrackerForWarehouseQuality : function(ssoId,locationPlaced ,containerQuantity,packingdetail) {
			return $http.post(
					'/suez/WareHouseController/setTrackerForWarehouseQuality/'
					+ ssoId+"/"+ locationPlaced +"/" + containerQuantity, JSON.stringify(packingdetail), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});

		},
		
		getPackingOrderForInventory : function() {
			
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventory',
					 {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		
		getPackingOrderForInventory1 : function(processorderPckgNumber){
			
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventory1/'+processorderPckgNumber,
					 {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		
		smsAlertForWHInventory : function (processorderProdNumber ,ssoId){
			return $http.get(
					'/suez/RoleBasedAlerts/smsAlertForWHInventory/'+processorderProdNumber+"/ "+ssoId,
					 {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		setTrackerforInventory : function(processorderProdNumber ,ssoId) {
			return $http.post(
					'/suez/WareHouseController/setTrackerforInventory/'
					+ processorderProdNumber +"/"+ ssoId, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}
	}

});

whApp.factory('whAllOrdersService', function($http, $q) {

	return {

		getAllOrders : function() {
			return $http.get('/suez/wareHousepending/getAllOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getReactors : function() {
			return $http.get('/suez/wareHousepending/getReactors').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysOrders : function() {
			return $http.get('/suez/wareHousepending/getTodaysOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getPastOrders : function() {
			return $http.get('/suez/wareHousepending/getPreviousOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getFutureOrders : function() {
			return $http.get('/suez/wareHousepending/getFutureOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysCompletedOrders : function() {
			return $http.get('/suez/wareHousepending/getTodaysCompletedOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/wareHousepending/getStatus/' + processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getChargedStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/wareHousepending/getChargedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getPackedStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/wareHousepending/getPackedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityCheckStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/wareHousepending/getQualityCheckStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityInspectionStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/wareHousepending/getQualityInspectionStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getInventoryStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/wareHousepending/getInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
		
		getRmInventoryStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/wareHousepending/getRmInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
	}

});

whApp.factory('wareHouseService5', function($http, $rootScope) {

	return {
		getAllPendingProcessOrders : function() {
			return $http.get(
					'/suez/WareHouseController/getAllPendingrocessOrders', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		
		setProcessOrderTrackerStatus : function(processorderProdNumber, ssoId) {
			return $http.post(
					'/suez/WareHouseController/setProcessOrderTrackerStatusForRejection/'
					+ processorderProdNumber + "/" + ssoId, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		
		setProcessOrderTrackerStatusForRejection : function(processorderProdNumber, ssoId ,submitMessage) {
			return $http.post(
					'/suez/WareHouseController/setProcessOrderTrackerStatusForRejection/'
					+ processorderProdNumber + "/" + ssoId + "/" + submitMessage, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}

	
	}

});

whApp.factory('wareHouseService8', function($http, $rootScope) {

	return {
		getAllContainerDetailsForProcessOrders : function() {
			return $http.get(
					'/suez/WareHouseController/getAllContainerDetailsForProcessOrders', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		
		getAllContainerDetailsForRepackProcessOrders : function() {
			return $http.get(
					'/suez/WareHouseController/getAllContainerDetailsForRepackProcessOrders', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
			
		}
	}

});

whApp.factory('wareHouseService9', function($http, $rootScope) {

	return {
		getContainerDetailsForProcessOrder : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getContainerDetailsForProcessOrder/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		},
		getContainerDetailsForRepackProcessOrder : function(processorderPckgNumber){
			return $http.get(
					'/suez/WareHouseController/getContainerDetailsForRepackProcessOrder/'+processorderPckgNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		
	}
		
	}
});


whApp.factory('repackservice', function($http, $rootScope) {

	return {
		getRepackCompletedCount : function() {
			return $http.get(
					'/suez/WareHouseController/getRepackingCount', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}
	}

});

whApp.factory('repackPOForRmReleaseService', function($http, $rootScope) {

	return {
		getRePackingOrderForRmRelease : function() {
			return $http.get(
					'/suez/WareHouseController/getRePackingOrderForRM', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}
	}

});

whApp.factory('repackPOForRmDetailsService', function($http, $rootScope) {

	return {
		getRePackingOrderDetailsForRmRelease : function(processorderPckgNumber) {
			return $http.get(
					'/suez/WareHouseController/getRePackingOrderForRMDetails/'+processorderPckgNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}
	}

});

whApp.factory('InventoryPosting', function($http, $rootScope) {

	return {
		getProcessOrdersForPosting : function() {
			return $http.get(
					'/suez/WareHouseController/getProcessOrdersForPosting', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		}
	}

});

whApp.factory('InventoryPosting2', function($http, $rootScope) {

	return {
		getProcessOrderForPosting : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getProcessOrdersForPostingCheck/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		} ,
		associatedPackingInfoForPosting : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/associatedPackingInfoForPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		
		setProcessOrderStatusForPosting : function(processorderProdNumber,ssoId) {
			return $http.post(
					'/suez/WareHouseController/setProcessOrderStatusForPosting/'
					+ processorderProdNumber +"/" + ssoId, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}

		
	}

});


whApp.factory('repackPosting', function($http, $rootScope) {

	return {
		getPackingOrderForInventoryPostingList : function() {
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventoryPostingList', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}
	}

});

whApp.factory('repackPosting2', function($http, $rootScope) {

	return {
		getPackingOrderForInventoryPosting : function(processorderPckgNumber) {
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventoryPosting/'+processorderPckgNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		} ,
		
		statusProcessOrderPackPosting : function(processorderPackNumber,ssoId) {
			return $http.post(
					'/suez/WareHouseController/setProcessOrderStatusForRepackPosting/'
					+ processorderPackNumber +"/" + ssoId, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						alert('error');
					});
		}

		
	}

});

