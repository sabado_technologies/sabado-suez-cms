	whApp.controller("wareHouseController", function($rootScope, $scope, $http,
			$state, $window, wareHouseService, shareService ,repackservice) {
	
		// Calling Controller for getting all process orders with respect to date
		wareHouseService.getAllProcessOrders().then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.processOrders = response.data;
			}
	
			$scope.length = $scope.processOrders.length;
		});
	
		// getting count of the processOrders to be processed
		wareHouseService.getProcessOrderCount().then(function(response) {
			$scope.isItemAviable = true;
			if (response != undefined) {
				$scope.ProcessOrderCount = response.data;
				if ($scope.ProcessOrderCount.length == 0) {
					$scope.isItemAviable = false;
				}
	
			}
			$scope.Count = $scope.ProcessOrderCount.length;
		});
	
		// getting count of the processOrders completed
		wareHouseService.getProcessOrderCompletedCount().then(function(response) {
			$scope.isItemAviable = true;
			if (response != undefined) {
				$scope.ProcessOrderCompletedCount = response.data;
				if ($scope.ProcessOrderCompletedCount.length == 0) {
					$scope.isItemAviable = false;
				}
	
			}
			$scope.CompletedCount = $scope.ProcessOrderCompletedCount.length;
		});
	
		//For Counting RepackOrders
		repackservice.getRepackCompletedCount().then(function(response) {
			
			if (response != undefined) {
				$scope.RepackOrderCompletedCount = response.data;
					
			}
			
		});
		
		$scope.rejectOrder = function(processorderProdNumber) {
			var formErrorStatus = false;
			var submitMessage = $("#exampleFormControlTextarea1").val();
			if(!submitMessage || 0 === submitMessage.length || /^\s*$/.test(submitMessage)){
				//alert();
				$("#submitvalidationMessage").text("Please fill the valid reason for rejection.");
				formErrorStatus= true;
				
			}
			
			var processOrderNumber = $("#processOrdernumberTemp").val();
			if(!formErrorStatus){
				wareHouseService.setProcessOrderTrackerStatusForRejection(processOrderNumber,
						$window.sessionStorage.ssoId,submitMessage).then(function(response) {
							if (response != undefined) {
								$scope.ProcessTrackerId = response.data;
								$("#rejectModal").modal('show');
								$state.reload();
								$("#remarksmodal").modal('hide');
							}
						}
	
						);
			}
			
	
		};
	
		$scope.initiateorder = function(processorderProdNumber) {
			shareService.set(processorderProdNumber);
			$window.sessionStorage.processorderProdNumber = processorderProdNumber;
			$state.go('initiateOrder');
		};
		
		$scope.showReject = function(processorderProdNumber){
			//alert(processorderProdNumber);
			$("#areyousure").modal('show');
			$("#processOrdernumberTemp").val(processorderProdNumber);
		};
	
	});
	
	whApp.controller("initiateController1",function($rootScope, $scope, $http, $state, $localStorage,
			$window, wareHouseService1, shareService) {					
		
		$scope.customAlert = function(message){
				new PNotify({
		            text    : message,
		            confirm : {
		                confirm: true,
		                buttons: [
		                    {
		                        text    : 'Close',
		                        addClass: 'btn btn-link',
		                        click   : function (notice)
		                        {
		                            notice.remove();
		                        }
		                    },
		                    null
		                ]
		            },
		            buttons : {
		                closer : false,
		                sticker: false
		            },
		            animate : {
		                animate  : true,
		                in_class : 'slideInDown',
		                out_class: 'slideOutUp'
		            },
		            addclass: 'md multiline'
		        });
				
				}
		
	var count = "0";
	$scope.showDone = false;
	$scope.showRelease = true;
	
	$scope.processorderProdNumber = $window.sessionStorage.processorderProdNumber;
	
	wareHouseService1.setProcessOrderTrackerStatus($scope.processorderProdNumber,$window.sessionStorage.ssoId).then(
			function(response) {
	
				if (response != undefined) {
					$scope.ProcessTrackerId = response.data;
				}
	
				$window.sessionStorage.ProcessTrackerId = $scope.ProcessTrackerId;
			}
	
	);
	
	
	wareHouseService1.getProcessOrderDetails($scope.processorderProdNumber).then(
			function(response) {
				if (response != undefined) {
					$scope.processOrderDetails = response.data;
					$scope.productNumber = $scope.processOrderDetails[0].productTable.productNumber;
					//diabling the realeased items
					
					angular.element(document).ready(function () {
						for(var i = 0 ; i <$scope.processOrderDetails.length;i++){
							var processOrder = $scope.processOrderDetails[i];
							var status = processOrder.status.statusId;
							var phase = processOrder.phase;
							var rmId = processOrder.rawmaterialTable.rawmaterialNumber;
							if(status == "9"){
								count++;
								$("#release"+rmId+phase).css("background-color","#000000");
								$("#release"+rmId+phase).val("RM Released");
								$("#release"+rmId+phase).prop('disabled', true);
								$("#addWa"+rmId+phase).css("display", "none");
							}
						}
				    });
					
					//Checking Whether All RM released ,Then Show Done 
					angular.element(document).ready(function () {
						if ($scope.processOrderDetails.length == count) {
							$scope.showDone = true;
						}
						});				
				}
			});
	
	// *******************************************************************
	// function to insert with new locations
	var locationOfRm = [];
	
	$scope.save = function(locationDetails) {
	
		var processOrderRm = {
				location : "",
				batchNumber : "",
				quantityTaken : ""
		}
	
		processOrderRm.location = locationDetails.location;
		processOrderRm.batchNumber = locationDetails.batchNumber;
		processOrderRm.quantityTaken = locationDetails.quantityTaken;
	
		if (processOrderRm.length == 3)
			locationOfRm.push(processOrderRm);
	}
	
	// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$scope.displayResult = function($event, rmId , phase) {
		var btn = $event.currentTarget;
		$(btn)
		.closest("tbody")
		.append(
				'<tr style=" background-color: #f4ff9c; border-bottom: 2px solid #ffffff;"><td  colspan="2" ><label for="inputPassword1" class="label-custom">Batch Number</label><input type="number" id="bacthNo'+rmId+phase+'" class="bacthNo form-control md-has-value" required  ></td><td colspan="2" ><label for="inputPassword1" class="label-custom">Location</label> <input type="text" id="location'+rmId+phase+'" class="location form-control md-has-value" required  ></td> <td colspan="2"  ><label for="inputPassword1" class="label-custom">Qty</label> <input type="text" id="qty'+rmId+phase+'" class="qty form-control md-has-value" required></td><td ><label style="display:none;" for="inputPassword1" class="label-custom">Qty</label> <input style="display:none;" type="text" id="rawMaterialId" class="rawMaterialId qty form-control md-has-value" required value="'
				+rmId+phase
				+'"></td><td><button type="button" class="btn btn-danger fuse-ripple-ready"  onclick="remove(this);">- remove</button></td></tr> ');
		// document.getElementById("appender").
	};
	
	// end of new function
	// *******************************************************************
	$scope.release = function(processOrderDetail) {
		var formStatus = true;
		var assignedQuantity = 0 ;
		var rmId = processOrderDetail.rawmaterialTable.rawmaterialNumber;
		var rmDesc = processOrderDetail.rawmaterialTable.rawmaterialDescription;
		var phase = processOrderDetail.phase;
		//logic to check realease logic
		var parentQuantval = $("#parentQuality"+rmId+phase).text();
		var childQuantTag = "qty"+rmId+phase;
		var batchTag = "bacthNo"+rmId+phase;
		var locationTag = "location"+rmId+phase;
		
		//validation the batch null check
		$("[id='"+batchTag+"']").each(function(){
			var batchNo = $(this).val();
			
			
				
			if(batchNo != "" || batchNo != null ){
				if(Number.isInteger(parseInt(batchNo)) && (parseInt(batchNo.length) == 10)){
	
					formStatus = true;
			}
				else
					{
					formStatus = false;
					}
			}
			if(!Number.isInteger(parseInt(batchNo))){
				formStatus = false;
			}
			
			
		});
		if(!formStatus ){
			var message='Enter valid bacthNo for '+rmDesc;
			$scope.customAlert(message);
			return formStatus;
		}
		
		
		//validation of the location null check
		$("[id='"+locationTag+"']").each(function(){
			var locationNo = $(this).val();
			if(locationNo == "" || locationNo == null){
				//alert("batch no is empty");
				hasLocationNoError = true;
				$scope.hasLocError = true;
				//alert("Please fill the location no for "+rmDesc);
				formStatus = false;
			}
		});
		if(!formStatus ){
			var message='Enter valid Location for '+rmDesc;
			$scope.customAlert(message);
			return formStatus;
		}
		
		
		//integration throught t
		$("[id='"+childQuantTag+"']").each(function(){
			var temp = $(this).val();
			if(!isNaN(temp)){				
			assignedQuantity = parseFloat(assignedQuantity) +parseFloat(temp);
			assignedQuantity = assignedQuantity.toFixed(4);
			//alert('Total qty=>' + assignedQuantity);
			}
		});
		
		if(assignedQuantity != Number(parentQuantval) && rmId != '5947' && rmId != '9570' && rmId != '9527'){
			
			//alert(assignedQuantity);
			var message='Enter Required Quantity of RawMaterial for '+rmDesc;
			$scope.customAlert(message);
			return false;
		}
		
		//validation over
		//bar code generation starts
		
		var bacthNos = [];
		var locations = [];
		var qtys = [];
		var rawMaterialId = rmId;
		var data = [];
		var convertData = [];
		
		$("[id='"+batchTag+"']").each(function(){
			var batchNo = $(this).val();
			bacthNos.push(batchNo);
		});
		
		$("[id='"+locationTag+"']").each(function(){
			var locationNo = $(this).val();
			locations.push(locationNo.toUpperCase());
		});
		
		$("[id='"+childQuantTag+"']").each(function(){
			var quant = $(this).val();
			qtys.push(quant);
		});
		
		for (var i = 0; i < bacthNos.length; i++) {
			data[i] = {};
			data[i].batchNumber = bacthNos[i];
			data[i].location = locations[i];
			data[i].quantity = qtys[i];
			data[i].rawMaterialId = rmId;
		}
		
		var batchStr = "";
		var qtyStr = "";
		var locationStr = "";
		convertData = {};
		
		for (var i = 0; i < data.length; i++) {
			var outerData = data[i];
			
			batchStr += outerData.batchNumber + ",";
			locationStr += outerData.location + ",";
			qtyStr += outerData.quantity + ",";
		}
		
		if(rmId != '5947' && rmId != '9570' && rmId != '9527'){
			convertData.batchNumber = batchStr.substring(0,
					batchStr.length - 1);
			convertData.location = locationStr.substring(0,
					locationStr.length - 1);
			convertData.quantity = qtyStr.substring(0,
					qtyStr.length - 1);
			}
			else{
				
				convertData.batchNumber = "demo,demo";
				convertData.location = "demo,demo";
				convertData.quantity = "demo,demo";
			}
		$("#release"+rmId+phase).css("background-color","#000000");
		$("#release"+rmId+phase).val("RM Released");
		$("#release"+rmId+phase).prop('disabled', true);
		
		convertData.phase = processOrderDetail.phase;
		var rawMaterialId = processOrderDetail.rawmaterialTable.rawmaterialNumber;
		var phase = processOrderDetail.phase;
		$("#addWa"+rawMaterialId+phase).css("display","none");
		// ****************************
		var user = {
				ssoid : ""
		};
	
		var processTrackerTable = {
				rawmaterialNumber : "",
				processorderNumber : "",
				weightFromSensor : "",
				user : {}
	
		};
	
		processTrackerTable.rawmaterialNumber = processOrderDetail.rawmaterialTable.rawmaterialNumber;
		processTrackerTable.processorderNumber = processOrderDetail.processorderProd.processorderProdNumber;
		processTrackerTable.user.ssoid = $window.sessionStorage.ssoId;
		//Using weightFromSensor for storing phase for Validation
		processTrackerTable.weightFromSensor = processOrderDetail.phase;
		
		wareHouseService1.setTrackerStatusForRm(processTrackerTable).then(
				function(response) {
	
					if (response != undefined) {
						var message = 'RawMaterial Released successfully';
						// alert('added');
						$scope.ProcessTrackerId = response.data;
						count++;
						if ($scope.processOrderDetails.length == count) {
							$scope.showRelease = false;
							$scope.showDone = true;
						}
	
					}
	
				}
	
		);
		
		/*convertData = convertData.filter(function(x) {
			if (JSON.stringify(x) === '{}') { // This will
				// check if the
				// object is
				// empty
				return false;
			} else
				return true;
		});*/
		var processorderProdNumber=processOrderDetail.processorderProd.processorderProdNumber;
	wareHouseService1.setRmLocationDetails(
				processorderProdNumber, rmId ,convertData).then(
						function(response) {
							if (response != undefined) {
								if (response.status == '200') {
								}
							}
						});
	
	}
	
	
	$scope.hasBatchError=false;
	$scope.hasLocError=false;
	$scope.hasQtyError=false;
	
	$scope.done = function(processorderProdNumber) {
				$scope.showSuccess=false;
		$scope.ProcessTrackerId = $window.sessionStorage.ProcessTrackerId;
		wareHouseService1
		.updateProcessOrderStatus(
				processorderProdNumber,
				$scope.ProcessTrackerId,$window.sessionStorage.ssoId).then(
						function(response) {
							if (response != undefined) {
								if (response.status == '200') {
									$scope.showSuccess=true;
									$("#doneModal").modal('show');
									$state.go('processOrders');
								}
							}
						});
	}
	
	});
	
	whApp.controller("OrderHistoryController", function($scope, $state,
			$localStorage, $window, wareHouseService2) {
	
		wareHouseService2.getAllTodayProcessOrders().then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.TodayprocessOrders = response.data;
			}
	
		});
	});
	
	whApp.controller("WarehouseQualityController", function($scope, $state,
			$localStorage, $window, wareHouseService3) {
	
		wareHouseService3.getProcessOrdersForQuality().then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.processOrdersForQuality = response.data;
				
			}
			$scope.length = $scope.processOrdersForQuality.length;
		});
	
		$scope.PackingDetailsInfo = function(processorderProdNumber) {
			$window.sessionStorage.processorderProdNumber = processorderProdNumber;
			$state.go('PackingDetailsInfo');
		}
	
	});
	
	whApp.controller("WarehouseQualityController2",function($scope, $state, $localStorage, $window,wareHouseService4) {
	
				$scope.customAlert = function(message){
			new PNotify({
	            text    : message,
	            confirm : {
	                confirm: true,
	                buttons: [
	                    {
	                        text    : 'Close',
	                        addClass: 'btn btn-link',
	                        click   : function (notice)
	                        {
	                            notice.remove();
	                        }
	                    },
	                    null
	                ]
	            },
	            buttons : {
	                closer : false,
	                sticker: false
	            },
	            animate : {
	                animate  : true,
	                in_class : 'slideInDown',
	                out_class: 'slideOutUp'
	            },
	            addclass: 'md multiline'
	        });
			
			}
					
				$scope.showDone = false;
				var formStatus = true;
				var count = "0";
	
				wareHouseService4.getProcessOrderForQuality($window.sessionStorage.processorderProdNumber).then(
								function(response) {
									if (response != undefined) {
										$scope.processOrderForQualityAssurance = response.data;
									}
								});
	
				wareHouseService4.associatedPackingInfo($window.sessionStorage.processorderProdNumber).then(
						function(response) {
	
							if (response != undefined) {
								$scope.associatedOrders = response.data;
	
							}
							angular.element(document).ready(function () {
					for(var i = 0 ; i <$scope.associatedOrders.length;i++){
						var associatedOrder = $scope.associatedOrders[i];
						var status = associatedOrder.status.statusId;
						
						if(status == "2"){
							count++;
								
							$("#myButton1"+associatedOrder.processorderPckgNumber).css("background-color","#000000");
							$("#myButton1"+associatedOrder.processorderPckgNumber).val("Inventory Check Done");
							$("#myButton1"+associatedOrder.processorderPckgNumber).prop('disabled',true);  
							
						}
					}
			    });
				
				//Checking Whether All Inventory released ,Then Show Done
				angular.element(document).ready(function () {
				if ($scope.associatedOrders.length == count) {
					$scope.showDone = true;
				}
				});
			
			});
							
						
				$scope.CheckQuality = function(packingdetail) {
				
			var locationPlaced = $("#locationPlaced"+packingdetail.processorderPckgNumber).val();
			formStatus= true;	
			if( 0 === locationPlaced.length || /^\s*$/.test(locationPlaced)){
				
				formStatus= false;
				
			}
		
		if(!formStatus ){
			var message='Enter valid Location Detail for '+packingdetail.processorderPckgNumber;
			$scope.customAlert(message);
			return formStatus;
		}
		
		var containerQuantity = $("#containerQuantity"+packingdetail.processorderPckgNumber).val();
		
		if(!Number.isInteger(parseInt(containerQuantity))){
			formStatus = false;
		}
		
		if(!formStatus ){
			var message='Enter valid Conatiner Quantities for '+packingdetail.processorderPckgNumber;
			$scope.customAlert(message);
			return formStatus;
		}
				
		$("#myButton1"+packingdetail.processorderPckgNumber).css("background-color","#000000");
		$("#myButton1"+packingdetail.processorderPckgNumber).val("Inventory Check Done");
		$("#myButton1"+packingdetail.processorderPckgNumber).prop('disabled',true);  
							
		
					wareHouseService4.setTrackerForWarehouseQuality($window.sessionStorage.ssoId,locationPlaced,containerQuantity,packingdetail).then(
									function(response) {
										if (response != undefined) {
											$scope.SetQualityWh = response.data;
											count++;
											if ($scope.associatedOrders.length == count) {
												$scope.showDone = true;
											}
	
										}
	
									})
	
				}
	
				$scope.inventoryDone = function() {
					
				wareHouseService4.smsAlertForWHInventory($window.sessionStorage.processorderProdNumber,$window.sessionStorage.ssoId).then(
						function(response) {
							if (response != undefined) {
								
							}

						});
				
					wareHouseService4.setTrackerforInventory($window.sessionStorage.processorderProdNumber,$window.sessionStorage.ssoId).then(
									function(response) {
										if (response != undefined) {
											if (response.status == '200') {
												$("#successModal").modal('show');
												$state
												.go('WarehouseInventoryCheck');
											}
										}
	
									})
				}
	
			});
				
	whApp.controller("AllOrdersController", function($scope, $state,
			whAllOrdersService, $location) {

		$scope.showTodays = false;
		$scope.showAll = true;
		$scope.showImg = false;

		whAllOrdersService.getAllOrders().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.allOrders = response.data.data;

				if ($scope.allOrders.length == "0") {
					$scope.showImg = true;

				} else if ($scope.allOrders.length != "0") {
					$scope.showImg = false;

				}
			}

		});

		whAllOrdersService.getTodaysOrders().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.todaysOrders = response.data.data;
			}

		});

		whAllOrdersService.getPastOrders().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.pastOrders = response.data.data;
			}

		});

		whAllOrdersService.getFutureOrders().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.futureOrders = response.data.data;
			}

		});

		whAllOrdersService.getTodaysCompletedOrders().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.completedOrders = response.data.data;
			}

		});

		$scope.all = function() {

			$scope.showTodays = false;
			$scope.showAll = true;
			$scope.showPast = false;
			$scope.showFuture = false;
			$scope.showCompleted = false;

			if ($scope.allOrders.length == "0") {
				$scope.showImg = true;

			} else if ($scope.allOrders.length != "0") {
				$scope.showImg = false;

			}

		}

		$scope.todays = function() {
			$scope.showTodays = true;
			$scope.showAll = false;
			$scope.showPast = false;
			$scope.showFuture = false;
			$scope.showCompleted = false;

			if ($scope.todaysOrders.length == "0") {
				$scope.showImg = true;

			} else if ($scope.todaysOrders.length != "0") {
				$scope.showImg = false;

			}
		}

		$scope.past = function() {
			$scope.showTodays = false;
			$scope.showAll = false;
			$scope.showPast = true;
			$scope.showFuture = false;
			$scope.showCompleted = false;

			if ($scope.pastOrders.length == "0") {
				$scope.showImg = true;

			} else if ($scope.pastOrders.length != "0") {
				$scope.showImg = false;

			}

		}

		$scope.future = function() {
			$scope.showTodays = false;
			$scope.showAll = false;
			$scope.showPast = false;
			$scope.showCompleted = false;
			$scope.showFuture = true;

			if ($scope.futureOrders.length == "0") {
				$scope.showImg = true;

			} else if ($scope.futureOrders.length != "0") {
				$scope.showImg = false;

			}

		}

		$scope.finished = function() {
			$scope.showTodays = false;
			$scope.showAll = false;
			$scope.showPast = false;
			$scope.showFuture = false;
			$scope.showCompleted = true;

			if ($scope.completedOrders.length == "0") {
				$scope.showImg = true;

			} else if ($scope.completedOrders.length != "0") {
				$scope.showImg = false;

			}

		}

		$scope.allStatus = function(processorderProdNumber, approved) {
			//alert(approved);
			$scope.orderNumber = processorderProdNumber;
			//$scope.currentStatus = status;
			$scope.reWorked = approved;

			whAllOrdersService.getStatus(processorderProdNumber).then(
					function(response) {
						if (response.data !== 'undefined') {
							$scope.status = response.data.data;
						}

					});
			whAllOrdersService.getChargedStatus(processorderProdNumber).then(
					function(response) {
						if (response.data !== 'undefined') {
							$scope.chargedStatus = response.data.data;
						}

					});
			whAllOrdersService.getPackedStatus(processorderProdNumber).then(
					function(response) {
						if (response.data !== 'undefined') {
							$scope.packedStatus = response.data.data;
						}

					});
			whAllOrdersService.getQualityCheckStatus(processorderProdNumber).then(
					function(response) {
						if (response.data !== 'undefined') {
							$scope.qualityCheckStatus = response.data.data;
						}

					});
			whAllOrdersService.getQualityInspectionStatus(processorderProdNumber)
					.then(function(response) {
						if (response.data !== 'undefined') {
							$scope.qualityInspectionStatus = response.data.data;
						}

					});

			whAllOrdersService.getInventoryStatus(processorderProdNumber).then(
					function(response) {
						if (response.data !== 'undefined') {
							$scope.inventoryStatus = response.data.data;
						}

					});

			whAllOrdersService.getRmInventoryStatus(processorderProdNumber).then(
					function(response) {
						if (response.data !== 'undefined') {
							$scope.rmInventoryStatus = response.data.data;
						}

					});
		}

	});
	whApp.controller("WarehousePendingOrdersController", function($scope, $state,
			$localStorage, $window, wareHouseService5) {
	
		wareHouseService5.getAllPendingProcessOrders().then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.pendingProcessOrders = response.data;
			}
	
			$scope.length = $scope.pendingProcessOrders.length;
		});
	
		$scope.rejectOrder = function(processorderProdNumber) {
			var formErrorStatus = false;
			var submitMessage = $("#exampleFormControlTextarea1").val();
			if(!submitMessage || 0 === submitMessage.length || /^\s*$/.test(submitMessage)){
				//alert();
				$("#submitvalidationMessage").text("Please fill the valid reason for rejection.");
				formErrorStatus= true;
				
			}
			
			var processOrderNumber = $("#processOrdernumberTemp").val();
			if(!formErrorStatus){
				wareHouseService5.setProcessOrderTrackerStatusForRejection(processOrderNumber,
						$window.sessionStorage.ssoId,submitMessage).then(function(response) {
							if (response != undefined) {
								$scope.ProcessTrackerId = response.data;
								$("#rejectModal").modal('show');
								$state.reload();
								$("#remarksmodal").modal('hide');
							}
						}
	
						);
			}
			
	
		};
	
		
		$scope.showReject = function(processorderProdNumber){
			//alert(processorderProdNumber);
			$("#areyousure").modal('show');
			$("#processOrdernumberTemp").val(processorderProdNumber);
		};
		
		
		$scope.initiateorder = function(processorderProdNumber) {
			$window.sessionStorage.processorderProdNumber = processorderProdNumber;
			$state.go('PendingInitiateOrder');
		}
	
	});
	
	//*****************Warehouse InititateController2************************
	whApp.controller("initiateController2",function($rootScope, $scope, $http, $state, $localStorage,
			$window,  wareHouseService1, shareService) {
		$scope.customAlert = function(message){
			new PNotify({
	            text    : message,
	            confirm : {
	                confirm: true,
	                buttons: [
	                    {
	                        text    : 'Close',
	                        addClass: 'btn btn-link',
	                        click   : function (notice)
	                        {
	                            notice.remove();
	                        }
	                    },
	                    null
	                ]
	            },
	            buttons : {
	                closer : false,
	                sticker: false
	            },
	            animate : {
	                animate  : true,
	                in_class : 'slideInDown',
	                out_class: 'slideOutUp'
	            },
	            addclass: 'md multiline'
	        });
			
			}
	
	var count = "0";
	$scope.showDone = false;
	$scope.showRelease = true;
	
	$scope.processorderProdNumber = $window.sessionStorage.processorderProdNumber;
	wareHouseService1.setProcessOrderTrackerStatus($scope.processorderProdNumber,$window.sessionStorage.ssoId).then(
		function(response) {
	
			if (response != undefined) {
				$scope.ProcessTrackerId = response.data;
			}
	
			$window.sessionStorage.ProcessTrackerId = $scope.ProcessTrackerId;
		}
	
	);
	
	
	wareHouseService1.getProcessOrderDetails($scope.processorderProdNumber).then(
		function(response) {
			if (response != undefined) {
				$scope.processOrderDetails = response.data;
				// console.log();
				
				angular.element(document).ready(function () {
					for(var i = 0 ; i <$scope.processOrderDetails.length;i++){
						var processOrder = $scope.processOrderDetails[i];
						var status = processOrder.status.statusId;
						var phase = processOrder.phase;
						var rmId = processOrder.rawmaterialTable.rawmaterialNumber;
						if(status == "9"){
							count++;
							$("#release"+rmId+phase).css("background-color","#000000");
							$("#release"+rmId+phase).val("RM Released");
							$("#release"+rmId+phase).prop('disabled', true);
							$("#addWa"+rmId+phase).css("display", "none");
						}
					}
			    });
				
				//Checking Whether All RM released ,Then Show Done
				angular.element(document).ready(function () {
				if ($scope.processOrderDetails.length == count) {
					$scope.showDone = true;
				}
				});
			
			}
		});
	
	//*******************************************************************
	//function to insert with new locations
	var locationOfRm = [];
	
	$scope.save = function(locationDetails) {
	
	var processOrderRm = {
			location : "",
			batchNumber : "",
			quantityTaken : ""
	}
	
	processOrderRm.location = locationDetails.location;
	processOrderRm.batchNumber = locationDetails.batchNumber;
	processOrderRm.quantityTaken = locationDetails.quantityTaken;
	
	if (processOrderRm.length == 3)
		locationOfRm.push(processOrderRm);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$scope.displayResult = function($event, rmId ,phase) {
	// console.log(button);
	var btn = $event.currentTarget;
	$(btn)
	.closest("tbody")
	.append(
			'<tr style=" background-color: #f4ff9c; border-bottom: 2px solid #ffffff;"><td  colspan="2" ><label for="inputPassword1" class="label-custom">Batch Number</label><input type="number" id="bacthNo'+rmId+phase+'" class="bacthNo form-control md-has-value" required  ></td><td colspan="2" ><label for="inputPassword1" class="label-custom">Location</label> <input type="text" id="location'+rmId+phase+'" class="location form-control md-has-value" required  ></td> <td colspan="2"  ><label for="inputPassword1" class="label-custom">Qty</label> <input type="text" id="qty'+rmId+phase+'" class="qty form-control md-has-value" required></td><td ><label style="display:none;" for="inputPassword1" class="label-custom">Qty</label> <input style="display:none;" type="text" id="rawMaterialId" class="rawMaterialId qty form-control md-has-value" required value="'
			+rmId+phase
			+'"></td><td><button type="button" class="btn btn-danger fuse-ripple-ready"  onclick="remove(this);">- remove</button></td></tr> ');
	// document.getElementById("appender").
	};
	
	//end of new function
	//*******************************************************************
	$scope.release = function(processOrderDetail) {
	var formStatus = true;
	var assignedQuantity = 0 ;
	var rmId = processOrderDetail.rawmaterialTable.rawmaterialNumber;
	var rmDesc = processOrderDetail.rawmaterialTable.rawmaterialDescription;
	var phase = processOrderDetail.phase;
	//logic to check realease logic
	var parentQuantval = $("#parentQuality"+rmId+phase).text();
	var childQuantTag = "qty"+rmId+phase;
	var batchTag = "bacthNo"+rmId+phase;
	var locationTag = "location"+rmId+phase;
	
	//validation the batch null check
	$("[id='"+batchTag+"']").each(function(){
		var batchNo = $(this).val();
		
		
			
		if(batchNo != "" || batchNo != null ){
			if(Number.isInteger(parseInt(batchNo)) && (parseInt(batchNo.length) == 10)){
	
				formStatus = true;
		}
			else
				{
				formStatus = false;
				}
		}
		if(!Number.isInteger(parseInt(batchNo))){
			formStatus = false;
		}
		
		
	});
	if(!formStatus ){
		var message='Enter valid bacthNo for '+rmDesc;
		$scope.customAlert(message);
		return formStatus;
	}
	
	
	//validation of the location null check
	$("[id='"+locationTag+"']").each(function(){
		var locationNo = $(this).val();
		if(locationNo == "" || locationNo == null){
			//alert("batch no is empty");
			hasLocationNoError = true;
			$scope.hasLocError = true;
			//alert("Please fill the location no for "+rmDesc);
			formStatus = false;
		}
	});
	if(!formStatus ){
		var message='Enter valid Location for '+rmDesc;
		$scope.customAlert(message);
		return formStatus;
	}
	
	
	//integration throught t
	$("[id='"+childQuantTag+"']").each(function(){
		var temp = $(this).val();
		if(!isNaN(temp)){
		//assignedQuantity = Number(assignedQuantity) +Number(temp);
			assignedQuantity = parseFloat(assignedQuantity) +parseFloat(temp);
			assignedQuantity = assignedQuantity.toFixed(4);
		}
	});
	
	if(assignedQuantity != Number(parentQuantval) && rmId != '5947' && rmId != '9570' && rmId != '9527'){
		
		
		var message='Enter Required Quantity of RawMaterial for '+rmDesc;
		$scope.customAlert(message);
		return false;
	}
	
	
	//validation over
	//bar code generation starts
	
	var bacthNos = [];
	var locations = [];
	var qtys = [];
	var rawMaterialId = rmId;
	var data = [];
	var convertData = [];
	
	$("[id='"+batchTag+"']").each(function(){
		var batchNo = $(this).val();
		bacthNos.push(batchNo);
	});
	
	$("[id='"+locationTag+"']").each(function(){
		var locationNo = $(this).val();
		locations.push(locationNo.toUpperCase());
	});
	
	$("[id='"+childQuantTag+"']").each(function(){
		var quant = $(this).val();
		qtys.push(quant);
	});
	
	for (var i = 0; i < bacthNos.length; i++) {
		data[i] = {};
		data[i].batchNumber = bacthNos[i];
		data[i].location = locations[i];
		data[i].quantity = qtys[i];
		data[i].rawMaterialId = rmId;
	}
	
	var batchStr = "";
	var qtyStr = "";
	var locationStr = "";
	var convertData = {};
	for (var i = 0; i < data.length; i++) {
		var outerData = data[i];
		batchStr += outerData.batchNumber + ",";
		locationStr += outerData.location + ",";
		qtyStr += outerData.quantity + ",";
	}
	
	if(rmId != '5947' && rmId != '9570' && rmId != '9527'){
	convertData.batchNumber = batchStr.substring(0,
			batchStr.length - 1);
	convertData.location = locationStr.substring(0,
			locationStr.length - 1);
	convertData.quantity = qtyStr.substring(0,
			qtyStr.length - 1);
	}
	else{
		
		convertData.batchNumber = "demo,demo";
		convertData.location = "demo,demo";
		convertData.quantity = "demo,demo";
	}

	
	$("#release"+rmId+phase).css("background-color","#000000");
	$("#release"+rmId+phase).val("RM Released");
	$("#release"+rmId+phase).prop('disabled', true);
	
	convertData.phase = processOrderDetail.phase;// setting Phase for Conver Data for Generating barcode;
	var rawMaterialId = processOrderDetail.rawmaterialTable.rawmaterialNumber;
	var phase = processOrderDetail.phase;
	$("#addWa"+rawMaterialId+phase).css("display","none");
	// ****************************
	var user = {
			ssoid : ""
	};
	
	var processTrackerTable = {
			rawmaterialNumber : "",
			processorderNumber : "",
			weightFromSensor : "",
			user : {}
	
	};
	
	processTrackerTable.rawmaterialNumber = processOrderDetail.rawmaterialTable.rawmaterialNumber;
	processTrackerTable.processorderNumber = processOrderDetail.processorderProd.processorderProdNumber;
	processTrackerTable.user.ssoid = $window.sessionStorage.ssoId;
	//Using weightFromSensor for storing phase for Validation
	processTrackerTable.weightFromSensor = processOrderDetail.phase;
	
	
	
	wareHouseService1.setTrackerStatusForRm(processTrackerTable).then(
			function(response) {
	
				if (response != undefined) {
					var message = 'RawMaterial Released successfully';
					// alert('added');
					/*Flash.create('success',	message);
*/					$scope.ProcessTrackerId = response.data;
					count++;
					if ($scope.processOrderDetails.length == count) {
						$scope.showRelease = false;
						$scope.showDone = true;
					}
	
				}
	
			}
	
	);
	
	
	var processorderProdNumber=processOrderDetail.processorderProd.processorderProdNumber;
wareHouseService1.setRmLocationDetails(
			processorderProdNumber, rmId ,convertData).then(
					function(response) {
						if (response != undefined) {
							if (response.status == '200') {
							}
						}
					});
	
	}
	
	
	$scope.checkContains = function(array, value) {
	var status = false;
	for (var i = 0; i < array.length; i++) {
		if (array[i].rawMaterialId == value) {
			status = true;
		}
	}
	return status;
	}
	
	
	$scope.hasBatchError=false;
	$scope.hasLocError=false;
	$scope.hasQtyError=false;
	
	$scope.done = function(processorderProdNumber) {
		$scope.showSuccess=false;
	$scope.ProcessTrackerId = $window.sessionStorage.ProcessTrackerId;
	wareHouseService1
	.updateProcessOrderStatus(
			processorderProdNumber,
			$scope.ProcessTrackerId,$window.sessionStorage.ssoId).then(
					function(response) {
						if (response != undefined) {
							if (response.status == '200') {
								$scope.showSuccess=true;
								$("#doneModal").modal('show');
								$state.go('PendingOrders');
							}
						}
					});
	}
	});
	
	whApp.controller("HistoryOfContainersController", function($scope, $state,
			$localStorage, $window, wareHouseService8) {
	
		wareHouseService8.getAllContainerDetailsForProcessOrders().then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.ContainerDetailsForProcessOrders = response.data;
			}
	
		});
		
		$scope.viewDetails = function(processorderProdNumber){
			
			$window.sessionStorage.processorderProdNumber=processorderProdNumber;
			$state.go('detailsOfContainers');
		}
		
		
	});
	
	
	whApp.controller("HistoryOfContainersController1", function($scope, $state,
			$localStorage, $window, wareHouseService8) {
	
		wareHouseService8.getAllContainerDetailsForRepackProcessOrders().then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.ContainerDetailsForRepackProcessOrders = response.data;
			}
	
		});
		
		$scope.repackviewDetails = function(processorderPckgNumber){
			
			$window.sessionStorage.processorderPckgNumber=processorderPckgNumber;
			$state.go('detailsOfContainers1');
		}
		
		
	});
	
	
	whApp.controller("detailsOfContainers", function($scope, $state,
			$localStorage, $window, wareHouseService9) {
		
		$scope.processorderProdNumber=$window.sessionStorage.processorderProdNumber;
		wareHouseService9.getContainerDetailsForProcessOrder($scope.processorderProdNumber).then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.getContainerDetailsForProcessOrder = response.data;
			}
	
		});
		
		
		
	});
	
	whApp.controller("detailsOfContainers1", function($scope, $state,
			$localStorage, $window, wareHouseService9) {
		
		$scope.processorderPckgNumber=$window.sessionStorage.processorderPckgNumber;
		wareHouseService9.getContainerDetailsForRepackProcessOrder($scope.processorderPckgNumber).then(function(response) {
			// $scope.isItemAviable = false;
			if (response != undefined) {
				$scope.getContainerDetailsForRepackProcessOrder = response.data;
			}
	
		});
		
		
	});

	
	whApp.controller("repackInventoryInitiate", function($scope, $state,
			$localStorage, $window, wareHouseService4) {
		
		wareHouseService4.getPackingOrderForInventory().then(
				function(response) {
					if (response != undefined) {
						$scope.packingOrderForInventory = response.data;
						
					}

				})
				
				$scope.initiateRepack = function(processorderPckgNumber) {
			$window.sessionStorage.processorderPckgNumber = processorderPckgNumber;
			$state.go('repack');
		};
		
	});
	
	
	whApp.controller("repackInventory", function($scope, $state,
			$localStorage, $window, wareHouseService4) {
		
		$scope.processorderPckgNumber = $window.sessionStorage.processorderPckgNumber;
		
		wareHouseService4.getPackingOrderForInventory1($scope.processorderPckgNumber).then(
				function(response) {
					if (response != undefined) {
						$scope.packingOrderForInventory = response.data;
						
					}

				})
		
		$scope.customAlert = function(message){
			new PNotify({
	            text    : message,
	            confirm : {
	                confirm: true,
	                buttons: [
	                    {
	                        text    : 'Close',
	                        addClass: 'btn btn-link',
	                        click   : function (notice)
	                        {
	                            notice.remove();
	                        }
	                    },
	                    null
	                ]
	            },
	            buttons : {
	                closer : false,
	                sticker: false
	            },
	            animate : {
	                animate  : true,
	                in_class : 'slideInDown',
	                out_class: 'slideOutUp'
	            },
	            addclass: 'md multiline'
	        });
			
			}
		
		
				
		$scope.CheckQuality = function(packingdetail) {
		var locationPlaced = $("#locationPlaced"+packingdetail.processorderPckgNumber).val();
		var formStatus= true;	
		if( 0 === locationPlaced.length || /^\s*$/.test(locationPlaced)){
			
			formStatus= false;
			
		}
	
	if(!formStatus ){
		var message='Enter valid Location Detail for '+packingdetail.processorderPckgNumber;
		$scope.customAlert(message);
		return formStatus;
	}
	
	var containerQuantity = $("#containerQuantity"+packingdetail.processorderPckgNumber).val();
	
	if(!Number.isInteger(parseInt(containerQuantity))){
		formStatus = false;
	}
	
	if(!formStatus ){
		var message='Enter valid Conatiner Quantities for '+packingdetail.processorderPckgNumber;
		$scope.customAlert(message);
		return formStatus;
	}
	
			
	$("#myButton1"+packingdetail.processorderPckgNumber).css("background-color","#000000");
	$("#myButton1"+packingdetail.processorderPckgNumber).val("Inventory Check Done");
	$("#myButton1"+packingdetail.processorderPckgNumber).prop('disabled',true);  
	
	wareHouseService4.smsAlertForWHInventory($window.sessionStorage.processorderPckgNumber,$window.sessionStorage.ssoId).then(
			function(response) {
				if (response != undefined) {
					
				}

			});
	
	wareHouseService4.setTrackerForWarehouseQuality($window.sessionStorage.ssoId,locationPlaced,containerQuantity,packingdetail).then(
								function(response) {
									if (response != undefined) {
										$scope.SetQualityWh = response.data;
										$("#successModal").modal('show');

										$state.go('repackInventoryInitiate');
										

									}

								})
		}
				
	});
	
	whApp.controller("repackPOForRmRelease", function($scope, $state,$localStorage, $window, repackPOForRmReleaseService) {
		
		repackPOForRmReleaseService.getRePackingOrderForRmRelease().then(
					function(response) {
						if (response != undefined) {
							$scope.rePackingOrderForRM = response.data;
							
						}

					});
		
		$scope.initiateRepackorder = function(processorderPckgNumber) {
			
			$window.sessionStorage.processorderPckgNumber = processorderPckgNumber;
			$state.go('initiateRepackOrderForRM');
		};
					
});

	whApp.controller("repackPOForRmDetailsRelease", function($scope, $state,$localStorage, $window, repackPOForRmDetailsService ,wareHouseService1) {
		
		$scope.processorderPckgNumber = $window.sessionStorage.processorderPckgNumber;
		
		repackPOForRmDetailsService.getRePackingOrderDetailsForRmRelease($scope.processorderPckgNumber).then(
					function(response) {
						if (response != undefined) {
							$scope.rePackingOrderDetailsForRM = response.data;
							
						}
					});
		
		    $scope.customAlert = function(message){
			new PNotify({
	            text    : message,
	            confirm : {
	                confirm: true,
	                buttons: [
	                    {
	                        text    : 'Close',
	                        addClass: 'btn btn-link',
	                        click   : function (notice)
	                        {
	                            notice.remove();
	                        }
	                    },
	                    null
	                ]
	            },
	            buttons : {
	                closer : false,
	                sticker: false
	            },
	            animate : {
	                animate  : true,
	                in_class : 'slideInDown',
	                out_class: 'slideOutUp'
	            },
	            addclass: 'md multiline'
	        });
			
			}
		    
		 // *******************************************************************
	        // function to insert with new locations
	        var locationOfRm = [];
	        
	        $scope.save = function(locationDetails) {
	        
	                var processOrderRm = {
	                                location : "",
	                                batchNumber : "",
	                                quantityTaken : ""
	                }
	        
	                processOrderRm.location = locationDetails.location;
	                processOrderRm.batchNumber = locationDetails.batchNumber;
	                processOrderRm.quantityTaken = locationDetails.quantityTaken;
	        
	        }
	        
	        // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	        $scope.displayResult = function($event, rmId) {
	                var btn = $event.currentTarget;
	                $(btn)
	                .closest("tbody")
	                .append(
	                                '<tr style=" background-color: #f4ff9c; border-bottom: 2px solid #ffffff;"><td  colspan="2" ><label for="inputPassword1" class="label-custom">Batch Number</label><input type="number" id="bacthNo'+rmId+'" class="bacthNo form-control md-has-value" required  ></td><td colspan="2" ><label for="inputPassword1" class="label-custom">Location</label> <input type="text" id="location'+rmId+'" class="location form-control md-has-value" required  ></td> <td colspan="2"  ><label for="inputPassword1" class="label-custom">Qty</label> <input type="text" id="qty'+rmId+'" class="qty form-control md-has-value" required></td><td ><label style="display:none;" for="inputPassword1" class="label-custom">Qty</label> <input style="display:none;" type="text" id="rawMaterialId" class="rawMaterialId qty form-control md-has-value" required value="'
	                                + rmId
	                                + '"></td><td><button type="button" class="btn btn-danger fuse-ripple-ready"  onclick="remove(this);">- remove</button></td></tr> ');
	                // document.getElementById("appender").
	        };
	        
	        // end of new function
	        // *******************************************************************
	        $scope.release = function(processOrderDetail) {
	        	
	                var formStatus = true;
	                var assignedQuantity = 0 ;
	                
	                var rmId = processOrderDetail.productNumber;
	                var rmDesc = processOrderDetail.productName;

	                //logic to check realease logic
	                var parentQuantval = $("#parentQuality"+rmId).text();
	                var childQuantTag = "qty"+rmId;
	                var batchTag = "bacthNo"+rmId;
	                var locationTag = "location"+rmId;
	                
	                //validation the batch null check
	                $("[id='"+batchTag+"']").each(function(){
	                        var batchNo = $(this).val();
	                        
	                                
	                        if(batchNo != "" || batchNo != null ){
	                                if(Number.isInteger(parseInt(batchNo)) && (parseInt(batchNo.length) == 10)){
	        
	                                        formStatus = true;
	                        }
	                                else
	                                        {
	                                        formStatus = false;
	                                        }
	                        }
	                        if(!Number.isInteger(parseInt(batchNo))){
	                                formStatus = false;
	                        }
	                        
	                        
	                });
	                
	                if(!formStatus ){
	                        var message='Enter valid bacthNo for '+rmDesc;
	                        $scope.customAlert(message);
	                        return formStatus;
	                }
	                
	                
	                //validation of the location null check
	                $("[id='"+locationTag+"']").each(function(){
	                        var locationNo = $(this).val();
	                        if(locationNo == "" || locationNo == null){
	                                //alert("batch no is empty");
	                                hasLocationNoError = true;
	                                $scope.hasLocError = true;
	                                //alert("Please fill the location no for "+rmDesc);
	                                formStatus = false;
	                        }
	                });
	                if(!formStatus ){
	                        var message='Enter valid Location for '+rmDesc;
	                        $scope.customAlert(message);
	                        return formStatus;
	                }
	                
	                
	                //integration throught t
	                $("[id='"+childQuantTag+"']").each(function(){
	                        var temp = $(this).val();
	                        if(!isNaN(temp)){
	                        	assignedQuantity = parseFloat(assignedQuantity) +parseFloat(temp);
	                			assignedQuantity = assignedQuantity.toFixed(4);
	                        }
	                });
	                
	                if(assignedQuantity != Number(parentQuantval) ){
	                        
	                        
	                        var message='Enter Required Quantity of RawMaterial for '+rmDesc;
	                        $scope.customAlert(message);
	                        return false;
	                }
	                
	                //validation over
	                //bar code generation starts
	                
	                var bacthNos = [];
	                var locations = [];
	                var qtys = [];
	                var rawMaterialId = rmId;
	                var data = [];
	                var convertData = [];
	                
	                $("[id='"+batchTag+"']").each(function(){
	                        var batchNo = $(this).val();
	                        bacthNos.push(batchNo);
	                });
	                
	                $("[id='"+locationTag+"']").each(function(){
	                        var locationNo = $(this).val();
	                        locations.push(locationNo.toUpperCase());
	                });
	                
	                $("[id='"+childQuantTag+"']").each(function(){
	                        var quant = $(this).val();
	                        qtys.push(quant);
	                });
	                
	                for (var i = 0; i < bacthNos.length; i++) {
	                        data[i] = {};
	                        data[i].batchNumber = bacthNos[i];
	                        data[i].location = locations[i];
	                        data[i].quantity = qtys[i];
	                        data[i].rawMaterialId = rmId;
	                }
	                
	                var batchStr = "";
	                var qtyStr = "";
	                var locationStr = "";
	                var convertData = {};
	                
	                for (var i = 0; i < data.length; i++) {
	                        var outerData = data[i];
	                        
	                        batchStr += outerData.batchNumber + ",";
	                        locationStr += outerData.location + ",";
	                        qtyStr += outerData.quantity + ",";
	                }
	                
	                
	                        convertData.batchNumber = batchStr.substring(0,
	                                        batchStr.length - 1);
	                        convertData.location = locationStr.substring(0,
	                                        locationStr.length - 1);
	                        convertData.quantity = qtyStr.substring(0,
	                                        qtyStr.length - 1);
	                        
	                $("#release"+rmId).css("background-color","#000000");
	                $("#release"+rmId).val("RM Released");
	                $("#release"+rmId).prop('disabled', true);
	                
	                
	                var rawMaterialId = processOrderDetail.productNumber;
	                $("#addWa"+rawMaterialId).css("display","none");
	                // ****************************
	                var user = {
	                                ssoid : ""
	                };
	        
	                var processTrackerTable = {
	                                rawmaterialNumber : "",
	                                processorderNumber : "",
	                                user : {}
	        
	                };


	        	        
	    // console.log(convertData);
	                convertData.phase = processOrderDetail.phase;// Setting Phase for Convert Data for Generating Barcode
	    	var processorderPckgNumber=processOrderDetail.processorderPckgNumber;
	    	var ssoId = $window.sessionStorage.ssoId;
	    wareHouseService1.setRmLocationDetailsForRepack(
	    		processorderPckgNumber,ssoId ,convertData).then(
	    					function(response) {
	    						if (response != undefined) {
	    							if (response.status == '200') {
	    								$("#doneModal").modal('show');
	    								$state.go('repackForRM');
	    							}
	    						}
	    					});
	        }
	    	
		});
	
	
	whApp.controller("WarehouseProcessOrderInventoryPosting", function($scope, $state,
			$localStorage, $window, InventoryPosting) {
	
		InventoryPosting.getProcessOrdersForPosting().then(function(response) {
			
			if (response != undefined) {
				$scope.processOrdersForPosting = response.data;
				
			}
			$scope.length = $scope.processOrdersForPosting.length;
		});
	
		$scope.PackingDetailsInfoForPosting = function(processorderProdNumber) {
			$window.sessionStorage.processorderProdNumber = processorderProdNumber;
			$state.go('PackingDetailsInfoForPosting');
		}
	
	});

whApp.controller("WarehouseProcessOrderInventoryPosting2",function($scope, $state, $localStorage, $window,InventoryPosting2) {
	
				
				InventoryPosting2.getProcessOrderForPosting($window.sessionStorage.processorderProdNumber).then(
								function(response) {
									if (response != undefined) {
										$scope.processOrderForQualityAssurance = response.data;
									}
								})
	
				InventoryPosting2.associatedPackingInfoForPosting($window.sessionStorage.processorderProdNumber).then(
						function(response) {
							if (response != undefined) {
								$scope.associatedOrders = response.data;
	
							}
						})
							
				$scope.Done = function() {
												
					InventoryPosting2.setProcessOrderStatusForPosting($window.sessionStorage.processorderProdNumber ,$window.sessionStorage.ssoId).then(
									function(response) {
										if (response != undefined) {
											if (response.status == '200') {

												$("#successModal").modal('show');
												$state
												.go('WarehouseInventoryPosting');

											}
										}
	
									})
				}
	
});


whApp.controller("repackInventoryPostingInitiate", function($scope, $state,
		$localStorage, $window, repackPosting) {
	
	repackPosting.getPackingOrderForInventoryPostingList().then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data;
					
				}

			})
			
			$scope.initiateRepack = function(processorderPckgNumber) {
		$window.sessionStorage.processorderPckgNumber = processorderPckgNumber;
		$state.go('repackInventoryPostingDetailsList');
	};
	
});


whApp.controller("repackInventoryPostingDetails", function($scope, $state,
		$localStorage, $window, repackPosting2) {
	
	$scope.processorderPckgNumber = $window.sessionStorage.processorderPckgNumber;
	
	repackPosting2.getPackingOrderForInventoryPosting($scope.processorderPckgNumber).then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data;
					
				}

			})
	
	
			$scope.Done = function() {

		repackPosting2.statusProcessOrderPackPosting($window.sessionStorage.processorderPckgNumber,$window.sessionStorage.ssoId).then(
				function(response) {
					if (response != undefined) {
						if (response.status == '200') {

							$("#successModal").modal('show');
							$state
							.go('RepackInventoryPosting');

						}
					}

				})
	}
});



	