whApp.controller("ProcessHistoryController", function($scope, $state, wareHouseService, $window) {	
	
	
	wareHouseService.getProcessHistory().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.processDetails = response.data.data;
				$scope.processDetailsCount = $scope.processDetails.length;
				//$state.reload();
			}

		});
		
		
		$scope.historyDetails = function(processorderProdNumber) {
			$window.sessionStorage.processorderHistory = processorderProdNumber;
			//alert($window.sessionStorage.processorderHistory);
			$state.go('historyfirst');
		}
	

});