whApp.controller("RepackingHistoryDetailsController", function($scope, $state,
		$localStorage, $window, wareHouseService) {
	
	$scope.processorderPckgNumber = $window.sessionStorage.processorderRepackHistoryPckgNumber;
	
	wareHouseService.getRepackProcessOrderForHistory($scope.processorderPckgNumber).then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data;
					
				}

			})
			
$scope.getVesselHrs =  function(){
		
		$scope.vesselHrsProcess = $scope.processorderPckgNumber;
		
		wareHouseService.getRepackPackinVesselHrs($scope.processorderPckgNumber)
		.then(function(response) {
			
			if (response.data !== 'undefined') {
				$scope.packingVesselHrs = response.data.data;
				//console.log($scope.packingVesselHrs);

		
			}
		});
	}
	
			
});