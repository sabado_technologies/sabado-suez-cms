myAdmin.controller("AddUserController", function($scope, $state, Flash,
		AdminService, $location, $window) {

	// /.....................Calling service to get All
	// Roles..............................
	AdminService.getRoles().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.allRoles = response.data.data;
		}

	});

	$scope.getStateByCountry = function() {
		$scope.country = $("#countries_states1 option:selected").text();
	}
	$scope.getState = function() {
		$scope.state = $("#state option:selected").text();
	}

	// /....................Passing user details to Add New
	
$scope.selected=[];
	
    $scope.toggleSelection = function(user){
		
		var idx=$scope.selected.indexOf(user);
		if(idx > -1){
			$scope.selected.splice(idx, 1);
			$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
			
		}
		else{
			$scope.selected.push(user);
			$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
		}
		
		if($scope.selected.length == ''){
			$scope.selectedUsers="";
		}
	}
	// User..........................
	$scope.addUser = function(userDetails) {
		
		if($scope.selected.length == 0){
			//alert('Please Select Role');
			$scope.showRole = true;
			return;
		}
		if ($scope.userDetails== undefined){
			Flash.clear();
			var message = "";
			message = 'Please fill all fields';
			Flash.create('danger', message);
			return;
		}
		
		var userRoles =$scope.selected[0].roleId;
		for(var i=1; i< $scope.selected.length;i++){ 
			if($scope.selected[i].roleId < userRoles){
				userRoles = $scope.selected[i].roleId;		
	           }		
		}	
		 
		$scope.userDetails.roleId = userRoles;
		$scope.userDetails.createdBy = $window.sessionStorage.ssoId;
		
		if (('' + $scope.userDetails.phonenumber).length < "19") {
			$scope.showPhone = true;
			return;

		} else if ($scope.userDetails.roleId == undefined) {
			$scope.showState = true;
			return;

		}

		/*$scope.userDetails.address = userDetails.address1 + ","
				+ userDetails.address2 + "," + userDetails.city + ","
				+ $scope.state + "," + $scope.country;*/
				
		var diffRoles =[];
		
		// ....................Assigning role-Id based on
		for(var i=0; i< $scope.selected.length;i++){
			//alert($scope.selected[i].roleId);
			diffRoles.push($scope.selected[i].roleId);
			//alert(diffRoles);
			
		}/*
		alert("Length =>" + Object.keys(userDetails).length);
		if(Object.keys(userDetails).length == "7" || Object.keys(userDetails).length > "7"){
			alert('OK');
			
		}*/
		// role............................
		
		//alert("Length =>" + Object.keys(userDetails).length);

		// /...........Checking the length of User Deatils before passing to
		// service.......
		if (Object.keys(userDetails).length == "6" || Object.keys(userDetails).length > "6	") {

			AdminService.addUser($scope.userDetails , diffRoles).then(
					function(response) {
						if (response.data != undefined) {
							if (response.data.code === '200') {
								Flash.clear();
								var message = response.data.data + " "
										+ 'Added successfully';
								Flash.create('success', message); // Display
								// message
								// if User
								// added
								$state.reload();
								

							} else if (response.data.code === '300') {
								$scope.existing = response.data.data;
								Flash.clear();
								var message = response.data.data + " "
										+ 'Already Exist';
								Flash.create('danger', message); // Display
								// message
								// if User
								// added

							} else {
								Flash.clear();
								var message = 'Adding' + response.data.data
										+ 'is failed';
								Flash.create('danger', message); // Display
								// message
								// if adding
								// user
								// fails
								//$state.reload();
							}
						}
					})

		} else {
			var message = "";
			message = 'Please fill all fields';
			Flash.create('danger', message);
		}
	}

	$scope.refresh = function() {
		$state.reload();
	}

});