myAdmin.controller("UserAlertsController", function($scope, $state, Flash,
		UserAlertService) {
	
	function alertForError(message){
		new PNotify({
	        text    : message,
	        confirm : {
	            confirm: true,
	            buttons: [
	                {
	                    text    : 'Close',
	                    addClass: 'btn btn-link',
	                    click   : function (notice)
	                    {
	                        notice.remove();
	                    }
	                },
	                null
	            ]
	        },
	        buttons : {
	            closer : false,
	            sticker: false
	        },
	        animate : {
	            animate  : true,
	            in_class : 'slideInDown',
	            out_class: 'slideOutUp'
	        },
	        addclass: 'md multiline'
	    });
		
	}

	UserAlertService.getUsersForAlerts().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.users = response.data.data;			
		}

	});
	
	$scope.addUser = function(){
		
		UserAlertService.addUserForAlerts($scope.userDetails).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					var message ="User added to receive Alerts";
					alertForError(message);
					$state.reload();
				}
				else{
					var message ="Please Try Again!";
					alertForError(message);
				}
			}

		});
	}
	
	$scope.editUser = function(user){
		$scope.editInfo = user;
	}
	
	$scope.upadteUserInfo = function(userId){
		
		UserAlertService.updateUserAlertInfo($scope.editInfo).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					var message ="User information updated successfully";
					alertForError(message);
					$state.reload();
				}
				else{
					var message ="Please Try Again!";
					alertForError(message);
				}
			}

		});
	}
	
	$scope.confirm = function(user){
		$scope.deleteInfo = user;
	}
	
	$scope.deleteUser = function(userId){
		
		UserAlertService.deleteUserAlertInfo(userId).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					var message ="User deleted successfully";
					alertForError(message);
					$state.reload();
				}
				else{
					var message ="Please Try Again!";
					alertForError(message);
				}
			}

		});
	}
	
});