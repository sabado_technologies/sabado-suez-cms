myAdmin.controller("RepackingHistoryDetailsController", function($scope, $state,
		$localStorage, $window, AdminService) {
	
	$scope.processorderPckgNumber = $window.sessionStorage.processorderRepackHistoryPckgNumber;
	
	AdminService.getRepackProcessOrderForHistory($scope.processorderPckgNumber).then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data;
					
				}

			})
	
$scope.getVesselHrs =  function(){
		
		$scope.vesselHrsProcess = $scope.processorderPckgNumber;
		
		AdminService.getRepackPackinVesselHrs($scope.processorderPckgNumber)
		.then(function(response) {
			
			if (response.data !== 'undefined') {
				$scope.packingVesselHrs = response.data.data;
				//console.log($scope.packingVesselHrs);

		
			}
		});
	}
			
});