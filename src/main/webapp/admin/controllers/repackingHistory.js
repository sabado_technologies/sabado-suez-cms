myAdmin.controller("RepackingHistoryController", function($scope, $state,
		$localStorage, $window, AdminService) {
	
	AdminService.getRepackProcessHistory().then(
			function(response) {
				if (response != undefined) {
					$scope.packingOrderForInventory = response.data.data;
					//console.log
					
				}

			})
			
			$scope.initiateRepack = function(processorderPckgNumber) {
		$window.sessionStorage.processorderRepackHistoryPckgNumber = processorderPckgNumber;
		$state.go('repackingHistoryDetails');
	};
	
});