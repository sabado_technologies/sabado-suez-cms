myAdmin.controller("EmailAlertController", function($scope,Flash,AdminService,EmailAlertService) {
	
	$scope.showPack = false;
	$scope.showRm = true;
	$scope.showMessage = false;
	$scope.showRmMessage = false;
	$scope.showPackTable = false;
	$scope.showRmTable = true;


	AdminService.getEmailAlertData().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.EmailDatas =[]; //response.data;
			if($scope.EmailDatas.length == ''){
				//alert('No Results');
				$scope.showRmMessage = true;
				$scope.showRmTable = false;
				$scope.showRm = false;
			}else{
				
				$scope.showPack = false;
				$scope.showRm = true;
				$scope.showMessage = false;
				$scope.showRmMessage = false;
				$scope.showPackTable = false;
				$scope.showRmTable = true;
				
			}
			
		}

	});
	//$scope.packingAlerts = [];
	EmailAlertService.getPackingAlerts().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.packingAlerts = []; //response.data.data;
		}

	});
	
	$scope.goToPackDetails = function(){
		//alert();
		if($scope.packingAlerts.length == ''){
			$scope.showPack = false;
			$scope.showRm = false;
			$scope.showMessage = true;
			$scope.showPackTable = false;
			$scope.showRmMessage = false;
			$scope.showRmTable = false;
		}else{
			$scope.showPack = true;
			$scope.showRm = false;
			$scope.showMessage = false;
			$scope.showPackTable = true;
			$scope.showRmMessage = false;
			$scope.showRmTable = false;
		}		
		
	}
	
	$scope.goToRmDetails = function(){
		if($scope.EmailDatas.length == ''){
			$scope.showPack = false;
			$scope.showRm = false;
			$scope.showMessage = false;
			$scope.showPackTable = false;
			$scope.showRmTable = false;
			$scope.showRmMessage = true;
		}else{
			$scope.showPack = false;
			$scope.showRm = true;
			$scope.showMessage = false;
			$scope.showPackTable = false;
			$scope.showRmTable = true;
			$scope.showRmMessage = false;
		}		
		
	}

	
	$scope.getEmailDataWithRange = function(fromDate,ToDate){
		//alert(fromDate);
		if(fromDate == undefined || ToDate == undefined){
			Flash.clear();
			//var message = "";
			var message = 'Please select dates';
			Flash.create('warning', message);
			return;
		}
		var RmDetails = [];	
		var FrmDate =new Date(fromDate);
		var ToDatee=new Date(ToDate);
		AdminService.getEmailAlertDataWithRange(FrmDate,ToDatee).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.EmailDatas = response.data.data;
				if(RmDetails.length == ''){
					//alert('Empty');
					$scope.showMessage = false;
					$scope.showRmMessage = true;
					$scope.showPack = false;
					$scope.showRm = true;
					$scope.showPackTable = false;
					$scope.showRmTable = false;
				}else{
					$scope.showMessage = false;
					$scope.showRmMessage = false;
					$scope.showPack = false;
					$scope.showRm = true;
					$scope.showPackTable = false;
					$scope.showRmTable = true;
					
				}
			}

		});
	}
	var packDetails=[];
	$scope.filterByDates = function(from,to){
		//alert(from);
		if(from == undefined || to == undefined){
			Flash.clear();
			//var message = "";
			var message = 'Please select dates';
			Flash.create('warning', message);
			return;
		}
		
		var startDate = new Date(from);
		var endDate = new Date(to);
		
		EmailAlertService.getPackDetails(startDate,endDate).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.packingAlerts = response.data.data;
				packDetails = $scope.packingAlerts;
				//alert(packDetails.length);
				if(packDetails.length == ''){
					//alert('Empty');
					$scope.showMessage = true;
					$scope.showPack = true;
					$scope.showRm = false;
					$scope.showPackTable = false;
				}else{
					$scope.showMessage = false;
					$scope.showPack = true;
					$scope.showRm = false;
					$scope.showPackTable = true;
					
				}
			}

		});	
		
	}
	
	$scope.tempDetailsOfPack = function(processOder){
		$scope.tempProcessOrder = processOder;
		//$scope.showRmDownload = false;
		//$scope.showPackDownload = true;
		
		EmailAlertService.getTemperature(processOder).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.tempDetails = response.data.data;				
			}

		});
		
	}
	
	$scope.tempDetailsOfRm = function(processOder,emailData){
		//$scope.emailInfo = emailData;
		$scope.tempProcessOrder = processOder;
		//$scope.showRmDownload = true;
		//$scope.showPackDownload = false;
		
		EmailAlertService.getRmTemperature(processOder).then(function(response) {
			if (response.data !== 'undefined') {
				$scope.tempDetails = response.data.data;				
			}

		});
		
	}
	
	$scope.temperatureReport = function () {
		//alert();
		 alasql('SELECT [date] AS [Date],[processOrder] AS [Process Order],[sso] AS [SSO],[userName] AS [User Name],[reactor] AS [Reactor],[temperature] AS [Temperature (C)],[time] AS [Time] INTO XLSX("Temperature Deviation Report.xlsx",{headers:true}) FROM ?',[$scope.tempDetails]);
 };
	
	

});