myAdmin.controller("AllOrdersController", function($scope, $state, Flash,
		AllOrdersService, $location) {

	$scope.showTodays = false;
	$scope.showAll = true;
	$scope.showImg = false;

	AllOrdersService.getAllOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.allOrders = response.data.data;

			if ($scope.allOrders.length == "0") {
				$scope.showImg = true;

			} else if ($scope.allOrders.length != "0") {
				$scope.showImg = false;

			}
		}

	});

	AllOrdersService.getTodaysOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.todaysOrders = response.data.data;
		}

	});

	AllOrdersService.getPastOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.pastOrders = response.data.data;
		}

	});

	AllOrdersService.getFutureOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.futureOrders = response.data.data;
		}

	});

	AllOrdersService.getTodaysCompletedOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.completedOrders = response.data.data;
		}

	});

	$scope.all = function() {

		$scope.showTodays = false;
		$scope.showAll = true;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = false;

		if ($scope.allOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.allOrders.length != "0") {
			$scope.showImg = false;

		}

	}

	$scope.todays = function() {
		$scope.showTodays = true;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = false;

		if ($scope.todaysOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.todaysOrders.length != "0") {
			$scope.showImg = false;

		}
	}

	$scope.past = function() {
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = true;
		$scope.showFuture = false;
		$scope.showCompleted = false;

		if ($scope.pastOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.pastOrders.length != "0") {
			$scope.showImg = false;

		}

	}

	$scope.future = function() {
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showCompleted = false;
		$scope.showFuture = true;

		if ($scope.futureOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.futureOrders.length != "0") {
			$scope.showImg = false;

		}

	}

	$scope.finished = function() {
		$scope.showTodays = false;
		$scope.showAll = false;
		$scope.showPast = false;
		$scope.showFuture = false;
		$scope.showCompleted = true;

		if ($scope.completedOrders.length == "0") {
			$scope.showImg = true;

		} else if ($scope.completedOrders.length != "0") {
			$scope.showImg = false;

		}

	}

	$scope.allStatus = function(processorderProdNumber, approved) {
		//alert(approved);
		$scope.orderNumber = processorderProdNumber;
		//$scope.currentStatus = status;
		$scope.reWorked = approved;

		AllOrdersService.getStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.status = response.data.data;
					}

				});
		AllOrdersService.getChargedStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.chargedStatus = response.data.data;
					}

				});
		AllOrdersService.getPackedStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.packedStatus = response.data.data;
					}

				});
		AllOrdersService.getQualityCheckStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.qualityCheckStatus = response.data.data;
					}

				});
		AllOrdersService.getQualityInspectionStatus(processorderProdNumber)
				.then(function(response) {
					if (response.data !== 'undefined') {
						$scope.qualityInspectionStatus = response.data.data;
					}

				});

		AllOrdersService.getInventoryStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.inventoryStatus = response.data.data;
					}

				});

		AllOrdersService.getRmInventoryStatus(processorderProdNumber).then(
				function(response) {
					if (response.data !== 'undefined') {
						$scope.rmInventoryStatus = response.data.data;
					}

				});
	}

});