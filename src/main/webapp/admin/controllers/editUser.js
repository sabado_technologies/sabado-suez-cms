myAdmin.controller("EditUserController", function($scope,$state,Flash,EditUserService,$window,AdminService) {
	


    $('#multiline-snackbar').on('click', function ()
    {
        new PNotify({
            text    : 'Connection timed out. Showing limited messages.',
            confirm : {
                confirm: true,
                buttons: [
                    {
                        text    : 'Dismiss',
                        addClass: 'btn btn-link',
                        click   : function (notice)
                        {
                            notice.remove();
                        }
                    },
                    null
                ]
            },
            buttons : {
                closer : false,
                sticker: false
            },
            animate : {
                animate  : true,
                in_class : 'slideInDown',
                out_class: 'slideOutUp'
            },
            addclass: 'md multiline'
        });
    });	

	$scope.ActiveUsers = false;
	$scope.DeActiveUsers = false;
	$scope.AllUsers = true;
	
	$scope.selectedUsers="";
	$scope.activation="";
	$scope.missMatch="";
	
	///................Calling service to get All Users information.........................
	EditUserService.getAllUsers().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.allUsers = response.data.data;
			$scope.counts = $scope.allUsers.length;
			$scope.allUserCounts = true;
			$scope.deActiveCounts = false;
			$scope.activeCounts = false;
		}

	});
	
	///................Calling service to get all Roles...................................
	AdminService.getRoles().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.allRoles = response.data.data;
		}

	});
	
	AdminService.getMultiRoles().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.usersRoles = response.data.data;
			//console.log($scope.usersRoles);
		}

	});
	
	$scope.findIt = function(item){
		
		/*for(var i=0;i < $scope.usersRoles.length;i++){
			if($scope.usersRoles[i].sso == sso){
				//alert($scope.usersRoles[i].sso);
				$scope.marked.push($scope.usersRoles[i].roleName);
			}
		}*/
		////console.log($scope.marked);
	    if($scope.marked.indexOf(item)!= -1){
	    	//alert('Marked');
	    	return true;
	    	}
	}
	
	//var multiRoles =[];
	
	$scope.edit =function(user){
		$scope.multiRoles =[];
		$scope.marked = [];
		//alert('SSO =>' +user.ssoid);
		
		/*AdminService.getUserRoles(user.ssoid).then(function(response) {
			if (response != 'undefined') {
				$scope.userRoles = response.data;
				////console.log(respone);
				//console.log(response.data);
				//console.log($scope.userRoles);
				$scope.multiRoles = $scope.userRoles;
				
				$scope.findIt = function(item){
					//alert('Role =>'+item);
					//alert($scope.userRoles);
				    if($scope.userRoles.indexOf(item) !== -1){
				    	return true;
				    	}
				}
			}

		});*/
		//alert($scope.userRoles);
		
		$scope.editModal=true;
		$scope.showModal = true;
		$scope.editForm = user;
		for(var i=0;i < $scope.usersRoles.length;i++){
			if($scope.usersRoles[i].sso == $scope.editForm.ssoid){
				//alert($scope.usersRoles[i].sso);
				$scope.marked.push($scope.usersRoles[i].roleName);
				//$scope.multiRoles.push($scope.usersRoles[i].role);
				//alert($scope.marked);
			}
		}
		//console.log($scope.marked);
		
		/*if($scope.userRoles != undefined){
			$scope.findIt = function(item){
				//alert(item);
				//alert($scope.userRoles);
			    if($scope.userRoles.indexOf(item) != -1){
			    	return true;
			    	}
			}
		}*/
	}
	
	
	
	$scope.confirm = function(sso,name,email){
		$scope.showSuccess = false;
		$scope.showFailed = false;
		$scope.showHead = true;
		$scope.showSso = true;
		$scope.showNo = true;
		$scope.showName = true;
		$scope.showYes = true;
		$scope.resetSso = sso;
		$scope.resetName = name;
		$scope.email = email;
	}
	
	$scope.resetPassword = function(){
		
		var user ={};
		user.ssoid = $scope.resetSso;
		user.username = $scope.resetName;
		user.email = $scope.email;
		
		EditUserService.resetUserPassword(user).then(function(response) {
			if (response.data !== 'undefined') {
				if(response.data.code === '200'){
					//alert('done');
					$scope.showSuccess = true;
					$scope.showYes = false;
					$scope.showNo = true;
					$scope.showFailed = false;
					$scope.showHead = false;
					$scope.showSso = false;
					$scope.showName = false;
				}else {
					$scope.showSuccess = false;
					$scope.showYes = false;
					$scope.showNo = true;
					$scope.showFailed = true;
					$scope.showHead = false;
					$scope.showSso = false;
					$scope.showName = false;
				}				
				//$scope.allRoles = response.data.data;
			}

		});
	}
	
	$scope.selected=[];
	
    $scope.toggleSelection = function(user){
		
		var idx=$scope.selected.indexOf(user);
		if(idx > -1){
			$scope.selected.splice(idx, 1);
			$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
			
		}
		else{
			$scope.selected.push(user);
			$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
		}
		
		if($scope.selected.length == ''){
			$scope.selectedUsers="";
		}
	}
    
$scope.toggleRoles = function(role){
	//alert('Function called');
		
		var idx=$scope.marked.indexOf(role);
		if(idx > -1){
			$scope.marked.splice(idx, 1);
			//alert('Updated Roles' +$scope.marked);
			//$scope.selectedUsers=($scope.selected.length  + " User Selected ");//
			
		}
		else{
			$scope.marked.push(role);
			//alert('Updated Roles' +$scope.marked);
			//$scope.marked=($scope.selected.length  + " User Selected ");//
		}
	}
	
	///..................Function to receive User information to Update..............	
	$scope.update = function(editForm){
		//var updateUser ={};
		if($scope.marked.length == ''){
			//alert('please select role');
			var message1 ='Please select Role';
			Flash.clear();
			Flash.create('warning', message1);
			return;
		}
		var message1 ='Please fill all fileds';			
		if (('' + $scope.editForm.phoneNumber).length < "19") {
			$scope.showPhone = true;
			Flash.clear();
			Flash.create('warning', message1);
			return;
		}else if ($scope.editForm.role.roleName == undefined){
			Flash.clear();
			Flash.create('warning', message1);
			return;			
		}else if($scope.editForm.userName==null){
			Flash.clear();
			Flash.create('warning', message1);
			return;			
		}else if($scope.editForm.email == undefined){
			Flash.clear();
			Flash.create('warning', message1);
			return;			
		}
		var ad="Admin";
		var wm="Warehouse Manager";
		var mang = "Management";
	    var po="Production Operator";
	    var qa = "Quality Assurance";
	    var role=editForm.role.roleName;
	    
	    $scope.role={
	    		roleId:"",
		 };
	    
	    $scope.userByModifiedBy={
	    		ssoid:""
	    };
	    
	    $scope.updateUser={
	    		role:{},
	    		userByModifiedBy:{},
	    		userName:"",
	    		ssoid:"",
	    		email:"",
	    		phoneNumber:""	    		
		 };
	    	
	    $scope.updateUser.userName=editForm.userName;
	    $scope.updateUser.ssoid=editForm.ssoid;
	    $scope.updateUser.email=editForm.email;
	    $scope.updateUser.phoneNumber=editForm.phoneNumber;
	    $scope.updateUser.userByModifiedBy.ssoid=$window.sessionStorage.ssoId;
       
		///....................Assigning Role-Id depending upon Role...................
	    var allMultiRoles = [];
	    for(var i=0 ; i < $scope.marked.length ; i++){
	    	if($scope.marked[i] == ad){
	    		//alert('role is admin');
	    		allMultiRoles.push("1");
	    	} else if($scope.marked[i] == wm){
	    		//alert('role is warehouse');
	    		allMultiRoles.push("2");
	    	} else if($scope.marked[i] == mang){
	    		//alert('role is warehouse');
	    		allMultiRoles.push("3");
	    	} else if($scope.marked[i] == po){
	    		//alert('role is PO');
	    		allMultiRoles.push("4");
	    	} else{
	    		//alert('role is Quality');
	    		allMultiRoles.push("5");
	    	}
	    }
	    //console.log(allMultiRoles);
	    var userRoles =allMultiRoles[0];
	    
		for(var i=1; i< allMultiRoles.length;i++){ 
			if(allMultiRoles[i] < userRoles){
				userRoles = allMultiRoles[i];		
	           }		
		}
		//alert('first role =>' + userRoles);
		$scope.updateUser.role.roleId = userRoles;
		/*if(role==ad){
			$scope.updateUser.role.roleId="1";
		}
		else if(role==wm){
			$scope.updateUser.role.roleId="2";
		}else if(role==mang){
			$scope.updateUser.role.roleId="3";
		}
		else if(role==po){
			$scope.updateUser.role.roleId="4";
		}
		else{
			$scope.updateUser.role.roleId="5";
		}*/
		//alert("Length =>" + Object.keys($scope.updateUser).length);
		
		if(Object.keys($scope.updateUser).length =="6"){
			EditUserService.updateUsers($scope.updateUser,allMultiRoles).then(function(response){
				if(response.data != undefined){
					if(response.data.code === '200'){
						$scope.showModal = false;
						Flash.clear();
						var message = response.data.data  + " " + 'Updated Successfully';
					    Flash.create('success', message);  //Display message if Updating User Success
						$state.reload();
					
				}
				else {
					Flash.clear();
					var message = 'Updating' +  response.data.data + 'is failed';
				    Flash.create('danger', message);  //Display message if Updating User fails
					$state.reload();
			       }
				}
			})
			
		}else{
			Flash.clear();
			var message ='Please fill all fileds';
			Flash.create('danger', message);
		}		
	}
	
   $scope.activateUser = function(){
	   
	   if ($scope.selected.length ==' '){
		   $scope.emptyUsers =('Please Select User');
		   return;
	   }
	   
	   var userList=[];
	   
	   for (var i = 0; i < $scope.selected.length; i++) {
		   
		   $scope.status={
				   statusId:"",
   		   };
		   
		   $scope.User={
				   status:{},
				   ssoid:""
		   }
		   
		   
		   if($scope.selected[i].status.statusDescription==="DeActivated"){
			   $scope.User.status.statusId="11";
			   $scope.User.ssoid=$scope.selected[i].ssoid;
		   }
		   else{
			   $scope.selectedUsers="";
			   $scope.missMatch =('Status Miss Match.You are selecting Active Users');
			   return;
		   }
		   
		   userList.push($scope.User);
	   }
	   
	   EditUserService.activeUsers(userList).then(function(response){
			if(response.data != undefined){
				if(response.data.code === '200'){
					Flash.clear();
				var message = 'Users Activated Successfully';
			    Flash.create('success', message);
				$state.reload();
				
			}
			else {
				Flash.clear();
			   var message = 'Users Activation Failed';
			   Flash.create('danger', message);
			   $state.reload();
			}
		}
		})
   }
   
$scope.deActivateUser = function(){
	
	if ($scope.selected.length ==' '){
		   $scope.emptyUsers =('Please Select User');
		   return;
	   }
	
	   var deActivateUserList=[];
	   
	   for (var i = 0; i < $scope.selected.length; i++) {
		   
		   $scope.status={
				   statusId:"",
   		   };
		   
		   $scope.User={
				   status:{},
				   ssoid:""
		   }
		   
		   
		   if($scope.selected[i].status.statusDescription==="Active"){
			   $scope.User.status.statusId="12";
			   $scope.User.ssoid=$scope.selected[i].ssoid;
		   }
		   else{
			   $scope.selectedUsers="";
			   $scope.missMatch =('Status Miss Match.You are selecting De-Activated Users');
			   return;
		   }
		   
		   deActivateUserList.push($scope.User);
	   }
	  
	   EditUserService.deActivateUsers(deActivateUserList).then(function(response){
			if(response.data != undefined){
				if(response.data.code === '200'){
					Flash.clear();
					var message = 'Users De-Activated Successfully';
				    Flash.create('success', message);
					$state.reload();
				
			}
			else {
				Flash.clear();
				var message = 'Users De-Activation Failed';
			    Flash.create('danger', message);
				$state.reload();
			}
		}
		})
   }

  $scope.showAll = function(){
	  $scope.AllUsers = true;
	  $scope.allUserCounts = true;
	  $scope.deActiveCounts = false;
	  $scope.activeCounts = false;
	  $scope.ActiveUsers = false;
	  $scope.DeActiveUsers = false;
  }

   $scope.active = function(){
	   
	   EditUserService.getActiveUsers().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.allActiveUsers = response.data.data;
				$scope.activeLength = $scope.allActiveUsers.length;
				if($scope.activeLength == 0){
					Flash.clear();
					var message = 'No active users'; 
				    Flash.create('warning', message);
					$scope.ActiveUsers = false;
					$scope.DeActiveUsers = false;
					$scope.AllUsers = true;
					$scope.deActiveCounts = false;
					$scope.allUserCounts = true;
					$scope.activeCounts = false;
					
				} else {					
					$scope.ActiveUsers = true;
					$scope.DeActiveUsers = false;
					$scope.AllUsers = false;
					$scope.allUserCounts = false;
					$scope.deActiveCounts = false;
					$scope.activeCounts = true;
					
				}
				
			}
		});
	   
   }
   
   $scope.deActive = function(){
	   
	   EditUserService.getDeActiveUsers().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.alldeActiveUsers = response.data.data;
				$scope.deActiveLength = $scope.alldeActiveUsers.length;
				if($scope.deActiveLength == 0){
					Flash.clear();
					var message = 'No De-activated users'; 
				    Flash.create('warning', message);
					$scope.ActiveUsers = false;
					$scope.DeActiveUsers = false;
					$scope.AllUsers = true;
					$scope.deActiveCounts = false;
					$scope.allUserCounts = true;
					$scope.activeCounts = false;
				} else {
					$scope.ActiveUsers = false;
					$scope.DeActiveUsers = true;
					$scope.AllUsers = false;
					$scope.deActiveCounts = true;
					$scope.allUserCounts = false;
					$scope.activeCounts = false;
					
				}
				
			}
		});
	   
   }
   
   
});
	