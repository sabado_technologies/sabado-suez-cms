myAdmin.controller("AssociationController", function($scope, $state, Flash,
		AssociateService, $location) {

	$scope.production = true;
	$scope.packaging = false;

	AssociateService.getProductOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.productOrders = response.data.data;
			$scope.productLength = $scope.productOrders.length;
		}

	});
	
	var packageLength=0;

	AssociateService.getPackageOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.packageOrders = response.data.data;
			packageLength = $scope.packageOrders.length;
			//$scope.materialMemo=response.data.data.productName;
			//alert($scope.materialMemo);
		}

	});

	$scope.associateProduction = function(productOrder) {

		if (packageLength == 0) {
			Flash.clear();
			var message = 'No Packing Orders To Associate';
			Flash.create('warning', message);
			/*$scope.noProcessOrders = ("No Packing Orders To Associate");*/
			return;

		}

		$scope.productProcess = productOrder.processorderProdNumber;
		$scope.materialNumber = productOrder.productTable.productNumber;
		$scope.materialMemo = productOrder.productTable.productDescription;
		$scope.production = false;
		$scope.packaging = true;
	}

	$scope.selected = [];

	$scope.toggleSelection = function(packageOrder) {

		var idx = $scope.selected.indexOf(packageOrder);
		if (idx > -1) {
			$scope.selected.splice(idx, 1);

		} else {
			$scope.selected.push(packageOrder);
		}
	}
	
	$scope.refresh = function(){
		$state.reload();
	}

	$scope.association = function(productProcess) {
		
		if($scope.selected.length==0){
			Flash.clear();
			var message = 'Please select packing orders';
			Flash.create('warning', message);
			return;
		}

		var associationDetails = [];
		for (var i = 0; i < $scope.selected.length; i++) {

			var associate = {
				productProcessNumber : "",
				pakageProcessNumber : ""
			};

			associate.productProcessNumber = productProcess;
			associate.pakageProcessNumber = $scope.selected[i];

			associationDetails.push(associate);
		}

		AssociateService.addAssociation(associationDetails).then(
				function(response) {
					if (response.data != undefined) {
						if (response.data.code === '200') {
							$scope.packaging = false;
							$scope.production = true;
							
							Flash.clear();
							Flash.dismiss(1);
							var message = 'Process Orders Associated';
							Flash.create('success', message);
							$state.reload();
						} else {
							Flash.clear();
							var message = 'Process Orders Association Failed';
							Flash.create('danger', message);
							$state.reload();
						}
					}
				})
	}

});