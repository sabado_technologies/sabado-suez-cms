myAdmin.controller("DeletedPoControllerForProcessOrders", function($scope, $state, Flash,$window,
		AssociateService) {

	
	AssociateService.getProcessOrdersForDeletion().then(function(response) {
		if (response !== 'undefined') {
			(response.data);
			$scope.processOrdersForDeletion = response.data;
			//$scope.productLength = $scope.productOrders.length;
		}

	});
	
	$scope.deleteProcessOrder = function(processorderProdNumber){
		
		$scope.ssoId = $window.sessionStorage.ssoId;
		

		$scope.customAlert = function(message){
	new PNotify({
        text    : message,
        confirm : {
            confirm: true,
            buttons: [
                {
                    text    : 'Close',
                    addClass: 'btn btn-link',
                    click   : function (notice)
                    {
                        notice.remove();
                    }
                },
                null
            ]
        },
        buttons : {
            closer : false,
            sticker: false
        },
        animate : {
            animate  : true,
            in_class : 'slideInDown',
            out_class: 'slideOutUp'
        },
        addclass: 'md multiline'
    });
	
	}
		
		
		var formStatus = true;
		
		var remarks = $("#remarks"+processorderProdNumber).val();
		formStatus= true;	
		if( 0 === remarks.length || /^\s*$/.test(remarks)){
			
			formStatus= false;
			
		}
	
	if(!formStatus ){
		var message='Enter valid remarks for Deleting '+processorderProdNumber;
		$scope.customAlert(message);
		return formStatus;
	}
	
	AssociateService.deleteProductionOrder(processorderProdNumber ,remarks ,$scope.ssoId).then(function(response) {
		if (response !== 'undefined') {
			//(response.data);
			//$scope.processOrdersForDeletion = response.data;
			//alert('Status =>' + response.status);
			if(response.status =='200'){
				var message = 'Process Order number ' +'<strong>' +processorderProdNumber+'</strong>' +' is successfully deleted';
				//alert('done');
				$scope.customAlert(message);
				$state.reload();
			}else {
				var message = 'Please Try Again !';
				$scope.customAlert(message);
				//alert('Not done');
			}
			//(response);
			//$("#deleteModal").modal('show');
			//$state.reload();
		}

	});
	
		
	}
	
	
});

myAdmin.controller("DeletedPoControllerForPackageOrders", function($scope, $state, Flash,$window,
		AssociateService) {

	
	AssociateService.getPackageOrdersForDeletion().then(function(response) {
		if (response !== 'undefined') {
			(response.data);
			$scope.packageOrdersForDeletion = response.data;
			//$scope.productLength = $scope.productOrders.length;
		}

	});
	
	$scope.deletePackageOrder = function(processorderPckgNumber){
		
		$scope.ssoId = $window.sessionStorage.ssoId;
		

		$scope.customAlert = function(message){
	new PNotify({
        text    : message,
        confirm : {
            confirm: true,
            buttons: [
                {
                    text    : 'Close',
                    addClass: 'btn btn-link',
                    click   : function (notice)
                    {
                        notice.remove();
                    }
                },
                null
            ]
        },
        buttons : {
            closer : false,
            sticker: false
        },
        animate : {
            animate  : true,
            in_class : 'slideInDown',
            out_class: 'slideOutUp'
        },
        addclass: 'md multiline'
    });
	
	}
		var formStatus = true;
		
		var remarks = $("#remarks"+processorderPckgNumber).val();
		formStatus= true;	
		if( 0 === remarks.length || /^\s*$/.test(remarks)){
			
			formStatus= false;
			
		}
	
	if(!formStatus ){
		var message='Enter valid remarks for Deleting '+processorderPckgNumber;
		$scope.customAlert(message);
		return formStatus;
	}
	
	AssociateService.deletePackageProductionOrder(processorderPckgNumber ,remarks ,$scope.ssoId).then(function(response) {
		if (response !== 'undefined') {
			//(response.data);
			//$scope.packageOrdersForDeletion = response.data;
			//(response.data);
			if(response.status =='200'){
				var message = 'Process Order number ' +'<strong>' +processorderPckgNumber+'</strong>' +' is successfully deleted';
				//alert('done');
				$scope.customAlert(message);
				$state.reload();
			}else {
				var message = 'Please Try Again !';
				$scope.customAlert(message);
				//alert('Not done');
			}
			//$("#deleteModal").modal('show');
			//$state.reload();
		}

	});
	
		
	}
	
	
});