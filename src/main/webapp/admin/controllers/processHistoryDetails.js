myAdmin.controller("ProcessHistoryDetailsController", function($scope, $state, AdminService, $window) {	
	
	
	/*AdminService.getProcessHistory().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.processDetails = response.data.data;
				$scope.processDetailsCount = $scope.processDetails.length;
				//$state.reload();
			}

		});*/
	
	AdminService.getProcessOrderForHistory($window.sessionStorage.processorderHistory).then(
			function(response) {
				if (response != undefined) {
					$scope.processOrderForQualityAssurance = response.data;
				}
			})

AdminService.associatedPackingHistory($window.sessionStorage.processorderHistory).then(
	function(response) {
		if (response != undefined) {
			$scope.associatedOrders = response.data;

		}
	})
	
	$scope.updateRmInfo = function(rmInfo){
		$scope.extraRmInfo = rmInfo;
	}
	
	$scope.getVesselHrs =  function(processOrder){
		//alert(processOrder);	
		
		$scope.vesselHrsProcess = processOrder;
		
		AdminService.getPackinVesselHrs(processOrder)
		.then(function(response) {
			
			if (response.data !== 'undefined') {
				$scope.packingVesselHrs = response.data;
				//console.log($scope.packingVesselHrs);

		
			}
		});
	}
	
	$scope.addExtraRm = function(){
		var process =$scope.processOrderForQualityAssurance[0].processorderProdNumber;
		//alert(process);
		//alert($scope.extraRmInfo);
		
		AdminService.updateExtraRm(process,$scope.extraRmInfo).then(function(response){
			if(response.data != undefined){
				if(response.data.code === '200'){
					//$scope.showModal = false;
					//Flash.clear();
					var message = 'Extra RM Details Added';
					alertForError(message);
				   // Flash.create('success', message);  //Display message if Updating User Success
					//$state.reload();
				
			}
			else {
				//Flash.clear();
				var message = 'Please try again !';
				alertForError(message);
			    //Flash.create('danger', message);  //Display message if Updating User fails
				//$state.reload();
		       }
			}
		})
	}
	
	function alertForError(message){
		new PNotify({
	        text    : message,
	        confirm : {
	            confirm: true,
	            buttons: [
	                {
	                    text    : 'Close',
	                    addClass: 'btn btn-link',
	                    click   : function (notice)
	                    {
	                        notice.remove();
	                    }
	                },
	                null
	            ]
	        },
	        buttons : {
	            closer : false,
	            sticker: false
	        },
	        animate : {
	            animate  : true,
	            in_class : 'slideInDown',
	            out_class: 'slideOutUp'
	        },
	        addclass: 'md multiline'
	    });
		
	}
	

});