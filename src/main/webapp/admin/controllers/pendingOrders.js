myAdmin.controller("PendingController", function($scope, $state, Flash,
		PendingService, $location) {
	
	$scope.production = true;
	$scope.packaging = false;
	
	PendingService.getReactors().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.reactors = response.data.data;
		}

	});
	
	PendingService.getPendingOrders().then(function(response) {
		if (response.data !== 'undefined') {
			$scope.pendingOrders = response.data.data;
			$scope.productLength = $scope.pendingOrders.length;
		}

	});
		
	$scope.edit = function(reactor){
		$scope.threashold =reactor;
		//$scope.maxThresh =0;
		$scope.maxThresh = parseInt(reactor.maxThreashold);
		//console.log($scope.threashold);
		$scope.showSave = true;
		$scope.showText = true;
		$scope.updated = false;
		$scope.failed = false;
		$scope.showClose = true;
	}
	
	$scope.update = function(threashold){
		//var temp = threashold.maxThreashold;
		var temp = $scope.maxThresh;
		
		if(temp == 0 || temp < 0 ){
			//alert('Enter valid threashold');
			var message = "";
			message = 'Enter valid threashold';
			Flash.create('warning', message);
			return;
		} else if(temp > 75){
			//alert('Threashold should be equal or less than 75 ');
			var message = "";
			message = 'Threashold should be equal or less than 75';
			Flash.create('warning', message);
			return;
		} else if(temp == null){
			//alert('Please enter threashold');
			var message = "";
			message = 'Please enter threashold';
			Flash.create('warning', message);
			return;
		}
		//$scope.updatedTemp = threashold.maxThreashold;
		$scope.threashold.maxThreashold = temp;
		
		PendingService.updateReactor(threashold).then(function(response) {
			if (response.data !== 'undefined') {
				if (response.data.code ==='200'){
					$scope.updated = true;
					$scope.showClose = true;
					$scope.failed = false;
					$scope.showSave = false;
					$scope.showText = false;
					
					
				}else{
					$scope.updated = false;
					$scope.failed = true;
					$scope.showClose = true;
					$scope.showSave = false;
					$scope.showText = false;
				}
			}

		});
		
	}
	
	$scope.close = function(){
		
		PendingService.getReactors().then(function(response) {
			if (response.data !== 'undefined') {
				$scope.reactors = response.data.data;
			}

		});
		
	}
	
	
});