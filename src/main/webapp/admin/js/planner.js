dates = [];
var existingPerday;
var existingPerWeek;
$(document).ready(function() {
	
	/*var minOffset = 0, maxOffset = 100;
	var thisYear = (new Date()).getFullYear();
					for (var i = minOffset; i <= maxOffset; i++) {
						var year = thisYear + i;
						$('<option>', {
							value : year,
							text : year
						}).appendTo(".year");
					}*/
					

										var min = 2016, max = 2118, select = document
							.getElementById('year');

					for (var i = min; i <= max; i++) {
						var opt = document.createElement('option');
						opt.value = i;
						opt.innerHTML = i;
						select.appendChild(opt);
					}

					select.value = new Date().getFullYear();

	$('#totalDate').click(function() {
		var itemArray = [];
		var yearval = $("#year").val();
		var quarter = $("#quarter").val();
		if (quarter == "1") {
			//$('#mdp-demo1').multiDatesPicker('getDates').length = 0;
			document.getElementById("daycount").value = ($('#mdp-demo1').multiDatesPicker('getDates').length);
		} 
		else if (quarter == "2") {
			document.getElementById("daycount").value = ($('#mdp-demo2').multiDatesPicker('getDates').length);
			//$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
		} else if (quarter == "3") {		
			document.getElementById("daycount").value = ($('#mdp-demo3').multiDatesPicker('getDates').length);
			//$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
		} else if (quarter == "4") {
			document.getElementById("daycount").value = ($('#mdp-demo4').multiDatesPicker('getDates').length);
			//$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
		}

	})
});

$(document).ready(
		function() {
			$(document).ready(function() {
				calendarInit();
			});
			$("#quarter").change(function() {
				var a = document.getElementById("daycount");
				a.value = 0;

				calendarInit();
			});
			$("#year").change(function() {
				calendarInit();
			});

			function getDates(startDate, endDate) {
				//alert("get Dates called=>startDate="+startDate+" and end Date ==>"+endDate);
				dates = [];
				var currentDateItr = new Date();
				var currentDate = startDate;
				(currentDate.getDate()+1);
				if(startDate.getDay() == 6 || startDate.getDay() == 0){
					
				}else{
					dates.push(startDate);
				}
				if(endDate.getDay() == 6 || endDate.getDay() == 0){
					
				}else{
					dates.push(endDate);
				}
				while (currentDate < endDate) {
					var dateTemp = currentDate.setDate(currentDate.getDate()+1);
					var tempDate = new Date(dateTemp);
					if(tempDate.getDay() == 6 || tempDate.getDay() == 0){
						
					}else{
						dates.push(dateTemp);
					}
				}
				//(dates);
				(dates);
				return dates;
			}
			
			function dayCount(){
				var quarter = $("#quarter").val();
				if (quarter == "1") {
					document.getElementById("daycount").value = ($('#mdp-demo1').multiDatesPicker('getDates').length);	
				} else if (quarter == "2") {
					document.getElementById("daycount").value = ($('#mdp-demo2').multiDatesPicker('getDates').length);
				} else if (quarter == "3") {
					document.getElementById("daycount").value = ($('#mdp-demo3').multiDatesPicker('getDates').length);
				} else if (quarter == "4") {
					document.getElementById("daycount").value = ($('#mdp-demo4').multiDatesPicker('getDates').length);	
				}
			}
			
			function enabledDefaultDates(){
				dates = [];
				var yearval = $("#year").val();
				var startDate = "";
				//alert("year val==>" + yearval);
				var quarter = $("#quarter").val();
				// alert("docuemtn ready year " + yearval + " and quarter=>" +
				// quarter);
				if (quarter == 1) {
					// alert("Inside if block");
					// Usage
					var dates = getDates(new Date(parseInt(yearval), 00, 00), new Date(parseInt(yearval),02, 31));
					("****************");
					("dates=>" + dates);
					("****************");
					dates.forEach(function(date) {
						(date);
					});
					(dates);
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo1").multiDatesPicker("destroy");
					$('#mdp-demo1').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '1/01/' + yearval,
						maxDate : '3/31/' + yearval,
						defaultDate : '1/1/' + yearval
					});
					$('#mdp-demo1').css("display", "block");
					$('#mdp-demo2').css("display", "none");
					$('#mdp-demo3').css("display", "none");
					$('#mdp-demo4').css("display", "none");

				} else if (quarter == 2) {
					var dates = getDates(new Date(parseInt(yearval), 03, 00), new Date(parseInt(yearval),05, 30));
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo2").multiDatesPicker("destroy");
					$('#mdp-demo2').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '4/1/' + yearval,
						maxDate : '6/30/' + yearval,
						defaultDate : '4/1/' + yearval
					});
					$('#mdp-demo1').css("display", "none");
					$('#mdp-demo2').css("display", "block");
					$('#mdp-demo3').css("display", "none");
					$('#mdp-demo4').css("display", "none");
				} else if (quarter == 3) {
					var dates = getDates(new Date(parseInt(yearval), 06, 00), new Date(parseInt(yearval),08, 30));
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo3").multiDatesPicker('destroy');
					$('#mdp-demo3').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '7/1/' + yearval,
						maxDate : '9/30/' + yearval,
						defaultDate : '7/1/' + yearval
					});
					$('#mdp-demo1').css("display", "none");
					$('#mdp-demo2').css("display", "none");
					$('#mdp-demo3').css("display", "block");
					$('#mdp-demo4').css("display", "none");

				} else if (quarter == 4) {
					var dates = getDates(new Date(parseInt(yearval), 09, 00), new Date(parseInt(yearval),11, 31));
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo4").multiDatesPicker("destroy");
					$('#mdp-demo4').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '10/1/' + yearval,
						maxDate : '12/31/' + yearval,
						defaultDate : '10/1/' + yearval
					});
					$('#mdp-demo1').css("display", "none");
					$('#mdp-demo2').css("display", "none");
					$('#mdp-demo3').css("display", "none");
					$('#mdp-demo4').css("display", "block");
				}
			}
			
			function enabledResponseDates(data){
				//alert("enabledResponseDates called");
				var perDay = data.data.perDay;
				existingPerday = data.data.perDay;
				var perWeek = data.data.perWeek;
				existingPerWeek = data.data.perWeek;
				var perQuarter = data.data.perQuarter;
				(data);
				dates = data.data.dates;
				$("#datesInp").val(dates);
				var datesAgain = $("#datesInp").val();
				("*****dataAgain*******");
				(datesAgain);
				("****************");
				
				("datesasdsa");
				(dates);
				$("#qty").val(perQuarter);
				$("#day1").val(perDay);
				$("#week").val(perWeek);
				//$("existingDay1").val
				//$("#addQty").val();
				var yearval = $("#year").val();
				var startDate = "";
				//alert("year val==>" + yearval);
				var quarter = $("#quarter").val();
				
				if (quarter == 1) {
					("****************");
					("dates=>" + dates);
					("****************");
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo1").multiDatesPicker("destroy");
					$('#mdp-demo1').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '1/01/' + yearval,
						maxDate : '3/31/' + yearval,
						defaultDate : '1/1/' + yearval
					});
					$('#mdp-demo1').css("display", "block");
					$('#mdp-demo2').css("display", "none");
					$('#mdp-demo3').css("display", "none");
					$('#mdp-demo4').css("display", "none");

				} else if (quarter == 2) {
					//var dates = getDates(new Date(2018, 03, 01), new Date(2018,05, 30));
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo2").multiDatesPicker("destroy");
					$('#mdp-demo2').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '4/1/' + yearval,
						maxDate : '6/30/' + yearval,
						defaultDate : '4/1/' + yearval
					});
					$('#mdp-demo1').css("display", "none");
					$('#mdp-demo2').css("display", "block");
					$('#mdp-demo3').css("display", "none");
					$('#mdp-demo4').css("display", "none");
				} else if (quarter == 3) {
					//var dates = getDates(new Date(2018, 06, 01), new Date(2018,08, 30));
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo3").multiDatesPicker('destroy');
					$('#mdp-demo3').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '7/1/' + yearval,
						maxDate : '9/30/' + yearval,
						defaultDate : '7/1/' + yearval
					});
					$('#mdp-demo1').css("display", "none");
					$('#mdp-demo2').css("display", "none");
					$('#mdp-demo3').css("display", "block");
					$('#mdp-demo4').css("display", "none");

				} else if (quarter == 4) {
					//var dates = getDates(new Date(2018, 09, 01), new Date(2018,11, 31));
					$('#mdp-demo1').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo2').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo3').multiDatesPicker('resetDates', 'picked');
					$('#mdp-demo4').multiDatesPicker('resetDates', 'picked');
					$("#mdp-demo4").multiDatesPicker("destroy");
					$('#mdp-demo4').multiDatesPicker({
						addDates : dates,
						numberOfMonths : [ 1, 3 ],
						minDate : '10/1/' + yearval,
						maxDate : '12/31/' + yearval,
						defaultDate : '10/1/' + yearval
					});
					$('#mdp-demo1').css("display", "none");
					$('#mdp-demo2').css("display", "none");
					$('#mdp-demo3').css("display", "none");
					$('#mdp-demo4').css("display", "block");
				}
			}

			function calendarInit() {
				//alert("calendarInit called");
				var selectedYear = $("#year").val();
				var selectedQuarter = $("#quarter").val();
				$.ajax({
				url : '/suez/planner/getPlannedDates/'+selectedYear+"/"+selectedQuarter,
				type : "GET",
				contentType : "application/json",
				dataType : "json",
				success : function(data) {
					//alert("Inside success block");
					(data);
					if (data.data.length == 0) {
						$("#planExist").val(0);
						//alert('Plan Added Successfully');
						enabledDefaultDates();
						$("#plannedProd").css("display","block");
						$("#addtionalPlannedProd").css("display","none");
						$("#qty").val(0);
						$( "#qty" ).prop( "disabled", false );
						$("#day1").val(0);
						$("#week").val(0);
						dayCount();
					}else{
						$("#planExist").val(1);
						$( "#qty" ).prop( "disabled", true );
						enabledResponseDates(data);
						$("#plannedProd").css("display","block");
						$("#addtionalPlannedProd").css("display","block");
						dayCount();
					}
				},
				error:function(error){
					("Inside error block");
				}
			});
				
			}
		});
function alertForError(message){
	new PNotify({
        text    : message,
        confirm : {
            confirm: true,
            buttons: [
                {
                    text    : 'Close',
                    addClass: 'btn btn-link',
                    click   : function (notice)
                    {
                        notice.remove();
                    }
                },
                null
            ]
        },
        buttons : {
            closer : false,
            sticker: false
        },
        animate : {
            animate  : true,
            in_class : 'slideInDown',
            out_class: 'slideOutUp'
        },
        addclass: 'md multiline'
    });
	
}

function addme() {
	var exitingPlan =document.getElementById("planExist").value;
	var daycount = document.getElementById("daycount").value;
	var qty = document.getElementById("qty").value;
	var addQty = document.getElementById("addQty").value;
	var qty = parseInt(qty);
	
	var addQty = parseInt(addQty);
	if(qty==0 ){
		var message ="Please enter quantity";
		alertForError(message);
		
		//$("#myDialogText").text("Please enter quantity");
		return;
	}else if (qty < 0){
		//alert('Invalid Quantity');
		var message ="Invalid Quantity";
		alertForError(message);
		//$("#myDialogText").text("Please enter quantity");
		return;
	}/* else if(exitingPlan == 1){
		if(addQty == 0){
			//alert('Invalid additional quantity ');
			var message = "Invalid additional quantity"
			alertForError(message);
			//$("#myDialogText").text("Invalid additional quantity");
			return;
		}
	}*/
	/*else if(addQty == 0){
		alert('Invalid additional quantity ');
		$("#myDialogText").text("Please enter quantity");
	}*/
	var day1 = (qty+addQty) / daycount;
	day1 = day1.toFixed(2);
	var week = day1 * 5;
	week = week.toFixed(2);
	document.getElementById("day1").value = day1;
	document.getElementById("week").value = week;
}

function addPlanner() {
	(dates);
	//alert(existingPerday);
	var exiting =document.getElementById("planExist").value;
	//alert('Exits =>' +exiting);
	var PlannerRequest = {};
	var actualQuant = parseInt(document.getElementById("qty").value);
	var additionalQuant = parseInt(document.getElementById("addQty").value);	
	
	/*if(exiting == 1){
		if(additionalQuant == 0){
			//alert('Invalid additional quantity');
			var message ="Invalid additional quantity";
			alertForError(message);
			return;
		}
	}*/
	
	//******************************************************
	//var datesss = $("#datesInp").val().split(",");
	//******************************************************
	var quarter = $("#quarter").val();
	
	
	//("");
	var totalQuant = actualQuant + additionalQuant;
	PlannerRequest.perQuarter = totalQuant;
	//alert(PlannerRequest.perQuarter);
	PlannerRequest.perDay = document.getElementById("day1").value;
	//alert('Planned per day' + perDay);
	//alert('Per Day' + PlannerRequest.perDay);
	/*if(PlannerRequest.perDay == perDay){
		alert('hdhhjnj');
		return;
	}*/
	PlannerRequest.perWeek = document.getElementById("week").value;
	PlannerRequest.workingDays = document.getElementById("daycount").value;
	if (quarter == "1") {
		PlannerRequest.dates  = $('#mdp-demo1').multiDatesPicker('getDates');	
	} else if (quarter == "2") {
		PlannerRequest.dates  = $('#mdp-demo2').multiDatesPicker('getDates');
	} else if (quarter == "3") {
		PlannerRequest.dates  =  $('#mdp-demo3').multiDatesPicker('getDates');
	} else if (quarter == "4") {
		PlannerRequest.dates  = $('#mdp-demo4').multiDatesPicker('getDates');	
	}
	
	if(PlannerRequest.perQuarter == 0 ){
		//alert('Please enter Quarter plan');
		var message ="Please enter valid Quarter plan";
		alertForError(message);
		//$("#myDialogText").text("Please enter Quarter plan");
		return;
	} else if(PlannerRequest.perDay==0 || PlannerRequest.perWeek == 0){
		//alert('Please calculate the plan');
		var message ="Please calculate the plan";
		alertForError(message);
		//$("#myDialogText").text("Please calculate the plan");
		return;
	} else if(existingPerday == PlannerRequest.perDay ||existingPerWeek == PlannerRequest.perWeek){
		//alert('Please re calculate');
		var message ="Please Re-Calculate";
		alertForError(message);
		return;
	}
	var elements = [];
	
	for(var i= 0 ; i < PlannerRequest.dates.length;i++){
		(PlannerRequest.dates[i]);
		var date = new Date(PlannerRequest.dates[i]);
		elements.push(date);
	}
	
	//if (PlannerRequest.perQuarter==0)
	
	(elements);
	/*alert('Length ' +elements.length);
	alert('selected dates ' + elements);*/
	//return;
	PlannerRequest.dates =  elements;
	
	
	PlannerRequest.quarter = $("#quarter").val();
	PlannerRequest.year = $("#year").val();
	("*****Planner reqest**********");
	(PlannerRequest);
	("*****Planner reqest**********");
	
			$.ajax({
				url : '/suez/planner/addPlanner',
				type : "POST",
				data : JSON.stringify(PlannerRequest),
				contentType : "application/json",
				dataType : "json",
				success : function(data) {
					(data);

					if (data.code === '200') {
						//alert('Plan Added Successfully');
						var message ="Plan Added Successfully";
						alertForError(message);
						//$("#added").text("Please calculate the plan");
					} else if (data.code === '300') {
						//alert('Plan Alreday Exits For The Quarter You Are Trying To Add ');
						var message ="Plan Alreday Exits For The Quarter You Are Trying To Add";
						alertForError(message);
						//$("#myDialogText").text("Please calculate the plan");
					} else {
						//alert('Adding Plan Failed');
						var message ="Please Try Again !";
						alertForError(message);
						//$("#myDialogText").text("Please calculate the plan");
					}

				}

			});
	// location.reload();

}