
var myAdmin = angular.module('myAdmin', [ 'ui.router', 'ngFlash', 'ngStorage',
		'ui.bootstrap', 'angularUtils.directives.dirPagination','multipleDatePicker','ngMessages']);

myAdmin.directive('fileModel', [ '$parse', function($parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;
			element.bind('change', function() {
				scope.$apply(function() {
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
} ]);

myAdmin.service('fileUpload', [ '$http','$rootScope','Flash', function($http,$rootScope,Flash) {
	$rootScope.msgForSuccess = false;
	$rootScope.msgForError = false;
	this.uploadFileToUrl = function(file, uploadUrl) {
		
		if(file == undefined){
			var message = 'Please select file to upload';
			Flash.create('danger', message);
			return;
		}
		var fd = new FormData();
		fd.append('file', file);
		//alert('File name =>'+ file);
		$http.post(uploadUrl, fd, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		}).then(function(success) {
			//console.log(success);
			var status = success.data.code;
			if (status == "200") {
				/*$rootScope.msgForSuccess = true;
				$rootScope.msgForError = false;*/
				var message = 'Process order '+'<strong>'+success.data.data+'</strong>'+' uploded successfully';
				Flash.create('success', message);
				
			} else if(status == "300") {
				
				var message = 'Process order '+'<strong>'+success.data.data+'</strong>'+' allready exits';
				Flash.create('warning', message);
			} else if(status == "500"){
				
				var message = 'You are uploading'+'<strong> packing order <strong>'+'instead'+'<strong> production order </strong>';
				Flash.create('warning', message);
				
			}
			else if(status == "400"){
				/*$rootScope.msgForSuccess = false;
				$rootScope.msgForError = true;*/
				var message = 'Uploading Process Order' + '<strong>'+success.data.data+'</strong>' + 'Failed';
				//var message = '<strong>'+success.data.data+'</strong>'+' uploding failed';
				Flash.create('danger', message);
			}
			//console.log(status);
		}).then(function(error) {
			//console.log(error);
		});

	}
	this.uploadFileForPackagingUrl = function(file, uploadUrl) {
		
		if(file == undefined){
			var message = 'Please select file to upload';
			Flash.create('danger', message);
			return;
		}
		
		var fd = new FormData();
		fd.append('file', file);
		$http.post(uploadUrl, fd, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		}).then(function(success) {
			//console.log(success);
			var status = success.data.code;
			/*if (status == "200") {
				$rootScope.msgForSuccess = true;
				$rootScope.msgForError = false;
			} else if (status == "400") {
				$rootScope.msgForSuccess = false;
				$rootScope.msgForError = true;
			}*/
			if (status == "200") {
				/*$rootScope.msgForSuccess = true;
				$rootScope.msgForError = false;*/
				var message = 'Process Order '+'<strong>'+success.data.data+'</strong>'+' uploded successfully';
				Flash.create('success', message);
				
			} else if(status == "300") {
				
				var message = 'Process order '+'<strong>'+success.data.data+'</strong>'+' allready exits';
				Flash.create('warning', message);
			} else if(status == "500"){
				
				var message = 'You are uploading'+'<strong> production order </strong>'+'instead'+'<strong> packing order </strong>';
				Flash.create('warning', message);
				
			}
			else if(status == "400"){
				/*$rootScope.msgForSuccess = false;
				$rootScope.msgForError = true;*/
				var message = 'Uploading Process Order' + '<strong>'+success.data.data+'</strong>' + 'Failed';
				//var message = '<strong>'+success.data.data+'</strong>'+' uploding failed';
				Flash.create('danger', message);
			}
			//console.log(status);
		}).then(function(error) {
			//console.log(error);
		});

	}
	
this.uploadInspectionLot = function(file, uploadUrl) {
		
		if(file == undefined){
			var message = 'Please select file to upload';
			Flash.create('danger', message);
			return;
		}
		var fd = new FormData();
		fd.append('file', file);
		$http.post(uploadUrl, fd, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		}).then(function(success) {
			var status = success.data.code;
			if (status == "200") {
				var message = 'Inspection Lot for batch number '+'<strong>'+success.data.data+'</strong>'+' uploaded successfully';
				Flash.create('success', message);
				
			} 
			else if(status == "400"){
				var message = 'Please Try Again !';
				Flash.create('danger', message);
			}
		}).then(function(error) {
		});

	}

this.instructionUpload = function(file, uploadUrl) {
	
	if(file == undefined){
		var message = 'Please select file to upload';
		Flash.create('danger', message);
		return;
	}
	var fd = new FormData();
	fd.append('file', file);
	$http.post(uploadUrl, fd, {
		transformRequest : angular.identity,
		headers : {
			'Content-Type' : undefined
		}
	}).then(function(success) {
		var status = success.data.code;
		if (status == "200") {
			var message = 'Instruction PDF for '+'<strong>'+success.data.data+'</strong>'+' uploaded successfully';
			Flash.create('success', message);
			
		} 
		else if(status == "400"){
			var message = 'Please Try Again !';
			Flash.create('danger', message);
		}
	}).then(function(error) {
	});

}
} ]);

myAdmin.controller('myCtrl', [ '$scope', 'fileUpload',
		function($scope, fileUpload) {
			$scope.uploadFileForProd = function() {
				var file = $scope.myFile;
				var uploadUrl = "/suez/file/fileUpload";
				fileUpload.uploadFileToUrl(file, uploadUrl);
			};
			$scope.uploadFileForPack = function() {
				var file = $scope.myFile;
				var uploadUrl = "/suez/fileP/fileUploadForPack";
				fileUpload.uploadFileForPackagingUrl(file, uploadUrl);
			};
			$scope.inspectionLotUpload = function() {
				var file = $scope.myFile;
				var uploadUrl = "/suez/inspectionLot/qualityCheckFileUpload";
				fileUpload.uploadInspectionLot(file, uploadUrl);
			};
			$scope.instructionPdfUpload = function() {
				var file = $scope.myFile;
				var uploadUrl = "/suez/instructionFile/instructionPdfFileUpload";
				fileUpload.instructionUpload(file, uploadUrl);
			};
		} ]);

myAdmin.config(function($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise('/allOrders');

	$stateProvider

	// HOME STATES AND NESTED VIEWS ========================================
	.state('adduser', {
		url : '/adduser',
		templateUrl : 'view/addUser.html',
		controller : 'AddUserController',
		data : {
			displayName : 'Login',
		}
	}).state('edituser', {
		url : '/edituser',
		templateUrl : 'view/editUser.html',
		controller : 'EditUserController'
	}).state('uploadFile', {
		url : '/uploadFile',
		templateUrl : 'view/uploadFile.html',
		controller : 'EditUserController'
	}).state('uploadFileForPack', {
		url : '/uploadFileForPack',
		templateUrl : 'view/uploadFileForPacking.html',
		controller : 'EditUserController'
	}).state('planner', {
		url : '/planner',
		templateUrl : 'view/planner.html',
		controller : ''
	}).state('associateProcess', {
		url : '/associateProcess',
		templateUrl : 'view/association.html',
		controller : 'AssociationController'
	}).state('allOrders', {
		url : '/allOrders',
		templateUrl : 'view/allProcessOrders.html',
		controller : 'AllOrdersController'
	}).state('allOrder', {
		url : '/allOrder',
		templateUrl : '../admin/view/allProcessOrders.html',
		controller : 'AllOrdersController'
	})		
.state('temp', {
		url : '/temp',
		templateUrl : '../admin/view/temperature.html',
		controller : 'EmailAlertController'
	})	
	.state('usersForAlerts', {
		url : '/usersForAlerts',
		templateUrl : 'view/usersForAlerts.html',
		controller : 'UserAlertsController'
	})	
	
	
	.state('orders', {
		url : '/orders',
		templateUrl : 'view/orders.html',
		controller : 'PendingController'
	})
		.state('history', {
		url : '/history',
		templateUrl : 'view/history.html',
		controller : 'ProcessHistoryController'
	})
	
	.state('historyfirst', {
		url : '/history',
		templateUrl : 'view/historyfirst.html',
		controller : 'ProcessHistoryDetailsController'
	})
	
	
	.state('processOrder-status', {
		url : '/processOrder-status',
		templateUrl : 'view/live.html',
		controller : 'StatusInfoController'
	}).state('deletedProcess', {
		url : '/deletedProcess',
		templateUrl : 'view/deletedProcessOrders.html',
		controller : 'DeletedPoController'
	})
	.state('deletedPackingOrders', {
		url : '/deletedPackingOrders',
		templateUrl : 'view/deletepackingOrders.html',
		controller : 'DeletedPoControllerForPackageOrders'
	})
	.state('delete', {
		url : '/delete',
		templateUrl : 'view/deleteprocess.html',
		controller : 'DeletedPoControllerForProcessOrders'
	}).state('uploadInspectionLot', {
		url : '/uploadInspectionLot',
		templateUrl : 'view/uploadInspectionLot.html',
		controller : ''
	}).state('uploadInstructionPdf', {
		url : '/uploadInstructionPdf',
		templateUrl : 'view/instructionPdfUpload.html',
		controller : ''
	}).state('repacking-status-info', {
		url : '/repacking-status-info',
		templateUrl : 'view/repackingLive.html',
		controller : 'RepackingStatusInfoController'
	}).state('repackingHistory', {
		url : '/repackingHistory',
		templateUrl : 'view/repackingHistory.html',
		controller : 'RepackingHistoryController'
	}).state('repackingHistoryDetails', {
		url : '/repackingHistoryDetails',
		templateUrl : 'view/repackingHistoryDetails.html',
		controller : 'RepackingHistoryDetailsController'
	})
});

myAdmin.controller('logOutController', [ '$scope', '$window', '$http','$filter','$rootScope','Flash','$localStorage',
		function($scope, $window, $http, $filter, $rootScope, Flash, $localStorage) {
	//alert('controller');
	//$scope.adminUserName = $localStorage.userName;
	
	var userLogged = localStorage.getItem('loginUser');
	//alert(userLogged);
	
	$scope.showModal = true;
	$scope.showButtons = true;
	$scope.showClose = false;
	$scope.showFailed = false;
	var diffRoles =[];
	var sso =$localStorage.userSso;
	if(userLogged == null || userLogged == ''){
		//alert('Empty');
		$window.sessionStorage.ssoId = '';
		$window.sessionStorage.role = '';
		$localStorage.userSso = " ";	
		$localStorage.userData =" ";
		localStorage.setItem('loginUser', '');
		window.location.href = "/suez";
	}
	$http({		
		method : "GET",		
		url : '/suez/po/RoleList/'+ sso ,		
	headers : {		
			'Content-Type' : 'application/json'		
		}		
	}).then(function mySucces(response) {		
		//console.log(response);	
		//alert('Got all Rroles');
		$scope.roles = response.data;
		diffRoles = $scope.roles;
		/*alert($scope.roles[0].role);
		alert($scope.roles[1].role);
		alert($scope.roles[2].role);*/
		if(diffRoles.length > 1){
			$rootScope.showLoginAs=true;
		}
		
		for(var i=0; i<diffRoles.length; i++){
			if(diffRoles[i].role=="2"){
				$rootScope.isWarehouse=true;
			} else if (diffRoles[i].role=="3"){
				$rootScope.isManagement=true;
			} else if (diffRoles[i].role=="4"){
				$rootScope.isOperator=true;
			}else if (diffRoles[i].role=="5"){
				$rootScope.isQuality=true;
			}
		}
		//console.log($scope.alertInformation);		
				
	}, function myError(error) {		
		//console.log(error);		
	});
	
	//var roles=["2","3"];
	$rootScope.isAdmin=false;
	$rootScope.isWarehouse=false;
	$rootScope.isOperator=false;
	$rootScope.isQuality=false;
	$rootScope.isManagement=false;
	$rootScope.showLoginAs=false;
	
		
	$scope.warehouseLogin = function(){
		//alert('Login to warehouse');
		window.location.href = "../warehouse/warehouse.html";
	}
	
	$scope.operatorLogin = function(){
		//alert('Login to Production');
		window.location.href ="../productionOperator/poWelcome.html";
	}
	
	$scope.managementLogin = function(){
		//alert('Login to Production');
		window.location.href ="../management/management.html";
	}
	
	$scope.qualityLogin = function(){
		//alert('Login to Production');
		window.location.href ="../quality/quality.html";
	}
	
	$scope.getAlertsForAdmin =function(){		
							
				$http({		
						method : "GET",		
						url : '/suez/RoleBasedAlerts/alertsForAdmin ' ,		
					headers : {		
							'Content-Type' : 'application/json'		
						}		
					}).then(function mySucces(response) {		
						//console.log(response);		
						$scope.alertInformation = response.data;		
						//console.log($scope.alertInformation);		
								
					}, function myError(error) {		
						//console.log(error);		
					});		
							
				};		
				
	
	//$scope.CurrentDate = new Date();
	$scope.getAlertsForAdmin();
         
			$scope.count = 0;
			$scope.logOut = function() {
				
				$window.sessionStorage.ssoId = '';
				$window.sessionStorage.role = '';
				$localStorage.userSso = " ";	
				$localStorage.userData =" ";
				localStorage.setItem('loginUser', '');
				window.location.href = "/suez";
			};

			$scope.getUserInformation = function(ssoId) {
				var ssoId = $window.sessionStorage.ssoId;
				$http({
					method : "GET",
					url : '/suez/user/getUserBySSOId/' + ssoId,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function mySucces(response) {
					//console.log(response);
					$scope.userInformation = response.data;
					//console.log($scope.userInformation);
				}, function myError(error) {
					//console.log(error);
				});
			};
						
			$scope.update = function(newPassword,checkPassword) {
				if(newPassword == undefined || checkPassword == undefined){
					$scope.fillAll = true;
					return;
					
				}
				if(newPassword !=checkPassword){
					return;
				}
				var user={};
				user.ssoid=$window.sessionStorage.ssoId;
				user.password=newPassword;
				
				$http.post('/suez/user/changePassword', JSON.stringify(user), {
					'Content-Type' : 'application/json'
				}).then(function(response) {
					$scope.showModal = false;
					$scope.showButtons = false;
					$scope.showClose = true;
					$scope.showFailed = false;

				}, function(response) {
					// something went wrong
					/*$scope.showModal = false;
					$scope.showButtons = false;
					$scope.showClose = false;
					$scope.showFailed = true;*/
					var message = "";
					message = 'Please try again';
					Flash.create('warning', message);
					//return response;
				});

			}

		} ]);
