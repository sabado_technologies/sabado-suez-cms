myAdmin.factory('AdminService', function($http, $q) {

	return {

		getRoles : function() {
			return $http.get('/suez/user/getRoles').then(function(response) {

				return response;

			}, function(response) {
				return $q.reject(response);
			});
		},

		addUser : function(user , roles) {
			return $http.post('/suez/user/addUser/' + roles, JSON.stringify(user), {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;

			}, function(response) {
				return response;
			});
		},
		getEmailAlertData : function() {
			return $http.get('/suez/emailAlert/AllData').then(function(response) {

				return response;

			}, function(response) {
				return $q.reject(response);
			});
		},
		
		
		getEmailAlertDataWithRange : function(fromDate,toDate) {
			return $http.get('/suez/emailAlert/AllDataWithRange/'+ fromDate+'/'+toDate, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				//console.log('error')
				//alert('error');
			});
		},
		
		getUserRoles : function(sso) {
			return $http.get('/suez/po/RoleList/'+ sso, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				//console.log('error')
				//alert('error');
			});
		},
		
		getMultiRoles : function() {
			return $http.get('/suez/user/getMultiRoles').then(function(response) {
				return response;
			}, function(response) {
				//console.log('error')
				//alert('error');
			});
		},
		
		getProcessHistory : function() {
			return $http.get(
					'/suez/pending/getProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},		
		getProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getProcessOrdersForPostingCheck/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		} ,
		associatedPackingHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/associatedPackingInfoForPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		getRepackProcessHistory : function() {
			return $http.get(
					'/suez/pending/getRepackingProcessOrderHistory', {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
					});
		},
		getRepackProcessOrderForHistory : function(processorderProdNumber) {
			return $http.get(
					'/suez/WareHouseController/getPackingOrderForInventoryPosting/'+processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {
						return response;
					}, function(response) {
						//alert('error');
					});
		},
		
		updateExtraRm : function(process,rmInfo) {
			return $http.post('/suez/pending/addExtraRawMaterial/'+ process+'/'+rmInfo, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				//console.log('error')
				//alert('error');
			});
		},
		getPackinVesselHrs : function(process) {
			return $http.get('/suez/Management/cycleTimeForModal/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},
		
		getRepackPackinVesselHrs : function(process) {
			return $http.get('/suez/pending/repackingHours/' + process, {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;
			}, function(response) {
				return $q.reject(response);
			});
		},

	}

});

myAdmin.factory('EditUserService', function($http, $q) {
	return {

		getAllUsers : function() {
			return $http.get('/suez/user/getAllUsers').then(function(response) {
				return response;

			}, function(response) {
				return response;
			});
		},

		activeUsers : function(userList) {
			return $http.post('/suez/user/activateUser',
					JSON.stringify(userList), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},

		deActivateUsers : function(userList) {
			return $http.post('/suez/user/deActivateUser',
					JSON.stringify(userList), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},

		updateUsers : function(updateUser,roles) {
			return $http.post('/suez/user/editUser/'+ roles,
					JSON.stringify(updateUser), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},

		getActiveUsers : function() {
			return $http.get('/suez/user/getActiveUsers').then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},

		getDeActiveUsers : function() {
			return $http.get('/suez/user/getDeActiveUsers').then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},
		
		resetUserPassword : function(user) {
			return $http.post('/suez/user/resetPassword',
					JSON.stringify(user), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		}

	}
});

myAdmin.factory('AssociateService', function($http, $q) {

	return {

		getProduct : function() {
			return $http.get('/suez/Management/getProcessOrderInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		associtateProcessId : function(assocaite) {
			return $http.post('/suez/Management/associateProcessId',
					JSON.stringify(assocaite), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(error) {
				return {
					error : error.data.message
				};
			});
		},

		getProductOrders : function() {
			return $http.get('/suez/associate/getProductionOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getPackageOrders : function() {
			return $http.get('/suez/associate/getPackageOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		addAssociation : function(associationDetails) {
			return $http.post('/suez/associate/associateProcessOrders',
					JSON.stringify(associationDetails), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},
		getDeletedProcessOrders : function() {
			return $http.get('/suez/associate/getDeletedProcessOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		getProcessOrdersForDeletion : function() {
			
			return $http.get('/suez/RoleBasedAlerts/processOrdersForDeletion').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		getPackageOrdersForDeletion : function() {
			
			return $http.get('/suez/RoleBasedAlerts/packagingOrdersForDeletion').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		deletePackageProductionOrder : function(processorderPckgNumber ,remarks ,ssoId) {
			return $http.post('/suez/RoleBasedAlerts/deletePackagingOrder/'+processorderPckgNumber+"/"+remarks+"/"+ssoId ).then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
			
		},
		
		deleteProductionOrder : function(processorderProdNumber ,remarks ,ssoId) {
			return $http.post('/suez/RoleBasedAlerts/deleteProductionOrder/'+processorderProdNumber+"/"+remarks+"/"+ssoId ).then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
			
		}

	}

});

myAdmin.factory('PendingService', function($http, $q) {

	return {

		getPendingOrders : function() {
			return $http.get('/suez/pending/getPendingOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getReactors : function() {
			return $http.get('/suez/pending/getReactors').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		updateReactor : function(reactor) {
			return $http.post('/suez/pending/updateReactor',
					JSON.stringify(reactor), {
						'Content-Type' : 'application/json'
					}).then(function(response) {
				return response;

			}, function(response) {
				// something went wrong
				return response;
			});
		},
		
		getAllStatusInfo : function() {
			return $http.get('/suez/pending/getAllStatusInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},
		
		getAllRepackingStatusInfo : function() {
			return $http.get('/suez/pending/getAllRepackingStatusInfo').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		}

	}

});

myAdmin.factory('AllOrdersService', function($http, $q) {

	return {

		getAllOrders : function() {
			return $http.get('/suez/pending/getAllOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getReactors : function() {
			return $http.get('/suez/pending/getReactors').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysOrders : function() {
			return $http.get('/suez/pending/getTodaysOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getPastOrders : function() {
			return $http.get('/suez/pending/getPreviousOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getFutureOrders : function() {
			return $http.get('/suez/pending/getFutureOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getTodaysCompletedOrders : function() {
			return $http.get('/suez/pending/getTodaysCompletedOrders').then(
					function(response) {

						return response;

					}, function(response) {
						// something went wrong
						return $q.reject(response);
					});
		},

		getStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getStatus/' + processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getChargedStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getChargedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getPackedStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getPackedStatus/' + processorderProdNumber,
					{
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityCheckStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getQualityCheckStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getQualityInspectionStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getQualityInspectionStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},

		getInventoryStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
		
		getRmInventoryStatus : function(processorderProdNumber) {
			return $http.get(
					'/suez/pending/getRmInventoryStatus/'
							+ processorderProdNumber, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
	}

});

myAdmin.factory('EmailAlertService', function($http, $q) {

	return {

		getPackingAlerts : function() {
			return $http.get('/suez/emailAlert/getPackingAlerts').then(function(response) {

				return response;

			}, function(response) {
				return $q.reject(response);
			});
		},
		
		getPackDetails : function(fromDate,toDate) {
			return $http.get(
					'/suez/emailAlert/getPackingAlertsWithDate/'
					+ fromDate +'/'+ toDate, {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
		
		getTemperature : function(processOrder) {
			return $http.get(
					'/suez/emailAlert/getTemperature/'
					+ processOrder , {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		},
		
		getRmTemperature : function(processOrder) {
			return $http.get(
					'/suez/emailAlert/getRmTemperature/'
					+ processOrder , {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		}

	}
});

myAdmin.factory('UserAlertService', function($http, $q) {

	return {
		
		addUserForAlerts : function(user) {
			return $http.post('/suez/user/addUserForAlerts' , JSON.stringify(user), {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;

			}, function(response) {
				return response;
			});
		},

		getUsersForAlerts : function() {
			return $http.get('/suez/user/getAlertUsers').then(function(response) {

				return response;

			}, function(response) {
				return $q.reject(response);
			});
		},
		
		updateUserAlertInfo : function(user) {
			return $http.post('/suez/user/updateUserAlertInfo' , JSON.stringify(user), {
				'Content-Type' : 'application/json'
			}).then(function(response) {
				return response;

			}, function(response) {
				return response;
			});
		},
		
		deleteUserAlertInfo : function(userId) {
			return $http.post(
					'/suez/user/deleteUserAlertInfo/'
					+ userId , {
						'Content-Type' : 'application/json'
					}).then(function(response) {

				return response;

			}, function(response) {
				// something went wrong
				return $q.reject(response);
			});
		}
	}

});