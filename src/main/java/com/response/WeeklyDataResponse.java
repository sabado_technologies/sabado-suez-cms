package com.response;

public class WeeklyDataResponse {
	private String weekNumber;
	private String quantity;
	public WeeklyDataResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WeeklyDataResponse(String weekNumber, String quantity) {
		super();
		this.weekNumber = weekNumber;
		this.quantity = quantity;
	}
	public String getWeekNumber() {
		return weekNumber;
	}
	public void setWeekNumber(String weekNumber) {
		this.weekNumber = weekNumber;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	

}
