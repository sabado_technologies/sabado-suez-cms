package com.response;

public class PackingReportResponse {
private Long processorderPackageNumber;
private Long processorderProdNumber;
private Long sixMillionNumber;
private String actualQuantityAfterPacking;
private int packetSizeAfterPacking;
private String packingStartTime;
private String packingEndTime;
private String packingLeadTime;
public PackingReportResponse() {
	super();
	// TODO Auto-generated constructor stub
}

public PackingReportResponse(Long processorderPackageNumber, Long processorderProdNumber, Long sixMillionNumber,
		String actualQuantityAfterPacking, int packetSizeAfterPacking, String packingStartTime, String packingEndTime,
		String packingLeadTime) {
	super();
	this.processorderPackageNumber = processorderPackageNumber;
	this.processorderProdNumber = processorderProdNumber;
	this.sixMillionNumber = sixMillionNumber;
	this.actualQuantityAfterPacking = actualQuantityAfterPacking;
	this.packetSizeAfterPacking = packetSizeAfterPacking;
	this.packingStartTime = packingStartTime;
	this.packingEndTime = packingEndTime;
	this.packingLeadTime = packingLeadTime;
}

public Long getProcessorderPackageNumber() {
	return processorderPackageNumber;
}
public void setProcessorderPackageNumber(Long processorderPackageNumber) {
	this.processorderPackageNumber = processorderPackageNumber;
}
public Long getProcessorderProdNumber() {
	return processorderProdNumber;
}
public void setProcessorderProdNumber(Long processorderProdNumber) {
	this.processorderProdNumber = processorderProdNumber;
}
public String getPackingStartTime() {
	return packingStartTime;
}
public void setPackingStartTime(String packingStartTime) {
	this.packingStartTime = packingStartTime;
}
public String getPackingEndTime() {
	return packingEndTime;
}
public void setPackingEndTime(String packingEndTime) {
	this.packingEndTime = packingEndTime;
}
public String getPackingLeadTime() {
	return packingLeadTime;
}
public void setPackingLeadTime(String packingLeadTime) {
	this.packingLeadTime = packingLeadTime;
}

public Long getSixMillionNumber() {
	return sixMillionNumber;
}

public void setSixMillionNumber(Long sixMillionNumber) {
	this.sixMillionNumber = sixMillionNumber;
}

public String getActualQuantityAfterPacking() {
	return actualQuantityAfterPacking;
}

public void setActualQuantityAfterPacking(String actualQuantityAfterPacking) {
	this.actualQuantityAfterPacking = actualQuantityAfterPacking;
}

public int getPacketSizeAfterPacking() {
	return packetSizeAfterPacking;
}

public void setPacketSizeAfterPacking(int packetSizeAfterPacking) {
	this.packetSizeAfterPacking = packetSizeAfterPacking;
}

}
