package com.response;

public class LocationRawmaterialDetails {
	
	private String rawMaterialId;
	private String batchNumber;
	private String location;
	private String quantity;
	private String phase;
	
	public LocationRawmaterialDetails(String rawMaterialId, String batchNumber, String location, String quantity, String phase) {
		
		this.rawMaterialId = rawMaterialId;
		this.batchNumber = batchNumber;
		this.location = location;
		this.quantity = quantity;
		this.phase = phase;
	}
	
	public String getRawMaterialId() {
		return rawMaterialId;
	}
	public void setRawMaterialId(String rawMaterialId) {
		this.rawMaterialId = rawMaterialId;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public LocationRawmaterialDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}
	
	
}
