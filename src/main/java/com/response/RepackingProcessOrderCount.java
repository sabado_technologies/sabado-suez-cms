package com.response;

public class RepackingProcessOrderCount {
	
	private String pendingOrders;
	private String completedOrders;
	
	public RepackingProcessOrderCount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RepackingProcessOrderCount(String pendingOrders, String completedOrders) {
		super();
		this.pendingOrders = pendingOrders;
		this.completedOrders = completedOrders;
	}

	public String getPendingOrders() {
		return pendingOrders;
	}

	public void setPendingOrders(String pendingOrders) {
		this.pendingOrders = pendingOrders;
	}

	public String getCompletedOrders() {
		return completedOrders;
	}

	public void setCompletedOrders(String completedOrders) {
		this.completedOrders = completedOrders;
	}
	
	

}
