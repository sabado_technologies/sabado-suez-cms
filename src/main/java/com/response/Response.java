package com.response;

public class Response {
	private String code;
	private Object data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Response(String code, Object data) {
		super();
		this.code = code;
		this.data = data;
	}

}
