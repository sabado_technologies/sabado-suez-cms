package com.response;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class QualityCheckResponce {

	private int id;
	private String charNumber;
	private Long productNumber;
	private String discription;
	private String method;
	private List<String> range1;
	// private String range1;
	private List<String> range2;
	private Integer type;
	private String result;
	private String ProcessOrder;
	private String remark;

	public QualityCheckResponce() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QualityCheckResponce(int id, String charNumber, Long productNumber, String discription, String method,
			List<String> range1, List<String> range2, Integer type, String result, String processOrder, String remark) {
		super();
		this.id = id;
		this.charNumber = charNumber;
		this.productNumber = productNumber;
		this.discription = discription;
		this.method = method;
		this.range1 = range1;
		this.range2 = range2;
		this.type = type;
		this.result = result;
		ProcessOrder = processOrder;
		this.remark = remark;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCharNumber() {
		return charNumber;
	}

	public void setCharNumber(String charNumber) {
		this.charNumber = charNumber;
	}

	public Long getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(Long productNumber) {
		this.productNumber = productNumber;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public List<String> getRange1() {
		return range1;
	}

	public void setRange1(List<String> range1) {
		this.range1 = range1;
	}

	public List<String> getRange2() {
		return range2;
	}

	public void setRange2(List<String> range2) {
		this.range2 = range2;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getProcessOrder() {
		return ProcessOrder;
	}

	public void setProcessOrder(String processOrder) {
		ProcessOrder = processOrder;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
