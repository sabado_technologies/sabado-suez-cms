package com.response;

public class ManagementReportResponse {
	private Long sum;
	private String batchDate;
	private String plannedDate;
	private String completedDate;
	private Long processOrderProdNumber;
	private Long productnumber;
	private String productDescription;
	private int quarterNumber;
	private Float WeeklySum;
	private Float sum1;
	private String reactor;
	private String pckg1;
	private String pckg2;
	private String pckg3;
	private String pckg4;
	private String pckg5;
	private String pckg6;
	private String pckg7;
	private String pckg8;
	private String pckg9;
	private String pckg10;
	private String pckg11;
	private String pckg12;
	private String pckg13;
	private String pckg14;
	private String pckg15;
	private String pckg16;
	private String pckg17;
	private String pckg18;
	private String pckg19;
	private String pckg20;
	private String pckg21;
	private String pckg22;
	private String pckg23;
	private String pckg24;
	private String pckg25;
	private String pckg26;
	private String pckg27;
	private String pckg28;
	private String pckg29;
	private String pckg30;
	private String pckg31;
	private String pckg32;
	private String pckg33;
	private String pckg34;
	private String pckg35;
	private String pckg36;
	private String pckg37;
	private String pckg38;
	private String pckg39;
	private String pckg40;
	private String pckg41;
	
	private String quarterlySum;
	private String dailySum;
	private String completedTime;
	private String leadTime;

	
	public ManagementReportResponse() {
		// TODO Auto-generated constructor stub
	}


	public ManagementReportResponse(Long sum, String batchDate, String plannedDate, String completedDate,
			Long processOrderProdNumber, Long productnumber, String productDescription, int quarterNumber,
			Float weeklySum, Float sum1, String reactor, String pckg1, String pckg2, String pckg3, String pckg4,
			String pckg5, String pckg6, String pckg7, String pckg8, String pckg9, String pckg10, String pckg11,
			String pckg12, String pckg13, String pckg14, String pckg15, String pckg16, String pckg17, String pckg18,
			String pckg19, String pckg20, String pckg21, String pckg22, String pckg23, String pckg24, String pckg25,
			String pckg26, String pckg27, String pckg28, String pckg29, String pckg30, String pckg31, String pckg32,
			String pckg33, String pckg34, String pckg35, String pckg36, String pckg37, String pckg38, String pckg39,
			String pckg40, String pckg41, String quarterlySum, String dailySum, String completedTime, String leadTime) {
		super();
		this.sum = sum;
		this.batchDate = batchDate;
		this.plannedDate = plannedDate;
		this.completedDate = completedDate;
		this.processOrderProdNumber = processOrderProdNumber;
		this.productnumber = productnumber;
		this.productDescription = productDescription;
		this.quarterNumber = quarterNumber;
		this.WeeklySum = weeklySum;
		this.sum1 = sum1;
		this.reactor = reactor;
		this.pckg1 = pckg1;
		this.pckg2 = pckg2;
		this.pckg3 = pckg3;
		this.pckg4 = pckg4;
		this.pckg5 = pckg5;
		this.pckg6 = pckg6;
		this.pckg7 = pckg7;
		this.pckg8 = pckg8;
		this.pckg9 = pckg9;
		this.pckg10 = pckg10;
		this.pckg11 = pckg11;
		this.pckg12 = pckg12;
		this.pckg13 = pckg13;
		this.pckg14 = pckg14;
		this.pckg15 = pckg15;
		this.pckg16 = pckg16;
		this.pckg17 = pckg17;
		this.pckg18 = pckg18;
		this.pckg19 = pckg19;
		this.pckg20 = pckg20;
		this.pckg21 = pckg21;
		this.pckg22 = pckg22;
		this.pckg23 = pckg23;
		this.pckg24 = pckg24;
		this.pckg25 = pckg25;
		this.pckg26 = pckg26;
		this.pckg27 = pckg27;
		this.pckg28 = pckg28;
		this.pckg29 = pckg29;
		this.pckg30 = pckg30;
		this.pckg31 = pckg31;
		this.pckg32 = pckg32;
		this.pckg33 = pckg33;
		this.pckg34 = pckg34;
		this.pckg35 = pckg35;
		this.pckg36 = pckg36;
		this.pckg37 = pckg37;
		this.pckg38 = pckg38;
		this.pckg39 = pckg39;
		this.pckg40 = pckg40;
		this.pckg41 = pckg41;
		this.quarterlySum = quarterlySum;
		this.dailySum = dailySum;
		this.completedTime = completedTime;
		this.leadTime = leadTime;
	}


	public Long getSum() {
		return sum;
	}


	public void setSum(Long sum) {
		this.sum = sum;
	}


	public String getBatchDate() {
		return batchDate;
	}


	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}


	public String getPlannedDate() {
		return plannedDate;
	}


	public void setPlannedDate(String plannedDate) {
		this.plannedDate = plannedDate;
	}


	public String getCompletedDate() {
		return completedDate;
	}


	public void setCompletedDate(String completedDate) {
		this.completedDate = completedDate;
	}


	public Long getProcessOrderProdNumber() {
		return processOrderProdNumber;
	}


	public void setProcessOrderProdNumber(Long processOrderProdNumber) {
		this.processOrderProdNumber = processOrderProdNumber;
	}


	public Long getProductnumber() {
		return productnumber;
	}


	public void setProductnumber(Long productnumber) {
		this.productnumber = productnumber;
	}


	public String getProductDescription() {
		return productDescription;
	}


	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}


	public int getQuarterNumber() {
		return quarterNumber;
	}


	public void setQuarterNumber(int quarterNumber) {
		this.quarterNumber = quarterNumber;
	}


	public Float getWeeklySum() {
		return WeeklySum;
	}


	public void setWeeklySum(Float weeklySum) {
		WeeklySum = weeklySum;
	}


	public Float getSum1() {
		return sum1;
	}


	public void setSum1(Float sum1) {
		this.sum1 = sum1;
	}


	public String getReactor() {
		return reactor;
	}


	public void setReactor(String reactor) {
		this.reactor = reactor;
	}


	public String getPckg1() {
		return pckg1;
	}


	public void setPckg1(String pckg1) {
		this.pckg1 = pckg1;
	}


	public String getPckg2() {
		return pckg2;
	}


	public void setPckg2(String pckg2) {
		this.pckg2 = pckg2;
	}


	public String getPckg3() {
		return pckg3;
	}


	public void setPckg3(String pckg3) {
		this.pckg3 = pckg3;
	}


	public String getPckg4() {
		return pckg4;
	}


	public void setPckg4(String pckg4) {
		this.pckg4 = pckg4;
	}


	public String getPckg5() {
		return pckg5;
	}


	public void setPckg5(String pckg5) {
		this.pckg5 = pckg5;
	}


	public String getPckg6() {
		return pckg6;
	}


	public void setPckg6(String pckg6) {
		this.pckg6 = pckg6;
	}


	public String getPckg7() {
		return pckg7;
	}


	public void setPckg7(String pckg7) {
		this.pckg7 = pckg7;
	}


	public String getPckg8() {
		return pckg8;
	}


	public void setPckg8(String pckg8) {
		this.pckg8 = pckg8;
	}


	public String getPckg9() {
		return pckg9;
	}


	public void setPckg9(String pckg9) {
		this.pckg9 = pckg9;
	}


	public String getPckg10() {
		return pckg10;
	}


	public void setPckg10(String pckg10) {
		this.pckg10 = pckg10;
	}


	public String getPckg11() {
		return pckg11;
	}


	public void setPckg11(String pckg11) {
		this.pckg11 = pckg11;
	}


	public String getPckg12() {
		return pckg12;
	}


	public void setPckg12(String pckg12) {
		this.pckg12 = pckg12;
	}


	public String getPckg13() {
		return pckg13;
	}


	public void setPckg13(String pckg13) {
		this.pckg13 = pckg13;
	}


	public String getPckg14() {
		return pckg14;
	}


	public void setPckg14(String pckg14) {
		this.pckg14 = pckg14;
	}


	public String getPckg15() {
		return pckg15;
	}


	public void setPckg15(String pckg15) {
		this.pckg15 = pckg15;
	}


	public String getPckg16() {
		return pckg16;
	}


	public void setPckg16(String pckg16) {
		this.pckg16 = pckg16;
	}


	public String getPckg17() {
		return pckg17;
	}


	public void setPckg17(String pckg17) {
		this.pckg17 = pckg17;
	}


	public String getPckg18() {
		return pckg18;
	}


	public void setPckg18(String pckg18) {
		this.pckg18 = pckg18;
	}


	public String getPckg19() {
		return pckg19;
	}


	public void setPckg19(String pckg19) {
		this.pckg19 = pckg19;
	}


	public String getPckg20() {
		return pckg20;
	}


	public void setPckg20(String pckg20) {
		this.pckg20 = pckg20;
	}


	public String getPckg21() {
		return pckg21;
	}


	public void setPckg21(String pckg21) {
		this.pckg21 = pckg21;
	}


	public String getPckg22() {
		return pckg22;
	}


	public void setPckg22(String pckg22) {
		this.pckg22 = pckg22;
	}


	public String getPckg23() {
		return pckg23;
	}


	public void setPckg23(String pckg23) {
		this.pckg23 = pckg23;
	}


	public String getPckg24() {
		return pckg24;
	}


	public void setPckg24(String pckg24) {
		this.pckg24 = pckg24;
	}


	public String getPckg25() {
		return pckg25;
	}


	public void setPckg25(String pckg25) {
		this.pckg25 = pckg25;
	}


	public String getPckg26() {
		return pckg26;
	}


	public void setPckg26(String pckg26) {
		this.pckg26 = pckg26;
	}


	public String getPckg27() {
		return pckg27;
	}


	public void setPckg27(String pckg27) {
		this.pckg27 = pckg27;
	}


	public String getPckg28() {
		return pckg28;
	}


	public void setPckg28(String pckg28) {
		this.pckg28 = pckg28;
	}


	public String getPckg29() {
		return pckg29;
	}


	public void setPckg29(String pckg29) {
		this.pckg29 = pckg29;
	}


	public String getPckg30() {
		return pckg30;
	}


	public void setPckg30(String pckg30) {
		this.pckg30 = pckg30;
	}


	public String getPckg31() {
		return pckg31;
	}


	public void setPckg31(String pckg31) {
		this.pckg31 = pckg31;
	}


	public String getPckg32() {
		return pckg32;
	}


	public void setPckg32(String pckg32) {
		this.pckg32 = pckg32;
	}


	public String getPckg33() {
		return pckg33;
	}


	public void setPckg33(String pckg33) {
		this.pckg33 = pckg33;
	}


	public String getPckg34() {
		return pckg34;
	}


	public void setPckg34(String pckg34) {
		this.pckg34 = pckg34;
	}


	public String getPckg35() {
		return pckg35;
	}


	public void setPckg35(String pckg35) {
		this.pckg35 = pckg35;
	}


	public String getPckg36() {
		return pckg36;
	}


	public void setPckg36(String pckg36) {
		this.pckg36 = pckg36;
	}


	public String getPckg37() {
		return pckg37;
	}


	public void setPckg37(String pckg37) {
		this.pckg37 = pckg37;
	}


	public String getPckg38() {
		return pckg38;
	}


	public void setPckg38(String pckg38) {
		this.pckg38 = pckg38;
	}


	public String getPckg39() {
		return pckg39;
	}


	public void setPckg39(String pckg39) {
		this.pckg39 = pckg39;
	}


	public String getPckg40() {
		return pckg40;
	}


	public void setPckg40(String pckg40) {
		this.pckg40 = pckg40;
	}


	public String getPckg41() {
		return pckg41;
	}


	public void setPckg41(String pckg41) {
		this.pckg41 = pckg41;
	}


	public String getQuarterlySum() {
		return quarterlySum;
	}


	public void setQuarterlySum(String quarterlySum) {
		this.quarterlySum = quarterlySum;
	}


	public String getDailySum() {
		return dailySum;
	}


	public void setDailySum(String dailySum) {
		this.dailySum = dailySum;
	}


	public String getCompletedTime() {
		return completedTime;
	}


	public void setCompletedTime(String completedTime) {
		this.completedTime = completedTime;
	}


	public String getLeadTime() {
		return leadTime;
	}


	public void setLeadTime(String leadTime) {
		this.leadTime = leadTime;
	}

	

}
