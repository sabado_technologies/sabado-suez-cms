package com.response;

import java.util.List;

public class QuarterBarchartResponse1 {
	List<QuarterBarchartResponse > response ;

	public QuarterBarchartResponse1() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QuarterBarchartResponse1(List<QuarterBarchartResponse> response) {
		super();
		this.response = response;
	}

	public List<QuarterBarchartResponse> getResponse() {
		return response;
	}

	public void setResponse(List<QuarterBarchartResponse> response) {
		this.response = response;
	}
	
}
