package com.response;

import java.sql.Timestamp;

public class CycleTimeResponse {
 
	private Float chargingTime;
	private Float qualityTime;
	private Float packingTime;
	private Float completedTime;
	private String processOrderNumber;
	private String productNumber;
	private String productDescription;
	private String batchDate;
	private String batchNumber;
	private String processingDate;
	
	
	
	public CycleTimeResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CycleTimeResponse(Float chargingTime, Float qualityTime, Float packingTime, Float completedTime,
			String processOrderNumber, String productNumber, String productDescription, String batchDate,
			String batchNumber, String processingDate) {
		super();
		this.chargingTime = chargingTime;
		this.qualityTime = qualityTime;
		this.packingTime = packingTime;
		this.completedTime = completedTime;
		this.processOrderNumber = processOrderNumber;
		this.productNumber = productNumber;
		this.productDescription = productDescription;
		this.batchDate = batchDate;
		this.batchNumber = batchNumber;
		this.processingDate = processingDate;
	}




	public Float getChargingTime() {
		return chargingTime;
	}

	public void setChargingTime(Float chargingTime) {
		this.chargingTime = chargingTime;
	}

	public Float getQualityTime() {
		return qualityTime;
	}

	public void setQualityTime(Float qualityTime) {
		this.qualityTime = qualityTime;
	}

	public Float getPackingTime() {
		return packingTime;
	}

	public void setPackingTime(Float packingTime) {
		this.packingTime = packingTime;
	}

	public Float getCompletedTime() {
		return completedTime;
	}

	public void setCompletedTime(Float completedTime) {
		this.completedTime = completedTime;
	}

	public String getProcessOrderNumber() {
		return processOrderNumber;
	}

	public void setProcessOrderNumber(String processOrderNumber) {
		this.processOrderNumber = processOrderNumber;
	}


	public String getProductNumber() {
		return productNumber;
	}


	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}


	public String getProductDescription() {
		return productDescription;
	}


	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getProcessingDate() {
		return processingDate;
	}

	public void setProcessingDate(String processingDate) {
		this.processingDate = processingDate;
	}
	
	
	
	
	
}
