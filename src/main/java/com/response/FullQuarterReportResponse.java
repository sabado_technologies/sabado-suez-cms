package com.response;

import java.util.ArrayList;
import java.util.List;

public class FullQuarterReportResponse {
	private int quarterNumber = 0;
	private Double actualProductionPer = 0.0 ;
	private Double remainingProductionPer = 0.0;
	private List<List<Object>> productInfos = new ArrayList<List<Object>>() ;
	
	
	public int getQuarterNumber() {
		return quarterNumber;
	}
	public void setQuarterNumber(int quarterNumber) {
		this.quarterNumber = quarterNumber;
	}
	public Double getActualProductionPer() {
		return actualProductionPer;
	}
	public void setActualProductionPer(Double actualProductionPer) {
		this.actualProductionPer = actualProductionPer;
	}
	public Double getRemainingProductionPer() {
		return remainingProductionPer;
	}
	public void setRemainingProductionPer(Double remainingProductionPer) {
		this.remainingProductionPer = remainingProductionPer;
	}
	public List<List<Object>> getProductInfos() {
		return productInfos;
	}
	public void setProductInfos(List<List<Object>> productInfos) {
		this.productInfos = productInfos;
	}
	
	
}
