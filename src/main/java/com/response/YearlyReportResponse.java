package com.response;

public class YearlyReportResponse {
	private int year;
	private String quarterlySum;
	private String quarter1;
	private String quarter2;
	private String quarter3;
	private String quarter4;
	private int weekNumber;
	private int dayNumber;
	private String dailySum;
	private String weeklySum;
	
	public YearlyReportResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public YearlyReportResponse(int year, String quarterlySum, String quarter1, String quarter2, String quarter3,
			String quarter4, int weekNumber, int dayNumber, String dailySum, String weeklySum) {
		super();
		this.year = year;
		this.quarterlySum = quarterlySum;
		this.quarter1 = quarter1;
		this.quarter2 = quarter2;
		this.quarter3 = quarter3;
		this.quarter4 = quarter4;
		this.weekNumber = weekNumber;
		this.dayNumber = dayNumber;
		this.dailySum = dailySum;
		this.weeklySum = weeklySum;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getQuarterlySum() {
		return quarterlySum;
	}
	public void setQuarterlySum(String quarterlySum) {
		this.quarterlySum = quarterlySum;
	}
	public String getQuarter1() {
		return quarter1;
	}
	public void setQuarter1(String quarter1) {
		this.quarter1 = quarter1;
	}
	public String getQuarter2() {
		return quarter2;
	}
	public void setQuarter2(String quarter2) {
		this.quarter2 = quarter2;
	}
	public String getQuarter3() {
		return quarter3;
	}
	public void setQuarter3(String quarter3) {
		this.quarter3 = quarter3;
	}
	public String getQuarter4() {
		return quarter4;
	}
	public void setQuarter4(String quarter4) {
		this.quarter4 = quarter4;
	}
	public int getWeekNumber() {
		return weekNumber;
	}
	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}
	public int getDayNumber() {
		return dayNumber;
	}
	public void setDayNumber(int dayNumber) {
		this.dayNumber = dayNumber;
	}
	public String getDailySum() {
		return dailySum;
	}
	public void setDailySum(String dailySum) {
		this.dailySum = dailySum;
	}
	public String getWeeklySum() {
		return weeklySum;
	}
	public void setWeeklySum(String weeklySum) {
		this.weeklySum = weeklySum;
	}
	

}
