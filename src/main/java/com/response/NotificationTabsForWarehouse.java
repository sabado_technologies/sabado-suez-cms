package com.response;

public class NotificationTabsForWarehouse {
	
	private String processOrdersCount;
	private String pendingOrdersCount;
	private String repackingOrdersCount;
	private String inventoryCheckCount;
	private String inventoryPostingCount;
	private String repackInventoryCheckCount;
	private String repackInventoryPostingCount;
	
	public NotificationTabsForWarehouse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotificationTabsForWarehouse(String processOrdersCount, String pendingOrdersCount,
			String repackingOrdersCount, String inventoryCheckCount, String inventoryPostingCount,
			String repackInventoryCheckCount, String repackInventoryPostingCount) {
		super();
		this.processOrdersCount = processOrdersCount;
		this.pendingOrdersCount = pendingOrdersCount;
		this.repackingOrdersCount = repackingOrdersCount;
		this.inventoryCheckCount = inventoryCheckCount;
		this.inventoryPostingCount = inventoryPostingCount;
		this.repackInventoryCheckCount = repackInventoryCheckCount;
		this.repackInventoryPostingCount = repackInventoryPostingCount;
	}

	public String getProcessOrdersCount() {
		return processOrdersCount;
	}

	public void setProcessOrdersCount(String processOrdersCount) {
		this.processOrdersCount = processOrdersCount;
	}

	public String getPendingOrdersCount() {
		return pendingOrdersCount;
	}

	public void setPendingOrdersCount(String pendingOrdersCount) {
		this.pendingOrdersCount = pendingOrdersCount;
	}

	public String getRepackingOrdersCount() {
		return repackingOrdersCount;
	}

	public void setRepackingOrdersCount(String repackingOrdersCount) {
		this.repackingOrdersCount = repackingOrdersCount;
	}

	public String getInventoryCheckCount() {
		return inventoryCheckCount;
	}

	public void setInventoryCheckCount(String inventoryCheckCount) {
		this.inventoryCheckCount = inventoryCheckCount;
	}

	public String getInventoryPostingCount() {
		return inventoryPostingCount;
	}

	public void setInventoryPostingCount(String inventoryPostingCount) {
		this.inventoryPostingCount = inventoryPostingCount;
	}

	public String getRepackInventoryCheckCount() {
		return repackInventoryCheckCount;
	}

	public void setRepackInventoryCheckCount(String repackInventoryCheckCount) {
		this.repackInventoryCheckCount = repackInventoryCheckCount;
	}

	public String getRepackInventoryPostingCount() {
		return repackInventoryPostingCount;
	}

	public void setRepackInventoryPostingCount(String repackInventoryPostingCount) {
		this.repackInventoryPostingCount = repackInventoryPostingCount;
	}
	
	

}
