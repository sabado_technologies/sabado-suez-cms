package com.response;

import java.util.List;

public class AssociationResponse {
	private List<String> processOrderProd;
	private List<String> processOrderPack;
	public List<String> getProcessOrderProd() {
		return processOrderProd;
	}
	public void setProcessOrderProd(List<String> processOrderProd) {
		this.processOrderProd = processOrderProd;
	}
	public List<String> getProcessOrderPack() {
		return processOrderPack;
	}
	public void setProcessOrderPack(List<String> processOrderPack) {
		this.processOrderPack = processOrderPack;
	}
	
	
}
