package com.response;

public class EmailAlertResponce {
	
	private int noOfAlert;
	private Long processOrder;
	private String sso;
	private String userName;
	private String process;
	private String reactor;
	private String time;
	private String temperature;
	private String date;
	
	public EmailAlertResponce() {
		super();
	}

	public EmailAlertResponce(int noOfAlert, Long processOrder, String sso, String userName, String process,
			String reactor, String time , String temperature , String date) {
		super();
		this.noOfAlert = noOfAlert;
		this.processOrder = processOrder;
		this.sso = sso;
		this.userName = userName;
		this.process = process;
		this.reactor = reactor;
		this.time = time;
		this.temperature = temperature;
		this.date = date;
	}

	public int getNoOfAlert() {
		return noOfAlert;
	}

	public void setNoOfAlert(int noOfAlert) {
		this.noOfAlert = noOfAlert;
	}

	public Long getProcessOrder() {
		return processOrder;
	}

	public void setProcessOrder(Long processOrder) {
		this.processOrder = processOrder;
	}

	public String getSso() {
		return sso;
	}

	public void setSso(String sso) {
		this.sso = sso;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getReactor() {
		return reactor;
	}

	public void setReactor(String reactor) {
		this.reactor = reactor;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	

}
