package com.response;

public class NotificationCounterResponse {
	private int processorderCount;
	private int rmPostingCount;
	private int repackProcessorderCount;
	private int repackRmPostingCount;
	public NotificationCounterResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NotificationCounterResponse(int processorderCount, int rmPostingCount, int repackProcessorderCount,
			int repackRmPostingCount) {
		super();
		this.processorderCount = processorderCount;
		this.rmPostingCount = rmPostingCount;
		this.repackProcessorderCount = repackProcessorderCount;
		this.repackRmPostingCount = repackRmPostingCount;
	}
	public int getProcessorderCount() {
		return processorderCount;
	}
	public void setProcessorderCount(int processorderCount) {
		this.processorderCount = processorderCount;
	}
	public int getRmPostingCount() {
		return rmPostingCount;
	}
	public void setRmPostingCount(int rmPostingCount) {
		this.rmPostingCount = rmPostingCount;
	}
	public int getRepackProcessorderCount() {
		return repackProcessorderCount;
	}
	public void setRepackProcessorderCount(int repackProcessorderCount) {
		this.repackProcessorderCount = repackProcessorderCount;
	}
	public int getRepackRmPostingCount() {
		return repackRmPostingCount;
	}
	public void setRepackRmPostingCount(int repackRmPostingCount) {
		this.repackRmPostingCount = repackRmPostingCount;
	}
	
	

}
