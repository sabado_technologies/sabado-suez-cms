package com.response;

import java.util.List;

import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.RawmaterialTable;
import com.entity.Status;

public class ProcessOrderRMResponse {


	private Integer processOrderRm;
	private ProcessorderProd processorderProd;
	private ProductTable productTable;
	private RawmaterialTable rawmaterialTable;
	private Status status;
	private String type;
	private String phase;
	private String quantity;
	private List<LocationRm> locationRm;
	private String extraRm;
	
	public ProcessOrderRMResponse(Integer processOrderRm, ProcessorderProd processorderProd, ProductTable productTable,
			RawmaterialTable rawmaterialTable, Status status, String type, String phase, String quantity,
			List<LocationRm> locationRm, String extraRm) {
		super();
		this.processOrderRm = processOrderRm;
		this.processorderProd = processorderProd;
		this.productTable = productTable;
		this.rawmaterialTable = rawmaterialTable;
		this.status = status;
		this.type = type;
		this.phase = phase;
		this.quantity = quantity;
		this.locationRm = locationRm;
		this.extraRm = extraRm;
	}
	public ProcessOrderRMResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getProcessOrderRm() {
		return processOrderRm;
	}
	public void setProcessOrderRm(Integer processOrderRm) {
		this.processOrderRm = processOrderRm;
	}
	public ProcessorderProd getProcessorderProd() {
		return processorderProd;
	}
	public void setProcessorderProd(ProcessorderProd processorderProd) {
		this.processorderProd = processorderProd;
	}
	public ProductTable getProductTable() {
		return productTable;
	}
	public void setProductTable(ProductTable productTable) {
		this.productTable = productTable;
	}
	public RawmaterialTable getRawmaterialTable() {
		return rawmaterialTable;
	}
	public void setRawmaterialTable(RawmaterialTable rawmaterialTable) {
		this.rawmaterialTable = rawmaterialTable;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPhase() {
		return phase;
	}
	public void setPhase(String phase) {
		this.phase = phase;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public List<LocationRm> getLocationRm() {
		return locationRm;
	}
	public void setLocationRm(List<LocationRm> locationRm) {
		this.locationRm = locationRm;
	}
	public String getExtraRm() {
		return extraRm;
	}
	public void setExtraRm(String extraRm) {
		this.extraRm = extraRm;
	}
	
	

}
