package com.response;

import java.util.List;

import com.entity.ContainerTable;
import com.entity.ProcessorderProd;
import com.entity.Status;

public class PackingOrderInventoryPostingResponse {

	private long processorderPckgNumber;
	private ContainerTable containerTable;
	private ProcessorderProd processorderProd;
	private Status status;
	private Long productNumber;
	private String batchNumber;
	private String batchDate;
	private Long totalQuantity;
	private Long wghtPckg;
	private String volume;
	private String sixmlNumber;
	private Integer isAssociated;
	private String productName;
	private String processQuant;
	private String materialMemo;
	private boolean isRepack;
	private String remarks;
	private String storage;
	private String pdfDate;
	private String completedDate;
	private String quantityAfterPacking ;
	private String locationAfterPacking;
	private List<LocationRm> locationRm;
	private int poUsedPails;
	private String packingRemarks;
	
	public PackingOrderInventoryPostingResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PackingOrderInventoryPostingResponse(long processorderPckgNumber, ContainerTable containerTable,
			ProcessorderProd processorderProd, Status status, Long productNumber, String batchNumber, String batchDate,
			Long totalQuantity, Long wghtPckg, String volume, String sixmlNumber, Integer isAssociated,
			String productName, String processQuant, String materialMemo, boolean isRepack, String remarks,
			String storage, String pdfDate, String completedDate, String quantityAfterPacking,
			String locationAfterPacking, List<LocationRm> locationRm, int poUsedPails, String packingRemarks) {
		super();
		this.processorderPckgNumber = processorderPckgNumber;
		this.containerTable = containerTable;
		this.processorderProd = processorderProd;
		this.status = status;
		this.productNumber = productNumber;
		this.batchNumber = batchNumber;
		this.batchDate = batchDate;
		this.totalQuantity = totalQuantity;
		this.wghtPckg = wghtPckg;
		this.volume = volume;
		this.sixmlNumber = sixmlNumber;
		this.isAssociated = isAssociated;
		this.productName = productName;
		this.processQuant = processQuant;
		this.materialMemo = materialMemo;
		this.isRepack = isRepack;
		this.remarks = remarks;
		this.storage = storage;
		this.pdfDate = pdfDate;
		this.completedDate = completedDate;
		this.quantityAfterPacking = quantityAfterPacking;
		this.locationAfterPacking = locationAfterPacking;
		this.locationRm = locationRm;
		this.poUsedPails = poUsedPails;
		this.packingRemarks = packingRemarks;
	}

	public long getProcessorderPckgNumber() {
		return processorderPckgNumber;
	}

	public void setProcessorderPckgNumber(long processorderPckgNumber) {
		this.processorderPckgNumber = processorderPckgNumber;
	}

	public ContainerTable getContainerTable() {
		return containerTable;
	}

	public void setContainerTable(ContainerTable containerTable) {
		this.containerTable = containerTable;
	}

	public ProcessorderProd getProcessorderProd() {
		return processorderProd;
	}

	public void setProcessorderProd(ProcessorderProd processorderProd) {
		this.processorderProd = processorderProd;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(Long productNumber) {
		this.productNumber = productNumber;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	public Long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Long getWghtPckg() {
		return wghtPckg;
	}

	public void setWghtPckg(Long wghtPckg) {
		this.wghtPckg = wghtPckg;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getSixmlNumber() {
		return sixmlNumber;
	}

	public void setSixmlNumber(String sixmlNumber) {
		this.sixmlNumber = sixmlNumber;
	}

	public Integer getIsAssociated() {
		return isAssociated;
	}

	public void setIsAssociated(Integer isAssociated) {
		this.isAssociated = isAssociated;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProcessQuant() {
		return processQuant;
	}

	public void setProcessQuant(String processQuant) {
		this.processQuant = processQuant;
	}

	public String getMaterialMemo() {
		return materialMemo;
	}

	public void setMaterialMemo(String materialMemo) {
		this.materialMemo = materialMemo;
	}

	public boolean isRepack() {
		return isRepack;
	}

	public void setRepack(boolean isRepack) {
		this.isRepack = isRepack;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getPdfDate() {
		return pdfDate;
	}

	public void setPdfDate(String pdfDate) {
		this.pdfDate = pdfDate;
	}

	public String getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(String completedDate) {
		this.completedDate = completedDate;
	}

	public String getQuantityAfterPacking() {
		return quantityAfterPacking;
	}

	public void setQuantityAfterPacking(String quantityAfterPacking) {
		this.quantityAfterPacking = quantityAfterPacking;
	}

	public String getLocationAfterPacking() {
		return locationAfterPacking;
	}

	public void setLocationAfterPacking(String locationAfterPacking) {
		this.locationAfterPacking = locationAfterPacking;
	}

	public List<LocationRm> getLocationRm() {
		return locationRm;
	}

	public void setLocationRm(List<LocationRm> locationRm) {
		this.locationRm = locationRm;
	}

	public int getPoUsedPails() {
		return poUsedPails;
	}

	public void setPoUsedPails(int poUsedPails) {
		this.poUsedPails = poUsedPails;
	}

	public String getPackingRemarks() {
		return packingRemarks;
	}

	public void setPackingRemarks(String packingRemarks) {
		this.packingRemarks = packingRemarks;
	}
	
	
	
		
}
