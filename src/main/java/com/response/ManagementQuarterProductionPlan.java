package com.response;

public class ManagementQuarterProductionPlan {

	private int totalWorkingDaysQ1;
	private int totalWorkingDaysQ2;
	private int totalWorkingDaysQ3;
	private int totalWorkingDaysQ4;
	private int actualWorkingDays;
	private String actualQuantity;
	private int quarter ;
	private Long plannedProductionQ1;
	private Long plannedProductionQ2;
	private Long plannedProductionQ3;
	private Long plannedProductionQ4;
	private int workingDays;
	private Long plannedProduction;
	
	

	public ManagementQuarterProductionPlan(int totalWorkingDaysQ1, int totalWorkingDaysQ2, int totalWorkingDaysQ3,
			int totalWorkingDaysQ4, int actualWorkingDays, String actualQuantity, int quarter, Long plannedProductionQ1,
			Long plannedProductionQ2, Long plannedProductionQ3, Long plannedProductionQ4, int workingDays,
			Long plannedProduction) {
		super();
		this.totalWorkingDaysQ1 = totalWorkingDaysQ1;
		this.totalWorkingDaysQ2 = totalWorkingDaysQ2;
		this.totalWorkingDaysQ3 = totalWorkingDaysQ3;
		this.totalWorkingDaysQ4 = totalWorkingDaysQ4;
		this.actualWorkingDays = actualWorkingDays;
		this.actualQuantity = actualQuantity;
		this.quarter = quarter;
		this.plannedProductionQ1 = plannedProductionQ1;
		this.plannedProductionQ2 = plannedProductionQ2;
		this.plannedProductionQ3 = plannedProductionQ3;
		this.plannedProductionQ4 = plannedProductionQ4;
		this.workingDays = workingDays;
		this.plannedProduction = plannedProduction;
	}

	public int getTotalWorkingDaysQ1() {
		return totalWorkingDaysQ1;
	}

	public void setTotalWorkingDaysQ1(int totalWorkingDaysQ1) {
		this.totalWorkingDaysQ1 = totalWorkingDaysQ1;
	}

	public int getTotalWorkingDaysQ2() {
		return totalWorkingDaysQ2;
	}

	public void setTotalWorkingDaysQ2(int totalWorkingDaysQ2) {
		this.totalWorkingDaysQ2 = totalWorkingDaysQ2;
	}

	public int getTotalWorkingDaysQ3() {
		return totalWorkingDaysQ3;
	}

	public void setTotalWorkingDaysQ3(int totalWorkingDaysQ3) {
		this.totalWorkingDaysQ3 = totalWorkingDaysQ3;
	}

	public int getTotalWorkingDaysQ4() {
		return totalWorkingDaysQ4;
	}

	public void setTotalWorkingDaysQ4(int totalWorkingDaysQ4) {
		this.totalWorkingDaysQ4 = totalWorkingDaysQ4;
	}

	public int getActualWorkingDays() {
		return actualWorkingDays;
	}

	public void setActualWorkingDays(int actualWorkingDays) {
		this.actualWorkingDays = actualWorkingDays;
	}

	public String getActualQuantity() {
		return actualQuantity;
	}

	public void setActualQuantity(String actualQuantity) {
		this.actualQuantity = actualQuantity;
	}

	public Long getPlannedProductionQ1() {
		return plannedProductionQ1;
	}

	public void setPlannedProductionQ1(Long plannedProductionQ1) {
		this.plannedProductionQ1 = plannedProductionQ1;
	}

	public Long getPlannedProductionQ2() {
		return plannedProductionQ2;
	}

	public void setPlannedProductionQ2(Long plannedProductionQ2) {
		this.plannedProductionQ2 = plannedProductionQ2;
	}

	public Long getPlannedProductionQ3() {
		return plannedProductionQ3;
	}

	public void setPlannedProductionQ3(Long plannedProductionQ3) {
		this.plannedProductionQ3 = plannedProductionQ3;
	}

	public Long getPlannedProductionQ4() {
		return plannedProductionQ4;
	}

	public void setPlannedProductionQ4(Long plannedProductionQ4) {
		this.plannedProductionQ4 = plannedProductionQ4;
	}
	
	public int getQuarter() {
		return quarter;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public int getWorkingDays() {
		return workingDays;
	}

	public void setWorkingDays(int workingDays) {
		this.workingDays = workingDays;
	}

	public Long getPlannedProduction() {
		return plannedProduction;
	}

	public void setPlannedProduction(Long plannedProduction) {
		this.plannedProduction = plannedProduction;
	}
	
	
}
