package com.response;

public class AverageCycleTimeResponse {
	
	String averageChargingTime;
	String averageQualityTime;
	String averagePackingTime;
	String averageCompletedTime;
	
	
	
	public AverageCycleTimeResponse() {
		super();
		// TODO Auto-generated constructor stub
	}


	public AverageCycleTimeResponse(String averageChargingTime, String averageQualityTime, String averagePackingTime,
			String averageCompletedTime) {
		super();
		this.averageChargingTime = averageChargingTime;
		this.averageQualityTime = averageQualityTime;
		this.averagePackingTime = averagePackingTime;
		this.averageCompletedTime = averageCompletedTime;
	}
	
	
	public String getAverageChargingTime() {
		return averageChargingTime;
	}
	public void setAverageChargingTime(String averageChargingTime) {
		this.averageChargingTime = averageChargingTime;
	}
	public String getAverageQualityTime() {
		return averageQualityTime;
	}
	public void setAverageQualityTime(String averageQualityTime) {
		this.averageQualityTime = averageQualityTime;
	}
	public String getAveragePackingTime() {
		return averagePackingTime;
	}
	public void setAveragePackingTime(String averagePackingTime) {
		this.averagePackingTime = averagePackingTime;
	}
	public String getAverageCompletedTime() {
		return averageCompletedTime;
	}
	public void setAverageCompletedTime(String averageCompletedTime) {
		this.averageCompletedTime = averageCompletedTime;
	}
	
	

}
