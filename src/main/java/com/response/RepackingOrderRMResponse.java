package com.response;

import java.util.List;

import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.RawmaterialTable;
import com.entity.Status;

public class RepackingOrderRMResponse {

	private Long processorderPckgNumber;
	private Long processorderProdNumber;
	private Long productNumber;
	private String materialMemo;
	private String totalQuantity;
	private List<LocationRm> locationRm;
    private Status status;
	public RepackingOrderRMResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RepackingOrderRMResponse(Long processorderPckgNumber, Long processorderProdNumber, Long productNumber,
			String materialMemo, String totalQuantity, List<LocationRm> locationRm, Status status) {
		super();
		this.processorderPckgNumber = processorderPckgNumber;
		this.processorderProdNumber = processorderProdNumber;
		this.productNumber = productNumber;
		this.materialMemo = materialMemo;
		this.totalQuantity = totalQuantity;
		this.locationRm = locationRm;
		this.status = status;
	}
	public Long getProcessorderPckgNumber() {
		return processorderPckgNumber;
	}
	public void setProcessorderPckgNumber(Long processorderPckgNumber) {
		this.processorderPckgNumber = processorderPckgNumber;
	}
	public Long getProcessorderProdNumber() {
		return processorderProdNumber;
	}
	public void setProcessorderProdNumber(Long processorderProdNumber) {
		this.processorderProdNumber = processorderProdNumber;
	}
	public Long getProductNumber() {
		return productNumber;
	}
	public void setProductNumber(Long productNumber) {
		this.productNumber = productNumber;
	}
	public String getMaterialMemo() {
		return materialMemo;
	}
	public void setMaterialMemo(String materialMemo) {
		this.materialMemo = materialMemo;
	}
	public String getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public List<LocationRm> getLocationRm() {
		return locationRm;
	}
	public void setLocationRm(List<LocationRm> locationRm) {
		this.locationRm = locationRm;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
    
}
