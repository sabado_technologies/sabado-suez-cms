package com.response;

public class RepackingReportResponse {
	private Long processOrderPackageNumber;
	private String sixMlNumber;
	private String batchDate;
	private String productNumber;
	private String productName;
	private String totalQuantity;
	private int statusId;
	private String statusDescription;
	private String packetSize;
	private String totalPacketCounts;
	
	public RepackingReportResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RepackingReportResponse(Long processOrderPackageNumber, String sixMlNumber, String batchDate,
			String productNumber, String productName, String totalQuantity, int statusId, String statusDescription,
			String packetSize, String totalPacketCounts) {
		super();
		this.processOrderPackageNumber = processOrderPackageNumber;
		this.sixMlNumber = sixMlNumber;
		this.batchDate = batchDate;
		this.productNumber = productNumber;
		this.productName = productName;
		this.totalQuantity = totalQuantity;
		this.statusId = statusId;
		this.statusDescription = statusDescription;
		this.packetSize = packetSize;
		this.totalPacketCounts = totalPacketCounts;
	}

	public Long getProcessOrderPackageNumber() {
		return processOrderPackageNumber;
	}

	public void setProcessOrderPackageNumber(Long processOrderPackageNumber) {
		this.processOrderPackageNumber = processOrderPackageNumber;
	}

	public String getSixMlNumber() {
		return sixMlNumber;
	}

	public void setSixMlNumber(String sixMlNumber) {
		this.sixMlNumber = sixMlNumber;
	}

	public String getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getPacketSize() {
		return packetSize;
	}

	public void setPacketSize(String packetSize) {
		this.packetSize = packetSize;
	}

	public String getTotalPacketCounts() {
		return totalPacketCounts;
	}

	public void setTotalPacketCounts(String totalPacketCounts) {
		this.totalPacketCounts = totalPacketCounts;
	}
	
	
	
	

}
