package com.response;

public class MonthlyResponse {
	
	private int year;
	private String jan;
	private String feb;
	private String mar;
	private String apr;
	private String may;
	private String june;
	private String july;
	private String aug;
	private String sep;
	private String oct;
	private String nov;
	private String decm;
	public MonthlyResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MonthlyResponse(int year, String jan, String feb, String mar, String apr, String may, String june,
			String july, String aug, String sep, String oct, String nov, String decm) {
		super();
		this.year = year;
		this.jan = jan;
		this.feb = feb;
		this.mar = mar;
		this.apr = apr;
		this.may = may;
		this.june = june;
		this.july = july;
		this.aug = aug;
		this.sep = sep;
		this.oct = oct;
		this.nov = nov;
		this.decm = decm;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getJan() {
		return jan;
	}
	public void setJan(String jan) {
		this.jan = jan;
	}
	public String getFeb() {
		return feb;
	}
	public void setFeb(String feb) {
		this.feb = feb;
	}
	public String getMar() {
		return mar;
	}
	public void setMar(String mar) {
		this.mar = mar;
	}
	public String getApr() {
		return apr;
	}
	public void setApr(String apr) {
		this.apr = apr;
	}
	public String getMay() {
		return may;
	}
	public void setMay(String may) {
		this.may = may;
	}
	public String getJune() {
		return june;
	}
	public void setJune(String june) {
		this.june = june;
	}
	public String getJuly() {
		return july;
	}
	public void setJuly(String july) {
		this.july = july;
	}
	public String getAug() {
		return aug;
	}
	public void setAug(String aug) {
		this.aug = aug;
	}
	public String getSep() {
		return sep;
	}
	public void setSep(String sep) {
		this.sep = sep;
	}
	public String getOct() {
		return oct;
	}
	public void setOct(String oct) {
		this.oct = oct;
	}
	public String getNov() {
		return nov;
	}
	public void setNov(String nov) {
		this.nov = nov;
	}
	public String getDecm() {
		return decm;
	}
	public void setDecm(String decm) {
		this.decm = decm;
	}
	
	
		

}
