package com.response;

import java.util.List;

public class QuarterBarchartResponse {
private String quarterNumber;
private String actualQuarterProduction;
private String plannedQuarterProduction;
private  List<WeekwiseData> weekwisedata;
public QuarterBarchartResponse() {
	super();
	// TODO Auto-generated constructor stub
}

public QuarterBarchartResponse(String quarterNumber, String actualQuarterProduction, String plannedQuarterProduction,
		List<WeekwiseData> weekwisedata) {
	super();
	this.quarterNumber = quarterNumber;
	this.actualQuarterProduction = actualQuarterProduction;
	this.plannedQuarterProduction = plannedQuarterProduction;
	this.weekwisedata = weekwisedata;
}

public String getQuarterNumber() {
	return quarterNumber;
}
public void setQuarterNumber(String quarterNumber) {
	this.quarterNumber = quarterNumber;
}
public String getActualQuarterProduction() {
	return actualQuarterProduction;
}
public void setActualQuarterProduction(String actualQuarterProduction) {
	this.actualQuarterProduction = actualQuarterProduction;
}
public String getPlannedQuarterProduction() {
	return plannedQuarterProduction;
}
public void setPlannedQuarterProduction(String plannedQuarterProduction) {
	this.plannedQuarterProduction = plannedQuarterProduction;
}

public List<WeekwiseData> getWeekwisedata() {
	return weekwisedata;
}

public void setWeekwisedata(List<WeekwiseData> weekwisedata) {
	this.weekwisedata = weekwisedata;
}


}
