package com.response;

import java.util.List;

public class LocationRm {
	
	private String location;
	private String batchNumber;
	private String quantityTaken;
	public LocationRm() {

	}
	public LocationRm(String location, String batchNumber, String quantityTaken) {
		super();
		this.location = location;
		this.batchNumber = batchNumber;
		this.quantityTaken = quantityTaken;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getQuantityTaken() {
		return quantityTaken;
	}
	public void setQuantityTaken(String quantityTaken) {
		this.quantityTaken = quantityTaken;
	}
	
	
	

}
