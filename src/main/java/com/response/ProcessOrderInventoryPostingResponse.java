package com.response;

import java.util.List;

import com.entity.ProcessOrderRm;
import com.entity.ProductTable;
import com.entity.RawmaterialTable;
import com.entity.Status;

public class ProcessOrderInventoryPostingResponse {
	
	private long processorderProdNumber;
	private ProductTable productTable;
	private Status status;
	private String batchNumber;
	private String batchDate;
	private String totalQuantity;
	private String volume;
	private String wghtPckg;
	private Integer isAssociated;
	private String reactor;
	private int sift;
	private String startTime;
	private String completedDate;
	private String completedTime;
	private String comment;
	private String remarks;
	private int isApproved;
	private List<LocationRm> locationRm;
	private RawmaterialTable rawmaterialTable;
	private ProcessOrderRm processOrderRm;
	private String pdfDate;
	private String extraRm;
	private String producedQuantity;
	public ProcessOrderInventoryPostingResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProcessOrderInventoryPostingResponse(long processorderProdNumber, ProductTable productTable, Status status,
			String batchNumber, String batchDate, String totalQuantity, String volume, String wghtPckg,
			Integer isAssociated, String reactor, int sift, String startTime, String completedDate,
			String completedTime, String comment, String remarks, int isApproved, List<LocationRm> locationRm,
			RawmaterialTable rawmaterialTable ,ProcessOrderRm processOrderRm, String pdfDate, String extraRm, String producedQuantity) {
		super();
		this.processorderProdNumber = processorderProdNumber;
		this.productTable = productTable;
		this.status = status;
		this.batchNumber = batchNumber;
		this.batchDate = batchDate;
		this.totalQuantity = totalQuantity;
		this.volume = volume;
		this.wghtPckg = wghtPckg;
		this.isAssociated = isAssociated;
		this.reactor = reactor;
		this.sift = sift;
		this.startTime = startTime;
		this.completedDate = completedDate;
		this.completedTime = completedTime;
		this.comment = comment;
		this.remarks = remarks;
		this.isApproved = isApproved;
		this.locationRm = locationRm;
		this.rawmaterialTable = rawmaterialTable;
		this.processOrderRm = processOrderRm;
		this.extraRm = extraRm;
		this.pdfDate = pdfDate;
		this.producedQuantity = producedQuantity;
	}

	public long getProcessorderProdNumber() {
		return processorderProdNumber;
	}

	public void setProcessorderProdNumber(long processorderProdNumber) {
		this.processorderProdNumber = processorderProdNumber;
	}

	public ProductTable getProductTable() {
		return productTable;
	}

	public void setProductTable(ProductTable productTable) {
		this.productTable = productTable;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	public String getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getWghtPckg() {
		return wghtPckg;
	}

	public void setWghtPckg(String wghtPckg) {
		this.wghtPckg = wghtPckg;
	}

	public Integer getIsAssociated() {
		return isAssociated;
	}

	public void setIsAssociated(Integer isAssociated) {
		this.isAssociated = isAssociated;
	}

	public String getReactor() {
		return reactor;
	}

	public void setReactor(String reactor) {
		this.reactor = reactor;
	}

	public int getSift() {
		return sift;
	}

	public void setSift(int sift) {
		this.sift = sift;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(String completedDate) {
		this.completedDate = completedDate;
	}

	public String getCompletedTime() {
		return completedTime;
	}

	public void setCompletedTime(String completedTime) {
		this.completedTime = completedTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(int isApproved) {
		this.isApproved = isApproved;
	}

	public List<LocationRm> getLocationRm() {
		return locationRm;
	}

	public void setLocationRm(List<LocationRm> locationRm) {
		this.locationRm = locationRm;
	}

	public RawmaterialTable getRawmaterialTable() {
		return rawmaterialTable;
	}

	public void setRawmaterialTable(RawmaterialTable rawmaterialTable) {
		this.rawmaterialTable = rawmaterialTable;
	}

	public ProcessOrderRm getProcessOrderRm() {
		return processOrderRm;
	}

	public void setProcessOrderRm(ProcessOrderRm processOrderRm) {
		this.processOrderRm = processOrderRm;
	}

	public String getPdfDate() {
		return pdfDate;
	}

	public void setPdfDate(String pdfDate) {
		this.pdfDate = pdfDate;
	}

	public String getExtraRm() {
		return extraRm;
	}

	public void setExtraRm(String extraRm) {
		this.extraRm = extraRm;
	}

	public String getProducedQuantity() {
		return producedQuantity;
	}

	public void setProducedQuantity(String producedQuantity) {
		this.producedQuantity = producedQuantity;
	}
	
	
	
	

}
