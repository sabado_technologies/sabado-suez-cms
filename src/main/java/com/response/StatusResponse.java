package com.response;

public class StatusResponse {
	
	private String rmIssuedTime;
	private String rmIssuedDate;
	private String rmIssuedBy;
	private String rmChargedTime;
	private String rmChargedDate;
	private String rmChargedBy;
	private String qualityCheckTime;
	private String qualityCheckDate;
	private String qualityCheckedBy;
	private String packedTime;
	private String packedDate;
	private String packedBy;
	private String rmPostTime;
	private String rmPostDate;
	private String rmPostedBy;
	private String qulityInspectionTime;
	private String qualityInspectionDate;
	private String qualityInspectedBy;
	private String inventoryTime;
	private String inventoryDate;
	private String inventoryBy;
	private String inventoryPostTime;
	private String inventoryPostDate;
	private String inventoryPostedBy;
	private Long processorderProdNumber;
	private String materialMemo;
	private String sso;
	private String userName;
	private String uploadedTime;
	private String uploadedDate;
	private String uploadedBy;
	
	public String getUploadedTime() {
		return uploadedTime;
	}
	public void setUploadedTime(String uploadedTime) {
		this.uploadedTime = uploadedTime;
	}
	public String getUploadedDate() {
		return uploadedDate;
	}
	public void setUploadedDate(String uploadedDate) {
		this.uploadedDate = uploadedDate;
	}
	public String getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	private int statusId;
	private String batchDate;
	private int isSelected;
	
	
	public String getRmIssuedTime() {
		return rmIssuedTime;
	}
	public void setRmIssuedTime(String rmIssuedTime) {
		this.rmIssuedTime = rmIssuedTime;
	}
	public String getRmIssuedDate() {
		return rmIssuedDate;
	}
	public void setRmIssuedDate(String rmIssuedDate) {
		this.rmIssuedDate = rmIssuedDate;
	}
	public String getRmChargedTime() {
		return rmChargedTime;
	}
	public void setRmChargedTime(String rmChargedTime) {
		this.rmChargedTime = rmChargedTime;
	}
	public String getRmChargedDate() {
		return rmChargedDate;
	}
	public void setRmChargedDate(String rmChargedDate) {
		this.rmChargedDate = rmChargedDate;
	}
	public String getQualityCheckTime() {
		return qualityCheckTime;
	}
	public void setQualityCheckTime(String qualityCheckTime) {
		this.qualityCheckTime = qualityCheckTime;
	}
	public String getQualityCheckDate() {
		return qualityCheckDate;
	}
	public void setQualityCheckDate(String qualityCheckDate) {
		this.qualityCheckDate = qualityCheckDate;
	}
	public String getPackedTime() {
		return packedTime;
	}
	public void setPackedTime(String packedTime) {
		this.packedTime = packedTime;
	}
	public String getPackedDate() {
		return packedDate;
	}
	public void setPackedDate(String packedDate) {
		this.packedDate = packedDate;
	}
	public String getQulityInspectionTime() {
		return qulityInspectionTime;
	}
	public void setQulityInspectionTime(String qulityInspectionTime) {
		this.qulityInspectionTime = qulityInspectionTime;
	}
	public String getQualityInspectionDate() {
		return qualityInspectionDate;
	}
	public void setQualityInspectionDate(String qualityInspectionDate) {
		this.qualityInspectionDate = qualityInspectionDate;
	}
	public String getInventoryTime() {
		return inventoryTime;
	}
	public void setInventoryTime(String inventoryTime) {
		this.inventoryTime = inventoryTime;
	}
	public String getInventoryDate() {
		return inventoryDate;
	}
	public void setInventoryDate(String inventoryDate) {
		this.inventoryDate = inventoryDate;
	}
	public Long getProcessorderProdNumber() {
		return processorderProdNumber;
	}
	public void setProcessorderProdNumber(Long processorderProdNumber) {
		this.processorderProdNumber = processorderProdNumber;
	}
	public String getSso() {
		return sso;
	}
	public void setSso(String sso) {
		this.sso = sso;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRmIssuedBy() {
		return rmIssuedBy;
	}
	public void setRmIssuedBy(String rmIssuedBy) {
		this.rmIssuedBy = rmIssuedBy;
	}
	public String getRmChargedBy() {
		return rmChargedBy;
	}
	public void setRmChargedBy(String rmChargedBy) {
		this.rmChargedBy = rmChargedBy;
	}
	public String getQualityCheckedBy() {
		return qualityCheckedBy;
	}
	public void setQualityCheckedBy(String qualityCheckedBy) {
		this.qualityCheckedBy = qualityCheckedBy;
	}
	public String getPackedBy() {
		return packedBy;
	}
	public void setPackedBy(String packedBy) {
		this.packedBy = packedBy;
	}
	public String getRmPostTime() {
		return rmPostTime;
	}
	public void setRmPostTime(String rmPostTime) {
		this.rmPostTime = rmPostTime;
	}
	public String getRmPostDate() {
		return rmPostDate;
	}
	public void setRmPostDate(String rmPostDate) {
		this.rmPostDate = rmPostDate;
	}
	public String getRmPostedBy() {
		return rmPostedBy;
	}
	public void setRmPostedBy(String rmPostedBy) {
		this.rmPostedBy = rmPostedBy;
	}
	public String getQualityInspectedBy() {
		return qualityInspectedBy;
	}
	public void setQualityInspectedBy(String qualityInspectedBy) {
		this.qualityInspectedBy = qualityInspectedBy;
	}
	public String getInventoryBy() {
		return inventoryBy;
	}
	public void setInventoryBy(String inventoryBy) {
		this.inventoryBy = inventoryBy;
	}
	public String getInventoryPostTime() {
		return inventoryPostTime;
	}
	public void setInventoryPostTime(String inventoryPostTime) {
		this.inventoryPostTime = inventoryPostTime;
	}
	public String getInventoryPostDate() {
		return inventoryPostDate;
	}
	public void setInventoryPostDate(String inventoryPostDate) {
		this.inventoryPostDate = inventoryPostDate;
	}
	public String getInventoryPostedBy() {
		return inventoryPostedBy;
	}
	public void setInventoryPostedBy(String inventoryPostedBy) {
		this.inventoryPostedBy = inventoryPostedBy;
	}
	public String getMaterialMemo() {
		return materialMemo;
	}
	public void setMaterialMemo(String materialMemo) {
		this.materialMemo = materialMemo;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public String getBatchDate() {
		return batchDate;
	}
	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}
	public int getIsSelected() {
		return isSelected;
	}
	public void setIsSelected(int isSelected) {
		this.isSelected = isSelected;
	}
	
	
	
}
