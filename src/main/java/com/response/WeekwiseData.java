package com.response;

public class WeekwiseData {
private String quarterNumber;
private String weekNumber;
private String weeklyPlannedProduction;
private String weeklyActualProduction;
public WeekwiseData() {
	super();
	// TODO Auto-generated constructor stub
}
public WeekwiseData(String quarterNumber, String weekNumber, String weeklyPlannedProduction,
		String weeklyActualProduction) {
	super();
	this.quarterNumber = quarterNumber;
	this.weekNumber = weekNumber;
	this.weeklyPlannedProduction = weeklyPlannedProduction;
	this.weeklyActualProduction = weeklyActualProduction;
}
public String getQuarterNumber() {
	return quarterNumber;
}
public void setQuarterNumber(String quarterNumber) {
	this.quarterNumber = quarterNumber;
}
public String getWeekNumber() {
	return weekNumber;
}
public void setWeekNumber(String weekNumber) {
	this.weekNumber = weekNumber;
}
public String getWeeklyPlannedProduction() {
	return weeklyPlannedProduction;
}
public void setWeeklyPlannedProduction(String weeklyPlannedProduction) {
	this.weeklyPlannedProduction = weeklyPlannedProduction;
}
public String getWeeklyActualProduction() {
	return weeklyActualProduction;
}
public void setWeeklyActualProduction(String weeklyActualProduction) {
	this.weeklyActualProduction = weeklyActualProduction;
}

}
