package com.response;

import java.util.List;

public class CurrentWeekPlannedResponse {

	private String Monday;
	private String Tuesday;
	private String Wednesday;
	private String Thursday;
	private String Friday;
	private String Saturday;
	private String Sunday;
	private String plannedProduction;
	private List<String>plannedDates;
	public CurrentWeekPlannedResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	


	public CurrentWeekPlannedResponse(String monday, String tuesday, String wednesday, String thursday, String friday,
			String saturday, String sunday, String plannedProduction, List<String> plannedDates) {
		super();
		Monday = monday;
		Tuesday = tuesday;
		Wednesday = wednesday;
		Thursday = thursday;
		Friday = friday;
		Saturday = saturday;
		Sunday = sunday;
		this.plannedProduction = plannedProduction;
		this.plannedDates = plannedDates;
	}





	public String getMonday() {
		return Monday;
	}
	public void setMonday(String monday) {
		Monday = monday;
	}
	public String getTuesday() {
		return Tuesday;
	}
	public void setTuesday(String tuesday) {
		Tuesday = tuesday;
	}
	public String getWednesday() {
		return Wednesday;
	}
	public void setWednesday(String wednesday) {
		Wednesday = wednesday;
	}
	public String getThursday() {
		return Thursday;
	}
	public void setThursday(String thursday) {
		Thursday = thursday;
	}
	public String getFriday() {
		return Friday;
	}
	public void setFriday(String friday) {
		Friday = friday;
	}
	public String getSaturday() {
		return Saturday;
	}
	public void setSaturday(String saturday) {
		Saturday = saturday;
	}
	public String getSunday() {
		return Sunday;
	}
	public void setSunday(String sunday) {
		Sunday = sunday;
	}

	public String getPlannedProduction() {
		return plannedProduction;
	}

	public void setPlannedProduction(String plannedProduction) {
		this.plannedProduction = plannedProduction;
	}





	public List<String> getPlannedDates() {
		return plannedDates;
	}


   public void setPlannedDates(List<String> plannedDates) {
		this.plannedDates = plannedDates;
	}


	
	
}
