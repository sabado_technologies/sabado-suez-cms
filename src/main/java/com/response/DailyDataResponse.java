package com.response;

public class DailyDataResponse {
	private String dayNumber;
	private String dailyQuantity;
	private String completedDate;
	public DailyDataResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DailyDataResponse(String dayNumber, String dailyQuantity, String completedDate) {
		super();
		this.dayNumber = dayNumber;
		this.dailyQuantity = dailyQuantity;
		this.completedDate = completedDate;
	}

	public String getDayNumber() {
		return dayNumber;
	}
	public void setDayNumber(String dayNumber) {
		this.dayNumber = dayNumber;
	}
	public String getDailyQuantity() {
		return dailyQuantity;
	}
	public void setDailyQuantity(String dailyQuantity) {
		this.dailyQuantity = dailyQuantity;
	}

	public String getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(String completedDate) {
		this.completedDate = completedDate;
	}
	

}
