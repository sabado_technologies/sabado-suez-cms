package com.response;

public class PlannedReportResponse {
	private int quarterNumber;
	private String plannedQuantityPerQuarter1;
	private String plannedQuantityPerQuarter2;
	private String plannedQuantityPerQuarter3;
	private String plannedQuantityPerQuarter4;
	private String plannedQuantityPerWeekQ1;
	private String plannedQuantityPerWeekQ2;
	private String plannedQuantityPerWeekQ3;
	private String plannedQuantityPerWeekQ4;
	public PlannedReportResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PlannedReportResponse(int quarterNumber, String plannedQuantityPerQuarter1,
			String plannedQuantityPerQuarter2, String plannedQuantityPerQuarter3, String plannedQuantityPerQuarter4,
			String plannedQuantityPerWeekQ1, String plannedQuantityPerWeekQ2, String plannedQuantityPerWeekQ3,
			String plannedQuantityPerWeekQ4) {
		super();
		this.quarterNumber = quarterNumber;
		this.plannedQuantityPerQuarter1 = plannedQuantityPerQuarter1;
		this.plannedQuantityPerQuarter2 = plannedQuantityPerQuarter2;
		this.plannedQuantityPerQuarter3 = plannedQuantityPerQuarter3;
		this.plannedQuantityPerQuarter4 = plannedQuantityPerQuarter4;
		this.plannedQuantityPerWeekQ1 = plannedQuantityPerWeekQ1;
		this.plannedQuantityPerWeekQ2 = plannedQuantityPerWeekQ2;
		this.plannedQuantityPerWeekQ3 = plannedQuantityPerWeekQ3;
		this.plannedQuantityPerWeekQ4 = plannedQuantityPerWeekQ4;
	}
	public int getQuarterNumber() {
		return quarterNumber;
	}
	public void setQuarterNumber(int quarterNumber) {
		this.quarterNumber = quarterNumber;
	}
	public String getPlannedQuantityPerQuarter1() {
		return plannedQuantityPerQuarter1;
	}
	public void setPlannedQuantityPerQuarter1(String plannedQuantityPerQuarter1) {
		this.plannedQuantityPerQuarter1 = plannedQuantityPerQuarter1;
	}
	public String getPlannedQuantityPerQuarter2() {
		return plannedQuantityPerQuarter2;
	}
	public void setPlannedQuantityPerQuarter2(String plannedQuantityPerQuarter2) {
		this.plannedQuantityPerQuarter2 = plannedQuantityPerQuarter2;
	}
	public String getPlannedQuantityPerQuarter3() {
		return plannedQuantityPerQuarter3;
	}
	public void setPlannedQuantityPerQuarter3(String plannedQuantityPerQuarter3) {
		this.plannedQuantityPerQuarter3 = plannedQuantityPerQuarter3;
	}
	public String getPlannedQuantityPerQuarter4() {
		return plannedQuantityPerQuarter4;
	}
	public void setPlannedQuantityPerQuarter4(String plannedQuantityPerQuarter4) {
		this.plannedQuantityPerQuarter4 = plannedQuantityPerQuarter4;
	}
	public String getPlannedQuantityPerWeekQ1() {
		return plannedQuantityPerWeekQ1;
	}
	public void setPlannedQuantityPerWeekQ1(String plannedQuantityPerWeekQ1) {
		this.plannedQuantityPerWeekQ1 = plannedQuantityPerWeekQ1;
	}
	public String getPlannedQuantityPerWeekQ2() {
		return plannedQuantityPerWeekQ2;
	}
	public void setPlannedQuantityPerWeekQ2(String plannedQuantityPerWeekQ2) {
		this.plannedQuantityPerWeekQ2 = plannedQuantityPerWeekQ2;
	}
	public String getPlannedQuantityPerWeekQ3() {
		return plannedQuantityPerWeekQ3;
	}
	public void setPlannedQuantityPerWeekQ3(String plannedQuantityPerWeekQ3) {
		this.plannedQuantityPerWeekQ3 = plannedQuantityPerWeekQ3;
	}
	public String getPlannedQuantityPerWeekQ4() {
		return plannedQuantityPerWeekQ4;
	}
	public void setPlannedQuantityPerWeekQ4(String plannedQuantityPerWeekQ4) {
		this.plannedQuantityPerWeekQ4 = plannedQuantityPerWeekQ4;
	}
	
}