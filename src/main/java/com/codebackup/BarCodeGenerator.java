package com.codebackup;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

public class BarCodeGenerator {

	public static void main(String[] args) {
		System.out.println("Main Method Started");
		createBarCode39("1002135899");
		System.out.println("Main Method Ended");
	}

	public static void createBarCode39(String fileName) {
		fileName = fileName.replaceAll("\\s+", "");
		fileName = fileName.replaceAll(",", "");

		try {
			Code39Bean bean39 = new Code39Bean();
			final int dpi = 300;

			// Configure the barcode generator
			bean39.setModuleWidth(UnitConv.in2mm(3f / dpi));
			bean39.setWideFactor(2);
			bean39.setMsgPosition(HumanReadablePlacement.HRP_NONE);
			bean39.setBarHeight(6);
			bean39.setDisplayStartStop(true);
			bean39.doQuietZone(false);

			// Open output file
			File outputFile = new File("E:\\po\\temp\\output\\barcodes\\" + fileName + ".png");

			FileOutputStream out = new FileOutputStream(outputFile);

			// Set up the canvas provider for monochrome PNG output
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi,
					BufferedImage.TYPE_BYTE_BINARY, false, 0);

			// Generate the barcode
			bean39.generateBarcode(canvas, fileName);

			// Signal end of generation
			canvas.finish();

			System.out.println("Bar Code is generated successfully…");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
