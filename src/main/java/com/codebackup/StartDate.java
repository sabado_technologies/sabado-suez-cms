package com.codebackup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StartDate {
	public static void main(String[] args) {
		System.out.println("main method started");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);

		// get start of this week in milliseconds
		cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
		System.out.println("Start of this week:       " + cal.getTime());
		
		String date = convertDate(cal.getTime());
		System.out.println(date);

		// start of the next week
		cal.add(Calendar.WEEK_OF_YEAR, 1);
		System.out.println("Start of the next week:   " + cal.getTime());
		
	}
	
	public static String convertDate(Date date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = df.format(date);
		return dateStr;
	}
}
