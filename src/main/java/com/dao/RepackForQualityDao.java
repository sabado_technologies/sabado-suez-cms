package com.dao;

import java.util.List;

import com.entity.ProcessorderPckg;

public interface RepackForQualityDao {
	
	public List<ProcessorderPckg> getRepackDetails();
	
    public long acceptQuality(long processorderProdNumber, String ssoid);
	
	public long rejectQuality(long processorderProdNumber, String ssoid, String comment);
	
	public long scrapQuality(long processorderProdNumber, String ssoid, String comment);

}
