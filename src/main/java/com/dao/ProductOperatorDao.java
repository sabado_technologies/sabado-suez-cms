package com.dao;

import java.util.List;

import com.entity.Alerts;
import com.entity.MultiRoles;
import com.entity.ProcessOrderRm;
import com.entity.ProcessTracker;
import com.entity.ProcessorderAssociatedPo;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.QualityCheckResult;
import com.entity.QualityMaster;
import com.entity.Reactor;
import com.entity.Remark;
import com.entity.Role;
import com.entity.User;
import com.response.ManagementReportResponse;
import com.response.ProcessOrderRMResponse;
import com.response.QualityCheckResponce;
import com.response.RepackingOrderRMResponse;

public interface ProductOperatorDao {

	public List<ProcessorderProd> displayProcessOrders();

	public List<ProcessOrderRMResponse> processOrderRecipeList(long a);

	public int updateStatus(Long processOrder, int status, String sso);

	public int updateProcessProgress(ProcessTracker processStatus);

	public List<User> listofPo();

	public List<ProcessorderPckg> processOrderForPacking(long processOrderProduct);

	public List<ProcessorderPckg> PackingOrderInfo(long a);

	public List<ProcessorderProd> AllProcessOrderList();

	public List<ManagementReportResponse> getWeeklyProcessOrders();

	public int SaveprocessAssociatedwithpo(List<ProcessorderAssociatedPo> list);

	public int updateStatusForPacking(Long processOrder, int status, String sso, String quantity, int pails, String remarks);

	public int updateStatusForRM(Long processOrder, int rMNumber, int status);

	public List<QualityCheckResponce> getQualityData(long productNo);

	public int SaveQualityInfo(List<QualityCheckResponce> list);

	public int updateProcessOrderProd(Long processOrder, String reactor, int status);

	public List<ProcessorderProd> displayProcessOrdersforInventory();

	public List<ProcessorderProd> dailyProcessOrder();

	public int saveAlert(Alerts alert);

	public List<Reactor> getReactorDetails();

	public List<MultiRoles> getRoleList(String sSO);

	public int InsertRemark(Remark remark);

	public int updateTracker(Long processOrder, int status);

	public List<ProcessorderPckg> reapckingOrderDiaplay();

	public int SaveprocessAssociatedwithpoForRepack(List<ProcessorderAssociatedPo> list);

	public int insertQualityResult(QualityCheckResult qualityCheckResult);

	public List<ProcessorderProd> processOrderDetailsForQuality(long a);

	public List<ProcessorderPckg> displayProcessOrdersforInventoryForRepacking();

	public List<RepackingOrderRMResponse> RepackingOrderRecipeList(long a);

	public int RepackingOrderStatusChange(long repackingOrder, int status, String sso);

	public List<ProcessorderPckg> RepackingOrderRecipee(long a);
	
	public int insertPackingStartTime(Long processOrder);
}
