package com.dao;

import java.util.List;

import com.entity.MultiRoles;
import com.entity.Role;
import com.entity.User;
import com.entity.UserDetailsForAlerts;
import com.request.AddUserRequest;

public interface UserDao {
	
	
	public String saveUser(AddUserRequest user , List<String> roles);
	
	public String editUser(User user,List<String> roles);
	
	public User findUser(String ssoid);
	
	public List<User> getAllUsers();
	
	public List<Role> getRoles();
	
	public List<User> getActiveUsers();
	
	public List<User> getDeActiveUsers();
	
	public int activateUser(final List<User> list);
	
	public User getUserBySSOId(String ssoId);
	
	public int deActivateUser(List<User> list);
	
	public int changePassword(AddUserRequest user);
	
	public int resetPassword(String sso,String password);
	
	public List<MultiRoles> getMultiRoles();
	
	public List<UserDetailsForAlerts> getAlertUsers();
	
	public int addUserForAlerts(UserDetailsForAlerts user);
	
	public int deleteAlertUser(int userId);
	
	public int updateAlertUser(UserDetailsForAlerts user);
	
	public int insertEmailAlert(String reactor,int temp,String process,long processOrder,String sso,String userName);
	
}
