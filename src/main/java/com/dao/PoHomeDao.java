package com.dao;

import java.util.List;

import com.entity.ProcessorderProd;

public interface PoHomeDao {
  
	public List<ProcessorderProd> getAllOrders();
	
public List<ProcessorderProd> getTodaysOrders();
	
	public List<ProcessorderProd> getPreviousOrders();
	
	public List<ProcessorderProd> getFutureOrders();
	
	public List<ProcessorderProd> getTodaysCompletedOrders();
	
}
