package com.dao;

import java.util.List;

import com.entity.User;
import com.request.UserRequest;

public interface LoginDao {

	List<User> getRole(UserRequest user) throws Exception;

}
