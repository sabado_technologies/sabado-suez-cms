package com.dao;

import java.util.Date;
import java.util.List;

import com.response.EmailAlertResponce;

public interface EmailAlertReportDao {

	List<EmailAlertResponce> getEmailAlert();

	List<EmailAlertResponce> getEmailAlertWithRange(Date fromDate, Date toDate);
	
	List<EmailAlertResponce> getPackingAlerts();
	
	List<EmailAlertResponce> getPackingAlertsByDate(Date fromDate, Date toDate);
	
	List<EmailAlertResponce> getTemperature(long processOrder);
	
	List<EmailAlertResponce> getRmTemperatureDetails(long processOrder);

}
