package com.dao;

import java.util.List;

import com.request.PlannerRequest;

public interface PlannerDao {
	
	public int addPlan(PlannerRequest plan);
	
	public PlannerRequest getPlan(int year,int quarter);
	
	public PlannerRequest findPlan(int year,int quarter);
	
	public int updatePlan(PlannerRequest plan);
	
	public int addPlanForReport(List<String> dates,PlannerRequest plan);
	
	public int deletePlan(PlannerRequest plan);

}
