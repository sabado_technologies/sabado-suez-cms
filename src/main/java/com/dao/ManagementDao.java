package com.dao;

import java.util.List;

import com.entity.ProcessorderProd;
import com.entity.ProdutionPlanned;
import com.highchart.CurrentQuarterReportResponse;
import com.highchart.ProductForChart;
import com.highchart.YearlyBarchartResponse;
import com.response.AverageCycleTimeResponse;
import com.response.CurrentQuarterWeeklyResponse;
import com.response.CurrentWeekPlannedResponse;
import com.response.CycleTimeResponse;
import com.response.FullQuarterReportResponse;
import com.response.ManagementQuarterProductionPlan;
import com.response.ManagementReportResponse;
import com.response.MonthlyResponse;
import com.response.PackingReportResponse;
import com.response.QuarterBarchartResponse;
import com.response.QuarterBarchartResponse1;
import com.response.RepackingReportResponse;
import com.response.YearlyReportResponse;

public interface ManagementDao {

	public List<ProcessorderProd> getDailyProcessOrders();
	
//-------------------------------------------------------------------------------------------------
	//Current Week production Report

	public List<ManagementReportResponse> getWeeklyProcessOrders();
	
	public List<ManagementReportResponse> getWeeklyProductionValue();

//----------------------------------------------------------------------------------------------------
	
	//For current year product wise report

	public List<ManagementReportResponse> productWisereport();
	
	public List<ManagementReportResponse> getPacketSizeCounts();

	public List<ManagementReportResponse> dateWiseProductReport(Long productnumber);
	
	public List<ManagementReportResponse> getAll1MTReactors(Long productnumber);

	public List<ManagementReportResponse> getAll5MTReactors(Long productnumber);

//-------------------------------------------------------------------------------------------------------
	
	//For different year Product report by selecting year
	
	public List<ManagementReportResponse> productWisereport(int year);
	
	public List<ManagementReportResponse> getPacketSizeCountsForYear(int year);


	public List<ManagementReportResponse> dateWiseProductReportYearwise(Long productnumber, int year);
	
	public List<ManagementReportResponse> getAll1MTReactors(Long productnumber ,int year);

	public List<ManagementReportResponse> getAll5MTReactors(Long productnumber, int year);

//-----------------------------------------------------------------------------------------------
	
	//Current Year current Quarter production Report
	
	public CurrentQuarterReportResponse currentQuaterProductionReport();

	public List<ProductForChart> currentQuaterProductWiseReport();
	
	public ManagementQuarterProductionPlan currentQuaterProductionPlan();

//-------------------------------------------------------------------------------------------------------
	//Current Year Production Report

	public MonthlyResponse MontlyProductionProductWiseReport();
	
	public ManagementReportResponse getProductionValue(int year);
	
//-------------------------------------------------------------------------------------------------------
	
	//Multiple year production report(not using)
	
	public MonthlyResponse MontlyProductionProductWiseReport(int year);
	
	public ManagementReportResponse getMonthlyProductionvalue(int year);
	
//---------------------------------------------------------------------------------------------------------
	
	//FullQuarter production report for Current Year
	
	public List<FullQuarterReportResponse> fullQuaterProductionReport();
	
	public List<ProductForChart> quaterProductionProductWiseReport(int quarterNumber);

	public List<ManagementQuarterProductionPlan> fullQuaterProductionPlan();

	
	
//-------------------------------------------------------------------------------------------------------
	//cycle time for process Orders
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForWeek();

	public AverageCycleTimeResponse averageCycleTimeForProcessOrdersForWeek();

	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForMonth();

	public List<CycleTimeResponse> cycleTimeForProcessOrdersForMonth();

	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForQuarter();

	public List<CycleTimeResponse> cycleTimeForProcessOrdersForQuarter();

	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForYear();

	public List<CycleTimeResponse> cycleTimeForProcessOrdersForYear();
	
//-----------------------------------------------------------------------------------------------------
	//New Yearly Production Report

	public YearlyReportResponse YearlyProductionReport();

	public List<ManagementReportResponse> getPlannedProduction();

	public YearlyBarchartResponse YearlyProductionBarchart();

	public YearlyBarchartResponse MultipleYearProductionBarchart(int year);

	
//-----------------------------------------------------------------------------------------------------
	//current quarter  weekly Barchart 

	/*public CurrentQuarterWeeklyResponse CurrentQuarterWeeklyResponse();

	public ManagementReportResponse getPlannedReport();

*/
//---------------------------------------------------------------------------------------------------------
	//Daywise production Report
	
	public List<ManagementReportResponse> dayWiseProductionReport();

	public List<ManagementReportResponse> dayWiseProductionReport(int quarter);
	
//---------------------------------------------------------------------------------------------------------
	//Repacking Report

	public List<RepackingReportResponse> rePackingReport();
	
	public List<RepackingReportResponse> rePackingReport(int year);

	
//---------------------------------------------------------------------------------------------------------
	public int askingRate();

	/*public CurrentWeekPlannedResponse getCurrentWeekDates();

	*/

	public List<PackingReportResponse> cycleTimeForModal(Long processorderProdNumber);

	public QuarterBarchartResponse1 barchartReports(String year);


}
