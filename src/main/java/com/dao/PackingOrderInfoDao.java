package com.dao;

import java.util.List;

import com.entity.ProcessorderPckg;

public interface PackingOrderInfoDao {
	
	public List<ProcessorderPckg> getPackingInfo(long processOrderNumber);

}
