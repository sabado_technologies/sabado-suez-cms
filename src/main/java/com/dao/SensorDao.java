package com.dao;

import java.io.IOException;

public interface SensorDao {

	String TemparatureSensor(int tempsensorno) throws IOException;

	String weightSensor(int weightsensorno) throws IOException;

	//int weightSensorTest(int weightsensorno, int olddata);

}
