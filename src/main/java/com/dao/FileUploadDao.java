package com.dao;

import com.entity.ProcessorderProd;
import com.request.PdfValues;
import com.request.PdfValuesForPack;

public interface FileUploadDao {

	public boolean inertIntoProductionOrders(PdfValues pdfValues);
	
	public boolean inertIntoPackagingOrders(PdfValuesForPack pdfValues);
	
	public boolean inertRePackagingOrders(PdfValuesForPack pdfValues);
	
	public ProcessorderProd getProcessOrder(String po);
	
	public ProcessorderProd getPackingOrder(String po);

}
