package com.dao;

import java.util.List;

import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;

public interface QualityResultsDao {
	
	public List<ProcessorderProd> getReworkDetails();
	
	public List<ProcessorderPckg> getPackingDetails(long processOrder);
	
	public List<ProcessorderProd> getScrapDetails();
	
	public long approve(long processOrder, String remarks, String ssoid);

}
