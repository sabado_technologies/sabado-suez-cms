package com.entity;

public class EmailAlerts {
	
	private int emailAlertsId;
	private Long processOrder;
	private String sso;
	private String userName;
	private String process;
	private String reactor;
	private String temperature;
	private String time;
	public EmailAlerts() {
		super();
	}
	public EmailAlerts(int emailAlertsId, Long processOrder, String sso, String userName, String process,
			String reactor, String temperature, String time) {
		super();
		this.emailAlertsId = emailAlertsId;
		this.processOrder = processOrder;
		this.sso = sso;
		this.userName = userName;
		this.process = process;
		this.reactor = reactor;
		this.temperature = temperature;
		this.time = time;
	}
	public int getEmailAlertsId() {
		return emailAlertsId;
	}
	public void setEmailAlertsId(int emailAlertsId) {
		this.emailAlertsId = emailAlertsId;
	}
	public Long getProcessOrder() {
		return processOrder;
	}
	public void setProcessOrder(Long processOrder) {
		this.processOrder = processOrder;
	}
	public String getSso() {
		return sso;
	}
	public void setSso(String sso) {
		this.sso = sso;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getReactor() {
		return reactor;
	}
	public void setReactor(String reactor) {
		this.reactor = reactor;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	
	

}

 