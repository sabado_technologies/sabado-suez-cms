package com.entity;

public class Remark {
	
	private int id;
	private String processOrder;
	private String  sso;
	private String  remark;
	private String  statusId;
	private String  remarkedBy;
	
	public Remark() {
		super();
	}

	public Remark(int id, String processOrder, String sso, String remark, String statusId, String remarkedBy) {
		super();
		this.id = id;
		this.processOrder = processOrder;
		this.sso = sso;
		this.remark = remark;
		this.statusId = statusId;
		this.remarkedBy = remarkedBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProcessOrder() {
		return processOrder;
	}

	public void setProcessOrder(String processOrder) {
		this.processOrder = processOrder;
	}

	public String getSso() {
		return sso;
	}

	public void setSso(String sso) {
		this.sso = sso;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getRemarkedBy() {
		return remarkedBy;
	}

	public void setRemarkedBy(String remarkedBy) {
		this.remarkedBy = remarkedBy;
	}
	
	
	
}
