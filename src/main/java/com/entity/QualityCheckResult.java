package com.entity;

public class QualityCheckResult {
	private int qualityCheckResultId;
	private Long processOrder;
	private String productNumber;
	private String materialMemo;
	private String batchNo;
	private String sso;
	private String remark;
	
	
	public QualityCheckResult() {
		super();
	}


	public QualityCheckResult(int qualityCheckResultId, Long processOrder, String productNumber, String materialMemo,
			String batchNo, String sso, String remark) {
		super();
		this.qualityCheckResultId = qualityCheckResultId;
		this.processOrder = processOrder;
		this.productNumber = productNumber;
		this.materialMemo = materialMemo;
		this.batchNo = batchNo;
		this.sso = sso;
		this.remark = remark;
	}


	public int getQualityCheckResultId() {
		return qualityCheckResultId;
	}


	public void setQualityCheckResultId(int qualityCheckResultId) {
		this.qualityCheckResultId = qualityCheckResultId;
	}


	public Long getProcessOrder() {
		return processOrder;
	}


	public void setProcessOrder(Long processOrder) {
		this.processOrder = processOrder;
	}


	public String getProductNumber() {
		return productNumber;
	}


	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}


	public String getMaterialMemo() {
		return materialMemo;
	}


	public void setMaterialMemo(String materialMemo) {
		this.materialMemo = materialMemo;
	}


	public String getBatchNo() {
		return batchNo;
	}


	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}


	public String getSso() {
		return sso;
	}


	public void setSso(String sso) {
		this.sso = sso;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	
	
}
