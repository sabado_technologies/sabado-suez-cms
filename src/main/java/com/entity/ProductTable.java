package com.entity;
// Generated Dec 5, 2017 4:26:21 PM by Hibernate Tools 5.2.6.Final

import java.util.HashSet;
import java.util.Set;

/**
 * ProductTable generated by hbm2java
 */
public class ProductTable implements java.io.Serializable {

	private long productNumber;
	private String productDescription;
	private String productName;
	private Set processOrderRmPckgs = new HashSet(0);
	private Set processOrderRms = new HashSet(0);
	private Set processorderProds = new HashSet(0);

	public ProductTable() {
	}

	public ProductTable(long productNumber) {
		this.productNumber = productNumber;
	}

	public ProductTable(long productNumber, String productDescription, String productName, Set processOrderRmPckgs,
			Set processOrderRms, Set processorderProds) {
		this.productNumber = productNumber;
		this.productDescription = productDescription;
		this.productName = productName;
		this.processOrderRmPckgs = processOrderRmPckgs;
		this.processOrderRms = processOrderRms;
		this.processorderProds = processorderProds;
	}

	public long getProductNumber() {
		return this.productNumber;
	}

	public void setProductNumber(long productNumber) {
		this.productNumber = productNumber;
	}

	public String getProductDescription() {
		return this.productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Set getProcessOrderRmPckgs() {
		return this.processOrderRmPckgs;
	}

	public void setProcessOrderRmPckgs(Set processOrderRmPckgs) {
		this.processOrderRmPckgs = processOrderRmPckgs;
	}

	public Set getProcessOrderRms() {
		return this.processOrderRms;
	}

	public void setProcessOrderRms(Set processOrderRms) {
		this.processOrderRms = processOrderRms;
	}

	public Set getProcessorderProds() {
		return this.processorderProds;
	}

	public void setProcessorderProds(Set processorderProds) {
		this.processorderProds = processorderProds;
	}

}
