package com.entity;

public class MultiRoles {
	
	private int multiRolesId;
	private String sso;
	private int role;
	private String roleName;
	
	
	public MultiRoles() {
		super();
		// TODO Auto-generated constructor stub
	}


	public MultiRoles(int multiRolesId, String sso, int role, String roleName) {
		super();
		this.multiRolesId = multiRolesId;
		this.sso = sso;
		this.role = role;
		this.roleName = roleName;
	}


	public int getMultiRolesId() {
		return multiRolesId;
	}


	public void setMultiRolesId(int multiRolesId) {
		this.multiRolesId = multiRolesId;
	}


	public String getSso() {
		return sso;
	}


	public void setSso(String sso) {
		this.sso = sso;
	}


	public int getRole() {
		return role;
	}


	public void setRole(int role) {
		this.role = role;
	}


	public String getRoleName() {
		return roleName;
	}


	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	

	
}
