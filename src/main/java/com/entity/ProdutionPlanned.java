package com.entity;
// Generated 14 Dec, 2017 5:31:13 PM by Hibernate Tools 5.1.0.Alpha1

/**
 * ProdutionPlanned generated by hbm2java
 */
public class ProdutionPlanned implements java.io.Serializable {

	private int id;
	private String produtionYearly;
	private String productionWeekly;
	private String productionMonthly;
	private String year;
	private String productionQ1;
	private String productionQ2;
	private String productionQ3;
	private String productionQ4;

	public ProdutionPlanned() {
	}

	public ProdutionPlanned(int id) {
		this.id = id;
	}

	public ProdutionPlanned(int id, String produtionYearly, String productionWeekly, String productionMonthly,
			String year, String productionQ1, String productionQ2, String productionQ3, String productionQ4) {
		this.id = id;
		this.produtionYearly = produtionYearly;
		this.productionWeekly = productionWeekly;
		this.productionMonthly = productionMonthly;
		this.year = year;
		this.productionQ1 = productionQ1;
		this.productionQ2 = productionQ2;
		this.productionQ3 = productionQ3;
		this.productionQ4 = productionQ4;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProdutionYearly() {
		return this.produtionYearly;
	}

	public void setProdutionYearly(String produtionYearly) {
		this.produtionYearly = produtionYearly;
	}

	public String getProductionWeekly() {
		return this.productionWeekly;
	}

	public void setProductionWeekly(String productionWeekly) {
		this.productionWeekly = productionWeekly;
	}

	public String getProductionMonthly() {
		return this.productionMonthly;
	}

	public void setProductionMonthly(String productionMonthly) {
		this.productionMonthly = productionMonthly;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getProductionQ1() {
		return this.productionQ1;
	}

	public void setProductionQ1(String productionQ1) {
		this.productionQ1 = productionQ1;
	}

	public String getProductionQ2() {
		return this.productionQ2;
	}

	public void setProductionQ2(String productionQ2) {
		this.productionQ2 = productionQ2;
	}

	public String getProductionQ3() {
		return this.productionQ3;
	}

	public void setProductionQ3(String productionQ3) {
		this.productionQ3 = productionQ3;
	}

	public String getProductionQ4() {
		return this.productionQ4;
	}

	public void setProductionQ4(String productionQ4) {
		this.productionQ4 = productionQ4;
	}

}
