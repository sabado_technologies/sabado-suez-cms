package com.entity;

public class InventoryCheckDetails {
	
	private ProcessorderProd processorderProd;
	private Status status;
	private ProcessorderPckg processorderPckg;
	private ContainerTable containerTable;
	private User user;
	
	private String materialDescription ;
	private String containerQuantity;
	private String batchDate;
	private String inventoryCheckDate;
	private String location_stored;
	
	
	
	public InventoryCheckDetails() {
		super();
		// TODO Auto-generated constructor stub
	}


	public InventoryCheckDetails(ProcessorderProd processorderProd, Status status, ProcessorderPckg processorderPckg,
			ContainerTable containerTable, User user, String materialDescription, String containerQuantity,
			String batchDate, String inventoryCheckDate, String location_stored) {
		
		super();
		this.processorderProd = processorderProd;
		this.status = status;
		this.processorderPckg = processorderPckg;
		this.containerTable = containerTable;
		this.user = user;
		this.materialDescription = materialDescription;
		this.containerQuantity = containerQuantity;
		this.batchDate = batchDate;
		this.inventoryCheckDate = inventoryCheckDate;
		this.location_stored = location_stored;
	}
	
	
	public ProcessorderProd getProcessorderProd() {
		return processorderProd;
	}
	public void setProcessorderProd(ProcessorderProd processorderProd) {
		this.processorderProd = processorderProd;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public ProcessorderPckg getProcessorderPckg() {
		return processorderPckg;
	}
	public void setProcessorderPckg(ProcessorderPckg processorderPckg) {
		this.processorderPckg = processorderPckg;
	}
	public ContainerTable getContainerTable() {
		return containerTable;
	}
	public void setContainerTable(ContainerTable containerTable) {
		this.containerTable = containerTable;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getMaterialDescription() {
		return materialDescription;
	}
	public void setMaterialDescription(String materialDescription) {
		this.materialDescription = materialDescription;
	}
	public String getContainerQuantity() {
		return containerQuantity;
	}
	public void setContainerQuantity(String containerQuantity) {
		this.containerQuantity = containerQuantity;
	}
	public String getBatchDate() {
		return batchDate;
	}
	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}
	public String getInventoryCheckDate() {
		return inventoryCheckDate;
	}
	public void setInventoryCheckDate(String inventoryCheckDate) {
		this.inventoryCheckDate = inventoryCheckDate;
	}
	public String getLocation_stored() {
		return location_stored;
	}
	public void setLocation_stored(String location_stored) {
		this.location_stored = location_stored;
	}
	

	
}
