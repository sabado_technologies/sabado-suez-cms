package com.entity;

public class Alerts implements java.io.Serializable {
private int alertsId;
private String processOrderNumber;
private User user;
private  String generatedTime;
private String statusName;
private Status status;
public Alerts(int alertsId, String processOrderNumber, User user, String generatedTime, String statusName,
		Status status) {
	super();
	this.alertsId = alertsId;
	this.processOrderNumber = processOrderNumber;
	this.user = user;
	this.generatedTime = generatedTime;
	this.statusName = statusName;
	this.status = status;
}
public Alerts() {
	super();
	// TODO Auto-generated constructor stub
}
public int getAlertsId() {
	return alertsId;
}
public void setAlertsId(int alertsId) {
	this.alertsId = alertsId;
}
public String getProcessOrderNumber() {
	return processOrderNumber;
}
public void setProcessOrderNumber(String processOrderNumber) {
	this.processOrderNumber = processOrderNumber;
}
public User getUser() {
	return user;
}
public void setUser(User user) {
	this.user = user;
}
public String getGeneratedTime() {
	return generatedTime;
}
public void setGeneratedTime(String generatedTime) {
	this.generatedTime = generatedTime;
}
public String getstatusName() {
	return statusName;
}
public void setstatusName(String statusName) {
	this.statusName = statusName;
}
public Status getStatus() {
	return status;
}
public void setStatus(Status status) {
	this.status = status;
} 

}
