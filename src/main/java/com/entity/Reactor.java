package com.entity;

public class Reactor  {
	
	private Integer reactorID;
	private String reactorName;
	private String minThreashold;
	private String maxThreashold;
	
	public Reactor() {
		
	}

	public Reactor(Integer reactorID, String reactorName, String minThreashold, String maxThreashold) {
		this.reactorID = reactorID;
		this.reactorName = reactorName;
		this.minThreashold = minThreashold;
		this.maxThreashold = maxThreashold;
	}

	public Integer getReactorID() {
		return reactorID;
	}

	public void setReactorID(Integer reactorID) {
		this.reactorID = reactorID;
	}

	public String getReactorName() {
		return reactorName;
	}

	public void setReactorName(String reactorName) {
		this.reactorName = reactorName;
	}

	public String getMinThreashold() {
		return minThreashold;
	}

	public void setMinThreashold(String minThreashold) {
		this.minThreashold = minThreashold;
	}

	public String getMaxThreashold() {
		return maxThreashold;
	}

	public void setMaxThreashold(String maxThreashold) {
		this.maxThreashold = maxThreashold;
	}
	
	
}
