package com.entity;

public class Quality {
	
	private Integer qualityId;
	private String qualityName;
	
	
	
	public Quality(Integer qualityId, String qualityName) {
		//super();
		this.qualityId = qualityId;
		this.qualityName = qualityName;
	}

	public Quality() {
		// TODO Auto-generated constructor stub
	}

	public Integer getAssociationId() {
		return qualityId;
	}

	public void setAssociationId(Integer qualityId) {
		this.qualityId = qualityId;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	
	

}
