/**
 * 
 */
package com.service;

import java.util.List;

import com.entity.InventoryCheckDetails;
import com.entity.ProcessOrderRm;
import com.entity.ProcessTracker;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.LocationRawmaterialDetails;
import com.response.NotificationTabsForWarehouse;
import com.response.PackingOrderInventoryPostingResponse;
import com.response.ProcessOrderInventoryPostingResponse;
import com.response.RepackingProcessOrderCount;

/**
 * @author ullas
 *
 */
public interface WarehouseManagerService {

	List<ProcessorderProd> getAllProcessOrders();

	List<ProcessOrderRm> getProcessOrderDetails(Long processorderProdNumber);

	/*
	 * int setSsoIdForProcessOrder(int processOrderId, String ssoId); int
	 * setStatusForTracker(int processOrderId, String ssoId);
	 */
	int setProcessOrderTrackerStatus(Long processorderProdNumber, String ssoId);

	List<ProcessorderProd> getProcessOrderCount();

	List<ProcessorderProd> getProcessOrderCompletedCount();

	int updateProcessOrderStatus(Long processorderProdNumber, String processTrackerId, String ssoId);

	int setTrackerStatusForRm(ProcessTracker processTrackerTable);

	List<ProcessorderProd> getAllTodayProcessOrders();

	List<ProcessorderProd> getProcessOrdersForQuality();

	List<ProcessorderPckg> associatedPackingInfo(long processorderProdNumber);

	int setTrackerForWarehouseQuality(String ssoId, String locationPlaced, String containerQuantity, ProcessorderPckg packingdetail);

	int setTrackerforInventory(Long processorderProdNumber, String ssoId);

	int setRmLocationDetails(Long processorderProdNumber, String rmId, LocationRawmaterialDetails locationRawmaterialDetails);
	
	int setRmLocationDetailsForRepack(Long processorderPckgNumber, String ssoId, LocationRawmaterialDetails locationRawmaterialDetails);

	List<ProcessorderProd> getProcessOrdersForQuality(Long processorderProdNumber);

	List<ProcessorderProd> getAllPendingProcessOrders();

	int setProcessOrderTrackerStatusForRejection(Long processorderProdNumber, String ssoId, String submitMessage);

	List<ProcessorderProd> getAllContainerDetailsForProcessOrders();

	List<InventoryCheckDetails> getContainerDetailsForProcessOrder(Long processorderProdNumber);

	List<ProcessorderPckg> getPackingOrderForInventory();

	List<ProcessorderPckg> getRePackingOrderForRM();

	List<ProcessorderPckg> getRePackingOrderForRMDetails(Long processorderPckgNumber);

	List<ProcessorderPckg> getAllContainerDetailsForRepackProcessOrders();

	List<InventoryCheckDetails> getContainerDetailsForRepackProcessOrder(Long processorderPckgNumber);

	List<ProcessorderPckg> getPackingOrderForInventory1(Long processorderPckgNumber);

	List<ProcessorderPckg> associatedPackingInfoForPosting(long processorderProdNumber);

	List<PackingOrderInventoryPostingResponse> getPackingOrderForInventoryPosting(Long processorderPckgNumber);

	List<ProcessorderProd> getProcessOrdersForPosting();

	List<ProcessOrderInventoryPostingResponse> getProcessOrdersForPostingCheck(Long processorderProdNumber);

	List<ProcessorderPckg> getPackingOrderForInventoryPostingList();

	int setProcessOrderStatusForPosting(Long processorderProdNumber, String ssoId);

	int setProcessOrderStatusForRepackPosting(Long processorderPckgNumber, String ssoId);

	RepackingProcessOrderCount getRepackingCount();

	NotificationTabsForWarehouse getNotificationForWarehouseCount();

	
}
