package com.service;

import java.util.List;

import com.entity.User;
import com.request.UserRequest;

public interface LoginService {

	List<User> getRole(UserRequest user) throws Exception;

}
