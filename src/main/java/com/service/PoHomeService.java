package com.service;

import java.util.List;

import com.entity.ProcessorderProd;

public interface PoHomeService
{
	public List<ProcessorderProd> getAllOrders();
	
	  public List<ProcessorderProd> getTodaysOrders();
		
	    
		public List<ProcessorderProd> getPreviousOrders();
		
		public List<ProcessorderProd> getFutureOrders();
		
		public List<ProcessorderProd> getTodaysCompletedOrders();
		
}
