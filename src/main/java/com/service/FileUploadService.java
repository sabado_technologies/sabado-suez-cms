package com.service;

import com.entity.ProcessorderProd;
import com.request.PdfValues;
import com.request.PdfValuesForPack;

public interface FileUploadService {
	
	public boolean inertIntoProductionOrders(PdfValues pdfValues);
	
	public boolean inertIntoPackingOrders(PdfValuesForPack pdfValues);
	
	public boolean inertRePackagingOrders(PdfValuesForPack pdfValues);
	
	public ProcessorderProd getProcessOrder(String po);
	
	public ProcessorderProd getPackingOrder(String po);
	
}
