package com.service;

import java.util.List;

import com.entity.Alerts;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.NotificationCounterResponse;

public interface AlertsService {

	public List<Alerts> alertsForAdmin();

	public int statusChangeForAdmin(String processOrderNumber, int statusId);

	public List<Alerts> alertsForWarehouse();

	public int deleteProductionOrder(Long processorderProdNumber, String remarks, String ssoId);

	public List<ProcessorderProd> processOrdersForDeletion();

	public int deletePackagingOrder(Long processorderPckgNumber, String remarks, String ssoId);

	public List<ProcessorderPckg> packagingOrdersForDeletion();

	public int smsAlertForWHInventory(String processOrderNumber, String ssoId);

	public int smsAlertForCharging(String processOrderNumber, String ssoId);

	public NotificationCounterResponse notificationCountForPo();

}
