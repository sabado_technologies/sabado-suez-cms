package com.service;

import java.util.List;

import com.entity.ProcessorderProd;
import com.entity.Reactor;
import com.response.StatusResponse;

public interface WarehousePendingService {
	
	public List<ProcessorderProd> getPendingOrders();
	
	public List<Reactor> getAllReactors();
	
	public String UpdateReactor(Reactor reactor);
	
	public List<ProcessorderProd> getAllOrders();
	
    public List<ProcessorderProd> getTodaysOrders();
    
	public List<ProcessorderProd> getPreviousOrders();
	
	public List<ProcessorderProd> getFutureOrders();
	
	public List<ProcessorderProd> getTodaysCompletedOrders();
	
	public StatusResponse getStatus(long processorderProdNumber);
	
	public StatusResponse getChargedStatus(long processorderProdNumber);
	
	public StatusResponse getPackedStatus(long processorderProdNumber);
	
    public StatusResponse getQualityCheckStatus(long processorderProdNumber);
	
	public StatusResponse getQualityInspectionStatus(long processorderProdNumber);
	
	public StatusResponse getInventoryCheckStatus(long processorderProdNumber);
	
	public StatusResponse getRmInventoryStatus(long processorderProdNumber);

}
