package com.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.request.PlannerRequest;

public interface PlannerService {
	
	public int addPlan(PlannerRequest plan);
	
	public PlannerRequest getPlan(int year,int quarter);
	
	public PlannerRequest findPlan(int year,int quarter);
	
	public int updatePlan(PlannerRequest plan);
	
	public List<Date> convertDate(List<String> dates) throws ParseException;
	
	public String addDates(List<Date> dates);
	
	public List<String> changeFormat(List<Date> dates);
	
	public int addPlanForReport(List<String> dates,PlannerRequest plan);
	
	public int deletePlan(PlannerRequest plan);

}
