package com.service;

import java.util.List;

import com.entity.Associate;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;

public interface AssociateService {
	
	public List<ProcessorderProd> getProductionProcessOrders();
	
	public List<ProcessorderPckg> getPackageProcessOrders();
	
	public int associateProcess(final List<Associate> list);
	
	public List<ProcessorderPckg> getDeletedProcessOrders();

}
