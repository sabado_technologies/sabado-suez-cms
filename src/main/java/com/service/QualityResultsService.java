package com.service;

import java.util.List;

import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;

public interface QualityResultsService {
	
	public List<ProcessorderProd> getReworkDetails();
	
	public List<ProcessorderPckg> getPackingDetails(long processOrder);
	
	public List<ProcessorderProd> getScrapDetails();
	
	public long approve(long processOrder, String remarks, String ssoid);

}
