package com.service;

import java.io.IOException;

public interface SensorService {

	String TemparatureSensor(int tempsensorno) throws IOException;

	String weightSensor(int weightsensorno) throws IOException;

	//int weightSensorTest(int weightsensorno, int olddata);

}
