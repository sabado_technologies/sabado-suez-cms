package com.service;

import java.util.List;

import com.entity.ProcessorderProd;
import com.entity.ProdutionPlanned;
import com.highchart.CurrentQuarterReportResponse;		
import com.highchart.ProductForChart;
import com.highchart.YearlyBarchartResponse;
import com.response.AverageCycleTimeResponse;
import com.response.CurrentQuarterWeeklyResponse;
import com.response.CurrentWeekPlannedResponse;
import com.response.CycleTimeResponse;
import com.response.FullQuarterReportResponse;
import com.response.ManagementQuarterProductionPlan;
import com.response.ManagementReportResponse;
import com.response.MonthlyResponse;
import com.response.PackingReportResponse;
import com.response.QuarterBarchartResponse;
import com.response.QuarterBarchartResponse1;
import com.response.RepackingReportResponse;
import com.response.YearlyReportResponse;

public interface ManagementService {
	
	public List<ProcessorderProd> getDailyProcessOrders();
	
//----------------------------------------------------------------------------------------------
	//Current week production Report
	
	public List<ManagementReportResponse> getWeeklyProcessOrders();
	
	public  List<ManagementReportResponse>  getWeeklyProductionValue();
	
//-----------------------------------------------------------------------------------------------
	//For current year product wise report
	
	public List<ManagementReportResponse> productWisereport();
	
	public List<ManagementReportResponse> getPacketSizeCounts();
	
	public List<ManagementReportResponse> dateWiseProductReport(Long productnumber);
	
	public List<ManagementReportResponse> getAll1MTReactors(Long productnumber);

	public List<ManagementReportResponse> getAll5MTReactors(Long productnumber);
	
//----------------------------------------------------------------------------------------
	
	//for different year product reports by Selecting Year

		public List<ManagementReportResponse> productWisereport(int year);
		
		public List<ManagementReportResponse> getPacketSizeCountsForYear(int year);

		
		public List<ManagementReportResponse> dateWiseProductReportYearwise(Long productnumber ,int year);
		
		public List<ManagementReportResponse> getAll1MTReactors(Long productnumber,int year);

		public List<ManagementReportResponse> getAll5MTReactors(Long productnumber,int year);
//---------------------------------------------------------------------------------------------
		//Current Year Current quarter Production  Report
		
	public CurrentQuarterReportResponse currentQuarterReportResponse();
	
	public List<ProductForChart> currentQuaterProductWiseReport();
	
	public ManagementQuarterProductionPlan currentQuaterProductionPlan();

//-----------------------------------------------------------------------------------------------
	//Current year Production Report
	
	public MonthlyResponse MontlyProductionProductWiseReport();
	
	public ManagementReportResponse getMonthlyProductionvalue();
	
//-------------------------------------------------------------------------------------------------
	
	//multiple year production report (not using)
	

	public MonthlyResponse MontlyProductionProductWiseReport(int year);
	
	public ManagementReportResponse getMonthlyProductionvalue(int year);
	
	
//----------------------------------------------------------------------------------------------------
	
//full Quarter production Report for Current Year
	
	public List<FullQuarterReportResponse> fullQuaterProductionReport();

	public List<ProductForChart> quaterProductionProductWiseReport(int quarterNumber);
	
	public List<ManagementQuarterProductionPlan> fullQuaterProductionPlan();

	
//--------------------------------------------------------------------------------------------------------	
	//cycle time for processOrders
	
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForWeek();

	public AverageCycleTimeResponse averageCycleTimeForProcessOrdersForWeek();

	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForMonth();

	public List<CycleTimeResponse> cycleTimeForProcessOrdersForMonth();

	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForQuarter();

	public List<CycleTimeResponse> cycleTimeForProcessOrdersForQuarter();

	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForYear();

	public List<CycleTimeResponse> cycleTimeForProcessOrdersForYear();
//-----------------------------------------------------------------------------------------------------
	//newYearlyProduction Report

	public YearlyReportResponse YearlyProductionReport();

	public List<ManagementReportResponse> getPlannedProduction();

	public YearlyBarchartResponse YearlyProductionBarchart();

	public YearlyBarchartResponse MultipleYearProductionBarchart(int year);

//-----------------------------------------------------------------------------------------------------------
	//current Quarter weeklyBarchart

	/*public CurrentQuarterWeeklyResponse currentQuarterWeeklyReport();

	public ManagementReportResponse getPlannedReport();
*/
//---------------------------------------------------------------------------------------------------------------
	//DaywiseProduction Report
	
	public List<ManagementReportResponse> dayWiseProductionReport();

	public List<ManagementReportResponse> dayWiseProductionReport(int quarter);

//-------------------------------------------------------------------------------------------------------------
	//Repacking Report
     
	public List<RepackingReportResponse> rePackingReport();
	
	public List<RepackingReportResponse> rePackingReport(int year);
	
//---------------------------------------------------------------------------------------------------------------
	//Aking Rate query

	public int askingRate();

	/*public CurrentWeekPlannedResponse getCurrentWeekDates();*/

	
	public List<PackingReportResponse> cycleTimeForModal(Long processorderProdNumber);
	
	public QuarterBarchartResponse1 barchartReports(String year);

	
}
