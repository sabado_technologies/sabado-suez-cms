package com.service;

import java.util.List;

import com.entity.ProcessorderPckg;

public interface PackingOrderInfoService {
	
	public List<ProcessorderPckg> getPackingInfo(long processOrderNumber);

}
