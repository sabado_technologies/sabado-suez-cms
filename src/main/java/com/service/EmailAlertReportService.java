package com.service;

import java.util.Date;
import java.util.List;

import com.entity.EmailAlerts;
import com.entity.ProcessorderProd;
import com.response.EmailAlertResponce;

public interface EmailAlertReportService {

	List<EmailAlertResponce> getEmailAlert();

	List<EmailAlertResponce> getEmailAlertWithRange(Date fromDate, Date toDate);
	
	List<EmailAlertResponce> getPackingAlerts();
	
	List<EmailAlertResponce> getPackingAlertsByDate(Date fromDate, Date toDate);
	
	List<EmailAlertResponce> getTemperature(long processOrder);
	
	List<EmailAlertResponce> getRmTemperatureDetails(long processOrder);

}
