package com.service;

import java.util.List;

import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.Quality;
import com.entity.QualityCheck;

public interface QualityService {
	
	public List<ProcessorderPckg> getPackageProcessOrders(long processNumber);
	
	public List<Quality> getQualities();
	
	public List<ProcessorderProd> getProductDetails();
	
	public long updatePackage(ProcessorderPckg processorderPckg);
	
	public List<QualityCheck> getQualityCheck(long processorderProdNumber);
	
	public long acceptQuality(long processorderProdNumber, String ssoid);
	
	//public long rejectQuality(long processorderProdNumber, String ssoid, String comment);
	
	public long scrapQuality(long processorderProdNumber, String ssoid, String comment);
	
	public List<ProcessorderPckg> getRepackDetails();

	public Long rejectQuality(long processorderProdNumber, String ssoid, String comment, String batchNo, String prodNo,
			String memo);

	//public List<ProcessorderPckg> getPackageProcessOrders(long processNumber);
	public int getRepackCount();

}
