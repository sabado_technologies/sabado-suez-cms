package com.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.exception.ValidationExceptions;
import com.request.AddUserRequest;
import com.util.MessagesConstant;

@Service
public class UserValidation {
	
	public void getUser(AddUserRequest user) throws ValidationExceptions {

		if (StringUtils.isBlank(user.getUsername())) {

			throw new ValidationExceptions(MessagesConstant.USERNAME_INVALID);
		} else if (StringUtils.isBlank(user.getSsoid())) {

			throw new ValidationExceptions(MessagesConstant.SSOID_INVALID);
		} else if (StringUtils.isBlank(user.getPassword())) {

			throw new ValidationExceptions(MessagesConstant.PASSWORD_INVALID);
		}else if (StringUtils.isBlank(user.getEmail())) {

			throw new ValidationExceptions(MessagesConstant.EMAIL_INVALID);
		}/* else if (StringUtils.isBlank(user.getAddress())) {

			throw new ValidationExceptions(MessagesConstant.ADDRESS_INVALID);
		}*/ else if (StringUtils.isBlank(user.getPhonenumber())) {

			throw new ValidationExceptions(MessagesConstant.PHONENUMBER_INVALID);
		} /*else if (StringUtils.isBlank(user.getPincode())) {

			throw new ValidationExceptions(MessagesConstant.PINCODE_INVALID);
		}*/ else if (StringUtils.isBlank(user.getCreatedBy())) {

			throw new ValidationExceptions(MessagesConstant.CREATED_BY);
		} 

	}

}
