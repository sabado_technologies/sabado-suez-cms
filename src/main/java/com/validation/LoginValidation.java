package com.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.exception.ValidationExceptions;
import com.request.UserRequest;
import com.util.MessagesConstant;

@Service
public class LoginValidation {
	public void getRole(UserRequest user) throws ValidationExceptions {

		if (StringUtils.isBlank(user.getSsoid())) {

			throw new ValidationExceptions(MessagesConstant.SSOID_INVALID);
		} else if (StringUtils.isBlank(user.getPassword())) {

			throw new ValidationExceptions(MessagesConstant.PASSWORD_INVALID);
		}

	}

}