package com.util;

import java.util.Arrays;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

public class GmailFetch {

    public static void main( String[] args ) throws Exception {
    	
    	Properties properties = new Properties();
        properties.put("mail.store.protocol", "pop3");
        properties.put("mail.imap.host", "imap.gmail.com");
        properties.put("mail.imap.port", "993");
        properties.put("mail.imap.starttls.enable", "*");

      Session session = Session.getDefaultInstance(properties);
      Store store = session.getStore("imaps");
      store.connect("imap.googlemail.com", 993, "jayanth.entransys@gmail.com", "radhamma");
      Folder inbox = store.getFolder( "INBOX" );
      inbox.open( Folder.READ_WRITE );

      // Fetch unseen messages from inbox folder
      Message[] messages = inbox.search(
          new FlagTerm(new Flags(Flags.Flag.SEEN), false));

      // Sort messages from recent to oldest
      Arrays.sort( messages, ( m1, m2 ) -> {
        try {
          return m2.getSentDate().compareTo( m1.getSentDate() );
        } catch ( MessagingException e ) {
          throw new RuntimeException( e );
        }
      } );

      for ( Message message : messages ) {
        System.out.println( 
            "sendDate: " + message.getSentDate()
            + " subject:" + message.getSubject() );
        System.out.println("Content : " + message.getContent());
        System.out.println("Desc : " + message.getDescription());
//message.setFlag(Flags.Flag.SEEN, true);
      }
    }
  }
