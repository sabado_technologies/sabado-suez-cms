package com.util;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;  
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;  
import javax.mail.MessagingException;  
import javax.mail.NoSuchProviderException;  
import javax.mail.Session;  
import com.sun.mail.pop3.POP3Store;

public class ReceiveMail {
	
	public static void receiveEmail(String pop3Host, String storeType,  
			  String user, String password) {  
			  try {  
			   //1) get the session object  
			   Properties properties = new Properties();  
			   final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
			  // properties.put("mail.pop3.host", pop3Host);  
			   properties.setProperty("mail.imaps.socketFactory.class", SSL_FACTORY);
			   properties.setProperty("mail.imaps.socketFactory.fallback", "false");
			   properties.setProperty("mail.imaps.port", "993");
			   properties.setProperty("mail.imaps.socketFactory.port", "993");
			   properties.put("mail.imaps.host", "pop3Host");
			   Session emailSession = Session.getDefaultInstance(properties);  
			     
			   //2) create the POP3 store object and connect with the pop server  
			   POP3Store emailStore = (POP3Store) emailSession.getStore(storeType);  
			   emailStore.connect(user, password);  
			  
			   //3) create the folder object and open it  
			   Folder emailFolder = emailStore.getFolder("INBOX");  
			   emailFolder.open(Folder.READ_ONLY);  
			  
			   //4) retrieve the messages from the folder in an array and print it  
			   Message[] messages = emailFolder.getMessages();  
			   for (int i = 0; i < messages.length; i++) {  
			    Message message = messages[i];  
			    System.out.println("---------------------------------");  
			    System.out.println("Email Number " + (i + 1));  
			    System.out.println("Subject: " + message.getSubject());  
			    System.out.println("From: " + message.getFrom()[0]);  
			    System.out.println("Text: " + message.getContent().toString());
			    
			    @SuppressWarnings("rawtypes")
				Enumeration allHeaders = messages[i].getAllHeaders();
    	        while (allHeaders.hasMoreElements()) {
    	            Header header = (Header) allHeaders.nextElement();
    	            String headerName = header.getName();
    	            String headerVal = header.getValue();
    	            
    	            System.out.println("Header Name : " + headerName);
    	            System.out.println("Header Value : " + headerVal);

    	           // headerMap.put(headerName, headerVal);

    	        }
			   }  
			  
			   //5) close the store and folder objects  
			   emailFolder.close(false);  
			   emailStore.close();  
			  
			  } catch (NoSuchProviderException e) {e.printStackTrace();}   
			  catch (MessagingException e) {e.printStackTrace();}  
			  catch (IOException e) {e.printStackTrace();}  
			 }  
			  
			 public static void main(String[] args) {  
			  
			  String host = "outlook.office365.com";//change accordingly  
			  String mailStoreType = "pop3";  
			  String username= "test@entransys.com";  
			  String password= "Spartan203";//change accordingly  
			  
			  receiveEmail(host, mailStoreType, username, password);  
			  
			 } 

}
