package com.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMailJavaAPI {
	
	public static void main(String[] args) throws Exception{

        String to="jayanthkumar.mc@entransys.com";
        String from="test@entransys.com";
        String subject = "Test mail for Deviation";
		String body = "Hello team";
		String toEmail = "jayanthkumar.mc@entransys.com";
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        String msgBody = "Testing";

        try {
            Message msg = new MimeMessage(session);
           /* msg.setFrom(new InternetAddress(from,  "NoReply-JD"));
            msg.addRecipient(Message.RecipientType.TO,
                             new InternetAddress(to, false));
            msg.setSubject(&quot;Welcome To Java Mail API&quot;);
            msg.setText(msgBody);
            Transport.send(msg);
            System.out.println(&quot;Email sent successfully...&quot;);*/
            msg.setFrom(new InternetAddress("test@entransys.com", "NoReply-JD"));

  	      msg.setReplyTo(InternetAddress.parse("test@entransys.com", false));

  	      msg.setSubject(subject);

  	      msg.setText(body);

  	      msg.setSentDate(new Date());

  	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
  	      System.out.println("Message is ready");
      	  Transport.send(msg);  

        } catch (AddressException e) {
        	e.printStackTrace();
            throw new RuntimeException(e);
        } catch (MessagingException e) {
        	e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
