package com.util;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class MessagesConstant {
	private static final Logger log = LoggerFactory.getLogger(MessagesConstant.class);
	private static final Properties prop = new Properties();

	static {
		try {
			Resource res = new ClassPathResource("/restApi.properties");
			prop.load(res.getInputStream());
		} catch (Exception ex) {
			log.error("MessagesConstant: restApi.properties file not found");
		}
	}

	private static String getProperty(String key) {
		String value = null;
		try {
			value = prop.getProperty(key).trim();
		} catch (Exception ex) {
			log.error("Missing property entry for " + key, ex);
			throw new RuntimeException("Missing property entry for " + key);
		}
		return value;
	}
	
	
	public final static String SSOID_INVALID = getProperty("ssoid.empty");
	public final static String PASSWORD_INVALID = getProperty("password.empty");
	public final static String USERNAME_INVALID = getProperty("username.empty");
	public final static String EMAIL_INVALID = getProperty("email.empty");
	//public final static String ADDRESS_INVALID = getProperty("addresss.empty");
	public final static String PHONENUMBER_INVALID = getProperty("phonenumber.empty");
	//public final static String PINCODE_INVALID = getProperty("pincode.empty");
	public final static String STATUS_INVALID = getProperty("statusId.empty");
	public final static String ROLE_INVALID = getProperty("roleId.empty");
	public final static String CREATED_BY = getProperty("createdBy.empty");

	/*	public final static String FOREX_ID_INVALID = getProperty("forexId.invalid");
	
*/

}