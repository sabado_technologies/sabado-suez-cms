package com.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSender {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String subject = "Test mail for Deviation";
		String body = "Hello team";
		String toEmail = "jayanthkumar.mc@entransys.com";
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		
		Properties props = System.getProperties();
        // Set manual Properties
        props.setProperty("mail.imaps.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.imaps.socketFactory.fallback", "false");
        props.setProperty("mail.imaps.port", "587");
        props.setProperty("mail.imaps.socketFactory.port", "587");
        props.put("mail.imaps.host", "smtp.office365.com");	
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.enable", "false");
        
        Session session = Session.getDefaultInstance(props);
		
		try
	    {
			
	        
	        
	       
	      MimeMessage msg = new MimeMessage(session);
	      //set message headers
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");

	      msg.setFrom(new InternetAddress("test@entransys.com", "NoReply-JD"));

	      msg.setReplyTo(InternetAddress.parse("test@entransys.com", false));

	      msg.setSubject(subject, "UTF-8");

	      msg.setText(body, "UTF-8");

	      msg.setSentDate(new Date());

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	      System.out.println("Message is ready");
    	  Transport.send(msg);  

	      System.out.println("EMail Sent Successfully!!");
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }

	}

}
