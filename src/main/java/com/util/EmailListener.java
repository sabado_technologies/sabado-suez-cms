package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.event.MessageCountAdapter;
import javax.mail.event.MessageCountEvent;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;

import org.jsoup.Jsoup;

import com.sun.mail.imap.IMAPFolder;

public class EmailListener {
	
	Folder inbox;
	private boolean textIsHtml = false;

	// Constructor of the calss.

	public EmailListener() throws IOException, MessagingException {
		System.out.println("Inside MailReader()...");
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties props = System.getProperties();
		// Set manual Properties
		props.setProperty("mail.imaps.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.imaps.socketFactory.fallback", "false");
		props.setProperty("mail.imaps.port", "993");
		props.setProperty("mail.imaps.socketFactory.port", "993");
		props.put("mail.imaps.host", "outlook.office365.com");
		// props.put("mail.debug", "true");
		Store store = null;

		try {
			/* Create the session and get the store for read the mail. */

			Session session = Session.getDefaultInstance(props, null);
			 store = session.getStore("imaps");

			store.connect("outlook.office365.com", 993, "test@entransys.com", "Spartan203");

			/* Mention the folder name which you want to read. */

			// dd52c308-1233-41dc-84e0-77f8faccf3a2

			inbox = store.getFolder("INBOX");
			// store.get

			/* Open the inbox using store. */

			// inbox.open(Folder.READ_ONLY);
			UIDFolder uf = (UIDFolder) inbox;
			inbox.open(Folder.READ_WRITE);

			Flags seen = new Flags(Flags.Flag.SEEN);// ++
			FlagTerm unseenFlagTerm = new FlagTerm(seen, false);// ++
			//Message messages[] = inbox.search(unseenFlagTerm);
			Message confirmationMAil = null;
			String emailMessage = "";
			
			int msgCount;
	        do {//go through all the unread emails in the inbox at least once
	            msgCount = inbox.getMessageCount();//set how many messages are in the inbox when the array is created
	            // Fetch unseen messages from inbox folder
	            javax.mail.Message[] messages = inbox.search(
	                    new FlagTerm(new Flags(Flags.Flag.SEEN), false));
	            for (javax.mail.Message msg : messages) {
	                //process emails here
	            	emailMessage = processMessage(msg);
	            	System.out.println("Email Message => " + emailMessage);
	            }
	            //if a new message came in while reading the messages start the loop over and get all unread messages    
	        } while (inbox.getMessageCount() != msgCount);
	        //add listener
	        inbox.addMessageCountListener(new MessageCountAdapter() {
	            @Override
	            public void messagesAdded(MessageCountEvent ev) {
	                javax.mail.Message[] messages = ev.getMessages();
	                for (javax.mail.Message msg : messages) {
	                    //process emails here
	                	try {
	                		String emailData = processMessage(msg);
	                		System.out.println("Email Data => " + emailData);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (MessagingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	                }
	            }
	        });
	        // wait for new messages
	        while (inbox.isOpen()) {                
	            //every 25 minutes poke the server with a inbox.getMessageCount() to keep the connection active/open
	            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	            final Runnable pokeInbox = () -> {
	                try {
	                    inbox.getMessageCount();
	                } catch (MessagingException ex) {
	                    //nothing doin'
	                }
	            };
	            scheduler.schedule(pokeInbox, 20, TimeUnit.MINUTES);
	            ((IMAPFolder) inbox).idle();
	        }
	    } catch (FolderClosedException e) {
	        e.printStackTrace();
	        System.out.println("error connection was dropped");
	        if (store != null) {
	            store.close();
	        }
	       // loadUnreadEmails();//restarts listening for email if the connection times out
	    } finally {
	        if (store != null) {
	            store.close();
	        }
	    }

	}
	
	private String processMessage(Message message) throws IOException, MessagingException {
		
		String result = " " ;
		String[] splitedResult = null;
		InputStream input = null;
		Path pathForOutput = null;
		
		if(message instanceof MimeMessage)
        {
            MimeMessage m = (MimeMessage)message;
            Object contentObject = m.getContent();
            if(contentObject instanceof Multipart)
            {
                BodyPart clearTextPart = null;
                BodyPart htmlTextPart = null;
                Multipart content = (Multipart)contentObject;
                int count = content.getCount();
                for(int j=0; j<count; j++)
                {
                    BodyPart part =  content.getBodyPart(j);
                    if(Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                    	System.out.println("Reading attachement");
                        input = part.getInputStream();
                    	pathForOutput = Paths.get("D:\\Email-attachements\\" + part.getFileName());
                    	Files.deleteIfExists(pathForOutput);
                		Files.copy(input, pathForOutput);
                    	
                    } else {
                    	System.out.println("Inside Else");
                    	System.out.println("Content-Type in Else => " + part.getContentType());
                    	
                    	
                    	if(part.getContentType().contains("text/plain"))
                        {
                        	System.out.println("Inside text/plain");
                            clearTextPart = part;
                            break;
                        }
                        else if(part.getContentType().contains("text/html"))
                        {
                        	System.out.println("Inside text/html");
                            htmlTextPart = part;
                        }
                    	
                    }
                    
                    
                }

                if(clearTextPart!=null)
                {
                	System.out.println("Inside clearTextPart");
                    result = (String) clearTextPart.getContent();
                }
                else if (htmlTextPart!=null)
                {
                	System.out.println("Inside htmlTextPart");
                   // String html = (String) htmlTextPart.getContent();
                    String html = getText(htmlTextPart);
                   // String reply = EmailReplyParser.parseReply(html);
                   // String reply = EmailReplyParser.parseReply(html);
                   // System.out.println("Reply = > " + reply);
                    result = Jsoup.parse(html).text();
                 //   result = Jsoup.parse(reply).text();
                    splitedResult = result.split("From:");
                    result = splitedResult[0];
                }

            }
             else if (contentObject instanceof String) // a simple text message
            {
            	 System.out.println("Inside simple text message");
                result = (String) contentObject;
                splitedResult = result.split("From:");
                result = splitedResult[0];
            }
        }
		
		/*String contentType = message.getContentType();
        String messageContent = "";

        // store attachment file name, separated by comma
        String attachFiles = "";

        if (contentType.contains("multipart")) {
            // content may contain attachments
            Multipart multiPart = (Multipart) message.getContent();
            int numberOfParts = multiPart.getCount();
            for (int partCount = 0; partCount < numberOfParts; partCount++) {
                MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                    // this part is attachment
                	System.out.println("Reading attachement");
                	InputStream input = part.getInputStream();
                	Path pathForOutput = Paths.get("D:\\Email-attachements\\" + part.getFileName());
                	Files.copy(input, pathForOutput);
                } else {
                    // this part may be the message content
                    messageContent = part.getContent().toString();
                }
            }

            if (attachFiles.length() > 1) {
                attachFiles = attachFiles.substring(0, attachFiles.length() - 2);
            }
        } else if (contentType.contains("text/plain")
                || contentType.contains("text/html")) {
            Object content = message.getContent();
            if (content != null) {
                messageContent = content.toString();
            }
        }*/
		
		return result;
		
	}

	private String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			System.out.println(" Inside text/* ");
			String s = (String) p.getContent();
			textIsHtml = p.isMimeType("text/html");
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			System.out.println(" Inside multipart/alternative ");
			// prefer html text over plain text
			Multipart mp = (Multipart) p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
				Part bp = mp.getBodyPart(i);
				if (bp.isMimeType("text/plain")) {
					System.out.println(" Inside multipart/alternative text/plain");
					if (text == null)
						text = getText(bp);
					continue;
				} else if (bp.isMimeType("text/html")) {
					System.out.println(" Inside multipart/alternative text/html");
					String s = getText(bp);
					if (s != null)
						return s;
				} else {
					return getText(bp);
				}
			}
			return text;
		} else if (p.isMimeType("multipart/*")) {
			System.out.println(" Inside multipart/*");
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
			}
		}

		return null;
	}

	public void printAllMessages(Message[] msgs) throws Exception {
		for (int i = 0; i < msgs.length; i++) {
			System.out.println("MESSAGE #" + (i + 1) + ":");
			printEnvelope(msgs[i]);
		}
	}

	public void printEnvelope(Message message) throws Exception {

		Address[] a;

		if ((a = message.getFrom()) != null) {
			for (int j = 0; j < a.length; j++) {
				System.out.println("Email From : " + a[j].toString());
			}
		}

		String subject = message.getSubject();

		Date receivedDate = message.getReceivedDate();
		Date sentDate = message.getSentDate();

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		System.out.println("Email Subject : " + subject);

		if (receivedDate != null) {
			System.out.println("Received Date: " + df.format(receivedDate));
		}

		System.out.println("Sent Date : " + df.format(sentDate));
	}

	public static void main(String args[]) throws IOException, MessagingException {
		new EmailListener();
	}

}
