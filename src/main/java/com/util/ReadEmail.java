package com.util;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;

public class ReadEmail {

    private static final String TRUE = "true";
    private static final String MAIL_POP3_HOST = "mail.pop3.host";
    private static final String MAIL_POP3_PORT = "mail.pop3.port";
    private static final String MAIL_POP3_STARTTLS_ENABLE = "mail.pop3.starttls.enable";
    private static final String MAIL_FOLDER_INBOX = "INBOX";

    public static void check(String host, String storeType, String user, String password) throws Exception {

        Store store = null;
        Folder emailFolder = null;
        try {
            Properties properties = new Properties();
            properties.put(MAIL_POP3_HOST, host);
            properties.put(MAIL_POP3_PORT, "995");
            properties.put(MAIL_POP3_STARTTLS_ENABLE, TRUE);
            Session emailSession = Session.getDefaultInstance(properties);

            // create the POP3 store object and connect with the pop server
            store = emailSession.getStore(storeType);

            store.connect(host, user, password);

            emailFolder = store.getFolder(MAIL_FOLDER_INBOX);
            emailFolder.open(Folder.READ_WRITE);

            Message[] messages = emailFolder.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));

            System.out.println("messages.length---" + messages.length);
            if (messages.length == 0) {
                System.out.println("No new messages found.");
            } else {
                for (int i = 0, len = messages.length; i < len; i++) {
                    Message message = messages[i];

                    boolean hasAttachments = hasAttachments(message);
                    if (hasAttachments) {
                        System.out.println(
                                "Email #" + (i + 1) + " with subject " + message.getSubject() + " has attachments.");
                        readAttachment(message);
                    } else {
                        System.out.println("Email #" + (i + 1) + " with subject " + message.getSubject()
                                + " does not have any attachments.");
                        continue;
                    }

                    Folder copyFolder = store.getFolder("copyData");
                    if (copyFolder.exists()) {
                        System.out.println("copy messages...");
                        copyFolder.copyMessages(messages, emailFolder);
                        message.setFlag(Flags.Flag.DELETED, true);
                    }
                }
            }

        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            emailFolder.close(false);
            store.close();
        }
    }

    public static void main(String[] args) throws Exception {

        String host = "outlook.office365.com";
        String username = "test@entransys.com";
        String password = "Spartan203";
        String mailStoreType = "pop3";

        check(host, mailStoreType, username, password);

    }

    private static boolean hasAttachments(Message msg) throws Exception {
        if (msg.isMimeType("multipart/mixed")) {
            Multipart mp = (Multipart) msg.getContent();
            if (mp.getCount() > 1) {
                return true;
            }
        }

        return false;
    }

    public static void readAttachment(Message message) throws Exception {

        Multipart multiPart = (Multipart) message.getContent();
        for (int i = 0; i < multiPart.getCount(); i++) {
            MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);
            if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                String destFilePath = "/home/user/Documents/" + part.getFileName();
                System.out.println("Email attachement ---- " + destFilePath);
                FileOutputStream output = new FileOutputStream(destFilePath);
                InputStream input = part.getInputStream();
                byte[] buffer = new byte[4096];
                int byteRead;
                while ((byteRead = input.read(buffer)) != -1) {
                    output.write(buffer, 0, byteRead);
                }
                output.close();
            }
        }
    }

}
