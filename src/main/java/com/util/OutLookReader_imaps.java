package com.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;

public class OutLookReader_imaps {

	Folder inbox;
	private boolean textIsHtml = false;

	// Constructor of the calss.

	public OutLookReader_imaps() throws IOException {
		System.out.println("Inside MailReader()...");
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties props = System.getProperties();
		// Set manual Properties
		props.setProperty("mail.imaps.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.imaps.socketFactory.fallback", "false");
		props.setProperty("mail.imaps.port", "993");
		props.setProperty("mail.imaps.socketFactory.port", "993");
		props.put("mail.imaps.host", "outlook.office365.com");
		// props.put("mail.debug", "true");

		try {
			/* Create the session and get the store for read the mail. */

			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imaps");

			store.connect("outlook.office365.com", 993, "test@entransys.com", "Spartan203");

			/* Mention the folder name which you want to read. */

			// dd52c308-1233-41dc-84e0-77f8faccf3a2

			inbox = store.getFolder("INBOX");
			// store.get

			/* Open the inbox using store. */

			// inbox.open(Folder.READ_ONLY);
			UIDFolder uf = (UIDFolder) inbox;
			inbox.open(Folder.READ_ONLY);

			Flags seen = new Flags(Flags.Flag.SEEN);// ++
			FlagTerm unseenFlagTerm = new FlagTerm(seen, false);// ++
			Message messages[] = inbox.search(unseenFlagTerm);
			Message confirmationMAil = null;
			/*
			 * FetchProfile fp = new FetchProfile(); fp.add("X-Mailer");
			 * inbox.fetch(messages, fp);
			 * 
			 * for (Message message : messages) { System.out.println(message);
			 * System.out.println("Fetch : " + message.getHeader("X-Mailer"));
			 * 
			 * }
			 */

			/*
			 * Message messages[] = inbox.search(new FlagTerm(new Flags(
			 * Flags.Flag.ANSWERED), false));
			 */
			// Message[] msgs = inbox.getMessages();

			if (messages.length == 0)
				System.out.println("No messages found.");

			System.out.println("No. of Unread Messages : " + inbox.getUnreadMessageCount());
			System.out.println("No. of Messages : " + inbox.getMessageCount());
			System.out.println("No. of Deleted Messages : " + inbox.getMode());

			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];
				// stop after listing ten messages
				if (i > 10) {
					System.exit(0);
					inbox.close(true);
					store.close();
				}
				String result = " " ;
				String[] splitedResult = null;
				
				if(message instanceof MimeMessage)
		        {
		            MimeMessage m = (MimeMessage)message;
		            Object contentObject = m.getContent();
		            if(contentObject instanceof Multipart)
		            {
		                BodyPart clearTextPart = null;
		                BodyPart htmlTextPart = null;
		                Multipart content = (Multipart)contentObject;
		                int count = content.getCount();
		                for(int j=0; j<count; j++)
		                {
		                    BodyPart part =  content.getBodyPart(j);
		                    if(part.isMimeType("text/plain"))
		                    {
		                        clearTextPart = part;
		                        break;
		                    }
		                    else if(part.isMimeType("text/html"))
		                    {
		                        htmlTextPart = part;
		                    }
		                }

		                if(clearTextPart!=null)
		                {
		                    result = (String) clearTextPart.getContent();
		                }
		                else if (htmlTextPart!=null)
		                {
		                    String html = (String) htmlTextPart.getContent();
		                   // String reply = EmailReplyParser.parseReply(html);
		                   // String reply = EmailReplyParser.parseReply(html);
		                   // System.out.println("Reply = > " + reply);
		                    result = Jsoup.parse(html).text();
		                 //   result = Jsoup.parse(reply).text();
		                    splitedResult = result.split("From:");
		                }

		            }
		             else if (contentObject instanceof String) // a simple text message
		            {
		                result = (String) contentObject;
		            }
		        }
		            
				/*String myMail = "";
				String thisLine = "" ;
				String result = getText(message);*/
				System.out.println("Result = > " + result);
				//System.out.println("Splited Result = > " + splitedResult[0]);
				
				/*Multipart mp = (Multipart) message.getContent();
				BodyPart bp = mp.getBodyPart(0);
				String content = bp.getContent().toString();
				
				System.out.println("Result = > " + content);*/
				
				/*Part messagePart = message;
		        Object content = messagePart.getContent();
		        // -- or its first body part if it is a multipart message --
		        if (content instanceof Multipart) {
		            messagePart = ((Multipart) content).getBodyPart(0);
		            System.out.println("[ Multipart Message ]");
		        }
		        // -- Get the content type --
		        String contentType = messagePart.getContentType();
		        // -- If the content is plain text, we can print it --
		        System.out.println("CONTENT:" + contentType);
		        if (contentType.startsWith("text/plain")
		                || contentType.startsWith("text/html")) {
		        	System.out.println("Inside text Result  " );
		            InputStream is = messagePart.getInputStream();
		            BufferedReader reader = new BufferedReader(
		                    new InputStreamReader(is));
		             thisLine = reader.readLine();
		            while (thisLine != null) {
		                System.out.println(thisLine);
		                myMail = myMail + thisLine;
		                thisLine = reader.readLine();
		            }
		        }
		        System.out.println("Result = > " + thisLine);*/
				

				/*String result = "";
				if (messages[i].isMimeType("text/plain")) {
					System.out.println("Content type is Plain ");
					result = messages[i].getContent().toString();
				} 
					 * else if (messages[i].isMimeType("multipart/*")) { MimeMultipart mimeMultipart
					 * = (MimeMultipart) messages[i].getContent(); result =
					 * getTextFromMimeMultipart(mimeMultipart); }
					 
				else if (messages[i].isMimeType("text/html")) {
					System.out.println("Content type is HTML ");
					result = messages[i].getContent().toString();
				} *//*
					 * else{ StringWriter writer = new StringWriter();
					 * IOUtils.copy(messages[i].getInputStream(), writer, "UTF-8"); result =
					 * writer.toString(); }
					 */
				//System.out.println("Result = > " + result);

				System.out.println("Message " + (i + 1));
				System.out.println("From : " + messages[i].getFrom()[0]);
				System.out.println("Subject : " + messages[i].getSubject());
				System.out.println("Sent Date : " + messages[i].getSentDate());
				System.out.println("Header : " + messages[i].getAllHeaders());
				System.out.println("Content : " + messages[i].getContent().toString());
				System.out.println("Content Type : " + messages[i].getContentType());
				System.out.println("Description : " + messages[i].getDescription());
				// System.out.println("Message : " + (Multipart) messages[i].getContent());
				// System.out.println("Content in String : " + (String)
				// messages[i].getContent());
				// messages[i].setFlag(Flags.Flag.SEEN, true);
				// messages[i].getHeader(i);
				// messages[i].ge
				// 70a0cf98-fb0b-43f3-a585-fc1ced0e4025

				System.out.println("Unique Header : " + messages[i].getHeader("Subject"));
				// messages[i].getHeader("X-Universally-Unique-Identifier");
				// Multipart mp =(Multipart) messages[i].getContent();
				System.out.println("Header uni : " + messages[i].getHeader("References"));
				// System.out.println("Message : " + mp.getBodyPart(1));
				// System.out.println("Message : " + mp.getBodyPart(2));
				// System.out.println("Message : " + mp.getBodyPart(3));
				System.out.println("UUID = > " + uf.getUID(messages[i]));

				// messages[i].getHeaderAllLines()
				// messages[i].getAllHeaderLines();
				// 82e496e2-f168-4b06-bc61-44ad72e16d0b
				// c1214c41-15a9-498a-b3fe-2afb45512682
				// String[] customeHeader = messages[i].getHeader("In-Reply-To");
				// System.out.println("Custom Header = > " +customeHeader);
				// System.out.println("String Header = > " +customeHeader.toString());
				// Header header = (Header) messages[i].getHeader("In-Reply-To");

				Enumeration allHeaders = messages[i].getAllHeaders();
				while (allHeaders.hasMoreElements()) {
					Header header = (Header) allHeaders.nextElement();
					String headerName = header.getName();
					String headerVal = header.getValue();
					String customeHeader = "References";
					String customValue;
					// header.get
					//if (headerName.equals(customeHeader)) {

						System.out.println("Header Name : " + headerName);
						System.out.println("Header Value : " + headerVal);
						customValue = headerVal;
						customValue.split("<");
						String[] split = customValue.split("<");
						System.out.println("Splited = > " + split[0]);

					//}

					// headerMap.put(headerName, headerVal);

				}
				// Long messageId = uf.getUID(messages[]);
			}

			/*
			 * System.out.println("No. of Unread Messages : " +
			 * inbox.getUnreadMessageCount()); System.out.println("No. of Messages : " +
			 * inbox.getMessageCount()); System.out.println("No. of Deleted Messages : " +
			 * inbox.getMode());
			 * 
			 * FetchProfile fp = new FetchProfile(); fp.add(FetchProfile.Item.ENVELOPE);
			 * 
			 * inbox.fetch(messages, fp);
			 */

			try {

				// printAllMessages(messages);

				inbox.close(true);
				store.close();

			} catch (Exception ex) {
				System.out.println("Exception arise at the time of read mail");
				ex.printStackTrace();
			}

		} catch (MessagingException e) {
			System.out.println("Exception while connecting to server: " + e.getLocalizedMessage());
			e.printStackTrace();
			System.exit(2);
		}

	}

	private String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			System.out.println(" Inside text/* ");
			String s = (String) p.getContent();
			textIsHtml = p.isMimeType("text/html");
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			System.out.println(" Inside multipart/alternative ");
			// prefer html text over plain text
			Multipart mp = (Multipart) p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
				Part bp = mp.getBodyPart(i);
				if (bp.isMimeType("text/plain")) {
					System.out.println(" Inside multipart/alternative text/plain");
					if (text == null)
						text = getText(bp);
					continue;
				} else if (bp.isMimeType("text/html")) {
					System.out.println(" Inside multipart/alternative text/html");
					String s = getText(bp);
					if (s != null)
						return s;
				} else {
					return getText(bp);
				}
			}
			return text;
		} else if (p.isMimeType("multipart/*")) {
			System.out.println(" Inside multipart/*");
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
			}
		}

		return null;
	}

	public void printAllMessages(Message[] msgs) throws Exception {
		for (int i = 0; i < msgs.length; i++) {
			System.out.println("MESSAGE #" + (i + 1) + ":");
			printEnvelope(msgs[i]);
		}
	}

	public void printEnvelope(Message message) throws Exception {

		Address[] a;

		if ((a = message.getFrom()) != null) {
			for (int j = 0; j < a.length; j++) {
				System.out.println("Email From : " + a[j].toString());
			}
		}

		String subject = message.getSubject();

		Date receivedDate = message.getReceivedDate();
		Date sentDate = message.getSentDate();

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		System.out.println("Email Subject : " + subject);

		if (receivedDate != null) {
			System.out.println("Received Date: " + df.format(receivedDate));
		}

		System.out.println("Sent Date : " + df.format(sentDate));
	}

	public static void main(String args[]) throws IOException {
		new OutLookReader_imaps();
	}

}
