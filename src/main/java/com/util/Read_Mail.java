package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;

public class Read_Mail {

    static String from;

    public static void main(String args[])
    {
    	final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
         Properties props = new Properties();
//         props.setProperty("mail.store.protocol", "imaps");
         props.setProperty("mail.imaps.socketFactory.class", SSL_FACTORY);
         props.setProperty("mail.imaps.socketFactory.fallback", "false");
         props.setProperty("mail.imaps.port", "993");
         props.setProperty("mail.imaps.socketFactory.port", "993");
         props.put("mail.imaps.host", "outlook.office365.com");

         Session session = Session.getDefaultInstance(props,null);

         IMAPStore imapstore = null;

         try {
             try {
                 imapstore = (IMAPStore) session.getStore("imaps");
             } catch (NoSuchProviderException e1) {
                 // TODO Auto-generated catch block
                 e1.printStackTrace();
             }
             imapstore.connect("outlook.office365.com", "test@entransys.com", "Spartan203");

             IMAPFolder folder;
             folder = (IMAPFolder) imapstore.getFolder("Inbox");

             folder.open(IMAPFolder.READ_ONLY);

             Message[] messages;
             messages = folder.getMessages();
             for (int i = 0; i < messages.length; i++) {
                 Message message = messages[i];
                 System.out.println("==============================");
                 System.out.println("Email #" + (i + 1));
                 System.out.println("Subject: " + message.getSubject());
                 System.out.println("From: " + message.getFrom()[0]);
                 //                  System.out.println("Text: " + message.getContent());

                 Object mp;
                 try{
                     mp = (Object) message.getContent();

                     if (mp instanceof String) {  
                         String body = (String)mp;
                         System.out.println("MSG Body : " + body);
                     }  else if (mp instanceof MimeMultipart) { 
                         MimeMultipart mpp = (MimeMultipart)mp;
                         for(int count =0;count<mpp.getCount();count++){
                             MimeBodyPart bp = (MimeBodyPart)mpp.getBodyPart(count);
                             InputStream fileNme = bp.getInputStream();
                             StringWriter writer = new StringWriter();
                             IOUtils.copy(fileNme, writer, "UTF-8");
                             String theString = writer.toString();
                             System.out.println("Text: " +theString);
                         }
                     } else if (mp instanceof Multipart) {
                         Multipart mpp = (Multipart)mp;
                         for(int count =0;count<mpp.getCount();count++){
                             MimeBodyPart bp = (MimeBodyPart)mpp.getBodyPart(count);
                             InputStream fileNme = bp.getInputStream();
                             StringWriter writer = new StringWriter();
                             IOUtils.copy(fileNme, writer, "UTF-8");
                             String theString = writer.toString();
                             System.out.println("Text: " +theString);
                         }
                     } 
                 }catch (IOException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
                 }
             }
         } catch (MessagingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
         }
         catch(Exception e)
         {
            System.out.println("Exception Msg: " + e.getMessage()); 
         }
    }
}
