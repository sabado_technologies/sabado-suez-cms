package com.util;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

	public class CheckUnreadEmail {

	public static void main(String args[]){

	    try{

	        //Create object email properties
	        PropertiesEmail propertiesEmail = new PropertiesEmail();

	        //Set email server properties
	        Properties props = propertiesEmail.setServerProperties();

	        Session session = Session.getDefaultInstance(props);

	        Store store = session.getStore();

	        store.connect(propertiesEmail.host, propertiesEmail.user, propertiesEmail.password);

	        //Create the folder object and open it
	        Folder folder = store.getFolder("INBOX");
	        folder.open(Folder.READ_WRITE);

	        //Total unread messages
	        System.out.println("Total messages: " + folder.getMessageCount());
	        System.out.println("Unread messages: " + folder.getUnreadMessageCount());


	        //Create variable for search unread message
	        FlagTerm flag = new FlagTerm(new Flags(Flags.Flag.SEEN), false);

	        //Retrieve all messages
	        //Message [] messages = folder.getMessages();

	        //Retrieve unread messages from the folder INBOX
	        Message [] unreadMessage = folder.search(flag);

	        for (int i = 0, n = unreadMessage.length; i < n; i++){
	            Message message = unreadMessage[i];
	            System.out.println("--------------");
	            System.out.println("Subject: " + message.getSubject());
	        }

	        //folder.setFlags(unreadMessage, new Flags(Flags.Flag.SEEN), true);

	        //close the store and folder objects
	        folder.close(false);
	        store.close();

	    }catch(Exception e){
	        e.printStackTrace();
	    }

	}

}
