package com.util;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

public class JavaMailSearchInbox {
	
	public static void main(String args[]) throws Exception {

	    // mail server info
	    String host = "outlook.office365.com";
	    String user = "test@entransys.com";
	    String password = "Spartan203";
	   // int port = 995;

	    // connect to my pop3 inbox in read-only mode
	   /* MailSSLSocketFactory socketFactory= new MailSSLSocketFactory();
	    socketFactory.setTrustAllHosts(true);*/
	   // prop.put("mail.imap.ssl.socketFactory", socketFactory);
	    
	     Properties properties = System.getProperties();
	     Properties propsSSL = new Properties();
	     propsSSL.put("mail.transport.protocol", "smtps");
	     propsSSL.put("mail.smtps.host", "outlook.office365.com");
	     propsSSL.put("mail.smtps.auth", "true");
	     propsSSL.put("mail.smtps.ssl.checkserveridentity", "false");
	     propsSSL.put("mail.smtps.ssl.trust", "*");
	     
	    // z.put("mail.imap.ssl.socketFactory", socketFactory);
	     
	    Session session = Session.getDefaultInstance(properties);
	    Store store = session.getStore("pop3");
	   // store.connect(host, port, user, password);
	    store.connect(host, user, password);
	    Folder inbox = store.getFolder("inbox");
	    inbox.open(Folder.READ_ONLY);

	    // search for all "unseen" messages
	    Flags seen = new Flags(Flags.Flag.SEEN);
	    FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
	    Message messages[] = inbox.search(unseenFlagTerm);
	    
	    if (messages.length == 0) System.out.println("No messages found.");

	    for (int i = 0; i < messages.length; i++) {
	      // stop after listing ten messages
	      if (i > 10) {
	        System.exit(0);
	        inbox.close(true);
	        store.close();
	      }

	      System.out.println("Message " + (i + 1));
	      System.out.println("From : " + messages[i].getFrom()[0]);
	      System.out.println("Subject : " + messages[i].getSubject());
	      System.out.println("Sent Date : " + messages[i].getSentDate());
	      System.out.println("Header : " + messages[i].getAllHeaders());
	    }

	    inbox.close(true);
	    store.close();
	  }

}
