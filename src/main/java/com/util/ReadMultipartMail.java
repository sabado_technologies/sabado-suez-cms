package com.util;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

public class ReadMultipartMail {

	  public static void main(String args[]) throws Exception {

	  String host = "outlook.office365.com";
	  String username = "test@entransys.com";
	  String passwoed = "Spartan203";

	  System.out.println("Inside MailReader()...");
      final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
      Properties props = System.getProperties();
      // Set manual Properties
      props.setProperty("mail.imaps.socketFactory.class", SSL_FACTORY);
      props.setProperty("mail.imaps.socketFactory.fallback", "false");
      props.setProperty("mail.imaps.port", "993");
      props.setProperty("mail.imaps.socketFactory.port", "993");
      props.put("mail.imaps.host", "outlook.office365.com");
	  Session session = Session.getDefaultInstance(props);

	  Store store = session.getStore("imaps");
	  store.connect(host, username, passwoed);

	  Folder folder = store.getFolder("inbox");

	  if (!folder.exists()) {
	  System.out.println("No INBOX...");
	  System.exit(0);
	  }
	  folder.open(Folder.READ_WRITE);
	  Message[] msg = folder.getMessages();

	  for (int i = 0; i < msg.length; i++) {
	  System.out.println("------------ Message " + (i + 1) + " ------------");
	  String from = InternetAddress.toString(msg[i].getFrom());
	  if (from != null) {
	  System.out.println("From: " + from);
	  }

	  String replyTo = InternetAddress.toString(
	  msg[i].getReplyTo());
	  if (replyTo != null) {
	  System.out.println("Reply-to: " + replyTo);
	  }
	  String to = InternetAddress.toString(
	  msg[i].getRecipients(Message.RecipientType.TO));
	  if (to != null) {
	  System.out.println("To: " + to);
	  }

	  String subject = msg[i].getSubject();
	  if (subject != null) {
	  System.out.println("Subject: " + subject);
	  }
	  Date sent = msg[i].getSentDate();
	  if (sent != null) {
	  System.out.println("Sent: " + sent);
	  }

	  System.out.println();
	  System.out.println("Message : ");

	  Multipart multipart = (Multipart) msg[i].getContent();
	  
	  for (int x = 0; x < multipart.getCount(); x++) {
	  BodyPart bodyPart = multipart.getBodyPart(x);

	  String disposition = bodyPart.getDisposition();

	  if (disposition != null && (disposition.equals(BodyPart.ATTACHMENT))) {
	  System.out.println("Mail have some attachment : ");

	  DataHandler handler = bodyPart.getDataHandler();
	  System.out.println("file name : " + handler.getName());
	  } else {
	  System.out.println(bodyPart.getContent());
	  }
	  }
	  System.out.println();
	  }
	  folder.close(true);
	  store.close();
	  }
	}
