package com.exception;

public class ValidationExceptions extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}

	public ValidationExceptions() {

	}
	
	public ValidationExceptions(String message){
		super(message);
	}
	
	public ValidationExceptions(Throwable cause){
		super(cause);
	}
	
	public ValidationExceptions(String message,Throwable cause){
		super(message,cause);
	}
	
	
}