package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entity.InventoryCheckDetails;
import com.entity.ProcessOrderRm;
import com.entity.ProcessTracker;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.LocationRawmaterialDetails;
import com.response.NotificationTabsForWarehouse;
import com.response.PackingOrderInventoryPostingResponse;
import com.response.ProcessOrderInventoryPostingResponse;
import com.response.RepackingProcessOrderCount;
import com.service.WarehouseManagerService;

/**
 * 
 * @author ullas
 *
 **/

@RestController
@RequestMapping("/WareHouseController")
public class WarehouseController {

	@Autowired
	WarehouseManagerService warehouseManagerService;

	@RequestMapping(value = "/getAllProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getAllProcessOrders() {
		List<ProcessorderProd> listOfProcessOrders = warehouseManagerService.getAllProcessOrders();
		return listOfProcessOrders;
	}

	@RequestMapping(value = "/getProcessOrderDetails/{processorderProdNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessOrderRm> getProcessOrderDetails(@PathVariable Long processorderProdNumber) {
		List<ProcessOrderRm> ProcessOrderDetails = warehouseManagerService
				.getProcessOrderDetails(processorderProdNumber);
		return ProcessOrderDetails;
	}

	@RequestMapping(value = "/setProcessOrderTrackerStatus/{processorderProdNumber}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int setProcessOrderTrackerStatus(@PathVariable Long processorderProdNumber, @PathVariable String ssoId) {
		int ProcessTrackerId = warehouseManagerService.setProcessOrderTrackerStatus(processorderProdNumber, ssoId);
		return ProcessTrackerId;
	}

	@RequestMapping(value = "/updateProcessOrderStatus/{processorderProdNumber}/{ProcessTrackerId}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int updateProcessOrderStatus(@PathVariable Long processorderProdNumber,
			@PathVariable String ProcessTrackerId,@PathVariable String ssoId) {
		int ProcessOrderStatus = warehouseManagerService.updateProcessOrderStatus(processorderProdNumber,
				ProcessTrackerId,ssoId);
		return ProcessOrderStatus;
	}

	@RequestMapping(value = "/getProcessOrderCount", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getProcessOrderCount() {
		List<ProcessorderProd> ProcessOrderCount = warehouseManagerService.getProcessOrderCount();
		return ProcessOrderCount;
	}

	@RequestMapping(value = "/getProcessOrderCompletedCount", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getProcessOrderCompletedCount() {
		List<ProcessorderProd> ProcessOrderCompletedCount = warehouseManagerService.getProcessOrderCompletedCount();
		return ProcessOrderCompletedCount;
	}

	@RequestMapping(value = "/setTrackerStatusForRm", produces = "application/json", method = RequestMethod.POST)
	public int setTrackerStatusForRm(@RequestBody ProcessTracker processTrackerTable) {
		// System.out.println(processTrackerTable.getProcessorderNumber());
		int ProcessTrackerId = warehouseManagerService.setTrackerStatusForRm(processTrackerTable);
		return ProcessTrackerId;
	}

	@RequestMapping(value = "/getAllTodayProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getAllTodayProcessOrders() {
		List<ProcessorderProd> listOfTodayProcessOrders = warehouseManagerService.getAllTodayProcessOrders();
		return listOfTodayProcessOrders;
	}

	@RequestMapping(value = "/getProcessOrdersForQuality", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getProcessOrdersForQuality() {
		List<ProcessorderProd> listOfProcessOrdersForQuality = warehouseManagerService.getProcessOrdersForQuality();
		return listOfProcessOrdersForQuality;
	}
	
	//forPosting
	@RequestMapping(value = "/getProcessOrdersForPosting", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getProcessOrdersForPosting() {
		List<ProcessorderProd> getProcessOrdersForPosting = warehouseManagerService.getProcessOrdersForPosting();
		return getProcessOrdersForPosting;
	}
	//forPosting
	@RequestMapping(value = "/getProcessOrdersForPostingCheck/{processorderProdNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessOrderInventoryPostingResponse> getProcessOrdersForPostingCheck(@PathVariable Long processorderProdNumber) {
		List<ProcessOrderInventoryPostingResponse> getProcessOrdersForPostingCheck = warehouseManagerService
				.getProcessOrdersForPostingCheck(processorderProdNumber);
		return getProcessOrdersForPostingCheck;
	}


	@RequestMapping(value = "/associatedPackingInfo/{processorderProdNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> associatedPackingInfo(@PathVariable long processorderProdNumber) {
		// TODO Auto-generated method stub
		List<ProcessorderPckg> associatedPackagingInfo = warehouseManagerService
				.associatedPackingInfo(processorderProdNumber);
		return associatedPackagingInfo;
	}
	
	//forPosting
	@RequestMapping(value = "/associatedPackingInfoForPosting/{processorderProdNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> associatedPackingInfoForPosting(@PathVariable long processorderProdNumber) {
		// TODO Auto-generated method stub
		List<ProcessorderPckg> associatedPackingInfoForPosting = warehouseManagerService
				.associatedPackingInfoForPosting(processorderProdNumber);
		return associatedPackingInfoForPosting;
	}
	
	
	@RequestMapping(value = "/setTrackerForWarehouseQuality/{ssoId}/{locationPlaced}/{containerQuantity}", produces = "application/json", method = RequestMethod.POST)
	public int setTrackerForWarehouseQuality(@PathVariable String ssoId, @PathVariable String locationPlaced, @PathVariable String containerQuantity,@RequestBody ProcessorderPckg packingdetail) {
		// System.out.println(ProcessorderPckg.getProcessorderNumber());
		int ProcessTrackerId = warehouseManagerService.setTrackerForWarehouseQuality(ssoId,locationPlaced,containerQuantity, packingdetail);
		return ProcessTrackerId;
	}

	@RequestMapping(value = "/setTrackerforInventory/{processorderProdNumber}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int setTrackerforInventory(@PathVariable Long processorderProdNumber ,@PathVariable String ssoId) {
		int ProcessOrderStatus = warehouseManagerService.setTrackerforInventory(processorderProdNumber ,ssoId);
		return ProcessOrderStatus;
	}

	@RequestMapping(value = "/setRmLocationDetails/{processorderProdNumber}/{rmId}", produces = "application/json", method = RequestMethod.POST)
	public int setRmLocationDetails(@PathVariable Long processorderProdNumber,@PathVariable String rmId,
			@RequestBody LocationRawmaterialDetails locationRawmaterialDetails) {
		// System.out.println(ProcessorderPckg.getProcessorderNumber());
		int LocationStatus = warehouseManagerService.setRmLocationDetails(processorderProdNumber,rmId,
				locationRawmaterialDetails);
		return LocationStatus;
	}

	@RequestMapping(value = "/processOrderQualityCheck/{processorderProdNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getProcessOrdersForQuality(@PathVariable Long processorderProdNumber) {
		List<ProcessorderProd> listOfProcessOrdersForQuality = warehouseManagerService
				.getProcessOrdersForQuality(processorderProdNumber);
		return listOfProcessOrdersForQuality;
	}

	@RequestMapping(value = "/getAllPendingrocessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getAllPendingProcessOrders() {
		List<ProcessorderProd> listOfProcessOrders = warehouseManagerService.getAllPendingProcessOrders();
		return listOfProcessOrders;
	}

	@RequestMapping(value = "/setProcessOrderTrackerStatusForRejection/{processorderProdNumber}/{ssoId}/{submitMessage}", produces = "application/json", method = RequestMethod.POST)
	public int setProcessOrderTrackerStatusForRejection(@PathVariable Long processorderProdNumber,
			@PathVariable String ssoId,@PathVariable String submitMessage) {
		int ProcessTrackerId = warehouseManagerService.setProcessOrderTrackerStatusForRejection(processorderProdNumber,
				ssoId ,submitMessage);
		return ProcessTrackerId;
	}
	
	@RequestMapping(value = "/getAllContainerDetailsForProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getAllContainerDetailsForProcessOrders() {
		List<ProcessorderProd> listOfProcessOrders = warehouseManagerService.getAllContainerDetailsForProcessOrders();
		return listOfProcessOrders;
	}
	
	@RequestMapping(value = "/getContainerDetailsForProcessOrder/{processorderProdNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<InventoryCheckDetails> getContainerDetailsForProcessOrder(@PathVariable Long processorderProdNumber) {
		List<InventoryCheckDetails> getContainerDetailsForProcessOrder = warehouseManagerService
				.getContainerDetailsForProcessOrder(processorderProdNumber);
		return getContainerDetailsForProcessOrder;
	}
	
	
	@RequestMapping(value = "/getAllContainerDetailsForRepackProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> getAllContainerDetailsForRepackProcessOrders() {
		List<ProcessorderPckg> listOfRepackProcessOrders = warehouseManagerService.getAllContainerDetailsForRepackProcessOrders();
		return listOfRepackProcessOrders;
	}
	
	@RequestMapping(value = "/getContainerDetailsForRepackProcessOrder/{processorderPckgNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<InventoryCheckDetails> getContainerDetailsForRepackProcessOrder(@PathVariable Long processorderPckgNumber) {
		List<InventoryCheckDetails> getContainerDetailsForRepackProcessOrder = warehouseManagerService
				.getContainerDetailsForRepackProcessOrder(processorderPckgNumber);
		return getContainerDetailsForRepackProcessOrder;
	}
	
	
	@RequestMapping(value = "/getPackingOrderForInventory", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> getPackingOrderForInventory() {
		List<ProcessorderPckg> listOfProcessOrders = warehouseManagerService.getPackingOrderForInventory();
		return listOfProcessOrders;
	}
	
	//forPosting Repackaging 
	@RequestMapping(value = "/getPackingOrderForInventoryPostingList", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> getPackingOrderForInventoryPostingList() {
		List<ProcessorderPckg> getPackingOrderForInventoryPostingList = warehouseManagerService.getPackingOrderForInventoryPostingList();
		return getPackingOrderForInventoryPostingList;
	}
	
	@RequestMapping(value = "/getRePackingOrderForRM", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> getRePackingOrderForRM() {
		List<ProcessorderPckg> PackingOrderForRM = warehouseManagerService.getRePackingOrderForRM();
		return PackingOrderForRM;
	}
	
	@RequestMapping(value = "/getRePackingOrderForRMDetails/{processorderPckgNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> getRePackingOrderForRMDetails(@PathVariable Long processorderPckgNumber) {
		List<ProcessorderPckg> PackingOrderForRM = warehouseManagerService.getRePackingOrderForRMDetails(processorderPckgNumber);
		return PackingOrderForRM;
	}

	
	@RequestMapping(value = "/setRmLocationDetailsForRepack/{processorderPckgNumber}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int setRmLocationDetailsForRepack(@PathVariable Long processorderPckgNumber,@PathVariable String ssoId ,
			@RequestBody LocationRawmaterialDetails locationRawmaterialDetails) {
		
		int LocationStatus = warehouseManagerService.setRmLocationDetailsForRepack(processorderPckgNumber, ssoId,
				locationRawmaterialDetails);
		return LocationStatus;
	}
	
	@RequestMapping(value = "/getPackingOrderForInventory1/{processorderPckgNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> getPackingOrderForInventory1(@PathVariable Long processorderPckgNumber) {
		List<ProcessorderPckg> listOfProcessOrders = warehouseManagerService.getPackingOrderForInventory1(processorderPckgNumber);
		return listOfProcessOrders;
	}
	
	//forPosting Repackaging 
	@RequestMapping(value = "/getPackingOrderForInventoryPosting/{processorderPckgNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<PackingOrderInventoryPostingResponse> getPackingOrderForInventoryPosting(@PathVariable Long processorderPckgNumber) {
		List<PackingOrderInventoryPostingResponse> getPackingOrderForInventoryPosting = warehouseManagerService.getPackingOrderForInventoryPosting(processorderPckgNumber);
		return getPackingOrderForInventoryPosting;
	}
	
	//for Posting
	@RequestMapping(value = "/setProcessOrderStatusForPosting/{processorderProdNumber}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int setProcessOrderStatusForPosting(@PathVariable Long processorderProdNumber ,@PathVariable String ssoId
			) {
		int ProcessTrackerId = warehouseManagerService.setProcessOrderStatusForPosting(processorderProdNumber ,ssoId);
		return ProcessTrackerId;
	}
	
	//for Posting Repacking
	
	@RequestMapping(value = "/setProcessOrderStatusForRepackPosting/{processorderPckgNumber}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int setProcessOrderStatusForRepackPosting(@PathVariable Long processorderPckgNumber ,@PathVariable String ssoId
			) {
		int ProcessTrackerId = warehouseManagerService.setProcessOrderStatusForRepackPosting(processorderPckgNumber ,ssoId);
		return ProcessTrackerId;
	}
	
	//For getting Repacking Count
	@RequestMapping(value = "/getRepackingCount", produces = "application/json", method = RequestMethod.GET)
	public RepackingProcessOrderCount getRepackingCount() {
		RepackingProcessOrderCount getRepackingCount = warehouseManagerService.getRepackingCount();
		return getRepackingCount;
	}
	
	//getNotificationForWarehouseCount
	@RequestMapping(value = "/getNotificationForWarehouseCount", produces = "application/json", method = RequestMethod.GET)
	public NotificationTabsForWarehouse getNotificationForWarehouseCount() {
		NotificationTabsForWarehouse getNotificationForWarehouseCount = warehouseManagerService.getNotificationForWarehouseCount();
		return getNotificationForWarehouseCount;
	}
}
