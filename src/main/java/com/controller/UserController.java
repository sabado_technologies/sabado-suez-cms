package com.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.MultiRoles;
import com.entity.Role;
import com.entity.User;
import com.entity.UserDetailsForAlerts;
import com.exception.ValidationExceptions;
import com.request.AESCrypt;
import com.request.AddUserRequest;
import com.response.Response;
import com.service.UserService;
import com.validation.UserValidation;

/**
 * @author Jayanth Kumar
 *
 */

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserValidation userValidation;
	
	///...........................Mapping for Adding New User........................................
	@RequestMapping(value = "/addUser/{roles}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response addUser(@RequestBody AddUserRequest user , @PathVariable List<String> roles) throws Exception {
		
				
		////System.out.println("SSo =>" + user.getSsoid());
		String userName = null;
		User userById =null;
		User newUser =null;
		String encryptedPassword=null;
		
		
		try {
			  userById = userService.getUserBySSOId(user.getSsoid());
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Response("400", ex.getMessage());
		}
		
		if(userById.getSsoid()!=null){
			return new Response("300", user.getSsoid());				
		}
		
		String password=userService.generatedPassword(6);
		
		try {
	        encryptedPassword = AESCrypt.encrypt(password);
	        } catch(Exception e) {
	        	//System.out.println("bug"+e.getMessage());
	        }
		user.setPassword(encryptedPassword);
		
		try {
			userValidation.getUser(user);
		} catch (ValidationExceptions ex) {
			ex.printStackTrace();
			return new Response("400", ex.getMessage());
		}
		try {
			userName = userService.saveUser(user , roles);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		try {
			newUser=userService.findUser(user.getSsoid());
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		if(newUser.getSsoid()!=null){
			String decryptedPassword = AESCrypt.decrypt(newUser.getPassword());
			final boolean transport_email_use_ssl = false;
			final boolean transport_email_use_tls = true;
			final int PORT = 587;
			final String HOST = "smtp.office365.com";
			
			String sender = "test@entransys.com";
			String pass = "Spartan203";			
			String fromAddress = "test@entransys.com";
			String toAddress = newUser.getEmail();
			String subject = "Login details for CMS application";
			String message = "Hello " +newUser.getUserName() +",\n\nPlease use below mentioned SSO and Password to Login to our application"+"\nSSO :"+newUser.getSsoid()+ "\nPassword : "+ decryptedPassword +"\n\nRegards, CMS application";
			
			try {

				Email email1 = new SimpleEmail();
				email1.setHostName(HOST);
				email1.setSmtpPort(PORT);
				email1.setAuthenticator(new DefaultAuthenticator(sender, pass));
				email1.setStartTLSEnabled(transport_email_use_tls);
				email1.setStartTLSRequired(transport_email_use_ssl);
				email1.setFrom(fromAddress);
				email1.setSubject(subject);
				email1.setMsg(message);
				email1.addTo(toAddress);
				email1.send();
			}catch(Exception ex) {
				return new Response("400", ex.getMessage());
			}
			
		}
		return new Response("200", userName);
	}
	
	///......................Mapping for Updating User information.....................
	@RequestMapping(value = "/editUser/{roles}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response editUser( @RequestBody User user,@PathVariable List<String> roles) throws Exception {
		String userName = null;
		try {
			userName = userService.editUser(user,roles);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", userName);
	}
	
	///......................Mapping for List of All Users.............................
	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getAllUsers() throws Exception {
		List<User> users = null;
		
		try {
			users = userService.getAllUsers();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", users);
	}
	
	///........................Searching User by SSO....................................
	@RequestMapping(value = "/findUser/{ssoid}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response findUser(@PathVariable String ssoid) throws Exception {
		User users = null;
		
		try {
			 users=userService.findUser(ssoid);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", users);
	}
	
	///.......................List of All Roles.......................................
	@RequestMapping(value = "/getRoles", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getRole() throws Exception {
		List<Role> roles = null;
		
		try {
			roles = userService.getRoles();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", roles);
	}
	
	///..........................List of All Active Users......................................
	@RequestMapping(value = "/getActiveUsers", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getActiveUsers() throws Exception {
		List<User> users = null;
		
		try {
			users = userService.getActiveUsers();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", users);
	}
	
	///..........................List of All De-Active Users.............................
	@RequestMapping(value = "/getDeActiveUsers", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getInActiveUsers() throws Exception {
		List<User> users = null;
		
		try {
			users = userService.getDeActiveUsers();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", users);
	}
	 
	///............................Activate Users....................................	
	@RequestMapping(value = "/activateUser", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response activateUser(@RequestBody List<User> user) throws Exception {
		int users ;
				
		try {
			users=userService.activateUser(user);
			
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", users);
	}
	
	///...........................De-Activate Users.......................................
	@RequestMapping(value = "/deActivateUser", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response deActivateUser(@RequestBody List<User> user) throws Exception {
		int users ;
				
		try {
			users=userService.deActivateUser(user);
			
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", users);
	}
	
	@RequestMapping(value = "/getUserBySSOId/{ssoId}", produces = "application/json" ,method=RequestMethod.GET)
	public @ResponseBody User getProcessOrderById(@PathVariable String ssoId){
		User  user = userService.getUserBySSOId(ssoId);
		return user;
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response changePassword(@RequestBody AddUserRequest user) throws Exception {
		int user1 = 0;
		String encryptedPassword=null;
		try {
			encryptedPassword = AESCrypt.encrypt(user.getPassword());
			user.setPassword(encryptedPassword);
			user1 = userService.changePassword(user);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Response("400", ex.getMessage());
		}
		return new Response("200", user1);
	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response resetPassword(@RequestBody AddUserRequest user) throws Exception {
		int user1 = 0;		
		String encryptedPassword=null;
		//AddUserRequest user=null;
		try {
			String password=userService.generatedPassword(6);
			//System.out.println("New Pass =>" +password);
			encryptedPassword = AESCrypt.encrypt(password);
			//System.out.println("encrypt =>" +encryptedPassword);
			//user.setSsoid(sso);
			//user.setPassword(encryptedPassword);
			//System.out.println(user.getPassword());
			
			user1 = userService.resetPassword(user.getSsoid(), encryptedPassword);
			
			String decryptedPassword = AESCrypt.decrypt(encryptedPassword);
			final boolean transport_email_use_ssl = false;
			final boolean transport_email_use_tls = true;
			final int PORT = 587;
			final String HOST = "smtp.office365.com";
			
			String sender = "test@entransys.com";
			String pass = "Spartan203";			
			String fromAddress = "test@entransys.com";
			String toAddress = user.getEmail();
			String subject = "Updated password for CMS application ";
			String message = "Hello " +user.getUsername() +",\n\nPlease use below mentioned SSO and Password to Login to our application"+"\nSSO :"+user.getSsoid()+ "\nPassword : "+ decryptedPassword +"\n\nRegards, CMS application";
			
			Email email1 = new SimpleEmail();
			email1.setHostName(HOST);
			email1.setSmtpPort(PORT);
			email1.setAuthenticator(new DefaultAuthenticator(sender, pass));
			email1.setStartTLSEnabled(transport_email_use_tls);
			email1.setStartTLSRequired(transport_email_use_ssl);
			email1.setFrom(fromAddress);
			email1.setSubject(subject);
			email1.setMsg(message);
			email1.addTo(toAddress);
			email1.send();
			
		} catch (Exception ex) {
			//ex.printStackTrace();
			//System.out.println("Failed");
			ex.printStackTrace();
			return new Response("400", ex.getMessage());
		}
		return new Response("200", user1);
	}
	
	@RequestMapping(value = "/tempDeviation/{reactor}/{temp}/{process}/{processOrder}/{sso}/{userName}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response tempDeviation(@PathVariable String reactor,@PathVariable int temp,@PathVariable String process,@PathVariable long processOrder,@PathVariable String sso,@PathVariable String userName) throws Exception {
		int user1 = 0;
		ArrayList<String> arr = new ArrayList<String>();
		//arr.add("subinbenny@entransys.com");
		arr.add("srinidhi.entransys@gmail.com");
		arr.add("abhilash.entransys@gmail.com");
		String date = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss aaa").format(new Date());
		
		try {
			user1=userService.insertEmailAlert(reactor, temp, process, processOrder, sso, userName);
		} catch(Exception ex){
			return new Response("400", ex.getMessage());
		}
		
		for(String to: arr){
			try {
				//user1=userService.insertEmailAlert(reactor, temp, process, processOrder, sso, userName);
				final boolean transport_email_use_ssl = false;
				final boolean transport_email_use_tls = true;
				final int PORT = 587;
				final String HOST = "smtp.office365.com";
				
				String sender = "test@entransys.com";
				String pass = "Spartan203";			
				String fromAddress = "test@entransys.com";
				String toAddress = to;
				String subject = "Suez Temperature Limit Crossed Alert";
				String message="Hello CMS User, \n\nMessage type : Temperature Alerts \nMessage : Temperature crossed the defined limit while " +process+"\nReactor : "+reactor+"\nCurrent temperature : "+ temp +" degree \nOpearator SSO : " +sso+"\nOperator Name : "+userName+"\nProcess order : "+processOrder+"\nDate/Time : "+date+"\n\nRegards, CMS application";
				//String message = "Hello Amod" +",\nTemperature in the Reactor " +" "+reactor+ " while " +process+" is crossed the average temperature limit."+"\nCurrent temperature of the Reactor is : "+ temp +" degree";
				
				Email email1 = new SimpleEmail();
				email1.setHostName(HOST);
				email1.setSmtpPort(PORT);
				email1.setAuthenticator(new DefaultAuthenticator(sender, pass));
				email1.setStartTLSEnabled(transport_email_use_tls);
				email1.setStartTLSRequired(transport_email_use_ssl);
				email1.setFrom(fromAddress);
				email1.setSubject(subject);
				email1.setMsg(message);
				email1.addTo(toAddress);
				email1.send();
				
			} catch (Exception ex) {
				ex.printStackTrace();
				//System.out.println(to);
				return new Response("400", ex.getMessage());
			}
		}
		
		return new Response("200", user1);
	}
	
	@RequestMapping(value = "/getMultiRoles", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getMultiRoles() throws Exception {
		List<MultiRoles> roles = null;
		
		try {
			roles = userService.getMultiRoles();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", roles);
	}
	
	@RequestMapping(value = "/addUserForAlerts", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response addUserForAlerts(@RequestBody UserDetailsForAlerts user) throws Exception {
		int user1 = 0;
		try {
			user1 = userService.addUserForAlerts(user);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Response("400", ex.getMessage());
		}
		return new Response("200", user1);
	}
	
	@RequestMapping(value = "/getAlertUsers", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getAlertUsers() throws Exception {
		List<UserDetailsForAlerts> users = null;
		
		try {
			users = userService.getAlertUsers();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", users);
	}
	
	@RequestMapping(value = "/updateUserAlertInfo", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response updateUserAlertInfo(@RequestBody UserDetailsForAlerts user) throws Exception {
		int user1 = 0;
		try {
			user1 = userService.updateAlertUser(user);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Response("400", ex.getMessage());
		}
		return new Response("200", user1);
	}
	
	@RequestMapping(value = "/deleteUserAlertInfo/{userId}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response deleteUserAlertInfo(@PathVariable int userId) throws Exception {
		int user =0;
		
		try {
			user=userService.deleteAlertUser(userId);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", user);
	}

}
