package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Associate;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.Response;
import com.service.AssociateService;

/**
 * @author Jayanth Kumar
 *
 */

@RestController
@RequestMapping("/associate")
public class AssociateController {
	
	@Autowired
	AssociateService associateService;
	
	@RequestMapping(value = "/getProductionOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getProductionProcessOrders() throws Exception {
		List<ProcessorderProd> processOrders = null;
		
		try {
			processOrders = associateService.getProductionProcessOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", processOrders);
	}
	
	@RequestMapping(value = "/getPackageOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPackageProcessOrders() throws Exception {
		List<ProcessorderPckg> packageOrders = null;
		
		try {
			packageOrders = associateService.getPackageProcessOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packageOrders);
	}
	
	@RequestMapping(value = "/associateProcessOrders", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response addUser(@RequestBody List<Associate> associate) throws Exception {
		int associates;
		
		try {
			associates = associateService.associateProcess(associate);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", associates);
	}
	
	@RequestMapping(value = "/getDeletedProcessOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getDeletedProcessOrders() throws Exception {
		List<ProcessorderPckg> packageOrders = null;
		
		try {
			packageOrders = associateService.getDeletedProcessOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packageOrders);
	}

}
