package com.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.entity.ProcessorderProd;
import com.request.PdfValues;
import com.request.PdfValuesForPack;
import com.response.Response;
import com.service.FileUploadService;
import com.service.LoginService;

@RestController
@RequestMapping("/fileP")
public class FileUploadForPackController {

	@Autowired
	FileUploadService fileUploadService;

	@RequestMapping(value = "/fileUploadForPack", method = RequestMethod.POST)
	/* TODO:Remove all the hard path to properties file */
	public Response fileUploadForPack(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Response responseD = new Response("200", "Success");
		MultipartHttpServletRequest mRequest;
		String filename = "output.pdf";
		ProcessorderProd orders =null;
		String productionOrder=null;
		try {
			mRequest = (MultipartHttpServletRequest) request;
			mRequest.getParameterMap();

			Iterator itr = mRequest.getFileNames();

			while (itr.hasNext()) {
				MultipartFile mFile = mRequest.getFile((String) itr.next());
				String fileName = mFile.getOriginalFilename();
				////System.out.println(fileName);

				java.nio.file.Path path = Paths.get("C:/CMS_HOSKOTE/ProcessOrders/temp/" + filename);
				Files.deleteIfExists(path);
				InputStream in1 = mFile.getInputStream();
				Files.copy(in1, path);
				PdfValuesForPack pdfValues = readPdf();
				//System.out.println("First Material Memo =>" + pdfValues.getMaterialMemo());
				String po = pdfValues.getPackingCode();
				productionOrder=pdfValues.getProcessOrderNo();
				orders = fileUploadService.getPackingOrder(pdfValues.getProcessOrderNo());
				if(orders.getBatchNumber() !=null){
					responseD.setCode("300");
					responseD.setData(pdfValues.getProcessOrderNo());
					return responseD;
				}
				//System.out.println("First =>"+pdfValues.getProcessOrderNo().charAt(0));
				//char a ='5';
				if(pdfValues.getPackingCode().charAt(0) !='6'){
					responseD.setCode("500");
					responseD.setData(productionOrder);
					return responseD;
				}
				//System.out.println("process order ==>" + po);
				String processDate = pdfValues.getBatchDate();
				processDate = processDate.replaceAll("/", "");
				//System.out.println("po =>" + po + " &processDate=>" + processDate);
				boolean value = new File("C:/CMS_HOSKOTE/ProcessOrders/temp/output/package/" + processDate + "/").mkdirs();
				java.nio.file.Path pathForOutput = Paths
						.get("C:/CMS_HOSKOTE/ProcessOrders/temp/output/package/" + processDate + "/" + productionOrder + ".pdf");
				InputStream in2 = mFile.getInputStream();
				Files.deleteIfExists(pathForOutput);
				//System.out.println("File is existing and successfully deleted");								
				Files.copy(in2, pathForOutput);
				uploadBarCodes(pdfValues);
				//String packMaterial = pdfValues.getRmCodes().get(0);
				////System.out.println("Re-Pack material =>" + packMaterial);
				////System.out.println("Re-Pack material first character =>" +pdfValues.getRmCodes().get(0).charAt(0));
				if(pdfValues.getRmCodes().get(0).charAt(0) !='5'){
					//System.out.println("This PDF is for Re-packing");
					fileUploadService.inertRePackagingOrders(pdfValues);
				}else {
					fileUploadService.inertIntoPackingOrders(pdfValues);
				}
				//fileUploadService.inertIntoPackingOrders(pdfValues);
			}
		} catch (Exception e) {
			responseD.setCode("400");
			responseD.setData(productionOrder);
		}
		responseD.setData(productionOrder);
		return responseD;
	}

	public static PdfValuesForPack readPdf() throws IOException {
		//System.out.println("Main Method Started");
		File file = new File("C:/CMS_HOSKOTE/ProcessOrders/temp/output.pdf");
		PDDocument document = PDDocument.load(file);
		PDFTextStripper pdfStripper = new PDFTextStripper();
		String text = pdfStripper.getText(document);
		text = text.trim();
		text = text.replaceAll(" +", " ");
		text = text.replaceAll("(?m)^[ \t]*\r?\n", "");
		// //System.out.println(text);
		deleteIfExist();
		writeToFile(text);
		PdfValuesForPack infos = readData();
		document.close();
		//System.out.println("Main Method Ended");
		return infos;

	}

	public static void deleteIfExist() {
		try {
			Files.deleteIfExists(Paths.get("C:\\CMS_HOSKOTE\\ProcessOrders\\txt\\temp.txt"));
		} catch (NoSuchFileException e) {
			//System.out.println("No such file/directory exists");
		} catch (DirectoryNotEmptyException e) {
			//System.out.println("Directory is not empty.");
		} catch (IOException e) {
			//System.out.println("Invalid permissions.");
		}

		//System.out.println("Deletion successful.");
	}

	public static void writeToFile(String content) {
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			fw = new FileWriter("C:\\CMS_HOSKOTE\\ProcessOrders\\txt\\temp.txt");
			bw = new BufferedWriter(fw);
			bw.write(content);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public static PdfValuesForPack readData() {
		String processOrderNo = "";
		String packingCode = "";
		String packingGroup = "";
		String plantInformation = "";
		String batchNo = "";
		String batchDate = "";
		String totalQuant = "";
		String volume = "";
		String wgtPkg = "";
		String storage = "";
		String processOrderNote = "";
		String materialMemo = "";
		List<String> rmCodes = new ArrayList<String>();
		List<String> rmName = new ArrayList<String>();
		List<String> rmQuants = new ArrayList<String>();
		List<String> rmTypes =  new ArrayList<String>();
		List<String> rmPhases =new ArrayList<String>(); 

		int lineNo = 0;
		try {
			File file = new File("C:\\CMS_HOSKOTE\\ProcessOrders\\txt\\temp.txt");
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				lineNo++;
				String[] elements = line.split(" ");
				// //System.out.println(elements[0]);
				int length = elements.length;

				if (length == 7) {
					rmCodes.add(elements[1]);

					rmTypes.add(elements[2]);

					rmPhases.add(elements[3]);

					String quant = elements[4] + "" + elements[5];
					rmQuants.add(quant);

					line = bufferedReader.readLine();
					rmName.add(line);
					//System.out.println(elements[0]);
				}

				if (lineNo == 2) {
					processOrderNo = line;
				}

				if (lineNo == 7) {
					elements = line.split(" ");
					//System.out.println(elements[1]);
					packingCode = elements[1];
				}

				if (lineNo == 8) {
					plantInformation = line;
				}

				if (lineNo == 10) {
					packingGroup = line;
				}

				if (lineNo == 14) {
					// //System.out.println(line);
					elements = line.split(" ");
					//System.out.println(elements[2]);
					batchNo = elements[2];
				}

				if (lineNo == 15) {
					// //System.out.println(line);
					elements = line.split(" ");
					//System.out.println(elements[2]);
					batchDate = elements[2];
				}

				if (lineNo == 16) {
					// //System.out.println(line);
					elements = line.split(" ");
					//System.out.println(elements[0]);
					totalQuant = elements[0];
				}

				if (lineNo == 17) {
					// //System.out.println(line);
					elements = line.split(" ");
					//System.out.println(elements[1]);
					volume = elements[1];
				}

				if (lineNo == 18) {
					// //System.out.println(line);
					elements = line.split(" ");
					//System.out.println(elements[1]);
					wgtPkg = elements[1];
				}
				
				if(lineNo == 19){
					elements = line.split(" ");
					//System.out.println("Storage Code =>"+elements[2]);
					storage =elements[2];
					
				}

				if (lineNo == 22) {
					//System.out.println(line);
					elements = line.split(" ");
					//System.out.println(elements[1]);
					processOrderNote = line;
				}
				
				if (lineNo == 24) {
					//System.out.println("Material Memo =>" +line);
					materialMemo = line;
				}

			}
			fileReader.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		//System.out.println("ProcessOrder No =>" + processOrderNo);
		//System.out.println("packingCode No =>" + packingCode);
		//System.out.println("packingGroup No =>" + packingGroup);
		//System.out.println("ProcessOrder No =>" + processOrderNo);
		//System.out.println("Batch No=>" + batchNo);
		//System.out.println("Batch Date=>" + batchDate);
		// //System.out.println("Process Name==>" + productName);
		//System.out.println("Total quant=>" + totalQuant);
		//System.out.println("Volumee=>" + volume);
		//System.out.println("wgtPkg=>" + wgtPkg);
		//System.out.println("processOrderNote=>" + processOrderNote);
		//System.out.println("rmCodes=>" + rmCodes);
		//System.out.println("rmNames=>" + rmName);
		PdfValuesForPack pdfValuesForPack = new PdfValuesForPack(processOrderNo, packingCode, packingGroup,
				plantInformation, batchNo, batchDate, totalQuant, volume, wgtPkg, processOrderNote, rmCodes, rmName,
				rmQuants,rmTypes,rmPhases,materialMemo,storage);

		return pdfValuesForPack;

	}
      
	public static void generateBarCodes160DPI(String fileName) throws IOException {
		fileName = fileName.replaceAll("\\s+", "");
		fileName = fileName.replaceAll(",", "");
		
		//System.out.println("File Name =>" + fileName);
		/*if(fileName.charAt(0) !='5'){
			fileName = fileName.replaceAll(".000", "");
			//System.out.println("After Replace =>" + fileName);
		}*/
		

		try {
			Code39Bean bean39 = new Code39Bean();
			final int dpi = 160;

			// Configure the barcode generator
			bean39.setModuleWidth(UnitConv.in2mm(2.8f / dpi));
			bean39.setWideFactor(2);
			bean39.setMsgPosition(HumanReadablePlacement.HRP_NONE);
			bean39.setBarHeight(6);
			bean39.setDisplayStartStop(true);
			bean39.doQuietZone(false);

			// Open output file
			File outputFile = new File("C:\\CMS_HOSKOTE\\ProcessOrders\\temp\\output\\barcodes\\" + fileName + ".png");

			FileOutputStream out = new FileOutputStream(outputFile);

			// Set up the canvas provider for monochrome PNG output
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi,
					BufferedImage.TYPE_BYTE_BINARY, false, 0);

			// Generate the barcode
			bean39.generateBarcode(canvas, fileName);

			// Signal end of generation
			canvas.finish();

			//System.out.println("Bar Code is generated successfully…");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void generateBarCodesForRmQuantity160DPI(String fileName,int count) throws IOException {
		fileName = fileName.replaceAll("\\s+", "");
		fileName = fileName.replaceAll(",", "");
		
		//System.out.println("File Name =>" + fileName);
		if(count > 1){
			fileName = fileName.replaceAll(".000", "");
			//System.out.println("After Replace =>" + fileName);
		}
		

		try {
			Code39Bean bean39 = new Code39Bean();
			final int dpi = 160;

			// Configure the barcode generator
			bean39.setModuleWidth(UnitConv.in2mm(2.8f / dpi));
			bean39.setWideFactor(2);
			bean39.setMsgPosition(HumanReadablePlacement.HRP_NONE);
			bean39.setBarHeight(6);
			bean39.setDisplayStartStop(true);
			bean39.doQuietZone(false);

			// Open output file
			File outputFile = new File("C:\\CMS_HOSKOTE\\ProcessOrders\\temp\\output\\barcodes\\" + fileName + ".png");

			FileOutputStream out = new FileOutputStream(outputFile);

			// Set up the canvas provider for monochrome PNG output
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi,
					BufferedImage.TYPE_BYTE_BINARY, false, 0);

			// Generate the barcode
			bean39.generateBarcode(canvas, fileName);

			// Signal end of generation
			canvas.finish();

			//System.out.println("Bar Code is generated successfully…");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void generateBarCodes300PI(String fileName) throws IOException {
		fileName = fileName.replaceAll("\\s+", "");
		fileName = fileName.replaceAll(",", "");

		try {
			Code39Bean bean39 = new Code39Bean();
			final int dpi = 300;

			// Configure the barcode generator
			bean39.setModuleWidth(UnitConv.in2mm(3f / dpi));
			bean39.setWideFactor(2);
			bean39.setMsgPosition(HumanReadablePlacement.HRP_NONE);
			bean39.setBarHeight(6);
			bean39.setDisplayStartStop(true);
			bean39.doQuietZone(false);

			// Open output file
			File outputFile = new File("C:\\CMS_HOSKOTE\\ProcessOrders\\temp\\output\\barcodes\\" + fileName + ".png");

			FileOutputStream out = new FileOutputStream(outputFile);

			// Set up the canvas provider for monochrome PNG output
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi,
					BufferedImage.TYPE_BYTE_BINARY, false, 0);

			// Generate the barcode
			bean39.generateBarcode(canvas, fileName);

			// Signal end of generation
			canvas.finish();

			//System.out.println("Bar Code is generated successfully…");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void uploadBarCodes(PdfValuesForPack pdfValues) {
		String packageCode = pdfValues.getPackingCode();
		String processOrderNumber = pdfValues.getProcessOrderNo();
		String batchNumber = pdfValues.getBatchNo();
		List<String> rmCodes = pdfValues.getRmCodes();
		List<String> rmQuants = pdfValues.getRmQuant();
		try {
			generateBarCodes300PI(processOrderNumber);
			generateBarCodes300PI(packageCode);
			generateBarCodes300PI(batchNumber);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			int i = 0;
			for (String rmCode : rmCodes) {
				if (i == 0) {
					generateBarCodes300PI(rmCode);
				} else {
					generateBarCodes160DPI(rmCode);
				}
				i++;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			int count=0;
			for (String rmQuant : rmQuants) {
				count++;
				generateBarCodesForRmQuantity160DPI(rmQuant,count);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}