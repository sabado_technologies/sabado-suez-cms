package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.entity.User;
import com.exception.ValidationExceptions;
import com.request.UserRequest;
import com.response.Response;
import com.service.LoginService;
import com.validation.LoginValidation;

@RestController
@RequestMapping("/loginController")
public class LoginController {
	@Autowired
	LoginService loginService;
	@Autowired
	LoginValidation loginValidation;

	@RequestMapping(value = "login", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getRole(@RequestBody UserRequest user) throws Exception {
		List<User> users = null;
		try {
			loginValidation.getRole(user);
		} catch (ValidationExceptions ex) {
			ex.printStackTrace();
			return new Response("400", ex.getMessage());
		}
		
		try {
			users = loginService.getRole(user);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
	     for(User u: users )
	     {
	    	if(u.getStatus().getStatusId() == 12)
	    	{
	    		return new Response("300", u.getSsoid());	
	    	}	
	    	 
	     }	 
		return new Response("200", users);
	}

	
}