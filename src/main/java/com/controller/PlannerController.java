package com.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.request.PlannerRequest;
import com.response.Response;
import com.service.PlannerService;

@RestController
@RequestMapping("/planner")
public class PlannerController {
	
	@Autowired
	PlannerService plannerService;
	
	@RequestMapping(value = "/addPlanner", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response editUser( @RequestBody PlannerRequest planner) throws Exception {
		int planner1 = 0;
		int reportPlan = 0;
		PlannerRequest existingPlan=null;
		String convertedDates=null;
		List<String> datesForReport = new ArrayList<String>();
		try{
			existingPlan = plannerService.findPlan(planner.getYear(), planner.getQuarter());
			if(existingPlan.getPerDay() == null){
				datesForReport = plannerService.changeFormat(planner.getDates());
				convertedDates=plannerService.addDates(planner.getDates());								
				planner.setSelectedDates(convertedDates);
				reportPlan = plannerService.addPlanForReport(datesForReport, planner);
				planner1=plannerService.addPlan(planner);
			}else{
				datesForReport = plannerService.changeFormat(planner.getDates());
				convertedDates=plannerService.addDates(planner.getDates());				
				planner.setSelectedDates(convertedDates);
				////System.out.println("Dates After =>" +planner.getSelectedDates());
				planner1=plannerService.updatePlan(planner);
				reportPlan = plannerService.deletePlan(planner);
				reportPlan = plannerService.addPlanForReport(datesForReport, planner);
				
			}
			
		}catch(Exception ex){
			return new Response("400", ex.getMessage());
		}
		return new Response("200", planner1);
		
		
	}
	
	@RequestMapping(value = "/getPlannedDates/{year}/{quarter}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPlannedDates( @PathVariable int year,@PathVariable int quarter ) throws Exception {
		
		List<String> plannedDates=new ArrayList<String>();
		PlannerRequest planner =new PlannerRequest();
		
		List<String> plannedDates1=null;
		plannedDates1 =new ArrayList<String>();
		plannedDates1=Collections.emptyList();
		List<Date> dates =new ArrayList<Date>();
		dates = null;
		try{
			planner = plannerService.findPlan(year, quarter);
			plannedDates=planner.getWorkingDates();
			
		} catch(Exception ex){
			return new Response("400", ex.getMessage());
		}
		try{
			if(plannedDates==null){
				return new Response("400",plannedDates1);
				
			}
			dates=plannerService.convertDate(plannedDates);
			planner.setDates(dates);
			
		} catch(Exception ex){
			return new Response("400", ex.getMessage());
		}
		return new Response("200", planner);
		
		
	}

}
