package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Alerts;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.NotificationCounterResponse;
import com.service.AlertsService;

@RestController
@RequestMapping("/RoleBasedAlerts")
public class AlertsController {
	@Autowired
	AlertsService alertsService;
	
	@RequestMapping(value = "/alertsForAdmin", produces = "application/json", method = RequestMethod.GET)
	public List<Alerts> alertsForAdmin() {
		List<Alerts> alertsforAdmin = alertsService.alertsForAdmin();
		return alertsforAdmin;
	}
	
	
	@RequestMapping(value = "/alertsForWarehouse", produces = "application/json", method = RequestMethod.GET)
	public List<Alerts> alertsForWarehouse() {
		List<Alerts> alertsforWarehouse = alertsService.alertsForWarehouse();
		return alertsforWarehouse;
	}
	
	
	@RequestMapping(value = "/statusChangeForAdmin/{processOrderNumber}/{statusId}", produces = "application/json", method = RequestMethod.GET)
	public int statusChangeForAdmin(@PathVariable String processOrderNumber,@PathVariable int statusId ) {
		return  alertsService.statusChangeForAdmin(processOrderNumber,statusId);
		
	}
	
	@RequestMapping(value = "/deleteProductionOrder/{processorderProdNumber}/{remarks}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int deleteProductionOrder(@PathVariable Long processorderProdNumber,@PathVariable String remarks ,@PathVariable String ssoId ) {
		return  alertsService.deleteProductionOrder(processorderProdNumber,remarks ,ssoId);
		
	}
	
	@RequestMapping(value = "/processOrdersForDeletion", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> processOrdersForDeletion() {
		List<ProcessorderProd> processOrdersForDeletion = alertsService.processOrdersForDeletion();
		return processOrdersForDeletion;
	}
	
	@RequestMapping(value = "/deletePackagingOrder/{processorderPckgNumber}/{remarks}/{ssoId}", produces = "application/json", method = RequestMethod.POST)
	public int deletePackagingOrder(@PathVariable Long processorderPckgNumber,@PathVariable String remarks ,@PathVariable String ssoId ) {
		return  alertsService.deletePackagingOrder(processorderPckgNumber,remarks ,ssoId);
		
	}
	
	@RequestMapping(value = "/packagingOrdersForDeletion", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> packagingOrdersForDeletion() {
		List<ProcessorderPckg> processOrdersForDeletion = alertsService.packagingOrdersForDeletion();
		return processOrdersForDeletion;
	}
	
	@RequestMapping(value = "/smsAlertForWHInventory/{processOrderNumber}/{ssoId}", produces = "application/json", method = RequestMethod.GET)
	public int smsAlertForWHInventory(@PathVariable String processOrderNumber,@PathVariable String ssoId ) {
		return  alertsService.smsAlertForWHInventory(processOrderNumber,ssoId);
		
	}
	
	@RequestMapping(value = "/smsAlertForCharging/{processOrderNumber}/{ssoId}", produces = "application/json", method = RequestMethod.GET)
	public int smsAlertForCharging(@PathVariable String processOrderNumber,@PathVariable String ssoId ) {
		return  alertsService.smsAlertForCharging(processOrderNumber,ssoId);
		
	}
	
	@RequestMapping(value = "/notificationCountForPo", produces = "application/json", method = RequestMethod.GET)
    public NotificationCounterResponse notificationCountForPo() {
		return alertsService.notificationCountForPo();
    }
	

}
