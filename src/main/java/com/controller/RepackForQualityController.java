package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ProcessorderPckg;
import com.response.Response;
import com.service.RepackForQualityService;

@RestController
@RequestMapping("/repackQuality")
public class RepackForQualityController {
	
	@Autowired
	RepackForQualityService repackForQualityService;
	
	@RequestMapping(value = "/getRepackDetails", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getRepackDetails() throws Exception {
		List<ProcessorderPckg> productDetails = null;
		
		try {
			productDetails = repackForQualityService.getRepackDetails();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", productDetails);
	}
	
	@RequestMapping(value = "/acceptQuality/{processorderProdNumber}/{ssoid}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response acceptQuality( @PathVariable long processorderProdNumber,@PathVariable String ssoid) throws Exception {
		Long packages = null;
		try {
			packages = repackForQualityService.acceptQuality(processorderProdNumber,ssoid);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packages);
	}
	
		@RequestMapping(value = "/rejectQuality/{processorderProdNumber}/{ssoid}/{comment}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
				MediaType.TEXT_PLAIN_VALUE })
	
		public @ResponseBody Response rejectQuality( @PathVariable long processorderProdNumber,@PathVariable String ssoid,@PathVariable String comment) throws Exception {
			Long packages = null;
			try {
				packages = repackForQualityService.rejectQuality(processorderProdNumber,ssoid, comment);
			} catch (Exception ex) {
				return new Response("400", ex.getMessage());
			}
			return new Response("200", packages);
		}
		
		@RequestMapping(value = "/scrapQuality/{processorderProdNumber}/{ssoid}/{comment}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
				MediaType.TEXT_PLAIN_VALUE })
	
		public @ResponseBody Response scrapQuality( @PathVariable long processorderProdNumber,@PathVariable String ssoid,@PathVariable String comment) throws Exception {
			Long packages = null;
			try {
				packages = repackForQualityService.scrapQuality(processorderProdNumber, ssoid, comment);
			} catch (Exception ex) {
				return new Response("400", ex.getMessage());
			}
			return new Response("200", packages);
		}

}
