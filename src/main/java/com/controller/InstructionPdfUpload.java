package com.controller;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.entity.ProcessorderProd;
import com.response.Response;

@RestController
@RequestMapping("/instructionFile")
public class InstructionPdfUpload {
	


	@Autowired
	ServletContext context;

	@RequestMapping(value = "/instructionPdfFileUpload", method = RequestMethod.POST)
	/* TODO:Remove all the hard path to properties file */
	public Response continueFileUpload(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Response responseD = new Response("200", "Success");
		//Response responseD1 = new Response("400", "Error");
		MultipartHttpServletRequest mRequest;
		String filename = "output.pdf";
		ProcessorderProd orders =null;
		String productionOrder=null;
		try {
			mRequest = (MultipartHttpServletRequest) request;
			mRequest.getParameterMap();

			Iterator itr = mRequest.getFileNames();

			while (itr.hasNext()) {
				MultipartFile mFile = mRequest.getFile((String) itr.next());
				String fileName = mFile.getOriginalFilename();				
				//System.out.println("PDF file Name =>" +fileName);
				fileName=fileName.substring(0, fileName.length()-4);
				//System.out.println("PDF file Name after Triming =>" +fileName);
				productionOrder = fileName;
				//System.out.println("po =>" + po + " &processDate=>" + processDate);
				java.nio.file.Path pathForOutput = Paths.get("C:/CMS_HOSKOTE/ProcessOrders/temp/output/instructionPdf/" + fileName + ".pdf");
				InputStream in2 = mFile.getInputStream();
				Files.deleteIfExists(pathForOutput);
				Files.copy(in2, pathForOutput);
			}
		} catch (Exception e) {
			responseD.setCode("400");
			responseD.setData(e);
		}
		responseD.setCode("200");
		responseD.setData(productionOrder);
		return responseD;
	}

}
