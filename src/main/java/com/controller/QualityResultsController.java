package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.Response;
import com.service.QualityResultsService;

@RestController
@RequestMapping("/qualityResults")
public class QualityResultsController {
	
	@Autowired
	QualityResultsService qualityResultsService;
	
	@RequestMapping(value = "/getReWorkedProcessOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getReWorkedProcessOrders() throws Exception {
		//System.out.println("jjsnsjxsjnj");
		List<ProcessorderProd> processOrders = null;
		
		try {
			processOrders = qualityResultsService.getReworkDetails();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", processOrders);
	}
	
	@RequestMapping(value = "/getReworkPackingDetails/{processOrder}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getReworkPackingDetails(@PathVariable long processOrder) throws Exception {
		//System.out.println("jjsnsjxsjnj");
		List<ProcessorderPckg> processOrders = null;
		
		try {
			processOrders = qualityResultsService.getPackingDetails(processOrder);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", processOrders);
	}
	
	@RequestMapping(value = "/getScrapDetails", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getScrapDetails() throws Exception {
		//System.out.println("jjsnsjxsjnj");
		List<ProcessorderProd> processOrders = null;
		
		try {
			processOrders = qualityResultsService.getScrapDetails();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", processOrders);
	}
	
	@RequestMapping(value = "/approveRework/{processOrder}/{remarks}/{ssoid}", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response approveRework(@PathVariable long processOrder, @PathVariable String remarks, @PathVariable String ssoid) throws Exception {
		//System.out.println("jjsnsjxsjnj");
		List<ProcessorderProd> processOrders = null;
		long productionOrder;
		
		try {
			productionOrder = qualityResultsService.approve(processOrder, remarks, ssoid);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", productionOrder);
	}

}
