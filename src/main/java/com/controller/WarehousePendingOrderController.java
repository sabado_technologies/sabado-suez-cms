package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ProcessorderProd;
import com.entity.Reactor;
import com.entity.User;
import com.response.Response;
import com.response.StatusResponse;
import com.service.WarehousePendingService;


@RestController
@RequestMapping("/wareHousepending")
public class WarehousePendingOrderController {
	
	@Autowired
	WarehousePendingService pendingOrdersService; 
	
	@RequestMapping(value = "/getPendingOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getAllUsers() throws Exception {
		List<ProcessorderProd> products = null;
		
		try {
			products = pendingOrdersService.getPendingOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", products);
	}
	
	@RequestMapping(value = "/updateReactor", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response updateReactor(@RequestBody Reactor reactor) throws Exception {
		String reactorName = null;
		
		try {
			reactorName = pendingOrdersService.UpdateReactor(reactor);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", reactorName);
	}
	
	@RequestMapping(value = "/getReactors", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getReactors() throws Exception {
		List<Reactor> reactors = null;
		
		try {
			reactors = pendingOrdersService.getAllReactors();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", reactors);
	}
	
	@RequestMapping(value = "/getAllOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getAllOrders() throws Exception {
		List<ProcessorderProd> orders = null;
		
		try {
			orders = pendingOrdersService.getAllOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", orders);
	}
	
	@RequestMapping(value = "/getTodaysOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getTodaysOrders() throws Exception {
		List<ProcessorderProd> todays = null;
		
		try {
			todays = pendingOrdersService.getTodaysOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", todays);
	}
	
	@RequestMapping(value = "/getPreviousOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPreviousOrders() throws Exception {
		List<ProcessorderProd> previous = null;
		
		try {
			previous = pendingOrdersService.getPreviousOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", previous);
	}
	
	@RequestMapping(value = "/getFutureOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getFutureOrders() throws Exception {
		List<ProcessorderProd> future = null;
		
		try {
			future = pendingOrdersService.getFutureOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", future);
	}
	
	@RequestMapping(value = "/getTodaysCompletedOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getTodaysCompletedOrders() throws Exception {
		List<ProcessorderProd> completed = null;
		
		try {
			completed = pendingOrdersService.getTodaysCompletedOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", completed);
	}
	
	@RequestMapping(value = "/getStatus/{processorderProdNumber}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getStatus(@PathVariable long processorderProdNumber) throws Exception {
		StatusResponse status =null;
		
		try {
			status=pendingOrdersService.getStatus(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", status);
	}
	
	@RequestMapping(value = "/getChargedStatus/{processorderProdNumber}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getChargedStatus(@PathVariable long processorderProdNumber) throws Exception {
		StatusResponse status =null;
		
		try {
			status=pendingOrdersService.getChargedStatus(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", status);
	}
	
	@RequestMapping(value = "/getPackedStatus/{processorderProdNumber}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPackedStatus(@PathVariable long processorderProdNumber) throws Exception {
		StatusResponse status =null;
		
		try {
			status=pendingOrdersService.getPackedStatus(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", status);
	}
	
	@RequestMapping(value = "/getQualityCheckStatus/{processorderProdNumber}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getQualityCheckStatus(@PathVariable long processorderProdNumber) throws Exception {
		StatusResponse status =null;
		
		try {
			status=pendingOrdersService.getQualityCheckStatus(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", status);
	}
	
	@RequestMapping(value = "/getQualityInspectionStatus/{processorderProdNumber}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getQualityInspectionStatus(@PathVariable long processorderProdNumber) throws Exception {
		StatusResponse status =null;
		
		try {
			status=pendingOrdersService.getQualityInspectionStatus(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", status);
	}
	
	@RequestMapping(value = "/getInventoryStatus/{processorderProdNumber}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getInventoryStatus(@PathVariable long processorderProdNumber) throws Exception {
		StatusResponse status =null;
		
		try {
			status=pendingOrdersService.getInventoryCheckStatus(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", status);
	}
	
	@RequestMapping(value = "/getRmInventoryStatus/{processorderProdNumber}", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getRmInventoryStatus(@PathVariable long processorderProdNumber) throws Exception {
		StatusResponse status =null;
		
		try {
			status=pendingOrdersService.getRmInventoryStatus(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", status);
	}

}
