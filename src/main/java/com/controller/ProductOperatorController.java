package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.entity.Alerts;
import com.entity.MultiRoles;
import com.entity.ProcessTracker;
import com.entity.ProcessorderAssociatedPo;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.QualityCheckResult;
import com.entity.Reactor;
import com.entity.Remark;
import com.entity.Role;
import com.entity.User;
import com.response.ManagementReportResponse;
import com.response.ProcessOrderRMResponse;
import com.response.QualityCheckResponce;
import com.response.RepackingOrderRMResponse;
import com.service.ProductOperatorService;

@RestController
@RequestMapping("/po")
public class ProductOperatorController {

	@Autowired
	private ProductOperatorService productOperatorService;

	//////////// all process order///////////////////////////
	@RequestMapping(value = "/process", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> displayProcessOrders() {

		return productOperatorService.displayProcessOrders();
	}

	//////////// process order Recipe///////////////////////////
	@RequestMapping(value = "/processDetails/{a}", produces = "application/json", method = RequestMethod.POST)
	public List<ProcessOrderRMResponse> processOrderRecipeList(@PathVariable long a) {

		return productOperatorService.processOrderRecipeList(a);
	}

	//////////// associated process order///////////////////////////
	@RequestMapping(value = "/associatedPackingInfo/{a}", produces = "application/json", method = RequestMethod.POST)
	public List<ProcessorderPckg> associatedPackingInfo(@PathVariable long a) {

		return productOperatorService.processOrderForPacking(a);
	}

	//////////// packing orders ///////////////////////////
	@RequestMapping(value = "/PackingOrderInfo/{a}", produces = "application/json", method = RequestMethod.POST)
	public List<ProcessorderPckg> PackingOrderInfo(@PathVariable long a) {

		return productOperatorService.PackingOrderInfo(a);
	}

	//////////// status update///////////////////////////
	@RequestMapping(value = "/updateStatus/{processOrder}/{status}/{sso}", produces = "application/json", method = RequestMethod.PUT)
	public int updateStatus(@PathVariable Long processOrder, @PathVariable int status, @PathVariable String sso) {

		return productOperatorService.updateStatus(processOrder, status, sso);
	}

	///////////// status for process order RM////////////////
	@RequestMapping(value = "/updateStatusForRM/{processOrder}/{RMNumber}/{status}", produces = "application/json", method = RequestMethod.PUT)
	public int updateStatusForRM(@PathVariable Long processOrder, @PathVariable int RMNumber,
			@PathVariable int status) {

		return productOperatorService.updateStatusForRM(processOrder, RMNumber, status);
	}

	//////////// status update for packing///////////////////////////
	@RequestMapping(value = "/updateStatusForPacking/{processOrder}/{status}/{sso}/{quantity}/{pails}/{remarks}", produces = "application/json", method = RequestMethod.PUT)
	public int updateStatusForPacking(@PathVariable Long processOrder, @PathVariable int status, @PathVariable String sso, @PathVariable String quantity, @PathVariable int pails, @PathVariable String remarks) {
       
         int pails1 = 0;
		return productOperatorService.updateStatusForPacking(processOrder, status, sso, quantity, pails, remarks );
	}

	////////////// inserting in process tracker//////////////////////
	@RequestMapping(value = "/updateProcessStatus", produces = "application/json", method = RequestMethod.POST)
	public int updateProcessProgress(@RequestBody ProcessTracker processStatus) {

		return productOperatorService.updateProcessProgress(processStatus);
	}

	//////////// list of all the po///////////////////////////
	@RequestMapping(value = "/listofpo", produces = "application/json", method = RequestMethod.GET)
	public List<User> listofPo() {

		return productOperatorService.listofPo();
	}

	//////////// daily process order///////////////////////////
	@RequestMapping(value = "/dailyProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> AllProcessOrderList() {

		return productOperatorService.AllProcessOrderList();
	}

	//////////// weekly process order//////////////////////////
	@RequestMapping(value = "/weeklyProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> getWeeklyProcessOrders() {
		List<ManagementReportResponse> weekWiseReport = productOperatorService.getWeeklyProcessOrders();
		return weekWiseReport;
	}

	//////////// save process order associated with po///////////////////////////
	@RequestMapping(value = "/SavePoProcessOrderDetails", produces = "application/json", method = RequestMethod.POST)
	public int SaveprocessAssociatedwithpo(@RequestBody List<ProcessorderAssociatedPo> list) {
		return productOperatorService.SaveprocessAssociatedwithpo(list);
	}

	/////////////// get quality info//////////////////////////
	@RequestMapping(value = "/getQualityMaster/{ProductNo}", produces = "application/json", method = RequestMethod.POST)
	public List<QualityCheckResponce> getQualityData(@PathVariable long ProductNo) {

		return productOperatorService.getQualityData(ProductNo);
	}

	//////////// save quality result///////////////////////////
	@RequestMapping(value = "/SaveQualityCheckData", produces = "application/json", method = RequestMethod.POST)
	public int SaveQualityInfo(@RequestBody List<QualityCheckResponce> list) {

		return productOperatorService.SaveQualityInfo(list);
	}

	//////////// update process order table reactor and sift///////////////////////////
	@RequestMapping(value = "/updateProcessOrderProd/{processOrder}/{reactor}/{shift}", produces = "application/json", method = RequestMethod.PUT)
	public int updateProcessOrderProd(@PathVariable Long processOrder, @PathVariable String reactor,
			@PathVariable int shift) {

		return productOperatorService.updateProcessOrderProd(processOrder, reactor, shift);
	}

	///////////////// process order inventory///////////////////////
	@RequestMapping(value = "/processOrderForInventory", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> displayProcessOrdersforInventory() {

		return productOperatorService.displayProcessOrdersforInventory();
	}

	//////////// daily process order done details///////////////////////////
	@RequestMapping(value = "/dailyProcessOrder", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> dailyProcessOrder() {

		return productOperatorService.dailyProcessOrder();
	}
	
    //////////////////save alert /////////////////////
	@RequestMapping(value = "/SaveAlert", produces = "application/json", method = RequestMethod.POST)
	public int saveAlert(@RequestBody Alerts alert) {
		//System.out.println("hiii form alert");
		return productOperatorService.saveAlert(alert);
	}
	
    //////////////////Reactor info /////////////////////
	@RequestMapping(value = "/ReactorInfo", produces = "application/json", method = RequestMethod.GET)
	public List<Reactor> getReactorDetails() {
		
		return productOperatorService.getReactorDetails();
	}
	
    //////////////////List of role /////////////////////
	@RequestMapping(value = "/RoleList/{SSO}", produces = "application/json", method = RequestMethod.GET)
	public List<MultiRoles> getRoleList(@PathVariable String SSO) {
		
		return productOperatorService.getRoleList(SSO);
	}
	
    //////////////////insert remark /////////////////////
	@RequestMapping(value = "/insertRemark", produces = "application/json", method = RequestMethod.POST)
	public int InsertRemark(@RequestBody Remark remark ) {
		
		//System.out.println("nitiiitiiititiitit"+remark.getProcessOrder());
		return productOperatorService.InsertRemark(remark);
	}
	
    //////////////////Update End time on tracker /////////////////////
	@RequestMapping(value = "/updateEndTime/{processOrder}/{status}", produces = "application/json", method = RequestMethod.PUT)
	public int updateTracker(@PathVariable Long processOrder, @PathVariable int status  ) {
		
	
		return productOperatorService.updateTracker(processOrder,status);
	}
	
	//////////// all repacking order///////////////////////////
	@RequestMapping(value = "/reapckingOrder", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> reapckingOrderDiaplay() {

		return productOperatorService.reapckingOrderDiaplay();
	}
	
	//////////// save process order associated with po///////////////////////////
	@RequestMapping(value = "/SaveprocessAssociatedwithpoForRepack", produces = "application/json", method = RequestMethod.POST)
	public int SaveprocessAssociatedwithpoForRepack(@RequestBody List<ProcessorderAssociatedPo> list) {
		return productOperatorService.SaveprocessAssociatedwithpoForRepack(list);
	}
	
	////////////// inserting in QualityResult Table//////////////////////
	@RequestMapping(value = "/insertQualityResult", produces = "application/json", method = RequestMethod.POST)
	public int insertQualityResult(@RequestBody QualityCheckResult qualityCheckResult) {

		return productOperatorService.insertQualityResult(qualityCheckResult);
	}
	
	
	//////////// associated process order///////////////////////////
	@RequestMapping(value = "/processOrderDetailsForQuality/{a}", produces = "application/json", method = RequestMethod.POST)
	public List<ProcessorderProd> processOrderDetailsForQuality(@PathVariable long a) {

		return productOperatorService.processOrderDetailsForQuality(a);
	}
	
	///////////////// process order inventory///////////////////////
	@RequestMapping(value = "/RepackingprocessOrderForInventory", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderPckg> displayProcessOrdersforInventoryForRepacking() {

		return productOperatorService.displayProcessOrdersforInventoryForRepacking();
	}
	
	//////////// REpacking  Recipe///////////////////////////
	@RequestMapping(value = "/RepackingprocessDetails/{a}", produces = "application/json", method = RequestMethod.POST)
	public List<RepackingOrderRMResponse> RepackingOrderRecipeList(@PathVariable long a) {

		return productOperatorService.RepackingOrderRecipeList(a);
	}

	//////////// REpacking  Status change///////////////////////////
	@RequestMapping(value = "/RepackingOrderStatusChange/{RepackingOrder}/{status}/{sso}", produces = "application/json", method = RequestMethod.PUT)
	public int RepackingOrderStatusChange(@PathVariable long RepackingOrder, @PathVariable int status, @PathVariable String sso) {

		return productOperatorService.RepackingOrderStatusChange(RepackingOrder,status,sso);
	} 
	
    
	//////////// REpacking  Recipe///////////////////////////
	@RequestMapping(value = "/RepackingQualityProcessDetails/{a}", produces = "application/json", method = RequestMethod.POST)
	public List<ProcessorderPckg> RepackingOrderRecipee(@PathVariable long a) {

		return productOperatorService.RepackingOrderRecipee(a);
	}
	
	@RequestMapping(value = "/insertPackingStartTime/{processOrder}", produces = "application/json", method = RequestMethod.POST)
	public int insertPackingStartTime(@PathVariable long processOrder) {

		return productOperatorService.insertPackingStartTime(processOrder);
	}

}
