package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ProcessorderPckg;
import com.response.Response;
import com.service.PackingOrderInfoService;

@RestController
@RequestMapping("/packingInfo")
public class PackingOrderInfoController {
	
	@Autowired
	PackingOrderInfoService packingOrderInfoService;
	
	@RequestMapping(value = "/getAllPackingInfo/{processOrderNumber}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getAllPackingInfo(@PathVariable long processOrderNumber) throws Exception {
		List<ProcessorderPckg> packingInfo = null;
		
		try {
			packingInfo = packingOrderInfoService.getPackingInfo(processOrderNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packingInfo);
	}

}
