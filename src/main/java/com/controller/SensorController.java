package com.controller;


import java.io.IOException;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.service.SensorService;


@RestController
@RequestMapping("/sensor")
public class SensorController {
	
	@Autowired
	SensorService sensorService;

	@RequestMapping(value = "/tempdata/{tempsensorno}", produces = "application/json",method = RequestMethod.GET)
	public String TemparatureSensor(@PathVariable int tempsensorno) throws IOException {
		
		
		return sensorService.TemparatureSensor(tempsensorno);
		
	}
	
	
	@RequestMapping(value = "/weight/{weightsensorno}", produces = "application/json",method = RequestMethod.GET)
	public String weightSensor(@PathVariable int weightsensorno) throws IOException {
		
		
		return sensorService.weightSensor(weightsensorno);
/*		
		 Random randomGenerator = new Random();
		      int randomInt = randomGenerator.nextInt(100);
		
		return  randomInt;*/
	}
	

	
}
