package com.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.EmailAlerts;
import com.entity.Role;
import com.response.EmailAlertResponce;
import com.response.Response;
import com.service.EmailAlertReportService;

@RestController
@RequestMapping("/emailAlert")
public class EmailAlertReportController {
	
	
	@Autowired
	private EmailAlertReportService emailAlertReportService;

	//////////// all process order///////////////////////////
	@RequestMapping(value = "/AllData", produces = "application/json", method = RequestMethod.GET)
	public List<EmailAlertResponce> getEmailAlert() {

		return emailAlertReportService.getEmailAlert();
	}
	
	/*//////////// all process order///////////////////////////
	@RequestMapping(value = "/AllDataWithRange/{fromDate}/{toDate}", produces = "application/json", method = RequestMethod.GET)
	public List<EmailAlertResponce> getEmailAlertWithRange(@PathVariable Date fromDate,@PathVariable Date toDate) {
         
      
		return emailAlertReportService.getEmailAlertWithRange(fromDate , toDate);
	}*/
	
	@RequestMapping(value = "/AllDataWithRange/{fromDate}/{toDate}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getEmailAlertWithRange(@PathVariable Date fromDate,@PathVariable Date toDate) throws Exception {
		List<EmailAlertResponce> alerts = null;
		
		try {
			alerts = emailAlertReportService.getEmailAlertWithRange(fromDate , toDate);;
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", alerts);
	}
	
	@RequestMapping(value = "/getPackingAlerts", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPackingAlerts() throws Exception {
		List<EmailAlertResponce> alerts = null;
		
		try {
			alerts = emailAlertReportService.getPackingAlerts();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", alerts);
	}
	
	@RequestMapping(value = "/getPackingAlertsWithDate/{fromDate}/{toDate}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPackingAlertsWithDate(@PathVariable Date fromDate,@PathVariable Date toDate) throws Exception {
		List<EmailAlertResponce> alerts = null;
		
		try {
			alerts = emailAlertReportService.getPackingAlertsByDate(fromDate,toDate);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", alerts);
	}
	
	@RequestMapping(value = "/getTemperature/{processOrder}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getTemperature(@PathVariable long processOrder ) throws Exception {
		List<EmailAlertResponce> alerts = null;
		
		try {
			alerts = emailAlertReportService.getTemperature(processOrder);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", alerts);
	}
	
	@RequestMapping(value = "/getRmTemperature/{processOrder}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getRmTemperature(@PathVariable long processOrder ) throws Exception {
		List<EmailAlertResponce> alerts = null;
		
		try {
			alerts = emailAlertReportService.getRmTemperatureDetails(processOrder);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", alerts);
	}

}
