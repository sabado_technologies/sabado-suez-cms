package com.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ProcessorderProd;
import com.highchart.CurrentQuarterReportResponse;
import com.highchart.CurrentQuarterResponse;
import com.highchart.ProductForChart;
import com.highchart.YearlyBarchartResponse;
import com.highchart.YearlyMonthWiseResponse;
import com.response.AverageCycleTimeResponse;
import com.response.CurrentQuarterWeeklyResponse;
import com.response.CurrentWeekPlannedResponse;
import com.response.CycleTimeResponse;
import com.response.FullQuarterReportResponse;
import com.response.ManagementQuarterProductionPlan;
import com.response.ManagementReportResponse;
import com.response.MonthlyResponse;
import com.response.PackingReportResponse;
import com.response.QuarterBarchartResponse;
import com.response.QuarterBarchartResponse1;
import com.response.RepackingReportResponse;
import com.response.YearlyReportResponse;
import com.service.ManagementService;

@RestController
@RequestMapping(value = "/Management")
public class ManagementController {
	@Autowired
	ManagementService managementservice;
	
	//Daily process Order Status

	@RequestMapping(value = "/dailyProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ProcessorderProd> getDailyProcessOrders() {
		List<ProcessorderProd> dailyprocessorders = managementservice.getDailyProcessOrders();
		return dailyprocessorders;
	}
//------------------------------------------------------------------------------------------------------------------
	// CurrentWeek process Order Status

	@RequestMapping(value = "/weeklyProcessOrders", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> getWeeklyProcessOrders() {
		List<ManagementReportResponse> weekWiseReport = managementservice.getWeeklyProcessOrders();
		return weekWiseReport;
	}
	@RequestMapping(value = "/PlannedWeeklyProduction", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> getWeeklyProductionValue() {
		List<ManagementReportResponse> plannedProductwiseReport = managementservice.getWeeklyProductionValue();
		return plannedProductwiseReport;
	}
	
	/*@RequestMapping(value="/currentWeekDates",produces="application/json",method=RequestMethod.GET)
	public CurrentWeekPlannedResponse getCurrentWeekDates(){
		return managementservice.getCurrentWeekDates();
	}
	*/
	
//-----------------------------------------------------------------------------------------------------------------
	
	//For Current Year Product Report
	
	@RequestMapping(value = "/productwiseReport", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> productWisereport() {
		List<ManagementReportResponse> productwiseReport = managementservice.productWisereport();
		return productwiseReport;
	}
	
	@RequestMapping(value = "/totalPacketCounts", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> getPacketSizeCounts() {
		List<ManagementReportResponse> packetCounts = managementservice.getPacketSizeCounts();
		return packetCounts;
	}

	@RequestMapping(value = "/datewiseProductReport/{productnumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> dateWiseProductReport(@PathVariable Long productnumber) {
		List<ManagementReportResponse> datewiseproductReport = managementservice.dateWiseProductReport(productnumber);
		return datewiseproductReport;
	}

	@RequestMapping(value = "/getAll1MTReactors/{productnumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> getAll1MTReactors(@PathVariable Long productnumber) {
		List<ManagementReportResponse> datewiseproductReport = managementservice.getAll1MTReactors(productnumber);
		return datewiseproductReport;
	}
	
	@RequestMapping(value = "/getAll5MTReactors/{productnumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementReportResponse> getAll5MTReactors(@PathVariable Long productnumber) {
		List<ManagementReportResponse> datewiseproductReport = managementservice.getAll5MTReactors(productnumber);
		return datewiseproductReport;
	}
	//-------------------------------------------------------------------------------------------------------------------
	// multiple year product report By selecting Year
		@RequestMapping(value = "/productwiseReport/{year}", produces = "application/json", method = RequestMethod.GET)
		public List<ManagementReportResponse> productWisereport(@PathVariable int year) {
			List<ManagementReportResponse> productwiseReport = managementservice.productWisereport(year);
			return productwiseReport;
		}
		
		@RequestMapping(value = "/PacketSizeCountsForYear/{year}", produces = "application/json", method = RequestMethod.GET)
		public List<ManagementReportResponse> getPacketSizeCountsForYear(@PathVariable int year) {
			List<ManagementReportResponse> packetCounts = managementservice.getPacketSizeCountsForYear(year);
			return packetCounts;
		}

		@RequestMapping(value = "/dateWiseProductReportYearwise/{productnumber}/{year}", produces = "application/json", method = RequestMethod.GET)
		public List<ManagementReportResponse> dateWiseProductReportYearwise(@PathVariable Long productnumber,@PathVariable int year) {
			List<ManagementReportResponse> datewiseproductReport = managementservice.dateWiseProductReportYearwise(productnumber,year);
			return datewiseproductReport;
		}
		
		@RequestMapping(value = "/getAll1MTReactors/{productnumber}/{year}", produces = "application/json", method = RequestMethod.GET)
		public List<ManagementReportResponse> getAll1MTReactors(@PathVariable Long productnumber,@PathVariable int year) {
			List<ManagementReportResponse> datewiseproductReport = managementservice.getAll1MTReactors(productnumber,year);
			return datewiseproductReport;
		}
		
		@RequestMapping(value = "/getAll5MTReactors/{productnumber}/{year}", produces = "application/json", method = RequestMethod.GET)
		public List<ManagementReportResponse> getAll5MTReactors(@PathVariable Long productnumber,@PathVariable int year) {
			List<ManagementReportResponse> datewiseproductReport = managementservice.getAll5MTReactors(productnumber,year);
			return datewiseproductReport;
		}
//-----------------------------------------------------------------------------------------------------------
	
	//Current Year Current Quarter production Report
	
	@RequestMapping(value = "/currentQuaterProductionReport", produces = "application/json", method = RequestMethod.GET)
	public CurrentQuarterReportResponse currentQuaterProductionReport() {
		CurrentQuarterReportResponse quaterProduction = managementservice.currentQuarterReportResponse();
		return quaterProduction;
	}

	@RequestMapping(value = "/currentQuaterProductionPlan", produces = "application/json", method = RequestMethod.GET)
	public ManagementQuarterProductionPlan currentQuaterProductionPlan() {
		ManagementQuarterProductionPlan quaterProductionPlan = managementservice.currentQuaterProductionPlan();
		return quaterProductionPlan;
	}
//-----------------------------------------------------------------------------------------------------------------------
	
//Current Year Production Report
	
	@RequestMapping(value = "/MonthlyProductionProductReports", produces = "application/json", method = RequestMethod.GET)
	public YearlyMonthWiseResponse MontlyProductionProductWiseReport() {
		YearlyMonthWiseResponse ymr = new YearlyMonthWiseResponse();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		MonthlyResponse monthlyResponses = managementservice.MontlyProductionProductWiseReport();
		Float monthlyProduction = managementservice.getMonthlyProductionvalue().getWeeklySum();

		List<Double> actualValues = new ArrayList<Double>();
		List<Double> plannedValues = new ArrayList<Double>();
		actualValues.add(Double.valueOf(monthlyResponses.getJan()));
		actualValues.add(Double.valueOf(monthlyResponses.getFeb()));
		actualValues.add(Double.valueOf(monthlyResponses.getMar()));
		actualValues.add(Double.valueOf(monthlyResponses.getApr()));
		actualValues.add(Double.valueOf(monthlyResponses.getMay()));
		actualValues.add(Double.valueOf(monthlyResponses.getJune()));
		actualValues.add(Double.valueOf(monthlyResponses.getJuly()));
		actualValues.add(Double.valueOf(monthlyResponses.getAug()));
		actualValues.add(Double.valueOf(monthlyResponses.getSep()));
		actualValues.add(Double.valueOf(monthlyResponses.getOct()));
		actualValues.add(Double.valueOf(monthlyResponses.getNov()));
		actualValues.add(Double.valueOf(monthlyResponses.getDecm()));

		for (int i = 1; i <= 12; i++) {
			plannedValues.add(Double.valueOf(monthlyProduction));
		}
		ymr.setPlanned(plannedValues);
		ymr.setYear(year);
		ymr.setActual(actualValues);
		return ymr;
	}
//---------------------------------------------------------------------------------------------------------------------	
	
	//Yearly Production Report For multiple years( not using)
 
	@RequestMapping(value = "/MonthlyProductionProductReports/{year}", produces = "application/json", method = RequestMethod.GET)
	public YearlyMonthWiseResponse MontlyProductionProductWiseReport(@PathVariable int year) {
		YearlyMonthWiseResponse ymr = new YearlyMonthWiseResponse();
		//int year = Calendar.getInstance().get(Calendar.YEAR);
		MonthlyResponse monthlyResponses = managementservice.MontlyProductionProductWiseReport(year);
		Float monthlyProduction = managementservice.getMonthlyProductionvalue(year).getWeeklySum();

		List<Double> actualValues = new ArrayList<Double>();
		List<Double> plannedValues = new ArrayList<Double>();
		actualValues.add(Double.valueOf(monthlyResponses.getJan()));
		actualValues.add(Double.valueOf(monthlyResponses.getFeb()));
		actualValues.add(Double.valueOf(monthlyResponses.getMar()));
		actualValues.add(Double.valueOf(monthlyResponses.getApr()));
		actualValues.add(Double.valueOf(monthlyResponses.getMay()));
		actualValues.add(Double.valueOf(monthlyResponses.getJune()));
		actualValues.add(Double.valueOf(monthlyResponses.getJuly()));
		actualValues.add(Double.valueOf(monthlyResponses.getAug()));
		actualValues.add(Double.valueOf(monthlyResponses.getSep()));
		actualValues.add(Double.valueOf(monthlyResponses.getOct()));
		actualValues.add(Double.valueOf(monthlyResponses.getNov()));
		actualValues.add(Double.valueOf(monthlyResponses.getDecm()));

		for (int i = 1; i <= 12; i++) {
			plannedValues.add(Double.valueOf(monthlyProduction));
		}
		ymr.setPlanned(plannedValues);
		ymr.setYear(year);
		ymr.setActual(actualValues);
		return ymr;
	}

//--------------------------------------------------------------------------------------------------------------------
	//Full Quarter Production Report for Current year
	
	@RequestMapping(value = "/fullQuaterProductionReport", produces = "application/json", method = RequestMethod.GET)
	public List<FullQuarterReportResponse> fullQuaterProductionReport() {
		List<FullQuarterReportResponse> fullQuarterReport= managementservice.fullQuaterProductionReport();
		return fullQuarterReport;
	}
	
	@RequestMapping(value = "/quaterProductionProductReports/{quarterNumber}", produces = "application/json", method = RequestMethod.GET)
	public List<ProductForChart> quaterProductionProductWiseReport(@PathVariable int quarterNumber) {
		List<ProductForChart> FullQuarterProductionReports = managementservice.quaterProductionProductWiseReport(quarterNumber);
		return FullQuarterProductionReports;
	}
	
	@RequestMapping(value = "/fullQuaterProductionPlan", produces = "application/json", method = RequestMethod.GET)
	public List<ManagementQuarterProductionPlan> fullQuaterProductionPlan() {
		List<ManagementQuarterProductionPlan> fullquaterProductionPlan = managementservice.fullQuaterProductionPlan();
		return fullquaterProductionPlan;
	}
//--------------------------------------------------------------------------------------------------------------------
	//cycle time for ProcessOrders
	
	@RequestMapping(value = "/aveargeCycleTimeForWeek", produces = "application/json", method = RequestMethod.GET)
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForWeek() {
		AverageCycleTimeResponse avaeragecompleteTime = managementservice.averageCycleTimeForProcessOrdersForWeek();
		return avaeragecompleteTime;
	}
	@RequestMapping(value = "/cycleTimeForWeek", produces = "application/json", method = RequestMethod.GET)
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForWeek() {
		List<CycleTimeResponse> completeTime = managementservice.cycleTimeForProcessOrdersForWeek();
		return completeTime;
	}
	
//-------------------------------------------------------------------------------------------------------------------
	//Cycle Time for Month
	@RequestMapping(value = "/aveargeCycleTimeForMonth", produces = "application/json", method = RequestMethod.GET)
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForMonth() {
		AverageCycleTimeResponse avaeragecompleteTime = managementservice.aveargeCycleTimeForProcessOrdersForMonth();
		return avaeragecompleteTime;
	}
	@RequestMapping(value = "/cycleTimeForMonth", produces = "application/json", method = RequestMethod.GET)
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForMonth() {
		List<CycleTimeResponse> completeTime = managementservice.cycleTimeForProcessOrdersForMonth();
		return completeTime;
	}
//------------------------------------------------------------------------------------------------------------------
	//Cycle Time For Quarter
	@RequestMapping(value = "/aveargeCycleTimeForQuarter", produces = "application/json", method = RequestMethod.GET)
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForQuarter() {
		AverageCycleTimeResponse avaeragecompleteTime = managementservice.aveargeCycleTimeForProcessOrdersForQuarter();
		return avaeragecompleteTime;
	}
	@RequestMapping(value = "/cycleTimeForQuarter", produces = "application/json", method = RequestMethod.GET)
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForQuarter() {
		List<CycleTimeResponse> completeTime = managementservice.cycleTimeForProcessOrdersForQuarter();
		return completeTime;
	}
//---------------------------------------------------------------------------------------------------------------------	
	//Cycle Time for Year
	@RequestMapping(value = "/aveargeCycleTimeForYear", produces = "application/json", method = RequestMethod.GET)
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForYear() {
		AverageCycleTimeResponse avaeragecompleteTime = managementservice.aveargeCycleTimeForProcessOrdersForYear();
		return avaeragecompleteTime;
	}
	@RequestMapping(value = "/cycleTimeForYear", produces = "application/json", method = RequestMethod.GET)
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForYear() {
		List<CycleTimeResponse> completeTime = managementservice.cycleTimeForProcessOrdersForYear();
		return completeTime;
	}
	
//-------------------------------------------------------------------------------------------------------
	//new YearlyReport
	
	@RequestMapping(value = "/YearlyProduction", produces = "application/json", method = RequestMethod.GET)
	public YearlyReportResponse YearlyProductionReport() {
		YearlyReportResponse ymr = new YearlyReportResponse();
		//int year = Calendar.getInstance().get(Calendar.YEAR);
		YearlyReportResponse yearlyReport = managementservice.YearlyProductionReport();
		List <ManagementReportResponse> plannedQuantity = managementservice.getPlannedProduction();

		/*List<Double> actualValues = new ArrayList<Double>();
		List<Double> plannedValues = new ArrayList<Double>();
		actualValues.add(Double.valueOf(monthlyResponses.getJan()));
		actualValues.add(Double.valueOf(monthlyResponses.getFeb()));
		actualValues.add(Double.valueOf(monthlyResponses.getMar()));
		actualValues.add(Double.valueOf(monthlyResponses.getApr()));
		actualValues.add(Double.valueOf(monthlyResponses.getMay()));
		actualValues.add(Double.valueOf(monthlyResponses.getJune()));
		actualValues.add(Double.valueOf(monthlyResponses.getJuly()));
		actualValues.add(Double.valueOf(monthlyResponses.getAug()));
		actualValues.add(Double.valueOf(monthlyResponses.getSep()));
		actualValues.add(Double.valueOf(monthlyResponses.getOct()));
		actualValues.add(Double.valueOf(monthlyResponses.getNov()));
		actualValues.add(Double.valueOf(monthlyResponses.getDecm()));

		for (int i = 1; i <= 12; i++) {
			plannedValues.add(Double.valueOf(monthlyProduction));
		}*/
		//ymr.setPlanned(plannedValues);
		//ymr.setYear(year);
		//ymr.setActual(actualValues);
		return ymr;
	}
//----------------------------------------------------------------------------------------------------------------------
	/*@RequestMapping(value = "/currentQuarterWeekwiseReport", produces = "application/json", method = RequestMethod.GET)
	public CurrentQuarterResponse CurrentQuarterWeekwiseReport() {
		CurrentQuarterResponse ymr = new CurrentQuarterResponse();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		CurrentQuarterWeeklyResponse currentQuarterWeeklyResponse = managementservice.currentQuarterWeeklyReport();
		String quarterweeklyResponse = managementservice.getPlannedReport().getQuarterlySum();

		List<Double> actualValues = new ArrayList<Double>();
		List<Double> plannedValues = new ArrayList<Double>();
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek1()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek2()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek3()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek4()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek5()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek6()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek7()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek8()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek9()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek10()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek11()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek12()));
		actualValues.add(Double.valueOf(currentQuarterWeeklyResponse.getWeek13()));
		

		for (int i = 1; i <= 13; i++) {
			plannedValues.add(Double.valueOf(quarterweeklyResponse));
		}
		ymr.setPlannedValues(plannedValues);
		ymr.setYear(year);
		ymr.setActualValues(actualValues);
		return ymr;
	}*/

//////////////////////////yearly Report /////////////////////////////


@RequestMapping(value = "/YearWiseReport", produces = "application/json", method = RequestMethod.GET)
public YearlyBarchartResponse YearlyProductionBarchart() {
	 return managementservice.YearlyProductionBarchart();
}

//multiple year BarChart
@RequestMapping(value = "/MultipleYearWiseReport/{year}", produces = "application/json", method = RequestMethod.GET)
public YearlyBarchartResponse MultipleYearProductionBarchart(@PathVariable int year) {
	 return managementservice.MultipleYearProductionBarchart(year);
}
 

//-------------------------------------------------------------------------------------------------------------
//DayWise Production report//////////////////////////////////////////////

@RequestMapping(value = "/DayWiseProductionReport", produces = "application/json", method = RequestMethod.GET)
public List<ManagementReportResponse> dayWiseProductionReport() {
	 return managementservice.dayWiseProductionReport();
}


@RequestMapping(value = "/DailyProductionReport/{quarter}", produces = "application/json", method = RequestMethod.GET)
public List<ManagementReportResponse> dayWiseProductionReport(@PathVariable int quarter) {
	return managementservice.dayWiseProductionReport(quarter);
}

//----------------------------------------------------------------------------------------------------------------
//Repacking report

@RequestMapping(value = "/RepackingReport", produces = "application/json", method = RequestMethod.GET)
public List<RepackingReportResponse> rePackingReport() {
	return managementservice.rePackingReport();
} 

  @RequestMapping(value = "/YearwiseRepackingReport/{year}", produces = "application/json", method = RequestMethod.GET)
  public List<RepackingReportResponse> rePackingReport(@PathVariable int year) {
	return managementservice.rePackingReport(year);
 } 
  @RequestMapping(value = "/AskingRate", produces = "application/json", method = RequestMethod.GET)
  public int askingRate(){
	  return managementservice.askingRate(); 
  }
//--------------------------------------------------------------------------------------------------------------------
  @RequestMapping(value = "/cycleTimeForModal/{processorderProdNumber}", produces = "application/json", method = RequestMethod.GET)
  public List<PackingReportResponse> cycleTimeForModal(@PathVariable Long processorderProdNumber) {
	 // System.out.println("Process Order =>" + processorderProdNumber);
	return managementservice.cycleTimeForModal(processorderProdNumber);
 } 
  
 //-----------------------------------------------------------------------------------------------------------------------
  @RequestMapping(value = "/barchartReports/{year}", produces = "application/json", method = RequestMethod.GET)
  public QuarterBarchartResponse1 barchartReports(@PathVariable String year) {
	return managementservice.barchartReports(year);
 } 
  
}