package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ProcessorderProd;
import com.response.Response;
import com.service.PoHomeService;

@RestController
@RequestMapping("/poHome")
public class PoHomeController {
	
	@Autowired
	PoHomeService pendingOrdersService;
	
	@RequestMapping(value = "/getAllOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getAllOrders() throws Exception {
		List<ProcessorderProd> orders = null;
		
		try {
			orders = pendingOrdersService.getAllOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", orders);
	}
	
	@RequestMapping(value = "/getTodaysOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getTodaysOrders() throws Exception {
		List<ProcessorderProd> todays = null;
		
		try {
			todays = pendingOrdersService.getTodaysOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", todays);
	}
	
	@RequestMapping(value = "/getPreviousOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPreviousOrders() throws Exception {
		List<ProcessorderProd> previous = null;
		
		try {
			previous = pendingOrdersService.getPreviousOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", previous);
	}
	
	@RequestMapping(value = "/getFutureOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getFutureOrders() throws Exception {
		List<ProcessorderProd> future = null;
		
		try {
			future = pendingOrdersService.getFutureOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", future);
	}
	
	@RequestMapping(value = "/getTodaysCompletedOrders", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getTodaysCompletedOrders() throws Exception {
		List<ProcessorderProd> completed = null;
		
		try {
			completed = pendingOrdersService.getTodaysCompletedOrders();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", completed);
	}

}
