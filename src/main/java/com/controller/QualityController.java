package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.Quality;
import com.entity.QualityCheck;
import com.response.Response;
import com.service.QualityService;

@RestController
@RequestMapping("/quality")
public class QualityController {
	
	@Autowired
	QualityService qualityService;
	
	@RequestMapping(value = "/getPackingOrders/{processorderProdNumber}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getPackageProcessOrders(@PathVariable long processorderProdNumber) throws Exception {
		List<ProcessorderPckg> packageOrders = null;
		
		try {
			packageOrders = qualityService.getPackageProcessOrders(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packageOrders);
	}
	
	@RequestMapping(value = "/getQualities", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getQualities() throws Exception {
		List<Quality> qualities = null;
		
		try {
			qualities = qualityService.getQualities();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", qualities);
	}
	
	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getProductDetails() throws Exception {
		List<ProcessorderProd> productDetails = null;
		
		try {
			productDetails = qualityService.getProductDetails();
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", productDetails);
	}
	
	@RequestMapping(value = "/updatePackage", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response updatePackage( @RequestBody ProcessorderPckg processorderPckg) throws Exception {
		Long packages = null;
		try {
			packages = qualityService.updatePackage(processorderPckg);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packages);
	}
	
	@RequestMapping(value = "/getQualityCheckDetails/{processorderProdNumber}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response getQualityCheckDetails(@PathVariable long processorderProdNumber ) throws Exception {
		List<QualityCheck> packageOrders = null;
		
		try {
			packageOrders = qualityService.getQualityCheck(processorderProdNumber);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packageOrders);
	}
	
	@RequestMapping(value = "/acceptQuality/{processorderProdNumber}/{ssoid}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_PLAIN_VALUE })

	public @ResponseBody Response acceptQuality( @PathVariable long processorderProdNumber,@PathVariable String ssoid) throws Exception {
		Long packages = null;
		try {
			packages = qualityService.acceptQuality(processorderProdNumber,ssoid);
		} catch (Exception ex) {
			return new Response("400", ex.getMessage());
		}
		return new Response("200", packages);
	}
	
		@RequestMapping(value = "/rejectQuality/{processorderProdNumber}/{ssoid}/{comment}/{batchNo}/{prodNo}/{memo}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
				MediaType.TEXT_PLAIN_VALUE })
	
		public @ResponseBody Response rejectQuality( @PathVariable long processorderProdNumber,@PathVariable String ssoid,@PathVariable String comment,@PathVariable String batchNo,@PathVariable String prodNo,@PathVariable String memo) throws Exception {
			Long packages = null;
			try {
				packages = qualityService.rejectQuality(processorderProdNumber,ssoid,comment,batchNo,prodNo,memo);
			} catch (Exception ex) {
				return new Response("400", ex.getMessage());
			}
			return new Response("200", packages);
		}
		
		@RequestMapping(value = "/scrapQuality/{processorderProdNumber}/{ssoid}/{comment}", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE,
				MediaType.TEXT_PLAIN_VALUE })
	
		public @ResponseBody Response scrapQuality( @PathVariable long processorderProdNumber,@PathVariable String ssoid,@PathVariable String comment) throws Exception {
			Long packages = null;
			try {
				packages = qualityService.scrapQuality(processorderProdNumber, ssoid, comment);
			} catch (Exception ex) {
				return new Response("400", ex.getMessage());
			}
			return new Response("200", packages);
		}
		
		@RequestMapping(value = "/getRepackDetails", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
				MediaType.TEXT_PLAIN_VALUE })

		public @ResponseBody Response getRepackDetails() throws Exception {
			List<ProcessorderPckg> productDetails = null;
			
			try {
				productDetails = qualityService.getRepackDetails();
			} catch (Exception ex) {
				return new Response("400", ex.getMessage());
			}
			return new Response("200", productDetails);
		}
		
		@RequestMapping(value = "/getRepackCount", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
				MediaType.TEXT_PLAIN_VALUE })

		public @ResponseBody Response getRepackCount() throws Exception {
			int count = 0;
			
			try {
				count = qualityService.getRepackCount();
			} catch (Exception ex) {
				return new Response("400", ex.getMessage());
			}
			return new Response("200", count);
		}

}
