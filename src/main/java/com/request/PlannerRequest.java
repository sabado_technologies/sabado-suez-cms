package com.request;

import java.util.Date;
import java.util.List;

public class PlannerRequest {
	
	private int plannedId;
	private String perQuarter;
	private String perDay;
	private String perWeek;
	private int workingDays;
	private int quarter;
	private int year;
	private List<String> workingDates;
	private List<Date> dates;
	private String selectedDates;
	
	public int getPlannedId() {
		return plannedId;
	}
	public void setPlannedId(int plannedId) {
		this.plannedId = plannedId;
	}
	public String getPerQuarter() {
		return perQuarter;
	}
	public void setPerQuarter(String perQuarter) {
		this.perQuarter = perQuarter;
	}
	public String getPerDay() {
		return perDay;
	}
	public void setPerDay(String perDay) {
		this.perDay = perDay;
	}
	public String getPerWeek() {
		return perWeek;
	}
	public void setPerWeek(String perWeek) {
		this.perWeek = perWeek;
	}
	public int getWorkingDays() {
		return workingDays;
	}
	public void setWorkingDays(int workingDays) {
		this.workingDays = workingDays;
	}
	public int getQuarter() {
		return quarter;
	}
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List<String> getWorkingDates() {
		return workingDates;
	}
	public void setWorkingDates(List<String> workingDates) {
		this.workingDates = workingDates;
	}
	public List<Date> getDates() {
		return dates;
	}
	public void setDates(List<Date> dates) {
		this.dates = dates;
	}
	public String getSelectedDates() {
		return selectedDates;
	}
	public void setSelectedDates(String selectedDates) {
		this.selectedDates = selectedDates;
	}
	
}
