package com.request;

import java.util.ArrayList;
import java.util.List;

public class PdfValues {

	public PdfValues(String processOderNo, String productNumber, String productName, String productGroup,
			String plantInformation, String batchNo, String batchDate, String totalQuant, String volume, String wgtPkg,
			String processOrderNote, List<String> rmCodes, List<String> rmName, List<String> rmQuant,
			List<String> rmTypes, List<String> rmPhases) {
		super();
		this.processOderNo = processOderNo;
		this.productNumber = productNumber;
		this.productName = productName;
		this.productGroup = productGroup;
		this.plantInformation = plantInformation;
		this.batchNo = batchNo;
		this.batchDate = batchDate;
		this.totalQuant = totalQuant;
		this.volume = volume;
		this.wgtPkg = wgtPkg;
		this.processOrderNote = processOrderNote;
		this.rmCodes = rmCodes;
		this.rmName = rmName;
		this.rmQuant = rmQuant;
		this.rmTypes = rmTypes;
		this.rmPhases = rmPhases;
	}

	private String processOderNo = "";
	private String productNumber = "";
	private String productName = "";
	private String productGroup = "";

	private String plantInformation = "";
	private String batchNo = "";
	private String batchDate = "";
	private String totalQuant = "";
	private String volume = "";
	private String wgtPkg = "";
	private String processOrderNote = "";
	private List<String> rmCodes = new ArrayList<String>();
	private List<String> rmName = new ArrayList<String>();
	private List<String> rmQuant = new ArrayList<String>();
	private List<String> rmTypes = new ArrayList<String>();
	private List<String> rmPhases = new ArrayList<String>();

	public String getProcessOrderNo() {
		return productNumber;
	}

	public void setProcessOrderNo(String processOrderNo) {
		this.productNumber = processOrderNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public String getPlantInformation() {
		return plantInformation;
	}

	public void setPlantInformation(String plantInformation) {
		this.plantInformation = plantInformation;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public List<String> getRmQuant() {
		return rmQuant;
	}

	public void setRmQuant(List<String> rmQuant) {
		this.rmQuant = rmQuant;
	}

	public String getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	public String getTotalQuant() {
		return totalQuant;
	}

	public void setTotalQuant(String totalQuant) {
		this.totalQuant = totalQuant;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getWgtPkg() {
		return wgtPkg;
	}

	public void setWgtPkg(String wgtPkg) {
		this.wgtPkg = wgtPkg;
	}

	public String getProcessOrderNote() {
		return processOrderNote;
	}

	public void setProcessOrderNote(String processOrderNote) {
		this.processOrderNote = processOrderNote;
	}

	public String getProcessOderNo() {
		return processOderNo;
	}

	public void setProcessOderNo(String processOderNo) {
		this.processOderNo = processOderNo;
	}

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	public List<String> getRmCodes() {
		return rmCodes;
	}

	public void setRmCodes(List<String> rmCodes) {
		this.rmCodes = rmCodes;
	}

	public List<String> getRmName() {
		return rmName;
	}

	public void setRmName(List<String> rmName) {
		this.rmName = rmName;
	}

	public List<String> getRmTypes() {
		return rmTypes;
	}

	public void setRmTypes(List<String> rmTypes) {
		this.rmTypes = rmTypes;
	}

	public List<String> getRmPhases() {
		return rmPhases;
	}

	public void setRmPhases(List<String> rmPhases) {
		this.rmPhases = rmPhases;
	}

}
