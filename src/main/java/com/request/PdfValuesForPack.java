package com.request;

import java.util.ArrayList;
import java.util.List;

public class PdfValuesForPack {

	String processOrderNo = "";
	String packingCode = "";
	String packingGroup = "";
	String plantInformation = "";
	String batchNo = "";
	String batchDate = "";
	String totalQuant = "";
	String volume = "";
	String wgtPkg = "";
	String processOrderNote = "";
	String materialMemo = "";
	String storage ="";
	List<String> rmCodes = new ArrayList<String>();
	List<String> rmName = new ArrayList<String>();
	List<String> rmQuant = new ArrayList<String>();
	List<String> rmTypes = new ArrayList<String>();
	List<String> rmPhases = new ArrayList<String>();

	public String getProcessOrderNo() {
		return processOrderNo;
	}

	public void setProcessOrderNo(String processOrderNo) {
		this.processOrderNo = processOrderNo;
	}
	
	public String getStorage(){
		return storage;
	}
	
	public void setStorage(String storage){
		this.storage = storage;
	}

	public String getPackingCode() {
		return packingCode;
	}

	public void setPackingCode(String packingCode) {
		this.packingCode = packingCode;
	}

	public String getPackingGroup() {
		return packingGroup;
	}

	public void setPackingGroup(String packingGroup) {
		this.packingGroup = packingGroup;
	}

	public String getPlantInformation() {
		return plantInformation;
	}

	public void setPlantInformation(String plantInformation) {
		this.plantInformation = plantInformation;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}

	public String getTotalQuant() {
		return totalQuant;
	}

	public void setTotalQuant(String totalQuant) {
		this.totalQuant = totalQuant;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getWgtPkg() {
		return wgtPkg;
	}

	public void setWgtPkg(String wgtPkg) {
		this.wgtPkg = wgtPkg;
	}

	public String getProcessOrderNote() {
		return processOrderNote;
	}

	public void setProcessOrderNote(String processOrderNote) {
		this.processOrderNote = processOrderNote;
	}
	
	public String getMaterialMemo(){
		return materialMemo;
	}
	
	public void setMaterialMemo(String materialMemo){
		this.materialMemo = materialMemo;
	}

	public List<String> getRmCodes() {
		return rmCodes;
	}

	public void setRmCodes(List<String> rmCodes) {
		this.rmCodes = rmCodes;
	}

	public List<String> getRmName() {
		return rmName;
	}

	public void setRmName(List<String> rmName) {
		this.rmName = rmName;
	}

	public PdfValuesForPack(String processOrderNo, String packingCode, String packingGroup, String plantInformation,
			String batchNo, String batchDate, String totalQuant, String volume, String wgtPkg, String processOrderNote,
			List<String> rmCodes, List<String> rmName, List<String> rmQuant, List<String> rmTypes,
			List<String> rmPhases, String materialMemo,String storage) {
		super();
		this.processOrderNo = processOrderNo;
		this.packingCode = packingCode;
		this.packingGroup = packingGroup;
		this.plantInformation = plantInformation;
		this.batchNo = batchNo;
		this.batchDate = batchDate;
		this.totalQuant = totalQuant;
		this.volume = volume;
		this.wgtPkg = wgtPkg;
		this.processOrderNote = processOrderNote;
		this.rmCodes = rmCodes;
		this.rmName = rmName;
		this.rmQuant = rmQuant;
		this.rmTypes = rmTypes;
		this.rmPhases = rmPhases;
		this.materialMemo = materialMemo;
		this.storage = storage;
	}

	public List<String> getRmQuant() {
		return rmQuant;
	}

	public void setRmQuant(List<String> rmQuant) {
		this.rmQuant = rmQuant;
	}

	public List<String> getRmTypes() {
		return rmTypes;
	}

	public void setRmTypes(List<String> rmTypes) {
		this.rmTypes = rmTypes;
	}

	public List<String> getRmPhases() {
		return rmPhases;
	}

	public void setRmPhases(List<String> rmPhases) {
		this.rmPhases = rmPhases;
	}

}
