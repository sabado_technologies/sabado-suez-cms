package com.request;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
/*import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;
*/
public class AESCrypt {
	
	 private static final String ALGORITHM = "AES";
	    private static final String KEY = "1Hbfh667adfDEJ78";
	    
	    public static String encrypt(String value) throws Exception
	    {
	        return  Base64.getEncoder().encodeToString(value.getBytes("utf-8"));
	        //return encryptedValue64;
	               
	    }
	    
	    public static String decrypt(String value) throws Exception
	    {
	        return  new String(Base64.getDecoder().decode(value), "utf-8");
	                
	    }
	    
	    private static Key generateKey() throws Exception 
	    {
	        Key key = new SecretKeySpec(AESCrypt.KEY.getBytes(),AESCrypt.ALGORITHM);
	        return key;
	    }

}
