package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import com.dao.LoginDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.Role;
import com.entity.Status;
import com.entity.User;
import com.request.AESCrypt;
import com.request.UserRequest;

@Repository
public class LoginDaoImpl extends NamedParameterJdbcDaoSupportClass implements LoginDao {

	public List<User> getRole(final UserRequest user) throws Exception {
		List<User> listOfUser = null;
		try {

		      String  encryptedPassword = AESCrypt.encrypt(user.getPassword());
			
			
			String query = "SELECT user_name,ssoid,plant_id,role_id,email,status_id FROM user where ssoid='"
					+ user.getSsoid() + "' and password='" + encryptedPassword + "';";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<User>>() {
				public List<User> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<User> list = new ArrayList<User>();
					while (rs.next()) {
						User u = new User();
						u.setUserName(rs.getString(1));
						u.setSsoid(rs.getString(2));
						Role r = new Role();
						r.setRoleId(rs.getInt(4));
						u.setRole(r);
						u.setEmail(rs.getString(5));
						Status s=new Status();
						s.setStatusId(rs.getInt(6));
						u.setStatus(s);
						list.add(u);

					}
					return list;
				}
			});

		} catch (Exception e) {
			// e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return listOfUser;

	}

}
