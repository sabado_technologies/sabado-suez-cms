package com.daoImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.dao.AssociateDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.Associate;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;

@Repository
public class AssociateDaoImpl extends NamedParameterJdbcDaoSupportClass implements AssociateDao {

	@Override
	public List<ProcessorderProd> getProductionProcessOrders() {

		try {
			String query ="SELECT p.processorder_prod_number,p.product_number,p.total_quantity,\r\n"
					+ "p.batch_number,pt.product_description FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "where p.is_associated=0 order by p.processorder_prod_number";
						
			List<ProcessorderProd> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
							while (rs.next()) {
								ProcessorderProd po = new ProcessorderProd();
								po.setProcessorderProdNumber(rs.getLong(1));
								po.setTotalQuantity(rs.getString(3));
								po.setBatchNumber(rs.getString(4));
								ProductTable pr = new ProductTable();
								pr.setProductNumber(rs.getLong(2));
								pr.setProductDescription(rs.getString(5));
								po.setProductTable(pr);
								list.add(po);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderPckg> getPackageProcessOrders() {

		try {
			String query = "SELECT p.processorder_pckg_number,p.sixml_number,p.wght_pckg,p.material_memo\r\n"
					      + "FROM processorder_pckg p where p.is_associated=0 and p.is_repack !=1 \r\n"
					      + "order by p.processorder_pckg_number";
			/*String query="SELECT p.processorder_pckg_number,pt.product_description,\r\n"
					+ "p.sixml_number,p.wght_pckg FROM processorder_pckg p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "where p.is_associated=0 order by p.processorder_pckg_number";*/
			
			//String query = "SELECT p.processorder_pckg_number FROM processorder_pckg p where p.is_associated=0 order by p.processorder_pckg_number";

			List<ProcessorderPckg> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg pack = new ProcessorderPckg();
								pack.setProcessorderPckgNumber(rs.getLong(1));
								pack.setProductName(rs.getString(4));
								pack.setSixmlNumber(rs.getString(2));
								pack.setWghtPckg(rs.getLong(3));
								list.add(pack);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int associateProcess(List<Associate> list) {

		try {

			String sql1 = "INSERT INTO associate(product_process_number,pakage_process_number) " + "VALUES(?, ?);;";

			getJdbcTemplate().batchUpdate(sql1, new BatchPreparedStatementSetter() {

				public int getBatchSize() {
					return list.size();
				}

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Associate associate = list.get(i);

					ps.setLong(1, associate.getProductProcessNumber());
					ps.setLong(2, associate.getPakageProcessNumber());
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String sql2 = "UPDATE processorder_pckg SET  is_associated=1,processorder_prod_number=? WHERE processorder_pckg_number =?";

			getJdbcTemplate().batchUpdate(sql2, new BatchPreparedStatementSetter() {

				public int getBatchSize() {
					return list.size();
				}

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Associate associate = list.get(i);

					ps.setLong(1, associate.getProductProcessNumber());
					ps.setLong(2, associate.getPakageProcessNumber());
				}
			});

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String sql3 = "UPDATE processorder_prod SET  is_associated=1 WHERE processorder_prod_number =?";

			getJdbcTemplate().batchUpdate(sql3, new BatchPreparedStatementSetter() {

				public int getBatchSize() {
					return list.size();
				}

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Associate associate = list.get(i);

					ps.setLong(1, associate.getProductProcessNumber());
				}
			});

		} catch (Exception e) {
			e.printStackTrace();

		}

		return 0;
	}

	@Override
	public List<ProcessorderPckg> getDeletedProcessOrders() {
		try {
			
			String query ="SELECT d.processorder_prod_number,d.product_number,d.batch_number,\r\n "
					+ "d.batch_date,d.total_quantity,d.volume,d.remarks FROM delete_details d \r\n"
					+ "order by d.processorder_prod_number,d.batch_date";
			
			/*String query ="SELECT p.processorder_prod_number,p.product_number,p.total_quantity,\r\n"
					+ "p.batch_number,pt.product_description FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "where p.is_associated=0 order by p.processorder_prod_number";*/
						
			List<ProcessorderPckg> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg po = new ProcessorderPckg();
								po.setProcessorderPckgNumber(rs.getLong(1));
								po.setProductNumber(rs.getLong(2));
								po.setBatchNumber(rs.getString(3));
								String reversed = reverseDate(rs.getString(4));
								po.setBatchDate(reversed);
								po.setTotalQuantity(rs.getLong(5));
								po.setVolume(rs.getString(6));
								po.setRemarks(rs.getString(7));
								
								/*po.setProcessorderProdNumber(rs.getLong(1));
								po.setTotalQuantity(rs.getString(3));
								po.setBatchNumber(rs.getString(4));
								ProductTable pr = new ProductTable();
								pr.setProductNumber(rs.getLong(2));
								pr.setProductDescription(rs.getString(5));
								po.setProductTable(pr);*/
								list.add(po);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}

}
