package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.dao.PackingOrderInfoDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ContainerTable;
import com.entity.ProcessorderPckg;

@Repository
public class PackingOrderInfoDaoImpl extends NamedParameterJdbcDaoSupportClass implements PackingOrderInfoDao {

	@Override
	public List<ProcessorderPckg> getPackingInfo(long processOrderNumber) {
		try {
			
			String query = "SELECT p.processorder_pckg_number,p.sixml_number,\r\n"
					+ "p.batch_number,p.material_memo,p.batch_date,p.total_quantity,\r\n"
					+ "p.wght_pckg,p.product_number,p.product_name,p.process_quant,\r\n"
					+ "p.container_number,c.container_description,p.storage_code FROM processorder_pckg p\r\n"
					+ " inner join container_table c on c.container_number=p.container_number \r\n"
					+ "where p.processorder_prod_number=' "+processOrderNumber+" '";

			List<ProcessorderPckg> packingInfo = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();

							while (rs.next()) {

								ProcessorderPckg pckg = new ProcessorderPckg();
								pckg.setProcessorderPckgNumber(rs.getLong(1));
								pckg.setSixmlNumber(rs.getString(2));
								pckg.setBatchNumber(rs.getString(3));
								pckg.setMaterialMemo(rs.getString(4));
								String reversed =reverseDate(rs.getString(5));
								pckg.setBatchDate(reversed);
								String reversed1 =reverseDate1(rs.getString(5));
								pckg.setPdfDate(reversed1);
								pckg.setTotalQuantity(rs.getLong(6));;
								pckg.setWghtPckg(rs.getLong(7));;
								pckg.setProductNumber(rs.getLong(8));
								pckg.setProductName(rs.getString(9));
								pckg.setProcessQuant(rs.getString(10));
								pckg.setStorage(rs.getString(13));
								
								ContainerTable c = new ContainerTable();
								c.setContainerNumber(rs.getLong(11));
								c.setContainerDescription(rs.getString(12));
								pckg.setContainerTable(c);
								list.add(pckg);
							}
							return list;
						}
					});
			return packingInfo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}
	
	public String reverseDate1(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1]  + splited[splited.length - 2] + splited[0];

		return reversedDate;

	}
	

}
