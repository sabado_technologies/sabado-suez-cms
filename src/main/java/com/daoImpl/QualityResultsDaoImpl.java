package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.QualityResultsDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;

@Repository
public class QualityResultsDaoImpl extends NamedParameterJdbcDaoSupportClass implements QualityResultsDao {

	@Override
	public List<ProcessorderProd> getReworkDetails() {
		try {
			
			String query ="SELECT r.processorder_prod_number,p.product_number,p.batch_number,\r\n"
					+ "p.batch_date,p.comments,pt.product_description,r.remarks,r.is_approved FROM rework r \r\n"
					+ "inner join processorder_prod p on p.processorder_prod_number=r.processorder_prod_number\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number";

			List<ProcessorderProd> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
							while (rs.next()) {
								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchNumber(rs.getString(3));
								String reversed = reverseDate(rs.getString(4));
								prod.setBatchDate(reversed);
								prod.setComment(rs.getString(5));
								prod.setRemarks(rs.getString(7));
								prod.setIsApproved(rs.getInt(8));
								
								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));								
								pt.setProductDescription(rs.getString(6));								
								prod.setProductTable(pt);
								list.add(prod);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}	
	
	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}

	@Override
	public List<ProcessorderPckg> getPackingDetails(long processOrder) {
try {
			String query = "SELECT p.processorder_pckg_number,p.sixml_number,\r\n"
					+ "p.batch_date,p.batch_number,p.material_memo,p.total_quantity FROM processorder_pckg p\r\n"
					+ "where p.processorder_prod_number='"+processOrder+"' ";

			List<ProcessorderPckg> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg pack = new ProcessorderPckg();
								pack.setProcessorderPckgNumber(rs.getLong(1));
								pack.setSixmlNumber(rs.getString(2));
								String reversed = reverseDate(rs.getString(3));
								pack.setBatchDate(reversed);
								pack.setBatchNumber(rs.getString(4));
								pack.setMaterialMemo(rs.getString(5));
								pack.setTotalQuantity(rs.getLong(6));
								list.add(pack);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getScrapDetails() {
try {
			String query = "SELECT p.processorder_prod_number,p.product_number,\r\n"
					+ "pt.product_description,p.batch_number,p.batch_date,p.total_quantity,\r\n"
					+ "p.comments FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "where p.status=22";

			List<ProcessorderProd> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
							while (rs.next()) {
								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchNumber(rs.getString(4));
								String reversed = reverseDate(rs.getString(5));
								prod.setBatchDate(reversed);
								prod.setTotalQuantity(rs.getString(6));
								prod.setComment(rs.getString(7));
								
								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));								
								pt.setProductDescription(rs.getString(3));								
								prod.setProductTable(pt);
								list.add(prod);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public long approve(long processOrder, String remarks, String ssoid) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		try {
			String query = " UPDATE processorder_prod SET status = 17 ,remarks = :remarks WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processOrder);
			param.addValue("remarks", remarks);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		try {
			String query = " UPDATE processorder_pckg SET status=17 WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processOrder);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		
		try {

			String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id, is_reworked) VALUES (:processOrder, :sso, :time, :status, :rework)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processOrder);
			param.addValue("time", timeStamp);
			param.addValue("status", 17);
			param.addValue("sso", ssoid);
			param.addValue("rework", 1);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {
			
			String query = "UPDATE rework SET remarks = :remarks ,approved_by = :sso ,approved_time = :time ,is_approved = :isApproved WHERE processorder_prod_number = :processOrder ;";

			/*String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id, is_reworked) VALUES (:processOrder, :sso, :time, :status, :rework)";*/

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processOrder);
			param.addValue("time", timeStamp);
			param.addValue("sso", ssoid);
			param.addValue("remarks", remarks);
			param.addValue("isApproved", 1);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		
		return processOrder;
	}

}
