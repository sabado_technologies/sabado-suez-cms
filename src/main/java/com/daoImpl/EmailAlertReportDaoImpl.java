package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.dao.EmailAlertReportDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.EmailAlerts;
import com.response.EmailAlertResponce;

@Repository
public class EmailAlertReportDaoImpl extends NamedParameterJdbcDaoSupportClass implements EmailAlertReportDao   {

	@Override
	public List<EmailAlertResponce> getEmailAlert() {
		List<EmailAlertResponce> listOfUser = null;
		try {

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String query = "SELECT count(e.process_order)as c,e. process_order,e.sso,\r\n"
					+ "e.user_name,e.reactor,date(e.time) as sent FROM email_alerts e\r\n"
					+ "where e.process='Raw Material Charging' group by e.process_order;";
			
//			/String query = "select count(process_order)c,process_order,sso,user_name,process,reactor,time  from suez.email_alerts where time group by process_order having c >1; ;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<EmailAlertResponce>>() {
				public List<EmailAlertResponce> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<EmailAlertResponce> list = new ArrayList<EmailAlertResponce>();
					while (rs.next()) {
						EmailAlertResponce po = new EmailAlertResponce();
						po.setNoOfAlert(rs.getInt(1));
						po.setProcessOrder(rs.getLong(2));
						po.setSso(rs.getString(3));
						po.setUserName(rs.getString(4));
						//po.setProcess(rs.getString(5));
						po.setReactor(rs.getString(5));
						String reversed = reverseDate(rs.getString(6));
						po.setTime(reversed);
						
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public List<EmailAlertResponce> getEmailAlertWithRange(Date fromDate, Date toDate) {
		List<EmailAlertResponce> listOfUser = null;
		try {  
			
			/*ystem.out.println("daaooooooooo");
               //2018-02-04 10:44:56
		        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss");
		        String FrmDate = formatter.format(fromDate);
		        String tooDate = formatter.format(toDate);
		       ////System.out.println(FrmDate);
		       //System.out.println(tooDate);*/
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	        String startDate = formatter.format(fromDate);
	        String endDate = formatter.format(toDate);
	        //System.out.println("From Date => "+startDate);
	        //System.out.println("To Date => "+endDate);
		       
		       String query = "SELECT count(e.process_order)as c,e. process_order,e.sso,\r\n"
						+ "e.user_name,e.reactor,date(e.time) as sent FROM email_alerts e\r\n"
						+ "where e.process='Raw Material Charging' and e.date BETWEEN '"+startDate+"' and '"+endDate+"' \r\n"
						+ "group by e.process_order;";
			
			
			/*String query = "select count(process_order)c,process_order,sso,user_name,process,reactor,time  from suez.email_alerts where time between '"+FrmDate+"' AND '"+tooDate+"'\r\n" + 
					"group by process_order having c >1;";*/
			
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<EmailAlertResponce>>() {
				public List<EmailAlertResponce> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<EmailAlertResponce> list = new ArrayList<EmailAlertResponce>();
					while (rs.next()) {
						EmailAlertResponce po = new EmailAlertResponce();
						po.setNoOfAlert(rs.getInt(1));
						po.setProcessOrder(rs.getLong(2));
						po.setSso(rs.getString(3));
						po.setUserName(rs.getString(4));
						//po.setProcess(rs.getString(5));
						po.setReactor(rs.getString(5));
						String reversed = reverseDate(rs.getString(6));
						po.setTime(reversed);
						/*//System.out.println(rs.getInt(1));
						po.setNoOfAlert(rs.getInt(1));
						po.setProcessOrder(rs.getLong(2));
						po.setSso(rs.getString(3));
						po.setUserName(rs.getString(4));
						po.setProcess(rs.getString(5));
						po.setReactor(rs.getString(6));
						po.setTime(rs.getString(7));*/
						
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public List<EmailAlertResponce> getPackingAlerts() {
		List<EmailAlertResponce> listOfAlerts = null;
		try {

			//String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String query = "SELECT count(e.process_order)as c,e. process_order,e.sso,\r\n"
					+ "e.user_name,e.reactor,date(e.time) as sent FROM email_alerts e\r\n"
					+ "where e.process='Packing' group by e.process_order;";
			
			/*String query = "SELECT count(e.process_order)c,e.process_order, e.sso, e.user_name,\r\n"
					+ "e.reactor,date(e.time) as sent FROM email_alerts e\r\n"
					+ "where process='Packing' group by e.process_order;";*/
			
			//String query = "select count(process_order)c,process_order,sso,user_name,process,reactor,time  from suez.email_alerts where time group by process_order having c >1; ;";
			listOfAlerts = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<EmailAlertResponce>>() {
				public List<EmailAlertResponce> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<EmailAlertResponce> list = new ArrayList<EmailAlertResponce>();
					while (rs.next()) {
						EmailAlertResponce po = new EmailAlertResponce();
						po.setNoOfAlert(rs.getInt(1));
						po.setProcessOrder(rs.getLong(2));
						po.setSso(rs.getString(3));
						po.setUserName(rs.getString(4));
						//po.setProcess(rs.getString(5));
						po.setReactor(rs.getString(5));
						String reversed = reverseDate(rs.getString(6));
						po.setTime(reversed);					
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfAlerts;
	}
	
	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}

	@Override
	public List<EmailAlertResponce> getPackingAlertsByDate(Date fromDate, Date toDate) {
		List<EmailAlertResponce> listOfAlerts = null;
		try {
			
			//2018-02-04 10:44:56
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	        String startDate = formatter.format(fromDate);
	        String endDate = formatter.format(toDate);
	        //System.out.println("From Date => "+startDate);
	        //System.out.println("To Date => "+endDate);
			//String query
			String query = "SELECT count(e.process_order)as c,e. process_order,e.sso,\r\n"
					+ "e.user_name,e.reactor,date(e.time) as sent FROM email_alerts e\r\n"
					+ "where e.process='Packing' and e.date BETWEEN '"+startDate+"' and '"+endDate+"' \r\n"
					+ "group by e.process_order;";			
			
			listOfAlerts = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<EmailAlertResponce>>() {
				public List<EmailAlertResponce> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<EmailAlertResponce> list = new ArrayList<EmailAlertResponce>();
					while (rs.next()) {
						EmailAlertResponce po = new EmailAlertResponce();
						po.setNoOfAlert(rs.getInt(1));
						po.setProcessOrder(rs.getLong(2));
						po.setSso(rs.getString(3));
						po.setUserName(rs.getString(4));
						po.setReactor(rs.getString(5));
						String reversed = reverseDate(rs.getString(6));
						po.setTime(reversed);					
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfAlerts;
	}

	@Override
	public List<EmailAlertResponce> getTemperature(long processOrder) {
		List<EmailAlertResponce> listOfAlerts = null;
		try {
			String query = "SELECT process_order,sso,user_name,reactor,date(time) as sent,"
					+ "temperature,DATE_FORMAT(time,'%r')as dates FROM email_alerts "
					+ "where process_order='"+processOrder+"' and process='Raw Material Charging'";
			
			/*String query = "SELECT temperature,DATE_FORMAT(time,'%r')as dates FROM email_alerts\r\n"
					+ "where process_order='"+processOrder+"' and process='Raw Material Charging'";*/
			
			listOfAlerts = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<EmailAlertResponce>>() {
				public List<EmailAlertResponce> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<EmailAlertResponce> list = new ArrayList<EmailAlertResponce>();
					while (rs.next()) {
						EmailAlertResponce po = new EmailAlertResponce();
						po.setProcessOrder(rs.getLong(1));
						po.setSso(rs.getString(2));
						po.setUserName(rs.getString(3));
						po.setReactor(rs.getString(4));
						String reversed = reverseDate(rs.getString(5));
						po.setDate(reversed);
						po.setTemperature(rs.getString(6));
						po.setTime(rs.getString(7));					
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfAlerts;
	}

	@Override
	public List<EmailAlertResponce> getRmTemperatureDetails(long processOrder) {
		List<EmailAlertResponce> listOfAlerts = null;
		try {
			String query = "SELECT process_order,sso,user_name,reactor,date(time) as sent,"
					+ "temperature,DATE_FORMAT(time,'%r')as dates FROM email_alerts "
					+ "where process_order='"+processOrder+"' and process='Raw Material Charging'";
			
			/*String query = "SELECT temperature,DATE_FORMAT(time,'%r')as dates FROM email_alerts\r\n"
					+ "where process_order='"+processOrder+"' and process='Raw Material Charging'";*/
			
			listOfAlerts = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<EmailAlertResponce>>() {
				public List<EmailAlertResponce> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<EmailAlertResponce> list = new ArrayList<EmailAlertResponce>();
					while (rs.next()) {
						EmailAlertResponce po = new EmailAlertResponce();
						po.setProcessOrder(rs.getLong(1));
						po.setSso(rs.getString(2));
						po.setUserName(rs.getString(3));
						po.setReactor(rs.getString(4));
						String reversed = reverseDate(rs.getString(5));
						po.setDate(reversed);
						po.setTemperature(rs.getString(6));
						po.setTime(rs.getString(7));					
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfAlerts;
	}

}
