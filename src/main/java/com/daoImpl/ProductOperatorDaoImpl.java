package com.daoImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.dao.ProductOperatorDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.Alerts;
import com.entity.ContainerTable;
import com.entity.MultiRoles;
import com.entity.ProcessTracker;
import com.entity.ProcessorderAssociatedPo;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.QualityCheckResult;
import com.entity.RawmaterialTable;
import com.entity.Reactor;
import com.entity.Remark;
import com.entity.Role;
import com.entity.Status;
import com.entity.User;
import com.response.LocationRm;
import com.response.ManagementReportResponse;
import com.response.ProcessOrderRMResponse;
import com.response.QualityCheckResponce;
import com.response.RepackingOrderRMResponse;

@Repository
public class ProductOperatorDaoImpl extends NamedParameterJdbcDaoSupportClass implements ProductOperatorDao {

	public List<ProcessorderProd> displayProcessOrders() {
		List<ProcessorderProd> listOfUser = null;
		try {

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String query = "SELECT p.processorder_prod_number,prod.product_number,prod.product_description,p.batch_number,p.batch_date,s.status_description,s.status_id,p.total_quantity,p.volume,p.is_associated,p.is_selected,p.reactor,p.sift\r\n"
					+ "					from processorder_prod p \r\n"
					+ "                    inner join product_table prod on prod.product_number=p.product_number\r\n"
					+ "					inner join status s on s.status_id=p.status  \r\n"
					+ "					where  s.status_id=8 or s.status_id=15 or s.status_id=18  order by p.batch_date asc ;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessorderProd>>() {
				public List<ProcessorderProd> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
					while (rs.next()) {
						ProcessorderProd po = new ProcessorderProd();
						po.setProcessorderProdNumber(rs.getLong(1));
						ProductTable pro = new ProductTable();
						pro.setProductNumber(rs.getLong(2));
						pro.setProductDescription(rs.getString(3));
						po.setProductTable(pro);
						po.setBatchNumber(rs.getString(4));
						po.setBatchDate( DateFormator(rs.getString(5)) );
						po.setPdfDate(reverseDate1(rs.getString(5)));
						po.setIsSelected(rs.getInt(11));
						po.setReactor(rs.getString(12));
						po.setSift(rs.getInt(13));
						Status s = new Status();
						s.setStatusDescription(rs.getString(6));
						s.setStatusId(rs.getInt(7));
						po.setStatus(s);
						po.setTotalQuantity(rs.getString(8));
						po.setVolume(rs.getString(9));
                        po.setIsAssociated(rs.getInt(10));
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	public List<ProcessOrderRMResponse> processOrderRecipeList(long a) {
		//System.out.println("hiiiii form dao");
		List<ProcessOrderRMResponse> listOfUser = null;
		try {

			String query = "SELECT por.process_order_rm,po.processorder_prod_number,pro.product_number,pro.product_description,por.rawmaterial_number,rm.rawmaterial_description ,por.type,por.phase,por.quantity,por.location,por.batch_number,por.quantity_taken,por.status,po.extra_rm\r\n"
					+ "from process_order_rm por \r\n"
					+ "inner join processorder_prod po on po.processorder_prod_number=por.process_order_number\r\n"
					+ "inner join product_table pro on pro.product_number=por.product_number \r\n"
					+ "inner join rawmaterial_table rm on rm.rawmaterial_number=por.rawmaterial_number \r\n"
					+ "where  process_order_number='" + a + "'  order by por.process_order_rm;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessOrderRMResponse>>() {
				public List<ProcessOrderRMResponse> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessOrderRMResponse> list = new ArrayList<ProcessOrderRMResponse>();
					while (rs.next()) {
						ProcessOrderRMResponse ps = new ProcessOrderRMResponse();
						ps.setProcessOrderRm(rs.getInt(1));
						ProcessorderProd pop = new ProcessorderProd();
						pop.setProcessorderProdNumber(rs.getLong(2));
						ps.setProcessorderProd(pop);
						ProductTable pt = new ProductTable();
						pt.setProductNumber(rs.getLong(3));
						pt.setProductDescription(rs.getString(4));
						ps.setProductTable(pt);
						RawmaterialTable rm = new RawmaterialTable();
						rm.setRawmaterialNumber(rs.getLong(5));
						rm.setRawmaterialDescription(rs.getString(6));
						ps.setRawmaterialTable(rm);
						ps.setType(rs.getString(7));
						ps.setPhase(rs.getString(8));
						//System.out.println("Quantiiiiiiiiiiiiiiiiiiii"+rs.getString(9));
						ps.setQuantity(rs.getString(9));
						Status s=new Status();
						s.setStatusId(rs.getInt(13));
						ps.setStatus(s);
						ps.setExtraRm(rs.getString(14));
						String location = rs.getString(10);
						String batchno = rs.getString(11);
						String quantityTaken = rs.getString(12);
					    if(location != null || batchno != null || quantityTaken != null)
						{	
					    	String[] locationList = location.split(",");
					    	String[] batchnoList = batchno.split(",");
					    	String[] quantityTakenList = quantityTaken.split(",");
					    	List<LocationRm> list1 =new ArrayList<LocationRm>();
					    	for(int i=0;i<locationList.length; i++){
					    		LocationRm  lrm=new LocationRm();
					    		lrm.setLocation(locationList[i]);
					    		lrm.setBatchNumber(batchnoList[i]);
					    		lrm.setQuantityTaken(quantityTakenList[i]);
								list1.add(lrm);
					    	}
				
						ps.setLocationRm(list1);
						
						}
						else {
						ps.setLocationRm(null);
						}
						list.add(ps);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;
	}

	

	public int updateStatus(Long processOrder, int status, String sso) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		
		String timeDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
		String query = "update processorder_prod set status=:status WHERE processorder_prod_number=:processorder_prod_number;";

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("status", status);
		param.addValue("processorder_prod_number", processOrder);
		getNamedParameterJdbcTemplate().update(query, param);
		
		if(status == 18 ){
			
			try {

				String query1 = "UPDATE status_info SET quality_check = :time,quality_checked_by = :ssoid,status_id = :status WHERE processorder_prodNo = :poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		} else if(status == 15){
			
			try {

				String query1 = "UPDATE status_info SET rm_charge = :time,rm_charged_by = :ssoid,status_id = :status WHERE processorder_prodNo = :poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		}else if(status == 14){
			
			try {

				String query1 = "UPDATE status_info SET packing = :time,packed_by = :ssoid,status_id = :status WHERE processorder_prodNo = :poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
			
			try {
				
				String quantiyProduced = getQuantity( processOrder);
				//System.out.println("Quantity =>" + quantiyProduced);

				String query1 = "update processorder_prod set packed_date=:date, packed_time=:time, actual_quantity_afterPacking=:quantity WHERE processorder_prod_number=:poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("quantity", quantiyProduced);
				param1.addValue("date", timeDate);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		}else if(status == 16){
			
			try {

				String query1 = "UPDATE status_info SET rm_posting = :time,rm_posted_by = :ssoid,status_id = :status WHERE processorder_prodNo = :poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		}
		
		return 0;
	}

	

	@Override
	public int updateProcessProgress(ProcessTracker processTrackerTable) {


		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		try {
			String query = "INSERT INTO process_tracker( processorder_number,process_id,ssoid,rawmaterial_number,process_start_time,process_end_time,status_id,reactor,sift,is_repack\r\n"
					+ "	) \r\n"
					+ "	VALUES(:processorder_number,:process_id,:ssoid,:rawmaterial_number,:process_start_time,:process_end_time,:status_id,:reactor,:sift,:isRepack); ";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processorder_number", processTrackerTable.getProcessorderNumber());
			param.addValue("process_id", 2);
			param.addValue("ssoid", processTrackerTable.getUser().getSsoid());
			param.addValue("rawmaterial_number", processTrackerTable.getRawmaterialNumber() != null ? processTrackerTable.getRawmaterialNumber() : null);
			param.addValue("process_start_time", timeStamp);
			param.addValue("process_end_time", timeStamp);
			param.addValue("status_id", processTrackerTable.getStatus().getStatusId());
			param.addValue("reactor", processTrackerTable.getReactor());
			param.addValue("sift", processTrackerTable.getSift());
			param.addValue("isRepack", processTrackerTable.getIsRepack());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			getNamedParameterJdbcTemplate().update(query, param, keyHolder);
			processTrackerTable.setProcessTrackerId(keyHolder.getKey().intValue());			

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		try {
			
			String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
					+ "WHERE processorder_pckg_number = :poNumber;";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("poNumber", processTrackerTable.getProcessorderNumber());
			param.addValue("reactor", processTrackerTable.getReactor());
			param.addValue("sift", processTrackerTable.getSift());
			param.addValue("selected", 1);
			getNamedParameterJdbcTemplate().update(query, param);			

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		
		/*try {
			
			String query = "UPDATE status_info SET rm_charge = :chargeTime,rm_charged_by = :ssoid,status_id = :status WHERE processorder_prodNo = :poNumber;";

			String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
					+ "WHERE processorder_pckg_number = :poNumber;";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("poNumber", processTrackerTable.getProcessorderNumber());
			param.addValue("chargeTime", timeStamp);
			param.addValue("ssoid", processTrackerTable.getUser().getSsoid());
			param.addValue("status", processTrackerTable.getStatus().getStatusId());
			//param.addValue("reactor", processTrackerTable.getReactor());
			//param.addValue("sift", processTrackerTable.getSift());
			//param.addValue("selected", 1);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}*/
		return processTrackerTable.getProcessTrackerId();

	}

	@Override
	public List<User> listofPo() {
		List<User> listOfUser = null;
		try {

			String query = "SELECT ssoid,user_name FROM user where role_id=4 and status_id=11;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<User>>() {
				public List<User> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<User> list = new ArrayList<User>();
					while (rs.next()) {
						User user = new User();
						user.setSsoid(rs.getString(1));
						user.setUserName(rs.getString(2));
						list.add(user);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public List<ProcessorderPckg> processOrderForPacking(long processOrderProdNo) {
		
		List<ProcessorderPckg> processOrderPack = null;
		String query = "SELECT * FROM processorder_pckg where processorder_pckg_number in (SELECT pakage_process_number FROM associate where product_process_number='"
				+ processOrderProdNo + "') and status=9;";

		try {

			processOrderPack = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg processorderPckg = new ProcessorderPckg();
								processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
								processorderPckg.setProductNumber(rs.getLong(2));
								processorderPckg.setBatchNumber(rs.getString(4));
								processorderPckg.setBatchDate(DateFormator(rs.getString(5)) /*rs.getString(5)*/);
								processorderPckg.setTotalQuantity(rs.getLong(6));
								processorderPckg.setWghtPckg(rs.getLong(7));
								processorderPckg.setPackingStartTime(rs.getString(30));
								Status s = new Status();
								s.setStatusId(rs.getInt(8));
								processorderPckg.setStatus(s);
								ContainerTable ct = new ContainerTable();
								ct.setContainerNumber(rs.getLong(9));
								processorderPckg.setContainerTable(ct);
								processorderPckg.setVolume(rs.getString(10));
								processorderPckg.setSixmlNumber(rs.getString(11));
								processorderPckg.setIsAssociated(rs.getInt(12));
								processorderPckg.setProductName(rs.getString(3));
								processorderPckg.setMaterialMemo(rs.getString(3));

								list.add(processorderPckg);
							}
							for (ProcessorderPckg var : list) 
							{ 
							    //System.out.println("hi product or"+var.getProcessorderPckgNumber());
							}
							return list;

						}
					});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processOrderPack;

	}

	@Override
	public List<ProcessorderPckg> PackingOrderInfo(long a) {

		List<ProcessorderPckg> processOrderPack = null;
	

		String query = "SELECT pp.processorder_pckg_number,pp.product_number,pp.batch_number,"
				+ "pp.batch_date,pp.total_quantity,pp.wght_pckg,pp.status,pp.container_number,"
				+ "pp.volume,pp.sixml_number,pp.is_associated,pp.product_name,pp.process_quant,"
				+ "ct.container_type,ct.container_phase,ct.container_quant,ct.container_description,pp.material_memo "
				+ "FROM processorder_pckg pp,container_table ct  " + "where processorder_pckg_number = '" + a
				+ "' and pp.container_number=ct.container_number;";

		try {

			processOrderPack = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg processorderPckg = new ProcessorderPckg();
								processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
								processorderPckg.setProductNumber(rs.getLong(2));
								processorderPckg.setBatchNumber(rs.getString(3));
								processorderPckg.setBatchDate(DateFormator(rs.getString(4)) );
								processorderPckg.setTotalQuantity(rs.getLong(5));
								processorderPckg.setWghtPckg(rs.getLong(6));
								Status s = new Status();
								s.setStatusId(rs.getInt(7));
								processorderPckg.setStatus(s);
								ContainerTable ct = new ContainerTable();
								ct.setContainerNumber(rs.getLong(8));
								ct.setContainerType(rs.getString(14));
								ct.setContainerPhase(rs.getString(15));
								ct.setContainerQuant(rs.getString(16));
								ct.setContainerDescription(rs.getString(17));
								processorderPckg.setContainerTable(ct);
								processorderPckg.setVolume(rs.getString(9));
								processorderPckg.setSixmlNumber(rs.getString(10));
								processorderPckg.setIsAssociated(rs.getInt(11));
								processorderPckg.setProductName(rs.getString(12));
								processorderPckg.setProcessQuant(rs.getString(13));
								processorderPckg.setMaterialMemo(rs.getString(18));
								list.add(processorderPckg);
							}
							return list;

						}
					});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processOrderPack;

	}

	@Override
	public List<ProcessorderProd> AllProcessOrderList() {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			final Calendar cal = Calendar.getInstance();

			String localDate = dateFormat.format(cal.getTime());

			String query = "select p.processorder_prod_number,prod.product_number,"
					+ " prod.product_description,p.total_quantity,"
					+ " p.batch_number,p.batch_date,p.status,s.status_description "
					+ " from processorder_prod p inner join product_table prod on p.product_number=prod.product_number "
					+ " inner join status s on  s.status_id=p.status " + " where batch_date='" + localDate + "'";

			List<ProcessorderProd> dailyProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processorderprod = new ProcessorderProd();
								processorderprod.setProcessorderProdNumber(rs.getLong(1));
								ProductTable productTable = new ProductTable();
								productTable.setProductNumber(rs.getLong(2));
								productTable.setProductDescription(rs.getString(3));
								processorderprod.setProductTable(productTable);
								processorderprod.setTotalQuantity(rs.getString(4));
								processorderprod.setBatchNumber(rs.getString(5));
								processorderprod.setBatchDate(DateFormator(rs.getString(6)) );
								Status status = new Status();
								status.setStatusId(rs.getInt(7));
								status.setStatusDescription(rs.getString(8));
								processorderprod.setStatus(status);
								list.add(processorderprod);
							}
							return list;
						}
					});
			return dailyProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<ManagementReportResponse> getWeeklyProcessOrders() {
		try {
			String query = "select pr.processorder_prod_number,p.product_number, p.product_description, pr.batch_date, "
					+ " sum(pr.total_quantity) as quantity " + " from processorder_prod pr "
					+ " inner join product_table p " + " on  p.product_number=pr.product_number "
					+ " where  YEAR(pr.batch_date)=  YEAR(CURDATE()) and pr.status=2 "
					+ " group by  p.product_description";
			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> productwiseReports = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {

						@Override
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse productwiseReport = new ManagementReportResponse();
							productwiseReport.setProcessOrderProdNumber(rs.getLong(1));
							productwiseReport.setProductnumber(rs.getLong(2));
							productwiseReport.setProductDescription(rs.getString(3));
							productwiseReport.setBatchDate(DateFormator(rs.getString(4)) );
							productwiseReport.setSum(rs.getLong("quantity"));
							return productwiseReport;

						}
					});
			return productwiseReports;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	@Override
	public int SaveprocessAssociatedwithpo(List<ProcessorderAssociatedPo> list) {

		String sql = "\r\n"
				+ "INSERT INTO processorder_associated_po(`processorder_prod_number`,`ssoid`) VALUES (?,?);";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {

			public int getBatchSize() {
				return list.size();
			}

			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ProcessorderAssociatedPo processorderAssociatedPo = list.get(i);
				ps.setLong(1, processorderAssociatedPo.getProcessorderProd().getProcessorderProdNumber());
				ps.setString(2, processorderAssociatedPo.getUser().getSsoid());

			}

		});
		return 0;

	}

	@Override
	public int updateStatusForPacking(Long processOrder, int status, String sso, String quantity, int pails, String remarks) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		String query = "update processorder_pckg set status=:status WHERE processorder_pckg_number=:processorder_pckg_number;";

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("status", status);
		param.addValue("processorder_pckg_number", processOrder);
		param.addValue("time", timeStamp);
		param.addValue("completedDate", date);
		param.addValue("quantity", quantity);
		param.addValue("pails", pails);
		param.addValue("remarks", remarks);
		getNamedParameterJdbcTemplate().update(query, param);
		
if(status == 18 ){
			
			try {

				String query1 = "UPDATE repacking_status_info SET quality_check = :time,quality_checked_by = :ssoid,status_id = :status WHERE processorder_packNo = :poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		} else if(status == 14){
			
			try {

				String query1 = "UPDATE repacking_status_info SET packing = :time,packed_by = :ssoid,status_id = :status WHERE processorder_packNo = :poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
			
			try {
				//System.out.println("Update started");
				String date1 = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

				String query1 = "update processorder_pckg set packing_end_time=:time, packing_completed_date=:completedDate, actual_quantity_afterPacking=:quantity, actual_pallets_used=:pails, packing_remarks=:remarks WHERE processorder_pckg_number=:poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				//param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				param1.addValue("completedDate", date1);
				param1.addValue("quantity", quantity);
				param1.addValue("pails", pails);
				param1.addValue("remarks", remarks);
				getNamedParameterJdbcTemplate().update(query1, param1);
				//System.out.println("Packing details updated");

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		}else if(status == 16){
			
			try {

				String query1 = "UPDATE repacking_status_info SET rm_posting = :time,rm_posted_by = :ssoid,status_id = :status WHERE processorder_packNo = :poNumber;";

				/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
						+ "WHERE processorder_pckg_number = :poNumber;";*/

				MapSqlParameterSource param1 = new MapSqlParameterSource();
				param1.addValue("poNumber", processOrder);
				param1.addValue("time", timeStamp);
				param1.addValue("ssoid", sso);
				param1.addValue("status", status);
				getNamedParameterJdbcTemplate().update(query1, param1);

			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		}
		
		/*try {

			String query1 = "UPDATE status_info SET rm_charge = :chargeTime,rm_charged_by = :ssoid,status_id = :status WHERE processorder_prodNo = :poNumber;";

			String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
					+ "WHERE processorder_pckg_number = :poNumber;";

			MapSqlParameterSource param1 = new MapSqlParameterSource();
			param1.addValue("poNumber", processOrder);
			param1.addValue("chargeTime", timeStamp);
			param1.addValue("ssoid", );
			param1.addValue("status", status);
			// param.addValue("reactor", processTrackerTable.getReactor());
			// param.addValue("sift", processTrackerTable.getSift());
			// param.addValue("selected", 1);
			getNamedParameterJdbcTemplate().update(query1, param1);

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}*/
		return 0;

	}

	@Override
	public int updateStatusForRM(Long processOrder, int rMNumber, int status) {
		String query = "update process_order_rm set status=:status WHERE process_order_number=:process_order_number and rawmaterial_number=:rawmaterial_number;";

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("status", status);
		param.addValue("process_order_number", processOrder);
		param.addValue("rawmaterial_number", rMNumber);
		return getNamedParameterJdbcTemplate().update(query, param);
	}

	@Override
	public List<QualityCheckResponce> getQualityData(long productNo) {
		List<QualityCheckResponce> QualityMasterlist = null;
		/*String query = "SELECT * FROM suez.quality_master 
				where id in (select quality_id from product_qualities where product_number
                in (select product_number from processorder_prod where processorder_prod_number='1002135898') )
                order by type;";
*/
		String query = "SELECT * FROM quality_master order by type" ; 

		try {

			QualityMasterlist = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<QualityCheckResponce>>() {
						public List<QualityCheckResponce> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<QualityCheckResponce> list = new ArrayList<QualityCheckResponce>();
							while (rs.next()) {
								QualityCheckResponce qualityMaster = new QualityCheckResponce();
								qualityMaster.setId(rs.getInt(1));
								qualityMaster.setCharNumber(rs.getString(2));
								qualityMaster.setProductNumber(rs.getLong(3));
								
								qualityMaster.setDiscription(rs.getString(4));
								qualityMaster.setMethod(rs.getString(5));
								String str1 = rs.getString(6);
								 if(str1 != null )
									{	
									 List<String> Range1 = Arrays.asList(str1.split(","));
									 qualityMaster.setRange1(Range1);
									}
									else {
										qualityMaster.setRange1(null);
									}
								String str2 = rs.getString(7);
								 if(str2 != null )
									{	
									 List<String> Range2 = Arrays.asList(str2.split(","));
									 qualityMaster.setRange2(Range2);
									}
									else {
										qualityMaster.setRange2(null);
									}
								
								qualityMaster.setType(rs.getInt(8));
						

								list.add(qualityMaster);
							}
							return list;

						}
					});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return QualityMasterlist;

	}
	
	
	public int SaveQualityInfo(List<QualityCheckResponce> list) {

		String sql = "\r\n"
				+ "INSERT INTO quality_check(`quality_id`,`discription`,`method`,`result`,`process_order_numbers`) VALUES (?,?,?,?,?);";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {

			public int getBatchSize() {
				return list.size();
			}

			public void setValues(PreparedStatement ps, int i) throws SQLException {
				QualityCheckResponce QualityCheckResponce = list.get(i);
				ps.setLong(1, QualityCheckResponce.getId());
				ps.setString(2, QualityCheckResponce.getDiscription());
				ps.setString(3, QualityCheckResponce.getMethod());
				ps.setString(4, QualityCheckResponce.getResult());
				ps.setString(5, QualityCheckResponce.getProcessOrder());

			}

		});
		return 0;

	}

	@Override
	public int updateProcessOrderProd(Long processOrder, String reactor, int status) {
		String query = "UPDATE processorder_prod\r\n" + 
				"SET\r\n" + 
				"reactor = :reactor,is_selected=:selected,\r\n" + 
				"sift = :sift\r\n" + 
				"WHERE processorder_prod_number = :process_order_number;" ; 
				

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("reactor", reactor);
		param.addValue("sift", status);
		param.addValue("process_order_number", processOrder);
		param.addValue("selected", 1);
		return getNamedParameterJdbcTemplate().update(query, param);
	}

	@Override
	public List<ProcessorderProd> displayProcessOrdersforInventory() {
		List<ProcessorderProd> listOfUser = null;
		try {
			//

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String query = "SELECT p.processorder_prod_number,prod.product_number,prod.product_description,p.batch_number,p.batch_date,s.status_description,s.status_id,p.total_quantity,p.volume\r\n"
					+ "					from processorder_prod p \r\n"
					+ "                    inner join product_table prod on prod.product_number=p.product_number\r\n"
					+ "					inner join status s on s.status_id=p.status  \r\n"
					+ "					where  s.status_id=14  order by p.batch_date asc ;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessorderProd>>() {
				public List<ProcessorderProd> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
					while (rs.next()) {
						ProcessorderProd po = new ProcessorderProd();
						po.setProcessorderProdNumber(rs.getLong(1));
						ProductTable pro = new ProductTable();
						pro.setProductNumber(rs.getLong(2));
						pro.setProductDescription(rs.getString(3));
						po.setProductTable(pro);
						po.setBatchNumber(rs.getString(4));
						po.setBatchDate(DateFormator(rs.getString(5)) );
						Status s = new Status();
						s.setStatusDescription(rs.getString(6));
						s.setStatusId(rs.getInt(7));
						po.setStatus(s);
						po.setTotalQuantity(rs.getString(8));
						po.setVolume(rs.getString(9));

						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public List<ProcessorderProd> dailyProcessOrder() {
		List<ProcessorderProd> listOfUser = null;
		try {
		

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			
			String query = "SELECT p.processorder_prod_number,prod.product_number,\r\n"
					+ "prod.product_description,p.batch_number,p.batch_date,s.status_description,\r\n"
					+ "s.status_id,p.total_quantity,p.volume,p.reactor from processorder_prod p \r\n"
					+ "inner join product_table prod on prod.product_number=p.product_number \r\n"
					+ "inner join status s on s.status_id=p.status \r\n"
					+ "where p.status in(15 , 18 , 14 , 8) and p.is_selected =1";
			
			/*String query = "SELECT p.processorder_prod_number,prod.product_number,prod.product_description,p.batch_number,p.batch_date,s.status_description,s.status_id,p.total_quantity,p.volume,p.reactor\r\n" + 
					"										from processorder_prod p\r\n" + 
					"					                    inner join product_table prod on prod.product_number=p.product_number\r\n" + 
					"										inner join status s on s.status_id=p.status \r\n" + 
					"									where p.completed_date='"+timeStamp+"' order by p.completed_time asc ;";*/
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessorderProd>>() {
				public List<ProcessorderProd> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
					while (rs.next()) {
						ProcessorderProd po = new ProcessorderProd();
						po.setProcessorderProdNumber(rs.getLong(1));
						ProductTable pro = new ProductTable();
						pro.setProductNumber(rs.getLong(2));
						pro.setProductDescription(rs.getString(3));
						po.setProductTable(pro);
						po.setBatchNumber(rs.getString(4));
						po.setBatchDate(DateFormator(rs.getString(5)) );
						po.setPdfDate(reverseDate1(rs.getString(5)));
						Status s = new Status();
						s.setStatusDescription(rs.getString(6));
						s.setStatusId(rs.getInt(7));
						po.setStatus(s);
						po.setTotalQuantity(rs.getString(8));
						po.setVolume(rs.getString(9));
						po.setReactor(rs.getString(10));

						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public int saveAlert(Alerts alert) {
		String query = "INSERT INTO `alerts`\r\n" + 
				"(\r\n" + 
				"`ssoId`,\r\n" + 
				"`process_order_number`,\r\n" + 
				"`status_id`,`generated_time`)\r\n" + 
				"VALUES\r\n" + 
				"(\r\n" + 
				":ssoId,\r\n" + 
				":process_order_number,\r\n" + 
				":status_id,:generated_time);" ; 
				

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("ssoId", alert.getUser().getSsoid());
		param.addValue("process_order_number", alert.getProcessOrderNumber());
		param.addValue("status_id", alert.getStatus().getStatusId());
		param.addValue("generated_time", timeStamp);
		return getNamedParameterJdbcTemplate().update(query, param);
	}

	@Override
	public List<Reactor> getReactorDetails() {
		List<Reactor> listOfUser = null;
		try {

			String query = "SELECT * from Reactor;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<Reactor>>() {
				public List<Reactor> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<Reactor> list = new ArrayList<Reactor>();
					while (rs.next()) {
						Reactor re = new Reactor();
						re.setReactorID(rs.getInt(1));
						re.setReactorName(rs.getString(2));
						re.setMaxThreashold(rs.getString(4));
						
						

						list.add(re);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public List<MultiRoles> getRoleList(String sSO) {
		List<MultiRoles> listOfUser = null;
		try {

			String query = "SELECT role from multi_roles where sso='"+sSO+"';";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<MultiRoles>>() {
				public List<MultiRoles> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<MultiRoles> list = new ArrayList<MultiRoles>();
					while (rs.next()) {
						MultiRoles re = new MultiRoles();
						re.setRole(rs.getInt(1));
						
						
						

						list.add(re);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public int InsertRemark(Remark remark) {


		try {
			String query = "INSERT INTO remark\r\n" + 
					"(\r\n" + 
					"Process_order,\r\n" + 
					"sso,\r\n" + 
					"remark,\r\n" + 
					"status_id,\r\n" + 
					"remarked_by)\r\n" + 
					"VALUES\r\n" + 
					"(\r\n" + 
					" :Processorder,\r\n" + 
					" :sso,\r\n" + 
					" :remark,\r\n" + 
					" :statusid,\r\n" + 
					" :remarkedby);; ";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			
			param.addValue("Processorder", remark.getProcessOrder());
			param.addValue("sso", remark.getSso());
			param.addValue("remark", remark.getRemark());
			param.addValue("statusid", remark.getStatusId());
			param.addValue("remarkedby", remark.getRemarkedBy());
			return getNamedParameterJdbcTemplate().update(query, param);
			

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		

	}
	
	
	public String DateFormator(String date) 
	{
		String s = date;
		String ar[] = s.split("-");
		String str2 = ar[2];
		str2 = str2.concat("-").concat(ar[1]).concat("-").concat(ar[0]);
		////System.out.println(str2);
		return str2;
		
	}

	@Override
	public int updateTracker(Long processOrder, int status) {
		
		String timeStamp = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
		String query = "UPDATE process_tracker\r\n" + 
				"SET process_end_time = :process_end_time\r\n" + 
				"WHERE processorder_number = :processorder_number and status_id= :status_id;";

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("status_id", status);
		param.addValue("process_end_time", timeStamp);
		param.addValue("processorder_number", processOrder);
		return getNamedParameterJdbcTemplate().update(query, param);
	}

	@Override
	public List<ProcessorderPckg> reapckingOrderDiaplay() {
		List<ProcessorderPckg> listOfUser = null;
		try {

			String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number,p.batch_date,s.status_description,s.status_id,p.total_quantity,p.volume,p.reactor,p.shift,p.is_selected\r\n" + 
					"						    from processorder_pckg p \r\n" + 
					"							inner join status s on s.status_id=p.status  \r\n" + 
					"							where s.status_id in(8 , 18 ) and p.is_repack = 1 order by p.batch_date asc ;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessorderPckg>>() {
				public List<ProcessorderPckg> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
					while (rs.next()) {
						ProcessorderPckg po = new ProcessorderPckg();
						po.setProcessorderPckgNumber(rs.getLong(1));
						po.setProductNumber(rs.getLong(2));
						po.setProductName(rs.getString(3));
						po.setBatchNumber(rs.getString(4));
						po.setBatchDate( DateFormator(rs.getString(5)));
						String dateForPdf = reverseDate1(rs.getString(5));
						po.setPdfDate(dateForPdf);
						po.setReactor(rs.getString(10));
						po.setShift(rs.getInt(11));
						po.setIsSelected(rs.getInt(12));
						Status s = new Status();
						s.setStatusDescription(rs.getString(6));
						s.setStatusId(rs.getInt(7));
						po.setStatus(s);
						po.setTotalQuantity(rs.getLong(8));
						po.setVolume(rs.getString(9));
						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public int SaveprocessAssociatedwithpoForRepack(List<ProcessorderAssociatedPo> list) {

		String sql = "\r\n"
				+ "INSERT INTO processorder_associated_po(`processorder_pckg_number`,`ssoid`) VALUES (?,?);";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {

			public int getBatchSize() {
				return list.size();
			}

			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ProcessorderAssociatedPo processorderAssociatedPo = list.get(i);
				ps.setLong(1, processorderAssociatedPo.getProcessorderProd().getProcessorderProdNumber());
				ps.setString(2, processorderAssociatedPo.getUser().getSsoid());

			}

		});
		return 0;

	}

	@Override
	public int insertQualityResult(QualityCheckResult qualityCheckResult) {
		String query = "INSERT INTO quality_check_result\r\n" + 
				"(\r\n" + 
				"process_order,\r\n" + 
				"product_number,\r\n" + 
				"material_memo,\r\n" + 
				"batch_no,\r\n" + 
				"sso,\r\n" + 
				"remark)\r\n" + 
				"VALUES\r\n" + 
				"(\r\n" + 
				":process_order,\r\n" + 
				":product_number,\r\n" + 
				":material_memo,\r\n" + 
				":batch_no,\r\n" + 
				":sso,\r\n" + 
				":remark);";

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("process_order", qualityCheckResult.getProcessOrder());
		param.addValue("product_number", qualityCheckResult.getProductNumber());
		param.addValue("material_memo", qualityCheckResult.getMaterialMemo());
		param.addValue("batch_no", qualityCheckResult.getBatchNo());
		param.addValue("sso", qualityCheckResult.getSso());
		param.addValue("remark", qualityCheckResult.getRemark());
		return getNamedParameterJdbcTemplate().update(query, param);
	}

	@Override
	public List<ProcessorderProd> processOrderDetailsForQuality(long a) {
		List<ProcessorderProd> listOfUser = null;
		try {

			String query = "SELECT po.processorder_prod_number,prod.product_number,\r\n" + 
					"prod.product_description,po.batch_number FROM processorder_prod po\r\n" + 
					"inner join product_table prod on po.product_number=prod.product_number\r\n" + 
					"where processorder_prod_number="+a+";";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessorderProd>>() {
				public List<ProcessorderProd> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
					while (rs.next()) {
						ProcessorderProd po = new ProcessorderProd();
						po.setProcessorderProdNumber(rs.getLong(1));
						ProductTable prodtbl =new ProductTable();
						prodtbl.setProductNumber(rs.getLong(2));
						prodtbl.setProductDescription(rs.getString(3));
						po.setProductTable(prodtbl);
						po.setBatchNumber(rs.getString(4));
						
					

						list.add(po);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public List<ProcessorderPckg> displayProcessOrdersforInventoryForRepacking() {
		List<ProcessorderPckg> listOfUser = null;
		try {

			String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,\r\n" + 
					"                 p.batch_number,p.batch_date,s.status_description,s.status_id,\r\n" + 
					"                 p.total_quantity,p.volume\r\n" + 
					"				from processorder_pckg p \r\n" + 
					"			    inner join status s on s.status_id=p.status  \r\n" + 
					"				where  s.status_id=14  and p.is_repack = 1 order by p.batch_date asc ; ";
		
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessorderPckg>>() {
				public List<ProcessorderPckg> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
					while (rs.next()) {
						ProcessorderPckg p = new ProcessorderPckg();
						p.setProcessorderPckgNumber(rs.getLong(1));
						p.setProductNumber(rs.getLong(2));
						p.setMaterialMemo(rs.getString(3));
						p.setBatchNumber(rs.getString(4));
						String reversedDate = DateFormator(rs.getString(5));
						p.setBatchDate(reversedDate);
						Status s =new Status();
						s.setStatusDescription(rs.getString(6));
						s.setStatusId(rs.getInt(7));
						p.setStatus(s);
						p.setTotalQuantity(rs.getLong(8));
						p.setVolume(rs.getString(9));
						list.add(p);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;

	}

	@Override
	public List<RepackingOrderRMResponse> RepackingOrderRecipeList(long a) {
		List<RepackingOrderRMResponse> listOfUser = null;
		try {

			String query = "SELECT processorder_pckg_number,product_number,\r\n" + 
					"        material_memo,total_quantity,location_stored,batchnumber_details,\r\n" + 
					"        quantity_given,status\r\n" + 
					"		from processorder_pckg \r\n" + 
					"	    where  processorder_pckg_number='"+a+"' ;";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<RepackingOrderRMResponse>>() {
				public List<RepackingOrderRMResponse> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<RepackingOrderRMResponse> list = new ArrayList<RepackingOrderRMResponse>();
					while (rs.next()) {
						RepackingOrderRMResponse ps = new RepackingOrderRMResponse();
						ps.setProcessorderPckgNumber(rs.getLong(1));
				        ps.setProductNumber(rs.getLong(2));
				        ps.setMaterialMemo(rs.getString(3));
				        ps.setTotalQuantity(rs.getString(4));
				        Status s=new Status();
				        s.setStatusId(rs.getInt(8));
				        ps.setStatus(s);
						String location = rs.getString(5);
						String batchno = rs.getString(6);
						String quantityTaken = rs.getString(7);
					    if(location != null || batchno != null || quantityTaken != null)
						{	
					    	String[] locationList = location.split(",");
					    	String[] batchnoList = batchno.split(",");
					    	String[] quantityTakenList = quantityTaken.split(",");
					    	List<LocationRm> list1 =new ArrayList<LocationRm>();
					    	for(int i=0;i<locationList.length; i++){
					    		LocationRm  lrm=new LocationRm();
					    		lrm.setLocation(locationList[i]);
					    		lrm.setBatchNumber(batchnoList[i]);
					    		lrm.setQuantityTaken(quantityTakenList[i]);
								list1.add(lrm);
					    	}
				
						ps.setLocationRm(list1);
						
						}
						else {
						ps.setLocationRm(null);
						}
						list.add(ps);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;
	}

	@Override
	public int RepackingOrderStatusChange(long repackingOrder, int status, String sso) {
		
		String timeStamp = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
		String query = "UPDATE `processorder_pckg`\r\n" + 
				"SET `status` = :status_id\r\n" + 
				"WHERE `processorder_pckg_number` = :repackingOrder;";

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("status_id", status);
		param.addValue("repackingOrder", repackingOrder);
		 getNamedParameterJdbcTemplate().update(query, param);
		
		try {

			String query1 = "UPDATE repacking_status_info SET rm_posting = :time,rm_posted_by = :ssoid,status_id = :status WHERE processorder_packNo = :poNumber;";

			/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
					+ "WHERE processorder_pckg_number = :poNumber;";*/

			MapSqlParameterSource param1 = new MapSqlParameterSource();
			param1.addValue("poNumber", repackingOrder);
			param1.addValue("time", timeStamp);
			param1.addValue("ssoid", sso);
			param1.addValue("status", status);
			getNamedParameterJdbcTemplate().update(query1, param1);

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		return  0;
	}

	@Override
	public List<ProcessorderPckg> RepackingOrderRecipee(long a) {
		List<ProcessorderPckg> listOfUser = null;
		try {

			String query = "SELECT * FROM processorder_pckg where processorder_pckg_number='"+a+"';";
			listOfUser = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<ProcessorderPckg>>() {
				public List<ProcessorderPckg> extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
					while (rs.next()) {
						ProcessorderPckg ps = new ProcessorderPckg();
						ps.setProcessorderPckgNumber(rs.getLong(1));
				        ps.setProductNumber(rs.getLong(2));
				        ps.setMaterialMemo(rs.getString(3));
				        ps.setBatchNumber(rs.getString(4));
				        Status s=new Status();
				        s.setStatusId(rs.getInt(8));
				        ps.setStatus(s);
						list.add(ps);

					}
					return list;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfUser;
	}
	
	public String reverseDate1(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1]  + splited[splited.length - 2] + splited[0];

		return reversedDate;

	}

	@Override
	public int insertPackingStartTime(Long processOrder) {
		
		String timeStamp = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
		try {

			String query = "UPDATE processorder_pckg SET packing_start_time = :time WHERE processorder_pckg_number = :poNumber;";

			/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
					+ "WHERE processorder_pckg_number = :poNumber;";*/

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("poNumber", processOrder);
			param.addValue("time", timeStamp);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		return 0;
	}

	public String getQuantity(Long processOrder) {
		//User user = null;
		String quantity = null;
		try {
			/*String query = "SELECT u.user_name,u.ssoid,u.email,u.phone_number,r.role_name,u.password from user u\r\n"
					+ "inner join role r on r.role_id=u.role_id\r\n" + "WHERE u.ssoid = '" + ssoid + "' ";*/
			
			String query = "select sum(actual_quantity_afterPacking) from processorder_pckg where processorder_prod_number='" + processOrder + "'";

			quantity = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<String>() {
				public String extractData(ResultSet rs) throws SQLException, DataAccessException {

					String q = null;

					while (rs.next()) {
						q = rs.getString(1);
					}
					return q;
				}
			});
			return quantity;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quantity;
	}
	
	

}
