package com.daoImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.PlannerDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.Role;
import com.entity.User;
import com.request.PlannerRequest;

@Repository
public class PlannerDaoImpl extends NamedParameterJdbcDaoSupportClass implements PlannerDao {

	@Override
	public int addPlan(PlannerRequest plan) {
		try {

			String query = "INSERT INTO planned_production(per_quarter ,per_day, per_week, quarter,\r\n"
					+ "year, working_days, working_dates) VALUES( :PerQuarter , :PerDay , :PerWeek , :Quarter,\r\n"
					+ ":Year , :WorkingDays,:workingDates );";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("PerQuarter", plan.getPerQuarter());
			param.addValue("PerDay", plan.getPerDay());
			param.addValue("PerWeek", plan.getPerWeek());
			param.addValue("Quarter", plan.getQuarter());
			param.addValue("Year", plan.getYear());
			param.addValue("WorkingDays", plan.getWorkingDays());
			param.addValue("workingDates", plan.getSelectedDates());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public PlannerRequest getPlan(int year, int quarter) {
		PlannerRequest planned = null;
		try {

			String query = "SELECT p.per_day ,p.per_quarter ,p.per_week ,p.working_days\r\n "
					+ "from planned_production p where p.year= " + year + " and p.quarter= " + quarter + " ";

			planned = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<PlannerRequest>() {
				public PlannerRequest extractData(ResultSet rs) throws SQLException, DataAccessException {

					PlannerRequest plan = new PlannerRequest();

					while (rs.next()) {
						plan.setPerDay(rs.getString(1));
						plan.setPerQuarter(rs.getString(2));
						plan.setPerWeek(rs.getString(3));
						plan.setWorkingDays(rs.getInt(4));
					}
					return plan;
				}
			});
			return planned;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return planned;
	}

	@Override
	public PlannerRequest findPlan(int year, int quarter) {
		PlannerRequest planned = null;
		try {

			String query = "SELECT  p.working_dates,p.per_quarter,p.per_day,p.per_week\r\n "
					+ "from planned_production p where p.year= " + year + " and p.quarter= " + quarter + " ";

			planned = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<PlannerRequest>() {
				public PlannerRequest extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<String> dates = null;
					PlannerRequest plan = new PlannerRequest();

					while (rs.next()) {
						String allDates = rs.getString(1);
						plan.setPerQuarter(rs.getString(2));
						plan.setPerDay(rs.getString(3));
						plan.setPerWeek(rs.getString(4));
						if (allDates != null) {
							dates = Arrays.asList(allDates.split(","));
							plan.setWorkingDates(dates);
						}

					}
					return plan;
				}
			});
			return planned;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return planned;
	}

	@Override
	public int updatePlan(PlannerRequest plan) {
		try {

			String query = "UPDATE planned_production SET per_quarter = '" + plan.getPerQuarter() + "' ,\r\n"
					+ " per_day = '" + plan.getPerDay() + "' ,per_week = '" + plan.getPerWeek() + "' ,working_days = '"
					+ plan.getWorkingDays() + "',\r\n" + "working_dates = '" + plan.getSelectedDates()
					+ "' WHERE quarter ='" + plan.getQuarter() + "' and year ='" + plan.getYear() + "' ";

			MapSqlParameterSource param = new MapSqlParameterSource();
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int addPlanForReport(List<String> dates,PlannerRequest plan) {
		
		String sql = "INSERT INTO planfor_report(per_quarter,per_day,quarter,year,\r\n"
				+ "working_days,working_dates) VALUES (?,?,?,?,?,?);";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {

			public int getBatchSize() {
				return dates.size();
			}

			public void setValues(PreparedStatement ps, int i) throws SQLException {
				String planDates = dates.get(i);
				
				ps.setString(1, plan.getPerQuarter());
				ps.setString(2, plan.getPerDay());
				ps.setLong(3, plan.getQuarter());
				ps.setInt(4, plan.getYear());
				ps.setInt(5, plan.getWorkingDays());
				ps.setString(6, planDates);
			}
		});
		return 0;
	}

	@Override
	public int deletePlan(PlannerRequest plan) {
		try {
			
			String query = "DELETE FROM planfor_report \r\n "
					+ "WHERE quarter ='" + plan.getQuarter() + "' and year ='" + plan.getYear() + "';";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
