package com.daoImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.AlertsDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.Alerts;
import com.entity.ContainerTable;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.Status;
import com.entity.User;
import com.response.ManagementQuarterProductionPlan;
import com.response.NotificationCounterResponse;
@Repository
public class AlertsDaoImpl extends NamedParameterJdbcDaoSupportClass implements AlertsDao {

	@Override
	public List<Alerts> alertsForAdmin() {
		try {
			
			String query = "select a.process_order_number,a.ssoid,DATE_FORMAT(a.generated_time, '%D %M %Y %T') as generated_time,a.status_name,s.status_id,"
		+ "s.status_description, u.user_name "
         +" from alerts a inner join user u on u.ssoid=a.ssoid inner join status s on s.status_id=a.status_id "
         +" where a.status_name = 'unseen' "
         +"  order by a.generated_time desc ";

			List<Alerts> alertsForAdmin = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<Alerts>>() {
				public List<Alerts> extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<Alerts> list = new ArrayList<Alerts>();
					while (rs.next()) {
						Alerts alerts = new Alerts();
						alerts.setProcessOrderNumber(rs.getString(1));
						User user=new User();
						user.setSsoid(rs.getString(2));
						user.setUserName(rs.getString(7));
						alerts.setUser(user);
						alerts.setGeneratedTime(rs.getString(3));
						alerts.setstatusName(rs.getString(4));
						Status status=new Status();
						status.setStatusId(rs.getInt(5));
						status.setStatusDescription(rs.getString(6));
						alerts.setStatus(status);
						list.add(alerts);
					}
					return list;
				}
			});
			return alertsForAdmin;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
			
	}

	@Override
	public int statusChangeForAdmin(String processOrderNumber, int statusId) {
		try {
			String query = "UPDATE alerts " + " SET status_name ='seen' "
			+ " WHERE process_order_number= '" + processOrderNumber+"' and status_id ="+statusId;
	    SqlParameterSource paramSource = null;
	       return getNamedParameterJdbcTemplate().update(query, paramSource);
	

} catch (Exception e) {
	e.printStackTrace();
	return 0;
}
	}

	
	@Override
	public List<Alerts> alertsForWarehouse() {
		try {
			String query = "select a.process_order_number,a.ssoid,a.generated_time,a.status_name,s.status_id,"
		+ "s.status_description, u.user_name "
         +" from alerts a inner join user u on u.ssoid=a.ssoid inner join status s on s.status_id=a.status_id "
         +" where a.status_name = 'unseen' and s.status_id=17 "
         +"  order by a.generated_time desc ";

			List<Alerts> alertsForWarehouse = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<Alerts>>() {
				public List<Alerts> extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<Alerts> list = new ArrayList<Alerts>();
					while (rs.next()) {
						Alerts alerts = new Alerts();
						alerts.setProcessOrderNumber(rs.getString(1));
						User user=new User();
						user.setSsoid(rs.getString(2));
						user.setUserName(rs.getString(7));
						alerts.setUser(user);
						alerts.setGeneratedTime(rs.getString(3));
						alerts.setstatusName(rs.getString(4));
						Status status=new Status();
						status.setStatusId(rs.getInt(5));
						status.setStatusDescription(rs.getString(6));
						alerts.setStatus(status);
						list.add(alerts);
					}
					return list;
				}
			});
			query ="select processorder_prod_number,status "
					+ "from processorder_prod where status=7 "
					+ "order by processorder_prod_number desc ";
			List<Alerts> alertsForWarehouse1 = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<Alerts>>() {
				public List<Alerts> extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<Alerts> list = new ArrayList<Alerts>();
					while (rs.next()) {
						Alerts alerts = new Alerts();
						alerts.setProcessOrderNumber(rs.getString(1));
						Status status=new Status();
						status.setStatusId(rs.getInt(2));
						alerts.setStatus(status);
						list.add(alerts);
					}
					return list;
				}
			});
			
			alertsForWarehouse1.addAll(alertsForWarehouse);
			
			return alertsForWarehouse1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int deleteProductionOrder(Long processOrderNumber, String remarks ,String ssoId) {
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		//System.out.println(ssoId);
		
		try {

			/*query="INSERT INTO process_tracker ( processorder_number, ssoid, process_start_time, process_end_time, status_id)\r\n" + 
					"	VALUES ('"+processOrderNumber+"', '"+ssoId+"', '"+timeStamp+"', '"+timeStamp+"', '24')";
			*/
			
			
			String query = "INSERT INTO delete_details(processorder_prod_number, product_number ,batch_number,batch_date,total_quantity ,volume)\r\n"
					+ "select processorder_prod_number,product_number,batch_number,batch_date ,total_quantity ,volume \r\n"
					+ "from processorder_prod where processorder_prod_number=" + processOrderNumber;
			SqlParameterSource paramSource = null;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "Update delete_details set remarks='" + remarks + "' ,ssoId ='"+ssoId+"'  where processorder_prod_number="
					+ processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from associate where product_process_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			/*query = "DELETE from process_order_rm_pckg where processorder_pckg_number in \r\n" + "( \r\n"
					+ "	select processorder_pckg_number from processorder_pckg where processorder_prod_number='"
					+ processOrderNumber + "')";
			getNamedParameterJdbcTemplate().update(query, paramSource);*/
			
			query = "DELETE from inventorycheck_details where processorder_prod_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "DELETE from status_info  where processorder_prodNo=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query ="DELETE FROM repacking_status_info \r\n" + 
					"WHERE processorder_packNo IN (SELECT \r\n" + 
					"        processorder_pckg_number\r\n" + 
					"    FROM\r\n" + 
					"        processorder_pckg\r\n" + 
					"    \r\n" + 
					"    WHERE\r\n" + 
					"processorder_prod_number = "+processOrderNumber+")";
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "DELETE from processorder_pckg where processorder_prod_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from process_order_rm where process_order_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from rework where processorder_prod_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from quality_check where process_order_numbers=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from processorder_associated_po where processorder_prod_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from process_tracker where processorder_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from email_alerts where process_order=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from processorder_prod where processorder_prod_number=" + processOrderNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			return 1;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	public List<ProcessorderProd> processOrdersForDeletion() {
		try {
			// String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			// batch_date='"+timeStamp+"' and
			String query="SELECT p.processorder_prod_number ,prod.product_number,prod.product_description, "
					+" p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity "
					+" from processorder_prod p inner join product_table prod on prod.product_number=p.product_number "
					+" inner join status s on s.status_id=p.status "
					+" where status in ( 7 , 8 )"
					+" order by p.batch_date, p.processorder_prod_number asc "; 
					

			List<ProcessorderProd> listOfProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(1));
								ProductTable prod = new ProductTable();
								prod.setProductNumber(rs.getLong(2));
								prod.setProductDescription(rs.getString(3));
								processOrderProd.setProductTable(prod);
								processOrderProd.setBatchNumber(rs.getString(4));
								processOrderProd.setBatchDate(rs.getString(5));
								Status s = new Status();
								s.setStatusDescription(rs.getString(6));
								processOrderProd.setStatus(s);
								processOrderProd.setTotalQuantity(rs.getString(7));
								list.add(processOrderProd);
							}
							return list;
						}
					});
			return listOfProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public int deletePackagingOrder(Long processorderPckgNumber, String remarks, String ssoId) {
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		//System.out.println(ssoId);
		
		try {

			String query = "INSERT INTO delete_details(processorder_prod_number, product_number ,batch_number,batch_date,total_quantity ,volume)\r\n"
					+ "select processorder_pckg_number,product_number,batch_number,batch_date ,process_quant ,volume \r\n"
					+ "from processorder_pckg where processorder_pckg_number=" + processorderPckgNumber;
			SqlParameterSource paramSource = null;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "Update delete_details set remarks='" + remarks + "' ,ssoId ='"+ssoId+"'  where processorder_prod_number="
					+ processorderPckgNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from associate where pakage_process_number=" + processorderPckgNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from inventorycheck_details where processorder_pckg_number=" + processorderPckgNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "DELETE from status_info  where processorder_prodNo=" + processorderPckgNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "DELETE from repacking_status_info where processorder_packNo=" + processorderPckgNumber; 
			getNamedParameterJdbcTemplate().update(query, paramSource);

			query = "DELETE from processorder_pckg where processorder_pckg_number=" + processorderPckgNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);

						return 1;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public List<ProcessorderPckg> packagingOrdersForDeletion() {
		
		List<ProcessorderPckg> processOrderPack = null;
		String query = "SELECT processorder_pckg_number,product_number,material_memo,batch_number, date_format(batch_date,'%d-%m-%Y'),total_quantity,\r\n" + 
				"wght_pckg,status,container_number,volume,sixml_number,is_associated,\r\n" + 
				"process_quant,processorder_prod_number,product_name\r\n" + 
				"FROM processorder_pckg  where status in ( 7 , 8, 9, 23 )\r\n" + 
				"and is_associated = 0 "
				+ " order by batch_date, processorder_pckg_number asc";

		try {

			processOrderPack = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg processorderPckg = new ProcessorderPckg();
								processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
								processorderPckg.setProductNumber(rs.getLong(2));
								processorderPckg.setBatchNumber(rs.getString(4));
								processorderPckg.setBatchDate(rs.getString(5));
								processorderPckg.setTotalQuantity(rs.getLong(6));
								processorderPckg.setWghtPckg(rs.getLong(7));
								Status s = new Status();
								s.setStatusId(rs.getInt(8));
								processorderPckg.setStatus(s);
								ContainerTable ct = new ContainerTable();
								ct.setContainerNumber(rs.getLong(9));
								processorderPckg.setContainerTable(ct);
								processorderPckg.setVolume(rs.getString(10));
								processorderPckg.setSixmlNumber(rs.getString(11));
								processorderPckg.setIsAssociated(rs.getInt(12));
								processorderPckg.setMaterialMemo(rs.getString(3));
								processorderPckg.setProcessQuant(rs.getString(13));
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(14));
								processorderPckg.setProcessorderProd(processOrderProd);
								processorderPckg.setProductName(rs.getString(15));

								list.add(processorderPckg);
							}
							return list;

						}
					});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processOrderPack;

	}

	@Override
	public int smsAlertForWHInventory(String processOrderNumber, String ssoId) {
		
		String query ="SELECT user_name ,ssoid FROM user where ssoid ='"+ssoId+"'";
		User alertsForWarehouse = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<User>() {
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {

				User user = new User();
				while (rs.next()) {
					user.setUserName(rs.getString(1));
					user.setSsoid(rs.getString(2));
					
				}
				return user;
			}
		});
		
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			String Message="Inventory Check Completed for ProcessOrder Number  "+processOrderNumber+"  by UserName : "+alertsForWarehouse.getUserName()+"SsoId :  "+alertsForWarehouse.getSsoid()+" at "+timeStamp;
			sendSMS(Message);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
		
	}
	
	public static void sendSMS(String Message) throws MalformedURLException, IOException {
		
		// Construct data
		String apiKey = "apikey=" + "0GQN4+NRjKc-We5wnNN7NBcBoMRNV1ZdI682UHhuxJ";
		String message = "&message=" + Message;
		String sender = "&sender=" + "TXTLCL";
		String numbers = "&numbers=" + "919731866033";
		
		// Send data
		HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
		String data = apiKey + numbers + message + sender;
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
		conn.getOutputStream().write(data.getBytes("UTF-8"));
		final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		final StringBuffer stringBuffer = new StringBuffer();
		String line;
		while ((line = rd.readLine()) != null) {
			stringBuffer.append(line);
		}
		rd.close();
		
		//System.out.println(stringBuffer.toString());
	
}

	@Override
	public int smsAlertForCharging(String processOrderNumber, String ssoId) {

		String query ="SELECT user_name ,ssoid FROM user where ssoid ='"+ssoId+"'";
		User alertsForWarehouse = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<User>() {
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {

				User user = new User();
				while (rs.next()) {
					user.setUserName(rs.getString(1));
					user.setSsoid(rs.getString(2));
					
				}
				return user;
			}
		});
		
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			String Message="Blending Started for ProcessOrder Number"+processOrderNumber+" by   UserName : "+alertsForWarehouse.getUserName()+"SsoId : "+alertsForWarehouse.getSsoid()+" at "+timeStamp;
			sendSMS(Message);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return 1;

	}

	@Override
	public NotificationCounterResponse notificationCountForPo() {
		NotificationCounterResponse response1 = null;
		NotificationCounterResponse response2 = null;
		NotificationCounterResponse response3= null;
		NotificationCounterResponse response4=null;
		try {
			String query="select count(processorder_prod_number) from processorder_prod where status=8 or status=15 or status=18";

			response1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<NotificationCounterResponse>() {
						public NotificationCounterResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							NotificationCounterResponse reponseVal = new NotificationCounterResponse();

							while (rs.next()) {
								reponseVal.setProcessorderCount(rs.getInt(1));
							}
							return reponseVal;
						}
					});
			query ="select count(processorder_prod_number) from processorder_prod where status=14 ";
			response2 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<NotificationCounterResponse>() {
						public NotificationCounterResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							NotificationCounterResponse reponseVal = new NotificationCounterResponse();

							while (rs.next()) {
								reponseVal.setRmPostingCount(rs.getInt(1));
							}
							return reponseVal;
						}
					});
			
			query ="select count(processorder_pckg_number) from processorder_pckg where (status=8 or status=18 ) and is_repack=1";
			response3 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<NotificationCounterResponse>() {
						public NotificationCounterResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							NotificationCounterResponse reponseVal = new NotificationCounterResponse();

							while (rs.next()) {
								reponseVal.setRepackProcessorderCount(rs.getInt(1));
							}
							return reponseVal;
						}
					});
			query ="select count(processorder_pckg_number) from processorder_pckg where status=14 and is_repack=1";
			response4 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<NotificationCounterResponse>() {
						public NotificationCounterResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							NotificationCounterResponse reponseVal = new NotificationCounterResponse();

							while (rs.next()) {
								reponseVal.setRepackRmPostingCount(rs.getInt(1));
							}
							return reponseVal;
						}
					});
			response4.setProcessorderCount(response1.getProcessorderCount());;
			response4.setRmPostingCount(response2.getRmPostingCount());
			response4.setRepackProcessorderCount(response3.getRepackProcessorderCount());
			return response4; 
		} catch (Exception ex) {
			

			ex.printStackTrace();
			return null;
		}
	}

}
