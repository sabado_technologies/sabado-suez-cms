package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.QualityDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ContainerTable;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.Quality;
import com.entity.QualityCheck;
import com.entity.QualityMaster;
import com.entity.Role;
import com.entity.User;

@Repository

public class QualityDaoImpl extends NamedParameterJdbcDaoSupportClass implements QualityDao {

	@Override
	public List<ProcessorderPckg> getPackageProcessOrders(long processNumber) {
		try {

			String query = "SELECT p.batch_number,p.product_number,p.processorder_pckg_number,"
					+ "p.container_number,p.product_name,c.container_description,p.processorder_prod_number \r\n"
					+ "FROM processorder_pckg p\r\n"
					+ "inner join container_table c on c.container_number = p.container_number\r\n"
					+ "where p.status=15 and p.processorder_prod_number=" + processNumber + " ";

			List<ProcessorderPckg> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg pack = new ProcessorderPckg();
								pack.setBatchNumber(rs.getString(1));
								pack.setProductNumber(rs.getLong(2));
								pack.setProcessorderPckgNumber(rs.getLong(3));
								pack.setProductName(rs.getString(5));

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(7));
								pack.setProcessorderProd(prod);

								ContainerTable con = new ContainerTable();
								con.setContainerNumber(rs.getLong(4));
								con.setContainerDescription(rs.getString(6));
								pack.setContainerTable(con);
								list.add(pack);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Quality> getQualities() {

		try {

			String query = "SELECT q.quality_name from quality q;";

			List<Quality> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<Quality>>() {
						public List<Quality> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<Quality> list = new ArrayList<Quality>();
							while (rs.next()) {
								Quality qt = new Quality();
								qt.setQualityName(rs.getString(1));
								list.add(qt);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public long updatePackage(ProcessorderPckg processorderPckg) {
		try {
			
			String query = " UPDATE processorder_pckg SET status=2 WHERE processorder_pckg_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderPckg.getProcessorderPckgNumber());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processorderPckg.getProcessorderPckgNumber();
	}

	@Override
	public List<ProcessorderProd> getProductDetails() {
		try {

			String query = "SELECT p.processorder_prod_number,p.product_number,p.batch_number,\r\n"
					+ "p.total_quantity,pt.product_description,p.batch_date from processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "where p.status=16";

			List<ProcessorderProd> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();
							while (rs.next()) {
								ProcessorderProd p = new ProcessorderProd();
								p.setProcessorderProdNumber(rs.getLong(1));
								p.setBatchNumber(rs.getString(3));
								p.setTotalQuantity(rs.getString(4));
								String batchDate =reverseDate(rs.getString(6));
								p.setBatchDate(batchDate);

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(5));
								p.setProductTable(pt);
								list.add(p);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<QualityCheck> getQualityCheck(long processorderProdNumber) {
		
		try {
			
			String query = "SELECT qr.process_order,qr.product_number,qr.material_memo,\r\n"
					+ "qr.batch_no,qr.sso,qr.remark FROM quality_check_result qr \r\n"
					+ "where qr.process_order ='"+processorderProdNumber+"' order by qr.process_order;";
			/*String query = "SELECT qc.quality_id,qm.discription,qm.method,qc.result,\r\n"
					+ "qm.range1,qm.range2 FROM quality_check qc\r\n"
					+ "inner join quality_master qm on qm.id=qc.quality_id\r\n"
					+ "where qc.process_order_numbers='"+processorderProdNumber+"' order by qm.id";*/

			
			List<QualityCheck> qualities = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<QualityCheck>>() {
						public List<QualityCheck> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<QualityCheck> list = new ArrayList<QualityCheck>();
							while (rs.next()) {
								QualityCheck qc = new QualityCheck();
								qc.setProcessOrder(rs.getLong(1));
								qc.setProductNumber(rs.getString(2));
								qc.setMaterialMemo(rs.getString(3));
								qc.setBatchNumber(rs.getString(4));
								qc.setSso(rs.getString(5));
								qc.setRemarks(rs.getString(6));
								/*qc.setResult(rs.getString(4));
								String str1 = rs.getString(5);								
								if(str1 != null )
								{	
								 List<String> Range1 = Arrays.asList(str1.split(","));
								 qc.setRange1(Range1);
								}
								else {
									qc.setRange1(null);
								}
								String str2 = rs.getString(6);
								if(str2 != null )
								{	
								 List<String> Range2 = Arrays.asList(str2.split(","));
								 qc.setRange2(Range2);
								}
								else {
									qc.setRange2(null);
								}

								QualityMaster qm = new QualityMaster();
								qm.setId(rs.getInt(1));
								qm.setDiscription(rs.getString(2));
								qm.setMethod(rs.getString(3));
								qc.setQualityMaster(qm);*/
								list.add(qc);
							}
							return list;
						}
					});
			return qualities;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public long acceptQuality(long processorderProdNumber, String ssoid) {
		////System.out.println("order => " + processorderProdNumber);
		//System.out.println("SSo => " + ssoid);

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		try {
			String query = " UPDATE processorder_prod SET status=17 WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		try {
			String query = " UPDATE processorder_pckg SET status=17 WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id, is_reworked) VALUES (:processOrder, :sso, :time, :status, :rework)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 17);
			param.addValue("sso", ssoid);
			param.addValue("rework", 0);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 17);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		
		try {

			String query1 = "UPDATE status_info SET quality_inspection = :time,quality_inspected_by = :ssoid,status_id = :status WHERE processorder_prodNo = :poNumber;";

			/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
					+ "WHERE processorder_pckg_number = :poNumber;";*/

			MapSqlParameterSource param1 = new MapSqlParameterSource();
			param1.addValue("poNumber", processorderProdNumber);
			param1.addValue("time", timeStamp);
			param1.addValue("ssoid", ssoid);
			param1.addValue("status", 17);
			getNamedParameterJdbcTemplate().update(query1, param1);

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		return processorderProdNumber;
	}

	@Override
	public Long rejectQuality(long processorderProdNumber, String ssoid, String comment, String batchNo, String prodNo,
			String memo) {

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		try {

			String query = " UPDATE processorder_prod SET status=19, comments= :comment, is_reworked= :isReworked WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processOrder", processorderProdNumber);
			param.addValue("comment", comment);
			param.addValue("isReworked", 1);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {
			
			String query = " UPDATE processorder_pckg SET status=19 WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processOrder", processorderProdNumber);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id, is_reworked) VALUES (:processOrder, :sso, :time, :status, :reworked)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 19);
			param.addValue("sso", ssoid);
			param.addValue("reworked", 1);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {

			String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 19);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			
			String query = "INSERT INTO rework(processorder_prod_number, created_time, created_by, comment, is_approved, batch_number, produc_number, material_memo) VALUES ( :processOrder, :time, :sso, :comment, :isApproved, :batchNo, :productNo, :memo);";

			/*String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";*/

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("comment", comment);
			param.addValue("sso", ssoid);
			param.addValue("isApproved", 0);
			param.addValue("productNo", prodNo);
			param.addValue("batchNo", batchNo);
			param.addValue("memo", memo);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return processorderProdNumber;
	}

	@Override
	public long scrapQuality(long processorderProdNumber, String ssoid, String comment) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		try {

			String query = " UPDATE processorder_prod SET status=22, comments= :comment WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processOrder", processorderProdNumber);
			param.addValue("comment", comment);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {
			
			String query = " UPDATE processorder_pckg SET status=22 WHERE processorder_prod_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processOrder", processorderProdNumber);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id) VALUES (:processOrder, :sso, :time, :status)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 22);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {

			String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 22);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processorderProdNumber;
	}

	@Override
	public List<ProcessorderPckg> getRepackDetails() {
try {
			
			String query = "SELECT p.processorder_pckg_number,p.product_number,\r\n"
					+ "p.material_memo,p.batch_date,p.batch_number ,p.total_quantity \r\n"
					+ " FROM processorder_pckg p where p.status=16 and p.is_repack=1";

			/*String query = "SELECT p.processorder_prod_number,p.product_number,p.batch_number,\r\n"
					+ "p.total_quantity,pt.product_description from processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "where p.status=16 and p.is_repack=1";*/

			List<ProcessorderPckg> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg p = new ProcessorderPckg();
								p.setProcessorderPckgNumber(rs.getLong(1));
								p.setProductNumber(rs.getLong(2));
								p.setMaterialMemo(rs.getString(3));
								String reversed = reverseDate(rs.getString(4));
								p.setBatchDate(reversed);
								p.setBatchNumber(rs.getString(5));
								p.setTotalQuantity(rs.getLong(6));
								/*p.setProcessorderProdNumber(rs.getLong(1));
								p.setBatchNumber(rs.getString(3));
								p.setTotalQuantity(rs.getString(4));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(5));
								p.setProductTable(pt);*/
								list.add(p);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}

	@Override
	public int getRepackCount() {
		int count = 0;
		try {
			String query = "SELECT p.processorder_pckg_number,count(p.processorder_pckg_number)as total_count\r\n"
					+ "FROM processorder_pckg p where p.status=16 and p.is_repack=1";

			count = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<Integer>() {
				public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {

					//User u = new User();
					int c = 0;

					while (rs.next()) {
						c = rs.getInt(2);
					}
					return c;
				}
			});
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

}
