package com.daoImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.UserDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.Associate;
import com.entity.MultiRoles;
import com.entity.Role;
import com.entity.Status;
import com.entity.User;
import com.entity.UserDetailsForAlerts;
import com.request.AddUserRequest;

@Repository
public class UserDaoImpl extends NamedParameterJdbcDaoSupportClass implements UserDao {

	/// .......................Adding New User to
	/// Database................................
	@Override
	public String saveUser(AddUserRequest user , List<String> roles) {

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		try {

			String query = "INSERT INTO user( user_name " + "           ,email " + "           ,ssoid "
					+ "           ,address " + "           ,password " + "           ,role_id "
					+ "           ,status_id " + "           ,phone_number " + "           ,pincode "
					+ "           ,created_time ,created_by )" + "     VALUES( :name " + "           ,:email "
					+ "           ,:sso_id " + "           ,:address " + "           ,:password "
					+ "           ,:role_id " + "           ,:status_id " + "           ,:phone_number "
					+ "           ,:pincode " + "           ,:created_time , :createdBy )";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("name", user.getUsername());
			param.addValue("email", user.getEmail());
			param.addValue("sso_id", user.getSsoid());
			param.addValue("address", user.getAddress());
			param.addValue("role_id", user.getRoleId());
			param.addValue("status_id", 11);
			param.addValue("phone_number", user.getPhonenumber());
			param.addValue("password", user.getPassword());
			param.addValue("pincode", user.getPincode());
			param.addValue("created_time", timeStamp);
			param.addValue("createdBy", user.getCreatedBy());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			
			String sql2 = "INSERT INTO multi_roles (sso , role )VALUES (?,?);";

			getJdbcTemplate().batchUpdate(sql2, new BatchPreparedStatementSetter() {

				public int getBatchSize() {
					return roles.size();
				}

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					String multiRole = roles.get(i);
					int role = Integer.parseInt(multiRole);
					
					ps.setString(1, user.getSsoid());
					ps.setInt(2, role);
					
				}
			});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return user.getUsername();
	}

	/// .............................Updating User in
	/// Database...........................
	@Override
	public String editUser(User user,List<String> roles) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		try {
			String query = " UPDATE user " + "  SET  user_name = :userName " + "      ,email = :email "
					+ "      ,phone_number = :phoneNumber " + "      ,modified_by = :modifiedBy,modified_time=:time,role_id = :role_id " + "  WHERE ssoid = :ssoid";
			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("userName", user.getUserName());
			param.addValue("email", user.getEmail());
			param.addValue("ssoid", user.getSsoid());
			param.addValue("role_id", user.getRole().getRoleId());
			param.addValue("phoneNumber", user.getPhoneNumber());
			param.addValue("modifiedBy", user.getUserByModifiedBy().getSsoid());
			param.addValue("time", timeStamp);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			String query = " DELETE FROM multi_roles WHERE sso = '" + user.getSsoid() + "' ";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("ssoid", user.getSsoid());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(int i=0 ; i < roles.size(); i++){
			
			try {
				String query = "INSERT INTO multi_roles(sso,role) VALUES ( :ssoid , :role);";
				
				MapSqlParameterSource param = new MapSqlParameterSource();
				param.addValue("ssoid", user.getSsoid());
				param.addValue("role", roles.get(i));
				getNamedParameterJdbcTemplate().update(query, param);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		return user.getUserName();

	}

	/// ........................Search User with Respect to
	/// SSO...........................
	@Override
	public User findUser(String ssoid) {
		User user = null;
		try {
			String query = "SELECT u.user_name,u.ssoid,u.email,u.phone_number,r.role_name,u.password from user u\r\n"
					+ "inner join role r on r.role_id=u.role_id\r\n" + "WHERE u.ssoid = '" + ssoid + "' ";

			user = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<User>() {
				public User extractData(ResultSet rs) throws SQLException, DataAccessException {

					User u = new User();

					while (rs.next()) {
						u.setUserName(rs.getString(1));
						u.setSsoid(rs.getString(2));
						u.setEmail(rs.getString(3));
						u.setPhoneNumber(rs.getString(4));
						u.setPassword(rs.getString(6));
						Role r = new Role();
						r.setRoleName(rs.getString(5));
						u.setRole(r);
					}
					return u;
				}
			});
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	/// .........................Fetching All Roles from
	/// Database......................
	@Override
	public List<Role> getRoles() {
		// TODO Auto-generated method stub
		try {
			String query = "SELECT role_id,role_name from role";

			List<Role> roles = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<Role>>() {
				public List<Role> extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<Role> list = new ArrayList<Role>();
					while (rs.next()) {
						Role r = new Role();
						r.setRoleId(rs.getInt(1));
						r.setRoleName(rs.getString(2));
						list.add(r);
					}
					return list;
				}
			});
			return roles;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/// ..........................Fetching all Active
	/// Users..............................
	@Override
	public List<User> getActiveUsers() {
		try {
			String query = "SELECT u.user_name,u.email,u.phone_number,r.role_name,u.ssoid,u.pincode,s.status_description from user u\r\n"
					+ "inner join role r on r.role_id=u.role_id\r\n"
					+ "inner join status s on s.status_id=u.status_id\r\n" + "where u.status_id=11;";

			List<User> activeUsers = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<User>>() {
				public List<User> extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<User> list = new ArrayList<User>();

					while (rs.next()) {

						User u = new User();
						u.setUserName(rs.getString(1));
						u.setEmail(rs.getString(2));
						u.setPhoneNumber(rs.getString(3));
						u.setSsoid(rs.getString(5));
						u.setPincode(rs.getString(6));
						Status s = new Status();
						s.setStatusDescription(rs.getString(7));
						u.setStatus(s);
						Role r = new Role();
						r.setRoleName(rs.getString(4));
						u.setRole(r);
						list.add(u);
					}
					return list;
				}
			});
			return activeUsers;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/// ............................Fetching all De-Activated
	/// Users.......................
	@Override
	public List<User> getDeActiveUsers() {
		try {
			String query = "SELECT u.user_name,u.email,u.phone_number,r.role_name,u.ssoid,u.pincode,s.status_description from user u\r\n"
					+ "inner join role r on r.role_id=u.role_id\r\n"
					+ "inner join status s on s.status_id=u.status_id\r\n" + "where u.status_id=12;";

			List<User> deActiveUsers = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<User>>() {
						public List<User> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<User> list = new ArrayList<User>();

							while (rs.next()) {

								User u = new User();
								u.setUserName(rs.getString(1));
								u.setEmail(rs.getString(2));
								u.setPhoneNumber(rs.getString(3));
								u.setSsoid(rs.getString(5));
								// u.setAddress(rs.getString(6));
								u.setPincode(rs.getString(6));
								Status s = new Status();
								s.setStatusDescription(rs.getString(7));
								u.setStatus(s);
								Role r = new Role();
								r.setRoleName(rs.getString(4));
								u.setRole(r);
								list.add(u);
							}
							return list;
						}
					});
			return deActiveUsers;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/// ..............................Fetching all
	/// Users.................................
	@Override
	public List<User> getAllUsers() {
		try {
			String query = "SELECT u.user_name,u.ssoid,u.email,u.phone_number,r.role_name,s.status_description from user u\r\n"
					+ "inner join role r on r.role_id=u.role_id\r\n"
					+ "inner join status s on s.status_id=u.status_id\r\n"
					+ "order by s.status_description,u.user_name;";

			List<User> listOfUsers = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<User>>() {
				public List<User> extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<User> list = new ArrayList<User>();

					while (rs.next()) {

						User u = new User();
						u.setUserName(rs.getString(1));
						u.setSsoid(rs.getString(2));
						u.setEmail(rs.getString(3));
						u.setPhoneNumber(rs.getString(4));
						Role r = new Role();
						r.setRoleName(rs.getString(5));
						u.setRole(r);
						Status s = new Status();
						s.setStatusDescription(rs.getString(6));
						u.setStatus(s);
						list.add(u);
					}
					return list;
				}
			});
			return listOfUsers;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/// ...........................Activating
	/// Users.................................
	@Override
	public int activateUser(final List<User> list) {
		// TODO Auto-generated method stub
		String sql = " UPDATE user SET  status_id=? WHERE ssoid =?\r\n ";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {

			public int getBatchSize() {
				return list.size();
			}

			public void setValues(PreparedStatement ps, int i) throws SQLException {
				User user = list.get(i);

				ps.setInt(1, user.getStatus().getStatusId());
				ps.setString(2, user.getSsoid());
			}
		});
		return 0;
	}

	/// ..........................De-Activating
	/// Users..................................
	@Override
	public int deActivateUser(List<User> list) {
		// TODO Auto-generated method stub
		String sql = " UPDATE user SET  status_id=? WHERE ssoid =?\r\n ";

		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {

			public int getBatchSize() {
				return list.size();
			}

			public void setValues(PreparedStatement ps, int i) throws SQLException {
				User user = list.get(i);

				ps.setInt(1, user.getStatus().getStatusId());
				ps.setString(2, user.getSsoid());
			}
		});
		return 0;
	}

	@Override
	public User getUserBySSOId(String ssoId) {
		User user = null;
		try {
			String query = "SELECT u.user_name,u.ssoid,u.email,u.phone_number,r.role_name from user u\r\n"
					+ "inner join role r on r.role_id=u.role_id\r\n" + "WHERE u.ssoid = '" + ssoId + "' ";

			user = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<User>() {
				public User extractData(ResultSet rs) throws SQLException, DataAccessException {

					User u = new User();

					while (rs.next()) {
						u.setUserName(rs.getString(1));
						u.setSsoid(rs.getString(2));
						u.setEmail(rs.getString(3));
						u.setPhoneNumber(rs.getString(4));
						Role r = new Role();
						r.setRoleName(rs.getString(5));
						u.setRole(r);
					}
					return u;
				}
			});
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public int changePassword(AddUserRequest user) {
		// TODO Auto-generated method stub
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ").format(new Date());
		try {
			String query = "UPDATE user set password=:password , modified_by=:ssoid , modified_time=:modified_time where ssoid=:ssoid";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("password", user.getPassword());
			param.addValue("ssoid", user.getSsoid());
			param.addValue("modified_time", timeStamp);
			getNamedParameterJdbcTemplate().update(query, param);
			//System.out.println("Inserted");

		} catch (Exception e) {
			//System.out.println("Upadate Failed");
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int insertEmailAlert(String reactor, int temp, String process, long processOrder, String sso,
			String userName) {
		// TODO Auto-generated method stub
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ").format(new Date());
		try {
			String query ="INSERT INTO email_alerts(process_order,sso,user_name,process,reactor,\r\n"
					+ "temperature,time) VALUES ( :process_order, :sso , :user_name, :process,\r\n"
					+ ":reactor, :temperature, :time);";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("process_order", processOrder);
			param.addValue("sso", sso);
			param.addValue("user_name", userName);
			param.addValue("process", process);
			param.addValue("reactor", reactor);
			param.addValue("temperature", temp);
			param.addValue("time", timeStamp);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int resetPassword(String sso, String password) {
		// TODO Auto-generated method stub
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ").format(new Date());
		try {
			String query = "UPDATE user set password= :password ,  modified_time= :modified_time where ssoid=:ssoid";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("password", password);
			param.addValue("ssoid", sso);
			param.addValue("modified_time", timeStamp);
			getNamedParameterJdbcTemplate().update(query, param);
			//System.out.println("Inserted");

		} catch (Exception e) {
			//System.out.println("Upadate Failed");
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<MultiRoles> getMultiRoles() {
		try {
			
			String query = "SELECT m.multi_roles_id,m.sso,r.role_name\r\n"
					+ "FROM multi_roles m inner join role r on r.role_id=m.role";

			List<MultiRoles> roles = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<List<MultiRoles>>() {
				public List<MultiRoles> extractData(ResultSet rs) throws SQLException, DataAccessException {

					List<MultiRoles> list = new ArrayList<MultiRoles>();
					while (rs.next()) {
						MultiRoles m = new MultiRoles();
						m.setMultiRolesId(rs.getInt(1));
						m.setSso(rs.getString(2));
						m.setRoleName(rs.getString(3));
						list.add(m);
					}
					return list;
				}
			});
			return roles;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<UserDetailsForAlerts> getAlertUsers() {
		// TODO Auto-generated method stub
		try {
			
			String query = "SELECT userdetails_id,user_name,email,phone_number FROM userdetails_for_alerts";

			/*String query1 = "SELECT m.multi_roles_id,m.sso,r.role_name\r\n"
					+ "FROM multi_roles m inner join role r on r.role_id=m.role";*/

			List<UserDetailsForAlerts> users = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<UserDetailsForAlerts>>() {
						public List<UserDetailsForAlerts> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<UserDetailsForAlerts> list = new ArrayList<UserDetailsForAlerts>();
							while (rs.next()) {
								UserDetailsForAlerts u = new UserDetailsForAlerts();
								u.setUserId(rs.getInt(1));
								u.setUserName(rs.getString(2));
								u.setEmail(rs.getString(3));
								u.setPhoneNumber(rs.getString(4));								
								list.add(u);
							}
							return list;
						}
					});
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int addUserForAlerts(UserDetailsForAlerts user) {
		// TODO Auto-generated method stub
		try {
			String query = "INSERT INTO userdetails_for_alerts(user_name ,email ,phone_number) VALUES(:name ,:email ,:phone );";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("name", user.getUserName());
			param.addValue("email", user.getEmail());
			param.addValue("phone", user.getPhoneNumber());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int deleteAlertUser(int userId) {
		try {
			String query = "DELETE FROM userdetails_for_alerts WHERE userdetails_id = '"+userId+"';";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("user", userId);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int updateAlertUser(UserDetailsForAlerts user) {
		try {
			String query = "UPDATE userdetails_for_alerts SET user_name = :userName , email = :email , phone_number = :phoneNumber WHERE userdetails_id = :userId ;";
			
			//String query = "DELETE FROM userdetails_for_alerts WHERE userdetails_id = : user;";
			
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("userId", user.getUserId());
			param.addValue("userName", user.getUserName());
			param.addValue("email", user.getEmail());
			param.addValue("phoneNumber", user.getPhoneNumber());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
}
