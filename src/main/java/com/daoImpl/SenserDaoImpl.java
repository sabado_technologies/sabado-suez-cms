package com.daoImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.springframework.stereotype.Repository;

import com.dao.SensorDao;

@Repository
public class SenserDaoImpl implements SensorDao {

	public String TemparatureSensor(int tempsensorno) throws IOException {
		if (tempsensorno == 1) {

			String stringUrl = "http://10.174.84.37:8080/tsdata.py";
			String result = null;
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();

			uc.setRequestProperty("X-Requested-With", "Curl");

			InputStreamReader inputStreamReader = new InputStreamReader(uc.getInputStream());
			//System.out.println("hiiii");
			try (BufferedReader br = new BufferedReader(inputStreamReader)) {
				String line = null;
				while ((line = br.readLine()) != null) {
					result = line;

					//System.out.println("result" + line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

		}

		else {

			String stringUrl = "http://10.174.84.36:8080/tsdata.py";
			String result = null;
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();

			uc.setRequestProperty("X-Requested-With", "Curl");

			InputStreamReader inputStreamReader = new InputStreamReader(uc.getInputStream());
			//System.out.println("hiiii");
			try (BufferedReader br = new BufferedReader(inputStreamReader)) {
				String line = null;
				while ((line = br.readLine()) != null) {
					result = line;

					//System.out.println("result" + line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

		}

	}

	public String weightSensor(int weightsensorno) throws IOException {
		if (weightsensorno == 1) {
			

			String stringUrl = "http://10.174.84.36:8080/wsdata.py";
			String result = null;
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();

			uc.setRequestProperty("X-Requested-With", "Curl");

			InputStreamReader inputStreamReader = new InputStreamReader(uc.getInputStream());
			//System.out.println("hiiii");
			try (BufferedReader br = new BufferedReader(inputStreamReader)) {
				String line = null;
				while ((line = br.readLine()) != null) {
					result = line;

					//System.out.println("result" + line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		} else if (weightsensorno == 2) {
			
			String stringUrl = "http://10.174.84.37:8080/wsdata.py";
			String result = null;
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();

			uc.setRequestProperty("X-Requested-With", "Curl");

			InputStreamReader inputStreamReader = new InputStreamReader(uc.getInputStream());
			//System.out.println("hiiii");
			try (BufferedReader br = new BufferedReader(inputStreamReader)) {
				String line = null;
				while ((line = br.readLine()) != null) {
					result = line;

					//System.out.println("result" + line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

		}

		else {
	
			String stringUrl = "http://10.174.84.36:8080/wsdata.py";
			String result = null;
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();

			uc.setRequestProperty("X-Requested-With", "Curl");

			InputStreamReader inputStreamReader = new InputStreamReader(uc.getInputStream());
			//System.out.println("hiiii");
			try (BufferedReader br = new BufferedReader(inputStreamReader)) {
				String line = null;
				while ((line = br.readLine()) != null) {
					result = line;

					//System.out.println("result" + line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

		}

	}

}
