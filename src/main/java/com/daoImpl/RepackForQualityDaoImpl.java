package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.RepackForQualityDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ProcessorderPckg;

@Repository
public class RepackForQualityDaoImpl extends NamedParameterJdbcDaoSupportClass implements RepackForQualityDao{

	@Override
	public List<ProcessorderPckg> getRepackDetails() {
		try {
			
			String query = "SELECT p.processorder_pckg_number,p.product_number,\r\n"
					+ "p.material_memo,p.batch_date,p.batch_number ,p.total_quantity \r\n"
					+ " FROM processorder_pckg p where p.status=16 and p.is_repack=1";

			/*String query = "SELECT p.processorder_prod_number,p.product_number,p.batch_number,\r\n"
					+ "p.total_quantity,pt.product_description from processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "where p.status=16 and p.is_repack=1";*/

			List<ProcessorderPckg> processOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg p = new ProcessorderPckg();
								p.setProcessorderPckgNumber(rs.getLong(1));
								p.setProductNumber(rs.getLong(2));
								p.setMaterialMemo(rs.getString(3));
								String reversed = reverseDate(rs.getString(4));
								p.setBatchDate(reversed);
								p.setBatchNumber(rs.getString(5));
								p.setTotalQuantity(rs.getLong(6));
								/*p.setProcessorderProdNumber(rs.getLong(1));
								p.setBatchNumber(rs.getString(3));
								p.setTotalQuantity(rs.getString(4));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(5));
								p.setProductTable(pt);*/
								list.add(p);
							}
							return list;
						}
					});
			return processOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}

	@Override
	public long acceptQuality(long processorderProdNumber, String ssoid) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		
		try {
			String query = " UPDATE processorder_pckg SET status=17 WHERE processorder_pckg_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id, is_reworked) VALUES (:processOrder, :sso, :time, :status, :rework)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 17);
			param.addValue("sso", ssoid);
			param.addValue("rework", 0);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 17);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		
		try {

			String query1 = "UPDATE repacking_status_info SET quality_inspection = :time,quality_inspected_by = :ssoid,status_id = :status WHERE processorder_packNo = :poNumber;";

			/*String query = "UPDATE processorder_pckg SET reactor = :reactor ,shift = :sift ,is_selected = :selected \r\n"
					+ "WHERE processorder_pckg_number = :poNumber;";*/

			MapSqlParameterSource param1 = new MapSqlParameterSource();
			param1.addValue("poNumber", processorderProdNumber);
			param1.addValue("time", timeStamp);
			param1.addValue("ssoid", ssoid);
			param1.addValue("status", 17);
			getNamedParameterJdbcTemplate().update(query1, param1);

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		return processorderProdNumber;
	}

	@Override
	public long rejectQuality(long processorderProdNumber, String ssoid, String comment) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		
		try {
			
			String query = " UPDATE processorder_pckg SET status=19 WHERE processorder_pckg_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processOrder", processorderProdNumber);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id, is_reworked) VALUES (:processOrder, :sso, :time, :status, :reworked)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 19);
			param.addValue("sso", ssoid);
			param.addValue("reworked", 1);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {

			String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 19);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			
			String query = "INSERT INTO rework(processorder_prod_number, created_time, created_by, comment, is_approved) VALUES ( :processOrder, :time, :sso, :comment, :isApproved);";

			/*String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";*/

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("comment", comment);
			param.addValue("sso", ssoid);
			param.addValue("isApproved", 0);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return processorderProdNumber;
	}

	@Override
	public long scrapQuality(long processorderProdNumber, String ssoid, String comment) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		
		try {
			
			String query = " UPDATE processorder_pckg SET status=22 WHERE processorder_pckg_number =:processOrder";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processOrder", processorderProdNumber);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}

		try {

			String query = "INSERT INTO process_tracker (processorder_number ,ssoid, process_end_time,\r\n"
					+ " status_id) VALUES (:processOrder, :sso, :time, :status)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 22);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {

			String query = "INSERT INTO alerts(ssoId,process_order_number,status_id,generated_time)\r\n"
					+ "VALUES(:sso,:processOrder,:status,:time)";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("processOrder", processorderProdNumber);
			param.addValue("time", timeStamp);
			param.addValue("status", 22);
			param.addValue("sso", ssoid);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processorderProdNumber;
	}

}
