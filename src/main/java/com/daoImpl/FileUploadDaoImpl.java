package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.FileUploadDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ProcessorderProd;
import com.entity.Role;
import com.entity.User;
import com.request.PdfValues;
import com.request.PdfValuesForPack;

@Repository
public class FileUploadDaoImpl extends NamedParameterJdbcDaoSupportClass implements FileUploadDao {

	@Override
	public boolean inertIntoProductionOrders(PdfValues pdfValues) {
		// batch date conversion
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String updatedBatchDate = convertDate(pdfValues.getBatchDate());
		pdfValues.setBatchDate(updatedBatchDate);
		// pdfValues.//System.out.println("asdasd");
		List<String> rawMaterialNames = pdfValues.getRmName();
		List<String> rawMaterialCode = pdfValues.getRmCodes();
		List<String> rawMaterialQuant = pdfValues.getRmQuant();
		List<String> rawMaterialTypes = pdfValues.getRmTypes();
		List<String> rawMaterialPhases = pdfValues.getRmPhases();
		// insert into product table

		try {
			String query1 = "INSERT INTO product_table(product_number,product_description,product_name) VALUES(:productNumber ,:product_description,:product_description)";

			MapSqlParameterSource param1 = new MapSqlParameterSource();
			param1.addValue("productNumber", pdfValues.getProductNumber());
			param1.addValue("product_description", pdfValues.getProductName());
			getNamedParameterJdbcTemplate().update(query1, param1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// insert into raw material table
		for (int i = 0; i < rawMaterialCode.size(); i++) {
			try {
				String rawMaterialDesc = rawMaterialNames.get(i);
				String rawMeterialNumber = rawMaterialCode.get(i);

				String query2 = "INSERT INTO rawmaterial_table(rawmaterial_number,rawmaterial_description) VALUES(:rawmaterialNumber ,:rawmaterialDesc)";

				MapSqlParameterSource param2 = new MapSqlParameterSource();
				param2.addValue("rawmaterialNumber", rawMeterialNumber);
				param2.addValue("rawmaterialDesc", rawMaterialDesc);
				getNamedParameterJdbcTemplate().update(query2, param2);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		try {
			// insert into production order table
			String query3 = "INSERT INTO `processorder_prod`"
					+ "(`processorder_prod_number`,`product_number`,`batch_number`,"
					+ "`batch_date`,`total_quantity`,`volume`,	`wght_pckg`,`status`,`is_associated`)	" + "VALUES	"
					+ "(:processorder_prod_number,:product_number,:batch_number,"
					+ ":batch_date,:total_quantity,:volume,:wght_pckg,:status,:is_associated);";

			MapSqlParameterSource param3 = new MapSqlParameterSource();
			param3.addValue("processorder_prod_number", pdfValues.getProcessOderNo());
			param3.addValue("product_number", pdfValues.getProductNumber());
			param3.addValue("batch_number", pdfValues.getBatchNo());
			param3.addValue("batch_date", pdfValues.getBatchDate());
			param3.addValue("total_quantity", pdfValues.getTotalQuant());
			param3.addValue("volume", pdfValues.getVolume());
			param3.addValue("wght_pckg", pdfValues.getWgtPkg());
			param3.addValue("status", 7);
			param3.addValue("is_associated", 0);

			getNamedParameterJdbcTemplate().update(query3, param3);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		for (int i = 0; i < rawMaterialCode.size(); i++) {
			try {
				String rawMaterialDesc = rawMaterialNames.get(i);
				String rawMeterialNumber = rawMaterialCode.get(i);
				String rawMaterialQuantQ = rawMaterialQuant.get(i);
				String rawMaterialType = rawMaterialTypes.get(i);
				String rawMaterialPhase = rawMaterialPhases.get(i);
				// String rawMaterialQuant = rawMa

				String query4 = "INSERT INTO process_order_rm"
						+ "(process_order_number,product_number,rawmaterial_number,type,phase,quantity) " + "VALUES"
						+ "(:process_order_number,:product_number,:rawmaterial_number,:rmType,:rmPhase,:quantity);";

				MapSqlParameterSource param4 = new MapSqlParameterSource();
				// param4.addValue("process_order_rm", rawMeterialNumber);
				param4.addValue("process_order_number", pdfValues.getProcessOderNo());
				param4.addValue("product_number", pdfValues.getProductNumber());
				param4.addValue("rawmaterial_number", rawMeterialNumber);
				param4.addValue("rmType", rawMaterialType);
				param4.addValue("rmPhase", rawMaterialPhase);
				param4.addValue("quantity", rawMaterialQuantQ);
				getNamedParameterJdbcTemplate().update(query4, param4);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
		try {
			// insert into status_info table
			String query5 = "INSERT INTO status_info(processorder_prodNo,upload,status_id) \r\n"
					+ " VALUES (:processorder,:uploadTime,:status);";

			MapSqlParameterSource param5 = new MapSqlParameterSource();
			param5.addValue("processorder", pdfValues.getProcessOderNo());
			param5.addValue("uploadTime", timeStamp);
			param5.addValue("status", 7);

			getNamedParameterJdbcTemplate().update(query5, param5);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean inertIntoPackagingOrders(PdfValuesForPack pdfValues) {
		/// ....................Insert into Container Table...........................
		String updatedBatchDate = convertDate(pdfValues.getBatchDate());
		pdfValues.setBatchDate(updatedBatchDate);
		try {

			String query1 = "INSERT INTO container_table(container_number,container_description,container_type,container_phase) VALUES(:containertNumber ,:containerDescription,:container_type,:container_phase)";
			String containerCode = pdfValues.getRmCodes().get(1);
			String containerDesc = pdfValues.getRmName().get(1);
			String containerType = pdfValues.getRmTypes().get(1);
			String containerPhase = pdfValues.getRmPhases().get(1);

			MapSqlParameterSource param1 = new MapSqlParameterSource();
			param1.addValue("containertNumber", containerCode);
			param1.addValue("containerDescription", containerDesc);
			param1.addValue("container_type", containerType);
			param1.addValue("container_phase", containerPhase);

			getNamedParameterJdbcTemplate().update(query1, param1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		/// ......................insert into processorder_pckg
		/// table.......................
		try {

			String productNumber = pdfValues.getRmCodes().get(0);

			String query2 = "INSERT INTO processorder_pckg"
					+ "(processorder_pckg_number , sixml_number , product_number , batch_number , storage_code ,"
					+ "batch_date , total_quantity , volume , wght_pckg , container_number , status,product_name,is_associated,process_quant,material_memo,is_repack) "
					+ "VALUES " + "(:processOrderNo , :sixMlNo , :productNumber , :batchNo , :storage ,"
					+ ":batchDate , :totalQuantity , :volume, :wghtPckg , :containerNo , :status,:product_name,:is_associated,:quantity,:material_memo,:is_repack);";

			MapSqlParameterSource param2 = new MapSqlParameterSource();
			param2.addValue("processOrderNo", pdfValues.getProcessOrderNo());
			param2.addValue("sixMlNo", pdfValues.getPackingCode());
			param2.addValue("productNumber", productNumber);
			param2.addValue("batchNo", pdfValues.getBatchNo());
			param2.addValue("batchDate", pdfValues.getBatchDate());
			param2.addValue("totalQuantity", pdfValues.getTotalQuant());
			param2.addValue("volume", pdfValues.getVolume());
			param2.addValue("wghtPckg", pdfValues.getWgtPkg());
			param2.addValue("containerNo", pdfValues.getRmCodes().get(1));
			param2.addValue("status", 9);
			param2.addValue("product_name", pdfValues.getRmName().get(0));
			param2.addValue("is_associated", 0);
			param2.addValue("quantity", pdfValues.getRmQuant().get(0));
			param2.addValue("material_memo", pdfValues.getMaterialMemo());
			param2.addValue("is_repack", 0);
			param2.addValue("storage", pdfValues.getStorage());

			getNamedParameterJdbcTemplate().update(query2, param2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean inertRePackagingOrders(PdfValuesForPack pdfValues) {
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String updatedBatchDate = convertDate(pdfValues.getBatchDate());
		pdfValues.setBatchDate(updatedBatchDate);
		try {

			String query1 = "INSERT INTO container_table(container_number,container_description,container_type,container_phase) VALUES(:containertNumber ,:containerDescription,:container_type,:container_phase)";
			String containerCode = pdfValues.getRmCodes().get(1);
			String containerDesc = pdfValues.getRmName().get(1);
			String containerType = pdfValues.getRmTypes().get(1);
			String containerPhase = pdfValues.getRmPhases().get(1);

			MapSqlParameterSource param1 = new MapSqlParameterSource();
			param1.addValue("containertNumber", containerCode);
			param1.addValue("containerDescription", containerDesc);
			param1.addValue("container_type", containerType);
			param1.addValue("container_phase", containerPhase);

			getNamedParameterJdbcTemplate().update(query1, param1);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		/// ......................insert into processorder_pckg
		/// table.......................
		try {

			String productNumber = pdfValues.getRmCodes().get(0);

			String query2 = "INSERT INTO processorder_pckg"
					+ "(processorder_pckg_number , sixml_number , product_number , batch_number , storage_code , "
					+ "batch_date , total_quantity , volume , wght_pckg , container_number , status,product_name,is_associated,process_quant,material_memo,is_repack) "
					+ "VALUES " + "(:processOrderNo , :sixMlNo , :productNumber , :batchNo , :storage ,"
					+ ":batchDate , :totalQuantity , :volume, :wghtPckg , :containerNo , :status,:product_name,:is_associated,:quantity,:material_memo,:is_repack);";

			MapSqlParameterSource param2 = new MapSqlParameterSource();
			param2.addValue("processOrderNo", pdfValues.getProcessOrderNo());
			param2.addValue("sixMlNo", pdfValues.getPackingCode());
			param2.addValue("productNumber", productNumber);
			param2.addValue("batchNo", pdfValues.getBatchNo());
			param2.addValue("batchDate", pdfValues.getBatchDate());
			param2.addValue("totalQuantity", pdfValues.getTotalQuant());
			param2.addValue("volume", pdfValues.getVolume());
			param2.addValue("wghtPckg", pdfValues.getWgtPkg());
			param2.addValue("containerNo", pdfValues.getRmCodes().get(1));
			param2.addValue("status", 23);
			param2.addValue("product_name", pdfValues.getRmName().get(0));
			param2.addValue("is_associated", 0);
			param2.addValue("quantity", pdfValues.getRmQuant().get(0));
			param2.addValue("material_memo", pdfValues.getMaterialMemo());
			param2.addValue("is_repack", 1);
			param2.addValue("storage", pdfValues.getStorage());

			getNamedParameterJdbcTemplate().update(query2, param2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try {
			//System.out.println("Status update started");
			// insert into status_info table
			String query5 = "INSERT INTO repacking_status_info(processorder_packNo,upload,status_id) \r\n"
					+ " VALUES (:processorder,:uploadTime,:status);";

			MapSqlParameterSource param5 = new MapSqlParameterSource();
			param5.addValue("processorder", pdfValues.getProcessOrderNo());
			param5.addValue("uploadTime", timeStamp);
			param5.addValue("status", 23);

			getNamedParameterJdbcTemplate().update(query5, param5);
			//System.out.println("Status update completed");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public String convertDate(String inputDate) {
		String outputDate = "";
		try {
			////System.out.println("input date=>" + inputDate);
			DateFormat inpDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date date = inpDateFormat.parse(inputDate);
			DateFormat outDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			outputDate = outDateFormat.format(date);
			//System.out.println("output date=>" + outputDate);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return outputDate;
	}

	@Override
	public ProcessorderProd getProcessOrder(String po) {
		// TODO Auto-generated method stub
		ProcessorderProd orders = null;
		try {
			String query = "SELECT batch_number FROM processorder_prod where processorder_prod_number='" + po + "'";
			
			orders = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<ProcessorderProd>() {
				public ProcessorderProd extractData(ResultSet rs) throws SQLException, DataAccessException {

					ProcessorderProd p = new ProcessorderProd();

					while (rs.next()) {
						p.setBatchNumber(rs.getString(1));
						
					}
					return p;
				}
			});
			return orders;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orders;
	}

	@Override
	public ProcessorderProd getPackingOrder(String po) {
		ProcessorderProd orders = null;
		try {
			String query = "SELECT batch_number FROM processorder_pckg where processorder_pckg_number='" + po + "'";
			
			orders = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<ProcessorderProd>() {
				public ProcessorderProd extractData(ResultSet rs) throws SQLException, DataAccessException {

					ProcessorderProd p = new ProcessorderProd();

					while (rs.next()) {
						p.setBatchNumber(rs.getString(1));
						
					}
					return p;
				}
			});
			return orders;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orders;
	}	

}
