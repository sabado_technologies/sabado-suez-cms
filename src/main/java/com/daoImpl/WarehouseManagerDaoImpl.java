/**
 * 
 */
package com.daoImpl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.dao.WarehouseManagerDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ContainerTable;
import com.entity.InventoryCheckDetails;
import com.entity.ProcessOrderRm;
import com.entity.ProcessTracker;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.QualityCheckResult;
import com.entity.RawmaterialTable;
import com.entity.Status;
import com.entity.User;
import com.response.LocationRawmaterialDetails;
import com.response.LocationRm;
import com.response.NotificationTabsForWarehouse;
import com.response.PackingOrderInventoryPostingResponse;
import com.response.ProcessOrderInventoryPostingResponse;
import com.response.RepackingProcessOrderCount;

/**
 * @author ullas
 *
 */

@Repository
public class WarehouseManagerDaoImpl extends NamedParameterJdbcDaoSupportClass implements WarehouseManagerDao {

	@Override
	public List<ProcessorderProd> getAllProcessOrders() {
		try {
			// String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			// batch_date='"+timeStamp+"' and
			String query="SELECT p.processorder_prod_number ,prod.product_number,prod.product_description, "
					+" p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity "
					+" from processorder_prod p inner join product_table prod on prod.product_number=p.product_number "
					+" inner join status s on s.status_id=p.status "
					+" where status=7 "
					+" and YEARWEEK(p.batch_date, 1) >= YEARWEEK(CURDATE(),1)"
					+" order by p.batch_date , p.processorder_prod_number  asc "; 
					

			List<ProcessorderProd> listOfProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(1));
								ProductTable prod = new ProductTable();
								prod.setProductNumber(rs.getLong(2));
								prod.setProductDescription(rs.getString(3));
								processOrderProd.setProductTable(prod);
								processOrderProd.setBatchNumber(rs.getString(4));
								processOrderProd.setBatchDate(rs.getString(5));
								Status s = new Status();
								s.setStatusDescription(rs.getString(6));
								processOrderProd.setStatus(s);
								processOrderProd.setTotalQuantity(rs.getString(7));
								list.add(processOrderProd);
							}
							return list;
						}
					});
			return listOfProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<ProcessOrderRm> getProcessOrderDetails(Long processorderProdNumber) {
		try {
			String query = "SELECT prm.process_order_number,pt.product_number,pt.product_description,\r\n"
					+ "rmt.rawmaterial_number,rmt.rawmaterial_description,prm.type,prm.phase,prm.quantity , prm.status\r\n"
					+ "from process_order_rm prm inner join product_table pt on prm.product_number=pt.product_number\r\n"
					+ "inner join rawmaterial_table rmt on rmt.rawmaterial_number=prm.rawmaterial_number\r\n"
					+ "where prm.process_order_number=" + processorderProdNumber + "\r\n"
					+ "order by prm.process_order_rm";

			List<ProcessOrderRm> listOfProcessOrdersDetails = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessOrderRm>>() {
						public List<ProcessOrderRm> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<ProcessOrderRm> list = new ArrayList<ProcessOrderRm>();

							while (rs.next()) {
								ProcessOrderRm processorderrm = new ProcessOrderRm();
								ProcessorderProd processordeprod = new ProcessorderProd();
								processordeprod.setProcessorderProdNumber(rs.getLong(1));
								processorderrm.setProcessorderProd(processordeprod);
								ProductTable prod = new ProductTable();
								prod.setProductNumber(rs.getLong(2));
								prod.setProductDescription(rs.getString(3));
								processorderrm.setProductTable(prod);
								RawmaterialTable rawmaterialtable = new RawmaterialTable();
								rawmaterialtable.setRawmaterialNumber(rs.getLong(4));
								rawmaterialtable.setRawmaterialDescription(rs.getString(5));
								processorderrm.setRawmaterialTable(rawmaterialtable);
								processorderrm.setType(rs.getString(6));
								processorderrm.setPhase(rs.getString(7));
								processorderrm.setQuantity(rs.getString(8));
								Status s = new Status();
								s.setStatusId(rs.getInt(9));
								processorderrm.setStatus(s);
								list.add(processorderrm);
							}
							return list;
						}
					});
			return listOfProcessOrdersDetails;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<ProcessorderProd> getProcessOrderCount() {

		// String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		try {
			String query = "select  p.processorder_prod_number  from processorder_prod p where " + " status=7 and YEARWEEK(p.batch_date, 1) = YEARWEEK(CURDATE(),1)";

			List<ProcessorderProd> ProcessOrderCount = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(1));
								list.add(processOrderProd);

							}
							return list;
						}
					});
			return ProcessOrderCount;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getProcessOrderCompletedCount() {
		// String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		// batch_date='"+timeStamp+"'
		try {
			String query = "SELECT  distinct processorder_number FROM process_tracker where status_id=8 and week(process_start_time)=week(curdate())";

			List<ProcessorderProd> ProcessOrderCompletedCount = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(1));
								list.add(processOrderProd);

							}
							return list;
						}
					});
			return ProcessOrderCompletedCount;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	@Override
	public int setProcessOrderTrackerStatus(Long processorderProdNumber, String ssoId) {

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		try {
			String query = "UPDATE processorder_prod " + " SET start_time ='" + timeStamp +"' WHERE processorder_prod_number= " + processorderProdNumber;
			SqlParameterSource param = null;
			getNamedParameterJdbcTemplate().update(query, param);
			
			 query = "insert into process_tracker (processorder_number ,process_id ,ssoid ,process_start_time ,status_id) \r\n"
					+ "VALUES ('" + processorderProdNumber + "','1','" + ssoId + "','" + timeStamp + "','10' )";

			 param = null;
			KeyHolder keyHolder = new GeneratedKeyHolder();
			getNamedParameterJdbcTemplate().update(query, param, keyHolder);

			return keyHolder.getKey().intValue();
			// return getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int updateProcessOrderStatus(Long processorderProdNumber, String processTrackerId,String ssoId) {

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		try {

			String query = "UPDATE process_tracker " + " SET process_end_time ='" + timeStamp + "', status_id = 8 "
					+ " WHERE process_tracker_id= " + processTrackerId;
			SqlParameterSource paramSource = null;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "insert into alerts (process_order_number ,ssoId,generated_time ,status_id) \r\n"
					+ "VALUES ('" + processorderProdNumber + "','"+ssoId+ "','" + timeStamp + "','8' )";
			SqlParameterSource param = null;
			getNamedParameterJdbcTemplate().update(query,param);
			

			query = " UPDATE processorder_prod " + " SET  status = 8 " + "  WHERE processorder_prod_number = "
					+ processorderProdNumber;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "UPDATE status_info set  rm_issue = '" + timeStamp + "' , rm_issued_by = '"+ssoId+ "' , status_id = 8  where processorder_prodNo = '"+processorderProdNumber+"' " ; 
			return getNamedParameterJdbcTemplate().update(query, paramSource);

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	public int setTrackerStatusForRm(ProcessTracker processTrackerTable) {
		
		String query = "update process_order_rm set status= 9 WHERE process_order_number='"+processTrackerTable.getProcessorderNumber()+"' and rawmaterial_number='"+processTrackerTable.getRawmaterialNumber()+"' and phase ='"+processTrackerTable.getWeightFromSensor()+"'";
		SqlParameterSource paramSource = null;
		getNamedParameterJdbcTemplate().update(query, paramSource);

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		try {
			 query = "INSERT INTO process_tracker( processorder_number,process_id,ssoid,rawmaterial_number,process_start_time,process_end_time,status_id\r\n"
					+ "	) \r\n"
					+ "	VALUES(:processorder_number,:process_id, :ssoid, :rawmaterial_number,:process_start_time,:process_end_time,:status_id); ";
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processorder_number", processTrackerTable.getProcessorderNumber());
			param.addValue("process_id", 2);
			param.addValue("ssoid", processTrackerTable.getUser().getSsoid());
			param.addValue("rawmaterial_number", processTrackerTable.getRawmaterialNumber());
			param.addValue("process_start_time", timeStamp);
			param.addValue("process_end_time", timeStamp);
			param.addValue("status_id", 8);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			getNamedParameterJdbcTemplate().update(query, param, keyHolder);
			processTrackerTable.setProcessTrackerId(keyHolder.getKey().intValue());
			return processTrackerTable.getProcessTrackerId();

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}

	}

	@Override
	public List<ProcessorderProd> getAllTodayProcessOrders() {

		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			// batch_date='"+timeStamp+"' and
			String query = "SELECT p.processorder_prod_number ,prod.product_number,prod.product_description,p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity\r\n"
					+ "from processorder_prod p inner join product_table prod on prod.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status\r\n" + "where  batch_date='" + timeStamp + "'";

			List<ProcessorderProd> listOfProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(1));
								ProductTable prod = new ProductTable();
								prod.setProductNumber(rs.getLong(2));
								prod.setProductDescription(rs.getString(3));
								processOrderProd.setProductTable(prod);
								processOrderProd.setBatchNumber(rs.getString(4));
								processOrderProd.setBatchDate(rs.getString(5));
								Status s = new Status();
								s.setStatusDescription(rs.getString(6));
								processOrderProd.setStatus(s);
								processOrderProd.setTotalQuantity(rs.getString(7));
								list.add(processOrderProd);
							}
							return list;
						}
					});
			return listOfProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<ProcessorderProd> getProcessOrdersForQuality() {
		try {

			String query = "SELECT p.processorder_prod_number ,prod.product_number,prod.product_description,p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity\r\n"
					+ "from processorder_prod p inner join product_table prod on prod.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status\r\n" + "where status=17 "
					+ "order by p.batch_date ,processorder_prod_number asc ";

			List<ProcessorderProd> listOfQualityCheckProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(1));
								ProductTable prod = new ProductTable();
								prod.setProductNumber(rs.getLong(2));
								prod.setProductDescription(rs.getString(3));
								processOrderProd.setProductTable(prod);
								processOrderProd.setBatchNumber(rs.getString(4));
								processOrderProd.setBatchDate(rs.getString(5));
								Status s = new Status();
								s.setStatusDescription(rs.getString(6));
								processOrderProd.setStatus(s);
								processOrderProd.setTotalQuantity(rs.getString(7));
								list.add(processOrderProd);
							}
							return list;
						}
					});
			return listOfQualityCheckProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderPckg> associatedPackingInfo(long processorderProdNumber) {

		List<ProcessorderPckg> processOrderPack = null;
		String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity,\r\n" + 
				"				p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,\r\n" + 
				"				p.process_quant,p.processorder_prod_number,p.product_name ,c.container_description  \r\n" + 
				"				FROM processorder_pckg p inner join container_table c on c.container_number = p.container_number "+
				" where processorder_prod_number='" + 
				processorderProdNumber + "' and status in (17 ,2)";

		try {

			processOrderPack = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg processorderPckg = new ProcessorderPckg();
								processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
								processorderPckg.setProductNumber(rs.getLong(2));
								processorderPckg.setBatchNumber(rs.getString(4));
								processorderPckg.setBatchDate(rs.getString(5));
								processorderPckg.setTotalQuantity(rs.getLong(6));
								processorderPckg.setWghtPckg(rs.getLong(7));
								Status s = new Status();
								s.setStatusId(rs.getInt(8));
								processorderPckg.setStatus(s);
								ContainerTable ct = new ContainerTable();
								ct.setContainerNumber(rs.getLong(9));
								ct.setContainerDescription(rs.getString(16));
								processorderPckg.setContainerTable(ct);
								processorderPckg.setVolume(rs.getString(10));
								processorderPckg.setSixmlNumber(rs.getString(11));
								processorderPckg.setIsAssociated(rs.getInt(12));
								processorderPckg.setMaterialMemo(rs.getString(3));
								processorderPckg.setProcessQuant(rs.getString(13));
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(14));
								processorderPckg.setProcessorderProd(processOrderProd);
								processorderPckg.setProductName(rs.getString(15));

								list.add(processorderPckg);
							}
							return list;

						}
					});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processOrderPack;

	}

	@Override
	public int setTrackerForWarehouseQuality(String ssoId, String locationPlaced, String containerQuantity, ProcessorderPckg packingdetail) {

		//String timeStamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		try {
			String query = " UPDATE processorder_pckg " + " SET  status = 2 ,completed_date='"+timeStamp+"',location_after_packing = '"+locationPlaced+"' ,quantity_after_packing = '"+containerQuantity +"'  WHERE processorder_pckg_number = "
					+ packingdetail.getProcessorderPckgNumber();

			SqlParameterSource paramSource = null;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "UPDATE status_info set inventory_check = '"+timeStamp+"', inventory_checked_by = '"+ssoId+"' , status_id = 2  where processorder_prodNo = '"+packingdetail.getProcessorderPckgNumber()+"'";
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query = "UPDATE repacking_status_info set inventory_check = '"+timeStamp+"', inventory_checked_by = '"+ssoId+"' , status_id = 2  where processorder_packNo = '"+packingdetail.getProcessorderPckgNumber()+"'";
			paramSource = null;
			getNamedParameterJdbcTemplate().update(query, paramSource);	
			
			query="insert into inventorycheck_details (processorder_prod_number,material_description,container_number,"
					+ "container_quantity,inventorycheck_doneby,batch_date,inventorycheck_date,processorder_pckg_number,location_stored) \r\n"
					+ "VALUES ('" + packingdetail.getProcessorderProd().getProcessorderProdNumber()+"' , '"+packingdetail.getMaterialMemo()+"', '"+packingdetail.getContainerTable().getContainerNumber()+"' ,'"
					+containerQuantity+"','"+ssoId+"', '"+packingdetail.getBatchDate()+"' ,'"+timeStamp+"','"+packingdetail.getProcessorderPckgNumber()+"','"+locationPlaced+"')";
					
			paramSource = null;
			getNamedParameterJdbcTemplate().update(query, paramSource);
			
			 query = "insert into process_tracker (processorder_number ,process_id ,ssoid ,process_start_time ,status_id,process_end_time) \r\n"
					+ "VALUES ('" + packingdetail.getProcessorderPckgNumber() + "','7','" + ssoId + "','" + timeStamp
					+ "','2' ,'" + timeStamp + "' )";

			SqlParameterSource param = null;
			KeyHolder keyHolder = new GeneratedKeyHolder();
			getNamedParameterJdbcTemplate().update(query, param, keyHolder);

			return keyHolder.getKey().intValue();
			// return getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	public int setTrackerforInventory(Long processorderProdNumber ,String ssoId) {
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String timeStamp1 = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
		//System.out.println("Inventory Check Time => " + timeStamp);
		try {
			String query = "insert into alerts (process_order_number ,ssoId ,generated_time ,status_id) \r\n"
					+ "VALUES ('" + processorderProdNumber + "','" +ssoId+"','"  + timeStamp + "','2' )";
			SqlParameterSource param = null;
			getNamedParameterJdbcTemplate().update(query,param);
			
			 query = "insert into process_tracker (processorder_number ,process_id ,ssoid ,process_start_time ,status_id,process_end_time) \r\n"
					+ "VALUES ('" + processorderProdNumber + "','7','" + ssoId + "','" + timeStamp
					+ "','2' ,'" + timeStamp + "' )";
			  param = null;
				getNamedParameterJdbcTemplate().update(query,param);

			
			 query = " UPDATE processorder_prod " + " SET  status = 2  ,completed_time='"+timeStamp+"' ,completed_date='"+timeStamp1+"'   WHERE processorder_prod_number = "
					+ processorderProdNumber;

			SqlParameterSource paramSource = null;
			 getNamedParameterJdbcTemplate().update(query, paramSource);
			
			query ="UPDATE status_info set inventory_check = '"+timeStamp+"', inventory_checked_by = '"+ssoId+"' , status_id = 2  where processorder_prodNo = '"+processorderProdNumber+"'"; 
					return getNamedParameterJdbcTemplate().update(query, paramSource);
			
			
		} catch (Exception Ex) {
			Ex.printStackTrace();
			return 0;
		}

	}

	@Override
	public int setRmLocationDetails(Long processorderProdNumber,String rmId,
			LocationRawmaterialDetails locationRawmaterialDetails) {

		/*
		 * List<String> list = new ArrayList<>(); for (Object value :
		 * locationRawmaterialDetails) { if (value instanceof String) { String str =
		 * (String) value; list.addAll(Arrays.asList(str.split(","))); } } String[]
		 * array = list.toArray(new String[list.size()]);
		 * System.out.println(array.toString());
		 */

			String[] iterator = locationRawmaterialDetails.getBatchNumber().split(",");
			for (String value : iterator) {
				try {
					generateBarCodes300PI(value);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println(value);
			}
			iterator = locationRawmaterialDetails.getLocation().split(",");
			for (String value : iterator) {
				try {
					generateBarCodes300PI(value);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println(value);

			}
			iterator = locationRawmaterialDetails.getQuantity().split(",");
			for (String value : iterator) {
				try {
					generateBarCodes300PI(value);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println(value);
			}
		

		try {
			String Query = "UPDATE process_order_rm SET location= '"+locationRawmaterialDetails.getLocation()+"' , batch_number ='"+locationRawmaterialDetails.getBatchNumber()+"' , quantity_taken = '"+locationRawmaterialDetails.getQuantity()+"' \r\n"
					+ "WHERE process_order_number = '"+processorderProdNumber +"'  and rawmaterial_number ='"+rmId+"'  and phase = "+locationRawmaterialDetails.getPhase();
			
			SqlParameterSource paramSource = null;
			getNamedParameterJdbcTemplate().update(Query,paramSource);
			
			
			return 1;
		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
	}
	
public static void generateBarCodes300PI(String fileName) throws IOException {
		
		try {
			Code39Bean bean39 = new Code39Bean();
			final int dpi = 300;

			// Configure the barcode generator
			bean39.setModuleWidth(UnitConv.in2mm(3f / dpi));
			bean39.setWideFactor(2);
			bean39.setMsgPosition(HumanReadablePlacement.HRP_NONE);
			bean39.setBarHeight(6);
			bean39.setDisplayStartStop(true);
			bean39.doQuietZone(false);

			// Open output file

			// Open output file
			File outputFile = new File("C:\\CMS_HOSKOTE\\ProcessOrders\\temp\\output\\barcodes\\" + fileName + ".png");

			FileOutputStream out = new FileOutputStream(outputFile);

			// Set up the canvas provider for monochrome PNG output
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi,
					BufferedImage.TYPE_BYTE_BINARY, false, 0);

			// Generate the barcode
			bean39.generateBarcode(canvas, fileName);

			// Signal end of generation
			canvas.finish();

			//System.out.println("Bar Code is generated successfully…");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}


@Override
public List<ProcessorderProd> getProcessOrdersForQuality(Long processorderProdNumber) {
	try {

		String query = "SELECT p.processorder_prod_number ,prod.product_number,prod.product_description,p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity\r\n"
				+ "from processorder_prod p inner join product_table prod on prod.product_number=p.product_number\r\n"
				+ "inner join status s on s.status_id=p.status\r\n" + "where status=17 and processorder_prod_number="+processorderProdNumber;
			

		List<ProcessorderProd> listOfQualityCheckProcessOrders = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderProd>>() {
					public List<ProcessorderProd> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

						while (rs.next()) {
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(1));
							ProductTable prod = new ProductTable();
							prod.setProductNumber(rs.getLong(2));
							prod.setProductDescription(rs.getString(3));
							processOrderProd.setProductTable(prod);
							processOrderProd.setBatchNumber(rs.getString(4));
							processOrderProd.setBatchDate(rs.getString(5));
							Status s = new Status();
							s.setStatusDescription(rs.getString(6));
							processOrderProd.setStatus(s);
							processOrderProd.setTotalQuantity(rs.getString(7));
							list.add(processOrderProd);
						}
						return list;
					}
				});
		
		query = "SELECT COALESCE(remark,'N/A')  from quality_check_result where process_order ="+processorderProdNumber;
		QualityCheckResult listOfQualityCheckProcessOrders1 = null ;
		listOfQualityCheckProcessOrders1 = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<QualityCheckResult>() {
					public QualityCheckResult extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						QualityCheckResult list = new QualityCheckResult();

						while (rs.next()) {
							
							//System.out.println(rs.getString(1));
							list.setRemark(rs.getString(1));
							
						}
						return list;
					}
				});
		
		for(ProcessorderProd e : listOfQualityCheckProcessOrders) {
			//System.out.println(listOfQualityCheckProcessOrders1.getRemark());
			e.setRemarks(listOfQualityCheckProcessOrders1.getRemark());
			//System.out.println(e.getRemarks());
		}
		
		return listOfQualityCheckProcessOrders;
		
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

@Override
public List<ProcessorderProd> getAllPendingProcessOrders() {
	try {
		
		String query="SELECT p.processorder_prod_number ,prod.product_number,prod.product_description, "
                   +" p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity "
                  +" from processorder_prod p inner join product_table prod on prod.product_number=p.product_number "
                  +" inner join status s on s.status_id=p.status "
                  +" where status=7 "
                  +" and p.batch_date < DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY) "
                 +" order by p.batch_date , p.processorder_prod_number asc";
		
		List<ProcessorderProd> listOfProcessOrders = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderProd>>() {
					public List<ProcessorderProd> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

						while (rs.next()) {
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(1));
							ProductTable prod = new ProductTable();
							prod.setProductNumber(rs.getLong(2));
							prod.setProductDescription(rs.getString(3));
							processOrderProd.setProductTable(prod);
							processOrderProd.setBatchNumber(rs.getString(4));
							processOrderProd.setBatchDate(rs.getString(5));
							Status s = new Status();
							s.setStatusDescription(rs.getString(6));
							processOrderProd.setStatus(s);
							processOrderProd.setTotalQuantity(rs.getString(7));
							list.add(processOrderProd);
						}
						return list;
					}
				});
		return listOfProcessOrders;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

@Override
public int setProcessOrderTrackerStatusForRejection(Long processorderProdNumber, String ssoId ,String submitMessage) {
	String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

	try {
		String query = " UPDATE processorder_prod " + " SET  status = 4  WHERE processorder_prod_number = "
				+ processorderProdNumber;

		SqlParameterSource paramSource = null;
		getNamedParameterJdbcTemplate().update(query,paramSource);
		
		
		
		
		query ="INSERT INTO remark ( `Process_order`, `sso`, `remark`, `status_id`, `remarked_by`) \r\n" + 
				"		VALUES ('"+processorderProdNumber+"','" + ssoId + "','" + submitMessage + "','4', 'Warehouse Manager')";
		 paramSource = null;
		getNamedParameterJdbcTemplate().update(query,paramSource);
		
		query = "insert into alerts (process_order_number ,ssoId ,generated_time ,status_id) \r\n"
				+ "VALUES ('" + processorderProdNumber + "','" + ssoId + "','" + timeStamp + "','4' )";
		SqlParameterSource param = null;
		getNamedParameterJdbcTemplate().update(query,paramSource);
		
		query = "insert into process_tracker (processorder_number ,process_id ,ssoid ,process_start_time ,process_end_time,status_id) \r\n"
				+ "VALUES ('" + processorderProdNumber + "','8','" + ssoId + "','" + timeStamp + "','" + timeStamp + "','4' )";

		param = null;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		return getNamedParameterJdbcTemplate().update(query, param, keyHolder);
		
		

	} catch (Exception e) {
		e.printStackTrace();
		return 0;
	}
}

@Override
public List<ProcessorderProd> getAllContainerDetailsForProcessOrders() {
try {
		
		String query="select p.processorder_prod_number,date_format(p.batch_date,'%d-%m-%Y') as batch_date,p.batch_number ,p.status ,p.product_number, prod.product_description ,p.total_quantity from processorder_prod p"
				+ " inner join product_table prod on prod.product_number=p.product_number\r\n" + 
				"where status=2  order by p.processorder_prod_number desc"; 
				
		List<ProcessorderProd> listOfProcessOrders = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderProd>>() {
					public List<ProcessorderProd> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

						while (rs.next()) {
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(1));
							ProductTable prod = new ProductTable();
							prod.setProductNumber(rs.getLong(5));
							prod.setProductDescription(rs.getString(6));
							processOrderProd.setProductTable(prod);
							processOrderProd.setBatchNumber(rs.getString(3));
							processOrderProd.setBatchDate(rs.getString(2));
							Status s = new Status();
							s.setStatusId(rs.getInt(4));
							processOrderProd.setStatus(s);
							processOrderProd.setTotalQuantity(rs.getString(7));
							list.add(processOrderProd);
						}
						return list;
					}
				});
		return listOfProcessOrders;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}

}

@Override
public List<InventoryCheckDetails> getContainerDetailsForProcessOrder(Long processorderProdNumber) {
try {
		
		String query="SELECT idc.processorder_prod_number ,idc.processorder_pckg_number,idc.container_number ,idc.container_quantity,idc.location_stored ,idc.batch_date,date_format(idc.inventorycheck_date,'%d-%m-%Y') as inventorycheck_date,\r\n" + 
				"				u.user_name,c.container_description\r\n" + 
				"				from inventorycheck_details idc inner join user u on u.ssoid=idc.inventorycheck_doneby\r\n" + 
				"				inner join container_table c on c.container_number=idc.container_number\r\n" + 
				"				where processorder_prod_number="+processorderProdNumber;

				
				
		List<InventoryCheckDetails> getContainerDetailsForProcessOrder = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<InventoryCheckDetails>>() {
					public List<InventoryCheckDetails> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<InventoryCheckDetails> list = new ArrayList<InventoryCheckDetails>();

						while (rs.next()) {
							InventoryCheckDetails idc= new InventoryCheckDetails(); 
							
							ProcessorderProd processorderProd = new ProcessorderProd();
							processorderProd.setProcessorderProdNumber(rs.getLong(1));
							idc.setProcessorderProd(processorderProd);
							
							ProcessorderPckg processOrderPckg = new ProcessorderPckg();
							processOrderPckg.setProcessorderPckgNumber(rs.getLong(2));
							idc.setProcessorderPckg(processOrderPckg);
							
							ContainerTable c = new ContainerTable();
							c.setContainerNumber(rs.getLong(3));
							c.setContainerDescription(rs.getString(9));
							idc.setContainerTable(c);
							
							idc.setContainerQuantity(rs.getString(4));
							idc.setLocation_stored(rs.getString(5));
							idc.setBatchDate(rs.getString(6));
							idc.setInventoryCheckDate(rs.getString(7));
							
							User user = new User();
							user.setUserName(rs.getString(8));
							idc.setUser(user);
							
						
							list.add(idc);
						}
						return list;
					}
				});
		return getContainerDetailsForProcessOrder;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}

}

@Override
public List<ProcessorderPckg> getPackingOrderForInventory() {
	List<ProcessorderPckg> processOrderPack = null;
	String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity,\r\n" + 
			"p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,\r\n" + 
			"p.process_quant,p.processorder_prod_number,p.product_name,c.container_description\r\n" + 
			"FROM processorder_pckg p inner join container_table c on p.container_number = c.container_number \r\n" + 
			" where status = 17 and is_repack = 1 order by p.batch_date ,p.processorder_pckg_number";

	try {

		processOrderPack = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderPckg>>() {
					public List<ProcessorderPckg> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
						while (rs.next()) {
							ProcessorderPckg processorderPckg = new ProcessorderPckg();
							processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
							processorderPckg.setProductNumber(rs.getLong(2));
							processorderPckg.setBatchNumber(rs.getString(4));
							processorderPckg.setBatchDate(rs.getString(5));
							processorderPckg.setTotalQuantity(rs.getLong(6));
							processorderPckg.setWghtPckg(rs.getLong(7));
							Status s = new Status();
							s.setStatusId(rs.getInt(8));
							processorderPckg.setStatus(s);
							ContainerTable ct = new ContainerTable();
							ct.setContainerNumber(rs.getLong(9));
							ct.setContainerDescription(rs.getString(16));
							processorderPckg.setContainerTable(ct);
							processorderPckg.setVolume(rs.getString(10));
							processorderPckg.setSixmlNumber(rs.getString(11));
							processorderPckg.setIsAssociated(rs.getInt(12));
							processorderPckg.setMaterialMemo(rs.getString(3));
							processorderPckg.setProcessQuant(rs.getString(13));
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(14));
							processorderPckg.setProcessorderProd(processOrderProd);
							processorderPckg.setProductName(rs.getString(15));

							list.add(processorderPckg);
						}
						return list;

					}
				});

	} catch (Exception e) {
		e.printStackTrace();

	}
	return processOrderPack;


}

@Override
public List<ProcessorderPckg> getRePackingOrderForRM() {
	try {
	String query = "SELECT   p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity, "
			+ " p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,"
			+ "	p.process_quant,p.processorder_prod_number,p.product_name ,c.container_description"
			+ "	FROM processorder_pckg p inner join container_table c on c.container_number = p.container_number"
			+ " where is_repack=1 and status=23 order by p.batch_date ,p.processorder_pckg_number";

	List<ProcessorderPckg> packingOrdersForRM = getNamedParameterJdbcTemplate().query(query,
			new ResultSetExtractor<List<ProcessorderPckg>>() {
				public List<ProcessorderPckg> extractData(ResultSet rs)
						throws SQLException, DataAccessException {

					List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();

					while (rs.next()) {
						ProcessorderPckg processorderPckg= new ProcessorderPckg(); 
						processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
						processorderPckg.setProductNumber(rs.getLong(2));
						processorderPckg.setBatchNumber(rs.getString(4));
						processorderPckg.setBatchDate(rs.getString(5));
						processorderPckg.setTotalQuantity(rs.getLong(6));
						processorderPckg.setWghtPckg(rs.getLong(7));
						Status s = new Status();
						s.setStatusId(rs.getInt(8));
						processorderPckg.setStatus(s);
						ContainerTable ct = new ContainerTable();
						ct.setContainerNumber(rs.getLong(9));
						ct.setContainerDescription(rs.getString(16));
						processorderPckg.setContainerTable(ct);
						processorderPckg.setVolume(rs.getString(10));
						processorderPckg.setSixmlNumber(rs.getString(11));
						processorderPckg.setIsAssociated(rs.getInt(12));
						processorderPckg.setMaterialMemo(rs.getString(3));
						processorderPckg.setProcessQuant(rs.getString(13));
						ProcessorderProd processOrderProd = new ProcessorderProd();
						processOrderProd.setProcessorderProdNumber(rs.getLong(14));
						processorderPckg.setProcessorderProd(processOrderProd);
						processorderPckg.setProductName(rs.getString(15));
						list.add(processorderPckg);
					}
					return list;
				}
			});
	return packingOrdersForRM;
} catch (Exception e) {
	e.printStackTrace();
	return null;
}

}

@Override
public List<ProcessorderPckg> getRePackingOrderForRMDetails(Long processorderPckgNumber) {
	try {
		String query = "SELECT   p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity, "
				+ " p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,"
				+ "	p.process_quant,p.processorder_prod_number,p.product_name ,c.container_description"
				+ "	FROM processorder_pckg p inner join container_table c on c.container_number = p.container_number"
				+ " where is_repack=1 and status=23 and processorder_pckg_number='"+processorderPckgNumber+"'";

		List<ProcessorderPckg> packingOrdersForRM = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderPckg>>() {
					public List<ProcessorderPckg> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();

						while (rs.next()) {
							ProcessorderPckg processorderPckg= new ProcessorderPckg(); 
							processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
							processorderPckg.setProductNumber(rs.getLong(2));
							processorderPckg.setBatchNumber(rs.getString(4));
							processorderPckg.setBatchDate(rs.getString(5));
							processorderPckg.setTotalQuantity(rs.getLong(6));
							processorderPckg.setWghtPckg(rs.getLong(7));
							Status s = new Status();
							s.setStatusId(rs.getInt(8));
							processorderPckg.setStatus(s);
							ContainerTable ct = new ContainerTable();
							ct.setContainerNumber(rs.getLong(9));
							ct.setContainerDescription(rs.getString(16));
							processorderPckg.setContainerTable(ct);
							processorderPckg.setVolume(rs.getString(10));
							processorderPckg.setSixmlNumber(rs.getString(11));
							processorderPckg.setIsAssociated(rs.getInt(12));
							processorderPckg.setMaterialMemo(rs.getString(3));
							processorderPckg.setProcessQuant(rs.getString(13));
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(14));
							processorderPckg.setProcessorderProd(processOrderProd);
							processorderPckg.setProductName(rs.getString(15));
							list.add(processorderPckg);
						}
						return list;
					}
				});
		return packingOrdersForRM;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}

}

@Override
public int setRmLocationDetailsForRepack(Long processorderPckgNumber,String ssoId ,
		LocationRawmaterialDetails locationRawmaterialDetails) {

	
		String[] iterator = locationRawmaterialDetails.getBatchNumber().split(",");
		for (String value : iterator) {
			try {
				generateBarCodes300PI(value);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println(value);
		}
		iterator = locationRawmaterialDetails.getLocation().split(",");
		for (String value : iterator) {
			try {
				generateBarCodes300PI(value);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println(value);

		}
		iterator = locationRawmaterialDetails.getQuantity().split(",");
		for (String value : iterator) {
			try {
				generateBarCodes300PI(value);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println(value);
		}
	
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	try {
		String Query = "UPDATE processorder_pckg SET location_stored = '"+locationRawmaterialDetails.getLocation()+"' , batchnumber_details ='"+locationRawmaterialDetails.getBatchNumber()+"' , quantity_given = '"+locationRawmaterialDetails.getQuantity()+"' , status = 8 \r\n"
				+ " WHERE processorder_pckg_number = "+processorderPckgNumber ;
		
		SqlParameterSource paramSource = null;
		getNamedParameterJdbcTemplate().update(Query,paramSource);
		
		Query = "insert into process_tracker (processorder_number ,process_id ,ssoid ,process_start_time ,status_id,process_end_time) \r\n"
				+ "VALUES ('" + processorderPckgNumber + "','1','" + ssoId + "','" + timeStamp
				+ "','8' ,'" + timeStamp + "' )";
		
				
		Query = "insert into alerts (process_order_number ,ssoId,generated_time ,status_id) \r\n"
				+ "VALUES ('" + processorderPckgNumber + "','"+ssoId+ "','" + timeStamp + "','8' )";
		
		SqlParameterSource param = null;
		getNamedParameterJdbcTemplate().update(Query,param);
		
		Query = "UPDATE repacking_status_info set  rm_issue = '" + timeStamp + "' , rm_issued_by = '"+ssoId+ "' , status_id = 8  where processorder_packNo = '"+processorderPckgNumber+"' " ;
		
		 param = null;
		getNamedParameterJdbcTemplate().update(Query,param);
		
		
		return 1;
	} catch (Exception ex) {
		ex.printStackTrace();
		return 0;
	}
}

@Override
public List<ProcessorderPckg> getAllContainerDetailsForRepackProcessOrders() {
	List<ProcessorderPckg> processOrderPack = null;
	String query = "SELECT processorder_pckg_number,product_number,material_memo,batch_number, date_format(batch_date,'%d-%m-%Y'),total_quantity,\r\n" + 
			"wght_pckg,status,container_number,volume,sixml_number,is_associated,\r\n" + 
			"process_quant,processorder_prod_number,product_name\r\n" + 
			"FROM processorder_pckg\r\n" + 
			" where status = 2 and is_repack = 1 order by batch_date ,processorder_pckg_number";

	try {

		processOrderPack = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderPckg>>() {
					public List<ProcessorderPckg> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
						while (rs.next()) {
							ProcessorderPckg processorderPckg = new ProcessorderPckg();
							processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
							processorderPckg.setProductNumber(rs.getLong(2));
							processorderPckg.setBatchNumber(rs.getString(4));
							processorderPckg.setBatchDate(rs.getString(5));
							processorderPckg.setTotalQuantity(rs.getLong(6));
							processorderPckg.setWghtPckg(rs.getLong(7));
							Status s = new Status();
							s.setStatusId(rs.getInt(8));
							processorderPckg.setStatus(s);
							ContainerTable ct = new ContainerTable();
							ct.setContainerNumber(rs.getLong(9));
							processorderPckg.setContainerTable(ct);
							processorderPckg.setVolume(rs.getString(10));
							processorderPckg.setSixmlNumber(rs.getString(11));
							processorderPckg.setIsAssociated(rs.getInt(12));
							processorderPckg.setMaterialMemo(rs.getString(3));
							processorderPckg.setProcessQuant(rs.getString(13));
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(14));
							processorderPckg.setProcessorderProd(processOrderProd);
							processorderPckg.setProductName(rs.getString(15));

							list.add(processorderPckg);
						}
						return list;

					}
				});

	} catch (Exception e) {
		e.printStackTrace();

	}
	return processOrderPack;


}

@Override
public List<InventoryCheckDetails> getContainerDetailsForRepackProcessOrder(Long processorderPckgNumber) {
try {
		
		String query="SELECT idc.processorder_prod_number ,idc.processorder_pckg_number,idc.container_number ,idc.container_quantity,idc.location_stored ,idc.batch_date,date_format(idc.inventorycheck_date,'%d-%m-%Y') as inventorycheck_date,\r\n" + 
				"				u.user_name,c.container_description\r\n" + 
				"				from inventorycheck_details idc inner join user u on u.ssoid=idc.inventorycheck_doneby\r\n" + 
				"				inner join container_table c on c.container_number=idc.container_number\r\n" + 
				"				where processorder_pckg_number="+processorderPckgNumber;

				
				
		List<InventoryCheckDetails> getContainerDetailsForProcessOrder = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<InventoryCheckDetails>>() {
					public List<InventoryCheckDetails> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<InventoryCheckDetails> list = new ArrayList<InventoryCheckDetails>();

						while (rs.next()) {
							InventoryCheckDetails idc= new InventoryCheckDetails(); 
							
							ProcessorderProd processorderProd = new ProcessorderProd();
							processorderProd.setProcessorderProdNumber(rs.getLong(1));
							idc.setProcessorderProd(processorderProd);
							
							ProcessorderPckg processOrderPckg = new ProcessorderPckg();
							processOrderPckg.setProcessorderPckgNumber(rs.getLong(2));
							idc.setProcessorderPckg(processOrderPckg);
							
							ContainerTable c = new ContainerTable();
							c.setContainerNumber(rs.getLong(3));
							c.setContainerDescription(rs.getString(9));
							idc.setContainerTable(c);
							
							idc.setContainerQuantity(rs.getString(4));
							idc.setLocation_stored(rs.getString(5));
							idc.setBatchDate(rs.getString(6));
							idc.setInventoryCheckDate(rs.getString(7));
							
							User user = new User();
							user.setUserName(rs.getString(8));
							idc.setUser(user);
							
						
							list.add(idc);
						}
						return list;
					}
				});
		return getContainerDetailsForProcessOrder;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

@Override
public List<ProcessorderPckg> getPackingOrderForInventory1(Long processorderPckgNumber) {
	List<ProcessorderPckg> processOrderPack = null;
	String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity,\r\n" + 
			"p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,\r\n" + 
			"p.process_quant,p.processorder_prod_number,p.product_name,c.container_description\r\n" + 
			"FROM processorder_pckg p inner join container_table c on p.container_number = c.container_number \r\n" + 
			" where status = 17 and is_repack = 1 and processorder_pckg_number="+processorderPckgNumber;

	try {

		processOrderPack = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderPckg>>() {
					public List<ProcessorderPckg> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
						while (rs.next()) {
							ProcessorderPckg processorderPckg = new ProcessorderPckg();
							processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
							processorderPckg.setProductNumber(rs.getLong(2));
							processorderPckg.setBatchNumber(rs.getString(4));
							processorderPckg.setBatchDate(rs.getString(5));
							processorderPckg.setTotalQuantity(rs.getLong(6));
							processorderPckg.setWghtPckg(rs.getLong(7));
							Status s = new Status();
							s.setStatusId(rs.getInt(8));
							processorderPckg.setStatus(s);
							ContainerTable ct = new ContainerTable();
							ct.setContainerNumber(rs.getLong(9));
							ct.setContainerDescription(rs.getString(16));
							processorderPckg.setContainerTable(ct);
							processorderPckg.setVolume(rs.getString(10));
							processorderPckg.setSixmlNumber(rs.getString(11));
							processorderPckg.setIsAssociated(rs.getInt(12));
							processorderPckg.setMaterialMemo(rs.getString(3));
							processorderPckg.setProcessQuant(rs.getString(13));
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(14));
							processorderPckg.setProcessorderProd(processOrderProd);
							processorderPckg.setProductName(rs.getString(15));

							list.add(processorderPckg);
						}
						return list;

					}
				});

	} catch (Exception e) {
		e.printStackTrace();

	}
	return processOrderPack;


}

@Override
public List<ProcessorderPckg> associatedPackingInfoForPosting(long processorderProdNumber) {
	List<ProcessorderPckg> processOrderPack = null;
	
	String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity,\r\n" + 
			"				p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,\r\n" + 
			"				p.process_quant,p.processorder_prod_number,p.product_name ,c.container_description ,p.quantity_after_packing ,p.location_after_packing,s.status_description,p.packing_remarks,p.actual_pallets_used,p.batch_date  \r\n" + 
			"				FROM processorder_pckg p inner join container_table c on c.container_number = p.container_number \r\n"+
			"inner join status s on s.status_id=p.status"+
			" where processorder_prod_number='" + 
			processorderProdNumber + "' ";

	try {

		processOrderPack = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderPckg>>() {
					public List<ProcessorderPckg> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
						while (rs.next()) {
							ProcessorderPckg processorderPckg = new ProcessorderPckg();
							processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
							processorderPckg.setProductNumber(rs.getLong(2));
							processorderPckg.setBatchNumber(rs.getString(4));
							processorderPckg.setBatchDate(rs.getString(5));
							processorderPckg.setTotalQuantity(rs.getLong(6));
							processorderPckg.setWghtPckg(rs.getLong(7));
							Status s = new Status();
							s.setStatusId(rs.getInt(8));
							s.setStatusDescription(rs.getString(19));
							processorderPckg.setStatus(s);
							ContainerTable ct = new ContainerTable();
							ct.setContainerNumber(rs.getLong(9));
							ct.setContainerDescription(rs.getString(16));
							processorderPckg.setContainerTable(ct);
							processorderPckg.setStatus(s);
							processorderPckg.setVolume(rs.getString(10));
							processorderPckg.setSixmlNumber(rs.getString(11));
							processorderPckg.setIsAssociated(rs.getInt(12));
							processorderPckg.setMaterialMemo(rs.getString(3));
							processorderPckg.setProcessQuant(rs.getString(13));
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(14));
							processorderPckg.setProcessorderProd(processOrderProd);
							processorderPckg.setProductName(rs.getString(15));
							processorderPckg.setQuantityAfterPacking(rs.getString(17));
							processorderPckg.setLocationAfterPacking(rs.getString(18));
							processorderPckg.setPackingRemarks(rs.getString(20));
							processorderPckg.setPoUsedPails(rs.getInt(21));
							String dateChanged = reverseDate1(rs.getString(22));
							processorderPckg.setPdfDate(dateChanged);

							list.add(processorderPckg);
						}
						return list;

					}
				});

	} catch (Exception e) {
		e.printStackTrace();

	}
	return processOrderPack;

}

@Override
public List<PackingOrderInventoryPostingResponse> getPackingOrderForInventoryPosting(Long processorderPckgNumber) {
	List<PackingOrderInventoryPostingResponse> processOrderPack = null;
	String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity,\r\n" + 
			"p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,\r\n" + 
			"p.process_quant,p.processorder_prod_number,p.product_name,c.container_description ,p.quantity_after_packing ,p.location_after_packing ,\r\n" + 
			"p.location_stored,\r\n" + 
			"p.batchnumber_details,\r\n" + 
			"p.quantity_given,p.actual_pallets_used,p.packing_remarks \r\n" + 
			"FROM processorder_pckg p inner join container_table c on p.container_number = c.container_number  \r\n" + 
			" where is_repack = 1 and processorder_pckg_number="+processorderPckgNumber;

	try {

		processOrderPack = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<PackingOrderInventoryPostingResponse>>() {
					public List<PackingOrderInventoryPostingResponse> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<PackingOrderInventoryPostingResponse> list = new ArrayList<PackingOrderInventoryPostingResponse>();
						while (rs.next()) {
							PackingOrderInventoryPostingResponse processorderPckg = new PackingOrderInventoryPostingResponse();
							processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
							processorderPckg.setProductNumber(rs.getLong(2));
							processorderPckg.setBatchNumber(rs.getString(4));
							processorderPckg.setBatchDate(rs.getString(5));
							processorderPckg.setTotalQuantity(rs.getLong(6));
							processorderPckg.setWghtPckg(rs.getLong(7));
							Status s = new Status();
							s.setStatusId(rs.getInt(8));
							processorderPckg.setStatus(s);
							ContainerTable ct = new ContainerTable();
							ct.setContainerNumber(rs.getLong(9));
							ct.setContainerDescription(rs.getString(16));
							processorderPckg.setContainerTable(ct);
							processorderPckg.setVolume(rs.getString(10));
							processorderPckg.setSixmlNumber(rs.getString(11));
							processorderPckg.setIsAssociated(rs.getInt(12));
							processorderPckg.setMaterialMemo(rs.getString(3));
							processorderPckg.setProcessQuant(rs.getString(13));
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(14));
							processorderPckg.setProcessorderProd(processOrderProd);
							processorderPckg.setProductName(rs.getString(15));
							processorderPckg.setQuantityAfterPacking(rs.getString(17));
							processorderPckg.setLocationAfterPacking(rs.getString(18));
						    processorderPckg.setPoUsedPails(rs.getInt(22));
						    processorderPckg.setPackingRemarks(rs.getString(23));

							String location = rs.getString(19);
							String batchno = rs.getString(20);
							String quantityTaken = rs.getString(21);
					    if(location != null || batchno != null || quantityTaken != null)
						{	
					    	String[] locationList = location.split(",");
					    	String[] batchnoList = batchno.split(",");
					    	String[] quantityTakenList = quantityTaken.split(",");
					    	List<LocationRm> list1 =new ArrayList<LocationRm>();
					    	for(int i=0;i<locationList.length; i++){
					    		LocationRm  lrm=new LocationRm();
					    		lrm.setLocation(locationList[i]);
					    		lrm.setBatchNumber(batchnoList[i]);
					    		lrm.setQuantityTaken(quantityTakenList[i]);
								list1.add(lrm);
					    	}
				
					    	processorderPckg.setLocationRm(list1);
						
						}
						else {
							processorderPckg.setLocationRm(null);
						}

							
							list.add(processorderPckg);
						}
						return list;

					}
				});

	} catch (Exception e) {
		e.printStackTrace();

	}
	return processOrderPack;


}

@Override
public List<ProcessorderProd> getProcessOrdersForPosting() {
	try {

		String query = "SELECT p.processorder_prod_number ,prod.product_number,prod.product_description,p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity\r\n"
				+ "from processorder_prod p inner join product_table prod on prod.product_number=p.product_number\r\n"
				+ "inner join status s on s.status_id=p.status\r\n" + "where status=2 and is_inventorydone = 0 "
				+ "order by batch_date ,processorder_prod_number ";

		List<ProcessorderProd> listOfQualityCheckProcessOrders = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderProd>>() {
					public List<ProcessorderProd> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

						while (rs.next()) {
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(1));
							ProductTable prod = new ProductTable();
							prod.setProductNumber(rs.getLong(2));
							prod.setProductDescription(rs.getString(3));
							processOrderProd.setProductTable(prod);
							processOrderProd.setBatchNumber(rs.getString(4));
							processOrderProd.setBatchDate(rs.getString(5));
							Status s = new Status();
							s.setStatusDescription(rs.getString(6));
							processOrderProd.setStatus(s);
							processOrderProd.setTotalQuantity(rs.getString(7));
							list.add(processOrderProd);
						}
						return list;
					}
				});
		return listOfQualityCheckProcessOrders;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}

}

@Override
public List<ProcessOrderInventoryPostingResponse> getProcessOrdersForPostingCheck(Long processorderProdNumber) {
	try {

		String query = "SELECT p.processorder_prod_number ,\r\n" + 
				"prod.product_number,\r\n" + 
				"prod.product_description,\r\n" + 
				"p.batch_number,\r\n" + 
				"date_format(p.batch_date,'%d-%m-%Y'),\r\n" + 
				"s.status_description,\r\n" + 
				"p.total_quantity,\r\n" + 
				"por.rawmaterial_number,\r\n" + 
				"rm.rawmaterial_description ,\r\n" + 
				"por.quantity,\r\n" + 
				"por.location,\r\n" + 
				"por.batch_number,\r\n" + 
				"por.quantity_taken,p.batch_date,p.extra_rm,p.actual_quantity_afterPacking\r\n" + 
				"from processorder_prod p \r\n" + 
				"inner join product_table prod on prod.product_number=p.product_number \r\n" + 
				"inner join process_order_rm por on por.process_order_number =p.processorder_prod_number \r\n" + 
				"inner join status s on s.status_id=p.status \r\n" + 
				"inner join rawmaterial_table rm on rm.rawmaterial_number=por.rawmaterial_number \r\n" + 
				" where p.processorder_prod_number='"+processorderProdNumber+"'";

		List<ProcessOrderInventoryPostingResponse> listOfQualityCheckProcessOrders = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessOrderInventoryPostingResponse>>() {
					public List<ProcessOrderInventoryPostingResponse> extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						List<ProcessOrderInventoryPostingResponse> list = new ArrayList<ProcessOrderInventoryPostingResponse>();

						while (rs.next()) {
							ProcessOrderInventoryPostingResponse processOrderProd = new ProcessOrderInventoryPostingResponse();
							processOrderProd.setProcessorderProdNumber(rs.getLong(1));
							String changeDate = reverseDate1(rs.getString(14));
							processOrderProd.setPdfDate(changeDate);
							ProductTable prod = new ProductTable();
							prod.setProductNumber(rs.getLong(2));
							prod.setProductDescription(rs.getString(3));
							processOrderProd.setProductTable(prod);
							processOrderProd.setBatchNumber(rs.getString(4));
							processOrderProd.setBatchDate(rs.getString(5));
							processOrderProd.setExtraRm(rs.getString(15));
							processOrderProd.setProducedQuantity(rs.getString(16));
							Status s = new Status();
							s.setStatusDescription(rs.getString(6));
							processOrderProd.setStatus(s);
							processOrderProd.setTotalQuantity(rs.getString(7));
							RawmaterialTable rm = new RawmaterialTable();
							rm.setRawmaterialNumber(rs.getLong(8));
							rm.setRawmaterialDescription(rs.getString(9));
							processOrderProd.setRawmaterialTable(rm);
							ProcessOrderRm prm = new ProcessOrderRm();
							prm.setQuantity(rs.getString(10));
							processOrderProd.setProcessOrderRm(prm);

							String location = rs.getString(11);
							String batchno = rs.getString(12);
							String quantityTaken = rs.getString(13);
					    if(location != null || batchno != null || quantityTaken != null)
						{	
					    	String[] locationList = location.split(",");
					    	String[] batchnoList = batchno.split(",");
					    	String[] quantityTakenList = quantityTaken.split(",");
					    	List<LocationRm> list1 =new ArrayList<LocationRm>();
					    	for(int i=0;i<locationList.length; i++){
					    		LocationRm  lrm=new LocationRm();
					    		lrm.setLocation(locationList[i]);
					    		lrm.setBatchNumber(batchnoList[i]);
					    		lrm.setQuantityTaken(quantityTakenList[i]);
								list1.add(lrm);
					    	}
				
						processOrderProd.setLocationRm(list1);
						
						}
						else {
						processOrderProd.setLocationRm(null);
						}


							list.add(processOrderProd);
						}
						return list;
					}
				});
		
		query = "SELECT COALESCE(remark,'N/A')  from quality_check_result where process_order ="+processorderProdNumber;
		QualityCheckResult listOfQualityCheckProcessOrders1 = null ;
		listOfQualityCheckProcessOrders1 = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<QualityCheckResult>() {
					public QualityCheckResult extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						QualityCheckResult list = new QualityCheckResult();

						while (rs.next()) {
							
							//System.out.println(rs.getString(1));
							list.setRemark(rs.getString(1));
							
						}
						return list;
					}
				});
		
		for(ProcessOrderInventoryPostingResponse e : listOfQualityCheckProcessOrders) {
			//System.out.println(listOfQualityCheckProcessOrders1.getRemark());
			e.setRemarks(listOfQualityCheckProcessOrders1.getRemark());
			//System.out.println(e.getRemarks());
		}
		
		return listOfQualityCheckProcessOrders;
		
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}

}
@Override
public List<ProcessorderPckg> getPackingOrderForInventoryPostingList() {
	List<ProcessorderPckg> processOrderPack = null;
	String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity,\r\n" + 
			"p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,\r\n" + 
			"p.process_quant,p.processorder_prod_number,p.product_name,c.container_description ,p.quantity_after_packing ,p.location_after_packing \r\n" + 
			"FROM processorder_pckg p inner join container_table c on p.container_number = c.container_number \r\n" + 
			" where status = 2 and is_repack = 1 and is_inventorydone= 0 order by p.batch_date ,p.processorder_pckg_number";

	try {

		processOrderPack = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<List<ProcessorderPckg>>() {
					public List<ProcessorderPckg> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
						while (rs.next()) {
							ProcessorderPckg processorderPckg = new ProcessorderPckg();
							processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
							processorderPckg.setProductNumber(rs.getLong(2));
							processorderPckg.setBatchNumber(rs.getString(4));
							processorderPckg.setBatchDate(rs.getString(5));
							processorderPckg.setTotalQuantity(rs.getLong(6));
							processorderPckg.setWghtPckg(rs.getLong(7));
							Status s = new Status();
							s.setStatusId(rs.getInt(8));
							processorderPckg.setStatus(s);
							ContainerTable ct = new ContainerTable();
							ct.setContainerNumber(rs.getLong(9));
							ct.setContainerDescription(rs.getString(16));
							processorderPckg.setContainerTable(ct);
							processorderPckg.setVolume(rs.getString(10));
							processorderPckg.setSixmlNumber(rs.getString(11));
							processorderPckg.setIsAssociated(rs.getInt(12));
							processorderPckg.setMaterialMemo(rs.getString(3));
							processorderPckg.setProcessQuant(rs.getString(13));
							ProcessorderProd processOrderProd = new ProcessorderProd();
							processOrderProd.setProcessorderProdNumber(rs.getLong(14));
							processorderPckg.setProcessorderProd(processOrderProd);
							processorderPckg.setProductName(rs.getString(15));
							processorderPckg.setQuantityAfterPacking(rs.getString(17));
							processorderPckg.setLocationAfterPacking(rs.getString(18));

							list.add(processorderPckg);
						}
						return list;

					}
				});

	} catch (Exception e) {
		e.printStackTrace();

	}
	return processOrderPack;


}

@Override
public int setProcessOrderStatusForPosting(Long processorderProdNumber ,String ssoId) {
	String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	try {
		String query = " UPDATE processorder_prod " + " SET  is_inventorydone = 1  WHERE processorder_prod_number = "
				+ processorderProdNumber;

		SqlParameterSource paramSource = null;
		getNamedParameterJdbcTemplate().update(query,paramSource);
		
		query = " UPDATE processorder_pckg " + " SET  is_inventorydone = 1  WHERE processorder_prod_number = "
				+ processorderProdNumber;

		paramSource = null;
		
		 getNamedParameterJdbcTemplate().update(query,paramSource);
		
			query =  "UPDATE status_info set inventory_posting = '"+timeStamp+"', inventory_posted_by = '"+ssoId+"'  where processorder_prodNo = '"+processorderProdNumber+"'";
	
		return getNamedParameterJdbcTemplate().update(query, paramSource);
		
		
		

	} catch (Exception e) {
		e.printStackTrace();
		return 0;
	}
}

@Override
public int setProcessOrderStatusForRepackPosting(Long processorderPckgNumber ,String ssoId) {
	String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	try {
		String query = " UPDATE processorder_pckg " + " SET  is_inventorydone = 1  WHERE processorder_pckg_number = "
				+ processorderPckgNumber;


		SqlParameterSource paramSource = null;
		 getNamedParameterJdbcTemplate().update(query,paramSource);
		 query =  "UPDATE status_info set inventory_posting = '"+timeStamp+"', inventory_posted_by = '"+ssoId+"'   where processorder_prodNo = '"+processorderPckgNumber+"'";
		 return getNamedParameterJdbcTemplate().update(query, paramSource);
		
	} catch (Exception e) {
		e.printStackTrace();
		return 0;
	}
}

@Override
public RepackingProcessOrderCount getRepackingCount() {
	
	
	try {
		
		String query = "SELECT count(processorder_pckg_number) as PendingOrders\r\n" + 
				"FROM processorder_pckg where is_repack = 1 and status=23 and week(batch_date)=week(curdate())";
		
		
		RepackingProcessOrderCount repackingCount1 = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<RepackingProcessOrderCount>() {
					public RepackingProcessOrderCount extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						RepackingProcessOrderCount list = new RepackingProcessOrderCount();
						while (rs.next()) {
							
							
							list.setPendingOrders( rs.getString(1) == null ? "0" : rs.getString(1));
							
						}
						return list;
					}
				});
		
		
		
		query = "SELECT count(processorder_pckg_number) as CompletedOrders\r\n" + 
				"FROM processorder_pckg where is_repack = 1 and status not in (23,4,2) and week(batch_date)=week(curdate())";
		
		
		RepackingProcessOrderCount repackingCount2 = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<RepackingProcessOrderCount>() {
					public RepackingProcessOrderCount extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						
						RepackingProcessOrderCount list = new RepackingProcessOrderCount();
						while (rs.next()) {
							
							
							list.setCompletedOrders(rs.getString(1) == null ? "0" : rs.getString(1));
							
						}
						return list;
					}
				});
		
		repackingCount2.setPendingOrders(repackingCount1.getPendingOrders());
		 
		 return repackingCount2;
	}
	
	catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

@Override
public NotificationTabsForWarehouse getNotificationForWarehouseCount() {

	try {

		String query = "select count(processorder_prod_number) from processorder_prod where status= 7 \r\n" + 
				"and YEARWEEK( batch_date, 1) >= YEARWEEK(CURDATE(),1)";

		NotificationTabsForWarehouse processOrderCount = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<NotificationTabsForWarehouse>() {
			public NotificationTabsForWarehouse extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NotificationTabsForWarehouse list = new NotificationTabsForWarehouse();
				while (rs.next()) {

					list.setProcessOrdersCount( rs.getString(1) == null ? "0" : rs.getString(1));

				}
				return list;
			}
		});
		
		
		query = "select count(processorder_prod_number) from processorder_prod where status= 7 \r\n" + 
				"and batch_date < DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY)";

		NotificationTabsForWarehouse pendingOrderCount = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<NotificationTabsForWarehouse>() {
			public NotificationTabsForWarehouse extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NotificationTabsForWarehouse list = new NotificationTabsForWarehouse();
				while (rs.next()) {


					list.setPendingOrdersCount( rs.getString(1) == null ? "0" : rs.getString(1));

				}
				return list;
			}
		});
		
		
		query = "SELECT count(processorder_pckg_number) FROM processorder_pckg where status= 23\r\n" + 
				" and is_repack = 1";
		NotificationTabsForWarehouse repackingOrderCount = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<NotificationTabsForWarehouse>() {
			public NotificationTabsForWarehouse extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NotificationTabsForWarehouse list = new NotificationTabsForWarehouse();
				while (rs.next()) {


					list.setRepackingOrdersCount( rs.getString(1) == null ? "0" : rs.getString(1));

				}
				return list;
			}
		});
		
		query = "SELECT count(processorder_prod_number) FROM processorder_prod where \r\n" + 
				"status = 17";
		NotificationTabsForWarehouse inventoryCheckCount = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<NotificationTabsForWarehouse>() {
			public NotificationTabsForWarehouse extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NotificationTabsForWarehouse list = new NotificationTabsForWarehouse();
				while (rs.next()) {


					list.setInventoryCheckCount( rs.getString(1) == null ? "0" : rs.getString(1));

				}
				return list;
			}
		});
		
		query = "SELECT count(processorder_pckg_number) FROM processorder_pckg where \r\n" + 
				"is_repack = 1 and status = 17 ";
		NotificationTabsForWarehouse repackingInventoryCheckCount = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<NotificationTabsForWarehouse>() {
			public NotificationTabsForWarehouse extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NotificationTabsForWarehouse list = new NotificationTabsForWarehouse();
				while (rs.next()) {

					list.setRepackInventoryCheckCount( rs.getString(1) == null ? "0" : rs.getString(1));

				}
				return list;
			}
		});
		
		query = "SELECT count(processorder_pckg_number) FROM processorder_pckg where \r\n" + 
				"status = 2 and is_repack = 1 and is_inventorydone = 0";
		NotificationTabsForWarehouse repackingInventoryPostingCount = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<NotificationTabsForWarehouse>() {
			public NotificationTabsForWarehouse extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NotificationTabsForWarehouse list = new NotificationTabsForWarehouse();
				while (rs.next()) {

					list.setRepackInventoryPostingCount( rs.getString(1) == null ? "0" : rs.getString(1));

				}
				return list;
			}
		});
		
		query = "SELECT count(processorder_prod_number) FROM processorder_prod where \r\n" + 
				"status = 2 and is_inventorydone = 0";
		
		NotificationTabsForWarehouse InventoryPostingCount = getNamedParameterJdbcTemplate().query(query,
				new ResultSetExtractor<NotificationTabsForWarehouse>() {
			public NotificationTabsForWarehouse extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				NotificationTabsForWarehouse list = new NotificationTabsForWarehouse();
				while (rs.next()) {

					list.setInventoryPostingCount( rs.getString(1) == null ? "0" : rs.getString(1));

				}
				return list;
			}
		});
		
		InventoryPostingCount.setProcessOrdersCount(processOrderCount.getProcessOrdersCount());
		InventoryPostingCount.setPendingOrdersCount(pendingOrderCount.getPendingOrdersCount());
		InventoryPostingCount.setRepackingOrdersCount(repackingOrderCount.getRepackingOrdersCount());
		InventoryPostingCount.setInventoryCheckCount(inventoryCheckCount.getInventoryCheckCount());
		InventoryPostingCount.setRepackInventoryCheckCount(repackingInventoryCheckCount.getRepackInventoryCheckCount());
		InventoryPostingCount.setRepackInventoryPostingCount(repackingInventoryPostingCount.getRepackInventoryPostingCount());
		
		return InventoryPostingCount;
	}
	catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}

public String reverseDate1(String date) {
	String[] splited = date.split("-");
	String reversedDate = splited[splited.length - 1]  + splited[splited.length - 2] + splited[0];

	return reversedDate;

}





}
