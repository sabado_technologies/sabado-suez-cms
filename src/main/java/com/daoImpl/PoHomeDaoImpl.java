package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.dao.PoHomeDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.Status;

@Repository
public class PoHomeDaoImpl extends NamedParameterJdbcDaoSupportClass implements PoHomeDao{
	
	
	public List<ProcessorderProd> getAllOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			
			String query = "SELECT p.processorder_prod_number, p.product_number ,pt.product_description,\r\n"
					+ "s.status_description ,p.batch_date,DATEDIFF(p.batch_date,'" + timeStamp
					+ "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status where (p.status =8 or p.status =15 or p.status =18 or p.status =14 or p.status =16)\r\n"
					+"and YEARWEEK(p.batch_date, 1) = YEARWEEK(' "+timeStamp+" ',1)\r\n "
					+ "order by diffs asc,p.processorder_prod_number asc";
					//+ "and MONTH(p.batch_date) = MONTH(' "+timeStamp+" ') order by diffs desc";

			List<ProcessorderProd> allorders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								String reversedDate =reverseDate(rs.getString(5));
								prod.setBatchDate(reversedDate);
								////System.out.println("Reversed =>" + prod.getBatchDate());
								prod.setVolume(rs.getString(6));
								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(4));
							    s.setStatusId(rs.getInt(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return allorders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
	public List<ProcessorderProd> getTodaysOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where (p.status =8 or p.status =15 or p.status =18 or p.status =14 or p.status =16) and p.batch_date='"
					+ timeStamp + "' order by p.processorder_prod_number asc";

			List<ProcessorderProd> todays = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return todays;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getPreviousOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where (p.status =8 or p.status =15 or p.status =18 or p.status =14 or p.status =16) and p.batch_date <'"
					+ timeStamp + "' order by diffs,p.processorder_prod_number asc";

			List<ProcessorderProd> previous = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return previous;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getFutureOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" 
					+ "where (p.status =8 or p.status =15 or p.status =18 or p.status =14 or p.status =16) and p.batch_date >'"+ timeStamp + "' \r\n"
					+ "and YEARWEEK(p.batch_date, 1) = YEARWEEK(' "+timeStamp+" ',1) \r\n"
					+ "order by diffs,p.processorder_prod_number asc";

			List<ProcessorderProd> future = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return future;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<ProcessorderProd> getTodaysCompletedOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where p.status =2 and p.batch_date='"
					+ timeStamp + "' order by diffs desc,p.processorder_prod_number asc";

			List<ProcessorderProd> completed = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return completed;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
	public String reverseDate(String date){
		
		//System.out.println("To be =>" + date);
		String[] splited =date.split("-");
		//System.out.println("Last =>" +splited[splited.length-1]);
		//System.out.println("First =>" + splited[0]);
		//System.out.println("Middle =>" +splited[splited.length-2]);
		String reversedDate=splited[splited.length-1] +"-" +splited[splited.length-2] +"-" +splited[0];
		//System.out.println("Final Date =>"+reversedDate);
		
		return reversedDate;
		
		}


}
