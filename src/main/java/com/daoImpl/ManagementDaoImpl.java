package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.ManagementDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.Status;
import com.highchart.CurrentQuarterReportResponse;
import com.highchart.ProductForChart;
import com.highchart.YearlyBarchartResponse;
import com.response.AverageCycleTimeResponse;
import com.response.CycleTimeResponse;
import com.response.DailyDataResponse;
import com.response.FullQuarterReportResponse;
import com.response.ManagementQuarterProductionPlan;
import com.response.ManagementReportResponse;
import com.response.MonthlyResponse;
import com.response.PackingReportResponse;
import com.response.PlannedReportResponse;
import com.response.QuarterBarchartResponse;
import com.response.QuarterBarchartResponse1;
import com.response.QuarterlyDataResponse;
import com.response.RepackingReportResponse;
import com.response.WeeklyDataResponse;
import com.response.WeekwiseData;
import com.response.YearlyReportResponse;

@Repository
public class ManagementDaoImpl extends NamedParameterJdbcDaoSupportClass implements ManagementDao {

	@Override
	public List<ProcessorderProd> getDailyProcessOrders() {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			final Calendar cal = Calendar.getInstance();
			// cal.add(Calendar.DATE, -1);

			String localDate = dateFormat.format(cal.getTime());

			String query = "select p.processorder_prod_number,prod.product_number,"
					+ " prod.product_description,p.total_quantity,"
					+ " p.batch_number,p.batch_date,p.status,s.status_description "
					+ " from processorder_prod p inner join product_table prod on p.product_number=prod.product_number "
					+ " inner join status s on  s.status_id=p.status " + " where completed_date='" + localDate + "'";

			List<ProcessorderProd> dailyProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processorderprod = new ProcessorderProd();
								processorderprod.setProcessorderProdNumber(rs.getLong(1));
								ProductTable productTable = new ProductTable();
								productTable.setProductNumber(rs.getLong(2));
								productTable.setProductDescription(rs.getString(3));
								processorderprod.setProductTable(productTable);
								processorderprod.setTotalQuantity(rs.getString(4));
								processorderprod.setBatchNumber(rs.getString(5));
								processorderprod.setBatchDate(rs.getString(6));
								Status status = new Status();
								status.setStatusId(rs.getInt(7));
								status.setStatusDescription(rs.getString(8));
								processorderprod.setStatus(status);
								list.add(processorderprod);
							}
							return list;
						}
					});
			return dailyProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	// ----------------------------------------------------------------------------------------------------------
	// Current Week Production Report

	public String DateFormator(String date) {
		String s = date;
		String ar[] = s.split("-");
		String str2 = ar[2];
		str2 = str2.concat("-").concat(ar[1]).concat("-").concat(ar[0]);
		return str2;

	}

	@Override
	public List<ManagementReportResponse> getWeeklyProcessOrders() {
		try {
			String query = "select DATE_FORMAT(qb.day_series,'%d %M %Y') as days , COALESCE(sum(actual_quantity_afterPacking)/1000, 0) as quantity "
					+ " from processorder_pckg qa right join ( "
					+ " select DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY) + INTERVAL "
					+ " a.a day as day_series from(  select 0 as a union all select 1 union all select 2 union all "
					+ " select 3 union all select 4 union all  select 5 union all select 6 ) as a ) as qb "
					+ " on date(qa.packing_completed_date) = qb.day_series and status in (2,14,17,18) and "
					+ " qa.packing_completed_date > DATE_SUB(curdate(), INTERVAL 7 day ) group by qb.day_series asc ";
			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> weekWiseReports = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse weekWiseReports = new ManagementReportResponse();
							// weekWiseReports.setBatchDate(rs.getString(2));
							weekWiseReports.setSum1(rs.getFloat("quantity"));
							weekWiseReports.setBatchDate(rs.getString(1));
							return weekWiseReports;

						}
					});
			return weekWiseReports;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	@Override
	public List<ManagementReportResponse> getWeeklyProductionValue() {

		try {

			String query = "SELECT qb.day_series as days , coalesce(sum(per_day),0) as quantity \r\n"
					+ "					 from planfor_report qa right join ( \r\n"
					+ "					select DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY) + \r\n"
					+ "                    INTERVAL a.a day as day_series from( \r\n"
					+ "					 select 0 as a union all select 1 union all select 2 union all \r\n"
					+ "					select 3 union all select 4 union all  select 5 union all select 6 ) as a ) as qb \r\n"
					+ "					on date(qa.working_dates) = qb.day_series and \r\n"
					+ "					qa.working_dates > DATE_SUB(curdate(), INTERVAL 7 day )\r\n"
					+ "					 group by qb.day_series asc ";

			MapSqlParameterSource param = new MapSqlParameterSource();
			List<ManagementReportResponse> plannedProductWiseReports = getNamedParameterJdbcTemplate().query(query,
					param, new RowMapper<ManagementReportResponse>() {

						@Override
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse plannedProductWiseReports = new ManagementReportResponse();
							plannedProductWiseReports.setBatchDate(rs.getString(1));
							plannedProductWiseReports.setWeeklySum(rs.getFloat(2));
							return plannedProductWiseReports;

						}
					});
			return plannedProductWiseReports;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	// -------------------------------------------------------------------------------------------------------

	// Current Year Product Wise Report

	public List<ManagementReportResponse> productWisereport() {
		try {
			/*
			 * String query =
			 * "select pr.processorder_prod_number,p.product_number, p.product_description, pr.batch_date, "
			 * + " sum(pr.total_quantity) as quantity,pr.reactor,pr.completed_date" +
			 * " from processorder_prod pr " + " inner join product_table p " +
			 * " on  p.product_number=pr.product_number " +
			 * " where  YEAR(pr.completed_date)=  YEAR(CURDATE()) and pr.status=2 " +
			 * " group by  p.product_description";
			 */

			String query = "select product_name, product_num,round(sum(Quantity)/1000,2) as Quantity,"
					+ "sum(1400kg) as p1, sum(1300kg) as p2,  sum(1200kg) as p3, sum(1100kg) as p4, sum(1000kg) as p5,"
					+ " sum(870kg) as p6,  sum(860kg) as p7,   sum(810kg) as p8,  sum(800kg) as p9, sum(280kg) as p10,"
					+ " sum(272kg) as p11, sum(250kg) as p12,  sum(240kg) as p13, sum(230kg) as p14, sum(227kg) as p15,"
					+ " sum(220kg) as p16,  sum(210kg) as p17,   sum(206kg) as p18,  sum(205kg) as p19,  sum(204kg) as p20,"
					+ " sum(200kg) as p21,  sum(190kg) as p22,   sum(188kg) as p23,   sum(185kg) as p24,  sum(180kg) as p25,"
					+ " sum(175kg) as p26,   sum(170kg) as p27,   sum(165kg) as p28,   sum(160kg) as p29,  sum(150kg) as p30,"
					+ " sum(110kg) as p31,   sum(100kg) as p32,   sum(80kg) as p33,  sum(70kg) as p34,  sum(60kg) as p35,"
					+ " sum(50kg) as p36, sum(28kg) as p37, sum(25kg) as p38, sum(20kg) as p39, sum(04kg) as p40, sum(02kg) as p41"
					+ " from " + " (select material_memo as product_name ,product_number as product_num,"
					+ "   sum(actual_quantity_afterPacking) as Quantity,"
					+ " (case when wght_pckg=1400 then sum(quantity_after_packing) end) as 1400kg,"
					+ "(case when wght_pckg=1300 then sum(quantity_after_packing) end) as 1300kg,"
					+ "  (case when wght_pckg=1200 then sum(quantity_after_packing) end) as 1200kg,"
					+ "	(case when wght_pckg=1100 then sum(quantity_after_packing) end) as 1100kg,"
					+ "  (case when wght_pckg=1000 then sum(quantity_after_packing) end) as 1000kg ,"
					+ "  (case when wght_pckg=870 then sum(quantity_after_packing) end) as 870kg ,"
					+ " (case when wght_pckg=860 then sum(quantity_after_packing) end) as 860kg ,"
					+ "  (case when wght_pckg=810 then sum(quantity_after_packing) end) as 810kg,"
					+ "   (case when wght_pckg=800 then sum(quantity_after_packing) end) as 800kg,"
					+ " (case when wght_pckg=280 then sum(quantity_after_packing) end) as 280kg ,"
					+ " (case when wght_pckg=272 then sum(quantity_after_packing) end) as 272kg ,"
					+ " (case when wght_pckg=250 then sum(quantity_after_packing) end) as 250kg ,"
					+ " (case when wght_pckg=240 then sum(quantity_after_packing) end) as 240kg , "
					+ "(case when wght_pckg=230 then sum(quantity_after_packing) end) as 230kg ,"
					+ " (case when wght_pckg=227 then sum(quantity_after_packing) end) as 227kg ,"
					+ " (case when wght_pckg=220 then sum(quantity_after_packing) end) as 220kg ,"
					+ " (case when wght_pckg=210 then sum(quantity_after_packing) end) as 210kg ,"
					+ "  (case when wght_pckg=206 then sum(quantity_after_packing) end) as 206kg, "
					+ " (case when wght_pckg=205 then sum(quantity_after_packing) end) as 205kg ,"
					+ " (case when wght_pckg=204 then sum(quantity_after_packing) end) as 204kg ,"
					+ " (case when wght_pckg=200 then sum(quantity_after_packing) end) as 200kg ,"
					+ " (case when wght_pckg=190 then sum(quantity_after_packing) end) as 190kg ,"
					+ "  (case when wght_pckg=188 then sum(quantity_after_packing) end) as 188kg, "
					+ " (case when wght_pckg=185 then sum(quantity_after_packing) end) as 185kg ,"
					+ " (case when wght_pckg=180 then sum(quantity_after_packing) end) as 180kg, "
					+ " (case when wght_pckg=175 then sum(quantity_after_packing) end) as 175kg, "
					+ " (case when wght_pckg=170 then sum(quantity_after_packing) end) as 170kg, "
					+ " (case when wght_pckg=165 then sum(quantity_after_packing) end) as 165kg ,"
					+ "  (case when wght_pckg=160 then sum(quantity_after_packing) end) as 160kg ,"
					+ " (case when wght_pckg=150 then sum(quantity_after_packing) end) as 150kg,"
					+ " (case when wght_pckg=110 then sum(quantity_after_packing) end) as 110kg ,"
					+ "  (case when wght_pckg=100 then sum(quantity_after_packing) end) as 100kg ,"
					+ " (case when wght_pckg=80 then sum(quantity_after_packing) end) as 80kg ,"
					+ " (case when wght_pckg=70 then sum(quantity_after_packing) end) as 70kg, "
					+ " (case when wght_pckg=60 then sum(quantity_after_packing) end) as 60kg,    "
					+ " (case when wght_pckg=50 then sum(quantity_after_packing) end) as 50kg,"
					+ " (case when wght_pckg=28 then sum(quantity_after_packing) end) as 28kg,"
					+ " (case when wght_pckg=25 then sum(quantity_after_packing) end) as 25kg,"
					+ "  (case when wght_pckg=20 then sum(quantity_after_packing) end) as 20kg,"
					+ " (case when wght_pckg=0.4 then sum(quantity_after_packing) end) as 04kg ,"
					+ " (case when wght_pckg=0.2 then sum(quantity_after_packing) end) as 02kg "
					+ " from processorder_pckg "
					+ " where status in( 2,14,17,18) and year(packing_completed_date)=year(curdate())"
					+ " group by product_number,wght_pckg,quantity_after_packing ) as t group by product_name ";

			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> productwiseReports = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {

						@Override
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse productwiseReport = new ManagementReportResponse();

							productwiseReport.setProductDescription(
									(rs.getString(1) == "" || rs.getString(1) == null || rs.getString(1) == "0") ? "-"
											: rs.getString(1));
							productwiseReport.setProductnumber(rs.getLong(2));
							productwiseReport.setSum1(
									(rs.getFloat(3) == 0.0f ) ? 0
											: rs.getFloat(3));

							productwiseReport.setPckg1(
									(rs.getString(4) == "" || rs.getString(4) == null || rs.getString(4) == "0") ? "-"
											: rs.getString(4));
							productwiseReport.setPckg2(
									(rs.getString(5) == "" || rs.getString(5) == null || rs.getString(5) == "0") ? "-"
											: rs.getString(5));
							productwiseReport.setPckg3(
									(rs.getString(6) == "" || rs.getString(6) == null || rs.getString(6) == "0") ? "-"
											: rs.getString(6));
							productwiseReport.setPckg4(
									(rs.getString(7) == "" || rs.getString(7) == null || rs.getString(7) == "0") ? "-"
											: rs.getString(7));
							productwiseReport.setPckg5(
									(rs.getString(8) == "" || rs.getString(8) == null || rs.getString(8) == "0") ? "-"
											: rs.getString(8));
							productwiseReport.setPckg6(
									(rs.getString(9) == "" || rs.getString(9) == null || rs.getString(9) == "0") ? "-"
											: rs.getString(9));
							productwiseReport.setPckg7(
									(rs.getString(10) == "" || rs.getString(10) == null || rs.getString(10) == "0")
											? "-"
											: rs.getString(10));
							productwiseReport.setPckg8(
									(rs.getString(11) == "" || rs.getString(11) == null || rs.getString(11) == "0")
											? "-"
											: rs.getString(11));
							productwiseReport.setPckg9(
									(rs.getString(12) == "" || rs.getString(12) == null || rs.getString(12) == "0")
											? "-"
											: rs.getString(12));
							productwiseReport.setPckg10(
									(rs.getString(13) == "" || rs.getString(13) == null || rs.getString(13) == "0")
											? "-"
											: rs.getString(13));
							productwiseReport.setPckg11(
									(rs.getString(14) == "" || rs.getString(14) == null || rs.getString(14) == "0")
											? "-"
											: rs.getString(14));
							productwiseReport.setPckg12(
									(rs.getString(15) == "" || rs.getString(15) == null || rs.getString(15) == "0")
											? "-"
											: rs.getString(15));
							productwiseReport.setPckg13(
									(rs.getString(16) == "" || rs.getString(16) == null || rs.getString(16) == "0")
											? "-"
											: rs.getString(16));
							productwiseReport.setPckg14(
									(rs.getString(17) == "" || rs.getString(17) == null || rs.getString(17) == "0")
											? "-"
											: rs.getString(17));
							productwiseReport.setPckg15(
									(rs.getString(18) == "" || rs.getString(18) == null || rs.getString(18) == "0")
											? "-"
											: rs.getString(18));
							productwiseReport.setPckg16(
									(rs.getString(19) == "" || rs.getString(19) == null || rs.getString(19) == "0")
											? "-"
											: rs.getString(19));
							productwiseReport.setPckg17(
									(rs.getString(20) == "" || rs.getString(20) == null || rs.getString(20) == "0")
											? "-"
											: rs.getString(20));
							productwiseReport.setPckg18(
									(rs.getString(21) == "" || rs.getString(21) == null || rs.getString(21) == "0")
											? "-"
											: rs.getString(21));
							productwiseReport.setPckg19(
									(rs.getString(22) == "" || rs.getString(22) == null || rs.getString(22) == "0")
											? "-"
											: rs.getString(22));
							productwiseReport.setPckg20(
									(rs.getString(23) == "" || rs.getString(23) == null || rs.getString(23) == "0")
											? "-"
											: rs.getString(23));
							productwiseReport.setPckg21(
									(rs.getString(24) == "" || rs.getString(24) == null || rs.getString(24) == "0")
											? "-"
											: rs.getString(24));
							productwiseReport.setPckg22(
									(rs.getString(25) == "" || rs.getString(25) == null || rs.getString(25) == "0")
											? "-"
											: rs.getString(25));
							productwiseReport.setPckg23(
									(rs.getString(26) == "" || rs.getString(26) == null || rs.getString(26) == "0")
											? "-"
											: rs.getString(26));
							productwiseReport.setPckg24(
									(rs.getString(27) == "" || rs.getString(27) == null || rs.getString(27) == "0")
											? "-"
											: rs.getString(27));
							productwiseReport.setPckg25(
									(rs.getString(28) == "" || rs.getString(28) == null || rs.getString(28) == "0")
											? "-"
											: rs.getString(28));
							productwiseReport.setPckg26(
									(rs.getString(29) == "" || rs.getString(29) == null || rs.getString(29) == "0")
											? "-"
											: rs.getString(29));
							productwiseReport.setPckg27(
									(rs.getString(30) == "" || rs.getString(30) == null || rs.getString(30) == "0")
											? "-"
											: rs.getString(30));
							productwiseReport.setPckg28(
									(rs.getString(31) == "" || rs.getString(31) == null || rs.getString(31) == "0")
											? "-"
											: rs.getString(31));
							productwiseReport.setPckg29(
									(rs.getString(32) == "" || rs.getString(32) == null || rs.getString(32) == "0")
											? "-"
											: rs.getString(32));
							productwiseReport.setPckg30(
									(rs.getString(33) == "" || rs.getString(33) == null || rs.getString(33) == "0")
											? "-"
											: rs.getString(33));
							productwiseReport.setPckg31(
									(rs.getString(34) == "" || rs.getString(34) == null || rs.getString(34) == "0")
											? "-"
											: rs.getString(34));
							productwiseReport.setPckg32(
									(rs.getString(35) == "" || rs.getString(35) == null || rs.getString(35) == "0")
											? "-"
											: rs.getString(35));
							productwiseReport.setPckg33(
									(rs.getString(36) == "" || rs.getString(36) == null || rs.getString(36) == "0")
											? "-"
											: rs.getString(36));
							productwiseReport.setPckg34(
									(rs.getString(37) == "" || rs.getString(37) == null || rs.getString(37) == "0")
											? "-"
											: rs.getString(37));
							productwiseReport.setPckg35(
									(rs.getString(38) == "" || rs.getString(38) == null || rs.getString(38) == "0")
											? "-"
											: rs.getString(38));
							productwiseReport.setPckg36(
									(rs.getString(39) == "" || rs.getString(39) == null || rs.getString(39) == "0")
											? "-"
											: rs.getString(39));
							productwiseReport.setPckg37(
									(rs.getString(40) == "" || rs.getString(40) == null || rs.getString(40) == "0")
											? "-"
											: rs.getString(40));
							productwiseReport.setPckg38(
									(rs.getString(41) == "" || rs.getString(41) == null || rs.getString(41) == "0")
											? "-"
											: rs.getString(41));
							productwiseReport.setPckg39(
									(rs.getString(42) == "" || rs.getString(42) == null || rs.getString(42) == "0")
											? "-"
											: rs.getString(42));
							productwiseReport.setPckg40(
									(rs.getString(43) == "" || rs.getString(43) == null || rs.getString(43) == "0")
											? "-"
											: rs.getString(43));
							productwiseReport.setPckg41(
									(rs.getString(44) == "" || rs.getString(44) == null || rs.getString(44) == "0")
											? "-"
											: rs.getString(44));

							return productwiseReport;

						}
					});
			return productwiseReports;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	public List<ManagementReportResponse> getPacketSizeCounts() {
		try {
			String query = "select  round(sum(totalVolume)/1000,2) as TotalVolume,"
					+ "sum(pck1) as pckg1,sum(pck2) as pckg2,sum(pck3) as pckg3,sum(pck4) as pckg4,sum(pck5) as pckg5 ,"
					+ " sum(pck6) as pckg6,sum(pck7) as pckg7,sum(pck8) as pckg8,sum(pck9) as pckg9,sum(pck10) as pckg10,"
					+ "sum(pck11) as pckg11,sum(pck12) as pckg12,sum(pck13) as pckg13,sum(pck14) as pckg14,sum(pck15) as pckg15,"
					+ "sum(pck16) as pckg16,sum(pck17) as pckg17,sum(pck18) as pckg18,sum(pck19) as pckg19,sum(pck20) as pckg20,"
					+ "sum(pck21) as pckg21,sum(pck22) as pckg22,sum(pck23) as pckg23,sum(pck24) as pckg24,sum(pck25) as pckg25,"
					+ "sum(pck26) as pckg26,sum(pck27) as pckg27,sum(pck28) as pckg28,sum(pck29) as pckg29,sum(pck30) as pckg30,"
					+ "sum(pck31) as pckg31,sum(pck32) as pckg32,sum(pck33) as pckg33,sum(pck34) as pckg34,sum(pck35) as pckg35,"
					+ "sum(pck36) as pckg36,sum(pck37) as pckg37,sum(pck38) as pckg38,sum(pck39) as pckg39,sum(pck40) as pckg40,"
					+ "sum(pck41) as pckg41 " + "from (select sum( actual_quantity_afterPacking) as totalVolume,"
					+ "sum(case when wght_pckg=1400 then `quantity_after_packing` end) as pck1,\r\n"
					+ "						sum(case when wght_pckg=1300 then `quantity_after_packing` end) as pck2,\r\n"
					+ "						sum(case when wght_pckg=1200 then `quantity_after_packing` end) as pck3,\r\n"
					+ "					sum(case when wght_pckg=1100 then `quantity_after_packing` end) as pck4,\r\n"
					+ "                    sum(case when wght_pckg=1000 then `quantity_after_packing` end) as pck5 ,\r\n"
					+ "                      sum(case when wght_pckg=870 then `quantity_after_packing` end) as pck6 ,\r\n"
					+ "                       sum(case when wght_pckg=860 then `quantity_after_packing` end) as pck7 ,\r\n"
					+ "                       sum(case when wght_pckg=810 then `quantity_after_packing` end) as pck8,\r\n"
					+ "                        sum(case when wght_pckg=800 then `quantity_after_packing` end) as pck9,\r\n"
					+ "                         sum(case when wght_pckg=280 then `quantity_after_packing` end) as pck10 ,\r\n"
					+ "                         sum(case when wght_pckg=272 then `quantity_after_packing` end) as pck11 ,\r\n"
					+ "                          sum(case when wght_pckg=250 then `quantity_after_packing` end) as pck12 ,\r\n"
					+ "                           sum(case when wght_pckg=240 then `quantity_after_packing` end) as pck13 ,\r\n"
					+ "                            sum(case when wght_pckg=230 then `quantity_after_packing` end) as pck14 ,\r\n"
					+ "                             sum(case when wght_pckg=227 then `quantity_after_packing` end) as pck15 ,\r\n"
					+ "                              sum(case when wght_pckg=220 then `quantity_after_packing` end) as pck16 ,\r\n"
					+ "                                sum(case when wght_pckg=210 then `quantity_after_packing` end) as pck17 ,\r\n"
					+ "                                sum(case when wght_pckg=206 then `quantity_after_packing` end) as pck18, \r\n"
					+ "                                sum(case when wght_pckg=205 then `quantity_after_packing` end) as pck19 ,\r\n"
					+ "                                 sum(case when wght_pckg=204 then `quantity_after_packing` end) as pck20 ,\r\n"
					+ "                                  sum(case when wght_pckg=200 then `quantity_after_packing` end) as pck21 ,\r\n"
					+ "                                   sum(case when wght_pckg=190 then `quantity_after_packing` end) as pck22 ,\r\n"
					+ "                                   sum(case when wght_pckg=188 then `quantity_after_packing` end) as pck23, \r\n"
					+ "                                    sum(case when wght_pckg=185 then `quantity_after_packing` end) as pck24 ,\r\n"
					+ "                                     sum(case when wght_pckg=180 then `quantity_after_packing` end) as pck25, \r\n"
					+ "                                       sum(case when wght_pckg=175 then `quantity_after_packing` end) as pck26, \r\n"
					+ "                                        sum(case when wght_pckg=170 then `quantity_after_packing` end) as pck27, \r\n"
					+ "                                        sum(case when wght_pckg=165 then `quantity_after_packing` end) as pck28 ,\r\n"
					+ "                                          sum(case when wght_pckg=160 then `quantity_after_packing` end) as pck29 ,\r\n"
					+ "                                           sum(case when wght_pckg=150 then `quantity_after_packing` end) as pck30,\r\n"
					+ "                                            sum(case when wght_pckg=110 then `quantity_after_packing` end) as pck31 ,\r\n"
					+ "                                            sum(case when wght_pckg=100 then `quantity_after_packing` end) as pck32 ,\r\n"
					+ "                                               sum(case when wght_pckg=80 then `quantity_after_packing` end) as pck33,\r\n"
					+ "                                               sum(case when wght_pckg=70 then `quantity_after_packing` end) as pck34, \r\n"
					+ "                                                sum(case when wght_pckg=60 then `quantity_after_packing` end) as pck35,\r\n"
					+ "                                                 sum(case when wght_pckg=50 then `quantity_after_packing` end) as pck36,\r\n"
					+ "                                                  sum(case when wght_pckg=28 then `quantity_after_packing` end) as pck37,\r\n"
					+ "                                                  sum(case when wght_pckg=25 then `quantity_after_packing` end) as pck38,\r\n"
					+ "                                                    sum(case when wght_pckg=20 then `quantity_after_packing` end) as pck39,\r\n"
					+ "                                                     sum(case when wght_pckg=0.4 then `quantity_after_packing` end) as pck40 ,\r\n"
					+ "                                                     sum(case when wght_pckg=0.2 then `quantity_after_packing` end) as pck41 \r\n"
					+

					"from processorder_pckg  where status in (2,14,17,18)  "
					+ " and year(packing_completed_date) = year(current_date()) group by wght_pckg ) as t";

			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> packetTotalCounts = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {

						@Override
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse productwiseReport = new ManagementReportResponse();
							productwiseReport.setSum1(
									(rs.getFloat(1) == 0.0f ) ? 0
											: rs.getFloat(1));
							productwiseReport.setPckg1(
									(rs.getString(2) == "" || rs.getString(2) == null || rs.getString(2) == "0") ? "-"
											: rs.getString(2));
							productwiseReport.setPckg2(
									(rs.getString(3) == "" || rs.getString(3) == null || rs.getString(3) == "0") ? "-"
											: rs.getString(3));
							productwiseReport.setPckg3(
									(rs.getString(4) == "" || rs.getString(4) == null || rs.getString(4) == "0") ? "-"
											: rs.getString(4));
							productwiseReport.setPckg4(
									(rs.getString(5) == "" || rs.getString(5) == null || rs.getString(5) == "0") ? "-"
											: rs.getString(5));
							productwiseReport.setPckg5(
									(rs.getString(6) == "" || rs.getString(6) == null || rs.getString(6) == "0") ? "-"
											: rs.getString(6));
							productwiseReport.setPckg6(
									(rs.getString(7) == "" || rs.getString(7) == null || rs.getString(7) == "0") ? "-"
											: rs.getString(7));
							productwiseReport.setPckg7(
									(rs.getString(8) == "" || rs.getString(8) == null || rs.getString(8) == "0") ? "-"
											: rs.getString(8));
							productwiseReport.setPckg8(
									(rs.getString(9) == "" || rs.getString(9) == null || rs.getString(9) == "0") ? "-"
											: rs.getString(9));
							productwiseReport.setPckg9(
									(rs.getString(10) == "" || rs.getString(10) == null || rs.getString(10) == "0")
											? "-"
											: rs.getString(10));
							productwiseReport.setPckg10(
									(rs.getString(11) == "" || rs.getString(11) == null || rs.getString(11) == "0")
											? "-"
											: rs.getString(11));
							productwiseReport.setPckg11(
									(rs.getString(12) == "" || rs.getString(12) == null || rs.getString(12) == "0")
											? "-"
											: rs.getString(12));
							productwiseReport.setPckg12(
									(rs.getString(13) == "" || rs.getString(13) == null || rs.getString(13) == "0")
											? "-"
											: rs.getString(13));
							productwiseReport.setPckg13(
									(rs.getString(14) == "" || rs.getString(14) == null || rs.getString(14) == "0")
											? "-"
											: rs.getString(14));
							productwiseReport.setPckg14(
									(rs.getString(15) == "" || rs.getString(15) == null || rs.getString(15) == "0")
											? "-"
											: rs.getString(15));
							productwiseReport.setPckg15(
									(rs.getString(16) == "" || rs.getString(16) == null || rs.getString(16) == "0")
											? "-"
											: rs.getString(16));
							productwiseReport.setPckg16(
									(rs.getString(17) == "" || rs.getString(17) == null || rs.getString(17) == "0")
											? "-"
											: rs.getString(17));
							productwiseReport.setPckg17(
									(rs.getString(18) == "" || rs.getString(18) == null || rs.getString(18) == "0")
											? "-"
											: rs.getString(18));
							productwiseReport.setPckg18(
									(rs.getString(19) == "" || rs.getString(19) == null || rs.getString(19) == "0")
											? "-"
											: rs.getString(19));
							productwiseReport.setPckg19(
									(rs.getString(20) == "" || rs.getString(20) == null || rs.getString(20) == "0")
											? "-"
											: rs.getString(20));
							productwiseReport.setPckg20(
									(rs.getString(21) == "" || rs.getString(21) == null || rs.getString(21) == "0")
											? "-"
											: rs.getString(21));
							productwiseReport.setPckg21(
									(rs.getString(22) == "" || rs.getString(22) == null || rs.getString(22) == "0")
											? "-"
											: rs.getString(22));
							productwiseReport.setPckg22(
									(rs.getString(23) == "" || rs.getString(23) == null || rs.getString(23) == "0")
											? "-"
											: rs.getString(23));
							productwiseReport.setPckg23(
									(rs.getString(24) == "" || rs.getString(24) == null || rs.getString(24) == "0")
											? "-"
											: rs.getString(24));
							productwiseReport.setPckg24(
									(rs.getString(25) == "" || rs.getString(25) == null || rs.getString(25) == "0")
											? "-"
											: rs.getString(25));
							productwiseReport.setPckg25(
									(rs.getString(26) == "" || rs.getString(26) == null || rs.getString(26) == "0")
											? "-"
											: rs.getString(26));
							productwiseReport.setPckg26(
									(rs.getString(27) == "" || rs.getString(27) == null || rs.getString(27) == "0")
											? "-"
											: rs.getString(27));
							productwiseReport.setPckg27(
									(rs.getString(28) == "" || rs.getString(28) == null || rs.getString(28) == "0")
											? "-"
											: rs.getString(28));
							productwiseReport.setPckg28(
									(rs.getString(29) == "" || rs.getString(29) == null || rs.getString(29) == "0")
											? "-"
											: rs.getString(29));
							productwiseReport.setPckg29(
									(rs.getString(30) == "" || rs.getString(30) == null || rs.getString(30) == "0")
											? "-"
											: rs.getString(30));
							productwiseReport.setPckg30(
									(rs.getString(31) == "" || rs.getString(31) == null || rs.getString(31) == "0")
											? "-"
											: rs.getString(31));
							productwiseReport.setPckg31(
									(rs.getString(32) == "" || rs.getString(32) == null || rs.getString(32) == "0")
											? "-"
											: rs.getString(32));
							productwiseReport.setPckg32(
									(rs.getString(33) == "" || rs.getString(33) == null || rs.getString(33) == "0")
											? "-"
											: rs.getString(33));
							productwiseReport.setPckg33(
									(rs.getString(34) == "" || rs.getString(34) == null || rs.getString(34) == "0")
											? "-"
											: rs.getString(34));
							productwiseReport.setPckg34(
									(rs.getString(35) == "" || rs.getString(35) == null || rs.getString(35) == "0")
											? "-"
											: rs.getString(35));
							productwiseReport.setPckg35(
									(rs.getString(36) == "" || rs.getString(36) == null || rs.getString(36) == "0")
											? "-"
											: rs.getString(36));
							productwiseReport.setPckg36(
									(rs.getString(37) == "" || rs.getString(37) == null || rs.getString(37) == "0")
											? "-"
											: rs.getString(37));
							productwiseReport.setPckg37(
									(rs.getString(38) == "" || rs.getString(38) == null || rs.getString(38) == "0")
											? "-"
											: rs.getString(38));
							productwiseReport.setPckg38(
									(rs.getString(39) == "" || rs.getString(39) == null || rs.getString(39) == "0")
											? "-"
											: rs.getString(39));
							productwiseReport.setPckg39(
									(rs.getString(40) == "" || rs.getString(40) == null || rs.getString(40) == "0")
											? "-"
											: rs.getString(40));
							productwiseReport.setPckg40(
									(rs.getString(41) == "" || rs.getString(41) == null || rs.getString(41) == "0")
											? "-"
											: rs.getString(41));
							productwiseReport.setPckg41(
									(rs.getString(42) == "" || rs.getString(42) == null || rs.getString(42) == "0")
											? "-"
											: rs.getString(42));
							return productwiseReport;

						}
					});
			return packetTotalCounts;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	/*
	 * @Override public List<ManagementReportResponse> dateWiseProductReport(Long
	 * productnumber) { try { //DateTimeFormatter formatter =
	 * DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	 * 
	 * String query = "select pr.processorder_prod_number,p.product_number," +
	 * " p.product_description,round(sum(pr.total_quantity)/1000,2)," +
	 * " pr.batch_date,pr.reactor,pr.completed_date," +
	 * " DATE_FORMAT(date('1970-12-31 23:59:59') + interval time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time)) second,'%edays %Hh:%im:%ss')  as leadtime  "
	 * + " from processorder_prod pr inner join product_table p " +
	 * "  on  p.product_number=pr.product_number " +
	 * " where p.product_number="+productnumber + "" +
	 * " and  YEAR(pr.completed_date)=  YEAR(CURDATE()) and pr.status=2 " +
	 * " group by processorder_prod_number " +
	 * " order by batch_date,processorder_prod_number desc  ";
	 * 
	 * List<ManagementReportResponse> datewiseProductReport =
	 * getNamedParameterJdbcTemplate().query(query, new
	 * ResultSetExtractor<List<ManagementReportResponse>>() { public
	 * List<ManagementReportResponse> extractData(ResultSet rs) throws SQLException,
	 * DataAccessException {
	 * 
	 * List<ManagementReportResponse> list = new
	 * ArrayList<ManagementReportResponse>();
	 * 
	 * while (rs.next()) { ManagementReportResponse processorderprod = new
	 * ManagementReportResponse();
	 * processorderprod.setProcessOrderProdNumber(rs.getLong(1));
	 * processorderprod.setProductnumber(rs.getLong(2));
	 * processorderprod.setProductDescription(rs.getString(3));
	 * processorderprod.setSum1(rs.getString(4));
	 * processorderprod.setBatchDate(DateFormator(rs.getString(5)));
	 * processorderprod.setReactor(rs.getString(6));
	 * processorderprod.setCompletedDate(DateFormator(rs.getString(7)));
	 * processorderprod.setLeadTime(rs.getString(8)); list.add(processorderprod); }
	 * return list; } }); return datewiseProductReport; } catch (Exception e) {
	 * e.printStackTrace(); return null;
	 * 
	 * }
	 * 
	 * }
	 * 
	 * @Override public List<ManagementReportResponse> getAll1MTReactors(Long
	 * productnumber) { try { String query =
	 * "select pr.processorder_prod_number,p.product_number,p.product_description,round(sum(pr.total_quantity)/1000,2) ,\r\n"
	 * +
	 * " pr.batch_date,pr.reactor,pr.completed_date,DATE_FORMAT(date('1970-12-31 23:59:59') + interval time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time)) second,'%edays %Hh:%im:%ss')  as leadtime  "
	 * + " from processorder_prod pr inner join product_table p \r\n" +
	 * "					on  p.product_number=pr.product_number\r\n" +
	 * "					where p.product_number=" + productnumber + " \r\n" +
	 * "            and  YEAR(pr.completed_date)=  YEAR(CURDATE()) and pr.status=2 and pr.reactor like'1%' \r\n"
	 * + " group by processorder_prod_number " +
	 * " order by batch_date,processorder_prod_number desc ";
	 * 
	 * List<ManagementReportResponse> datewiseProductReport =
	 * getNamedParameterJdbcTemplate().query(query, new
	 * ResultSetExtractor<List<ManagementReportResponse>>() { public
	 * List<ManagementReportResponse> extractData(ResultSet rs) throws SQLException,
	 * DataAccessException {
	 * 
	 * List<ManagementReportResponse> list = new
	 * ArrayList<ManagementReportResponse>();
	 * 
	 * while (rs.next()) { ManagementReportResponse processorderprod = new
	 * ManagementReportResponse();
	 * processorderprod.setProcessOrderProdNumber(rs.getLong(1));
	 * processorderprod.setProductnumber(rs.getLong(2));
	 * processorderprod.setProductDescription(rs.getString(3));
	 * processorderprod.setSum1(rs.getString(4));
	 * processorderprod.setBatchDate(DateFormator(rs.getString(5)));
	 * processorderprod.setReactor(rs.getString(6));
	 * processorderprod.setCompletedDate(DateFormator(rs.getString(7)));
	 * processorderprod.setLeadTime(rs.getString(8)); list.add(processorderprod); }
	 * return list; } }); return datewiseProductReport; } catch (Exception e) {
	 * e.printStackTrace(); return null;
	 * 
	 * } }
	 * 
	 * @Override public List<ManagementReportResponse> getAll5MTReactors(Long
	 * productnumber) { try { String query =
	 * "select pr.processorder_prod_number,p.product_number,p.product_description,round(sum(pr.total_quantity)/1000,2) ,\r\n"
	 * + " pr.batch_date,pr.reactor,pr.completed_date," +
	 * " DATE_FORMAT(date('1970-12-31 23:59:59') + interval time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time)) second,'%edays %Hh:%im:%ss') as leadtime "
	 * + " from processorder_prod pr inner join product_table p \r\n" +
	 * "					on  p.product_number=pr.product_number\r\n" +
	 * "					where p.product_number=" + productnumber + " \r\n" +
	 * "            and  YEAR(pr.completed_date)=  YEAR(CURDATE()) and pr.status=2 and pr.reactor like '5%' \r\n"
	 * + " group by processorder_prod_number " +
	 * " order by batch_date,processorder_prod_number desc ";
	 * 
	 * List<ManagementReportResponse> datewiseProductReport =
	 * getNamedParameterJdbcTemplate().query(query, new
	 * ResultSetExtractor<List<ManagementReportResponse>>() { public
	 * List<ManagementReportResponse> extractData(ResultSet rs) throws SQLException,
	 * DataAccessException {
	 * 
	 * List<ManagementReportResponse> list = new
	 * ArrayList<ManagementReportResponse>();
	 * 
	 * while (rs.next()) { ManagementReportResponse processorderprod = new
	 * ManagementReportResponse();
	 * processorderprod.setProcessOrderProdNumber(rs.getLong(1));
	 * processorderprod.setProductnumber(rs.getLong(2));
	 * processorderprod.setProductDescription(rs.getString(3));
	 * processorderprod.setSum1(rs.getString(4));
	 * processorderprod.setBatchDate(DateFormator(rs.getString(5)));
	 * processorderprod.setReactor(rs.getString(6));
	 * processorderprod.setCompletedDate(DateFormator(rs.getString(7)));
	 * processorderprod.setLeadTime(rs.getString(8)); list.add(processorderprod); }
	 * return list; } }); return datewiseProductReport; } catch (Exception e) {
	 * e.printStackTrace(); return null;
	 * 
	 * } }
	 */

	@Override
	public List<ManagementReportResponse> dateWiseProductReport(Long productnumber) {
		try {
			// DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd
			// HH:mm:ss");

			String query = "select pr.processorder_prod_number,p.product_number,"
					+ " p.product_description,round(sum(pr.actual_quantity_afterPacking)/1000,2),"
					+ " DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packed_date,'%d-%m-%Y'),"
					+ " CONCAT(round(time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time))/60 ,1),'min')  as leadtime  "
					+ " from processorder_prod pr inner join product_table p "
					+ "  on  p.product_number=pr.product_number " + " where p.product_number=" + productnumber + ""
					+ " and  YEAR(pr.packed_date)=  YEAR(CURDATE()) and pr.status in (2,14,18,17) "
					+ " group by processorder_prod_number " + " order by batch_date,processorder_prod_number desc  ";

			List<ManagementReportResponse> datewiseProductReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});

			query = "select pr.processorder_pckg_number,pr.product_number,\r\n"
					+ "						pr.material_memo,round(sum(pr.actual_quantity_afterPacking)/1000,2),\r\n"
					+ "					 DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packing_completed_date,'%d-%m-%Y'),\r\n"
					+ "						 CONCAT(round(time_to_sec(TIMEDIFF( pr.packing_end_time, pr.packing_start_time))/60 ,1),'min')  as leadtime  \r\n"
					+ "						 from processorder_pckg pr \r\n"
					+ "						 where pr.product_number=" + productnumber + "  and is_repack=1\r\n"
					+ "						 and  YEAR(pr.packing_completed_date)=  YEAR(CURDATE()) and pr.status in (2,14,17,18) \r\n"
					+ "						 group by processorder_pckg_number \r\n"
					+ "						order by batch_date,processorder_pckg_number desc  ";

			List<ManagementReportResponse> datewiseProductReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});

			datewiseProductReport.addAll(datewiseProductReport1);
			return datewiseProductReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	@Override
	public List<ManagementReportResponse> getAll1MTReactors(Long productnumber) {
		try {
			String query = "select pr.processorder_prod_number,p.product_number,p.product_description,round(sum(pr.actual_quantity_afterPacking)/1000,2) ,\r\n"
					+ " DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packed_date,'%d-%m-%Y'),CONCAT(round(time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time))/60 ,1),'min')  as leadtime  "
					+ " from processorder_prod pr inner join product_table p \r\n"
					+ "					on  p.product_number=pr.product_number\r\n"
					+ "					where p.product_number=" + productnumber + " \r\n"
					+ "            and  YEAR(pr.packed_date)=  YEAR(CURDATE()) and pr.status in (2,14,17,18) and pr.reactor like'1%' \r\n"
					+ " group by processorder_prod_number " + " order by batch_date,processorder_prod_number desc ";

			List<ManagementReportResponse> datewiseProductReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			query = "select pr.processorder_pckg_number,pr.product_number,\r\n"
					+ "						pr.material_memo,round(sum(pr.actual_quantity_afterPacking)/1000,2),\r\n"
					+ "					 DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packing_completed_date,'%d-%m-%Y'),\r\n"
					+ "						 CONCAT(round(time_to_sec(TIMEDIFF( pr.packing_end_time, pr.packing_start_time))/60 ,1),'min')  as leadtime  \r\n"
					+ "						 from processorder_pckg pr \r\n"
					+ "						 where pr.product_number=" + productnumber + "  and is_repack=1\r\n"
					+ "						 and  YEAR(pr.packing_completed_date)=  YEAR(CURDATE()) and pr.status in (2,14,17,18) and pr.reactor like'1%' \r\n"
					+ "						 group by processorder_pckg_number \r\n"
					+ "						order by batch_date,processorder_pckg_number desc  ";

			List<ManagementReportResponse> datewiseProductReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			datewiseProductReport.addAll(datewiseProductReport1);

			return datewiseProductReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	@Override
	public List<ManagementReportResponse> getAll5MTReactors(Long productnumber) {
		try {
			String query = "select pr.processorder_prod_number,p.product_number,p.product_description,round(sum(pr.actual_quantity_afterPacking)/1000,2) ,\r\n"
					+ " DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packed_date,'%d-%m-%Y'),"
					+ " CONCAT(round(time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time))/60 ,1),'min') as leadtime "
					+ " from processorder_prod pr inner join product_table p \r\n"
					+ "					on  p.product_number=pr.product_number\r\n"
					+ "					where p.product_number=" + productnumber + " \r\n"
					+ "            and  YEAR(pr.packed_date)=  YEAR(CURDATE()) and pr.status in(2,14,17,18) and pr.reactor like '5%' \r\n"
					+ " group by processorder_prod_number " + " order by batch_date,processorder_prod_number desc ";

			List<ManagementReportResponse> datewiseProductReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});

			query = "select pr.processorder_pckg_number,pr.product_number,\r\n"
					+ "						pr.material_memo,round(sum(pr.actual_quantity_afterPacking)/1000,2),\r\n"
					+ "					 DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packing_completed_date,'%d-%m-%Y'),\r\n"
					+ "						 CONCAT(round(time_to_sec(TIMEDIFF( pr.packing_end_time, pr.packing_start_time))/60 ,1),'min')  as leadtime  \r\n"
					+ "						 from processorder_pckg pr \r\n"
					+ "						 where pr.product_number=" + productnumber + "  and is_repack=1\r\n"
					+ "						 and  YEAR(pr.packing_completed_date)=  YEAR(CURDATE()) and pr.status in (2,14,17,18) and pr.reactor like'5%' \r\n"
					+ "						 group by processorder_pckg_number \r\n"
					+ "						order by batch_date,processorder_pckg_number desc  ";

			List<ManagementReportResponse> datewiseProductReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			datewiseProductReport.addAll(datewiseProductReport1);

			return datewiseProductReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	// ----------------------------------------------------------------------------------------------------
	// Multiple year product wise report by selecting year

	@Override
	public List<ManagementReportResponse> productWisereport(int year) {
		try {
			/*
			 * String query =
			 * "select pr.processorder_prod_number,p.product_number, p.product_description, pr.batch_date, "
			 * +"  sum(pr.total_quantity) as quantity,pr.reactor,pr.completed_date from processorder_prod pr "
			 * +" inner join product_table p on  p.product_number=pr.product_number "
			 * +" where  YEAR(pr.completed_date)= "+year+" and pr.status=2 "
			 * +" group by  p.product_description";
			 */

			/*
			 * String
			 * query="select product_name, product_num,sum(quantity)/1000 as Quantity,sum(1000kg) as p1,sum(200kg) as p2,"
			 * + "sum(100kg) as p3 ,sum(50kg) as p4,sum(25kg) p5 from "
			 * +"  (select pd.product_description as product_name ,pd.product_number as product_num, "
			 * +" sum((prodPr.total_quantity)*(prodPr.wght_pckg)) as Quantity,"
			 * +" (case when wght_pckg=1000 then `total_quantity` end) as 1000kg,"
			 * +"(case when wght_pckg=100 then `total_quantity` end) as 100kg,"
			 * +"(case when wght_pckg=50 then `total_quantity` end) as 50kg,"
			 * +" (case when wght_pckg=25 then `total_quantity` end) as 25kg,"
			 * +"(case when wght_pckg=200 then `total_quantity` end) as 200kg "
			 * +" from processorder_pckg as prodPr inner join product_table as pd on prodPr.product_number=pd.product_number "
			 * +" where status=2 and is_repack=0 and year(completed_date)="+year+" "
			 * +" group by pd.product_description ,prodPr.wght_pckg,total_quantity ) as t group by product_name"
			 * ;
			 */

			String query = "select product_name, product_num,round(sum(Quantity)/1000,2) as Quantity,"
					+ "sum(1400kg) as p1, sum(1300kg) as p2,  sum(1200kg) as p3, sum(1100kg) as p4, sum(1000kg) as p5,"
					+ " sum(870kg) as p6,  sum(860kg) as p7,   sum(810kg) as p8,  sum(800kg) as p9, sum(280kg) as p10,"
					+ " sum(272kg) as p11, sum(250kg) as p12,  sum(240kg) as p13, sum(230kg) as p14, sum(227kg) as p15,"
					+ " sum(220kg) as p16,  sum(210kg) as p17,   sum(206kg) as p18,  sum(205kg) as p19,  sum(204kg) as p20,"
					+ " sum(200kg) as p21,  sum(190kg) as p22,   sum(188kg) as p23,   sum(185kg) as p24,  sum(180kg) as p25,"
					+ " sum(175kg) as p26,   sum(170kg) as p27,   sum(165kg) as p28,   sum(160kg) as p29,  sum(150kg) as p30,"
					+ " sum(110kg) as p31,   sum(100kg) as p32,   sum(80kg) as p33,  sum(70kg) as p34,  sum(60kg) as p35,"
					+ " sum(50kg) as p36, sum(28kg) as p37, sum(25kg) as p38, sum(20kg) as p39, sum(04kg) as p40, sum(02kg) as p41"
					+ " from " + " (select material_memo as product_name ,product_number as product_num,"
					+ "   sum(actual_quantity_afterPacking) as Quantity,"
					+ " (case when wght_pckg=1400 then sum(quantity_after_packing) end) as 1400kg,"
					+ "(case when wght_pckg=1300 then sum(quantity_after_packing) end) as 1300kg,"
					+ "  (case when wght_pckg=1200 then sum(quantity_after_packing) end) as 1200kg,"
					+ "	(case when wght_pckg=1100 then sum(quantity_after_packing) end) as 1100kg,"
					+ "  (case when wght_pckg=1000 then sum(quantity_after_packing) end) as 1000kg ,"
					+ "  (case when wght_pckg=870 then sum(quantity_after_packing) end) as 870kg ,"
					+ " (case when wght_pckg=860 then sum(quantity_after_packing) end) as 860kg ,"
					+ "  (case when wght_pckg=810 then sum(quantity_after_packing) end) as 810kg,"
					+ "   (case when wght_pckg=800 then sum(quantity_after_packing) end) as 800kg,"
					+ " (case when wght_pckg=280 then sum(quantity_after_packing) end) as 280kg ,"
					+ " (case when wght_pckg=272 then sum(quantity_after_packing) end) as 272kg ,"
					+ " (case when wght_pckg=250 then sum(quantity_after_packing) end) as 250kg ,"
					+ " (case when wght_pckg=240 then sum(quantity_after_packing) end) as 240kg , "
					+ "(case when wght_pckg=230 then sum(quantity_after_packing) end) as 230kg ,"
					+ " (case when wght_pckg=227 then sum(quantity_after_packing) end) as 227kg ,"
					+ " (case when wght_pckg=220 then sum(quantity_after_packing) end) as 220kg ,"
					+ " (case when wght_pckg=210 then sum(quantity_after_packing) end) as 210kg ,"
					+ "  (case when wght_pckg=206 then sum(quantity_after_packing) end) as 206kg, "
					+ " (case when wght_pckg=205 then sum(quantity_after_packing) end) as 205kg ,"
					+ " (case when wght_pckg=204 then sum(quantity_after_packing) end) as 204kg ,"
					+ " (case when wght_pckg=200 then sum(quantity_after_packing) end) as 200kg ,"
					+ " (case when wght_pckg=190 then sum(quantity_after_packing) end) as 190kg ,"
					+ "  (case when wght_pckg=188 then sum(quantity_after_packing) end) as 188kg, "
					+ " (case when wght_pckg=185 then sum(quantity_after_packing) end) as 185kg ,"
					+ " (case when wght_pckg=180 then sum(quantity_after_packing) end) as 180kg, "
					+ " (case when wght_pckg=175 then sum(quantity_after_packing) end) as 175kg, "
					+ " (case when wght_pckg=170 then sum(quantity_after_packing) end) as 170kg, "
					+ " (case when wght_pckg=165 then sum(quantity_after_packing) end) as 165kg ,"
					+ "  (case when wght_pckg=160 then sum(quantity_after_packing) end) as 160kg ,"
					+ " (case when wght_pckg=150 then sum(quantity_after_packing) end) as 150kg,"
					+ " (case when wght_pckg=110 then sum(quantity_after_packing) end) as 110kg ,"
					+ "  (case when wght_pckg=100 then sum(quantity_after_packing) end) as 100kg ,"
					+ " (case when wght_pckg=80 then sum(quantity_after_packing) end) as 80kg ,"
					+ " (case when wght_pckg=70 then sum(quantity_after_packing) end) as 70kg, "
					+ " (case when wght_pckg=60 then sum(quantity_after_packing) end) as 60kg,    "
					+ " (case when wght_pckg=50 then sum(quantity_after_packing) end) as 50kg,"
					+ " (case when wght_pckg=28 then sum(quantity_after_packing) end) as 28kg,"
					+ " (case when wght_pckg=25 then sum(quantity_after_packing) end) as 25kg,"
					+ "  (case when wght_pckg=20 then sum(quantity_after_packing) end) as 20kg,"
					+ " (case when wght_pckg=0.4 then sum(quantity_after_packing) end) as 04kg ,"
					+ " (case when wght_pckg=0.2 then sum(quantity_after_packing) end) as 02kg "
					+ " from processorder_pckg " + " where status in( 2,14,17,18) and year(packing_completed_date)="
					+ year + " "
					+ " group by product_number,wght_pckg,quantity_after_packing ) as t group by product_name ";

			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> productwiseReports = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {

						@Override
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse productwiseReport = new ManagementReportResponse();

							productwiseReport.setProductDescription(
									(rs.getString(1) == "" || rs.getString(1) == null || rs.getString(1) == "0") ? "-"
											: rs.getString(1));
							productwiseReport.setProductnumber(rs.getLong(2));
							productwiseReport.setSum1(
									(rs.getFloat(3) == 0.0f ) ? 0
											: rs.getFloat(3));

							productwiseReport.setPckg1(
									(rs.getString(4) == "" || rs.getString(4) == null || rs.getString(4) == "0") ? "-"
											: rs.getString(4));
							productwiseReport.setPckg2(
									(rs.getString(5) == "" || rs.getString(5) == null || rs.getString(5) == "0") ? "-"
											: rs.getString(5));
							productwiseReport.setPckg3(
									(rs.getString(6) == "" || rs.getString(6) == null || rs.getString(6) == "0") ? "-"
											: rs.getString(6));
							productwiseReport.setPckg4(
									(rs.getString(7) == "" || rs.getString(7) == null || rs.getString(7) == "0") ? "-"
											: rs.getString(7));
							productwiseReport.setPckg5(
									(rs.getString(8) == "" || rs.getString(8) == null || rs.getString(8) == "0") ? "-"
											: rs.getString(8));
							productwiseReport.setPckg6(
									(rs.getString(9) == "" || rs.getString(9) == null || rs.getString(9) == "0") ? "-"
											: rs.getString(9));
							productwiseReport.setPckg7(
									(rs.getString(10) == "" || rs.getString(10) == null || rs.getString(10) == "0")
											? "-"
											: rs.getString(10));
							productwiseReport.setPckg8(
									(rs.getString(11) == "" || rs.getString(11) == null || rs.getString(11) == "0")
											? "-"
											: rs.getString(11));
							productwiseReport.setPckg9(
									(rs.getString(12) == "" || rs.getString(12) == null || rs.getString(12) == "0")
											? "-"
											: rs.getString(12));
							productwiseReport.setPckg10(
									(rs.getString(13) == "" || rs.getString(13) == null || rs.getString(13) == "0")
											? "-"
											: rs.getString(13));
							productwiseReport.setPckg11(
									(rs.getString(14) == "" || rs.getString(14) == null || rs.getString(14) == "0")
											? "-"
											: rs.getString(14));
							productwiseReport.setPckg12(
									(rs.getString(15) == "" || rs.getString(15) == null || rs.getString(15) == "0")
											? "-"
											: rs.getString(15));
							productwiseReport.setPckg13(
									(rs.getString(16) == "" || rs.getString(16) == null || rs.getString(16) == "0")
											? "-"
											: rs.getString(16));
							productwiseReport.setPckg14(
									(rs.getString(17) == "" || rs.getString(17) == null || rs.getString(17) == "0")
											? "-"
											: rs.getString(17));
							productwiseReport.setPckg15(
									(rs.getString(18) == "" || rs.getString(18) == null || rs.getString(18) == "0")
											? "-"
											: rs.getString(18));
							productwiseReport.setPckg16(
									(rs.getString(19) == "" || rs.getString(19) == null || rs.getString(19) == "0")
											? "-"
											: rs.getString(19));
							productwiseReport.setPckg17(
									(rs.getString(20) == "" || rs.getString(20) == null || rs.getString(20) == "0")
											? "-"
											: rs.getString(20));
							productwiseReport.setPckg18(
									(rs.getString(21) == "" || rs.getString(21) == null || rs.getString(21) == "0")
											? "-"
											: rs.getString(21));
							productwiseReport.setPckg19(
									(rs.getString(22) == "" || rs.getString(22) == null || rs.getString(22) == "0")
											? "-"
											: rs.getString(22));
							productwiseReport.setPckg20(
									(rs.getString(23) == "" || rs.getString(23) == null || rs.getString(23) == "0")
											? "-"
											: rs.getString(23));
							productwiseReport.setPckg21(
									(rs.getString(24) == "" || rs.getString(24) == null || rs.getString(24) == "0")
											? "-"
											: rs.getString(24));
							productwiseReport.setPckg22(
									(rs.getString(25) == "" || rs.getString(25) == null || rs.getString(25) == "0")
											? "-"
											: rs.getString(25));
							productwiseReport.setPckg23(
									(rs.getString(26) == "" || rs.getString(26) == null || rs.getString(26) == "0")
											? "-"
											: rs.getString(26));
							productwiseReport.setPckg24(
									(rs.getString(27) == "" || rs.getString(27) == null || rs.getString(27) == "0")
											? "-"
											: rs.getString(27));
							productwiseReport.setPckg25(
									(rs.getString(28) == "" || rs.getString(28) == null || rs.getString(28) == "0")
											? "-"
											: rs.getString(28));
							productwiseReport.setPckg26(
									(rs.getString(29) == "" || rs.getString(29) == null || rs.getString(29) == "0")
											? "-"
											: rs.getString(29));
							productwiseReport.setPckg27(
									(rs.getString(30) == "" || rs.getString(30) == null || rs.getString(30) == "0")
											? "-"
											: rs.getString(30));
							productwiseReport.setPckg28(
									(rs.getString(31) == "" || rs.getString(31) == null || rs.getString(31) == "0")
											? "-"
											: rs.getString(31));
							productwiseReport.setPckg29(
									(rs.getString(32) == "" || rs.getString(32) == null || rs.getString(32) == "0")
											? "-"
											: rs.getString(32));
							productwiseReport.setPckg30(
									(rs.getString(33) == "" || rs.getString(33) == null || rs.getString(33) == "0")
											? "-"
											: rs.getString(33));
							productwiseReport.setPckg31(
									(rs.getString(34) == "" || rs.getString(34) == null || rs.getString(34) == "0")
											? "-"
											: rs.getString(34));
							productwiseReport.setPckg32(
									(rs.getString(35) == "" || rs.getString(35) == null || rs.getString(35) == "0")
											? "-"
											: rs.getString(35));
							productwiseReport.setPckg33(
									(rs.getString(36) == "" || rs.getString(36) == null || rs.getString(36) == "0")
											? "-"
											: rs.getString(36));
							productwiseReport.setPckg34(
									(rs.getString(37) == "" || rs.getString(37) == null || rs.getString(37) == "0")
											? "-"
											: rs.getString(37));
							productwiseReport.setPckg35(
									(rs.getString(38) == "" || rs.getString(38) == null || rs.getString(38) == "0")
											? "-"
											: rs.getString(38));
							productwiseReport.setPckg36(
									(rs.getString(39) == "" || rs.getString(39) == null || rs.getString(39) == "0")
											? "-"
											: rs.getString(39));
							productwiseReport.setPckg37(
									(rs.getString(40) == "" || rs.getString(40) == null || rs.getString(40) == "0")
											? "-"
											: rs.getString(40));
							productwiseReport.setPckg38(
									(rs.getString(41) == "" || rs.getString(41) == null || rs.getString(41) == "0")
											? "-"
											: rs.getString(41));
							productwiseReport.setPckg39(
									(rs.getString(42) == "" || rs.getString(42) == null || rs.getString(42) == "0")
											? "-"
											: rs.getString(42));
							productwiseReport.setPckg40(
									(rs.getString(43) == "" || rs.getString(43) == null || rs.getString(43) == "0")
											? "-"
											: rs.getString(43));
							productwiseReport.setPckg41(
									(rs.getString(44) == "" || rs.getString(44) == null || rs.getString(44) == "0")
											? "-"
											: rs.getString(44));

							return productwiseReport;

						}
					});
			return productwiseReports;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	public List<ManagementReportResponse> getPacketSizeCountsForYear(int year) {
		try {
			/*
			 * String query =
			 * "select  sum(totalVolume)/1000 as TotalVolume,sum(pck1) as pckg1,sum(pck2) as pckg2,sum(pck3) as pckg3,"
			 * +" sum(pck4) as pckg4,sum(pck5) as pckg5 from (select sum( (wght_pckg)*(total_quantity)) as totalVolume,"
			 * +"sum(case when wght_pckg='1000' then `total_quantity` end) as pck1,"
			 * +"sum(case when wght_pckg='200' then `total_quantity` end) as pck2,"
			 * +"sum(case when wght_pckg='100' then `total_quantity` end) as pck3, "
			 * +"sum(case when wght_pckg='50' then `total_quantity` end) as pck4, "
			 * +"sum(case when wght_pckg='25' then `total_quantity` end) as pck5 "
			 * +"from processorder_pckg  where status = 2 and is_repack=0 "
			 * +" and year(completed_date) = "+year+" group by wght_pckg ) as t";
			 */
			String query = "select  round(sum(totalVolume)/1000,2) as TotalVolume,"
					+ "sum(pck1) as pckg1,sum(pck2) as pckg2,sum(pck3) as pckg3,sum(pck4) as pckg4,sum(pck5) as pckg5 ,"
					+ " sum(pck6) as pckg6,sum(pck7) as pckg7,sum(pck8) as pckg8,sum(pck9) as pckg9,sum(pck10) as pckg10,"
					+ "sum(pck11) as pckg11,sum(pck12) as pckg12,sum(pck13) as pckg13,sum(pck14) as pckg14,sum(pck15) as pckg15,"
					+ "sum(pck16) as pckg16,sum(pck17) as pckg17,sum(pck18) as pckg18,sum(pck19) as pckg19,sum(pck20) as pckg20,"
					+ "sum(pck21) as pckg21,sum(pck22) as pckg22,sum(pck23) as pckg23,sum(pck24) as pckg24,sum(pck25) as pckg25,"
					+ "sum(pck26) as pckg26,sum(pck27) as pckg27,sum(pck28) as pckg28,sum(pck29) as pckg29,sum(pck30) as pckg30,"
					+ "sum(pck31) as pckg31,sum(pck32) as pckg32,sum(pck33) as pckg33,sum(pck34) as pckg34,sum(pck35) as pckg35,"
					+ "sum(pck36) as pckg36,sum(pck37) as pckg37,sum(pck38) as pckg38,sum(pck39) as pckg39,sum(pck40) as pckg40,"
					+ "sum(pck41) as pckg41 " + "from (select sum( actual_quantity_afterPacking) as totalVolume,"
					+ "sum(case when wght_pckg=1400 then `quantity_after_packing` end) as pck1,\r\n"
					+ "						sum(case when wght_pckg=1300 then `quantity_after_packing` end) as pck2,\r\n"
					+ "						sum(case when wght_pckg=1200 then `quantity_after_packing` end) as pck3,\r\n"
					+ "					sum(case when wght_pckg=1100 then `quantity_after_packing` end) as pck4,\r\n"
					+ "                    sum(case when wght_pckg=1000 then `quantity_after_packing` end) as pck5 ,\r\n"
					+ "                      sum(case when wght_pckg=870 then `quantity_after_packing` end) as pck6 ,\r\n"
					+ "                       sum(case when wght_pckg=860 then `quantity_after_packing` end) as pck7 ,\r\n"
					+ "                       sum(case when wght_pckg=810 then `quantity_after_packing` end) as pck8,\r\n"
					+ "                        sum(case when wght_pckg=800 then `quantity_after_packing` end) as pck9,\r\n"
					+ "                         sum(case when wght_pckg=280 then `quantity_after_packing` end) as pck10 ,\r\n"
					+ "                         sum(case when wght_pckg=272 then `quantity_after_packing` end) as pck11 ,\r\n"
					+ "                          sum(case when wght_pckg=250 then `quantity_after_packing` end) as pck12 ,\r\n"
					+ "                           sum(case when wght_pckg=240 then `quantity_after_packing` end) as pck13 ,\r\n"
					+ "                            sum(case when wght_pckg=230 then `quantity_after_packing` end) as pck14 ,\r\n"
					+ "                             sum(case when wght_pckg=227 then `quantity_after_packing` end) as pck15 ,\r\n"
					+ "                              sum(case when wght_pckg=220 then `quantity_after_packing` end) as pck16 ,\r\n"
					+ "                                sum(case when wght_pckg=210 then `quantity_after_packing` end) as pck17 ,\r\n"
					+ "                                sum(case when wght_pckg=206 then `quantity_after_packing` end) as pck18, \r\n"
					+ "                                sum(case when wght_pckg=205 then `quantity_after_packing` end) as pck19 ,\r\n"
					+ "                                 sum(case when wght_pckg=204 then `quantity_after_packing` end) as pck20 ,\r\n"
					+ "                                  sum(case when wght_pckg=200 then `quantity_after_packing` end) as pck21 ,\r\n"
					+ "                                   sum(case when wght_pckg=190 then `quantity_after_packing` end) as pck22 ,\r\n"
					+ "                                   sum(case when wght_pckg=188 then `quantity_after_packing` end) as pck23, \r\n"
					+ "                                    sum(case when wght_pckg=185 then `quantity_after_packing` end) as pck24 ,\r\n"
					+ "                                     sum(case when wght_pckg=180 then `quantity_after_packing` end) as pck25, \r\n"
					+ "                                       sum(case when wght_pckg=175 then `quantity_after_packing` end) as pck26, \r\n"
					+ "                                        sum(case when wght_pckg=170 then `quantity_after_packing` end) as pck27, \r\n"
					+ "                                        sum(case when wght_pckg=165 then `quantity_after_packing` end) as pck28 ,\r\n"
					+ "                                          sum(case when wght_pckg=160 then `quantity_after_packing` end) as pck29 ,\r\n"
					+ "                                           sum(case when wght_pckg=150 then `quantity_after_packing` end) as pck30,\r\n"
					+ "                                            sum(case when wght_pckg=110 then `quantity_after_packing` end) as pck31 ,\r\n"
					+ "                                            sum(case when wght_pckg=100 then `quantity_after_packing` end) as pck32 ,\r\n"
					+ "                                               sum(case when wght_pckg=80 then `quantity_after_packing` end) as pck33,\r\n"
					+ "                                               sum(case when wght_pckg=70 then `quantity_after_packing` end) as pck34, \r\n"
					+ "                                                sum(case when wght_pckg=60 then `quantity_after_packing` end) as pck35,\r\n"
					+ "                                                 sum(case when wght_pckg=50 then `quantity_after_packing` end) as pck36,\r\n"
					+ "                                                  sum(case when wght_pckg=28 then `quantity_after_packing` end) as pck37,\r\n"
					+ "                                                  sum(case when wght_pckg=25 then `quantity_after_packing` end) as pck38,\r\n"
					+ "                                                    sum(case when wght_pckg=20 then `quantity_after_packing` end) as pck39,\r\n"
					+ "                                                     sum(case when wght_pckg=0.4 then `quantity_after_packing` end) as pck40 ,\r\n"
					+ "                                                     sum(case when wght_pckg=0.2 then `quantity_after_packing` end) as pck41 \r\n"
					+

					"from processorder_pckg  where status in (2,14,17,18)  " + " and year(packing_completed_date) ="
					+ year + " group by wght_pckg ) as t";

			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> packetTotalCounts = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {

						@Override
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse productwiseReport = new ManagementReportResponse();
							productwiseReport.setSum1(
									(rs.getFloat(1) ==0.0f ) ? 0
											: rs.getFloat(1));
							productwiseReport.setPckg1(
									(rs.getString(2) == "" || rs.getString(2) == null || rs.getString(2) == "0") ? "-"
											: rs.getString(2));
							productwiseReport.setPckg2(
									(rs.getString(3) == "" || rs.getString(3) == null || rs.getString(3) == "0") ? "-"
											: rs.getString(3));
							productwiseReport.setPckg3(
									(rs.getString(4) == "" || rs.getString(4) == null || rs.getString(4) == "0") ? "-"
											: rs.getString(4));
							productwiseReport.setPckg4(
									(rs.getString(5) == "" || rs.getString(5) == null || rs.getString(5) == "0") ? "-"
											: rs.getString(5));
							productwiseReport.setPckg5(
									(rs.getString(6) == "" || rs.getString(6) == null || rs.getString(6) == "0") ? "-"
											: rs.getString(6));
							productwiseReport.setPckg6(
									(rs.getString(7) == "" || rs.getString(7) == null || rs.getString(7) == "0") ? "-"
											: rs.getString(7));
							productwiseReport.setPckg7(
									(rs.getString(8) == "" || rs.getString(8) == null || rs.getString(8) == "0") ? "-"
											: rs.getString(8));
							productwiseReport.setPckg8(
									(rs.getString(9) == "" || rs.getString(9) == null || rs.getString(9) == "0") ? "-"
											: rs.getString(9));
							productwiseReport.setPckg9(
									(rs.getString(10) == "" || rs.getString(10) == null || rs.getString(10) == "0")
											? "-"
											: rs.getString(10));
							productwiseReport.setPckg10(
									(rs.getString(11) == "" || rs.getString(11) == null || rs.getString(11) == "0")
											? "-"
											: rs.getString(11));
							productwiseReport.setPckg11(
									(rs.getString(12) == "" || rs.getString(12) == null || rs.getString(12) == "0")
											? "-"
											: rs.getString(12));
							productwiseReport.setPckg12(
									(rs.getString(13) == "" || rs.getString(13) == null || rs.getString(13) == "0")
											? "-"
											: rs.getString(13));
							productwiseReport.setPckg13(
									(rs.getString(14) == "" || rs.getString(14) == null || rs.getString(14) == "0")
											? "-"
											: rs.getString(14));
							productwiseReport.setPckg14(
									(rs.getString(15) == "" || rs.getString(15) == null || rs.getString(15) == "0")
											? "-"
											: rs.getString(15));
							productwiseReport.setPckg15(
									(rs.getString(16) == "" || rs.getString(16) == null || rs.getString(16) == "0")
											? "-"
											: rs.getString(16));
							productwiseReport.setPckg16(
									(rs.getString(17) == "" || rs.getString(17) == null || rs.getString(17) == "0")
											? "-"
											: rs.getString(17));
							productwiseReport.setPckg17(
									(rs.getString(18) == "" || rs.getString(18) == null || rs.getString(18) == "0")
											? "-"
											: rs.getString(18));
							productwiseReport.setPckg18(
									(rs.getString(19) == "" || rs.getString(19) == null || rs.getString(19) == "0")
											? "-"
											: rs.getString(19));
							productwiseReport.setPckg19(
									(rs.getString(20) == "" || rs.getString(20) == null || rs.getString(20) == "0")
											? "-"
											: rs.getString(20));
							productwiseReport.setPckg20(
									(rs.getString(21) == "" || rs.getString(21) == null || rs.getString(21) == "0")
											? "-"
											: rs.getString(21));
							productwiseReport.setPckg21(
									(rs.getString(22) == "" || rs.getString(22) == null || rs.getString(22) == "0")
											? "-"
											: rs.getString(22));
							productwiseReport.setPckg22(
									(rs.getString(23) == "" || rs.getString(23) == null || rs.getString(23) == "0")
											? "-"
											: rs.getString(23));
							productwiseReport.setPckg23(
									(rs.getString(24) == "" || rs.getString(24) == null || rs.getString(24) == "0")
											? "-"
											: rs.getString(24));
							productwiseReport.setPckg24(
									(rs.getString(25) == "" || rs.getString(25) == null || rs.getString(25) == "0")
											? "-"
											: rs.getString(25));
							productwiseReport.setPckg25(
									(rs.getString(26) == "" || rs.getString(26) == null || rs.getString(26) == "0")
											? "-"
											: rs.getString(26));
							productwiseReport.setPckg26(
									(rs.getString(27) == "" || rs.getString(27) == null || rs.getString(27) == "0")
											? "-"
											: rs.getString(27));
							productwiseReport.setPckg27(
									(rs.getString(28) == "" || rs.getString(28) == null || rs.getString(28) == "0")
											? "-"
											: rs.getString(28));
							productwiseReport.setPckg28(
									(rs.getString(29) == "" || rs.getString(29) == null || rs.getString(29) == "0")
											? "-"
											: rs.getString(29));
							productwiseReport.setPckg29(
									(rs.getString(30) == "" || rs.getString(30) == null || rs.getString(30) == "0")
											? "-"
											: rs.getString(30));
							productwiseReport.setPckg30(
									(rs.getString(31) == "" || rs.getString(31) == null || rs.getString(31) == "0")
											? "-"
											: rs.getString(31));
							productwiseReport.setPckg31(
									(rs.getString(32) == "" || rs.getString(32) == null || rs.getString(32) == "0")
											? "-"
											: rs.getString(32));
							productwiseReport.setPckg32(
									(rs.getString(33) == "" || rs.getString(33) == null || rs.getString(33) == "0")
											? "-"
											: rs.getString(33));
							productwiseReport.setPckg33(
									(rs.getString(34) == "" || rs.getString(34) == null || rs.getString(34) == "0")
											? "-"
											: rs.getString(34));
							productwiseReport.setPckg34(
									(rs.getString(35) == "" || rs.getString(35) == null || rs.getString(35) == "0")
											? "-"
											: rs.getString(35));
							productwiseReport.setPckg35(
									(rs.getString(36) == "" || rs.getString(36) == null || rs.getString(36) == "0")
											? "-"
											: rs.getString(36));
							productwiseReport.setPckg36(
									(rs.getString(37) == "" || rs.getString(37) == null || rs.getString(37) == "0")
											? "-"
											: rs.getString(37));
							productwiseReport.setPckg37(
									(rs.getString(38) == "" || rs.getString(38) == null || rs.getString(38) == "0")
											? "-"
											: rs.getString(38));
							productwiseReport.setPckg38(
									(rs.getString(39) == "" || rs.getString(39) == null || rs.getString(39) == "0")
											? "-"
											: rs.getString(39));
							productwiseReport.setPckg39(
									(rs.getString(40) == "" || rs.getString(40) == null || rs.getString(40) == "0")
											? "-"
											: rs.getString(40));
							productwiseReport.setPckg40(
									(rs.getString(41) == "" || rs.getString(41) == null || rs.getString(41) == "0")
											? "-"
											: rs.getString(41));
							productwiseReport.setPckg41(
									(rs.getString(42) == "" || rs.getString(42) == null || rs.getString(42) == "0")
											? "-"
											: rs.getString(42));
							return productwiseReport;

						}
					});
			return packetTotalCounts;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

		/*
		 * MapSqlParameterSource param = new MapSqlParameterSource();
		 * param.addValue("pwaId", pwaId); List<ManagementReportResponse>
		 * packetTotalCounts = getNamedParameterJdbcTemplate().query(query, param, new
		 * RowMapper<ManagementReportResponse>() {
		 * 
		 * @Override public ManagementReportResponse mapRow(ResultSet rs, int i) throws
		 * SQLException { ManagementReportResponse packetCounts = new
		 * ManagementReportResponse(); packetCounts.setSum1(rs.getString(1));
		 * packetCounts.setPckg1(rs.getString(2));
		 * packetCounts.setPckg2(rs.getString(3));
		 * packetCounts.setPckg3(rs.getString(4));
		 * packetCounts.setPckg4(rs.getString(5));
		 * packetCounts.setPckg5(rs.getString(6)); return packetCounts;
		 * 
		 * } }); return packetTotalCounts; } catch (Exception e) { e.printStackTrace();
		 * return null;
		 * 
		 * }
		 */
	}

	@Override
	public List<ManagementReportResponse> dateWiseProductReportYearwise(Long productnumber, int year) {
		try {
			String query = "select pr.processorder_prod_number,p.product_number,"
					+ " p.product_description,round(sum(pr.actual_quantity_afterPacking)/1000,2),"
					+ " DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packed_date,'%d-%m-%Y'),"
					+ " CONCAT(round(time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time))/60 ,1),'min')  as leadtime  "
					+ " from processorder_prod pr inner join product_table p "
					+ "  on  p.product_number=pr.product_number " + " where p.product_number=" + productnumber + ""
					+ " and  YEAR(pr.packed_date)=  " + year + " and pr.status in (2,14,18,17) "
					+ " group by processorder_prod_number " + " order by batch_date,processorder_prod_number desc  ";

			List<ManagementReportResponse> datewiseProductReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});

			query = "select pr.processorder_pckg_number,pr.product_number,\r\n"
					+ "						pr.material_memo,round(sum(pr.actual_quantity_afterPacking)/1000,2),\r\n"
					+ "					 DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packing_completed_date,'%d-%m-%Y'),\r\n"
					+ "						 CONCAT(round(time_to_sec(TIMEDIFF( pr.packing_end_time, pr.packing_start_time))/60 ,1),'min')  as leadtime  \r\n"
					+ "						 from processorder_pckg pr \r\n"
					+ "						 where pr.product_number=" + productnumber + "  and is_repack=1\r\n"
					+ "						 and  YEAR(pr.packing_completed_date)= " + year
					+ " and pr.status in (2,14,17,18) \r\n"
					+ "						 group by processorder_pckg_number \r\n"
					+ "						order by batch_date,processorder_pckg_number desc  ";

			List<ManagementReportResponse> datewiseProductReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});

			datewiseProductReport.addAll(datewiseProductReport1);
			return datewiseProductReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	@Override
	public List<ManagementReportResponse> getAll1MTReactors(Long productnumber, int year) {
		try {
			String query = "select pr.processorder_prod_number,p.product_number,p.product_description,round(sum(pr.actual_quantity_afterPacking)/1000,2) ,\r\n"
					+ " DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packed_date,'%d-%m-%Y'),CONCAT(round(time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time))/60 ,1),'min')  as leadtime  "
					+ " from processorder_prod pr inner join product_table p \r\n"
					+ "					on  p.product_number=pr.product_number\r\n"
					+ "					where p.product_number=" + productnumber + " \r\n"
					+ "            and  YEAR(pr.packed_date)= " + year
					+ " and pr.status in (2,14,17,18) and pr.reactor like'1%' \r\n"
					+ " group by processorder_prod_number " + " order by batch_date,processorder_prod_number desc ";

			List<ManagementReportResponse> datewiseProductReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			query = "select pr.processorder_pckg_number,pr.product_number,\r\n"
					+ "						pr.material_memo,round(sum(pr.actual_quantity_afterPacking)/1000,2),\r\n"
					+ "					DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packing_completed_date,'%d-%m-%Y'),\r\n"
					+ "						 CONCAT(round(time_to_sec(TIMEDIFF( pr.packing_end_time, pr.packing_start_time))/60 ,1),'min')  as leadtime  \r\n"
					+ "						 from processorder_pckg pr \r\n"
					+ "						 where pr.product_number=" + productnumber + "  and is_repack=1\r\n"
					+ "						 and  YEAR(pr.packing_completed_date)= " + year
					+ " and pr.status in (2,14,17,18) and pr.reactor like'1%' \r\n"
					+ "						 group by processorder_pckg_number \r\n"
					+ "						order by batch_date,processorder_pckg_number desc  ";

			List<ManagementReportResponse> datewiseProductReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			datewiseProductReport.addAll(datewiseProductReport1);

			return datewiseProductReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	@Override
	public List<ManagementReportResponse> getAll5MTReactors(Long productnumber, int year) {
		try {
			String query = "select pr.processorder_prod_number,p.product_number,p.product_description,round(sum(pr.actual_quantity_afterPacking)/1000,2) ,\r\n"
					+ " DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packed_date,'%d-%m-%Y'),"
					+ " CONCAT(round(time_to_sec(TIMEDIFF( pr.completed_time, pr.start_time))/60 ,1),'min') as leadtime "
					+ " from processorder_prod pr inner join product_table p \r\n"
					+ "					on  p.product_number=pr.product_number\r\n"
					+ "					where p.product_number=" + productnumber + " \r\n"
					+ "            and  YEAR(pr.packed_date)= " + year
					+ " and pr.status in(2,14,17,18) and pr.reactor like '5%' \r\n"
					+ " group by processorder_prod_number " + " order by batch_date,processorder_prod_number desc ";

			List<ManagementReportResponse> datewiseProductReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});

			query = "select pr.processorder_pckg_number,pr.product_number,\r\n"
					+ "						pr.material_memo,round(sum(pr.actual_quantity_afterPacking)/1000,2),\r\n"
					+ "					DATE_FORMAT(pr.batch_date,'%d-%m-%Y'),pr.reactor,DATE_FORMAT(pr.packing_completed_date,'%d-%m-%Y'),\r\n"
					+ "						 CONCAT(round(time_to_sec(TIMEDIFF( pr.packing_end_time, pr.packing_start_time))/60 ,1),'min')  as leadtime  \r\n"
					+ "						 from processorder_pckg pr \r\n"
					+ "						 where pr.product_number=" + productnumber + "  and is_repack=1\r\n"
					+ "						 and  YEAR(pr.packing_completed_date)= " + year
					+ " and pr.status in (2,14,17,18) and pr.reactor like'5%' \r\n"
					+ "						 group by processorder_pckg_number \r\n"
					+ "						order by batch_date,processorder_pckg_number desc  ";

			List<ManagementReportResponse> datewiseProductReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementReportResponse>>() {
						public List<ManagementReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementReportResponse> list = new ArrayList<ManagementReportResponse>();

							while (rs.next()) {
								ManagementReportResponse processorderprod = new ManagementReportResponse();
								processorderprod.setProcessOrderProdNumber(rs.getLong(1));
								processorderprod.setProductnumber(rs.getLong(2));
								processorderprod.setProductDescription(rs.getString(3));
								processorderprod.setSum1(rs.getFloat(4));
								processorderprod.setBatchDate(rs.getString(5));
								processorderprod.setReactor(rs.getString(6));
								processorderprod.setCompletedDate(rs.getString(7));
								processorderprod.setLeadTime(rs.getString(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			datewiseProductReport.addAll(datewiseProductReport1);

			return datewiseProductReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}
	// -----------------------------------------------------------------------------------------------------

	// Current Year Current Quarter Production Report

	@Override
	public CurrentQuarterReportResponse currentQuaterProductionReport() {
		CurrentQuarterReportResponse PlannedcurrentQuarterResponse = null;
		CurrentQuarterReportResponse ActualcurrentQuarterResponse = null;
		try {
			String query = "select per_quarter,working_days,quarter from planned_production "
					+ " where quarter=quarter(curdate())";

			PlannedcurrentQuarterResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<CurrentQuarterReportResponse>() {
						public CurrentQuarterReportResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							CurrentQuarterReportResponse reponseVal = new CurrentQuarterReportResponse();

							while (rs.next()) {
								reponseVal.setRemainingProductionPer(Double.valueOf(rs.getString(1)));
								reponseVal.setWorkingDatesCount(rs.getInt(2));
							}
							return reponseVal;
						}
					});

			query = "select round(sum(actual_quantity_afterPacking/1000),2) from processorder_pckg "
					+ " where quarter(packing_completed_date)=quarter(curdate()) and status in (2,14,17,18) ";

			ActualcurrentQuarterResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<CurrentQuarterReportResponse>() {
						public CurrentQuarterReportResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							CurrentQuarterReportResponse reponseVal = new CurrentQuarterReportResponse();

							while (rs.next()) {
								reponseVal.setActualProductionPer(Double.valueOf(rs.getString(1)));

							}
							return reponseVal;
						}
					});
			ActualcurrentQuarterResponse
					.setRemainingProductionPer(PlannedcurrentQuarterResponse.getRemainingProductionPer());
			ActualcurrentQuarterResponse.setWorkingDatesCount(PlannedcurrentQuarterResponse.getWorkingDatesCount());
			return ActualcurrentQuarterResponse;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<ProductForChart> currentQuaterProductWiseReport() {
		List<ProductForChart> productsForChart = null;
		try {
			String query = "select product_number,material_memo,round(sum(actual_quantity_afterPacking)/1000,2) as quantity "
					+ " from processorder_pckg where status in (2,14,17,18) and quarter(packing_completed_date)=quarter(curdate()) "
					+ " group by material_memo ";

			productsForChart = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProductForChart>>() {
						public List<ProductForChart> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProductForChart> productVal = new ArrayList<>();

							while (rs.next()) {
								ProductForChart productForChart = new ProductForChart();
								productForChart.setProductName(rs.getString(2));
								productForChart.setQuantity(String.valueOf(rs.getDouble(3)));
								productVal.add(productForChart);
							}
							return productVal;
						}
					});
			return productsForChart;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return productsForChart;
	}

	@Override
	public ManagementQuarterProductionPlan currentQuaterProductionPlan() {
		ManagementQuarterProductionPlan managementQuarterProductionPlanResponse = null;
		ManagementQuarterProductionPlan managementQuarterProductionPlanResponse1 = null;
		ManagementQuarterProductionPlan managementQuarterProductionPlanResponse2 = null;
		try {
			String query = "SELECT count(working_dates) from planfor_report "
					+ " where quarter(working_dates)=quarter(curdate()) and working_dates<= curdate()";

			managementQuarterProductionPlanResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<ManagementQuarterProductionPlan>() {
						public ManagementQuarterProductionPlan extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							ManagementQuarterProductionPlan reponseVal = new ManagementQuarterProductionPlan(0, 0, 0, 0,
									0, null, 0, null, null, null, null, 0, null);

							while (rs.next()) {
								reponseVal.setActualWorkingDays(rs.getInt(1));
							}
							return reponseVal;
						}
					});
			query = "select  round(sum(actual_quantity_afterPacking)/1000,2) as Q from processorder_pckg "
					+ " where status in (2,14,17,18) and quarter( packing_completed_date)=quarter(curdate()) ";
			managementQuarterProductionPlanResponse1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<ManagementQuarterProductionPlan>() {
						public ManagementQuarterProductionPlan extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							ManagementQuarterProductionPlan reponseVal = new ManagementQuarterProductionPlan(0, 0, 0, 0,
									0, null, 0, null, null, null, null, 0, null);

							while (rs.next()) {
								reponseVal.setActualQuantity(rs.getString("Q"));
							}
							return reponseVal;
						}
					});

			query = "select per_quarter,working_days from planned_production where quarter=quarter(curdate()) and year=year(curdate())";
			managementQuarterProductionPlanResponse2 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<ManagementQuarterProductionPlan>() {
						public ManagementQuarterProductionPlan extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							ManagementQuarterProductionPlan reponseVal = new ManagementQuarterProductionPlan(0, 0, 0, 0,
									0, null, 0, null, null, null, null, 0, null);

							while (rs.next()) {
								reponseVal.setPlannedProduction(rs.getLong(1));
								reponseVal.setWorkingDays(rs.getInt(2));
							}
							return reponseVal;
						}
					});
			managementQuarterProductionPlanResponse2
					.setActualWorkingDays(managementQuarterProductionPlanResponse.getActualWorkingDays());
			managementQuarterProductionPlanResponse2
					.setActualQuantity(managementQuarterProductionPlanResponse1.getActualQuantity());
			return managementQuarterProductionPlanResponse2;
		} catch (Exception ex) {

			ex.printStackTrace();
			return null;
		}

	}

	// -----------------------------------------------------------------------------------------------------------------------------------------------

	// Current year Production Report

	@Override
	public MonthlyResponse MontlyProductionProductWiseReport() {

		MonthlyResponse monthlyResponse = null;
		try {
			//

			String timeStamp = new SimpleDateFormat("yyyy").format(new Date());
			String query = "SELECT\r\n" + "  Year(`" + "`) as year,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=1 then `total_quantity` end) As jan,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=2 then `total_quantity` end) As feb,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=3 then `total_quantity` end) As mar,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=4 then `total_quantity` end) As apr,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=5 then `total_quantity` end) As may,\r\n" + " \r\n"
					+ "  sum(case when month(`batch_date`)=6 then `total_quantity` end) As june,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=7 then `total_quantity` end) As july,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=8 then `total_quantity` end) As aug,\r\n" + "\r\n"
					+ "  sum(case when month(`batch_date`)=9 then `total_quantity` end) As sep,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=10 then `total_quantity` end) As oct,\r\n" + "\r\n"
					+ "  sum(case when month(`batch_date`)=11 then `total_quantity` end) As nov,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=12 then `total_quantity` end) As decm\r\n" + "  \r\n"
					+ "  \r\n" + "FROM processorder_prod where Year(`batch_date`) = '" + timeStamp + "' and status=2 ";
			monthlyResponse = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<MonthlyResponse>() {
				public MonthlyResponse extractData(ResultSet rs) throws SQLException, DataAccessException {
					MonthlyResponse monRep = new MonthlyResponse();
					while (rs.next()) {
						// MonthlyResponse monRep = new MonthlyResponse();
						monRep.setYear(rs.getInt(1));
						monRep.setJan(rs.getString(2) == null ? "0" : rs.getString(2));
						monRep.setFeb(rs.getString(3) == null ? "0" : rs.getString(3));
						monRep.setMar(rs.getString(4) == null ? "0" : rs.getString(4));
						monRep.setApr(rs.getString(5) == null ? "0" : rs.getString(5));
						monRep.setMay(rs.getString(6) == null ? "0" : rs.getString(6));
						monRep.setJune(rs.getString(7) == null ? "0" : rs.getString(7));
						monRep.setJuly(rs.getString(8) == null ? "0" : rs.getString(8));
						monRep.setAug(rs.getString(9) == null ? "0" : rs.getString(9));
						monRep.setSep(rs.getString(10) == null ? "0" : rs.getString(10));
						monRep.setOct(rs.getString(11) == null ? "0" : rs.getString(11));
						monRep.setNov(rs.getString(12) == null ? "0" : rs.getString(12));
						monRep.setDecm(rs.getString(13) == null ? "0" : rs.getString(13));

					}
					// return listOfUser;
					return monRep;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			// throw new Exception(e.getMessage());
		}
		return monthlyResponse;

	}

	@Override
	public ManagementReportResponse getProductionValue(int year) {
		ManagementReportResponse pp = null;
		try {

			String query = "SELECT Round((sum(per_quarter)/12),3) as Quantity from planned_production "
					+ " where year='" + year + "'";

			pp = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<ManagementReportResponse>() {
				public ManagementReportResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					ManagementReportResponse pp = new ManagementReportResponse();

					while (rs.next()) {
						pp.setWeeklySum(rs.getFloat("Quantity"));
					}
					return pp;
				}
			});
			return pp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pp;
	}

	// --------------------------------------------------------------------------------------------------------------------------------------------

	// For multiple year production report(not using)

	@Override
	public MonthlyResponse MontlyProductionProductWiseReport(int year) {
		MonthlyResponse monthlyResponse = null;
		try {
			//

			// String timeStamp = new SimpleDateFormat("yyyy").format(new Date());
			String query = "SELECT\r\n" + "  Year(`batch_date`) as year,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=1 then `total_quantity` end) As jan,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=2 then `total_quantity` end) As feb,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=3 then `total_quantity` end) As mar,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=4 then `total_quantity` end) As apr,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=5 then `total_quantity` end) As may,\r\n" + " \r\n"
					+ "  sum(case when month(`batch_date`)=6 then `total_quantity` end) As june,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=7 then `total_quantity` end) As july,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=8 then `total_quantity` end) As aug,\r\n" + "\r\n"
					+ "  sum(case when month(`batch_date`)=9 then `total_quantity` end) As sep,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=10 then `total_quantity` end) As oct,\r\n" + "\r\n"
					+ "  sum(case when month(`batch_date`)=11 then `total_quantity` end) As nov,\r\n" + "  \r\n"
					+ "  sum(case when month(`batch_date`)=12 then `total_quantity` end) As decm\r\n" + "  \r\n"
					+ "  \r\n" + "FROM processorder_prod where Year(`batch_date`) = '" + year + "' and status=2 ";
			monthlyResponse = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<MonthlyResponse>() {
				public MonthlyResponse extractData(ResultSet rs) throws SQLException, DataAccessException {
					MonthlyResponse monRep = new MonthlyResponse();
					while (rs.next()) {
						// MonthlyResponse monRep = new MonthlyResponse();
						monRep.setYear(rs.getInt(1));
						monRep.setJan(rs.getString(2) == null ? "0" : rs.getString(2));
						monRep.setFeb(rs.getString(3) == null ? "0" : rs.getString(3));
						monRep.setMar(rs.getString(4) == null ? "0" : rs.getString(4));
						monRep.setApr(rs.getString(5) == null ? "0" : rs.getString(5));
						monRep.setMay(rs.getString(6) == null ? "0" : rs.getString(6));
						monRep.setJune(rs.getString(7) == null ? "0" : rs.getString(7));
						monRep.setJuly(rs.getString(8) == null ? "0" : rs.getString(8));
						monRep.setAug(rs.getString(9) == null ? "0" : rs.getString(9));
						monRep.setSep(rs.getString(10) == null ? "0" : rs.getString(10));
						monRep.setOct(rs.getString(11) == null ? "0" : rs.getString(11));
						monRep.setNov(rs.getString(12) == null ? "0" : rs.getString(12));
						monRep.setDecm(rs.getString(13) == null ? "0" : rs.getString(13));

					}
					// return listOfUser;
					return monRep;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			// throw new Exception(e.getMessage());
		}
		return monthlyResponse;
	}

	@Override
	public ManagementReportResponse getMonthlyProductionvalue(int year) {
		ManagementReportResponse pp = null;
		try {

			String query = "SELECT Round((sum(per_quarter)/12),3) as Quantity from planned_production "
					+ " where year='" + year + "'";

			pp = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<ManagementReportResponse>() {
				public ManagementReportResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					ManagementReportResponse pp = new ManagementReportResponse();

					while (rs.next()) {
						pp.setWeeklySum(rs.getFloat("Quantity"));
					}
					return pp;
				}
			});
			return pp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pp;
	}

	// -----------------------------------------------------------------------------------------------------------------------------------------

	// Full Quarter Production Report for Current Year

	@Override
	public List<FullQuarterReportResponse> fullQuaterProductionReport() {
		List<FullQuarterReportResponse> currentQuarterResponses = new ArrayList<FullQuarterReportResponse>();
		for (int i = 1; i <= 4; i++) {
			final int val = i;
			FullQuarterReportResponse currentQuarterResponse = new FullQuarterReportResponse();
			try {
				String query = "select quarter as q ,sum(prodPr.total_quantity) as actual,"
						+ "(case when quarter=1 then `per_quarter` end) as planned_q1,"
						+ "(case when quarter=2 then `per_quarter` end) as planned_q2,"
						+ "(case when quarter=3 then `per_quarter` end) as planned_q3,"
						+ "(case when quarter=4 then `per_quarter` end) as planned_q4 "
						+ " from processorder_prod as prodPr,planned_production prodPl where quarter = " + i
						+ " and year = year(current_date())  and status=2";

				currentQuarterResponse = getNamedParameterJdbcTemplate().query(query,
						new ResultSetExtractor<FullQuarterReportResponse>() {
							public FullQuarterReportResponse extractData(ResultSet rs)
									throws SQLException, DataAccessException {

								FullQuarterReportResponse reponseVal = new FullQuarterReportResponse();

								while (rs.next()) {
									reponseVal.setQuarterNumber((val));
									reponseVal.setActualProductionPer(
											Double.valueOf(rs.getString(2) == null ? "0" : rs.getString(2)));
									if (rs.getString(3) != null) {
										reponseVal.setRemainingProductionPer(
												Double.valueOf(rs.getString(3) == null ? "0" : rs.getString(3)));
									} else if (rs.getString(4) != null) {
										reponseVal.setRemainingProductionPer(
												Double.valueOf(rs.getString(4) == null ? "0" : rs.getString(4)));
									} else if (rs.getString(5) != null) {
										reponseVal.setRemainingProductionPer(
												Double.valueOf(rs.getString(5) == null ? "0" : rs.getString(5)));
									} else if (rs.getString(6) != null) {
										reponseVal.setRemainingProductionPer(
												Double.valueOf(rs.getString(6) == null ? "0" : rs.getString(6)));
									}
								}
								currentQuarterResponses.add(reponseVal);

								return reponseVal;
							}
						});

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return currentQuarterResponses;
	}

	@Override
	public List<ProductForChart> quaterProductionProductWiseReport(int quarterNumber) {
		List<ProductForChart> productsForChart = null;
		try {
			String query = "select prod.product_number,prod.product_description,sum(p.total_quantity) as quantity "
					+ " from processorder_prod p "
					+ " inner join product_table prod on p.product_number=prod.product_number "
					+ " where year(completed_date) = YEAR(CURDATE()) and status=2 and quarter(completed_date)="
					+ quarterNumber + " group by prod.product_description";

			productsForChart = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProductForChart>>() {
						public List<ProductForChart> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProductForChart> productVal = new ArrayList<>();

							while (rs.next()) {
								ProductForChart productForChart = new ProductForChart();
								productForChart.setProductName(rs.getString(2));
								productForChart.setQuantity(String.valueOf(rs.getDouble(3)));
								productVal.add(productForChart);
							}
							return productVal;
						}
					});
			return productsForChart;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return productsForChart;
	}

	@Override
	public List<ManagementQuarterProductionPlan> fullQuaterProductionPlan() {
		try {
			String query = "SELECT quarter,working_days,per_quarter "
					+ " from planned_production where year=year(curdate()) " + " group by quarter "
					+ " order by quarter ";
			List<ManagementQuarterProductionPlan> productionPlan = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ManagementQuarterProductionPlan>>() {
						public List<ManagementQuarterProductionPlan> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ManagementQuarterProductionPlan> list = new ArrayList<ManagementQuarterProductionPlan>();

							while (rs.next()) {
								ManagementQuarterProductionPlan productionPlans = new ManagementQuarterProductionPlan(0,
										0, 0, 0, 0, null, 0, null, null, null, null, 0, null);
								productionPlans.setQuarter(rs.getInt(1));
								productionPlans.setWorkingDays(rs.getInt(2));
								productionPlans.setPlannedProduction(rs.getLong(3));
								list.add(productionPlans);
							}
							return list;
						}
					});
			return productionPlan;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	// -----------------------------------------------------------------------------------------------------------------------------------------------
	// cycle Time for currentWeek
	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForWeek() {

		try {

			String query = "select processorderno,batchdate,batchno,processingtime,productnumber,productname,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF, \r\n"
					+ "					                   PackingTime  as PackingTimeF ,processTime as ProcessTimeF from  \r\n"
					+ "										 (Select distinct processorderno,batchdate,batchno,processingtime,productnumber,productname, \r\n"
					+ "										 round(time_to_sec(abs(timediff(ChargingTime2,ChargingTime1) ))/60,2) as ChargingTime , \r\n"
					+ "										round (time_to_sec(abs(timediff(QualityTime2,QualityTime1)) ) /60,2) as QualityTime, \r\n"
					+ "										round(time_to_sec(abs(timediff(PackingTime2,PackingTime1) )) /60,2) as PackingTime, \r\n"
					+ "					                   round(time_to_sec(abs(timediff(ProcessTime2,ProcessTime1)) ) /60,2) as processTime \r\n"
					+ "										 From ( \r\n"
					+ "										 Select processorderno,batchdate,batchno,processingtime,productnumber,productname,sum(ChargingStartTime) as ChargingTime1 , \r\n"
					+ "										 sum(ChargingEndTime) as ChargingTime2, \r\n"
					+ "										 sum(QualityCheckStartTime) as QualityTime1, \r\n"
					+ "										 sum(QualityCheckEndTime) as QualityTime2, \r\n"
					+ "										 sum(PackingStartTime) as PackingTime1, \r\n"
					+ "										 sum(PackingEndTime) as PackingTime2, \r\n"
					+ "					                    sum(processStartTime) as ProcessTime1, \r\n"
					+ "					                    sum(processEndTime) as ProcessTime2 \r\n"
					+ "										 from  ( \r\n"
					+ "										 SELECT  distinct pt.processorder_number as processorderno, pd.batch_date as batchdate,pd.batch_number as batchno,date(pd.start_time) as processingtime,pd.product_number as productnumber,prod.product_description as productname,\r\n"
					+ "										 (CASE WHEN status_id= 1  THEN min(pt.process_start_time)  END ) as ChargingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=21  THEN max(pt.process_start_time)  END ) as ChargingEndTime, \r\n"
					+ "										 (CASE WHEN status_id=15  THEN max(pt.process_start_time)  END ) as QualityCheckStartTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as QualityCheckEndTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as PackingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=14  THEN max(pt.process_start_time)  END ) as PackingEndTime, \r\n"
					+ "					                    (CASE WHEN status_id=8  THEN min(pt.process_start_time)  END ) as processStartTime, \r\n"
					+ "					                    (CASE WHEN status_id=2  THEN max(pt.process_start_time)  END ) as processEndTime \r\n"
					+ "										 from process_tracker pt inner join processorder_prod pd on pt.processorder_number=pd.processorder_prod_number\r\n"
					+ "                                            inner join product_table prod on pd.product_number=prod.product_number\r\n"
					+ "                                         where week(pt.process_start_time)=week(curdate())  \r\n"
					+ "										 group by pt.status_id,pt.processorder_number ) \r\n"
					+ "										  as tab1 group by processorderno ) \r\n"
					+ "										 as tab2 group by processorderno ) \r\n"
					+ "					as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL  \r\n"
					+ "					group by processorderno ";

			List<CycleTimeResponse> WeeklyCycleTimeReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<CycleTimeResponse>>() {
						public List<CycleTimeResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<CycleTimeResponse> list = new ArrayList<CycleTimeResponse>();

							while (rs.next()) {
								CycleTimeResponse processTimeForWeek = new CycleTimeResponse();
								processTimeForWeek.setProcessOrderNumber(rs.getString(1));
								processTimeForWeek.setBatchDate(DateFormator(rs.getString(2)));
								processTimeForWeek.setBatchNumber(rs.getString(3));
								processTimeForWeek.setProcessingDate(DateFormator(rs.getString(4)));
								processTimeForWeek.setProductNumber(rs.getString(5));
								processTimeForWeek.setProductDescription(rs.getString(6));
								processTimeForWeek.setChargingTime(rs.getFloat(7));
								processTimeForWeek.setPackingTime(rs.getFloat(9));
								processTimeForWeek.setQualityTime(rs.getFloat(8));
								processTimeForWeek.setCompletedTime(rs.getFloat(10) == 0.0 ? 0 : rs.getFloat(10));
								list.add(processTimeForWeek);
							}
							return list;
						}
					});
			return WeeklyCycleTimeReport1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	// -----------------------------------------------average for
	// week--------------------------------------------------
	@Override
	public AverageCycleTimeResponse averageCycleTimeForProcessOrdersForWeek() {
		AverageCycleTimeResponse WeeklyCycleTimeReport = null;
		try {

			String query = "select  round(sum(ChargingTimeF)/count(processorder_number),2),\r\n"
					+ "				round(sum(QualityTimeF)/count(processorder_number),2),\r\n"
					+ "					round(sum(PackingTimeF)/count(processorder_number),2),\r\n"
					+ "                   round( sum(ProcessTimeF)/count(processorder_number),2)\r\n"
					+ "                    from\r\n"
					+ "                    (select processorder_number,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF,\r\n"
					+ "                    PackingTime  as PackingTimeF ,processTime as ProcessTimeF from \r\n"
					+ "					 (Select distinct processorder_number ,\r\n"
					+ "					 time_to_sec(timediff(ChargingTime2,ChargingTime1) )/60 as ChargingTime ,\r\n"
					+ "					 time_to_sec(timediff(QualityTime2,QualityTime1) ) /60 as QualityTime,\r\n"
					+ "					 time_to_sec(timediff(PackingTime2,PackingTime1) ) /60 as PackingTime,\r\n"
					+ "                     time_to_sec(timediff(ProcessTime2,ProcessTime1) ) /60 as processTime\r\n"
					+ "					 From (\r\n"
					+ "					 Select distinct processorder_number,sum(ChargingStartTime) as ChargingTime1 ,\r\n"
					+ "					 sum(ChargingEndTime) as ChargingTime2,\r\n"
					+ "					 sum(QualityCheckStartTime) as QualityTime1,\r\n"
					+ "					 sum(QualityCheckEndTime) as QualityTime2,\r\n"
					+ "					 sum(PackingStartTime) as PackingTime1,\r\n"
					+ "					 sum(PackingEndTime) as PackingTime2,\r\n"
					+ "                     sum(processStartTime) as ProcessTime1,\r\n"
					+ "                     sum(processEndTime) as ProcessTime2\r\n"
					+ "					 from  (\r\n" + "					 SELECT  distinct processorder_number,\r\n"
					+ "					 (CASE WHEN status_id= 1  THEN min(process_start_time)  END ) as ChargingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=21  THEN max(process_start_time)  END ) as ChargingEndTime,\r\n"
					+ "					 (CASE WHEN status_id=15  THEN max(process_start_time)  END ) as QualityCheckStartTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as QualityCheckEndTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as PackingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=14  THEN max(process_start_time)  END ) as PackingEndTime,\r\n"
					+ "                     (CASE WHEN status_id=8  THEN min(process_start_time)  END ) as processStartTime,\r\n"
					+ "                     (CASE WHEN status_id=2  THEN max(process_start_time)  END ) as processEndTime\r\n"
					+ "					 from process_tracker where week(process_start_time)=week(curdate()) \r\n"
					+ "					 group by status_id,processorder_number )\r\n"
					+ "					  as tab1 group by processorder_number )\r\n"
					+ "					 as tab2 group by processorder_number )\r\n"
					+ " as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL \r\n"
					+ " group by processorder_number)as tab3 ";

			WeeklyCycleTimeReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<AverageCycleTimeResponse>() {
						public AverageCycleTimeResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							AverageCycleTimeResponse responseVal = new AverageCycleTimeResponse();

							while (rs.next()) {
								responseVal.setAverageQualityTime(rs.getString(2) == null ? "-" : rs.getString(2));
								responseVal.setAverageChargingTime(rs.getString(1) == null ? "-" : rs.getString(1));
								responseVal.setAveragePackingTime(rs.getString(3) == null ? "-" : rs.getString(3));
								responseVal.setAverageCompletedTime(rs.getString(4) == null ? "-" : rs.getString(4));
							}
							return responseVal;
						}
					});

			return WeeklyCycleTimeReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	// -----------------------------------------------------------------------------------------------------------------
	// CycleTime For Month
	@Override
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForMonth() {
		AverageCycleTimeResponse MonthlyCycleTimeReport = null;

		try {

			String query = "select  round(sum(ChargingTimeF)/count(processorder_number),2),\r\n"
					+ "				round(sum(QualityTimeF)/count(processorder_number),2),\r\n"
					+ "					round(sum(PackingTimeF)/count(processorder_number),2),\r\n"
					+ "                   round( sum(ProcessTimeF)/count(processorder_number),2)\r\n"
					+ "                    from\r\n"
					+ "                    (select processorder_number,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF,\r\n"
					+ "                    PackingTime  as PackingTimeF ,processTime as ProcessTimeF from \r\n"
					+ "					 (Select distinct processorder_number ,\r\n"
					+ "					 time_to_sec(timediff(ChargingTime2,ChargingTime1) )/60 as ChargingTime ,\r\n"
					+ "					 time_to_sec(timediff(QualityTime2,QualityTime1) ) /60 as QualityTime,\r\n"
					+ "					 time_to_sec(timediff(PackingTime2,PackingTime1) ) /60 as PackingTime,\r\n"
					+ "                     time_to_sec(timediff(ProcessTime2,ProcessTime1) ) /60 as processTime\r\n"
					+ "					 From (\r\n"
					+ "					 Select distinct processorder_number,sum(ChargingStartTime) as ChargingTime1 ,\r\n"
					+ "					 sum(ChargingEndTime) as ChargingTime2,\r\n"
					+ "					 sum(QualityCheckStartTime) as QualityTime1,\r\n"
					+ "					 sum(QualityCheckEndTime) as QualityTime2,\r\n"
					+ "					 sum(PackingStartTime) as PackingTime1,\r\n"
					+ "					 sum(PackingEndTime) as PackingTime2,\r\n"
					+ "                     sum(processStartTime) as ProcessTime1,\r\n"
					+ "                     sum(processEndTime) as ProcessTime2\r\n"
					+ "					 from  (\r\n" + "					 SELECT  distinct processorder_number,\r\n"
					+ "					 (CASE WHEN status_id= 1  THEN min(process_start_time)  END ) as ChargingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=21  THEN max(process_start_time)  END ) as ChargingEndTime,\r\n"
					+ "					 (CASE WHEN status_id=15  THEN max(process_start_time)  END ) as QualityCheckStartTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as QualityCheckEndTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as PackingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=14  THEN max(process_start_time)  END ) as PackingEndTime,\r\n"
					+ "                     (CASE WHEN status_id=8  THEN min(process_start_time)  END ) as processStartTime,\r\n"
					+ "                     (CASE WHEN status_id=2  THEN max(process_start_time)  END ) as processEndTime\r\n"
					+ "					 from process_tracker where month(process_start_time)=month(curdate()) \r\n"
					+ "					 group by status_id,processorder_number )\r\n"
					+ "					  as tab1 group by processorder_number )\r\n"
					+ "					 as tab2 group by processorder_number )\r\n"
					+ " as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL \r\n"
					+ " group by processorder_number)as tab3 ";

			MonthlyCycleTimeReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<AverageCycleTimeResponse>() {
						public AverageCycleTimeResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							AverageCycleTimeResponse responseVal = new AverageCycleTimeResponse();

							while (rs.next()) {
								responseVal.setAverageQualityTime(rs.getString(2) == null ? "-" : rs.getString(2));
								responseVal.setAverageChargingTime(rs.getString(1) == null ? "-" : rs.getString(1));
								responseVal.setAveragePackingTime(rs.getString(3) == null ? "-" : rs.getString(3));
								responseVal.setAverageCompletedTime(rs.getString(4) == null ? "-" : rs.getString(4));
							}
							return responseVal;
						}
					});

			return MonthlyCycleTimeReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// ----------------------------------------------cycletimefor
	// Month------------------------------------
	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForMonth() {
		try {

			String query = "select processorderno,batchdate,batchno,processingtime,productnumber,productname,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF, \r\n"
					+ "					                   PackingTime  as PackingTimeF ,processTime as ProcessTimeF from  \r\n"
					+ "										 (Select distinct processorderno,batchdate,batchno,processingtime,productnumber,productname, \r\n"
					+ "										 round(time_to_sec(abs(timediff(ChargingTime2,ChargingTime1) ))/60,2) as ChargingTime , \r\n"
					+ "										 round(time_to_sec(abs(timediff(QualityTime2,QualityTime1)) ) /60,2) as QualityTime, \r\n"
					+ "										 round(time_to_sec(abs(timediff(PackingTime2,PackingTime1) )) /60,2) as PackingTime, \r\n"
					+ "					                    round(time_to_sec(abs(timediff(ProcessTime2,ProcessTime1)) ) /60,2) as processTime \r\n"
					+ "										 From ( \r\n"
					+ "										 Select processorderno,batchdate,batchno,processingtime,productnumber,productname,sum(ChargingStartTime) as ChargingTime1 , \r\n"
					+ "										 sum(ChargingEndTime) as ChargingTime2, \r\n"
					+ "										 sum(QualityCheckStartTime) as QualityTime1, \r\n"
					+ "										 sum(QualityCheckEndTime) as QualityTime2, \r\n"
					+ "										 sum(PackingStartTime) as PackingTime1, \r\n"
					+ "										 sum(PackingEndTime) as PackingTime2, \r\n"
					+ "					                    sum(processStartTime) as ProcessTime1, \r\n"
					+ "					                    sum(processEndTime) as ProcessTime2 \r\n"
					+ "										 from  ( \r\n"
					+ "										 SELECT  distinct pt.processorder_number as processorderno, pd.batch_date as batchdate,pd.batch_number as batchno,date(pd.start_time) as processingtime,pd.product_number as productnumber,prod.product_description as productname,\r\n"
					+ "										 (CASE WHEN status_id= 1  THEN min(pt.process_start_time)  END ) as ChargingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=21  THEN max(pt.process_start_time)  END ) as ChargingEndTime, \r\n"
					+ "										 (CASE WHEN status_id=15  THEN max(pt.process_start_time)  END ) as QualityCheckStartTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as QualityCheckEndTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as PackingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=14  THEN max(pt.process_start_time)  END ) as PackingEndTime, \r\n"
					+ "					                    (CASE WHEN status_id=8  THEN min(pt.process_start_time)  END ) as processStartTime, \r\n"
					+ "					                    (CASE WHEN status_id=2  THEN max(pt.process_start_time)  END ) as processEndTime \r\n"
					+ "										 from process_tracker pt inner join processorder_prod pd on pt.processorder_number=pd.processorder_prod_number\r\n"
					+ "                                            inner join product_table prod on pd.product_number=prod.product_number\r\n"
					+ "                                         where month(pt.process_start_time)=month(curdate())  \r\n"
					+ "										 group by pt.status_id,pt.processorder_number ) \r\n"
					+ "										  as tab1 group by processorderno ) \r\n"
					+ "										 as tab2 group by processorderno ) \r\n"
					+ "					as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL  \r\n"
					+ "					group by processorderno ";

			List<CycleTimeResponse> MonthlyCycleTimeReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<CycleTimeResponse>>() {
						public List<CycleTimeResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<CycleTimeResponse> list = new ArrayList<CycleTimeResponse>();

							while (rs.next()) {
								CycleTimeResponse processTimeForWeek = new CycleTimeResponse();
								processTimeForWeek.setProcessOrderNumber(rs.getString(1));
								processTimeForWeek.setBatchDate(DateFormator(rs.getString(2)));
								processTimeForWeek.setBatchNumber(rs.getString(3));
								processTimeForWeek.setProcessingDate(DateFormator(rs.getString(4)));
								processTimeForWeek.setProductNumber(rs.getString(5));
								processTimeForWeek.setProductDescription(rs.getString(6));
								processTimeForWeek.setChargingTime(rs.getFloat(7));
								processTimeForWeek.setPackingTime(rs.getFloat(9));
								processTimeForWeek.setQualityTime(rs.getFloat(8));
								processTimeForWeek.setCompletedTime(rs.getFloat(10) == 0.0f ? 0 : rs.getFloat(10));
								list.add(processTimeForWeek);
							}
							return list;
						}
					});

			return MonthlyCycleTimeReport1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// ----------------------------------------------------------------------------------------------------------------
	// cycleTime for Quarter
	@Override
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForQuarter() {
		AverageCycleTimeResponse QuarterlyCycleTimeReport = null;

		try {

			String query = "select  round(sum(ChargingTimeF)/count(processorder_number),2),\r\n"
					+ "				round(sum(QualityTimeF)/count(processorder_number),2),\r\n"
					+ "					round(sum(PackingTimeF)/count(processorder_number),2),\r\n"
					+ "                   round( sum(ProcessTimeF)/count(processorder_number),2)\r\n"
					+ "                    from\r\n"
					+ "                    (select processorder_number,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF,\r\n"
					+ "                    PackingTime  as PackingTimeF ,processTime as ProcessTimeF from \r\n"
					+ "					 (Select distinct processorder_number ,\r\n"
					+ "					 time_to_sec(timediff(ChargingTime2,ChargingTime1) )/60 as ChargingTime ,\r\n"
					+ "					 time_to_sec(timediff(QualityTime2,QualityTime1) ) /60 as QualityTime,\r\n"
					+ "					 time_to_sec(timediff(PackingTime2,PackingTime1) ) /60 as PackingTime,\r\n"
					+ "                     time_to_sec(timediff(ProcessTime2,ProcessTime1) ) /60 as processTime\r\n"
					+ "					 From (\r\n"
					+ "					 Select distinct processorder_number,sum(ChargingStartTime) as ChargingTime1 ,\r\n"
					+ "					 sum(ChargingEndTime) as ChargingTime2,\r\n"
					+ "					 sum(QualityCheckStartTime) as QualityTime1,\r\n"
					+ "					 sum(QualityCheckEndTime) as QualityTime2,\r\n"
					+ "					 sum(PackingStartTime) as PackingTime1,\r\n"
					+ "					 sum(PackingEndTime) as PackingTime2,\r\n"
					+ "                     sum(processStartTime) as ProcessTime1,\r\n"
					+ "                     sum(processEndTime) as ProcessTime2\r\n"
					+ "					 from  (\r\n" + "					 SELECT  distinct processorder_number,\r\n"
					+ "					 (CASE WHEN status_id= 1  THEN min(process_start_time)  END ) as ChargingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=21  THEN max(process_start_time)  END ) as ChargingEndTime,\r\n"
					+ "					 (CASE WHEN status_id=15  THEN max(process_start_time)  END ) as QualityCheckStartTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as QualityCheckEndTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as PackingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=14  THEN max(process_start_time)  END ) as PackingEndTime,\r\n"
					+ "                     (CASE WHEN status_id=8  THEN min(process_start_time)  END ) as processStartTime,\r\n"
					+ "                     (CASE WHEN status_id=2  THEN max(process_start_time)  END ) as processEndTime\r\n"
					+ "					 from process_tracker where quarter(process_start_time)=quarter(curdate()) \r\n"
					+ "					 group by status_id,processorder_number )\r\n"
					+ "					  as tab1 group by processorder_number )\r\n"
					+ "					 as tab2 group by processorder_number )\r\n"
					+ " as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL \r\n"
					+ " group by processorder_number)as tab3 ";

			QuarterlyCycleTimeReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<AverageCycleTimeResponse>() {
						public AverageCycleTimeResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							AverageCycleTimeResponse reponseVal = new AverageCycleTimeResponse();

							while (rs.next()) {
								reponseVal.setAverageQualityTime(rs.getString(2) == null ? "-" : rs.getString(2));
								reponseVal.setAverageChargingTime(rs.getString(1) == null ? "-" : rs.getString(1));
								reponseVal.setAveragePackingTime(rs.getString(3) == null ? "-" : rs.getString(3));
								reponseVal.setAverageCompletedTime(rs.getString(4) == null ? "-" : rs.getString(4));
							}
							return reponseVal;
						}
					});

			return QuarterlyCycleTimeReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// --------------------------------------------------cycle time for
	// Quarter----------------------------------
	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForQuarter() {
		/*
		 * try {
		 * 
		 * String query = "Select processorder_number," +
		 * " time_format(sec_to_time(time_to_sec(timediff(ChargingTime2,ChargingTime1) )),'%e Days,%Hh:%im:%ss') as ChargingTime ,"
		 * +
		 * " time_format(sec_to_time(time_to_sec(timediff(PackingTime2,PackingTime1) )),'%e Days,%Hh:%im:%ss') as PackingTime,"
		 * +
		 * " time_format(sec_to_time(time_to_sec(timediff(QualityTime2,QualityTime1) )), '%e Days,%Hh:%im:%ss')as QualityTime "
		 * + " From (  " +
		 * " Select distinct processorder_number,sum(ChargingStartTime) as ChargingTime1 ,"
		 * + " sum(ChargingEndTime) as ChargingTime2, " +
		 * " sum(QualityCheckStartTime) as QualityTime1," +
		 * " sum(QualityCheckEndTime) as QualityTime2," +
		 * " sum(PackingStartTime) as PackingTime1," +
		 * " sum(PackingEndTime) as PackingTime2" + " from (" +
		 * " SELECT  distinct processorder_number," +
		 * " (CASE WHEN status_id = 1  THEN min(process_start_time)  END ) as ChargingStartTime,"
		 * +
		 * " (CASE WHEN status_id=21  THEN max(process_start_time)  END ) as ChargingEndTime,"
		 * +
		 * " (CASE WHEN status_id=15  THEN max(process_start_time)  END ) as QualityCheckStartTime,"
		 * +
		 * " (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as QualityCheckEndTime,"
		 * +
		 * " (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as PackingStartTime,"
		 * +
		 * " (CASE WHEN status_id=14  THEN max(process_start_time)  END ) as PackingEndTime"
		 * +
		 * " from process_tracker where quarter(process_start_time)=quarter(curdate())"
		 * + " group by status_id,processorder_number )" +
		 * " as tab1 group by processorder_number )" +
		 * " as tab2 group by processorder_number";
		 * 
		 * 
		 * List<CycleTimeResponse> QuarterlyCycleTimeReport =
		 * getNamedParameterJdbcTemplate().query(query, new
		 * ResultSetExtractor<List<CycleTimeResponse>>() { public
		 * List<CycleTimeResponse> extractData(ResultSet rs) throws SQLException,
		 * DataAccessException {
		 * 
		 * List<CycleTimeResponse> list = new ArrayList<CycleTimeResponse>();
		 * 
		 * while (rs.next()) { CycleTimeResponse processTimeForWeek=new
		 * CycleTimeResponse();
		 * processTimeForWeek.setProcessOrderNumber(rs.getString(1));
		 * processTimeForWeek.setChargingTime(rs.getString(2));
		 * processTimeForWeek.setPackingTime(rs.getString(3));
		 * processTimeForWeek.setQualityTime(rs.getString(4));
		 * list.add(processTimeForWeek); } return list; } });
		 * 
		 * return QuarterlyCycleTimeReport; } catch (Exception e) { e.printStackTrace();
		 * return null; }
		 */
		try {
			String query = "select processorderno,batchdate,batchno,processingtime,productnumber,productname,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF, \r\n"
					+ "					                   PackingTime  as PackingTimeF ,processTime as ProcessTimeF from  \r\n"
					+ "										 (Select distinct processorderno,batchdate,batchno,processingtime,productnumber,productname, \r\n"
					+ "										 round(time_to_sec(abs(timediff(ChargingTime2,ChargingTime1) ))/60,2) as ChargingTime , \r\n"
					+ "										 round(time_to_sec(abs(timediff(QualityTime2,QualityTime1)) ) /60,2) as QualityTime, \r\n"
					+ "										 round(time_to_sec(abs(timediff(PackingTime2,PackingTime1) )) /60,2) as PackingTime, \r\n"
					+ "					                    round(time_to_sec(abs(timediff(ProcessTime2,ProcessTime1)) ) /60,2) as processTime \r\n"
					+ "										 From ( \r\n"
					+ "										 Select processorderno,batchdate,batchno,processingtime,productnumber,productname,sum(ChargingStartTime) as ChargingTime1 , \r\n"
					+ "										 sum(ChargingEndTime) as ChargingTime2, \r\n"
					+ "										 sum(QualityCheckStartTime) as QualityTime1, \r\n"
					+ "										 sum(QualityCheckEndTime) as QualityTime2, \r\n"
					+ "										 sum(PackingStartTime) as PackingTime1, \r\n"
					+ "										 sum(PackingEndTime) as PackingTime2, \r\n"
					+ "					                    sum(processStartTime) as ProcessTime1, \r\n"
					+ "					                    sum(processEndTime) as ProcessTime2 \r\n"
					+ "										 from  ( \r\n"
					+ "										 SELECT  distinct pt.processorder_number as processorderno, pd.batch_date as batchdate,pd.batch_number as batchno,date(pd.start_time) as processingtime,pd.product_number as productnumber,prod.product_description as productname,\r\n"
					+ "										 (CASE WHEN status_id= 1  THEN min(pt.process_start_time)  END ) as ChargingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=21  THEN max(pt.process_start_time)  END ) as ChargingEndTime, \r\n"
					+ "										 (CASE WHEN status_id=15  THEN max(pt.process_start_time)  END ) as QualityCheckStartTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as QualityCheckEndTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as PackingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=14  THEN max(pt.process_start_time)  END ) as PackingEndTime, \r\n"
					+ "					                    (CASE WHEN status_id=8  THEN min(pt.process_start_time)  END ) as processStartTime, \r\n"
					+ "					                    (CASE WHEN status_id=2  THEN max(pt.process_start_time)  END ) as processEndTime \r\n"
					+ "										 from process_tracker pt inner join processorder_prod pd on pt.processorder_number=pd.processorder_prod_number\r\n"
					+ "                                            inner join product_table prod on pd.product_number=prod.product_number\r\n"
					+ "                                         where quarter(pt.process_start_time)=quarter(curdate())  \r\n"
					+ "										 group by pt.status_id,pt.processorder_number ) \r\n"
					+ "										  as tab1 group by processorderno ) \r\n"
					+ "										 as tab2 group by processorderno ) \r\n"
					+ "					as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL  \r\n"
					+ "					group by processorderno ";

			List<CycleTimeResponse> QuarterlyCycleTimeReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<CycleTimeResponse>>() {
						public List<CycleTimeResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<CycleTimeResponse> list = new ArrayList<CycleTimeResponse>();

							while (rs.next()) {
								CycleTimeResponse processTimeForWeek = new CycleTimeResponse();
								processTimeForWeek.setProcessOrderNumber(rs.getString(1));
								processTimeForWeek.setBatchDate(DateFormator(rs.getString(2)));
								processTimeForWeek.setBatchNumber(rs.getString(3));
								processTimeForWeek.setProcessingDate(DateFormator(rs.getString(4)));
								processTimeForWeek.setProductNumber(rs.getString(5));
								processTimeForWeek.setProductDescription(rs.getString(6));
								processTimeForWeek.setChargingTime(rs.getFloat(7));
								processTimeForWeek.setPackingTime(rs.getFloat(9));
								processTimeForWeek.setQualityTime(rs.getFloat(8));
								processTimeForWeek.setCompletedTime(rs.getFloat(10) == 0.0f ? 0 : rs.getFloat(10));
								list.add(processTimeForWeek);
							}
							return list;
						}
					});

			return QuarterlyCycleTimeReport1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// ----------------------------------------------------------------------------------------------------------------
	// cycleTime for Year
	@Override
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForYear() {
		AverageCycleTimeResponse yearlyCycleTimeReport = null;

		try {

			String query = "select  round(sum(ChargingTimeF)/count(processorder_number),2),\r\n"
					+ "				round(sum(QualityTimeF)/count(processorder_number),2),\r\n"
					+ "					round(sum(PackingTimeF)/count(processorder_number),2),\r\n"
					+ "                   round( sum(ProcessTimeF)/count(processorder_number),2)\r\n"
					+ "                    from\r\n"
					+ "                    (select processorder_number,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF,\r\n"
					+ "                    PackingTime  as PackingTimeF ,processTime as ProcessTimeF from \r\n"
					+ "					 (Select distinct processorder_number ,\r\n"
					+ "					 time_to_sec(timediff(ChargingTime2,ChargingTime1) )/60 as ChargingTime ,\r\n"
					+ "					 time_to_sec(timediff(QualityTime2,QualityTime1) ) /60 as QualityTime,\r\n"
					+ "					 time_to_sec(timediff(PackingTime2,PackingTime1) ) /60 as PackingTime,\r\n"
					+ "                     time_to_sec(timediff(ProcessTime2,ProcessTime1) ) /60 as processTime\r\n"
					+ "					 From (\r\n"
					+ "					 Select distinct processorder_number,sum(ChargingStartTime) as ChargingTime1 ,\r\n"
					+ "					 sum(ChargingEndTime) as ChargingTime2,\r\n"
					+ "					 sum(QualityCheckStartTime) as QualityTime1,\r\n"
					+ "					 sum(QualityCheckEndTime) as QualityTime2,\r\n"
					+ "					 sum(PackingStartTime) as PackingTime1,\r\n"
					+ "					 sum(PackingEndTime) as PackingTime2,\r\n"
					+ "                     sum(processStartTime) as ProcessTime1,\r\n"
					+ "                     sum(processEndTime) as ProcessTime2\r\n"
					+ "					 from  (\r\n" + "					 SELECT  distinct processorder_number,\r\n"
					+ "					 (CASE WHEN status_id= 1  THEN min(process_start_time)  END ) as ChargingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=21  THEN max(process_start_time)  END ) as ChargingEndTime,\r\n"
					+ "					 (CASE WHEN status_id=15  THEN max(process_start_time)  END ) as QualityCheckStartTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as QualityCheckEndTime,\r\n"
					+ "					 (CASE WHEN status_id=18  THEN max(process_start_time)  END ) as PackingStartTime,\r\n"
					+ "					 (CASE WHEN status_id=14  THEN max(process_start_time)  END ) as PackingEndTime,\r\n"
					+ "                     (CASE WHEN status_id=8  THEN min(process_start_time)  END ) as processStartTime,\r\n"
					+ "                     (CASE WHEN status_id=2  THEN max(process_start_time)  END ) as processEndTime\r\n"
					+ "					 from process_tracker where Year(process_start_time)=Year(curdate()) \r\n"
					+ "					 group by status_id,processorder_number )\r\n"
					+ "					  as tab1 group by processorder_number )\r\n"
					+ "					 as tab2 group by processorder_number )\r\n"
					+ " as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL \r\n"
					+ " group by processorder_number)as tab3 ";

			yearlyCycleTimeReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<AverageCycleTimeResponse>() {
						public AverageCycleTimeResponse extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							AverageCycleTimeResponse reponseVal = new AverageCycleTimeResponse();

							while (rs.next()) {
								reponseVal.setAverageQualityTime(rs.getString(2) == null ? "-" : rs.getString(2));
								reponseVal.setAverageChargingTime(rs.getString(1) == null ? "-" : rs.getString(1));
								reponseVal.setAveragePackingTime(rs.getString(3) == null ? "-" : rs.getString(3));
								reponseVal.setAverageCompletedTime(rs.getString(4) == null ? "-" : rs.getString(4));
							}
							return reponseVal;
						}
					});

			return yearlyCycleTimeReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	// ---------------------------------------cycle time for
	// year------------------------------

	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForYear() {
		try {
			String query = "select processorderno,batchdate,batchno,processingtime,productnumber,productname,ChargingTime  as ChargingTimeF, QualityTime  as QualityTimeF, \r\n"
					+ "					                   PackingTime  as PackingTimeF ,processTime as ProcessTimeF from  \r\n"
					+ "										 (Select distinct processorderno,batchdate,batchno,processingtime,productnumber,productname, \r\n"
					+ "										 round(time_to_sec(abs(timediff(ChargingTime2,ChargingTime1) ))/60,2) as ChargingTime , \r\n"
					+ "										round(time_to_sec(abs(timediff(QualityTime2,QualityTime1)) ) /60,2) as QualityTime, \r\n"
					+ "										 round(time_to_sec(abs(timediff(PackingTime2,PackingTime1) )) /60,2) as PackingTime, \r\n"
					+ "					                    round(time_to_sec(abs(timediff(ProcessTime2,ProcessTime1)) ) /60,2) as processTime \r\n"
					+ "										 From ( \r\n"
					+ "										 Select processorderno,batchdate,batchno,processingtime,productnumber,productname,sum(ChargingStartTime) as ChargingTime1 , \r\n"
					+ "										 sum(ChargingEndTime) as ChargingTime2, \r\n"
					+ "										 sum(QualityCheckStartTime) as QualityTime1, \r\n"
					+ "										 sum(QualityCheckEndTime) as QualityTime2, \r\n"
					+ "										 sum(PackingStartTime) as PackingTime1, \r\n"
					+ "										 sum(PackingEndTime) as PackingTime2, \r\n"
					+ "					                    sum(processStartTime) as ProcessTime1, \r\n"
					+ "					                    sum(processEndTime) as ProcessTime2 \r\n"
					+ "										 from  ( \r\n"
					+ "										 SELECT  distinct pt.processorder_number as processorderno, pd.batch_date as batchdate,pd.batch_number as batchno,date(pd.start_time) as processingtime,pd.product_number as productnumber,prod.product_description as productname,\r\n"
					+ "										 (CASE WHEN status_id= 1  THEN min(pt.process_start_time)  END ) as ChargingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=21  THEN max(pt.process_start_time)  END ) as ChargingEndTime, \r\n"
					+ "										 (CASE WHEN status_id=15  THEN max(pt.process_start_time)  END ) as QualityCheckStartTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as QualityCheckEndTime, \r\n"
					+ "										 (CASE WHEN status_id=18  THEN max(pt.process_start_time)  END ) as PackingStartTime, \r\n"
					+ "										 (CASE WHEN status_id=14  THEN max(pt.process_start_time)  END ) as PackingEndTime, \r\n"
					+ "					                    (CASE WHEN status_id=8  THEN min(pt.process_start_time)  END ) as processStartTime, \r\n"
					+ "					                    (CASE WHEN status_id=2  THEN max(pt.process_start_time)  END ) as processEndTime \r\n"
					+ "										 from process_tracker pt inner join processorder_prod pd on pt.processorder_number=pd.processorder_prod_number\r\n"
					+ "                                            inner join product_table prod on pd.product_number=prod.product_number\r\n"
					+ "                                         where Year(pt.process_start_time)=Year(curdate())  \r\n"
					+ "										 group by pt.status_id,pt.processorder_number ) \r\n"
					+ "										  as tab1 group by processorderno ) \r\n"
					+ "										 as tab2 group by processorderno ) \r\n"
					+ "					as tab4 where ChargingTime IS NOT NULL and QualityTime IS NOT NULL and PackingTime IS NOT NULL  \r\n"
					+ "					group by processorderno ";

			List<CycleTimeResponse> YearlyCycleTimeReport1 = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<CycleTimeResponse>>() {
						public List<CycleTimeResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<CycleTimeResponse> list = new ArrayList<CycleTimeResponse>();

							while (rs.next()) {
								CycleTimeResponse processTimeForWeek = new CycleTimeResponse();
								processTimeForWeek.setProcessOrderNumber(rs.getString(1));
								processTimeForWeek.setBatchDate(DateFormator(rs.getString(2)));
								processTimeForWeek.setBatchNumber(rs.getString(3));
								processTimeForWeek.setProcessingDate(DateFormator(rs.getString(4)));
								processTimeForWeek.setProductNumber(rs.getString(5));
								processTimeForWeek.setProductDescription(rs.getString(6));
								processTimeForWeek.setChargingTime(rs.getFloat(7));
								processTimeForWeek.setPackingTime(rs.getFloat(9));
								processTimeForWeek.setQualityTime(rs.getFloat(8));
								processTimeForWeek.setCompletedTime(rs.getFloat(10) == 0.0f ? 0 : rs.getFloat(10));
								list.add(processTimeForWeek);
							}
							return list;
						}
					});
			return YearlyCycleTimeReport1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	// ----------------------------------------------------------------------------------------------------
	// New Yearly Production Report

	@Override
	public YearlyReportResponse YearlyProductionReport() {
		YearlyReportResponse quarterlyResponse = null;
		YearlyReportResponse weeklyResponse = null;
		YearlyReportResponse dailyResponse = null;

		try {
			String timeStamp = new SimpleDateFormat("yyyy").format(new Date());

			String query = "SELECT  Year(`completed_date`) as year,"
					+ "sum(case when quarter(`completed_date`)=1 then `total_quantity` end) As quarter1,"
					+ " sum(case when quarter(`completed_date`)=2 then `total_quantity` end) As quarter2,"
					+ "  sum(case when quarter(`completed_date`)=3 then `total_quantity` end) As quarter3,"
					+ " sum(case when quarter(`completed_date`)=4 then `total_quantity` end) As quarter4 "
					+ " FROM processorder_prod where Year(`completed_date`) = year(curdate()) and status=2  ";

			quarterlyResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<YearlyReportResponse>() {
						public YearlyReportResponse extractData(ResultSet rs) throws SQLException, DataAccessException {
							YearlyReportResponse monRep = new YearlyReportResponse();
							while (rs.next()) {
								// MonthlyResponse monRep = new MonthlyResponse();
								monRep.setYear(rs.getInt(1));
								monRep.setQuarter1(rs.getString(2));
								monRep.setQuarter2(rs.getString(3));
								monRep.setQuarter3(rs.getString(4));
								monRep.setQuarter4(rs.getString(5));
							}
							// return listOfUser;
							return monRep;
						}
					});

			query = "SELECT  WEEKOFYEAR(completed_date) AS week_number,"
					+ "SUM(total_quantity) as Quantity FROM processorder_prod WHERE status = 2 "
					+ "GROUP BY  week_number ORDER BY  week_number ";

			weeklyResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<YearlyReportResponse>() {
						public YearlyReportResponse extractData(ResultSet rs) throws SQLException, DataAccessException {
							YearlyReportResponse monRep = new YearlyReportResponse();
							while (rs.next()) {
								// MonthlyResponse monRep = new MonthlyResponse();
								monRep.setWeekNumber(rs.getInt(1));
								monRep.setWeeklySum(rs.getString(2));

							}
							// return listOfUser;
							return monRep;
						}
					});

			query = "select total_quantity,dayofyear(completed_date) as dayNumber"
					+ " from processorder_prod WHERE (`completed_date` between  DATE_FORMAT(NOW() ,'%Y-01-01')"
					+ "AND LAST_DAY(DATE_ADD(NOW(), INTERVAL 12-MONTH(NOW()) MONTH)) ) and status=2 "
					+ "group by completed_date";

			dailyResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<YearlyReportResponse>() {
						public YearlyReportResponse extractData(ResultSet rs) throws SQLException, DataAccessException {
							YearlyReportResponse monRep = new YearlyReportResponse();
							while (rs.next()) {
								// MonthlyResponse monRep = new MonthlyResponse();
								monRep.setDailySum(rs.getString(1));
								monRep.setDayNumber(rs.getInt(2));
							}
							// return listOfUser;
							return monRep;
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
			// throw new Exception(e.getMessage());
		}
		return quarterlyResponse;
	}

	@Override
	public List<ManagementReportResponse> getPlannedProduction() {
		List<ManagementReportResponse> managementReportResponse = new ArrayList<ManagementReportResponse>();
		for (int i = 1; i <= 4; i++) {
			final int val = i;
			ManagementReportResponse managementReportResponse1 = new ManagementReportResponse();
			try {
				String query = "select quarter,per_quarter as plannedQuantityPerQuarter,"
						+ "per_week as plannedQuantityPerWeek,per_day as plannedQuantityPerDay "
						+ "from planned_production where year=year(curdate()) and quarter=" + i;

				managementReportResponse1 = getNamedParameterJdbcTemplate().query(query,
						new ResultSetExtractor<ManagementReportResponse>() {
							public ManagementReportResponse extractData(ResultSet rs)
									throws SQLException, DataAccessException {

								ManagementReportResponse reponseVal = new ManagementReportResponse();

								while (rs.next()) {
									reponseVal.setQuarterNumber(rs.getInt(1));
									reponseVal.setQuarterlySum(rs.getString(2));
									reponseVal.setWeeklySum(rs.getFloat(3));
									reponseVal.setDailySum(rs.getString(4));

								}
								managementReportResponse.add(reponseVal);

								return reponseVal;
							}
						});

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return managementReportResponse;

	}

	// ----------------------------------------------------------------------------------------------------------------

	/*
	 * @Override public CurrentQuarterWeeklyResponse CurrentQuarterWeeklyResponse()
	 * { CurrentQuarterWeeklyResponse currentQuarterWeeklyResponse = null; try {
	 * 
	 * String query = "SELECT\r\n" + "  quarter(completed_date) as quarter, \r\n" +
	 * "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=1 then `total_quantity` end) As week1,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=2 then `total_quantity` end) As week2,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=3 then `total_quantity` end) As week3,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=4 then `total_quantity` end) As week4,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=5 then `total_quantity` end) As week5,\r\n"
	 * + " \r\n" +
	 * "  sum(case when weekofyear(completed_date)=6 then `total_quantity` end) As week6,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=7 then `total_quantity` end) As week7,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=8 then `total_quantity` end) As week8,\r\n"
	 * + "\r\n" +
	 * "  sum(case when weekofyear(completed_date)=9 then `total_quantity` end) As week9,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=10 then `total_quantity` end) As week10,\r\n"
	 * + "\r\n" +
	 * "  sum(case when weekofyear(completed_date)=11 then `total_quantity` end) As week11,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=12 then `total_quantity` end) As week12,\r\n"
	 * + "  \r\n" +
	 * "  sum(case when weekofyear(completed_date)=13 then `total_quantity` end) As week13 \r\n"
	 * + "  \r\n" + "  \r\n" +
	 * " FROM processorder_prod where quarter(completed_date) = quarter(curdate()) and status=2 "
	 * ;
	 * 
	 * currentQuarterWeeklyResponse = getNamedParameterJdbcTemplate().query(query,
	 * new ResultSetExtractor<CurrentQuarterWeeklyResponse>() { public
	 * CurrentQuarterWeeklyResponse extractData(ResultSet rs) throws SQLException,
	 * DataAccessException { CurrentQuarterWeeklyResponse monRep = new
	 * CurrentQuarterWeeklyResponse(); while (rs.next()) { // MonthlyResponse monRep
	 * = new MonthlyResponse(); monRep.setquarter(rs.getInt(1));
	 * monRep.setWeek1(rs.getString(2) == null ? "0" : rs.getString(2));
	 * monRep.setWeek2(rs.getString(3) == null ? "0" : rs.getString(3));
	 * monRep.setWeek3(rs.getString(4) == null ? "0" : rs.getString(4));
	 * monRep.setWeek4(rs.getString(5) == null ? "0" : rs.getString(5));
	 * monRep.setWeek5(rs.getString(6) == null ? "0" : rs.getString(6));
	 * monRep.setWeek6(rs.getString(7) == null ? "0" : rs.getString(7));
	 * monRep.setWeek7(rs.getString(8) == null ? "0" : rs.getString(8));
	 * monRep.setWeek8(rs.getString(9) == null ? "0" : rs.getString(9));
	 * monRep.setWeek9(rs.getString(10) == null ? "0" : rs.getString(10));
	 * monRep.setWeek10(rs.getString(11) == null ? "0" : rs.getString(11));
	 * monRep.setWeek11(rs.getString(12) == null ? "0" : rs.getString(12));
	 * monRep.setWeek12(rs.getString(13) == null ? "0" : rs.getString(13));
	 * monRep.setWeek13(rs.getString(14) == null ? "0" : rs.getString(14));
	 * 
	 * } // return listOfUser; return monRep; } });
	 * 
	 * } catch (Exception e) { e.printStackTrace(); // throw new
	 * Exception(e.getMessage()); } return currentQuarterWeeklyResponse;
	 * 
	 * 
	 * }
	 * 
	 * 
	 * @Override public ManagementReportResponse getPlannedReport() {
	 * ManagementReportResponse pp = null; try {
	 * 
	 * String query = "SELECT per_week as Quantity from planned_production "
	 * +" where quarter= quarter(curdate()) ";
	 * 
	 * pp = getNamedParameterJdbcTemplate().query(query, new
	 * ResultSetExtractor<ManagementReportResponse>() { public
	 * ManagementReportResponse extractData(ResultSet rs) throws SQLException,
	 * DataAccessException {
	 * 
	 * ManagementReportResponse pp = new ManagementReportResponse();
	 * 
	 * while (rs.next()) { pp.setQuarterlySum(rs.getString(1)); } return pp; } });
	 * return pp; } catch (Exception e) { e.printStackTrace(); } return pp; }
	 */
	// ---------------------------------------------------------------------------------------------------------------------
	@Override
	public YearlyBarchartResponse YearlyProductionBarchart() {
		YearlyBarchartResponse yearly = new YearlyBarchartResponse();
		List<QuarterlyDataResponse> quarterResponse = null;
		List<WeeklyDataResponse> weeklyResponse = null;
		List<DailyDataResponse> dailyReponse = null;
		List<PlannedReportResponse> plannedResponse = null;
		try {
			String timeStamp = new SimpleDateFormat("yyyy").format(new Date());

			String query = " select year,round(quarter1/1000,2),round(quarter2/1000,2),round(quarter3/1000,2),round(quarter4/1000,2) from"
					+ " (SELECT  Year(`completed_date`) as year,"
					+ "sum(case when quarter(`completed_date`)=1 then `total_quantity` end) As quarter1,"
					+ " sum(case when quarter(`completed_date`)=2 then `total_quantity` end) As quarter2,"
					+ "  sum(case when quarter(`completed_date`)=3 then `total_quantity` end) As quarter3,"
					+ " sum(case when quarter(`completed_date`)=4 then `total_quantity` end) As quarter4 "
					+ " FROM processorder_prod where Year(`completed_date`) = year(curdate()) and status=2) as t1  ";

			quarterResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<QuarterlyDataResponse>>() {
						public List<QuarterlyDataResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<QuarterlyDataResponse> list = new ArrayList<QuarterlyDataResponse>();
							while (rs.next()) {
								QuarterlyDataResponse monRep = new QuarterlyDataResponse();
								monRep.setYear(rs.getInt(1));
								monRep.setQuarter1(rs.getString(2) == null ? "0" : rs.getString(2));
								monRep.setQuarter2(rs.getString(3) == null ? "0" : rs.getString(3));
								monRep.setQuarter3(rs.getString(4) == null ? "0" : rs.getString(4));
								monRep.setQuarter4(rs.getString(5) == null ? "0" : rs.getString(5));
								list.add(monRep);

							}
							return list;
						}
					});

			query = "SELECT  WEEKOFYEAR(completed_date) AS week_number,"
					+ "round(SUM(total_quantity)/1000,2) as Quantity FROM processorder_prod WHERE status = 2 "
					+ "and year(completed_date)=year(curdate()) " + "GROUP BY  week_number ORDER BY  week_number ";

			weeklyResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<WeeklyDataResponse>>() {
						public List<WeeklyDataResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<WeeklyDataResponse> weeklyResponse = new ArrayList<WeeklyDataResponse>();
							for (int i = 1; i <= 54; i++) {
								WeeklyDataResponse wdr = new WeeklyDataResponse();
								wdr.setWeekNumber(String.valueOf(i - 1));
								wdr.setQuantity(String.valueOf(String.valueOf(0)));
								weeklyResponse.add(wdr);
							}
							while (rs.next()) {
								WeeklyDataResponse monRep = new WeeklyDataResponse();
								// String weekNumber = rs.getString(1);
								// WeeklyDataResponse weekly = weeklyResponse.get();
								// weeklyResponse.get(Integer.valueOf(rs.getString(1))).setWeekNumber(rs.getString(1));
								weeklyResponse.get(Integer.valueOf(rs.getString(1))).setQuantity(rs.getString(2));
								/*
								 * monRep.setWeekNumber(rs.getString(1)); monRep.setQuantity(rs.getString(2));
								 * weeklyResponse.add(monRep);
								 */
							}
							return weeklyResponse;
						}
					});

			query = "select total_quantity,dayofyear(completed_date) as dayNumber,completed_date "
					+ " from processorder_prod WHERE (`completed_date` between  DATE_FORMAT(NOW() ,'%Y-01-01')"
					+ "AND LAST_DAY(DATE_ADD(NOW(), INTERVAL 12-MONTH(NOW()) MONTH)) ) and status=2 "
					+ "group by completed_date";

			dailyReponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<DailyDataResponse>>() {
						public List<DailyDataResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<DailyDataResponse> list = new ArrayList<DailyDataResponse>();
							while (rs.next()) {
								DailyDataResponse monRep = new DailyDataResponse();
								// MonthlyResponse monRep = new MonthlyResponse();
								monRep.setDayNumber(rs.getString(2));
								monRep.setDailyQuantity(rs.getString(1));
								monRep.setCompletedDate(DateFormator(rs.getString(3)));
								list.add(monRep);
							}
							return list;
						}
					});

			query = "SELECT  quarter as Quarter,"
					+ " sum(case when quarter=1 then `per_quarter` end) As Planned_quarter1,"
					+ " sum(case when quarter=2 then `per_quarter` end) As Planned_quarter2,"
					+ " sum(case when quarter=3 then `per_quarter` end) As Planned_quarter3,"
					+ " sum(case when quarter=4 then `per_quarter` end) As Planned_quarter4 ,"
					+ " sum(case when quarter=1 then `per_week` end) As Planned_week_q1,"
					+ " sum(case when quarter=2 then `per_week` end) As Planned_week_q2,"
					+ " sum(case when quarter=3 then `per_week` end) As Planned_week_q3,"
					+ " sum(case when quarter=4 then `per_week` end) As Planned_week_q4 "
					+ " FROM planned_production where Year= year(curdate()) ";

			plannedResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<PlannedReportResponse>>() {
						public List<PlannedReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<PlannedReportResponse> list = new ArrayList<PlannedReportResponse>();
							while (rs.next()) {
								PlannedReportResponse reponseVal = new PlannedReportResponse();
								reponseVal.setQuarterNumber(rs.getInt(1));
								reponseVal
										.setPlannedQuantityPerQuarter1(rs.getString(2) == null ? "0" : rs.getString(2));
								reponseVal
										.setPlannedQuantityPerQuarter2(rs.getString(3) == null ? "0" : rs.getString(3));
								reponseVal
										.setPlannedQuantityPerQuarter3(rs.getString(4) == null ? "0" : rs.getString(4));
								reponseVal
										.setPlannedQuantityPerQuarter4(rs.getString(5) == null ? "0" : rs.getString(5));
								reponseVal.setPlannedQuantityPerWeekQ1(rs.getString(6) == null ? "0" : rs.getString(6));
								reponseVal.setPlannedQuantityPerWeekQ2(rs.getString(7) == null ? "0" : rs.getString(7));
								reponseVal.setPlannedQuantityPerWeekQ3(rs.getString(8) == null ? "0" : rs.getString(8));
								reponseVal.setPlannedQuantityPerWeekQ4(rs.getString(9) == null ? "0" : rs.getString(9));
								list.add(reponseVal);
							}
							return list;
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
		}

		yearly.setQuarterlyData(quarterResponse);
		yearly.setWeeklyData(weeklyResponse);
		yearly.setDailyData(dailyReponse);
		yearly.setPlannedData(plannedResponse);

		return yearly;

	}

	// -------------------------------------------------------------------------------------------------------------
	// multiple year Barchart

	@Override
	public YearlyBarchartResponse MultipleYearProductionBarchart(int year) {
		YearlyBarchartResponse yearly = new YearlyBarchartResponse();
		List<QuarterlyDataResponse> quarterResponse = null;
		List<WeeklyDataResponse> weeklyResponse = null;
		List<DailyDataResponse> dailyReponse = null;
		List<PlannedReportResponse> plannedResponse = null;
		try {
			String timeStamp = new SimpleDateFormat("yyyy").format(new Date());

			String query = " select year,round(quarter1/1000,2),round(quarter2/1000,2),round(quarter3/1000,2),round(quarter4/1000,2) from"
					+ " (SELECT  Year(`completed_date`) as year,"
					+ "sum(case when quarter(`completed_date`)=1 then `total_quantity` end) As quarter1,"
					+ " sum(case when quarter(`completed_date`)=2 then `total_quantity` end) As quarter2,"
					+ "  sum(case when quarter(`completed_date`)=3 then `total_quantity` end) As quarter3,"
					+ " sum(case when quarter(`completed_date`)=4 then `total_quantity` end) As quarter4 "
					+ " FROM processorder_prod where Year(`completed_date`) =" + year + " and status=2 ) as t1  ";

			quarterResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<QuarterlyDataResponse>>() {
						public List<QuarterlyDataResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<QuarterlyDataResponse> list = new ArrayList<QuarterlyDataResponse>();
							while (rs.next()) {
								QuarterlyDataResponse monRep = new QuarterlyDataResponse();
								monRep.setYear(rs.getInt(1));
								monRep.setQuarter1(rs.getString(2) == null ? "0" : rs.getString(2));
								monRep.setQuarter2(rs.getString(3) == null ? "0" : rs.getString(3));
								monRep.setQuarter3(rs.getString(4) == null ? "0" : rs.getString(4));
								monRep.setQuarter4(rs.getString(5) == null ? "0" : rs.getString(5));
								list.add(monRep);

							}
							return list;
						}
					});

			query = "SELECT  WEEK(completed_date) AS week_number,"
					+ "round(SUM(total_quantity)/1000,2) as Quantity FROM processorder_prod WHERE status = 2 "
					+ " and year(completed_date)=" + year + " " + "GROUP BY  week_number ORDER BY  week_number ";

			weeklyResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<WeeklyDataResponse>>() {
						public List<WeeklyDataResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<WeeklyDataResponse> weeklyResponse = new ArrayList<WeeklyDataResponse>();
							for (int i = 1; i <= 54; i++) {
								WeeklyDataResponse wdr = new WeeklyDataResponse();
								wdr.setWeekNumber(String.valueOf(i - 1));
								wdr.setQuantity(String.valueOf(String.valueOf(0)));
								weeklyResponse.add(wdr);
							}
							/*
							 * System.out.println("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");
							 * for(WeeklyDataResponse e:weeklyResponse){
							 * System.out.println(e.getQuantity()); System.out.println(e.getWeekNumber());}
							 * System.out.println("ssssssssssssssssssssssssssssssssssssssssssssssssss");
							 */
							while (rs.next()) {
								WeeklyDataResponse monRep = new WeeklyDataResponse();
								// String weekNumber = rs.getString(1);
								// WeeklyDataResponse weekly = weeklyResponse.get();
								// weeklyResponse.get(Integer.valueOf(rs.getString(1))).setWeekNumber(rs.getString(1));
								weeklyResponse.get(Integer.valueOf(rs.getString(1))).setQuantity(rs.getString(2));
								System.out.println("Week num "
										+ weeklyResponse.get(Integer.valueOf(rs.getString(1))).getWeekNumber()
										+ "Quantity "
										+ weeklyResponse.get(Integer.valueOf(rs.getString(1))).getQuantity());
								/*
								 * monRep.setWeekNumber(rs.getString(1)); monRep.setQuantity(rs.getString(2));
								 * weeklyResponse.add(monRep);
								 */
							}
							return weeklyResponse;
						}
					});

			query = "select total_quantity,dayofyear(completed_date) as dayNumber,completed_date "
					+ " from processorder_prod WHERE (`completed_date` between  DATE_FORMAT(NOW() ,'%Y-01-01')"
					+ "AND LAST_DAY(DATE_ADD(NOW(), INTERVAL 12-MONTH(NOW()) MONTH)) ) and status=2 and year(completed_date)="
					+ year + "  " + "group by completed_date";

			dailyReponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<DailyDataResponse>>() {
						public List<DailyDataResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<DailyDataResponse> list = new ArrayList<DailyDataResponse>();
							while (rs.next()) {
								DailyDataResponse monRep = new DailyDataResponse();
								// MonthlyResponse monRep = new MonthlyResponse();
								monRep.setDayNumber(rs.getString(2));
								monRep.setDailyQuantity(rs.getString(1));
								monRep.setCompletedDate(DateFormator(rs.getString(3)));
								list.add(monRep);
							}
							return list;
						}
					});

			query = "SELECT  quarter as Quarter,"
					+ " sum(case when quarter=1 then `per_quarter` end) As Planned_quarter1,"
					+ " sum(case when quarter=2 then `per_quarter` end) As Planned_quarter2,"
					+ " sum(case when quarter=3 then `per_quarter` end) As Planned_quarter3,"
					+ " sum(case when quarter=4 then `per_quarter` end) As Planned_quarter4 ,"
					+ " sum(case when quarter=1 then `per_week` end) As Planned_week_q1,"
					+ " sum(case when quarter=2 then `per_week` end) As Planned_week_q2,"
					+ " sum(case when quarter=3 then `per_week` end) As Planned_week_q3,"
					+ " sum(case when quarter=4 then `per_week` end) As Planned_week_q4 "
					+ " FROM planned_production where Year= " + year + " ";

			plannedResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<PlannedReportResponse>>() {
						public List<PlannedReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<PlannedReportResponse> list = new ArrayList<PlannedReportResponse>();
							while (rs.next()) {
								PlannedReportResponse reponseVal = new PlannedReportResponse();
								reponseVal.setQuarterNumber(rs.getInt(1));
								reponseVal
										.setPlannedQuantityPerQuarter1(rs.getString(2) == null ? "0" : rs.getString(2));
								reponseVal
										.setPlannedQuantityPerQuarter2(rs.getString(3) == null ? "0" : rs.getString(3));
								reponseVal
										.setPlannedQuantityPerQuarter3(rs.getString(4) == null ? "0" : rs.getString(4));
								reponseVal
										.setPlannedQuantityPerQuarter4(rs.getString(5) == null ? "0" : rs.getString(5));
								reponseVal.setPlannedQuantityPerWeekQ1(rs.getString(6) == null ? "0" : rs.getString(6));
								reponseVal.setPlannedQuantityPerWeekQ2(rs.getString(7) == null ? "0" : rs.getString(7));
								reponseVal.setPlannedQuantityPerWeekQ3(rs.getString(8) == null ? "0" : rs.getString(8));
								reponseVal.setPlannedQuantityPerWeekQ4(rs.getString(9) == null ? "0" : rs.getString(9));
								list.add(reponseVal);
							}
							return list;
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
		}

		yearly.setQuarterlyData(quarterResponse);
		yearly.setWeeklyData(weeklyResponse);
		yearly.setDailyData(dailyReponse);
		yearly.setPlannedData(plannedResponse);

		return yearly;

	}
	// --------------------------------------------------------------------------------------------------------------
	// Daywise Production Report

	/*
	 * @Override public List<ManagementReportResponse> dayWiseProductionReport() {
	 * 
	 * try { String query = "select working_dates,per_day from planfor_report " +
	 * " where quarter=quarter(curdate()) group by working_dates order by working_dates desc "
	 * ; MapSqlParameterSource param = new MapSqlParameterSource();
	 * param.addValue("pwaId", pwaId); List<ManagementReportResponse>
	 * plannedDaywiseReports = getNamedParameterJdbcTemplate().query(query, param,
	 * new RowMapper<ManagementReportResponse>() { public ManagementReportResponse
	 * mapRow(ResultSet rs, int i) throws SQLException { ManagementReportResponse
	 * plannedDaywiseReports = new ManagementReportResponse(); //
	 * weekWiseReports.setBatchDate(rs.getString(2));
	 * plannedDaywiseReports.setPlannedDate(rs.getString(1));
	 * plannedDaywiseReports.setSum1(rs.getString(2)); return plannedDaywiseReports;
	 * 
	 * } });
	 * 
	 * for(ManagementReportResponse e:plannedDaywiseReports){
	 * System.out.println(e.getPlannedDate()+" quantity "+e.getSum1()); }
	 * 
	 * query =
	 * "select date(packing_completed_date)  as packingdate,round(sum(actual_quantity_afterPacking)/1000,2) from processorder_pckg  "
	 * +
	 * " where status in(2,14,17,18) and quarter(packing_completed_date)=quarter(curdate()) "
	 * + " group by packingdate order by packingdate desc"; MapSqlParameterSource
	 * param1 = new MapSqlParameterSource(); param.addValue("pwaId", pwaId);
	 * List<ManagementReportResponse> actualDaywiseReports =
	 * getNamedParameterJdbcTemplate().query(query, param1, new
	 * RowMapper<ManagementReportResponse>() { public ManagementReportResponse
	 * mapRow(ResultSet rs, int i) throws SQLException { ManagementReportResponse
	 * actualDaywiseReports = new ManagementReportResponse(); //
	 * weekWiseReports.setBatchDate(rs.getString(2));
	 * actualDaywiseReports.setCompletedDate(rs.getString(1));
	 * actualDaywiseReports.setWeeklySum(rs.getString(2)); return
	 * actualDaywiseReports;
	 * 
	 * } }); for(ManagementReportResponse e : actualDaywiseReports){
	 * System.out.println(e.getCompletedDate()+""+e.getWeeklySum());; }
	 * 
	 * for(ManagementReportResponse e:plannedDaywiseReports) {
	 * for(ManagementReportResponse e1:actualDaywiseReports) {
	 * if(e.getPlannedDate().equals(e1.getCompletedDate())) {
	 * e.setWeeklySum(e1.getWeeklySum()); } if(e.getPlannedDate()
	 * !=e1.getCompletedDate()) { if(e1.getWeeklySum()!=null) {
	 * e.setWeeklySum(e1.getWeeklySum()); } } } } for(ManagementReportResponse
	 * e:daywiseReports){
	 * //e.getDailySum((e.getSum1()-e.getQuarterlySum())/(e.getQuarterlySum())*100);
	 * Float a=Float.valueOf(e.getSum1()); Float
	 * b=Float.valueOf(e.getQuarterlySum()); Float c=((a-b)/b)*100;
	 * 
	 * String d=String.valueOf(c); String g=String.format("%.2f", c);
	 * e.setDailySum(g);
	 * 
	 * }
	 * 
	 * 
	 * return plannedDaywiseReports; } catch (Exception e) { e.printStackTrace();
	 * return null;
	 * 
	 * } }
	 */

	@Override
	public List<ManagementReportResponse> dayWiseProductionReport() {

		try {
			String query = "select DATE_FORMAT(working_dates,'%d-%m-%Y'),per_day from planfor_report "
					+ " where quarter=quarter(curdate()) group by working_dates order by working_dates desc ";
			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> plannedDaywiseReports = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse plannedDaywiseReports = new ManagementReportResponse();
							// weekWiseReports.setBatchDate(rs.getString(2));
							plannedDaywiseReports.setPlannedDate(rs.getString(1));
							plannedDaywiseReports.setSum1(rs.getFloat(2));
							return plannedDaywiseReports;

						}
					});

			query = "select date_format(packing_completed_date,'%d-%m-%Y')  as packingdate,round(sum(actual_quantity_afterPacking)/1000,2) from processorder_pckg  "
					+ " where status in(2,14,17,18) and quarter(packing_completed_date)=quarter(curdate()) "
					+ " group by packingdate order by packingdate desc";
			MapSqlParameterSource param1 = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> actualDaywiseReports = getNamedParameterJdbcTemplate().query(query, param1,
					new RowMapper<ManagementReportResponse>() {
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse actualDaywiseReports = new ManagementReportResponse();
							// weekWiseReports.setBatchDate(rs.getString(2));
							actualDaywiseReports.setCompletedDate(rs.getString(1));
							actualDaywiseReports.setWeeklySum(rs.getFloat(2));
							return actualDaywiseReports;

						}
					});

			for (ManagementReportResponse e : plannedDaywiseReports) {
				for (ManagementReportResponse e1 : actualDaywiseReports) {
					if (e.getPlannedDate().equals(e1.getCompletedDate())) {
						e.setWeeklySum(e1.getWeeklySum());

					}

				}
			}
			for (ManagementReportResponse e : plannedDaywiseReports) {
				if (e.getWeeklySum() == null) {
					e.setWeeklySum(0.0f);
				}
			}
			for (ManagementReportResponse e : plannedDaywiseReports) {
				// e.getDailySum((e.getSum1()-e.getQuarterlySum())/(e.getQuarterlySum())*100);
				Float a = Float.valueOf(e.getWeeklySum());
				Float b = Float.valueOf(e.getSum1());
				Float c = ((a - b) / b) * 100;
				String d = String.valueOf(c);
				String g = String.format("%.2f", c);
				e.setDailySum(g);

			}

			return plannedDaywiseReports;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

	@Override
	public List<ManagementReportResponse> dayWiseProductionReport(int quarter) {
		try {
			String query = "select DATE_FORMAT(working_dates,'%d-%m-%Y'),per_day from planfor_report "
					+ " where quarter=" + quarter + " group by working_dates order by working_dates desc ";
			MapSqlParameterSource param = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> plannedDaywiseReports = getNamedParameterJdbcTemplate().query(query, param,
					new RowMapper<ManagementReportResponse>() {
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse plannedDaywiseReports = new ManagementReportResponse();
							// weekWiseReports.setBatchDate(rs.getString(2));
							plannedDaywiseReports.setPlannedDate(rs.getString(1));
							plannedDaywiseReports.setSum1(rs.getFloat(2));
							return plannedDaywiseReports;

						}
					});

			query = "select date_format(packing_completed_date,'%d-%m-%Y') as packingdate,round(sum(actual_quantity_afterPacking)/1000,2) from processorder_pckg  "
					+ " where status in(2,14,17,18) and quarter(packing_completed_date)=" + quarter + " "
					+ " group by packingdate order by packingdate desc";
			MapSqlParameterSource param1 = new MapSqlParameterSource();
			/* param.addValue("pwaId", pwaId); */
			List<ManagementReportResponse> actualDaywiseReports = getNamedParameterJdbcTemplate().query(query, param1,
					new RowMapper<ManagementReportResponse>() {
						public ManagementReportResponse mapRow(ResultSet rs, int i) throws SQLException {
							ManagementReportResponse actualDaywiseReports = new ManagementReportResponse();
							// weekWiseReports.setBatchDate(rs.getString(2));
							actualDaywiseReports.setCompletedDate(rs.getString(1));
							actualDaywiseReports.setWeeklySum(rs.getFloat(2));
							return actualDaywiseReports;

						}
					});

			for (ManagementReportResponse e : plannedDaywiseReports) {
				for (ManagementReportResponse e1 : actualDaywiseReports) {
					if (e.getPlannedDate().equals(e1.getCompletedDate())) {
						e.setWeeklySum(e1.getWeeklySum());
					}

				}
			}
			for (ManagementReportResponse e : plannedDaywiseReports) {
				if (e.getWeeklySum() == null) {
					e.setWeeklySum(0.0f);
				}
			}
			for (ManagementReportResponse e : plannedDaywiseReports) {
				// e.getDailySum((e.getSum1()-e.getQuarterlySum())/(e.getQuarterlySum())*100);
				Float a = Float.valueOf(e.getWeeklySum());
				Float b = Float.valueOf(e.getSum1());
				Float c = ((a - b) / b) * 100;
				String d = String.valueOf(c);
				String g = String.format("%.2f", c);
				e.setDailySum(g);

			}
			return plannedDaywiseReports;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}
	// ================================================================================================================
	// Repacking Report

	@Override
	public List<RepackingReportResponse> rePackingReport() {
		try {
			String query = "select processorder_pckg_number,sixml_number,batch_date,product_name,"
					+ " round((total_quantity*wght_pckg)/1000,2) as totalQuant,status,wght_pckg,total_quantity,product_number"
					+ " from processorder_pckg "
					+ " where status=2 and is_repack=1 and year(completed_date)=year(curdate())"
					+ " group by completed_date ";

			List<RepackingReportResponse> repackReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<RepackingReportResponse>>() {
						public List<RepackingReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<RepackingReportResponse> list = new ArrayList<RepackingReportResponse>();

							while (rs.next()) {
								RepackingReportResponse repackReport = new RepackingReportResponse();
								repackReport.setProcessOrderPackageNumber(rs.getLong(1));
								repackReport.setSixMlNumber(rs.getString(2));
								repackReport.setBatchDate(DateFormator(rs.getString(3)));
								repackReport.setProductName(rs.getString(4));
								repackReport.setTotalQuantity(rs.getString(5));
								repackReport.setStatusId(rs.getInt(6));
								repackReport.setPacketSize(rs.getString(7));
								repackReport.setTotalPacketCounts(rs.getString(8));
								repackReport.setProductNumber(rs.getString(9));
								list.add(repackReport);
							}
							return list;
						}
					});
			return repackReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<RepackingReportResponse> rePackingReport(int year) {
		try {
			String query = "select processorder_pckg_number,sixml_number,batch_date,product_name,"
					+ " round((total_quantity*wght_pckg)/1000,2) as totalQuant,status,wght_pckg,total_quantity,product_number"
					+ " from processorder_pckg " + " where status=2 and is_repack=1 and year(completed_date)='" + year
					+ "'" + " group by completed_date ";

			List<RepackingReportResponse> repackReport = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<RepackingReportResponse>>() {
						public List<RepackingReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<RepackingReportResponse> list = new ArrayList<RepackingReportResponse>();

							while (rs.next()) {
								RepackingReportResponse repackReport = new RepackingReportResponse();
								repackReport.setProcessOrderPackageNumber(rs.getLong(1));
								repackReport.setSixMlNumber(rs.getString(2));
								repackReport.setBatchDate(DateFormator(rs.getString(3)));
								repackReport.setProductName(rs.getString(4));
								repackReport.setTotalQuantity(rs.getString(5));
								repackReport.setStatusId(rs.getInt(6));
								repackReport.setPacketSize(rs.getString(7));
								repackReport.setTotalPacketCounts(rs.getString(8));
								repackReport.setProductNumber(rs.getString(9));
								list.add(repackReport);
							}
							return list;
						}
					});
			return repackReport;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// -------------------------------------------------------------------------------------------------------
	// Asking Rate
	@Override
	public int askingRate() {
		int count = 0;
		Date previousWD = null;
		try {
			String query = "SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(working_dates, ',', n.n), ',', -1) value"
					+ " FROM planned_production t CROSS JOIN " + "(" + " SELECT a.N + b.N * 10 + 1 n  FROM "
					+ " (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a"
					+ "  ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b"
					+ " ORDER BY n ) n"
					+ " WHERE quarter = quarter(curdate()) and n.n <= 1 + (LENGTH(t.working_dates) - LENGTH(REPLACE(t.working_dates, ',', '')))"
					+ " order by working_dates asc ";

			List<String> data = getJdbcTemplate().query(query, new RowMapper<String>() {
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getString(1);
				}
			});

			String currentDate = new SimpleDateFormat("dd-MM-yyyy ").format(new Date());
			Date curdate = new SimpleDateFormat("dd-MM-yyyy").parse(currentDate);
			SimpleDateFormat format = new SimpleDateFormat();

			for (String wd : data) {

				/* Date workdate=format.parse(wd); */
				Date workdate = new SimpleDateFormat("dd-MM-yyyy").parse(wd);
				if (workdate.equals(curdate)) {
					count++;
					break;
				} else if (workdate.after(curdate)) {
					break;
				} else {
					previousWD = workdate;
					count++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

		return count;
	}

	// -------------------------------------------------

	@Override
	public List<PackingReportResponse> cycleTimeForModal(Long processorderProdNumber) {
		try {

			String query = "select distinct processorder_pckg_number, processorder_prod_number,"
					+ " date_format(packing_start_time,'%d-%m-%Y %H:%i:%s  %p'),date_format(packing_end_time,'%d-%m-%Y %h:%i:%s %p'), "
					+ " round(time_to_sec(abs(timediff(packing_end_time,packing_start_time) ))/60,2) as packingLeadTime ,"
					+ " sixml_number,actual_quantity_afterPacking,quantity_after_packing from processorder_pckg\r\n"
					+ "where processorder_prod_number=" + processorderProdNumber + " \r\n"
					+ "group by processorder_pckg_number";

			List<PackingReportResponse> dailyProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<PackingReportResponse>>() {
						public List<PackingReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<PackingReportResponse> list = new ArrayList<PackingReportResponse>();

							while (rs.next()) {
								PackingReportResponse processorderprod = new PackingReportResponse();
								processorderprod.setProcessorderPackageNumber(rs.getLong(1));
								processorderprod.setProcessorderProdNumber(rs.getLong(2));
								processorderprod.setPackingStartTime(rs.getString(3));
								processorderprod.setPackingEndTime(rs.getString(4));
								processorderprod.setPackingLeadTime(rs.getString(5));
								processorderprod.setSixMillionNumber(rs.getLong(6));
								processorderprod.setActualQuantityAfterPacking(rs.getString(7));
								processorderprod.setPacketSizeAfterPacking(rs.getInt(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			return dailyProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	// -----------------------------------------------------------------------------------------------------------------------
	@Override
	public QuarterBarchartResponse1 barchartReports(String year) {
		QuarterBarchartResponse1 response = new QuarterBarchartResponse1();
		try {

			String query = "select distinct quarter,per_quarter from planfor_report " + " where year=" + year
					+ " group by quarter order by quarter  ";

			List<QuarterBarchartResponse> plannedResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<QuarterBarchartResponse>>() {
						public List<QuarterBarchartResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<QuarterBarchartResponse> plannedResponse = new ArrayList<QuarterBarchartResponse>();
							for (int i = 1; i <= 4; i++) {
								QuarterBarchartResponse wdr = new QuarterBarchartResponse();
								wdr.setQuarterNumber(String.valueOf(i));
								wdr.setPlannedQuarterProduction(String.valueOf(String.valueOf(0)));
								wdr.setActualQuarterProduction(String.valueOf(String.valueOf(0)));
								plannedResponse.add(wdr);
							}

							while (rs.next()) {
								plannedResponse.get(Integer.valueOf(rs.getString(1)) - 1)
										.setPlannedQuarterProduction(rs.getString(2));
							}

							return plannedResponse;
						}

					});

			/*
			 * for(QuarterBarchartResponse e:plannedResponse) {
			 * System.out.println("quarter"+e.getQuarterNumber()+"quantity"+e.
			 * getPlannedQuarterProduction()); }
			 */

			query = " select quarter(packing_completed_date) as q," + " round(sum(actual_quantity_afterPacking/1000),2)"
					+ " from processorder_pckg where status in (2,14,17,18) and year(packing_completed_date)=" + year
					+ " " + " group by q order by q ";

			List<QuarterBarchartResponse> actualResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<QuarterBarchartResponse>>() {
						public List<QuarterBarchartResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<QuarterBarchartResponse> list = new ArrayList<QuarterBarchartResponse>();

							while (rs.next()) {
								plannedResponse.get(Integer.valueOf(rs.getString(1)) - 1)
										.setActualQuarterProduction(rs.getString(2));
							}

							return list;
						}

					});

			query = "select weekofyear(working_dates) as weeks,sum(per_day) from planfor_report " + " where year="
					+ year + "" + " group by weeks " + " order by weeks ";

			List<WeekwiseData> weeklyplannedResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<WeekwiseData>>() {
						public List<WeekwiseData> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<WeekwiseData> weeklyplannedResponse = new ArrayList<WeekwiseData>();
							for (int i = 1; i < 54; i++) {
								WeekwiseData wdr = new WeekwiseData();
								wdr.setWeekNumber(String.valueOf(i));
								wdr.setWeeklyPlannedProduction(String.valueOf(String.valueOf(0)));
								wdr.setWeeklyActualProduction(String.valueOf(String.valueOf(0)));

								weeklyplannedResponse.add(wdr);
							}

							while (rs.next()) {
								weeklyplannedResponse.get(Integer.valueOf(rs.getString(1)) - 1)
										.setWeeklyPlannedProduction(rs.getString(2));

							}

							return weeklyplannedResponse;
						}

					});

			query = "select weekofyear(packing_completed_date) as weeks,"
					+ "  round(sum(actual_quantity_afterPacking)/1000,2) as quant"
					+ " from processorder_pckg where status in(2,14,17,18) and year(packing_completed_date)=" + year
					+ " group by weeks order by weeks";

			List<WeekwiseData> weeklyactualResponse = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<WeekwiseData>>() {
						public List<WeekwiseData> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<WeekwiseData> weeklyactualResponse = new ArrayList<WeekwiseData>();
							while (rs.next()) {
								weeklyplannedResponse.get(Integer.valueOf(rs.getString(1)) - 1)
										.setWeeklyActualProduction(rs.getString(2));
							}

							return weeklyactualResponse;
						}

					});

			List<WeekwiseData> q1 = new ArrayList<WeekwiseData>();
			List<WeekwiseData> q2 = new ArrayList<WeekwiseData>();
			List<WeekwiseData> q3 = new ArrayList<WeekwiseData>();
			List<WeekwiseData> q4 = new ArrayList<WeekwiseData>();

			for (WeekwiseData e : weeklyplannedResponse) {
				if (Integer.parseInt(e.getWeekNumber()) <= 13) {
					q1.add(e);

				}
				if (Integer.parseInt(e.getWeekNumber()) > 13 && Integer.parseInt(e.getWeekNumber()) < 27) {
					q2.add(e);
				}
				if (Integer.parseInt(e.getWeekNumber()) >= 27 && Integer.parseInt(e.getWeekNumber()) < 40) {
					q3.add(e);
				}
				if (Integer.parseInt(e.getWeekNumber()) >= 40 && Integer.parseInt(e.getWeekNumber()) < 54) {
					q4.add(e);
				}
			}
			plannedResponse.get(0).setWeekwisedata(q1);
			plannedResponse.get(1).setWeekwisedata(q2);
			plannedResponse.get(2).setWeekwisedata(q3);
			plannedResponse.get(3).setWeekwisedata(q4);

			response.setResponse(plannedResponse);

			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
