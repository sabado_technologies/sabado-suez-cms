package com.daoImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.WarehousePendingOrdersDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.Reactor;
import com.entity.Status;
import com.response.StatusResponse;
@Repository
public class WarehousePendingOrdersDaoImpl extends NamedParameterJdbcDaoSupportClass implements WarehousePendingOrdersDao {

	@Override
	public List<ProcessorderProd> getPendingOrders() {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		try {

			String query = "SELECT p.processorder_prod_number,p.product_number,p.batch_date,\r\n"
					+ "p.batch_number,p.total_quantity,pt.product_description,s.status_description\r\n"
					+ " from processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status where p.status = 7 "
					+ "and MONTH(p.batch_date) = MONTH(' " + timeStamp + " ') order by p.batch_date,p.completed_time";

			List<ProcessorderProd> listOfUser = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								String reversed = reverseDate(rs.getString(3));
								prod.setBatchDate(reversed);
								prod.setBatchNumber(rs.getString(4));
								prod.setTotalQuantity(rs.getString(5));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(6));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return listOfUser;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Reactor> getAllReactors() {
		try {
			String query = "SELECT reactor_name,max_threashold FROM reactor";

			List<Reactor> reactors = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<Reactor>>() {
						public List<Reactor> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<Reactor> list = new ArrayList<Reactor>();
							while (rs.next()) {

								Reactor r = new Reactor();
								r.setReactorName(rs.getString(1));
								r.setMaxThreashold(rs.getString(2));
								list.add(r);
							}
							return list;
						}
					});
			return reactors;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String UpdateReactor(Reactor reactor) {
		try {

			String query = "UPDATE reactor SET max_threashold = :threashold WHERE reactor_name = :reactor ;";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("threashold", reactor.getMaxThreashold());
			param.addValue("reactor", reactor.getReactorName());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return reactor.getReactorName();
	}

	@Override
	public List<ProcessorderProd> getAllOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number ,pt.product_description,\r\n"
					+ "s.status_description ,p.batch_date,DATEDIFF(p.batch_date,'" + timeStamp
					+ "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status where (p.status=7 or p.status=8)\r\n"
					+ "and YEARWEEK(p.batch_date, 1) = YEARWEEK(' " + timeStamp + " ',1)\r\n "
					+ "order by diffs asc,p.processorder_prod_number asc";
			// + "and MONTH(p.batch_date) = MONTH(' "+timeStamp+" ') order by
			// diffs desc";

			List<ProcessorderProd> allorders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								String reversedDate = reverseDate(rs.getString(5));
								prod.setBatchDate(reversedDate);
								prod.setVolume(rs.getString(6));
								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(4));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return allorders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getTodaysOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where (p.status=7 or p.status=8 ) and p.batch_date='"
					+ timeStamp + "' order by p.processorder_prod_number asc";

			List<ProcessorderProd> todays = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return todays;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getPreviousOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status,p.is_reworked FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where (p.status=7 or p.status=8) and p.batch_date <'"
					+ timeStamp + "' order by diffs,p.processorder_prod_number asc";

			List<ProcessorderProd> previous = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));
								prod.setIsApproved(rs.getInt(8));
								//System.out.println("Is reworked =>" + rs.getInt(8) +"Process order" + rs.getLong(1));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return previous;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getFutureOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where p.status=7 and p.batch_date >'"
					+ timeStamp + "' \r\n" + "and YEARWEEK(p.batch_date, 1) = YEARWEEK(' " + timeStamp + " ',1) \r\n"
					+ "order by diffs,p.processorder_prod_number asc";

			List<ProcessorderProd> future = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return future;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getTodaysCompletedOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where p.status =2 and p.completed_date='"
					+ timeStamp + "' order by diffs desc,p.processorder_prod_number asc";

			List<ProcessorderProd> completed = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return completed;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public StatusResponse getChargedStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=15\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setRmChargedTime(rs.getString(1));
						s.setRmChargedDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getPackedStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=14\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setPackedTime(rs.getString(1));
						s.setPackedDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getQualityCheckStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber + "' and pt.status_id=18\r\n" 
					+ "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQualityCheckTime(rs.getString(1));
						s.setQualityCheckDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getQualityInspectionStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=17\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQulityInspectionTime(rs.getString(1));
						s.setQualityInspectionDate(rs.getString(2));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getInventoryCheckStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid  \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber	+ "' and pt.status_id=2\r\n" 
					+ "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setInventoryTime(rs.getString(1));
						s.setInventoryDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getRmInventoryStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber +"' \r\n"
					+ " and pt.status_id=16\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setInventoryTime(rs.getString(1));
						s.setInventoryDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	@Override
	public StatusResponse getStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {
			
			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,\r\n"
					+ "pt.ssoid,u.user_name FROM process_tracker pt\r\n"
					+ "inner join user u on u.ssoid = pt.ssoid\r\n"
					+ "where pt.processorder_number='" + processorderProdNumber +"' and pt.status_id=8\r\n"
					+ "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			/*String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=8\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";*/

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						// s.setRmChargedTime(rs.getString(1));
						s.setRmIssuedTime(rs.getString(1));
						s.setRmIssuedDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query1 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=4\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query1, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setRmChargedTime(rs.getString(1));
						s.setRmChargedDate(rs.getString(2));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query2 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=5\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query2, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setPackedTime(rs.getString(1));
						s.setPackedDate(rs.getString(2));
					}

					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query3 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=18\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query3, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQualityCheckTime(rs.getString(1));
						s.setQualityCheckDate(rs.getString(2));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query4 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=17\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query4, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQulityInspectionTime(rs.getString(1));
						s.setQualityInspectionDate(rs.getString(2));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query5 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=16\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query5, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setInventoryTime(rs.getString(1));
						s.setInventoryDate(rs.getString(2));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}

}
