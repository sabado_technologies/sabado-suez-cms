package com.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.dao.PendingOrdersDao;
import com.daoSupport.NamedParameterJdbcDaoSupportClass;
import com.entity.ContainerTable;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.ProductTable;
import com.entity.Reactor;
import com.entity.Role;
import com.entity.Status;
import com.entity.User;
import com.response.PackingReportResponse;
import com.response.StatusResponse;

@Repository
public class PendingOrdersDaoImpl extends NamedParameterJdbcDaoSupportClass implements PendingOrdersDao {

	@Override
	public List<ProcessorderProd> getPendingOrders() {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		try {
			
			String query = "SELECT p.processorder_prod_number,p.product_number,p.batch_date,\r\n"
					+ "p.batch_number,p.total_quantity,pt.product_description,s.status_description,\r\n"
					+ "DATE_FORMAT(p.completed_time, '%W %D %M %Y') As completedate,\r\n"
					+ "DATE_FORMAT(p.completed_time, '%r') As completetime, \r\n"
					+ "DATE_FORMAT(p.packed_time, '%W %D %M %Y') As packeddate,\r\n"
					+ "DATE_FORMAT(p.packed_time, '%r') As packedtime from processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status where p.status not in (7,8,15,18) and MONTH(p.packed_date) = MONTH(' " + timeStamp + " ') \r\n"
					+ "order by p.packed_time desc";

			/*String query = "SELECT p.processorder_prod_number,p.product_number,p.batch_date,\r\n"
					+ "p.batch_number,p.total_quantity,pt.product_description,s.status_description\r\n"
					+ " from processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status where p.status =2 "
					+ "and MONTH(p.batch_date) = MONTH(' " + timeStamp + " ') order by p.completed_time desc,p.batch_date";*/

			List<ProcessorderProd> listOfUser = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								String reversed = reverseDate(rs.getString(3));
								prod.setBatchDate(reversed);
								prod.setBatchNumber(rs.getString(4));
								prod.setTotalQuantity(rs.getString(5));
								prod.setCompletedDate(rs.getString(8));
								prod.setCompletedTime(rs.getString(9));
								prod.setPackedDate(rs.getString(10));
								prod.setPackedTime(rs.getString(11));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(6));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return listOfUser;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Reactor> getAllReactors() {
		try {
			String query = "SELECT reactor_name,max_threashold FROM reactor";

			List<Reactor> reactors = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<Reactor>>() {
						public List<Reactor> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<Reactor> list = new ArrayList<Reactor>();
							while (rs.next()) {

								Reactor r = new Reactor();
								r.setReactorName(rs.getString(1));
								r.setMaxThreashold(rs.getString(2));
								list.add(r);
							}
							return list;
						}
					});
			return reactors;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String UpdateReactor(Reactor reactor) {
		try {

			String query = "UPDATE reactor SET max_threashold = :threashold WHERE reactor_name = :reactor ;";

			MapSqlParameterSource param = new MapSqlParameterSource();

			param.addValue("threashold", reactor.getMaxThreashold());
			param.addValue("reactor", reactor.getReactorName());
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return reactor.getReactorName();
	}

	@Override
	public List<ProcessorderProd> getAllOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number ,pt.product_description,\r\n"
					+ "s.status_description ,p.batch_date,DATEDIFF(p.batch_date,'" + timeStamp
					+ "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status where p.status !=2\r\n"
					+ "and YEARWEEK(p.batch_date, 1) = YEARWEEK(' " + timeStamp + " ',1)\r\n "
					+ "order by diffs asc,p.processorder_prod_number asc";
			// + "and MONTH(p.batch_date) = MONTH(' "+timeStamp+" ') order by
			// diffs desc";

			List<ProcessorderProd> allorders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								String reversedDate = reverseDate(rs.getString(5));
								prod.setBatchDate(reversedDate);
								prod.setVolume(rs.getString(6));
								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(4));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return allorders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getTodaysOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where p.status !=2 and p.batch_date='"
					+ timeStamp + "' order by p.processorder_prod_number asc";

			List<ProcessorderProd> todays = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return todays;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getPreviousOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status,p.is_reworked FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where p.status !=2 and p.batch_date <'"
					+ timeStamp + "' order by diffs,p.processorder_prod_number asc";

			List<ProcessorderProd> previous = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));
								prod.setIsApproved(rs.getInt(8));
								////System.out.println("Is reworked =>" + rs.getInt(8) +"Process order" + rs.getLong(1));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return previous;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getFutureOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where p.status !=2 and p.batch_date >'"
					+ timeStamp + "' \r\n" + "and YEARWEEK(p.batch_date, 1) = YEARWEEK(' " + timeStamp + " ',1) \r\n"
					+ "order by diffs,p.processorder_prod_number asc";

			List<ProcessorderProd> future = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);
								list.add(prod);
							}
							return list;
						}
					});
			return future;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getTodaysCompletedOrders() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			String query = "SELECT p.processorder_prod_number, p.product_number,\r\n"
					+ "pt.product_description,p.batch_date,s.status_description,\r\n" + "DATEDIFF(p.batch_date,'"
					+ timeStamp + "') AS Diffs,p.status FROM processorder_prod p\r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number\r\n"
					+ "inner join status s on  s.status_id = p.status\r\n" + "where p.status =2 and p.completed_date='"
					+ timeStamp + "' order by diffs desc,p.processorder_prod_number asc";

			List<ProcessorderProd> completed = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {

								ProcessorderProd prod = new ProcessorderProd();
								prod.setProcessorderProdNumber(rs.getLong(1));
								prod.setBatchDate(rs.getString(4));
								prod.setVolume(rs.getString(6));

								ProductTable pt = new ProductTable();
								pt.setProductNumber(rs.getLong(2));
								pt.setProductDescription(rs.getString(3));
								prod.setProductTable(pt);

								Status s = new Status();
								s.setStatusDescription(rs.getString(5));
								s.setStatusId(rs.getInt(7));
								prod.setStatus(s);

								list.add(prod);
							}
							return list;
						}
					});
			return completed;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public StatusResponse getChargedStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=15\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setRmChargedTime(rs.getString(1));
						s.setRmChargedDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getPackedStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=14\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setPackedTime(rs.getString(1));
						s.setPackedDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getQualityCheckStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber + "' and pt.status_id=18\r\n" 
					+ "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQualityCheckTime(rs.getString(1));
						s.setQualityCheckDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getQualityInspectionStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=17\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQulityInspectionTime(rs.getString(1));
						s.setQualityInspectionDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getInventoryCheckStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid  \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber	+ "' and pt.status_id=2\r\n" 
					+ "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setInventoryTime(rs.getString(1));
						s.setInventoryDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public StatusResponse getRmInventoryStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {

			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,pt.ssoid,u.user_name\r\n"
					+ "FROM process_tracker pt inner join user u on u.ssoid = pt.ssoid \r\n"
					+ "where pt.processorder_number='" + processorderProdNumber +"' \r\n"
					+ " and pt.status_id=16\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setInventoryTime(rs.getString(1));
						s.setInventoryDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	@Override
	public StatusResponse getStatus(long processorderProdNumber) {
		StatusResponse status = null;
		try {
			
			String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate,\r\n"
					+ "pt.ssoid,u.user_name FROM process_tracker pt\r\n"
					+ "inner join user u on u.ssoid = pt.ssoid\r\n"
					+ "where pt.processorder_number='" + processorderProdNumber +"' and pt.status_id=8\r\n"
					+ "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			/*String query = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=8\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";*/

			status = getNamedParameterJdbcTemplate().query(query, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						// s.setRmChargedTime(rs.getString(1));
						s.setRmIssuedTime(rs.getString(1));
						s.setRmIssuedDate(rs.getString(2));
						s.setSso(rs.getString(3));
						s.setUserName(rs.getString(4));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query1 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=4\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query1, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setRmChargedTime(rs.getString(1));
						s.setRmChargedDate(rs.getString(2));
					}
					return s;
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query2 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=5\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query2, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setPackedTime(rs.getString(1));
						s.setPackedDate(rs.getString(2));
					}

					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query3 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=18\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query3, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQualityCheckTime(rs.getString(1));
						s.setQualityCheckDate(rs.getString(2));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query4 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=17\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query4, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setQulityInspectionTime(rs.getString(1));
						s.setQualityInspectionDate(rs.getString(2));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			String query5 = "SELECT DATE_FORMAT(pt.process_end_time, '%r') As rmtime,\r\n"
					+ "DATE_FORMAT(pt.process_end_time, '%W %D %M %Y') As rmdate\r\n"
					+ "FROM process_tracker pt where pt.processorder_number='" + processorderProdNumber
					+ "' and pt.status_id=16\r\n" + "ORDER BY pt.process_tracker_id DESC LIMIT 1";

			status = getNamedParameterJdbcTemplate().query(query5, new ResultSetExtractor<StatusResponse>() {
				public StatusResponse extractData(ResultSet rs) throws SQLException, DataAccessException {

					StatusResponse s = new StatusResponse();

					while (rs.next()) {
						s.setInventoryTime(rs.getString(1));
						s.setInventoryDate(rs.getString(2));
					}
					return s;
				}
			});
			 return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public String reverseDate(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1] + "-" + splited[splited.length - 2] + "-" + splited[0];

		return reversedDate;

	}
	
	public String reverseDate1(String date) {
		String[] splited = date.split("-");
		String reversedDate = splited[splited.length - 1]  + splited[splited.length - 2] + splited[0];

		return reversedDate;

	}

	@Override
	public List<StatusResponse> getStatusInfo() {
		
		try {
			
						
			String query = "SELECT s.processorder_prodNo,up.user_name as uploadedBy,ur.user_name as rm_issued_by,\r\n"
					+ "uc.user_name as rm_charged_by,uq.user_name as qualityCheckBy,upc.user_name as packedBy,\r\n"
					+ "upo.user_name as rmPostBy,uqi.user_name as inspectedBy,uic.user_name as inventoryCheckBy,\r\n"
					+ "uip.user_name as inventoryPostBy,DATE_FORMAT(s.rm_issue, '%r') As rmIssueTime,\r\n"
					+ "DATE_FORMAT(s.upload, '%r') As uploadTime,\r\n "
					+ "DATE_FORMAT(s.upload, '%W %D %M %Y') As uploadDate,\r\n "
					+ "DATE_FORMAT(s.rm_issue, '%W %D %M %Y') As rmIssueDate,\r\n"
					+ "DATE_FORMAT(s.rm_charge, '%r') As rmChargeTime,\r\n"
					+ "DATE_FORMAT(s.rm_charge, '%W %D %M %Y') As rmChargeDate,\r\n"
					+ "DATE_FORMAT(s.quality_check, '%r') As qualityCheckTime,\r\n"
					+ "DATE_FORMAT(s.quality_check, '%W %D %M %Y') As qualityCheckDate,\r\n"
					+ "DATE_FORMAT(s.packing, '%r') As packTime,DATE_FORMAT(s.packing, '%W %D %M %Y') As packDate,\r\n"
					+ "DATE_FORMAT(s.rm_posting, '%r') As rmPostTime,DATE_FORMAT(s.rm_posting, '%W %D %M %Y') As rmPostDate,\r\n"
					+ "DATE_FORMAT(s.quality_inspection, '%r') As qualityInspectionTime,\r\n"
					+ "DATE_FORMAT(s.quality_inspection, '%W %D %M %Y') As qualityInspectionDate,\r\n"
					+ "DATE_FORMAT(s.inventory_check, '%r') As inventoryCheckTime,\r\n"
					+ "DATE_FORMAT(s.inventory_check, '%W %D %M %Y') As inventoryCheckDate,\r\n"
					+ "DATE_FORMAT(s.inventory_posting, '%r') As inventoryPostTime,\r\n"
					+ "DATE_FORMAT(s.inventory_posting, '%W %D %M %Y') As inventoryPostDate,\r\n"
					+ "p.batch_date,pt.product_description,p.is_selected,s.status_id FROM suez.status_info s \r\n"
					+ "left join user up on up.ssoid=s.uploaded_by \r\n"
					+ "left join user ur on ur.ssoid=s.rm_issued_by \r\n"
					+ "left join user uc on uc.ssoid=s.rm_charged_by \r\n"
					+ "left join user uq on uq.ssoid=s.quality_checked_by \r\n"
					+ "left join user upc on upc.ssoid=s.packed_by \r\n"
					+ "left join user upo on upo.ssoid=s.rm_posted_by \r\n"
					+ "left join user uqi on uqi.ssoid=s.quality_inspected_by \r\n"
					+ "left join user uic on uic.ssoid=s.inventory_checked_by \r\n"
					+ "left join user uip on uip.ssoid=s.inventory_posted_by \r\n"
					+ "inner join processorder_prod p on p.processorder_prod_number=s.processorder_prodNo \r\n"
					+ "inner join product_table pt on pt.product_number=p.product_number \r\n"
					+ "order by s.processorder_prodNo desc";

			List<StatusResponse> statusInfo = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<StatusResponse>>() {
						public List<StatusResponse> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<StatusResponse> list = new ArrayList<StatusResponse>();
							while (rs.next()) {

								StatusResponse s = new StatusResponse();
								s.setProcessorderProdNumber(rs.getLong(1));
								s.setUploadedBy(rs.getString(2));
								s.setRmIssuedBy(rs.getString(3));
								s.setRmChargedBy(rs.getString(4));
								s.setQualityCheckedBy(rs.getString(5));
								s.setPackedBy(rs.getString(6));
								s.setRmPostedBy(rs.getString(7));
								s.setQualityInspectedBy(rs.getString(8));
								s.setInventoryBy(rs.getString(9));
								s.setInventoryPostedBy(rs.getString(10));
								s.setUploadedTime(rs.getString(12));
								s.setUploadedDate(rs.getString(13));
								s.setRmIssuedTime(rs.getString(11));
								s.setRmIssuedDate(rs.getString(14));
								s.setRmChargedTime(rs.getString(15));
								s.setRmChargedDate(rs.getString(16));
								s.setQualityCheckTime(rs.getString(17));
								s.setQualityCheckDate(rs.getString(18));
								s.setPackedTime(rs.getString(19));
								s.setPackedDate(rs.getString(20));
								s.setRmPostTime(rs.getString(21));
								s.setRmPostDate(rs.getString(22));
								s.setQulityInspectionTime(rs.getString(23));
								s.setQualityInspectionDate(rs.getString(24));
								s.setInventoryTime(rs.getString(25));
								s.setInventoryDate(rs.getString(26));
								s.setInventoryPostTime(rs.getString(27));
								s.setInventoryPostDate(rs.getString(28));
								String reversedDate = reverseDate(rs.getString(29));
								s.setBatchDate(reversedDate);
								s.setMaterialMemo(rs.getString(30));
								s.setIsSelected(rs.getInt(31));
								s.setStatusId(rs.getInt(32));
								list.add(s);
							}
							return list;
						}
					});
			return statusInfo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		//return null;
	}

	@Override
	public List<StatusResponse> getRepackingStatusInfo() {
		try {
			
			
			String query = "SELECT s.processorder_packNo,up.user_name as uploadedBy,ur.user_name as rm_issued_by,\r\n"
					+ "uc.user_name as rm_charged_by,uq.user_name as qualityCheckBy,upc.user_name as packedBy,\r\n"
					+ "upo.user_name as rmPostBy,uqi.user_name as inspectedBy,uic.user_name as inventoryCheckBy,\r\n"
					+ "uip.user_name as inventoryPostBy,DATE_FORMAT(s.rm_issue, '%r') As rmIssueTime,\r\n"
					+ "DATE_FORMAT(s.upload, '%r') As uploadTime,\r\n "
					+ "DATE_FORMAT(s.upload, '%W %D %M %Y') As uploadDate,\r\n "
					+ "DATE_FORMAT(s.rm_issue, '%W %D %M %Y') As rmIssueDate,\r\n"
					+ "DATE_FORMAT(s.rm_charge, '%r') As rmChargeTime,\r\n"
					+ "DATE_FORMAT(s.rm_charge, '%W %D %M %Y') As rmChargeDate,\r\n"
					+ "DATE_FORMAT(s.quality_check, '%r') As qualityCheckTime,\r\n"
					+ "DATE_FORMAT(s.quality_check, '%W %D %M %Y') As qualityCheckDate,\r\n"
					+ "DATE_FORMAT(s.packing, '%r') As packTime,DATE_FORMAT(s.packing, '%W %D %M %Y') As packDate,\r\n"
					+ "DATE_FORMAT(s.rm_posting, '%r') As rmPostTime,DATE_FORMAT(s.rm_posting, '%W %D %M %Y') As rmPostDate,\r\n"
					+ "DATE_FORMAT(s.quality_inspection, '%r') As qualityInspectionTime,\r\n"
					+ "DATE_FORMAT(s.quality_inspection, '%W %D %M %Y') As qualityInspectionDate,\r\n"
					+ "DATE_FORMAT(s.inventory_check, '%r') As inventoryCheckTime,\r\n"
					+ "DATE_FORMAT(s.inventory_check, '%W %D %M %Y') As inventoryCheckDate,\r\n"
					+ "DATE_FORMAT(s.inventory_posting, '%r') As inventoryPostTime,\r\n"
					+ "DATE_FORMAT(s.inventory_posting, '%W %D %M %Y') As inventoryPostDate,\r\n"
					+ "p.batch_date,p.material_memo,p.is_selected,s.status_id FROM repacking_status_info s \r\n"
					+ "left join user up on up.ssoid=s.uploaded_by \r\n"
					+ "left join user ur on ur.ssoid=s.rm_issued_by \r\n"
					+ "left join user uc on uc.ssoid=s.rm_charged_by \r\n"
					+ "left join user uq on uq.ssoid=s.quality_checked_by \r\n"
					+ "left join user upc on upc.ssoid=s.packed_by \r\n"
					+ "left join user upo on upo.ssoid=s.rm_posted_by \r\n"
					+ "left join user uqi on uqi.ssoid=s.quality_inspected_by \r\n"
					+ "left join user uic on uic.ssoid=s.inventory_checked_by \r\n"
					+ "left join user uip on uip.ssoid=s.inventory_posted_by \r\n"
					+ "inner join processorder_pckg p on p.processorder_pckg_number=s.processorder_packNo \r\n"
					+ "order by s.processorder_packNo desc";

			List<StatusResponse> statusInfo = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<StatusResponse>>() {
						public List<StatusResponse> extractData(ResultSet rs) throws SQLException, DataAccessException {

							List<StatusResponse> list = new ArrayList<StatusResponse>();
							while (rs.next()) {

								StatusResponse s = new StatusResponse();
								s.setProcessorderProdNumber(rs.getLong(1));
								s.setUploadedBy(rs.getString(2));
								s.setRmIssuedBy(rs.getString(3));
								s.setRmChargedBy(rs.getString(4));
								s.setQualityCheckedBy(rs.getString(5));
								s.setPackedBy(rs.getString(6));
								s.setRmPostedBy(rs.getString(7));
								s.setQualityInspectedBy(rs.getString(8));
								s.setInventoryBy(rs.getString(9));
								s.setInventoryPostedBy(rs.getString(10));
								s.setUploadedTime(rs.getString(12));
								s.setUploadedDate(rs.getString(13));
								s.setRmIssuedTime(rs.getString(11));
								s.setRmIssuedDate(rs.getString(14));
								s.setRmChargedTime(rs.getString(15));
								s.setRmChargedDate(rs.getString(16));
								s.setQualityCheckTime(rs.getString(17));
								s.setQualityCheckDate(rs.getString(18));
								s.setPackedTime(rs.getString(19));
								s.setPackedDate(rs.getString(20));
								s.setRmPostTime(rs.getString(21));
								s.setRmPostDate(rs.getString(22));
								s.setQulityInspectionTime(rs.getString(23));
								s.setQualityInspectionDate(rs.getString(24));
								s.setInventoryTime(rs.getString(25));
								s.setInventoryDate(rs.getString(26));
								s.setInventoryPostTime(rs.getString(27));
								s.setInventoryPostDate(rs.getString(28));
								String reversedDate = reverseDate(rs.getString(29));
								s.setBatchDate(reversedDate);
								s.setMaterialMemo(rs.getString(30));
								s.setIsSelected(rs.getInt(31));
								s.setStatusId(rs.getInt(32));
								list.add(s);
							}
							return list;
						}
					});
			return statusInfo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderProd> getProcessOrderHistory() {
		try {

			String query = "SELECT p.processorder_prod_number ,prod.product_number,prod.product_description,p.batch_number,date_format(p.batch_date,'%d-%m-%Y'),s.status_description,p.total_quantity,p.batch_date\r\n"
					+ "from processorder_prod p inner join product_table prod on prod.product_number=p.product_number\r\n"
					+ "inner join status s on s.status_id=p.status\r\n"
					+ "order by p.processorder_prod_number desc";

			List<ProcessorderProd> listOfQualityCheckProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderProd>>() {
						public List<ProcessorderProd> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<ProcessorderProd> list = new ArrayList<ProcessorderProd>();

							while (rs.next()) {
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(1));
								ProductTable prod = new ProductTable();
								prod.setProductNumber(rs.getLong(2));
								prod.setProductDescription(rs.getString(3));
								processOrderProd.setProductTable(prod);
								processOrderProd.setBatchNumber(rs.getString(4));
								processOrderProd.setBatchDate(rs.getString(5));
								String changeDate = reverseDate1(rs.getString(8));
								processOrderProd.setPdfDate(changeDate);
								Status s = new Status();
								s.setStatusDescription(rs.getString(6));
								processOrderProd.setStatus(s);
								processOrderProd.setTotalQuantity(rs.getString(7));
								list.add(processOrderProd);
							}
							return list;
						}
					});
			return listOfQualityCheckProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ProcessorderPckg> getRepackingProcessOrderHistory() {
		List<ProcessorderPckg> processOrderPack = null;
		String query = "SELECT p.processorder_pckg_number,p.product_number,p.material_memo,p.batch_number, date_format(p.batch_date,'%d-%m-%Y'),p.total_quantity,\r\n" + 
				"p.wght_pckg,p.status,p.container_number,p.volume,p.sixml_number,p.is_associated,\r\n" + 
				"p.process_quant,p.processorder_prod_number,p.product_name,c.container_description ,p.quantity_after_packing ,p.location_after_packing, s.status_description, p.batch_date \r\n" + 
				"FROM processorder_pckg p inner join container_table c on p.container_number = c.container_number \r\n"
				+ "inner join status s on p.status = s.status_id \r\n" + 
				" where is_repack = 1 order by p.processorder_pckg_number desc";

		try {

			processOrderPack = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<ProcessorderPckg>>() {
						public List<ProcessorderPckg> extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							List<ProcessorderPckg> list = new ArrayList<ProcessorderPckg>();
							while (rs.next()) {
								ProcessorderPckg processorderPckg = new ProcessorderPckg();
								processorderPckg.setProcessorderPckgNumber(rs.getLong(1));
								processorderPckg.setProductNumber(rs.getLong(2));
								processorderPckg.setBatchNumber(rs.getString(4));
								processorderPckg.setBatchDate(rs.getString(5));
								String dateChanged = reverseDate1(rs.getString(20));
								processorderPckg.setPdfDate(dateChanged);
								processorderPckg.setTotalQuantity(rs.getLong(6));
								processorderPckg.setWghtPckg(rs.getLong(7));
								Status s = new Status();
								s.setStatusId(rs.getInt(8));
								s.setStatusDescription(rs.getString(19));
								processorderPckg.setStatus(s);
								ContainerTable ct = new ContainerTable();
								ct.setContainerNumber(rs.getLong(9));
								ct.setContainerDescription(rs.getString(16));
								processorderPckg.setContainerTable(ct);
								processorderPckg.setVolume(rs.getString(10));
								processorderPckg.setSixmlNumber(rs.getString(11));
								processorderPckg.setIsAssociated(rs.getInt(12));
								processorderPckg.setMaterialMemo(rs.getString(3));
								processorderPckg.setProcessQuant(rs.getString(13));
								ProcessorderProd processOrderProd = new ProcessorderProd();
								processOrderProd.setProcessorderProdNumber(rs.getLong(14));
								processorderPckg.setProcessorderProd(processOrderProd);
								processorderPckg.setProductName(rs.getString(15));
								processorderPckg.setQuantityAfterPacking(rs.getString(17));
								processorderPckg.setLocationAfterPacking(rs.getString(18));

								list.add(processorderPckg);
							}
							return list;

						}
					});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return processOrderPack;
	}

	@Override
	public int addExtraRm(long processOrder, String rmDetails) {
		try {
			String query = "UPDATE processorder_prod set extra_rm=:rmInfo where processorder_prod_number=:processOrderNo";

			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("processOrderNo", processOrder);
			param.addValue("rmInfo", rmDetails);
			getNamedParameterJdbcTemplate().update(query, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<PackingReportResponse> repackingTime(Long processorderProdNumber) {
		try {

			String query = "select distinct processorder_pckg_number, processorder_prod_number,"
					+ " date_format(packing_start_time,'%d-%m-%Y %H:%i:%s  %p'),date_format(packing_end_time,'%d-%m-%Y %h:%i:%s %p'), "
					+ " round(time_to_sec(abs(timediff(packing_end_time,packing_start_time) ))/60,2) as packingLeadTime ,"
					+ " sixml_number,actual_quantity_afterPacking,quantity_after_packing from processorder_pckg\r\n" + 
					"where processorder_pckg_number="+processorderProdNumber+" \r\n" + 
					"group by processorder_pckg_number";

			List<PackingReportResponse> dailyProcessOrders = getNamedParameterJdbcTemplate().query(query,
					new ResultSetExtractor<List<PackingReportResponse>>() {
						public List<PackingReportResponse> extractData(ResultSet rs)
								throws SQLException, DataAccessException {

							List<PackingReportResponse> list = new ArrayList<PackingReportResponse>();

							while (rs.next()) {
								PackingReportResponse processorderprod = new PackingReportResponse();
								processorderprod.setProcessorderPackageNumber(rs.getLong(1));
								processorderprod.setProcessorderProdNumber(rs.getLong(2));
								processorderprod.setPackingStartTime(rs.getString(3));
								processorderprod.setPackingEndTime(rs.getString(4));
								processorderprod.setPackingLeadTime(rs.getString(5));
								processorderprod.setSixMillionNumber(rs.getLong(6));
								processorderprod.setActualQuantityAfterPacking(rs.getString(7));
								processorderprod.setPacketSizeAfterPacking(rs.getInt(8));
								list.add(processorderprod);
							}
							return list;
						}
					});
			return dailyProcessOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
