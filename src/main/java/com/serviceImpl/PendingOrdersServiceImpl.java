package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PendingOrdersDao;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.Reactor;
import com.response.PackingReportResponse;
import com.response.StatusResponse;
import com.service.PendingOrdersService;

@Service
public class PendingOrdersServiceImpl implements PendingOrdersService{
	
	@Autowired
	PendingOrdersDao pendingOrdersDao;  

	@Override
	public List<ProcessorderProd> getPendingOrders() {
		List<ProcessorderProd> products =pendingOrdersDao.getPendingOrders();
		return products;
	}

	@Override
	public List<Reactor> getAllReactors() {
		List<Reactor> reactor =pendingOrdersDao.getAllReactors();
		return reactor;
	}

	@Override
	public String UpdateReactor(Reactor reactor) {
		return pendingOrdersDao.UpdateReactor(reactor);
	}

	@Override
	public List<ProcessorderProd> getAllOrders() {
		List<ProcessorderProd> orders = pendingOrdersDao.getAllOrders();
		return orders;
	}

	@Override
	public List<ProcessorderProd> getTodaysOrders() {
		List<ProcessorderProd> todays = pendingOrdersDao.getTodaysOrders();
		return todays;
	}

	@Override
	public List<ProcessorderProd> getPreviousOrders() {
		List<ProcessorderProd> previous = pendingOrdersDao.getPreviousOrders();
		return previous;
	}

	@Override
	public List<ProcessorderProd> getFutureOrders() {
		List<ProcessorderProd> future = pendingOrdersDao.getFutureOrders();
		return future;
	}
	
	@Override
	public List<ProcessorderProd> getTodaysCompletedOrders() {
		List<ProcessorderProd> completed = pendingOrdersDao.getTodaysCompletedOrders();
		return completed;
	}

	@Override
	public StatusResponse getStatus(long processorderProdNumber) {
		return pendingOrdersDao.getStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getChargedStatus(long processorderProdNumber) {
		return pendingOrdersDao.getChargedStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getPackedStatus(long processorderProdNumber) {
		return pendingOrdersDao.getPackedStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getQualityCheckStatus(long processorderProdNumber) {
		return pendingOrdersDao.getQualityCheckStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getQualityInspectionStatus(long processorderProdNumber) {
		return pendingOrdersDao.getQualityInspectionStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getInventoryCheckStatus(long processorderProdNumber) {
		return pendingOrdersDao.getInventoryCheckStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getRmInventoryStatus(long processorderProdNumber) {
		return pendingOrdersDao.getRmInventoryStatus(processorderProdNumber);
	}

	@Override
	public List<StatusResponse> getStatusInfo() {
		// TODO Auto-generated method stub
		return pendingOrdersDao.getStatusInfo();
	}

	@Override
	public List<StatusResponse> getRepackingStatusInfo() {
		// TODO Auto-generated method stub
		return pendingOrdersDao.getRepackingStatusInfo();
	}

	@Override
	public List<ProcessorderProd> getProcessOrderHistory() {
		// TODO Auto-generated method stub
		return pendingOrdersDao.getProcessOrderHistory();
	}

	@Override
	public List<ProcessorderPckg> getRepackingProcessOrderHistory() {
		// TODO Auto-generated method stub
		return pendingOrdersDao.getRepackingProcessOrderHistory();
	}

	@Override
	public int addExtraRm(long processOrder, String rmDetails) {
		// TODO Auto-generated method stub
		return pendingOrdersDao.addExtraRm(processOrder, rmDetails);
	}

	@Override
	public List<PackingReportResponse> repackingTime(Long processorderProdNumber) {
		// TODO Auto-generated method stub
		return pendingOrdersDao.repackingTime(processorderProdNumber);
	}

	
}
