package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.QualityResultsDao;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.service.QualityResultsService;

@Service
public class QualityResultsServiceImpl implements QualityResultsService{
	
	@Autowired
	QualityResultsDao qualityResultsDao;

	@Override
	public List<ProcessorderProd> getReworkDetails() {
		// TODO Auto-generated method stub
		return qualityResultsDao.getReworkDetails();
	}

	@Override
	public List<ProcessorderPckg> getPackingDetails(long processOrder) {
		// TODO Auto-generated method stub
		return qualityResultsDao.getPackingDetails(processOrder);
	}

	@Override
	public List<ProcessorderProd> getScrapDetails() {
		// TODO Auto-generated method stub
		return qualityResultsDao.getScrapDetails();
	}

	@Override
	public long approve(long processOrder, String remarks, String ssoid) {
		// TODO Auto-generated method stub
		return qualityResultsDao.approve(processOrder, remarks, ssoid);
	}

}
