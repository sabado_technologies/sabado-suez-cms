package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PoHomeDao;
import com.entity.ProcessorderProd;
import com.service.PoHomeService;

@Service
public class PoHomeServiceImpl implements PoHomeService {
	
	@Autowired
	PoHomeDao pendingOrdersDao;
	
	@Override
	public List<ProcessorderProd> getAllOrders() {
		// TODO Auto-generated method stub
		List<ProcessorderProd> orders = pendingOrdersDao.getAllOrders();
		return orders;
	}
	
	@Override
	public List<ProcessorderProd> getTodaysOrders() {
		// TODO Auto-generated method stub
		List<ProcessorderProd> todays = pendingOrdersDao.getTodaysOrders();
		return todays;
	}

	@Override
	public List<ProcessorderProd> getPreviousOrders() {
		// TODO Auto-generated method stub
		List<ProcessorderProd> previous = pendingOrdersDao.getPreviousOrders();
		return previous;
	}

	@Override
	public List<ProcessorderProd> getFutureOrders() {
		// TODO Auto-generated method stub
		List<ProcessorderProd> future = pendingOrdersDao.getFutureOrders();
		return future;
	}
	
	@Override
	public List<ProcessorderProd> getTodaysCompletedOrders() {
		// TODO Auto-generated method stub
		List<ProcessorderProd> completed = pendingOrdersDao.getTodaysCompletedOrders();
		return completed;
	}

}
