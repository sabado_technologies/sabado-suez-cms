package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.RepackForQualityDao;
import com.entity.ProcessorderPckg;
import com.service.RepackForQualityService;

@Service
public class RepackForQualityServiceImpl implements RepackForQualityService {
	
	@Autowired
	RepackForQualityDao repackForQualityDao;

	@Override
	public List<ProcessorderPckg> getRepackDetails() {
		// TODO Auto-generated method stub
		return repackForQualityDao.getRepackDetails();
	}

	@Override
	public long acceptQuality(long processorderProdNumber, String ssoid) {
		// TODO Auto-generated method stub
		return repackForQualityDao.acceptQuality(processorderProdNumber, ssoid);
	}

	@Override
	public long rejectQuality(long processorderProdNumber, String ssoid, String comment) {
		// TODO Auto-generated method stub
		return repackForQualityDao.rejectQuality(processorderProdNumber, ssoid, comment);
	}

	@Override
	public long scrapQuality(long processorderProdNumber, String ssoid, String comment) {
		// TODO Auto-generated method stub
		return repackForQualityDao.scrapQuality(processorderProdNumber, ssoid, comment);
	}

}
