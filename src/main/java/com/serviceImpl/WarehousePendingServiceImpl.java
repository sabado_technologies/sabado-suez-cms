package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PendingOrdersDao;
import com.dao.WarehousePendingOrdersDao;
import com.entity.ProcessorderProd;
import com.entity.Reactor;
import com.response.StatusResponse;
import com.service.WarehousePendingService;
@Service
public class WarehousePendingServiceImpl implements  WarehousePendingService {
 @Autowired
 WarehousePendingOrdersDao warehousePendingOrdersDao;  
 
 @Override
	public List<ProcessorderProd> getPendingOrders() {
		List<ProcessorderProd> products =warehousePendingOrdersDao.getPendingOrders();
		return products;
	}

	@Override
	public List<Reactor> getAllReactors() {
		List<Reactor> reactor =warehousePendingOrdersDao.getAllReactors();
		return reactor;
	}

	@Override
	public String UpdateReactor(Reactor reactor) {
		return warehousePendingOrdersDao.UpdateReactor(reactor);
	}

	@Override
	public List<ProcessorderProd> getAllOrders() {
		List<ProcessorderProd> orders = warehousePendingOrdersDao.getAllOrders();
		return orders;
	}

	@Override
	public List<ProcessorderProd> getTodaysOrders() {
		List<ProcessorderProd> todays = warehousePendingOrdersDao.getTodaysOrders();
		return todays;
	}

	@Override
	public List<ProcessorderProd> getPreviousOrders() {
		List<ProcessorderProd> previous = warehousePendingOrdersDao.getPreviousOrders();
		return previous;
	}

	@Override
	public List<ProcessorderProd> getFutureOrders() {
		List<ProcessorderProd> future = warehousePendingOrdersDao.getFutureOrders();
		return future;
	}
	
	@Override
	public List<ProcessorderProd> getTodaysCompletedOrders() {
		List<ProcessorderProd> completed = warehousePendingOrdersDao.getTodaysCompletedOrders();
		return completed;
	}

	@Override
	public StatusResponse getStatus(long processorderProdNumber) {
		return warehousePendingOrdersDao.getStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getChargedStatus(long processorderProdNumber) {
		return warehousePendingOrdersDao.getChargedStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getPackedStatus(long processorderProdNumber) {
		return warehousePendingOrdersDao.getPackedStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getQualityCheckStatus(long processorderProdNumber) {
		return warehousePendingOrdersDao.getQualityCheckStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getQualityInspectionStatus(long processorderProdNumber) {
		return warehousePendingOrdersDao.getQualityInspectionStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getInventoryCheckStatus(long processorderProdNumber) {
		return warehousePendingOrdersDao.getInventoryCheckStatus(processorderProdNumber);
	}

	@Override
	public StatusResponse getRmInventoryStatus(long processorderProdNumber) {
		return warehousePendingOrdersDao.getRmInventoryStatus(processorderProdNumber);
	}

}
