package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.ProductOperatorDao;
import com.entity.Alerts;
import com.entity.MultiRoles;
import com.entity.ProcessOrderRm;
import com.entity.ProcessTracker;
import com.entity.ProcessorderAssociatedPo;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.QualityCheckResult;
import com.entity.QualityMaster;
import com.entity.Reactor;
import com.entity.Remark;
import com.entity.Role;
import com.entity.User;
import com.response.ManagementReportResponse;
import com.response.ProcessOrderRMResponse;
import com.response.QualityCheckResponce;
import com.response.RepackingOrderRMResponse;
import com.service.ProductOperatorService;

@Service
public class ProductOperatorServiceImpl implements ProductOperatorService {

	@Autowired
	ProductOperatorDao productOperatorDao;

	public List<ProcessorderProd> displayProcessOrders() {
		return productOperatorDao.displayProcessOrders();
	}

	public List<ProcessOrderRMResponse> processOrderRecipeList(long a) {
		return productOperatorDao.processOrderRecipeList(a);
	}

	public int updateStatus(Long processOrder, int status, String sso) {
		return productOperatorDao.updateStatus(processOrder, status, sso);
	}

	public int updateProcessProgress(ProcessTracker processStatus) {
		return productOperatorDao.updateProcessProgress(processStatus);
	}

	@Override
	public List<User> listofPo() {
		return productOperatorDao.listofPo();
	}

	@Override
	public List<ProcessorderPckg> processOrderForPacking(long a) {
		return productOperatorDao.processOrderForPacking(a);
	}

	@Override
	public List<ProcessorderPckg> PackingOrderInfo(long a) {
		return productOperatorDao.PackingOrderInfo(a);
	}

	@Override
	public List<ProcessorderProd> AllProcessOrderList() {
		return productOperatorDao.AllProcessOrderList();
	}

	@Override
	public List<ManagementReportResponse> getWeeklyProcessOrders() {
		return productOperatorDao.getWeeklyProcessOrders();
	}

	@Override
	public int SaveprocessAssociatedwithpo(List<ProcessorderAssociatedPo> list) {
		return productOperatorDao.SaveprocessAssociatedwithpo(list);
	}

	@Override
	public int updateStatusForPacking(Long processOrder, int status, String sso, String quantity, int pails, String remarks) {
		//System.out.println("Service called");
		return productOperatorDao.updateStatusForPacking(processOrder, status, sso, quantity, pails, remarks);
	}

	@Override
	public int updateStatusForRM(Long processOrder, int rMNumber, int status) {
		return productOperatorDao.updateStatusForRM(processOrder, rMNumber, status);
	}

	@Override
	public List<QualityCheckResponce> getQualityData(long productNo) {
		return productOperatorDao.getQualityData(productNo);
	}

	@Override
	public int SaveQualityInfo(List<QualityCheckResponce> list) {
		return productOperatorDao.SaveQualityInfo(list);
	}

	@Override
	public int updateProcessOrderProd(Long processOrder, String reactor, int status) {
		return productOperatorDao.updateProcessOrderProd(processOrder, reactor, status);
	}

	@Override
	public List<ProcessorderProd> displayProcessOrdersforInventory() {
		return productOperatorDao.displayProcessOrdersforInventory();
	}

	@Override
	public List<ProcessorderProd> dailyProcessOrder() {
		return productOperatorDao.dailyProcessOrder();
	}

	@Override
	public int saveAlert(Alerts alert) {
		// TODO Auto-generated method stub
		return productOperatorDao.saveAlert(alert);
	}

	@Override
	public List<Reactor> getReactorDetails() {
		// TODO Auto-generated method stub
		return productOperatorDao.getReactorDetails();
	}

	@Override
	public List<MultiRoles> getRoleList(String sSO) {
		// TODO Auto-generated method stub
		return productOperatorDao.getRoleList(sSO);
	}

	@Override
	public int InsertRemark(Remark remark) {
		// TODO Auto-generated method stub
		return productOperatorDao.InsertRemark(remark);
	}

	@Override
	public int updateTracker(Long processOrder, int status) {
		// TODO Auto-generated method stub
		return productOperatorDao.updateTracker(processOrder,status);
	}

	@Override
	public List<ProcessorderPckg> reapckingOrderDiaplay() {
		// TODO Auto-generated method stub
		return  productOperatorDao.reapckingOrderDiaplay();
	}

	@Override
	public int SaveprocessAssociatedwithpoForRepack(List<ProcessorderAssociatedPo> list) {
		// TODO Auto-generated method stub
		return productOperatorDao.SaveprocessAssociatedwithpoForRepack(list);

}

	@Override
	public int insertQualityResult(QualityCheckResult qualityCheckResult) {
		// TODO Auto-generated method stub
		return productOperatorDao.insertQualityResult(qualityCheckResult);
	}

	@Override
	public List<ProcessorderProd> processOrderDetailsForQuality(long a) {
		// TODO Auto-generated method stub
		return productOperatorDao.processOrderDetailsForQuality(a);
	}

	@Override
	public List<ProcessorderPckg> displayProcessOrdersforInventoryForRepacking() {
		// TODO Auto-generated method stub
		return productOperatorDao.displayProcessOrdersforInventoryForRepacking();
	}

	@Override
	public List<RepackingOrderRMResponse> RepackingOrderRecipeList(long a) {
		// TODO Auto-generated method stub
		return productOperatorDao.RepackingOrderRecipeList(a);
	}

	@Override
	public int RepackingOrderStatusChange(long repackingOrder, int status, String sso) {
		// TODO Auto-generated method stub
		return productOperatorDao.RepackingOrderStatusChange(repackingOrder,status, sso);
	}

	@Override
	public List<ProcessorderPckg> RepackingOrderRecipee(long a) {
		// TODO Auto-generated method stub
		return productOperatorDao.RepackingOrderRecipee(a);
	}

	@Override
	public int insertPackingStartTime(Long processOrder) {
		// TODO Auto-generated method stub
		return productOperatorDao.insertPackingStartTime(processOrder);
	}
}