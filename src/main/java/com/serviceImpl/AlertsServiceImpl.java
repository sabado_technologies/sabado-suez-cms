package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.AlertsDao;
import com.entity.Alerts;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.NotificationCounterResponse;
import com.service.AlertsService;

@Service
public class AlertsServiceImpl implements AlertsService{
	@Autowired
	AlertsDao alertsDao;

	@Override
	public List<Alerts> alertsForAdmin() {
		List<Alerts> alertsforAdmin = alertsDao.alertsForAdmin();
		return alertsforAdmin;
	}

	@Override
	public int statusChangeForAdmin(String processOrderNumber, int statusId) {
		// TODO Auto-generated method stub
		return alertsDao.statusChangeForAdmin(processOrderNumber,  statusId);
	}

	@Override
	public List<Alerts> alertsForWarehouse() {
		List<Alerts> alertsforWarehouse= alertsDao.alertsForWarehouse();
		return alertsforWarehouse;
	}

	@Override
	public int deleteProductionOrder(Long processOrderNumber, String remarks ,String ssoId) {
		return alertsDao.deleteProductionOrder(processOrderNumber,  remarks ,ssoId);
	}

	@Override
	public List<ProcessorderProd> processOrdersForDeletion() {
		List<ProcessorderProd> processOrdersForDeletion= alertsDao.processOrdersForDeletion();
		return processOrdersForDeletion;
	}

	@Override
	public int deletePackagingOrder(Long processorderPckgNumber, String remarks, String ssoId) {
		// TODO Auto-generated method stub
		return alertsDao.deletePackagingOrder(processorderPckgNumber,  remarks ,ssoId);
	}

	@Override
	public List<ProcessorderPckg> packagingOrdersForDeletion() {
		List<ProcessorderPckg> packagingOrdersForDeletion= alertsDao.packagingOrdersForDeletion();
		return packagingOrdersForDeletion;
	}

	@Override
	public int smsAlertForWHInventory(String processOrderNumber, String ssoId) {
		// TODO Auto-generated method stub
		return alertsDao.smsAlertForWHInventory(processOrderNumber,ssoId);
	}

	@Override
	public int smsAlertForCharging(String processOrderNumber, String ssoId) {
		// TODO Auto-generated method stub
		return alertsDao.smsAlertForCharging(processOrderNumber,ssoId);
	}

	@Override
	public NotificationCounterResponse notificationCountForPo() {
		// TODO Auto-generated method stub
		return alertsDao.notificationCountForPo();
	}

}
