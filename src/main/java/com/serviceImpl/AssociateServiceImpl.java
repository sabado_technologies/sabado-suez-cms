package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.AssociateDao;
import com.entity.Associate;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.service.AssociateService;

@Service
public class AssociateServiceImpl implements AssociateService {
	
	@Autowired
	AssociateDao associateDao;

	@Override
	public List<ProcessorderProd> getProductionProcessOrders() {
		List<ProcessorderProd> processOrders = associateDao.getProductionProcessOrders();
		return processOrders;
	}

	@Override
	public List<ProcessorderPckg> getPackageProcessOrders() {
		List<ProcessorderPckg> packageOrders = associateDao.getPackageProcessOrders();
		return packageOrders;
	}

	@Override
	public int associateProcess(List<Associate> list) {
		return associateDao.associateProcess(list);
	}

	@Override
	public List<ProcessorderPckg> getDeletedProcessOrders() {
		// TODO Auto-generated method stub
		return associateDao.getDeletedProcessOrders();
	}

}
