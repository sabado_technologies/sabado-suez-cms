package com.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.EmailAlertReportDao;
import com.response.EmailAlertResponce;
import com.service.EmailAlertReportService;

@Service
public class EmailAlertReportServiceImpl implements EmailAlertReportService {
	@Autowired
	EmailAlertReportDao emailAlertReportDao;

	@Override
	public List<EmailAlertResponce> getEmailAlert() {
		return emailAlertReportDao.getEmailAlert();
	}

	public List<EmailAlertResponce> getEmailAlertWithRange(Date fromDate, Date toDate) {
		return emailAlertReportDao.getEmailAlertWithRange(fromDate,toDate);
	}

	@Override
	public List<EmailAlertResponce> getPackingAlerts() {
		// TODO Auto-generated method stub
		return emailAlertReportDao.getPackingAlerts();
	}

	@Override
	public List<EmailAlertResponce> getPackingAlertsByDate(Date fromDate, Date toDate) {
		// TODO Auto-generated method stub
		return emailAlertReportDao.getPackingAlertsByDate(fromDate,toDate);
	}

	@Override
	public List<EmailAlertResponce> getTemperature(long processOrder) {
		// TODO Auto-generated method stub
		return emailAlertReportDao.getTemperature(processOrder);
	}

	@Override
	public List<EmailAlertResponce> getRmTemperatureDetails(long processOrder) {
		// TODO Auto-generated method stub
		return emailAlertReportDao.getRmTemperatureDetails(processOrder);
	}

}
