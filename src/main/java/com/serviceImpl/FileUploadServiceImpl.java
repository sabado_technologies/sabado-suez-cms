package com.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.FileUploadDao;
import com.dao.LoginDao;
import com.entity.ProcessorderProd;
import com.request.PdfValues;
import com.request.PdfValuesForPack;
import com.service.FileUploadService;

@Service
public class FileUploadServiceImpl implements FileUploadService   {
	
	
	@Autowired
	FileUploadDao fileUploadDao;

	
	@Override
	public boolean inertIntoProductionOrders(PdfValues pdfValues) {
		fileUploadDao.inertIntoProductionOrders(pdfValues);
		return false;
	}


	@Override
	public boolean inertIntoPackingOrders(PdfValuesForPack pdfValues) {
		fileUploadDao.inertIntoPackagingOrders(pdfValues);
		return false;
	}


	@Override
	public ProcessorderProd getProcessOrder(String po) {
		// TODO Auto-generated method stub
		return fileUploadDao.getProcessOrder(po);
	}


	@Override
	public ProcessorderProd getPackingOrder(String po) {
		// TODO Auto-generated method stub
		return fileUploadDao.getPackingOrder(po);
	}


	@Override
	public boolean inertRePackagingOrders(PdfValuesForPack pdfValues) {
		// TODO Auto-generated method stub
		return fileUploadDao.inertRePackagingOrders(pdfValues);
	}
	



}
