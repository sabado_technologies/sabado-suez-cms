package com.serviceImpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.ManagementDao;
import com.entity.ProcessorderProd;
import com.entity.ProdutionPlanned;
import com.highchart.CurrentQuarterReportResponse;
import com.highchart.ProductForChart;
import com.highchart.YearlyBarchartResponse;
import com.response.AverageCycleTimeResponse;
import com.response.CurrentQuarterWeeklyResponse;
import com.response.CurrentWeekPlannedResponse;
import com.response.CycleTimeResponse;
import com.response.FullQuarterReportResponse;
import com.response.ManagementQuarterProductionPlan;
import com.response.ManagementReportResponse;
import com.response.MonthlyResponse;
import com.response.PackingReportResponse;
import com.response.QuarterBarchartResponse;
import com.response.QuarterBarchartResponse1;
import com.response.RepackingReportResponse;
import com.response.YearlyReportResponse;
import com.service.ManagementService;

@Service
public class ManagementServiceImpl implements ManagementService {
	@Autowired
	ManagementDao managementdao;

	@Override
	public List<ProcessorderProd> getDailyProcessOrders() {
		List<ProcessorderProd> reportLabel = managementdao.getDailyProcessOrders();
		return reportLabel;
	}
//--------------------------------------------------------------------------------------------
	//Current Week Production report
	
	@Override
	public List<ManagementReportResponse> getWeeklyProcessOrders() {
		List<ManagementReportResponse> weekWiseReport = managementdao.getWeeklyProcessOrders();
		return weekWiseReport;
	}
	
	@Override
	public List<ManagementReportResponse> getWeeklyProductionValue() {
		// TODO Auto-generated method stub
		return managementdao.getWeeklyProductionValue();

	}

//----------------------------------------------------------------------------------------------------
//For current year product wise report
	
	@Override
	public List<ManagementReportResponse> productWisereport() {
		List<ManagementReportResponse> productwiseReport = managementdao.productWisereport();
		return productwiseReport;
	}
	
	@Override
	public List<ManagementReportResponse> getPacketSizeCounts() {
		List<ManagementReportResponse> packetCounts = managementdao.getPacketSizeCounts();
		return packetCounts;
	}

	@Override
	public List<ManagementReportResponse> dateWiseProductReport(Long productnumber) {
		List<ManagementReportResponse> datewiseProductReport = managementdao.dateWiseProductReport(productnumber);
		return datewiseProductReport;
	}
	
	@Override
	public List<ManagementReportResponse> getAll1MTReactors(Long productnumber) {
		List<ManagementReportResponse> reactor1MTProducts = managementdao.getAll1MTReactors(productnumber);
		return reactor1MTProducts;
	}

	@Override
	public List<ManagementReportResponse> getAll5MTReactors(Long productnumber) {
		List<ManagementReportResponse> reactor5MTProducts = managementdao.getAll5MTReactors(productnumber);
		return reactor5MTProducts;
	}
//-------------------------------------------------------------------------------------
	//for different year product reports by Selecting Year
	
	@Override
	public List<ManagementReportResponse> productWisereport(int year) {
		List<ManagementReportResponse> productwiseReport = managementdao.productWisereport(year);
		return productwiseReport;
	}
	
	@Override
	public List<ManagementReportResponse> getPacketSizeCountsForYear(int year) {
		List<ManagementReportResponse> packetCounts = managementdao.getPacketSizeCountsForYear(year);
		return packetCounts;
	}

	@Override
	public List<ManagementReportResponse> dateWiseProductReportYearwise(Long productnumber ,int year) {
		List<ManagementReportResponse> datewiseProductReport = managementdao.dateWiseProductReportYearwise(productnumber, year);
		return datewiseProductReport;
	}
	@Override
	public List<ManagementReportResponse> getAll1MTReactors(Long productnumber,int year) {
		List<ManagementReportResponse> reactor1MTProducts = managementdao.getAll1MTReactors(productnumber,year);
		return reactor1MTProducts;
	}

	@Override
	public List<ManagementReportResponse> getAll5MTReactors(Long productnumber, int year) {
		List<ManagementReportResponse> reactor5MTProducts = managementdao.getAll5MTReactors(productnumber,year);
		return reactor5MTProducts;
	}

//--------------------------------------------------------------------------------------------------
 
	//Current Year Current Quarter production Report
	
	@Override
	public CurrentQuarterReportResponse currentQuarterReportResponse() {
		CurrentQuarterReportResponse quaterProduction = managementdao.currentQuaterProductionReport();
		Double actualProduction = Double.valueOf(quaterProduction.getActualProductionPer());
		Double palnnedProduction = Double.valueOf(quaterProduction.getRemainingProductionPer());
		Double reminingProduction = 0.0;
		actualProduction = (actualProduction / palnnedProduction) * 100;
		reminingProduction = (100) - actualProduction;

		quaterProduction.setActualPercentage(Double.valueOf(actualProduction.toString()));
		quaterProduction.setRemainingPercentage(Double.valueOf(reminingProduction.toString()));

		List<ProductForChart> productReports = currentQuaterProductWiseReport();

		List<List<Object>> productInfos = new ArrayList<>();
		for (ProductForChart productForChart : productReports) {
			List<Object> innerVal = new ArrayList<>();
			innerVal.add(productForChart.getProductName());
			innerVal.add(Double.valueOf(productForChart.getQuantity()));
			productInfos.add(innerVal);
		}
		quaterProduction.setProductInfos(productInfos);
		return quaterProduction;
	}
	
	@Override
	public List<ProductForChart> currentQuaterProductWiseReport() {
		return managementdao.currentQuaterProductWiseReport();
	}


	@Override
	public ManagementQuarterProductionPlan currentQuaterProductionPlan() {
		
		ManagementQuarterProductionPlan quaterProductionPlan = managementdao.currentQuaterProductionPlan();
		return quaterProductionPlan;
	}

//--------------------------------------------------------------------------------------------------------	

	// Current year Production Report
	
	@Override
	public MonthlyResponse MontlyProductionProductWiseReport() {
		// TODO Auto-generated method stub
		return managementdao.MontlyProductionProductWiseReport();
	}

	@Override
	public ManagementReportResponse getMonthlyProductionvalue() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return managementdao.getProductionValue(year);
	}
	
//------------------------------------------------------------------------------------------------

	//Multiple year production report(not using)
	@Override
	public MonthlyResponse MontlyProductionProductWiseReport(int year) {
		// TODO Auto-generated method stub
		return managementdao.MontlyProductionProductWiseReport(year);
	}
	
	@Override
	public ManagementReportResponse getMonthlyProductionvalue(int year) {
		return managementdao.getMonthlyProductionvalue(year);
	}

//-------------------------------------------------------------------------------------------------------	
	
    //Full Quarter Production Report for Current year

	@Override
	public List<FullQuarterReportResponse> fullQuaterProductionReport() {
		
		//Full Quarter currentQuarterResponses
		List<FullQuarterReportResponse> quaterProduction = managementdao.fullQuaterProductionReport();
		for(FullQuarterReportResponse e : quaterProduction ){
		int quarterNumber = Integer.valueOf(e.getQuarterNumber());
		Double actualProduction = Double.valueOf(e.getActualProductionPer());
		Double palnnedProduction = Double.valueOf(e.getRemainingProductionPer());
		Double reminingProduction = 0.0;
		actualProduction = (actualProduction / palnnedProduction) * 100;
		reminingProduction = (100) - actualProduction;

		e.setActualProductionPer(Double.valueOf(actualProduction.toString()));
		e.setRemainingProductionPer(Double.valueOf(reminingProduction.toString()));
           
		List<ProductForChart> productReports = quaterProductionProductWiseReport(quarterNumber);
		for(ProductForChart p:productReports){
		List<List<Object>> productInfos = new ArrayList<>();
		for (ProductForChart productForChart : productReports) {
			List<Object> innerVal = new ArrayList<>();
			innerVal.add(productForChart.getProductName());
			innerVal.add(Double.valueOf(productForChart.getQuantity()));
			productInfos.add(innerVal);
		}
		e.setProductInfos(productInfos);
		}
           }
		return quaterProduction;
		
	}


	@Override
	public List<ProductForChart> quaterProductionProductWiseReport(int quarterNumber) {
		List<ProductForChart> ProductWiseReports = managementdao.quaterProductionProductWiseReport(quarterNumber);
		return ProductWiseReports;
	}
	
	@Override
	public List<ManagementQuarterProductionPlan> fullQuaterProductionPlan() {
		List<ManagementQuarterProductionPlan> fullquaterProductionPlan = managementdao.fullQuaterProductionPlan();
		return fullquaterProductionPlan;
	}

	
//------------------------------------------------------------------------------------------------------
	//cycle time for process Orders
	
	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForWeek() {
		List<CycleTimeResponse> completeTime=managementdao.cycleTimeForProcessOrdersForWeek();
		return completeTime;
	}

	@Override
	public AverageCycleTimeResponse averageCycleTimeForProcessOrdersForWeek() {
		AverageCycleTimeResponse averageTime=managementdao.averageCycleTimeForProcessOrdersForWeek();
		return averageTime;
	}
//-----------------------------------------------------------------------------------------------------------------
	//CycleTimeFor month
	@Override
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForMonth() {
		AverageCycleTimeResponse averageTime=managementdao.aveargeCycleTimeForProcessOrdersForMonth();		
		return averageTime;
	}

	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForMonth() {
		List<CycleTimeResponse> completeTime= managementdao.cycleTimeForProcessOrdersForMonth();		
		return completeTime;
	}
//-----------------------------------------------------------------------------------------------------------------
	//CycleTime For Quarter

	@Override
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForQuarter() {
		AverageCycleTimeResponse averageTime=managementdao.aveargeCycleTimeForProcessOrdersForQuarter();		
		return averageTime;	
	}

	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForQuarter() {
		List<CycleTimeResponse> completeTime= managementdao.cycleTimeForProcessOrdersForQuarter();		
		return completeTime;
	}
//-----------------------------------------------------------------------------------------------------------------
		//CycleTime For year

	@Override
	public AverageCycleTimeResponse aveargeCycleTimeForProcessOrdersForYear() {
		AverageCycleTimeResponse averageTime=managementdao.aveargeCycleTimeForProcessOrdersForYear();		
		return averageTime;
	}

	@Override
	public List<CycleTimeResponse> cycleTimeForProcessOrdersForYear() {
		List<CycleTimeResponse> completeTime= managementdao.cycleTimeForProcessOrdersForYear();		
		return completeTime;
	}
	
//-----------------------------------------------------------------------------------------------------
  
//new YearlyProduction Report	
	@Override
	public YearlyReportResponse YearlyProductionReport() {
		return managementdao.YearlyProductionReport();
	}

	@Override
	public List<ManagementReportResponse> getPlannedProduction() {
		return managementdao.getPlannedProduction();
	}

	@Override
	public YearlyBarchartResponse YearlyProductionBarchart() {
		// TODO Auto-generated method stub
		return managementdao.YearlyProductionBarchart();
	}
//multiple year barChart
	@Override
	public YearlyBarchartResponse MultipleYearProductionBarchart(int year) {
		// TODO Auto-generated method stub
		return managementdao.MultipleYearProductionBarchart(year);
	}
	
//-----------------------------------------------------------------------------------------------------------
	/*//current quarter weeklyBarchart

	@Override
	public CurrentQuarterWeeklyResponse currentQuarterWeeklyReport() {
		return managementdao.CurrentQuarterWeeklyResponse();
	}

	@Override
	public ManagementReportResponse getPlannedReport() {
		return managementdao.getPlannedReport();
	}*/
//--------------------------------------------------------------------------------------------------------------
	//Daywise Production Report
	@Override
	public List<ManagementReportResponse> dayWiseProductionReport() {
		// TODO Auto-generated method stub
		return managementdao.dayWiseProductionReport();
	}
	
	@Override
	public List<ManagementReportResponse> dayWiseProductionReport(int quarter) {
		// TODO Auto-generated method stub
		return managementdao.dayWiseProductionReport(quarter);
	}
//------------------------------------------------------------------------------------------------------------------
	//Repacking Report
	
	@Override
	public List<RepackingReportResponse> rePackingReport() {
		// TODO Auto-generated method stub
		return managementdao.rePackingReport();
	}

	
	@Override
	public List<RepackingReportResponse> rePackingReport(int year) {
		// TODO Auto-generated method stub
		return managementdao.rePackingReport(year);
	}
//-----------------------------------------------------------------------------------------------------------
	
	@Override
	public int askingRate() {
		// TODO Auto-generated method stub
		return managementdao.askingRate();
	}

	/*@Override
	public CurrentWeekPlannedResponse getCurrentWeekDates() {
		// TODO Auto-generated method stub
		return managementdao.getCurrentWeekDates();
	}*/

	@Override
	public List<PackingReportResponse> cycleTimeForModal(Long processorderProdNumber) {
		// TODO Auto-generated method stub
		return managementdao.cycleTimeForModal(processorderProdNumber);
	}
	
	@Override
	public QuarterBarchartResponse1 barchartReports(String year) {
		// TODO Auto-generated method stub
		return managementdao.barchartReports(year);
	}
	
	
}
