package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.LoginDao;
import com.entity.User;
import com.request.UserRequest;
import com.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService   {
	
	
	@Autowired
	LoginDao loginDao;
	

	public List<User> getRole(UserRequest user)throws Exception {
		List<User> users = null;
		try {
			users =  loginDao.getRole(user);
		}
		catch(Exception e)
		{
			throw new Exception(e.getMessage());
		}
		return users;
	}

}
