package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PackingOrderInfoDao;
import com.entity.ProcessorderPckg;
import com.service.PackingOrderInfoService;

@Service
public class PackingOrderInfoServiceImpl implements PackingOrderInfoService {
	
	@Autowired
	PackingOrderInfoDao packingOrderInfoDao;

	@Override
	public List<ProcessorderPckg> getPackingInfo(long processOrderNumber) {
		// TODO Auto-generated method stub
		return packingOrderInfoDao.getPackingInfo(processOrderNumber);
	}

}
