package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.QualityDao;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.entity.Quality;
import com.entity.QualityCheck;
import com.service.QualityService;

@Service

public class QualityServiceImpl implements QualityService{
	
	@Autowired
	QualityDao qualityDao;

	@Override
	public List<ProcessorderPckg> getPackageProcessOrders(long processNumber) {
		List<ProcessorderPckg> packageOrders = qualityDao.getPackageProcessOrders(processNumber);
		return packageOrders;
	}

	@Override
	public List<Quality> getQualities() {
		List<Quality> qualities = qualityDao.getQualities();
		return qualities;
	}

	@Override
	public long updatePackage(ProcessorderPckg processorderPckg) {
		return qualityDao.updatePackage(processorderPckg);
	}

	@Override
	public List<ProcessorderProd> getProductDetails() {
		List<ProcessorderProd> productDetails = qualityDao.getProductDetails();
		return productDetails;
	}

	@Override
	public List<QualityCheck> getQualityCheck(long processorderProdNumber) {
		List<QualityCheck> qualityDetails = qualityDao.getQualityCheck(processorderProdNumber);
		return qualityDetails;
	}

	@Override
	public long acceptQuality(long processorderProdNumber,String ssoid) {
		return qualityDao.acceptQuality(processorderProdNumber,ssoid);
	}

	@Override
	public Long rejectQuality(long processorderProdNumber, String ssoid, String comment, String batchNo, String prodNo,
			String memo) {
		return qualityDao.rejectQuality(processorderProdNumber,ssoid,comment,batchNo,prodNo,memo);
	}

	@Override
	public long scrapQuality(long processorderProdNumber, String ssoid, String comment) {
		// TODO Auto-generated method stub
		return qualityDao.scrapQuality(processorderProdNumber, ssoid,comment);
	}

	@Override
	public List<ProcessorderPckg> getRepackDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getRepackCount() {
		// TODO Auto-generated method stub
		return qualityDao.getRepackCount();
	}

}
