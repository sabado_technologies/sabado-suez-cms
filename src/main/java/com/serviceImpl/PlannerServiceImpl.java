package com.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PlannerDao;
import com.request.PlannerRequest;
import com.service.PlannerService;

@Service
public class PlannerServiceImpl implements PlannerService{
	
	@Autowired
	PlannerDao plannerDao;

	@Override
	public int addPlan(PlannerRequest plan) {
		return plannerDao.addPlan(plan);
	}

	@Override
	public PlannerRequest getPlan(int year, int quarter) {
		return plannerDao.getPlan(year, quarter);
	}

	@Override
	public PlannerRequest findPlan(int year, int quarter) {
		return plannerDao.findPlan(year, quarter);
	}

	@Override
	public List<Date> convertDate(List<String> dates) throws ParseException {
		
		List<Date> listOfDate =new ArrayList<Date>();
		try{
			
		} catch(Exception e){
			
		}
		for(String s:dates)
		{
			Date date1=new SimpleDateFormat("dd-MM-yyyy").parse(s);
			listOfDate.add(date1);
			
		}				
		return listOfDate;
	}

	@Override
	public int updatePlan(PlannerRequest plan) {
		return plannerDao.updatePlan(plan);
	}

	@Override
	public String addDates(List<Date> dates) {
		StringBuilder selectedDate=new StringBuilder();
		for(Date date : dates){
			String dateStr = new SimpleDateFormat("dd-MM-yyyy").format(date);
			selectedDate.append(dateStr);
			selectedDate.append(",");
		}
		String str = selectedDate.toString();
		str = str.substring(0, str.length()-1);
		return str;
	}
	
	@Override
	public List<String> changeFormat(List<Date> dates) {
		List<String> changedDates = new ArrayList<String>();
		for(Date date : dates){
			String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(date);
			changedDates.add(dateStr);
		}
		return changedDates;
	}

	@Override
	public int addPlanForReport(List<String> dates , PlannerRequest plan) {
		// TODO Auto-generated method stub
		return plannerDao.addPlanForReport(dates, plan);
	}

	@Override
	public int deletePlan(PlannerRequest plan) {
		// TODO Auto-generated method stub
		return plannerDao.deletePlan(plan);
	}

}
