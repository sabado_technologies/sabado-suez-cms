package com.serviceImpl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.SensorDao;
import com.service.SensorService;

@Service
public class SensorServiceImpl  implements SensorService {
	
	@Autowired
	SensorDao sensorDao; 

	@Override
	public String TemparatureSensor(int tempsensorno) throws IOException {

		
		return sensorDao.TemparatureSensor(tempsensorno);
		
	}

	@Override
	public String weightSensor(int weightsensorno) throws IOException {
		// TODO Auto-generated method stub
		return sensorDao.weightSensor(weightsensorno);
	}


}
