package com.serviceImpl;

import java.util.List;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.entity.MultiRoles;
import com.entity.Role;
import com.entity.User;
import com.entity.UserDetailsForAlerts;
import com.request.AddUserRequest;
import com.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
    UserDao userDao;

	@Override
	public String saveUser(AddUserRequest user , List<String> roles) {
		return userDao.saveUser(user , roles);
	}

	@Override
	public List<Role> getRoles() {
		List<Role> roles = userDao.getRoles();
		return roles;
	}

	@Override
	public List<User> getActiveUsers() {
		List<User> activeUsers = userDao.getActiveUsers();
		return activeUsers;
	}

	@Override
	public List<User> getDeActiveUsers() {
		List<User> deActiveSUsers = userDao.getDeActiveUsers();
		return deActiveSUsers;
		
	}

	@Override
	public String generatedPassword(int length) {
		String letters="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		String numbers="0123456789";
		
		String Password="";
		String pwd=letters+numbers;
		Random r=new Random();
		char[] newPass=new char[length];
		
		for(int i=0;i<length;i++){
			newPass[i]=pwd.charAt(r.nextInt(pwd.length()));
		}
		for(int i=0;i<length;i++){
			Password=Password+newPass[i];
			
		}
		return Password;
	}

	
	@Override
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

	@Override
	public User findUser(String ssoid) {
		return userDao.findUser(ssoid);
	}

	@Override
	public int activateUser(List<User> user) {
		return userDao.activateUser(user);
	}

	@Override
	public int deActivateUser(List<User> user) {
		return userDao.deActivateUser(user);
	}

	@Override
	public String editUser(User user,List<String> roles) {
		return userDao.editUser(user,roles);
		
	}

	@Override
	public User getUserBySSOId(String ssoId) {
		return userDao.getUserBySSOId(ssoId);
	}

	@Override
	public int changePassword(AddUserRequest user) {
		return userDao.changePassword(user);
	}

	@Override
	public int insertEmailAlert(String reactor, int temp, String process, long processOrder, String sso,
			String userName) {
		// TODO Auto-generated method stub
		return userDao.insertEmailAlert(reactor, temp, process, processOrder, sso, userName);
	}

	@Override
	public int resetPassword(String sso, String password) {
		// TODO Auto-generated method stub
		return userDao.resetPassword(sso, password);
	}

	@Override
	public List<MultiRoles> getMultiRoles() {
		// TODO Auto-generated method stub
		return userDao.getMultiRoles();
	}

	@Override
	public List<UserDetailsForAlerts> getAlertUsers() {
		// TODO Auto-generated method stub
		return userDao.getAlertUsers();
	}

	@Override
	public int addUserForAlerts(UserDetailsForAlerts user) {
		// TODO Auto-generated method stub
		return userDao.addUserForAlerts(user);
	}

	@Override
	public int deleteAlertUser(int userId) {
		// TODO Auto-generated method stub
		return userDao.deleteAlertUser(userId);
	}

	@Override
	public int updateAlertUser(UserDetailsForAlerts user) {
		// TODO Auto-generated method stub
		return userDao.updateAlertUser(user);
	}
	

}
