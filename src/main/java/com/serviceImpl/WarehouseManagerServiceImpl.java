/**
 * 
 */
package com.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.WarehouseManagerDao;
import com.entity.InventoryCheckDetails;
import com.entity.ProcessOrderRm;
import com.entity.ProcessTracker;
import com.entity.ProcessorderPckg;
import com.entity.ProcessorderProd;
import com.response.LocationRawmaterialDetails;
import com.response.NotificationTabsForWarehouse;
import com.response.PackingOrderInventoryPostingResponse;
import com.response.ProcessOrderInventoryPostingResponse;
import com.response.RepackingProcessOrderCount;
import com.service.WarehouseManagerService;

/**
 * @author ullas
 *
 */
@Service
public class WarehouseManagerServiceImpl implements WarehouseManagerService {

	@Autowired
	WarehouseManagerDao warehouseManagerDao;

	public List<ProcessorderProd> getAllProcessOrders() {
		// TODO Auto-generated method stub
		List<ProcessorderProd> listOfProcessOrders = warehouseManagerDao.getAllProcessOrders();
		return listOfProcessOrders;
	}

	@Override
	public List<ProcessOrderRm> getProcessOrderDetails(Long processorderProdNumber) {
		// TODO Auto-generated method stub
		List<ProcessOrderRm> listOfProcessOrdersdetais = warehouseManagerDao
				.getProcessOrderDetails(processorderProdNumber);
		return listOfProcessOrdersdetais;
	}

	@Override
	public List<ProcessorderProd> getProcessOrderCount() {
		List<ProcessorderProd> ProcessOrderCount = warehouseManagerDao.getProcessOrderCount();
		return ProcessOrderCount;
	}

	@Override
	public List<ProcessorderProd> getProcessOrderCompletedCount() {
		List<ProcessorderProd> ProcessOrderCompletedCount = warehouseManagerDao.getProcessOrderCompletedCount();
		return ProcessOrderCompletedCount;
	}

	@Override
	public int setProcessOrderTrackerStatus(Long processorderProdNumber, String ssoId) {
		int ProcessTrackerId = warehouseManagerDao.setProcessOrderTrackerStatus(processorderProdNumber, ssoId);
		return ProcessTrackerId;
	}

	@Override
	public int updateProcessOrderStatus(Long processorderProdNumber, String processTrackerId,String ssoId) {
		int ProcessOrderStatus = warehouseManagerDao.updateProcessOrderStatus(processorderProdNumber, processTrackerId ,ssoId);
		return ProcessOrderStatus;
	}

	@Override
	public int setTrackerStatusForRm(ProcessTracker processTrackerTable) {
		int ProcessTrackerId = warehouseManagerDao.setTrackerStatusForRm(processTrackerTable);
		return ProcessTrackerId;
	}

	@Override
	public List<ProcessorderProd> getAllTodayProcessOrders() {
		List<ProcessorderProd> listOfTodayProcessOrders = warehouseManagerDao.getAllTodayProcessOrders();
		return listOfTodayProcessOrders;

	}

	@Override
	public List<ProcessorderProd> getProcessOrdersForQuality() {
		List<ProcessorderProd> listOfQualityCheckProcessOrders = warehouseManagerDao.getProcessOrdersForQuality();
		return listOfQualityCheckProcessOrders;
	}

	@Override
	public List<ProcessorderPckg> associatedPackingInfo(long processorderProdNumber) {
		List<ProcessorderPckg> associatedPackingInfo = warehouseManagerDao
				.associatedPackingInfo(processorderProdNumber);
		return associatedPackingInfo;
	}

	@Override
	public int setTrackerForWarehouseQuality(String ssoId, String locationPlaced, String containerQuantity,ProcessorderPckg packingdetail) {
		int ProcessTrackerId = warehouseManagerDao.setTrackerForWarehouseQuality(ssoId,locationPlaced,containerQuantity, packingdetail);
		return ProcessTrackerId;
	}

	@Override
	public int setTrackerforInventory(Long processorderProdNumber, String ssoId) {
		int ProcessOrderStatus = warehouseManagerDao.setTrackerforInventory(processorderProdNumber ,ssoId);
		return ProcessOrderStatus;
	}

	@Override
	public int setRmLocationDetails(Long processorderProdNumber, String rmId,
			LocationRawmaterialDetails locationRawmaterialDetails) {
		int LocationStatus = warehouseManagerDao.setRmLocationDetails(processorderProdNumber,rmId,
				locationRawmaterialDetails);
		return LocationStatus;
	}

	@Override
	public List<ProcessorderProd> getProcessOrdersForQuality(Long processorderProdNumber) {
		List<ProcessorderProd> listOfQualityCheckProcessOrders = warehouseManagerDao
				.getProcessOrdersForQuality(processorderProdNumber);
		return listOfQualityCheckProcessOrders;
	}

	@Override
	public List<ProcessorderProd> getAllPendingProcessOrders() {
		List<ProcessorderProd> listOfProcessOrders = warehouseManagerDao.getAllPendingProcessOrders();
		return listOfProcessOrders;
	}

	@Override
	public int setProcessOrderTrackerStatusForRejection(Long processorderProdNumber, String ssoId ,String submitMessage) {
		int ProcessTrackerId = warehouseManagerDao.setProcessOrderTrackerStatusForRejection(processorderProdNumber,
				ssoId ,submitMessage);
		return ProcessTrackerId;
	}

	@Override
	public List<ProcessorderProd> getAllContainerDetailsForProcessOrders() {
		List<ProcessorderProd> listOfProcessOrders = warehouseManagerDao.getAllContainerDetailsForProcessOrders();
		return listOfProcessOrders;
	}

	@Override
	public List<InventoryCheckDetails> getContainerDetailsForProcessOrder(Long processorderProdNumber) {
		List<InventoryCheckDetails> getContainerDetailsForProcessOrder = warehouseManagerDao
				.getContainerDetailsForProcessOrder(processorderProdNumber);
		return getContainerDetailsForProcessOrder;
	}

	@Override
	public List<ProcessorderPckg> getPackingOrderForInventory() {
		List<ProcessorderPckg> PackingOrderForInventory = warehouseManagerDao
				.getPackingOrderForInventory();
		return PackingOrderForInventory;
	}

	@Override
	public List<ProcessorderPckg> getRePackingOrderForRM() {
		List<ProcessorderPckg> PackingOrderForRM = warehouseManagerDao.getRePackingOrderForRM();
		return PackingOrderForRM;
	}

	@Override
	public List<ProcessorderPckg> getRePackingOrderForRMDetails(Long processorderPckgNumber) {
		List<ProcessorderPckg> PackingOrderForRM = warehouseManagerDao.getRePackingOrderForRMDetails(processorderPckgNumber);
		return PackingOrderForRM;
	}

	@Override
	public int setRmLocationDetailsForRepack(Long processorderPckgNumber,String ssoId ,
			LocationRawmaterialDetails locationRawmaterialDetails) {
		
			int LocationStatus = warehouseManagerDao.setRmLocationDetailsForRepack(processorderPckgNumber,ssoId ,locationRawmaterialDetails);
			return LocationStatus;
		}

	@Override
	public List<ProcessorderPckg> getAllContainerDetailsForRepackProcessOrders() {
		List<ProcessorderPckg> listOfRepackProcessOrders = warehouseManagerDao.getAllContainerDetailsForRepackProcessOrders();
		return listOfRepackProcessOrders;
	}

	@Override
	public List<InventoryCheckDetails> getContainerDetailsForRepackProcessOrder(Long processorderPckgNumber) {
		List<InventoryCheckDetails> getContainerDetailsForRepackProcessOrder = warehouseManagerDao
				.getContainerDetailsForRepackProcessOrder(processorderPckgNumber);
		return getContainerDetailsForRepackProcessOrder;
	}

	@Override
	public List<ProcessorderPckg> getPackingOrderForInventory1(Long processorderPckgNumber) {
		
			List<ProcessorderPckg> PackingOrderForInventory1 = warehouseManagerDao
					.getPackingOrderForInventory1(processorderPckgNumber);
			return PackingOrderForInventory1;
		
	}

	@Override
	public List<ProcessorderPckg> associatedPackingInfoForPosting(long processorderProdNumber) {
		List<ProcessorderPckg> associatedPackingInfoForPosting = warehouseManagerDao
				.associatedPackingInfoForPosting(processorderProdNumber);
		return associatedPackingInfoForPosting;
	}

	@Override
	public List<PackingOrderInventoryPostingResponse> getPackingOrderForInventoryPosting(Long processorderPckgNumber) {
		List<PackingOrderInventoryPostingResponse> getPackingOrderForInventoryPosting = warehouseManagerDao
				.getPackingOrderForInventoryPosting(processorderPckgNumber);
		return getPackingOrderForInventoryPosting;
	}

	@Override
	public List<ProcessorderProd> getProcessOrdersForPosting() {
		List<ProcessorderProd> getProcessOrdersForPosting = warehouseManagerDao.getProcessOrdersForPosting();
		return getProcessOrdersForPosting;
	}

	@Override
	public List<ProcessOrderInventoryPostingResponse> getProcessOrdersForPostingCheck(Long processorderProdNumber) {
		List<ProcessOrderInventoryPostingResponse> getProcessOrdersForPostingCheck = warehouseManagerDao
				.getProcessOrdersForPostingCheck(processorderProdNumber);
		return getProcessOrdersForPostingCheck;
	}

	@Override
	public List<ProcessorderPckg> getPackingOrderForInventoryPostingList() {
		List<ProcessorderPckg> getPackingOrderForInventoryPostingList = warehouseManagerDao
				.getPackingOrderForInventoryPostingList();
		return getPackingOrderForInventoryPostingList;
	}

	@Override
	public int setProcessOrderStatusForPosting(Long processorderProdNumber ,String ssoId) {
		int ProcessTrackerId = warehouseManagerDao.setProcessOrderStatusForPosting(processorderProdNumber ,ssoId);
		return ProcessTrackerId;
	}

	@Override
	public int setProcessOrderStatusForRepackPosting(Long processorderPckgNumber ,String ssoId) {
		int ProcessTrackerId = warehouseManagerDao.setProcessOrderStatusForRepackPosting(processorderPckgNumber ,ssoId);
		return ProcessTrackerId;
	}

	@Override
	public RepackingProcessOrderCount getRepackingCount() {
		RepackingProcessOrderCount getRepackingCount = warehouseManagerDao.getRepackingCount();
		return getRepackingCount;
	}

	@Override
	public NotificationTabsForWarehouse getNotificationForWarehouseCount() {
		NotificationTabsForWarehouse getNotificationForWarehouseCount = warehouseManagerDao.getNotificationForWarehouseCount();
		return getNotificationForWarehouseCount;
	}

	
	
	
}
