package com.highchart;

import java.util.List;

public class CurrentQuarterReportResponse {
	private Double actualProductionPer;
	private Double remainingProductionPer;
	private Double remainingPercentage;
	private Double actualPercentage;
	private int workingDatesCount;
	private List<List<Object>> productInfos;
	public CurrentQuarterReportResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CurrentQuarterReportResponse(Double actualProductionPer, Double remainingProductionPer,
			Double remainingPercentage, Double actualPercentage, int workingDatesCount,
			List<List<Object>> productInfos) {
		super();
		this.actualProductionPer = actualProductionPer;
		this.remainingProductionPer = remainingProductionPer;
		this.remainingPercentage = remainingPercentage;
		this.actualPercentage = actualPercentage;
		this.workingDatesCount = workingDatesCount;
		this.productInfos = productInfos;
	}
	public Double getActualProductionPer() {
		return actualProductionPer;
	}
	public void setActualProductionPer(Double actualProductionPer) {
		this.actualProductionPer = actualProductionPer;
	}
	public Double getRemainingProductionPer() {
		return remainingProductionPer;
	}
	public void setRemainingProductionPer(Double remainingProductionPer) {
		this.remainingProductionPer = remainingProductionPer;
	}
	public Double getRemainingPercentage() {
		return remainingPercentage;
	}
	public void setRemainingPercentage(Double remainingPercentage) {
		this.remainingPercentage = remainingPercentage;
	}
	public Double getActualPercentage() {
		return actualPercentage;
	}
	public void setActualPercentage(Double actualPercentage) {
		this.actualPercentage = actualPercentage;
	}
	public int getWorkingDatesCount() {
		return workingDatesCount;
	}
	public void setWorkingDatesCount(int workingDatesCount) {
		this.workingDatesCount = workingDatesCount;
	}
	public List<List<Object>> getProductInfos() {
		return productInfos;
	}
	public void setProductInfos(List<List<Object>> productInfos) {
		this.productInfos = productInfos;
	}

	

}
