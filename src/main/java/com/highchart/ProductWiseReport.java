package com.highchart;

import java.util.ArrayList;
import java.util.List;

public class ProductWiseReport {
	private List<List<String>> listOfLists = new ArrayList<List<String>>();

	public List<List<String>> getListOfLists() {
		return listOfLists;
	}

	public void setListOfLists(List<List<String>> listOfLists) {
		this.listOfLists = listOfLists;
	}

}
