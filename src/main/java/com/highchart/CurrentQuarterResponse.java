package com.highchart;

import java.util.List;

public class CurrentQuarterResponse {
	private int year;
	private List<Double> plannedValues;
	private List<Double> actualValues;
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List<Double> getPlannedValues() {
		return plannedValues;
	}
	public void setPlannedValues(List<Double> plannedValues) {
		this.plannedValues = plannedValues;
	}
	public List<Double> getActualValues() {
		return actualValues;
	}
	public void setActualValues(List<Double> actualValues) {
		this.actualValues = actualValues;
	}
	

}
