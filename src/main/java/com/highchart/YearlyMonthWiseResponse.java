package com.highchart;

import java.util.List;

public class YearlyMonthWiseResponse {
	private int year;
	private List<Double> planned;
	private List<Double> actual;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public List<Double> getPlanned() {
		return planned;
	}

	public void setPlanned(List<Double> planned) {
		this.planned = planned;
	}

	public List<Double> getActual() {
		return actual;
	}

	public void setActual(List<Double> actual) {
		this.actual = actual;
	}

}
