package com.highchart;

import java.util.List;

import com.response.DailyDataResponse;
import com.response.PlannedReportResponse;
import com.response.QuarterlyDataResponse;
import com.response.WeeklyDataResponse;

public class YearlyBarchartResponse {
	private int year;
	private List<QuarterlyDataResponse> quarterlyData;
	private List<WeeklyDataResponse> weeklyData;
	private List<DailyDataResponse> dailyData;
	private List<PlannedReportResponse> plannedData;
	public YearlyBarchartResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public YearlyBarchartResponse(int year, List<QuarterlyDataResponse> quarterlyData,
			List<WeeklyDataResponse> weeklyData, List<DailyDataResponse> dailyData,
			List<PlannedReportResponse> plannedData) {
		super();
		this.year = year;
		this.quarterlyData = quarterlyData;
		this.weeklyData = weeklyData;
		this.dailyData = dailyData;
		this.plannedData = plannedData;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List<QuarterlyDataResponse> getQuarterlyData() {
		return quarterlyData;
	}
	public void setQuarterlyData(List<QuarterlyDataResponse> quarterlyData) {
		this.quarterlyData = quarterlyData;
	}
	public List<WeeklyDataResponse> getWeeklyData() {
		return weeklyData;
	}
	public void setWeeklyData(List<WeeklyDataResponse> weeklyData) {
		this.weeklyData = weeklyData;
	}
	public List<DailyDataResponse> getDailyData() {
		return dailyData;
	}
	public void setDailyData(List<DailyDataResponse> dailyData) {
		this.dailyData = dailyData;
	}
	public List<PlannedReportResponse> getPlannedData() {
		return plannedData;
	}
	public void setPlannedData(List<PlannedReportResponse> plannedData) {
		this.plannedData = plannedData;
	}
	
}
